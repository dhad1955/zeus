package net.tazogaming.hydra.runtime.login.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.GraveStone;
import net.tazogaming.hydra.game.gametick.ZoneUpdateTick;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.game.minigame.pkleague.PKTournament;
import net.tazogaming.hydra.game.minigame.pkraffle.PKRaffle;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.skill.farming2.Farming;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.runtime.login.LoginHook;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.SystemUpdate;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 15:43
 */
public class StaticLoginHook implements LoginHook {

    /**
     * Method preLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void preLogin(Player player) {
        player.setLastPacket(Core.currentTimeMillis());
    }

    /**
     * Method postLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void postLogin(Player player) {
        player.getEquipment().updateWeapon();
        player.setStartupEvents();
        player.getBank().checkWhips();
        if (player.getAccount().hasVar("double_xp")) {
            player.getAccount().setFlag(Account.DOUBLE_XP, true);
        }

        if (player.getAccount().hasVar("double_xp_2")) {
            player.getAccount().setFlag(Account.DOUBLE_XP_2, true);
        }

        if (player.getCurrentHouse() != null) {
            if (player.getCurrentHouse().isRegistered(player)) {
                House.re_render(player, player.getCurrentHouse());
            } else {
                player.getCurrentHouse().destructHouse(player);
            }
        }

        player.setKnownClientLoc(player.getLocation());

        if (SystemUpdate.isActive()) {
            SystemUpdate.sendTimer(player);
        }

        for (int i = 0; i < 2000; i++) {
            int button = player.getAccount().getButtonConfig(i);
            int main   = player.getAccount().get(i);

            if (button != -1) {
                player.getActionSender().sendBConfig(button, player.getAccount().getButtonConfig(i));
            }

            if (main != -1) {
                player.getActionSender().sendVar(i, main);
            }
        }

        for (int i = 0; i < 25; i++) {
            player.getActionSender().sendStat(i);
        }

        player.getActionSender().updateEnergy();

        if (player.getAccount().hasVar("infinite_run")) {
            player.getAccount().putSmall(Account.INFINITE_RUN, 1);
        }

        if (player.getCutScene() != null) {
            player.getCutScene().rewind();
        }

        if ((player.getAccount().get(Account.TUTORIAL_STAGE) == 0)) {
            player.getGameFrame().changeRoot(1028);

            for(Player p : World.getWorld().getPlayers()){
                if(p.getRights() >= Player.MODERATOR){
                    p.getActionSender().sendMessage(Text.RED("ALERT!!!")+" NEW PLAYER JOINED "+player.getUsername());
                }
            }
            player.getExps()[3] = GameMath.getXPForLevel(10);
            player.setCurStat(3, 10);
            player.setCurrentHealth(10);
            player.setMaxStat(3, 10);
            player.getActionSender().sendStat(3);
        } else {
            player.getGameFrame().changeRoot(player.getGameFrame().getRoot());
        }

        player.getGameFrame().sendFullScreenAMasks();
        player.getGameFrame().sendFixedAMasks();
        player.getGameFrame().getContextMenu().render();
        player.getFriendsList().resetStatus();
        player.getFriendsList().sendFriends();
        player.getActionSender().sendHintIcon(player.getGameFrame().getCurrentHintIcon());
        Farming.bind(player);

        if (player.getChannel() != null) {
            ClanChannel.sendClanList(player, player.getChannel());
        }

        if (Main.TEST_MODE) {
            player.getActionSender().sendMessage(
                Text.RED("WARNING: SERVER IN TEST MODE, NO AUTHENTICATION OR SAVEGAMES WORKING."));
        }

        GraveStone.checkGravestones(player);

        if (player.getAccount().getInt32("pmunread") > 0) {
            player.getActionSender().sendMessage(Text.RED("You have " + player.getAccount().getInt32("pmunread")
                    + " new messages type ::forums to view it"));
        }

        if (Config.doublePk) {
            player.getActionSender().sendMessage(Text.RED("Double PK Points is now active!"));
        }

        PKTournament.puresLeaderboard.checkPrizeClaim(player);
        PKTournament.mainsLeaderboard.checkPrizeClaim(player);
        BossTournament.mainsLeaderboard.checkPrizeClaim(player);
        PKTournament.zerkersLeaderboard.checkPrizeClaim(player);

        for (Zone e : player.getAreas()) {
            ZoneUpdateTick.zoneRefreshed(player, e);
        }

        PKRaffle.checkRaffle(player);
    }
}
