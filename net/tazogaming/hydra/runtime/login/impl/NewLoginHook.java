package net.tazogaming.hydra.runtime.login.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.runtime.login.LoginHook;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.util.Config;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 15:47
 */
public class NewLoginHook implements LoginHook {

    public static final List<Trigger> loginHooks = new ArrayList<Trigger>();


    /**
     * Method preLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void preLogin(Player player) {
        player.setGameFrame(new GameFrame(player));
        player.getGameFrame().init();
        SpecialAttacks.updateSpecial(player);
        if(player.getUsername().equalsIgnoreCase("daniel")){
            player.getAppearance().resetAppearence();
        }
    }

    /**
     * Method postLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void postLogin(Player player) {

        if (Config.double_drops) {
            player.getActionSender().sendMessage("@red@x2 DROPS ARE NOW ACTIVE!!!!!!!!!");
        }

        SignedBlock block = World.getWorld().getScriptManager().get_block("new_login");

        if (block != null) {
            Scope scr = new Scope();

            scr.setRunning(block);
            scr.setController(player);
            player.getNewScriptQueue().add(scr);
        }

        player.getActionSender().sendMessage("Welcome to Hydrascape 2");
        player.loadFamiliar();

        player.log("Logged into game [Fresh login] uid="+player.getUID()+" ip address="+player.getCurrentIP());
        if (player.getRights() >= Player.ADMINISTRATOR) {
            World.getWorld().getScriptManager().addDeveloper(player);
        }

        Scope scope;

        for (Trigger t : loginHooks) {
            scope = new Scope();
            scope.setRunning(t);
            scope.setController(player);

            if (!World.getWorld().getScriptManager().runScript(scope)) {
                player.getNewScriptQueue().add(scope);
            }
        }

        if(player.getAccount().hasVar("auto_join")){
            ClanChannel channel = World.getWorld().getChannelList().getByPlayer(player.getAccount().getString("auto_join"));
            if(channel != null){
                channel.addPlayer(player);
            }
        }
       try {
           if (player.getAccount().hasVar("spell_book")) {
               player.getGameFrame().changeTab(GameFrame.SIDEBAR_MAGIC, player.getAccount().getInt32("spell_book"));
           }
       }catch (Exception error) {}

    }
}
