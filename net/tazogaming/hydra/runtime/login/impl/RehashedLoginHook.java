package net.tazogaming.hydra.runtime.login.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.login.LoginHook;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 15:46
 */
public class RehashedLoginHook implements LoginHook {
    public static final List<Trigger> loginHooks = new ArrayList<Trigger>();

    /**
     * Method preLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void preLogin(Player player) {}

    /**
     * Method postLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void postLogin(Player player) {
        SignedBlock block = World.getWorld().getScriptManager().get_block("reconnect");

        if (block != null) {
            Scope scr = new Scope();

            scr.setRunning(block);
            scr.setController(player);
            player.getNewScriptQueue().add(scr);
        }

        player.log("Logged into game [rehash]");

        Scope scope;

        for (Trigger t : loginHooks) {
            scope = new Scope();
            scope.setRunning(t);
            scope.setController(player);

            if (!World.getWorld().getScriptManager().runScript(scope)) {
                player.getNewScriptQueue().add(scope);
            }
        }

        if(player.getGameFrame().getDialog() != null){
            player.getGameFrame().getDialog().draw(player);
            player.getGameFrame().showDialog(player.getGameFrame().getDialog());
        }
    }
}
