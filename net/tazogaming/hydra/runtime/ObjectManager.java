package net.tazogaming.hydra.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/10/13
 * Time: 14:35
 */
public class ObjectManager implements RecurringTickEvent {

    /** objectsToUpdate made: 14/09/29 **/
    private ArrayList<GameObject> objectsToUpdate = new ArrayList<GameObject>();

    /**
     * Constructs ...
     *
     */
    public ObjectManager() {
        Core.submitTask(this);
    }

    /**
     * Method isRegistered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj
     *
     * @return
     */
    public boolean isRegistered(GameObject obj) {
        return objectsToUpdate.contains(obj);
    }

    /**
     * Method updateObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateObjects() {
        for (Iterator<GameObject> iterator = objectsToUpdate.iterator(); iterator.hasNext(); ) {
            GameObject object = iterator.next();

            if (object.isScheduledRemoved() && object.needsChange()) {
                object.remove();
            }

            if (object.isRemoved() || (!object.isDoor() && object.getDefaultId() != 733 && object.isMapObject() && object.needsRemove() && object.isDefault())) {
                iterator.remove();    // remove from list, it's un-needed since the client has its default status stored
                object.setLocation(null);

                if (!object.isMapObject() && (object.getId() > 0)) {
                    ClippingDecoder.removeClippingForSolidObject(object.getX(), object.getY(), object.getHeight(),
                            CacheObjectDefinition.forId(object.getDefaultId()).sizeX,
                            CacheObjectDefinition.forId(object.getDefaultId()).sizeY, true, true);
                }
            }

            if (!object.isDefault() && object.needsChange()) {    // change back
                object.revertAll();
            }
        }
    }

    /**
     * Method registerObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj
     */
    public void registerObject(GameObject obj) {
        objectsToUpdate.add(obj);

        if (obj.getId() == 0) {
            ClippingDecoder.removeClipping(obj.getX(), obj.getY(), obj.getHeight());
        } else {
            ClippingDecoder.addObject(obj.getDefaultId(), obj.getX(), obj.getY(), obj.getHeight(), obj.getType(),
                                      obj.getRotation(), false);
        }
    }

    /**
     * Method registerObjectUnclipped
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj
     */
    public void registerObjectUnclipped(GameObject obj) {
        objectsToUpdate.add(obj);
    }

    private static int findNextDoor(int id) {
        CacheObjectDefinition doorDef = CacheObjectDefinition.forId(id);
        String                option  = doorDef.options[0];

        if (option.equalsIgnoreCase("open")) {
            for (int i = 0; i < 4; i++) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("close")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }

            for (int i = 0; i > -4; i--) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("close")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }
        } else if (option.equalsIgnoreCase("close")) {
            for (int i = 0; i > -4; i--) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("open")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }

            for (int i = 0; i < 4; i++) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("open")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }
        }

        return 0;
    }

    /**
     * Method openDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param object
     *
     * @return
     */
    public boolean openDoor(Player player, final GameObject object) {
        int direction  = (object.getRotation() + 1) % 4;
        int transformX = 0,
            transformY = 0;

        switch (object.getRotation() % 4) {
        case 0 :
            transformX--;

            break;

        case 1 :
            transformY++;

            break;

        case 2 :
            transformX++;

            break;

        case 3 :
            transformY--;

            break;
        }

        int nextDoor = findNextDoor(object.getDefaultId());

        if (nextDoor == 0) {
            player.getActionSender().sendMessage("error: unable to find door: " + object.getDefaultId());

            return false;
        }

        // ClippingDecoder.removeClippingForVariableObject(object.getX(), object.getY(), object.getHeight(),
        // object.getType(), object.getRotation(), true);
        object.remove();

        GameObject newObject = new GameObject();

        newObject.setCurrentRotation(direction);
        newObject.setObjectType(0);
        newObject.setLocation(Point.location(object.getX() + transformX, object.getY() + transformY,
                object.getHeight()));
        newObject.setIdentifier(nextDoor);
        object.setLocation(null);
        newObject.setDoor(true);
        newObject.setOpen(true);

        return true;
    }

    /**
     * Method closeDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param object
     *
     * @return
     */
    public boolean closeDoor(Player player, final GameObject object) {
        int direction = object.getRotation() - 1;

        if (direction < 0) {
            direction *= -1;
        }

        int transformX = 0,
            transformY = 0;

        switch (object.getRotation() % 4) {
        case 0 :
            transformY++;

            break;

        case 1 :
            transformX++;

            break;

        case 2 :
            transformY--;

            break;

        case 3 :
            transformX--;

            break;
        }

        int nextDoor = findNextDoor(object.getDefaultId());

        if (nextDoor == 0) {
            player.getActionSender().sendMessage("error: unable to find door: " + object.getDefaultId());

            return false;
        }


        object.remove();

        GameObject newObject = new GameObject();

        newObject.setCurrentRotation(direction);
        newObject.setObjectType(0);
        newObject.setLocation(Point.location(object.getX() + transformX, object.getY() + transformY,
                object.getHeight()));
        newObject.setIdentifier(nextDoor);
        newObject.setDoor(true);
        newObject.setOpen(false);
        object.setLocation(null);

        return true;
    }

    /**
     * Method getObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public GameObject getObject(int id, int x, int y, int h) {
        Tile t = World.getWorld().getTile(x, y, h);

        if (t.hasObjects()) {
            for (GameObject e : t.getObjects()) {
                if ((e.getId() == id) || (e.getDefaultId() == id)) {
                    return e;
                }
            }
        }

        return null;
    }

    /**
     * Method getDefaultObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectId
     * @param objectX
     * @param objectY
     * @param z
     *
     * @return
     */
    public GameObject getDefaultObject(int objectId, int objectX, int objectY, int z) {
        GameObject try_object = getObject(objectId, objectX, objectY, z);

        if (try_object != null) {
            return try_object;
        }

        Tile t = World.getWorld().getTile(objectX, objectY, z);

        if (t.hasMappedObject()) {
            if (t.getMappedObjectId() == objectId ||objectId == -2) {
                GameObject obj = new GameObject();

                obj.setIdentifier(objectId);
                obj.setObjectType(t.getMappedType());
                obj.setIsMapObject(true);
                obj.setDefault(true);

                obj.setCurrentRotation(t.getMappedRotation());
                obj.setLocation(Point.location(objectX, objectY, z));
                obj.setLastInteraction(Core.currentTime);
                registerObject(obj);

                return obj;
            }
        }

        return null;
    }

    /**
     * Method save_custom_object
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj
     */
    public static void save_custom_object(GameObject obj) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("config/world/object_placements.cfg", true));

            writer.write("obj = " + obj.getDefaultId() + " " + obj.getX() + " " + obj.getY() + " " + obj.getHeight()
                         + " " + obj.getRotation() + " " + obj.getType());
            writer.newLine();
            writer.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method load_custom_objects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     */
    public void load_custom_objects(File location) {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(location));

            while (true) {
                String line = reader.readLine();

                if (line == null) {
                    break;
                }

                if (line.startsWith("//") ||!line.startsWith("obj") || (line.length() == 0)) {
                    continue;
                }

                ArgumentTokenizer pr = new ArgumentTokenizer(line.split(" "));

                pr.next();    // obj
                pr.next();    // =

                int        id            = pr.nextInt();
                int        x             = pr.nextInt();
                int        y             = pr.nextInt();
                int        h             = pr.nextInt();
                int        rotation      = pr.nextInt();
                int        placementType = pr.nextInt();
                GameObject obj           = new GameObject();

                obj.setIdentifier(id);
                obj.setObjectType(placementType);
                obj.setCurrentRotation(rotation);
                obj.setLocation(Point.location(x, y, h));
                registerObject(obj);

                if (id == 0) {
                    ClippingDecoder.unclip(x, y, h);
                }
            }

            reader.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        updateObjects();

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
