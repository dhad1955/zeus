package net.tazogaming.hydra.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.IoUtils;
import net.tazogaming.hydra.script.sched.*;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/08/14
 * Time: 18:55
 */
public class ServerTaskRegister extends ScheduledScriptTaskRegister implements RecurringTickEvent {

    /**
     * Constructs a new Server task register
     *
     */
    public ServerTaskRegister() {
        byte[] buffer = IoUtils.ReadFile("config/tasks_global.sched");

        if (buffer != null) {
            load(buffer, null);
        }

        Core.submitTask(this);
    }



    public void prepareForReload() {

    }

    @Override
    protected void saveTasks(byte[] buffer, Object o) {
        try {
            IoUtils.dumpFile("config/tasks_global.sched", buffer);
        } catch (IOException ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        update();
    }

    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
        return false;
    }
}
