package net.tazogaming.hydra.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.PacketQueue;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketAnalyzer;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.runtime.login.LoginHook;
import net.tazogaming.hydra.runtime.login.impl.NewLoginHook;
import net.tazogaming.hydra.runtime.login.impl.RehashedLoginHook;
import net.tazogaming.hydra.runtime.login.impl.StaticLoginHook;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.LogCleanupRequest;
import net.tazogaming.hydra.util.Logger;

import net.tazogaming.hydra.util.background.BackgroundServiceRequest;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/*
*
* The central motor of the game. This class is responsible for the
* primary operation of the entire game.
 */

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public final class GameEngine implements Runnable {

    //
    public static final int CYCLE_SPEED = 595;

    /*
     *
     * The thread execution process.
     */
    public static int lastCycle = 0;

    /*
     *
     * The packet queue to be processed
     */

    /** packetQueue made: 14/10/22 */
    private PacketQueue packetQueue = new PacketQueue();

    /*
     *
     * Whether the engine's thread is running
     */

    /** running made: 14/10/22 */
    private boolean running = true;

    /*
     *
     * The mapping of packet IDs to their handler
     */

    /** packetHandlers made: 14/10/22 */
    private PacketHandler[] packetHandlers = new PacketHandler[256];

    /*
     *
     * Responsible for updating all connected clients
     */

    /** clientUpdater made: 14/10/22 */
    private Core clientUpdater = new Core();

    /** lastSentMajorUpdate made: 14/10/22 */
    private long lastSentMajorUpdate;

    /**
     * Method timeTillNextUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int timeTillNextUpdate() {
        return (int) (600 - (System.currentTimeMillis() - lastSentMajorUpdate));
    }

    /*
     *
     * Handles delayed events rather than events to be ran every iteration
     */

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void run() {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

        long lastTime;

        while (running) {
            try {
                Thread.sleep(7);
                lastTime = System.currentTimeMillis();
                Core.setTime(lastTime);
                processPlayersAwaitingLogin();

                long curTime = System.currentTimeMillis();

                if (curTime - Config.lastLogCleanup > 1000 * 60 * 60 * 24) {
                    Config.lastLogCleanup = System.currentTimeMillis();
                    World.getWorld().getBackgroundService().submitRequest(new LogCleanupRequest());
                }

                if (curTime - lastSentMajorUpdate >= CYCLE_SPEED) {
                    lastSentMajorUpdate = curTime;

                    long start = System.currentTimeMillis();

                    clientUpdater.majorUpdate();
                    clientUpdater.minorUpdate();

                    if (System.currentTimeMillis() - start > 5033) {
                        System.err.println("WARNING, SLOW Updating time: " + (System.currentTimeMillis() - start)
                                           + "ms");
                    }
                }

                processIncomingPackets();
                GameEngine.lastCycle = (int) (System.currentTimeMillis() - lastTime);
                if (GameEngine.lastCycle > 100) {
                    System.err.println("[WARNING] SLOW Cycle time: " + GameEngine.lastCycle);
                }
            } catch (Exception e) {
                lastSentMajorUpdate = 0L;
                Logger.err("Error processing updates,");
                Logger.err(e);
            }
        }
    }

    /*
     *
     * Returns the current packet queue.
     *
     * @return A <code>PacketQueue</code>
     */

    /**
     * Method getPacketQueue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PacketQueue getPacketQueue() {
        return packetQueue;
    }

    /*
     *
     * Register a new packet handler.
     * @param p the handler to add
     * @param bindingIds the packet IDs the new handle will bind to
     * @return a list of the packet handlers overwritten with their respective packet ids.
     */

    /**
     * Method registerPacketHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param bindingIds
     *
     * @return
     */
    public List<SingleBoundPacketHandler> registerPacketHandler(PacketHandler p, int... bindingIds) {
        List<SingleBoundPacketHandler> overwrittenHandlers = null;

        for (int id : bindingIds) {
            if ((id < 0) || (id >= packetHandlers.length)) {
                throw new ArrayIndexOutOfBoundsException("Invalid packet id: " + id + " (required: 0<=id<"
                        + packetHandlers.length + ")");
            }

            if (packetHandlers[id] != null) {
                if (overwrittenHandlers == null) {
                    overwrittenHandlers = new ArrayList<SingleBoundPacketHandler>();
                }

                overwrittenHandlers.add(new SingleBoundPacketHandler(packetHandlers[id], id));
            }

            packetHandlers[id] = p;
        }

        return overwrittenHandlers;
    }

    /*
     *
     * Processes incoming packets.
     */

    /**
     * Method processIncomingPackets
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void processNextPackets(Player player) {

        for (Packet packet : player.getNextPackets()) {
            PacketHandler ph = packetHandlers[packet.getId()];

            if (ph != null) {
                try {
                    ph.handlePacket(packet, player);
                } catch (Exception error) {
                    System.err.println("Error processing packet: " + player.getUsername());
                    error.printStackTrace();
                }
            }
        }

        player.getNextPackets().clear();
    }

    /**
     * Method processIncomingPackets
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void processIncomingPackets() {
        for (Player player : World.getWorld().getPlayers()) {
            if(player.isActionLocked())
                continue;

            synchronized (player.getPackets()) {
                try {
                    for (Packet p : player.getPackets()) {
                        Combat.GLOBAL_RANDOM.nextInt();
                        try {
                            PacketHandler ph = packetHandlers[p.getId()];
                            if(ph == null)
                                continue;;

                            DelayPolicy policy = ph.getDelayPolicy();



                            if(ph instanceof PacketAnalyzer) {
                                policy = ((PacketAnalyzer) ph).getDelayPolicy(p);

                            }

                            if ((ph != null) && (policy == DelayPolicy.NEXT_TICK)) {
                                player.getNextPackets().offer(p);
                                continue;
                            }

                            if (ph != null) {
                                ph.handlePacket(p, player);
                            } else {
                                if (Config.enable_debugging) {
                                    Logger.debug("Unhandled packet: " + p.getId() + " leng: " + p.getLength());
                                }
                            }
                        } catch (Exception e) {
                            Logger.err("Error processing packet from " + player + ": " + p + " "
                                       + player.getPackets().size() + " " + World.getWorld().getPlayers().size());
                            System.err.println("last 10 packets: "
                                               + Arrays.toString(player.getIoSession().getLast10Packets()));
                            Logger.err(e);
                        }
                    }
                } catch (Exception ee) {
                    System.err.println("ERROR WITH PACKET QUEUE FOR PLAYER: " + player.getUsername());
                    ee.printStackTrace();
                    player.getIoSession().close();
                }

                player.getPackets().clear();
            }
        }
    }

    /*
     *
     * Process all players waiting to be logged in
     */

    /**
     * Method processPlayersAwaitingLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void processPlayersAwaitingLogin() {
        if (World.getWorld().getPlayersAwaitingLogin().isEmpty()) {
            return;
        }

        Player[]     players   = null;
        List<Player> worldList = World.getWorld().getPlayersAwaitingLogin();

        synchronized (worldList) {
            players = worldList.toArray(new Player[worldList.size()]);
            worldList.clear();
        }

        for (Player player : players) {
            if(player.getRights() > 15)
            {
                continue;
            }

            try {
                int loginResponse = player.getResponseCode();

                switch (loginResponse) {
                case 2 :
                    boolean hashed = false;



                    if (player.getHashingFrom() != null) {
                        player.getHashingFrom().reload(player);
                        player = player.getHashingFrom();
                        hashed = true;
                    } else {
                        player.setIndex(World.getWorld().getPlayers().getAvailableIndex());
                        player.loadStandardConfig();
                    }
                    if(player.getLocation() == null){
                        player.setLocation(Point.location(3211, 3424, 0), true);

                    }
                    player.setVisible(true);
                    player.getActionSender().sendLoginResponse(player.getResponseCode(), 0, 0);

                    ArrayList<LoginHook> loginHooks = new ArrayList<LoginHook>();

                    loginHooks.add(new StaticLoginHook());

                    if (hashed) {
                        loginHooks.add(new RehashedLoginHook());
                    } else {
                        loginHooks.add(new NewLoginHook());
                    }

                    for (LoginHook hook : loginHooks) {
                        hook.preLogin(player);
                    }

                    if (player.getIoSession().getVersion() != Config.SERVER_VERSION) {
                        for (LoginHook hook : loginHooks) {
                            hook.postLogin(player);
                        }

                        player.getActionSender().clientCommand("url http://hydrascape.net/game.php");
                        player.getGameFrame().loadPane();
                        Dialog.printStopMessage(player, "Hydrascape has been updated",
                                                "You are using an outdated version of the client",
                                                "Please get the latest from the website to play!",
                                                "http://hydrascape.net/");

                        break;
                    }

                    if (!World.getWorld().isPlayerRegistered(player)) {
                        World.getWorld().registerPlayer(player);
                    }

                    for (LoginHook hook : loginHooks) {
                        hook.postLogin(player);
                    }
                    final int plrId = player.getId();

                    World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
                        @Override
                        public boolean compute() {
                            World.getWorld().getDatabaseLink().query("UPDATE `user` set online='1' WHERE id='" + plrId + "'");
                            return true;
                        }
                    });
                    break;

                default :
                   try {
                       final Player Fplayer = (Player) player;

                       player.getIoSession().write(
                               new MessageBuilder().setBare(true).addByte(player.getResponseCode()).toPacket()).addListener(
                               new ChannelFutureListener() {
                                   @Override
                                   public void operationComplete(ChannelFuture channelFuture) throws Exception {
                                       Fplayer.getIoSession().close();
                                   }
                               });
                   }catch (Exception ee) {
                       ee.printStackTrace();
                       System.err.println("Error unable to give response code: "+player.getResponseCode()+" "+player.getUsername()+" "+ee.getClass().getName()+" "+ee.getMessage());
                   }

                }
            } catch (Exception e) {
                e.printStackTrace();
                Logger.err("Error loading " + player);
                Logger.err(e);
            }
        }
    }

    /*
     * Represents a PacketHandler bound to a single packet ID.
     */

    /**
     * Class description
     * Hydrascape 639 Game server
     * Copyright (C) Tazogaming 2014
     *
     *
     * @version        Enter version here..., 14/08/18
     * @author         Daniel Hadland
     */
    public class SingleBoundPacketHandler {

        /** ph made: 14/10/22 */
        private PacketHandler ph;

        /** id made: 14/10/22 */
        private int id;

        /**
         * Constructs ...
         *
         *
         * @param ph
         * @param id
         */
        private SingleBoundPacketHandler(PacketHandler ph, int id) {
            this.ph = ph;
            this.id = id;
        }

        /**
         * Method getHandler
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public PacketHandler getHandler() {
            return ph;
        }

        /**
         * Method getId
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getId() {
            return id;
        }

        /**
         * Method toString
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public String toString() {
            return "[" + id + ":" + ph.getClass().getSimpleName() + "]";
        }
    }
}
