package net.tazogaming.hydra.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.minigame.Targetting;
import net.tazogaming.hydra.game.skill.farming.patch.FarmingMap;
import net.tazogaming.hydra.game.ui.rsi.interfaces.tab.StatisticsInterface;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.io.message.PlayerMessage;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.packetbuilder.GNP;
import net.tazogaming.hydra.net.packetbuilder.GPI;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.test.performancetest.BotPlayer;
import net.tazogaming.hydra.util.*;
import net.tazogaming.hydra.util.collections.EntityList;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public final class Core {
    public static int currentTime = 0;

    /** core_tasks made: 14/08/18 */
    private static LinkedList<RecurringTickEvent> core_tasks = new LinkedList<RecurringTickEvent>();

    /** taskRequests made: 14/08/18 */
    private static ArrayList<RecurringTickEvent> taskRequests = new ArrayList<RecurringTickEvent>();

    /** currentTimeMillis made: 14/08/18 */
    private static long currentTimeMillis = System.currentTimeMillis();

    /**
     * Update player/npc appearances, game objects, items, wall objects, ping
     */
    public static final LinkedBlockingQueue<PlayerMessage> yellQueue      = new LinkedBlockingQueue<PlayerMessage>();
    public static final LinkedBlockingQueue<String>        globalMessages = new LinkedBlockingQueue<String>();

    /** players made: 14/08/18 */
    private EntityList<Player> players = World.getWorld().getPlayers();

    /** npcs made: 14/08/18 */
    private EntityList<NPC> npcs = World.getWorld().getNpcs();

    /** npcUpdate made: 14/08/18 */
    private GNP npcUpdate = new GNP();


    public static int error_test_timer = 0;
    /**
     * Constructs ...
     *
     */
    public Core() {
        World.getWorld().setClientUpdater(this);
    }

    /**
     * Method currentTimeMillis
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static long currentTimeMillis() {
        return currentTimeMillis;
    }

    /**
     * Method submitTask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public static void submitTask(RecurringTickEvent t) {
        taskRequests.add(t);
    }

    /**
     * Method doTasks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void doTasks() {
        for (Iterator<RecurringTickEvent> tasks = core_tasks.iterator(); tasks.hasNext(); ) {
            RecurringTickEvent tickable = tasks.next();

            if (tickable.terminate()) {
                tasks.remove();

                continue;
            }

            try {
                tickable.tick();
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        if (taskRequests.size() > 0) {
            core_tasks.addAll(taskRequests);
            taskRequests.clear();
        }
    }

    /**
     * Method timeSince
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     *
     * @return
     */
    public static int timeSince(int time) {
        return currentTime - time;
    }

    /**
     * Method timeUntil
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     *
     * @return
     */
    public static int timeUntil(int ticks) {
        return ticks - currentTime;
    }

    /**
     * Method timePassed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param seconds
     *
     * @return
     */
    public static boolean timePassed(int seconds) {
        int ticks = (int) (seconds * 0.6);

        return currentTime % ticks == 0;
    }

    /**
     * Method ticksPassed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param seconds
     *
     * @return
     */
    public static boolean ticksPassed(int seconds) {
        return currentTime % seconds == 0;
    }

    /**
     * Method getSecondsForTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     *
     * @return
     */
    public static int getSecondsForTicks(int ticks) {
        return (ticks * 600) / 1000;
    }

    /**
     * Method getTicksForSeconds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param seconds
     *
     * @return
     */
    public static int getTicksForSeconds(int seconds) {
        return (seconds * 1000) / 600;
    }

    /**
     * Method getTicksForMinutes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param minutes
     *
     * @return
     */
    public static int getTicksForMinutes(int minutes) {
        return getTicksForSeconds(minutes * 60);
    }

    /**
     * Method getTicksForHours
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hrs
     *
     * @return
     */
    public static int getTicksForHours(int hrs) {
        return (getTicksForMinutes(60)) * hrs;
    }

    /**
     * Method setTime
     * Created on 14/08/18
     * Sets the current time (in milliseconds)
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time the time in milliseconds
     */
    public static void setTime(long time) {
        currentTimeMillis = time;
    }

    /**
     * Update player/npc movements
     */
    public void majorUpdate() {

        // shuffle players Around
        Collections.shuffle(World.getWorld().getPlayers().indicies);
        /*
         * Update the current time(tick) updated every 600ms
         */
        currentTime++;

        if (currentTime == Integer.MAX_VALUE) {
            currentTime = 0;
        }

        doTasks();
        World.getWorld().doEvents();

        for (Player p : players) {
            try {

                if(p.getLocation() != null && p.getLocation().getRegionX() <= 0){
                    p.teleport(3222, 3222, 0);
                }

                p.getIoSession().resetOutputCounter();
                World.getWorld().getGameEngine().processNextPackets(p);

                /*
                 *
                 * Unregister from server if disconnected
                 */
                if (!p.isLoggedIn()) {
                    try {
                        p.on_unlink();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    World.getWorld().unregisterPlayer(p, true);

                    continue;
                }

                /*
                 * Removes a player if they've been logged out for a certain period of time
                 * Saves alot of time and makes the gameplay more easier for the player
                 */
                if (!p.isConnected()
                        && ((System.currentTimeMillis() - p.getDisconTime() > (Config.DISCONNECT_GIVE_UP * 1000))
                            || (p.getDisconTime() == 0)) && p.isLoggedIn() &&!p.isPreventUnlink()) {
                    if (!(p instanceof BotPlayer) && (p.getGameFrame().getDuel() == null)) {
                       try {
                           p.getGameFrame().closeAll();
                       }catch (Exception ere){
                       }


                        p.log("player unregistered (Timed out)");
                        p.setLoggedIn(false);
                    }

                    continue;
                }

                if ((p.getDisconTime() != 0) ||!p.isConnected()) {
                    if (p.canLogout() &&!(p instanceof BotPlayer)) {
                        p.setVisible(false);
                    } else {
                        p.setVisible(true);
                    }
                }

                if (p.getDisconTime() == 0) {
                    if (!p.getIoSession().isConnected()) {
                        p.setDisconTime(System.currentTimeMillis());

                        try {
                            p.getIoSession().close();
                        } catch (Exception er) {}

                        continue;
                    }
                }

                p.updatePathLocation();
                p.clearProjectiles();

                if (p.getMovementMask() != null) {
                    if (!p.getMovementMask().isActive()) {
                        p.teleport(p.getMovementMask().getNewX(), p.getMovementMask().getNewY(), p.getHeight(), true);
                        p.getMovementMask().getWakeupScript().release(Scope.WAIT_TYPE_MOVEMENT);
                        p.setMovementMask(null);
                        p.setMovementLock(false);
                    }
                }

                if ((p.getCutScene() != null) && p.getCutScene().pre_process(p)) {
                    continue;
                }

                if (!p.isMapAreaChanged() &&!p.getRoute().isIdle() &&!p.isBusy()) {
                    if (p.getRoute().updatePosition()) {}

                    if (p.isRunning() || (p.getAccount().getButtonConfig(Account.RUN_STATUS) == 1)) {
                        if (p.getRequest().isCloseToEntity()) {
                            p.getRoute().resetPath();
                        } else {
                            boolean updated = p.getRoute().updatePosition();

                            if (p.getAccount().getButtonConfig(Account.RUN_STATUS) == 1) {
                                if (updated) {
                                    p.removeEnergy();
                                }
                            }
                        }
                    }
                }

                p.updateFollowList();
                p.moveFollow();
                p.checkFollow();



                if(error_test_timer > 0) {
                    error_test_timer--;
                    throw new RuntimeException();
                }

            } catch (Exception ee) {
                ee.printStackTrace();
                System.err.println("[PLAYERS]Error processing update: " + p);

                if (!p.isTestPilot()) {
                    p.getIoSession().close();
                }

                try {
                    p.getGameFrame().closeAll();
                } catch (Exception eex) {}
                catch (StackOverflowError ER) {}

                if ((p.getGameFrame().getDuel() == null) && (p.getGameFrame().getTrade() == null)) {

                    p.setLoggedIn(false);
                }
            }
        }

        for(Player player: players)
            player.moveFollowList();

        for (NPC npc : npcs) {
            try {
                if ((npc.getCutScene() != null) && npc.getCutScene().process(npc)) {
                    continue;
                }

                if (npc.hasCombatScript()) {
                    npc.getCombatScript().tick();
                }

                npc.update_quest_controller();
                npc.updateChatting();

                /*
                 * reset data
                 */
                npc.clearProjectiles();

                /*
                 * Update NPC movement
                 */
                npc.updatePathLocation();

                /*
                 * Update current scripts
                 */
                if (npc.getControllerScript() != null) {
                    npc.getControllerScript().updateScripts();
                }

                npc.updateMovement();

                /*
                 * update curent controller.
                 */
                npc.updateController();

                /*
                 * Update combat
                 */
                npc.getCombatHandler().update();

                /*
                 * Update projectiles
                 */
                npc.updateProjectiles();
                npc.validatePlayersInRange();
            } catch (Exception ee) {
                System.err.println("Error updating NPC: " + npc.getId());
                ee.printStackTrace();
            }
        }

        /*
         *   Process and logic loop, this is where combat and other things should be handled
         *      The next loop is for player updating so its best to calculate all actions here, then send the update
         *      in the next loop
         */
        for (Player p : players) {
            try {
                if ((!p.getIoSession().isConnected() || (currentTimeMillis() - p.getLastPacket() > 15000))
                        && (p.getDisconTime() == 0)) {

                    if(currentTimeMillis() - p.getLastPacket() > 15000) {
                        System.err.println("Warning: "+p.getUsername()+" timed out ("+(currentTimeMillis() - p.getLastPacket()));
                    }
                    p.setDisconTime(currentTimeMillis());

                    try {
                        p.getIoSession().close();
                    } catch (Exception ee) {}
                }

                if (!p.isLoggedIn()) {
                    continue;
                }

                if ((p.getCutScene() != null) && p.getCutScene().process()) {
                    continue;
                }

                /*
                 * Update projectiles
                 */
                p.updateProjectiles();

                /*
                 * update combat here
                 */
                p.updateActions();

                /*
                 * Process combat and skills and such here.
                 */
                p.getRequest().update();

                if(p.getCombatAdapter().isInCombat() && p.getEquipment().getId(3) == 4153 && p.isSpecialHighlighted())
                {
                    if(p.getCombatAdapter().isWithinBoundry() && p.getCombatAdapter().isWithinDistanceToTarget()){
                        SpecialAttacks.doSpecial(p, p.getCombatAdapter().getFightingWith());
                    }
                }
                p.getCombatAdapter().update();

                Targetting.checkTargets(p);

            } catch (Exception ee) {
               System.err.println("Error updating player: "+p);
                ee.printStackTrace();


                if (!p.isTestPilot()) {
                    p.getIoSession().close();
                }
            }
        }

        for (NPC npc : npcs) {
            try {
                if (npc == null) {
                    continue;
                }

                npc.updateDamageQueue();
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        for (Player p : players) {
            try {
                p.updateDamageQueue();

                if (p.isMapAreaChanged() &&!p.is_con) {
                    p.getActionSender().sendMapArea();
                    p.setKnownRegion(p.getLocation().getRegionX(), p.getLocation().getRegionY());
                }

                p.populateEntitiesInView();
                p.validatedWatchedPlayers();
                p.validatedWatchedNpcs();
                p.validatedFloorItems();
                p.validatedWatchedObjects();
                p.updateWatchedPlayers();

                p.updateWatchedNpcs();
                p.updateFloorItems();
                p.findNewObjects();
                p.updateDoorsInView();
                p.getCombatAdapter().updateDelayTimer();
            } catch (Exception ee) {

                p.setLocation(Point.location(3222, 3222, 0));
                p.setDisconTime(0);
                p.getIoSession().close();
                p.setLoggedIn(false);
                ee.printStackTrace();
            }
        }

        for (Player p : players) {
            try {
                if (!p.isLoggedIn()) {
                    continue;
                }

                if (!p.isTestPilot()) {
                    p.getIoSession().write(new GPI().getPacket(p));
                    p.getIoSession().write(this.npcUpdate.getPacket(p));

                    if ((Core.currentTime % 10 == 0) || (p.getRights() >= Player.ADMINISTRATOR)) {
                        StatisticsInterface.rebuildStats(p);
                    }

                    p.getActionSender().sendGroundItems();
                    p.getActionSender().sendObjects();
                    p.getActionSender().updateDoors();
                    FarmingMap.updateForPlayer(p);
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        for (Player p : players) {
            try {
                p.getAppearance().setChanged(false);
                p.getAnimation().setChanged(false);
                p.setMapAreaChanged(false);
                p.getAppearance().setForceAppearanceRequired(false);
                p.resetMoved();
                p.setDidTeleport(false);

                if (p.getMovementMask() != null) {
                    p.getMovementMask().setRendered(true);
                }

                p.resetDamage();
                p.setLastChatMessage(null);
                p.updateHitRecords();
                p.resetMoved();
                p.getGraphic().reset();
                p.getWatchedPlayers().update();
                p.getWatchedNPCs().update();
                p.getAnimation().reset();
                p.getWatchedItems().update();
                p.getWatchedObjects().update();
                p.getTimers().resetFoodProtTime();
                p.setTextUpdate(null);
                p.hitDebug = "";
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        for (NPC npc : npcs) {
            npc.setDidTeleport(false);
            npc.clearProjectiles();
            npc.resetMoved();
            npc.getAnimation().setChanged(false);
            npc.getGraphic().reset();
            npc.resetDamage();
            npc.setDidTeleport(false);
            npc.updateHitRecords();
            npc.setTextUpdate(null);
        }

        // This loop resets player data
        ShopManager.resetShops();
        World.getWorld().getScriptManager().updateTasks();
    }

    /**
     * Method yell
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param username
     * @param rights
     * @param message
     */
    public static void yell(String username, int rights, int ironMode, String message, String title) {
        PlayerMessage yellMsG =     new PlayerMessage(PlayerMessage.TYPE_YELL_MESSAGE, message, username, null, rights);
        yellMsG.setTitle(title);
        yellMsG.setIronMode(ironMode);
        yellQueue.offer(yellMsG);
    }

    /**
     * Method minorUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void minorUpdate() {
        for (Player p : World.getWorld().getPlayers()) {
            if (!p.getAccount().hasVar("yelloff")) {
                for (PlayerMessage yellMessage : yellQueue) {
                    p.getActionSender().sendYellMessage(yellMessage.getPlayerRights(), yellMessage.getSender(), yellMessage.getTitle(),yellMessage.getIronMode(),
                            yellMessage.getMessageData_string());
                }
            }

            for (String str : globalMessages) {
                p.getActionSender().sendMessage(str);
            }

            if (Core.currentTime % 30 == 0) {    // update friends every 15 secs
                p.getFriendsList().updateFriends();
            }
        }

        yellQueue.clear();
        globalMessages.clear();

        for (int index = 0; index < World.getWorld().getFloorItems().size(); index++) {
            FloorItem i = World.getWorld().getFloorItems().get(index);

            i.update();

            if (i.isRemoved()) {
                World.getWorld().getFloorItems().remove(index);
                i.setLocation(null);
            }
        }
    }
}
