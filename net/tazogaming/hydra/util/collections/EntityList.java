package net.tazogaming.hydra.util.collections;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @param <T>
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class EntityList<T extends Entity> extends AbstractCollection<T> {
    private static final int
        DEFAULT_CAPACITY               = 2048,
        MIN_VALUE                      = 1;
    public ArrayList<Integer> indicies = new ArrayList<Integer>();
    public int                curIndex = MIN_VALUE;
    public Object[]           entities;
    public int                capacity;

    /**
     * Constructs ...
     *
     */
    public EntityList() {
        this(DEFAULT_CAPACITY);
    }

    /**
     * Constructs ...
     *
     *
     * @param capacity
     */
    public EntityList(int capacity) {
        entities      = new Object[capacity];
        this.capacity = capacity;
    }

    /**
     * Method shuffle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void shuffle() {
        Collections.shuffle(indicies);
    }

    /**
     * Method clear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void clear() {
        entities      = new Object[capacity];
        this.curIndex = 0;
        indicies.clear();
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     *
     * @return
     */
    public boolean add(T entity) {
        int i = getAvailableIndex();

        if (i == -1) {
            return false;
        }

        entities[i] = entity;
        indicies.add(i);
        entity.setIndex(i);

        return true;
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     */
    public void remove(T entity) {
        if (entities[entity.getIndex()] == null) {
            return;
        }

        entities[entity.getIndex()] = null;
        indicies.remove((Integer) entity.getIndex());
    }

    /**
     * Method getAvailableIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAvailableIndex() {
        for (int i = 1; i < capacity; i++) {
            if (entities[i] == null) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public T remove(int index) {
        Object temp = entities[index];

        entities[index] = null;
        indicies.remove(index);

        return (T) temp;
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public T get(int index) {
        return (T) entities[index];
    }

    /**
     * Method iterator
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Iterator<T> iterator() {
        return new EntityListIterator<T>(entities, indicies, this);
    }

    /**
     * Method increaseIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void increaseIndex() {
        curIndex++;

        if (curIndex >= capacity) {
            curIndex = MIN_VALUE;
        }
    }

    /**
     * Method contains
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     *
     * @return
     */
    public boolean contains(T entity) {
        return indexOf(entity) > -1;
    }

    /**
     * Method indexOf
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     *
     * @return
     */
    public int indexOf(T entity) {
        for (int index : indicies) {
            if (entities[index].equals(entity)) {
                return index;
            }
        }

        return -1;
    }

    /**
     * Method count
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int count() {
        return indicies.size();
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return indicies.size();
    }
}
