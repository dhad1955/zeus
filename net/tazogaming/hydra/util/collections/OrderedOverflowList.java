package net.tazogaming.hydra.util.collections;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/05/14
 * Time: 13:39
 *
 * @param <T>
 */
public class OrderedOverflowList<T> {
    private int      capacity = 0;
    private int      offset   = 0;
    private Object[] objects;

    /**
     * Constructs ...
     *
     *
     * @param capacity
     */
    public OrderedOverflowList(int capacity) {
        this.capacity = capacity;
        this.objects  = new Object[capacity];
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param element
     */
    public void add(T element) {}

    private void shiftElements(int spaces) {
        Object[] tmp = new Object[capacity];
    }
}
