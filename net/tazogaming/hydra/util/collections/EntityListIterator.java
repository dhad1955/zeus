package net.tazogaming.hydra.util.collections;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @param <E>
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class EntityListIterator<E extends Entity> implements Iterator<E> {
    private int        curIndex = 0;
    private Integer[]  indicies;
    private Object[]   entities;
    private EntityList entityList;

    /**
     * Constructs ...
     *
     *
     * @param entities
     * @param indicies
     * @param entityList
     */
    public EntityListIterator(Object[] entities, List<Integer> indicies, EntityList entityList) {
        this.entities   = entities;
        this.indicies   = indicies.toArray(new Integer[0]);
        this.entityList = entityList;
    }

    /**
     * Method shuffleArray
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param array
     */
    public static void shuffleArray(Integer[] array) {
        Logger.log("### shuffle start####");

        for (int i = 0; i < array.length; i++) {
            Logger.log("pid: " + array[i]);
        }

        int    index;
        Random random = new Random();

        for (int i = array.length - 1; i > 0; i--) {
            index = random.nextInt(i + 1);

            if (index != i) {
                array[index] ^= array[i];
                array[i]     ^= array[index];
                array[index] ^= array[i];
            }
        }

        Logger.log("### shuffle finished ###");

        for (int i = 0; i < array.length; i++) {
            Logger.log("pid: " + array[i]);
        }
    }

    /**
     * Method hasNext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasNext() {
        return indicies.length != curIndex;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public E next() {
        Object temp = entities[indicies[curIndex]];

        curIndex++;

        return (E) temp;
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void remove() {
        if (curIndex >= 1) {
            entityList.remove(indicies[curIndex - 1]);
        }
    }
}
