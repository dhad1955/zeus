package net.tazogaming.hydra.util.collections;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;

/**
 * This class is a collection which is backed by 3 seperate Lists.
 *
 * These Lists control the state of this collection.
 *
 * To update this collections current state,
 * you need to explicity call the update method.
 *
 * The purpose of this collection is to seperate new values added to
 * this collection untill the update method has been called. Removal
 * of entities will NOT take effect until the update method is called.
 * This is so we can see what is being removed (and in cases this is
 * required by the server) to handle them specially.
 *
 *
 * @param <T>
 */
public class StatefulEntityCollection<T extends Entity> {
    private Collection<T> newEntities      = new HashSet<T>();
    private Collection<T> entitiesToRemove = new HashSet<T>();
    private Collection<T> refreshing       = new HashSet<T>();
    private Collection<T> knownEntities;

    /**
     * Constructs ...
     *
     */
    public StatefulEntityCollection() {
        this(true);
    }

    /**
     * Constructs ...
     *
     *
     * @param retainOrder
     */
    public StatefulEntityCollection(boolean retainOrder) {

        // knownEntities = retainOrder ? new ArrayList<T>() : new HashSet<T>();
        knownEntities = new LinkedHashSet();
    }

    /**
     * Method clear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void clear() {
        refreshing.clear();
        entitiesToRemove.clear();
        refreshing.clear();
        newEntities.clear();
        knownEntities.clear();
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return knownEntities.size();
    }

    // We need to keep these in the order they logged in, currently it doesn't seem to?

    /**
     * Method isRefreshing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     *
     * @return
     */
    public boolean isRefreshing(Entity e) {
        return refreshing.contains(e);
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     */
    public void add(T entity) {
        newEntities.add(entity);
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entities
     */
    public void add(Collection<T> entities) {
        newEntities.addAll(entities);
    }

    /**
     * Method contains
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     *
     * @return
     */
    public boolean contains(T entity) {
        return newEntities.contains(entity) || knownEntities.contains(entity);
    }

    /**
     * Method isKnown
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     *
     * @return
     */
    public boolean isKnown(T entity) {
        return knownEntities.contains(entity) &&!entitiesToRemove.contains(entity);
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     */
    public void remove(T entity) {
        entitiesToRemove.add(entity);
    }

    /**
     * Method isRemoving
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     *
     * @return
     */
    public boolean isRemoving(T entity) {
        return entitiesToRemove.contains(entity);
    }

    /**
     * Method isAdding
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     *
     * @return
     */
    public boolean isAdding(T entity) {
        return newEntities.contains(entity);
    }

    /**
     * Method refresh
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void refresh(T pla) {
        refreshing.add(pla);
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        knownEntities.removeAll(entitiesToRemove);
        knownEntities.addAll(newEntities);
        newEntities.clear();
        refreshing.clear();
        entitiesToRemove.clear();
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     *
     * @return
     */
    public String toString(Collection<T> l) {
        StringBuilder out = new StringBuilder();
        Iterator<T>   it  = l.iterator();

        while (it.hasNext()) {
            out.append(((Entity) (it.next())).getIndex());

            if (it.hasNext()) {
                out.append(",");
            }
        }

        return out.toString();
    }

    /**
     * Method changed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean changed() {
        return !entitiesToRemove.isEmpty() ||!newEntities.isEmpty();
    }

    /**
     * Method getRemovingEntities
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Collection<T> getRemovingEntities() {
        return entitiesToRemove;
    }

    /**
     * Method getNewEntities
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Collection<T> getNewEntities() {
        return newEntities;
    }

    /**
     * Method getKnownEntities
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Collection<T> getKnownEntities() {
        return knownEntities;
    }

    /**
     * Method getAllEntities
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Collection<T> getAllEntities() {
        List<T> temp = new ArrayList<T>();

        temp.addAll(newEntities);
        temp.addAll(knownEntities);

        return temp;
    }
}
