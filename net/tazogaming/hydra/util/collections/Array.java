package net.tazogaming.hydra.util.collections;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/11/13
 * Time: 03:43
 */
public class Array {
    private ArrayList<Object> dynamic_array = new ArrayList<Object>();
    private boolean           save          = false;

    /**
     * Constructs ...
     *
     *
     * @param siz
     * @param save
     */
    public Array(int siz, boolean save) {
        dynamic_array = new ArrayList<Object>(siz);
        this.save     = save;
    }

    /**
     * Method iterator
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Iterator<Object> iterator() {
        return dynamic_array.iterator();
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean save() {
        return save;
    }

    /**
     * Method contains
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param o
     *
     * @return
     */
    public boolean contains(Object o) {
        return dynamic_array.contains(o);
    }

    /**
     * Method removeIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     */
    public void removeIndex(int index) {
        dynamic_array.remove(index);
    }

    /**
     * Method index_of
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param o
     *
     * @return
     */
    public int index_of(Object o) {
        return dynamic_array.indexOf(o);
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param o
     *
     * @return
     */
    public boolean remove(Object o) {
        return dynamic_array.remove(o);
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return dynamic_array.size();
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param o
     */
    public void add(Object o) {
        dynamic_array.add(o);
    }

    /**
     * Method clear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void clear() {
        dynamic_array.clear();
    }
}
