package net.tazogaming.hydra.util.background;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.Server;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 03:19
 */
public class BackgroundService extends Thread {

    /** new_requests made: 14/09/14 **/
    private final LinkedList<BackgroundServiceRequest> new_requests = new LinkedList<BackgroundServiceRequest>();

    /** requests made: 14/09/14 **/
    private LinkedList<BackgroundServiceRequest> requests = new LinkedList<BackgroundServiceRequest>();

    /**
     * Constructs ...
     *
     */
    public BackgroundService() {
        Server.newThread(this, "On-demandservice", Thread.MIN_PRIORITY);
        this.setPriority(Thread.MIN_PRIORITY);
    }

    /**
     * Method submitRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rq
     */
    public void submitRequest(BackgroundServiceRequest rq) {
        synchronized (new_requests) {
            new_requests.add(rq);

            synchronized (this) {
                this.notify();
            }
        }
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void run() {
        Logger.log("[ONDEMAND] running");

        while (true) {
            while (requests.isEmpty() && new_requests.isEmpty()) {
                try {
                    synchronized (this) {
                        this.wait();
                    }
                } catch (InterruptedException ignored) {}
            }

            synchronized (new_requests) {
                if (new_requests.size() > 0) {
                    requests.addAll(new_requests);
                }

                new_requests.clear();
            }

            for (Iterator<BackgroundServiceRequest> requestIterator =
                    requests.iterator(); requestIterator.hasNext(); ) {
                BackgroundServiceRequest current_request = requestIterator.next();
              try {
                if (current_request.exec()) {
                    if (current_request.getTargetListener() != null) {
                        current_request.getTargetListener().requestComplete(current_request.getResult());
                    }
                    requestIterator.remove();
                }
              }catch (Exception ee) {
                  ee.printStackTrace();
              }

            }

            try {
                Thread.sleep(80);
            } catch (Exception ee) {}
        }
    }
}
