package net.tazogaming.hydra.util.background;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.entity3d.Route;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 03:04
 */
public class PathFindRequest extends BackgroundServiceRequest {
    private boolean ignoreNpcs = false;
    private Point   location;
    private Mob     mob;

    /**
     * Constructs ...
     *
     *
     * @param l
     * @param m
     * @param destination
     * @param ignore
     */
    public PathFindRequest(BackgroundTaskCompleteListener l, Mob m, Point destination, boolean ignore) {
        this.mob = m;
        setRequest(l);
        this.location   = destination;
        this.ignoreNpcs = ignore;
    }

    /**
     * Method npcOnTile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mob
     * @param b
     * @param x
     * @param y
     *
     * @return
     */
    public boolean npcOnTile(Mob mob, boolean b, int x, int y) {
        try {
            if (b && World.getWorld().getTile(x, y, mob.getHeight()).blocked((NPC) mob)) {
                return true;
            }
        } catch (Exception ee) {
            Logger.log("error: " + x + " " + y);
            ee.printStackTrace();
        }

        return false;
    }

    /**
     * Method findRoute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mob
     * @param handler
     * @param ignoreNpcs
     * @param destX
     * @param destY
     *
     * @return
     */
    public Point findRoute(Mob mob, Route handler, boolean ignoreNpcs, int destX, int destY) {
        int h             = mob.getHeight();
        int destLocationX = destX;
        int destLocationY = destY;

        destX = destX - 8 * mob.getLocation().getRegionX();
        destY = destY - 8 * mob.getLocation().getRegionY();

        int[][]             via        = new int[104][104];
        int[][]             cost       = new int[104][104];
        LinkedList<Integer> tileQueueX = new LinkedList<Integer>();
        LinkedList<Integer> tileQueueY = new LinkedList<Integer>();

        for (int xx = 0; xx < 104; xx++) {
            for (int yy = 0; yy < 104; yy++) {
                cost[xx][yy] = 99999999;
            }
        }

        int curX = mob.getLocation().getLocalX();
        int curY = mob.getLocation().getLocalY();

        via[curX][curY]  = 99;
        cost[curX][curY] = 0;

        int tail = 0;

        tileQueueX.add(curX);
        tileQueueY.add(curY);
        ignoreNpcs = true;

        int     pathLength  = 4000;
        boolean foundPath   = false;
        int     maxAttempts = 2000;
        int     count       = 0;

        while ((tail != tileQueueX.size()) && (tileQueueX.size() < pathLength)) {
            if (++count > maxAttempts) {
                break;
            }

            curX = tileQueueX.get(tail);
            curY = tileQueueY.get(tail);

            int curAbsX = mob.getLocation().getRegionX() * 8 + curX;
            int curAbsY = mob.getLocation().getRegionY() * 8 + curY;

            if ((curX == destX) && (curY == destY)) {
                foundPath = true;

                break;
            }

            tail = (tail + 1) % pathLength;

            int thisCost = cost[curX][curY] + 1;

            if (!npcOnTile(mob, ignoreNpcs, curAbsX, curAbsY - 1) && (curY > 0) && (via[curX][curY - 1] == 0)
                    && (!ClippingDecoder.blockedSouth(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX);
                tileQueueY.add(curY - 1);
                via[curX][curY - 1]  = 1;
                cost[curX][curY - 1] = thisCost;
            }

            if (!npcOnTile(mob, ignoreNpcs, curAbsX - 1, curAbsY) && (curX > 0) && (via[curX - 1][curY] == 0)
                    && (!ClippingDecoder.blockedWest(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY);
                via[curX - 1][curY]  = 2;
                cost[curX - 1][curY] = thisCost;
            }

            if (!npcOnTile(mob, ignoreNpcs, curAbsX, curAbsY + 1) && (curY < 104 - 1) && (via[curX][curY + 1] == 0)
                    && (!ClippingDecoder.blockedNorth(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX);
                tileQueueY.add(curY + 1);
                via[curX][curY + 1]  = 4;
                cost[curX][curY + 1] = thisCost;
            }

            if (!npcOnTile(mob, ignoreNpcs, curAbsX + 1, curAbsY) && (curX < 104 - 1) && (via[curX + 1][curY] == 0)
                    && (!ClippingDecoder.blockedEast(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY);
                via[curX + 1][curY]  = 8;
                cost[curX + 1][curY] = thisCost;
            }

            if (!npcOnTile(mob, ignoreNpcs, curAbsX - 1, curAbsY - 1) && (curX > 0) && (curY > 0)
                    && (via[curX - 1][curY - 1] == 0) && (!ClippingDecoder.blockedSouthWest(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY - 1);
                via[curX - 1][curY - 1]  = 3;
                cost[curX - 1][curY - 1] = thisCost;
            }

            if (!npcOnTile(mob, ignoreNpcs, curAbsX - 1, curAbsY + 1) && (curX > 0) && (curY < 104 - 1)
                    && (via[curX - 1][curY + 1] == 0) && (!ClippingDecoder.blockedNorthWest(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY + 1);
                via[curX - 1][curY + 1]  = 6;
                cost[curX - 1][curY + 1] = thisCost;
            }

            if (!npcOnTile(mob, ignoreNpcs, curAbsX + 1, curAbsY - 1) && (curX < 104 - 1) && (curY > 0)
                    && (via[curX + 1][curY - 1] == 0) && (!ClippingDecoder.blockedSouthEast(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY - 1);
                via[curX + 1][curY - 1]  = 9;
                cost[curX + 1][curY - 1] = thisCost;
            }

            if (!npcOnTile(mob, ignoreNpcs, curAbsX + 1, curAbsY + 1) && (curX < 104 - 1) && (curY < 104 - 1)
                    && (via[curX + 1][curY + 1] == 0) && (!ClippingDecoder.blockedNorthEast(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY + 1);
                via[curX + 1][curY + 1]  = 12;
                cost[curX + 1][curY + 1] = thisCost;
            }
        }

        if (!foundPath) {
            NPC n        = (NPC) mob;
            int i_223_   = 1000;
            int thisCost = 100;
            int i_225_   = 10;

            for (int x = destX - i_225_; x <= destX + i_225_; x++) {
                for (int y = destY - i_225_; y <= destY + i_225_; y++) {
                    if ((x >= 0) && (y >= 0) && (x < 104) && (y < 104) && (cost[x][y] < 100)) {
                        int i_228_ = 0;

                        if (x < destX) {
                            i_228_ = destX - x;
                        } else if (x > destX + 1 - 1) {
                            i_228_ = x - (destX + 1 - 1);
                        }

                        int i_229_ = 0;

                        if (y < destY) {
                            i_229_ = destY - y;
                        } else if (y > destY + 1 - 1) {
                            i_229_ = y - (destY + 1 - 1);
                        }

                        int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;

                        if ((i_230_ < i_223_) || ((i_230_ == i_223_) && (cost[x][y] < thisCost))) {
                            i_223_   = i_230_;
                            thisCost = cost[x][y];
                            curX     = x;
                            curY     = y;
                        }
                    }
                }
            }

            if (i_223_ == 1000) {
                return null;
            }
        }

        tail = 0;
        tileQueueX.set(tail, curX);
        tileQueueY.set(tail++, curY);

        int l5;

        for (int j5 = l5 = via[curX][curY];
                (curX != mob.getLocation().getLocalX()) || (curY != mob.getLocation().getLocalY());
                j5 = via[curX][curY]) {
            if (j5 != l5) {
                l5 = j5;
                tileQueueX.set(tail, curX);
                tileQueueY.set(tail++, curY);
            }

            if ((j5 & 2) != 0) {
                curX++;
            } else if ((j5 & 8) != 0) {
                curX--;
            }

            if ((j5 & 1) != 0) {
                curY++;
            } else if ((j5 & 4) != 0) {
                curY--;
            }
        }

        int   size = tail--;
        Point dest = (Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), mob.getLocation().getRegionX(),
                                mob.getLocation().getRegionY(), h));

        handler.addPoint(dest);

        for (int i = 1; i < size; i++) {
            tail--;

            Point p2 = Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), mob.getLocation().getRegionX(),
                                 mob.getLocation().getRegionY(), h);

            handler.addPoint(p2);
        }

        return dest;
    }

    /**
     * Method compute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean compute() {
        Route p = new Route(mob);

        if (location == null) {
            setResult(p);

            return true;
        }

        findRoute(mob, p, ignoreNpcs, location.getX(), location.getY());
        this.setResult(p);

        // To change body of implemented methods use File | Settings | File Templates.
        return true;
    }
}
