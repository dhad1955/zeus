package net.tazogaming.hydra.util.background;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 03:16
 */
public abstract class BackgroundServiceRequest {
    protected Object                       result;
    private BackgroundTaskCompleteListener target;
    private Object                         param;

    /**
     * Constructs ...
     *
     */
    public BackgroundServiceRequest() {
        this(null);
    }

    /**
     * Constructs ...
     *
     *
     * @param param
     */
    public BackgroundServiceRequest(Object param) {
        this.param = param;
    }

    /**
     * Method getParam
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object getParam() {
        return param;
    }

    /**
     * Method exec
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean exec() {
        return compute();
    }

    protected void setRequest(BackgroundTaskCompleteListener l) {
        this.target = l;
    }

    /**
     * Method getTargetListener
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public BackgroundTaskCompleteListener getTargetListener() {
        return target;
    }

    /**
     * Method getResult
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object getResult() {
        return result;
    }

    protected void setResult(Object result) {
        this.result = result;
    }

    /**
     * Method compute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract boolean compute();
}
