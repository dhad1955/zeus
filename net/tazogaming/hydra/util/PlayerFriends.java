package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;

//~--- JDK imports ------------------------------------------------------------

import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/10/13
 * Time: 19:45
 */
public class PlayerFriends {
    private boolean                statusSent          = false;
    private final LinkedList<Long> knownOfflineFriends = new LinkedList<Long>();
    private LinkedList<Long>       knownOnlineFriends  = new LinkedList<Long>();
    private Player                 player;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    public PlayerFriends(Player player) {
        this.player = player;
    }

    /**
     * Method addNew
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void addNew(long name) {
        synchronized (knownOfflineFriends) {
            knownOfflineFriends.add(name);
        }
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        try {
            if (!statusSent) {
                synchronized (player.getIoSession()) {

                    // player.getIoSession().write(new MessageBuilder().setId(221).addByte((byte) 2).toPacket());
                }

                statusSent = true;
            }

            for (Long l : player.getPlayerFriends()) {
                Player there_friend = World.getWorld().getPlayer(l);

                if (((there_friend == null) ||!there_friend.isLoggedIn()) || (there_friend.getPrivacySetting(1) == 2)
                        || ((there_friend.getPrivacySetting(1) == 1)
                            &&!there_friend.getPlayerFriends().contains(this.player.getUsernameHash()))) {
                    if (!knownOfflineFriends.contains(l)) {
                        synchronized (player.getIoSession()) {
                            player.getActionSender().loadpm(0, l, 0);
                            knownOfflineFriends.add(l);

                            if (knownOnlineFriends.contains(l)) {
                                knownOnlineFriends.remove(l);
                            }
                        }
                    }
                } else {
                    if (!knownOnlineFriends.contains(l)) {
                        player.getActionSender().loadpm(1, l, 0);
                        knownOnlineFriends.add(l);

                        if (knownOfflineFriends.contains(l)) {
                            knownOfflineFriends.remove(l);
                        }
                    }
                }
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }
}
