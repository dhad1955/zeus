package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 00:08
 */
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.runnable.ScriptRunnable;

//~--- JDK imports ------------------------------------------------------------

import java.util.NoSuchElementException;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class ArgumentTokenizer {
    private int      offset = 0;
    private int      leng;
    private Object[] args;

    /**
     * Constructs ...
     *
     *
     * @param args
     */
    public ArgumentTokenizer(Object... args) {
        this.args = args;
        leng      = args.length;

        for (Object obj : args) {
            if (obj instanceof ArgumentTokenizer) {
                throw new RuntimeException();
            }
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param data
     * @param regex
     * @param dataLength
     */
    public ArgumentTokenizer(String data, String regex, int dataLength) {
        this.args = data.split(regex, dataLength);
        leng      = dataLength;
    }

    /**
     * Method remaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int remaining() {
        return leng - offset;
    }

    /**
     * Method getVariableForId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param pla
     *
     * @return
     */
    public String getVariableForId(int id, Player pla) {
        switch (id) {
        case RuntimeInstruction.NAME :
            return pla.getUsername();

        case RuntimeInstruction.SLAYCOUNT :
        }

        return "Unkown Variable";
    }

    /**
     * Method hasNext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasNext() {
        return offset < leng;
    }

    /**
     * Method replace
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param pla
     * @param id
     * @param offset
     *
     * @return
     */
    public String replace(String str, Player pla, int id, int offset) {
        StringBuffer si = new StringBuffer(str);

        si.insert(offset, getVariableForId(id, pla));

        return si.toString();
    }

    /**
     * Method check
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pr
     * @param c
     * @param exec
     */
    public void check(Player pr, RuntimeInstruction c, ScriptRunnable exec) {
        for (int i = 0; i < leng; i++) {
            if (args[i] instanceof String) {
                String str = (String) args[i];

                if (c.idOffsetReplace[i] > -1) {

                    // str =  ScriptRuntime.replaceString(str, pr, exec);
                    this.args[i] = str;
                }
            }
        }
    }

    /**
     * Returns Object array
     * @return Object[] The objects in the parser
     */
    public Object[] getObjects() {
        return args;
    }

    /**
     * Method caret
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int caret() {
        return this.offset;
    }

    /**
     * Method remainingObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object[] remainingObjects() {
        Object[] c = new Object[getObjects().length - offset];

        System.arraycopy(remainingObjects(), offset, c, 0, getObjects().length - offset);

        return c;
    }

    /**
     * Method nextChar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public char nextChar() {
        return nextString().charAt(0);
    }

    /**
     * Returns 32 bit (DWord)
     * @return  int
     */
    public int nextInt() {
        Object obj = next();

        return (int) Double.parseDouble(obj.toString());
    }

    /**
     * Returns next argument as float
     * @return float
     */
    public float nextFloat() {
        return Float.parseFloat(next().toString());
    }

    /**
     * Returns next argument as short
     * @return short
     */
    public short nextShort() {
        Object obj = next();

        return Short.parseShort(obj.toString());
    }

    /**
     * Returns next argument as byte
     * @return byte
     */
    public byte nextByte() {
        Object obj = next();

        return Byte.parseByte(obj.toString());
    }

    /**
     * Returns next argument as string
     * @return  string
     */
    public String nextString() {
        Object obj = next();

        if (obj == null) {
            return null;
        }

        return obj.toString();
    }

    /**
     * Returns next argument as double
     * @return double
     */
    public double nextDouble() {
        Object obj = next();

        return Double.parseDouble(obj.toString());
    }

    /**
     * Returns next argument as Q Word (long)
     * @return  long
     */
    public long nextLong() {
        Object obj = next();

        return Long.parseLong(obj.toString());
    }

    /**
     * Returns next argument
     * @return Next argument as object
     */
    public Object next() {
        if (offset == leng) {
            throw new NoSuchElementException("No more elements");
        }

        return args[offset++];
    }

    /**
     * Method length
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        return leng;
    }

    /**
     * Method getRemainingParameters
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getRemainingParameters() {
        if (args == null) {
            return null;
        }

        String[] ret = new String[leng - offset];

        for (int i = 0; hasNext(); i++) {
            ret[i] = nextString();
        }

        return ret;
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("Offset: ").append(offset);
        sb.append("leng:").append(args.length);

        for (int i = 0; i < args.length; i++) {
            sb.append("'" + args[i] + "' ");
        }

        return sb.toString();
    }
}


/**
 * Rune War Game Server
 * http://hydra.tazogaming.org
 * Builder {$build}
 * net.tazo_.server.script.condition
 *
 * @author Gander
 *         Date: 25-Sep-2008 {10:12:55}
 */
class ArrayResult {
    int    id;
    String match;
}
