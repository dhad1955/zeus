package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.runtime.Core;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import java.text.DecimalFormat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * A class to handle logging events
 */
public class Logger {
    private static final DecimalFormat      df2        = new DecimalFormat("00");
    private static Map<String, PrintStream> outStreams = new HashMap<String, PrintStream>();
    private static PrintStream              out, err;
    private static long                     START_TIME;

    static {

        /*
         *    try {
         * //      out = newStream("out.log");
         *  //    err = newStream("err.log");
         *  } catch (IOException ieo) {
         *      ieo.printStackTrace();
         *      System.exit(1);
         *  }
         */
    }

    static {
        START_TIME = System.currentTimeMillis();
    }

    /**
     * Method printStackTrace
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    public static void printStackTrace(Exception ee) {
        StackTraceElement[] elements = ee.getStackTrace();

        Logger.err(elements[0].getClassName() + "(" + elements[0].getMethodName() + ") " + elements[0].getLineNumber());
    }

    /**
     * Method debug
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public static void debug(String str) {
        System.err.println(time() + "[DEBUG]: " + str);
    }

    private static PrintStream newStream(String file) throws IOException {
        PrintStream p = new PrintStream(new BufferedOutputStream(new FileOutputStream(new File("logs/" + file)), 1024));

        outStreams.put(file, p);

        return p;
    }

    private static String time() {
        return timeSince(START_TIME) + " cycle: " + Core.currentTime;
    }

    private static String prefix() {
        return "[" + time() + "] [" + Thread.currentThread().getName() + "] ";
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void close() {
        Iterator<Map.Entry<String, PrintStream>> it = outStreams.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry<String, PrintStream> entry  = it.next();
            PrintStream                    stream = entry.getValue();

            stream.println("Closing stream..");
            stream.flush();
            stream.close();
        }
    }

    /**
     * Method flush
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     */
    public static void flush(String f) {
        PrintStream tmpStream = outStreams.get(f);

        if (tmpStream == null) {
            Logger.err("Failed to flush " + f + " (No such stream)");
        } else {
            tmpStream.flush();
        }
    }

    /**
     * Method log
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param o
     */
    public static synchronized void log(Object o) {
        if (!Config.TEST_MODE) {
            return;
        }

        String s = prefix() + o.toString();

//      out.println(s);
        System.out.println(s);
    }

    /**
     * Method log2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     */
    public static synchronized void log2(String f) {
        System.out.print(prefix() + f);
    }

    /**
     * Method log
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     * @param o
     */
    public static synchronized void log(String f, Object o) {
        String s = prefix() + o.toString();

        try {
            PrintStream tmpStream = outStreams.get(f);

            if (tmpStream == null) {
                tmpStream = newStream(f);
            }

            tmpStream.println(s);
        } catch (IOException ex) {
            Logger.err("Failed to log to " + f + ", msg='" + s + "'");
        }
    }

    /**
     * Method dbg
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param values
     */
    public static synchronized void dbg(Object... values) {
        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < values.length; i++) {
            builder.append(values[i].toString());
            builder.append(":");
            i++;

            if (i >= values.length) {
                break;
            }

            builder.append(" " + values[i]);

            if (i + 1 != values.length) {
                builder.append(", ");
            }
        }

        err(builder.toString());
    }

    /**
     * Method err
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public static synchronized void err(Throwable e) {
        e.printStackTrace();

        String s = prefix() + e.toString();

        // err.println(s);
        Logger.debug(s);

        for (StackTraceElement ste : e.getStackTrace()) {
            String s1 = prefix() + "\t" + ste.toString();

//          err.println(s1);
            Logger.debug(s1);
        }
    }

    /**
     * Method err
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s1
     */
    public static synchronized void err(String s1) {
        String s = prefix() + s1;

        Logger.debug(s);
    }

    /**
     * Method timeSince
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     *
     * @return
     */
    public static final String timeSince(long time) {
        int    seconds = (int) ((System.currentTimeMillis() - time) / 1000);
        int    minutes = (int) (seconds / 60);
        int    hours   = (int) (minutes / 60);
        int    days    = (int) (hours / 24);
        String dayStr  = "";

        if (days > 0) {
            dayStr = days + " days, ";
        }

        String s = null;

        synchronized (df2) {
            s = dayStr + df2.format(hours % 24) + ":" + df2.format(minutes % 60) + ":" + df2.format(seconds % 60);
        }

        return s;
    }
}
