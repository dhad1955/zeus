package net.tazogaming.hydra.util;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/07/14
 * Time: 18:25
 */
public class WorldDef {
    private final String activity;
    private final int    country;
    private final int    flag;
    private final String ip;
    private final int    location;
    private final String region;
    private final int    worldId;

    /**
     * Constructs ...
     *
     *
     * @param worldId
     * @param location
     * @param flag
     * @param activity
     * @param ip
     * @param region
     * @param country
     */
    public WorldDef(int worldId, int location, int flag, String activity, String ip, String region, int country) {
        this.worldId  = worldId;
        this.location = location;
        this.flag     = flag;
        this.activity = activity;
        this.ip       = ip;
        this.region   = region;
        this.country  = country;
    }

    /**
     * Method getActivity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getActivity() {
        return activity;
    }

    /**
     * Method getCountry
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCountry() {
        return country;
    }

    /**
     * Method getFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFlag() {
        return flag;
    }

    /**
     * Method getIp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getIp() {
        return ip;
    }

    /**
     * Method getLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLocation() {
        return location;
    }

    /**
     * Method getRegion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getRegion() {
        return region;
    }

    /**
     * Method getWorldId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWorldId() {
        return worldId;
    }
}
