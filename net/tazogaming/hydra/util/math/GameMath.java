package net.tazogaming.hydra.util.math;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.AttackStyle;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.util.Config;

//~--- JDK imports ------------------------------------------------------------

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 22:30
 * To change this template use File | Settings | File Templates.
 */
public class GameMath {
    private static Random random = new Random();

    /**
     * Method combatLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param attLevel
     * @param defLevel
     * @param strLevel
     * @param hpLevel
     * @param prayLevel
     * @param magLevel
     * @param ranLevel
     * @param sumLevel
     *
     * @return
     */
    public static double combatLevel(int attLevel, int defLevel, int strLevel, int hpLevel, int prayLevel,
                                     int magLevel, int ranLevel, int sumLevel) {
        sumLevel = 1;


        double baseLevel   = (defLevel + hpLevel + Math.floor(prayLevel / 2) + Math.floor(sumLevel / 2)) * 0.25;
        double meleeLevel  = (attLevel + strLevel) * 0.325;
        double rangerLevel = Math.floor(ranLevel * 1.5) * 0.325;
        double mageLevel   = Math.floor(magLevel * 1.5) * 0.325;

        return baseLevel + Math.max(meleeLevel, Math.max(rangerLevel, mageLevel));
    }

    /**
     * Method formatMoney
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param j
     *
     * @return
     */
    public static final String formatMoney(int j) {
        if ((j >= 0) && (j < 10000)) {
            return String.valueOf(j);
        }

        if ((j >= 10000) && (j < 10000000)) {
            return j / 1000 + "K";
        }

        if ((j >= 10000000) && (j < 999999999)) {
            return j / 1000000 + "M";
        }

        if (j >= 999999999) {
            return "*";
        } else {
            return "?";
        }
    }

    /**
     * Method rand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public static int rand(int i) {
        return (int) (Math.random() * (i + 1));
    }

    /**
     * Method rand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     *
     * @return
     */
    public static int rand(double d) {
        double x = Math.random() * (d + 1);

        return (int) x;
    }

    /**
     * Method rand2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public static int rand2(int i) {
        return (int) ((Math.random() * i) + 1);
    }

    /**
     * Method rand3
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public static int rand3(int i) {
        return (int) (Math.random() * i);
    }

    public static double randf(double i){
        return Math.random() * i;
    }

    /**
     * Method rand4
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public static int rand4(int i) {
        return (int) ((Math.random() * (i + 1)) + 1);
    }

    /**
     * Method roundTen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     *
     * @return
     */
    public static int roundTen(int x) {
        return x - x % 10;
    }

    /**
     * Method roundHundred
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     *
     * @return
     */
    public static int roundHundred(int x) {
        return x - x % 100;
    }

    /**
     * Method getRangeStr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public static int getRangeStr(int i) {
        int     str  = 0;
        int[][] data = {
            { 877, 10 }, { 9140, 46 }, { 9145, 36 }, { 9141, 64 }, { 9142, 82 }, { 9143, 85 }, { 9144, 95 },
            { 9236, 14 }, { 9237, 30 }, { 9238, 48 }, { 9239, 66 }, { 9240, 83 }, { 9241, 85 }, { 9242, 103 },
            { 9243, 105 }, { 9244, 117 }, { 9245, 120 }, { 882, 7 }, { 884, 10 }, { 886, 16 }, { 888, 22 }, { 890, 31 },
            { 892, 49 }, { 4740, 55 }, { 11212, 52 }, { 806, 1 }, { 807, 3 }, { 808, 4 }, { 809, 7 }, { 810, 10 },
            { 811, 14 }, { 11230, 20 }, { 864, 3 }, { 863, 4 }, { 865, 7 }, { 866, 10 }, { 867, 14 }, { 868, 24 },
            { 825, 6 }, { 826, 10 }, { 827, 12 }, { 828, 18 }, { 829, 28 }, { 830, 42 }, { 800, 5 }, { 801, 7 },
            { 802, 11 }, { 803, 16 }, { 804, 23 }, { 805, 36 }, { 9976, 0 }, { 9977, 15 }, { 4212, 70 }, { 4214, 70 },
            { 4215, 60 }, { 4216, 40 }, { 4217, 30 }, { 4218, 20 }, { 4219, 15 }, { 4220, 10 }, { 4221, 9 },
            { 4222, 8 }, { 4223, 6 }, { 6522, 49 }, { 10034, 29 }, { 15243, 150 }, { 13879, 125 }, { 20171, 100},
            { 11165, 160 }, { 16877, 145 }
        };

        for (int l = 0; l < data.length; l++) {
            if (i == data[l][0]) {
                str = data[l][1];
            }
        }

        return str;
    }

    /**
     * Method rangeMaxHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param thrown
     *
     * @return
     */
    public static double rangeMaxHit(Player pla, boolean thrown, int id) {
        return rangeMaxHit(pla, 1.00, thrown, id);
    }

    /**
     * Method rangeMaxHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param multi
     *
     * @return
     */
    public static double rangeMaxHit(Player pla, double multi, int id) {
        return rangeMaxHit(pla, multi, false, id);
    }

    /**
     * Method getMaxHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static double getMaxHit(Player pla) {
        int a = pla.getCurStat(2);// + pla.getPrayerBook().getTurmoilStrIncrease();

        a *= Combat.getVoidMultiplier(pla, 2);

        int    b  = pla.getEquipment().getTotalBonuses()[Item.BONUS_STRENGTH];
        double c  = (double) a;
        double d  = (double) b;
        double e  = 0;
        double f  = 0;
        double g  = 0;
        double h  = 0;
        double gg = pla.getPrayerBook().getBoost(2) - 1;

        e = c * (1 + g + gg);

        if (pla.getEquipment().getCurrentFightStyle() == AttackStyle.MODE_AGGRESSIVE) {
            e += 3;
        } else if (pla.getEquipment().getCurrentFightStyle() == AttackStyle.MODE_CONTROLLED) {
            e += 1;
        }



        f = (d * 0.00175) + 0.1;
        h = Math.floor(e * f + 2.05);



        if(h > 40)
            h-=2;

        return h;
    }

    /**
     * Method rangeMaxHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param multi
     * @param thrown
     *
     * @return
     */
    public static double rangeMaxHit(Player pla, double multi, boolean thrown, int id) {
        int a = pla.getCurStat(4);    // .playerLevel[4];

        int d = getRangeStr(pla.getEquipment().getId(Equipment.ARROWS));

        if (id == -1 && ((thrown || (pla.getEquipment().getId(3) == 4124) || (pla.getEquipment().getId(3) == 25)
                || (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_SPECIALBOW)
                || (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_SPECIAL_XBOW))) || id == 13879) {
            int id3 = pla.getEquipment().getId(3);

            d = getRangeStr(id3);
        } else {

            d = getRangeStr(id);
        }

        double b = pla.getPrayerBook().getBoost(4);
        double e = Math.floor(a * b);

        if (pla.getEquipment().getCurrentFightStyle() == AttackStyle.MODE_ACCURATE) {
            e = (e + 3.0);
        }

        if (pla.isTenth(4) && (pla.getGameFrame().getDuel() == null)) {
            b *= 1.10;
        }

        // b * CombatFunctions.getVoidMultiplier(pla, 4);
        double max = ((1.3 + e / 10 + d / 80 + e * d / 640) * multi) * b;

        max *= Config.RANGE_STRENGTH_MODIFIER;
        max *= Combat.getVoidMultiplier(pla, 4);

        return  Math.floor(max);
    }

    /**
     * Method getPercentFromTotal
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param subtotal
     * @param total
     *
     * @return
     */
    public static double getPercentFromTotal(double subtotal, double total) {
        return  (((float) subtotal / (float) total) * 100);
    }
    public static double getDubPercentFromTotal(int subtotal, int total) {
        return (((double) subtotal / (double) total) * 100);
    }
    /**
     * Method getAgilityBoost
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rate
     * @param agility
     *
     * @return
     */
    public static double getAgilityBoost(double rate, int agility) {
        rate -= (rate * ((agility * 0.65) / 100d));

        return rate;
    }

    /**
     * Method getEnergyDeplecation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param weight
     * @param agility
     *
     * @return
     */
    public static double getEnergyDeplecation(double weight, int agility) {
        final double energy_used_per_tile = 0.218;

        return getAgilityBoost(energy_used_per_tile * 3
                               * Math.pow(Math.E,
                                          0.0027725887222397812376689284858327062723020005374410 * weight), agility);
    }

    /**
     * Method getMeleeDefence
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param victim
     * @param bonus
     *
     * @return
     */
    public static double getMeleeDefence(Killable victim, int bonus) {
        int    style    = 0;
        int    defBonus = bonus;
        int    defLvl   = 1;
        double defMult  = 1;

        if(victim.isNPC() && victim.getNpc().getId() == 1265)
            return -20;

        if (bonus < 0) {
            return 1;
        }

        int defenceMultiplier = 1;

        if (victim instanceof Player) {
            Player plr = (Player) victim;

            defMult = plr.getPrayerBook().getBoost(1);

            if(plr.getPrayerBook().curseActive(PrayerBook.TURMOIL))
                defMult += .23;

            defLvl = plr.getCurStat(1);

            Equipment e = plr.getEquipment();

            if (e.getCurrentFightStyle() == AttackStyle.MODE_CONTROLLED) {
                style = 1;
            } else if (e.getCurrentFightStyle() == AttackStyle.MODE_DEFENSIVE) {
                style = 3;
            }
        }else if(victim instanceof NPC){
            defLvl = victim.getNpc().getDefinition().getCombatStats()[1];
        }

        if(victim instanceof Player){
            if(victim.getPlayer().getGameFrame().getDuel() != null){
                style = 1;
                defenceMultiplier = 1;
                defMult = 1;
            }
        }



        double cumulativeDef = ((defLvl + style) * 0.90) * defMult;

        return (cumulativeDef * defBonus) * defenceMultiplier;
    }


    public static double randomMaxHit(double hit){


       return random.nextDouble() * hit;
       }

    /**
     * Method getMeleeAccuracy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param source
     * @param attBonus
     *
     * @return
     */
    public static double getMeleeAccuracy(Killable source, int attBonus) {
        int    style              = 0;
        int    attLvl             = 1;
        double attMult            = 1.00;
        double accuracyMultiplier = 1.00;

        if (source instanceof Player) {
            Player player  = (Player) source;
            int    atStyle = player.getEquipment().getCurrentFightStyle();

            if (atStyle == AttackStyle.MODE_ACCURATE) {
                style = 3;
            } else if (atStyle == AttackStyle.MODE_CONTROLLED) {
                style = 1;
            }

            attMult            = player.getPrayerBook().getBoost(0);
            attLvl             = player.getCurStat(0);
            accuracyMultiplier = player.getSpecialMultiplier();
            attLvl *= Combat.getVoidMultiplier(source.getPlayer(), 0);
        }

        if (source instanceof NPC) {
            attLvl = source.getNpc().getDefinition().getCombatStats()[0];
        }

        if(source instanceof Player){
            if(source.getPlayer().getGameFrame().getDuel() != null){
                style = 1;
                accuracyMultiplier = 1;
                attMult = 1;
            }
        }


        double cumulativeAtt = (attLvl + style) * attMult;


        return (cumulativeAtt * attBonus) * accuracyMultiplier;
    }

    /**
     * Method getRangeAccuracy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param source
     * @param accuracyMultiplier
     *
     * @return
     */
    public static double getRangeAccuracy(Killable source, double accuracyMultiplier) {
        int style = 0;

        if (source.isPlayer()
                && (source.getPlayer().getEquipment().getCurrentFightStyle() == AttackStyle.MODE_ACCURATE)) {
            style = 3;

        }
       if(source.isPlayer())
        accuracyMultiplier = source.getPlayer().getSpecialMultiplier();

        int    attLvl        = source.isPlayer()
                               ? source.getPlayer().getCurStat(4)
                               : source.getNpc().getDefinition().getCombatStats()[4];
        int    attBonus      = source.isPlayer()
                               ? source.getPlayer().getEquipment().getTotalBonuses()[Item.BONUS_ATTACK_RANGE]
                               : source.getNpc().getDefinition().getBonuses()[Item.BONUS_ATTACK_RANGE];
        double attMult       = source.isPlayer()
                               ? source.getPlayer().getPrayerBook().getBoost(4)
                               : 1;

        if (source instanceof Player) {
            attLvl *= Combat.getVoidMultiplier(source.getPlayer(), 4);
        }
        double cumulativeAtt = attLvl * attMult + style;

        //

        return (((14 + cumulativeAtt + (attBonus / 8) + ((cumulativeAtt * attBonus) / 64))) * accuracyMultiplier) * 1.10;
    }

    /**
     * Method getRangedDefence
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param victim
     * @param defenceMultiplier
     *
     * @return
     */
    public static double getRangedDefence(Killable victim, double defenceMultiplier) {
        int style = 0;

        if (victim.isPlayer()) {
            if (victim.getPlayer().getEquipment().getCurrentFightStyle() == AttackStyle.MODE_LONGRANGE) {
                style = 3;
            }
        }

        if(victim.isNPC() && victim.getNpc().getId() == 1265)
            return -20;


        int    defLvl   = victim.isNPC()
                          ? victim.getNpc().getDefinition().getCombatStats()[1]
                          : victim.getPlayer().getCurStat(1);
        int    defBonus = victim.isNPC()
                          ? victim.getNpc().getDefinition().getBonuses()[Item.BONUS_DEFENCE_RANGE]
                          : victim.getPlayer().getEquipment().getTotalBonuses()[Item.BONUS_DEFENCE_RANGE];
        double defMult  = 1.0;

        defMult = victim.isPlayer()
                  ? victim.getPlayer().getPrayerBook().getBoost(1)
                  : 1;

        double cumulativeDef = (defLvl * 0.8) * defMult + style;

        return (14 + cumulativeDef + (defBonus / 8) + ((cumulativeDef * defBonus) / 64)) * defenceMultiplier;
    }

    /**
     * Method getLevelForXP
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param exp
     * @param pla
     *
     * @return
     */
    public static int getLevelForXP(int exp, int skillID) {
        int points = 0;
        int output;
        int i = 99;
        if(skillID == 24)
                i = 120;

        if (exp == Integer.MAX_VALUE) {
            exp--;
        }

        exp += 1;

        for (int lvl = 1; lvl <= i; lvl++) {
            points += Math.floor((double) lvl + 300.0 * Math.pow(2.0, (double) lvl / 7.0));
            output = (int) Math.floor(points / 4);

            if (output >= exp) {
                return lvl;
            }
        }

        return i;
    }

    /**
     * Method getHarvestChance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param base
     * @param lvl
     * @param requiredLevel
     * @param bonus
     *
     * @return
     */
    public static int getHarvestChance(int base, int lvl, int requiredLevel, double bonus) {
        int    difference  = lvl - requiredLevel;
        double basePercent = base;

        return (int) (basePercent + (difference * 0.70) * bonus);
    }

    /**
     * Method getXPModifier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param prestigeLevel
     *
     * @return
     */
    public static double getXPModifier(int prestigeLevel) {
        double base_time = 30;

        switch (prestigeLevel) {
        case 0 :
            return 45;

        case 1 :
            return 30;

        case 2 :
            return 26;

        case 3 :
            return 20;

        case 4 :
            return 17;

        case 5 :
            return 13;

        case 6 :
            return 10;

        case 7 :
            return 8;

        case 8 :
            return 6;

        case 9 :
            return 4;

        case 10 :
            return 2;
        }

        if (prestigeLevel == 10) {
            return 1;
        }

        prestigeLevel += 1;
        base_time     /= (prestigeLevel * 1.85);

        return base_time;
    }

    /**
     * Method getXPForLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param level
     *
     * @return
     */
    public static int getXPForLevel(int level) {
        int points = 0;
        int output = 0;

        for (int lvl = 1; lvl <= level; lvl++) {
            points += Math.floor((double) lvl + 300.0 * Math.pow(2.0, (double) lvl / 7.0));

            if (lvl >= level) {
                return output;
            }

            output = (int) Math.floor(points / 4);
        }

        return 0;
    }

    /**
     * Method getGaussian
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param meanModifier
     * @param r
     * @param maximum
     *
     * @return
     */

    public static final Random ROLL_RAND = new Random();

    public static double getGaussian(double meanModifier, Random r, double maximum) {
        double mean      = maximum * meanModifier;
        double deviation = mean * 1.79;
        double value     = 0;
        double attempts = 0;

        do {
            if(++attempts > 1000){
               throw new RuntimeException("ERROR TOO MANY GAUSSIAN ATTEMPTS "+meanModifier+" "+maximum);
            }
            value = Math.floor(mean + r.nextGaussian() * deviation);
        } while ((value < 0) || (value > maximum));

        return value;
    }


    public static boolean success(int level, int required, int bonus) {

        return new Random().nextInt((int) (700 +bonus
                + (((level - required) * 10)
                * Math.ceil(((GameMath.getPercentFromTotal(level, level + required))
                / 10))))) > new Random().nextInt(1000);
    }
}
