package net.tazogaming.hydra.util.math;

//~--- JDK imports ------------------------------------------------------------

import java.security.SecureRandom;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/01/2015
 * Time: 01:24
 */
public class ZeusRandom {

    /** probabiltiies made: 15/01/17 **/
    private Boolean[][] probabiltiies;

    /**
     * Constructs ...
     *
     *
     * @param bucketSize
     */
    public ZeusRandom(int bucketSize) {
        probabiltiies = new Boolean[100][bucketSize];

        for (int i = 1; i < 100; i++) {
            List<Boolean> chance_list = new ArrayList<Boolean>();
            int           trueCount   = (int) Math.ceil((double) ((double) bucketSize / (double) 100) * i);

            for (int k = 0; k < trueCount; k++) {
                chance_list.add(true);
            }

            for (int k = trueCount; k < bucketSize; k++) {
                chance_list.add(false);
            }

            probabiltiies[i] = new Boolean[bucketSize];

            // Shuffle them, this part is very important
            Collections.shuffle(chance_list, new SecureRandom());

            try {
                Thread.sleep(new SecureRandom().nextInt(80));
            } catch (Exception ee) {}

            chance_list.toArray(probabiltiies[i]);
        }
    }

    /**
     * Method isTrue
     * Created on 15/01/17
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param probability
     *
     * @return
     */
    public boolean isTrue(int probability) {
        if ((probability < 1) || (probability > 100)) {
            throw new IndexOutOfBoundsException("Error probability must be atleast 1% and must not be lower than 100%");
        }

        return this.probabiltiies[probability][GameMath.rand3(probabiltiies[probability].length)];
    }

    /**
     * Method getProbabilities
     * Created on 15/01/17
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Boolean[][] getProbabilities() {
        return probabiltiies;
    }
}
