package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.test.Main;

//~--- JDK imports ------------------------------------------------------------

import java.io.FileReader;

import java.util.Properties;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 23/09/13
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
public class Config {

    public static boolean DEBUG_HITS = false;

    public static int          DISCONNECT_GIVE_UP      = Main.TEST_MODE
            ? 0
            : (25);                                              // number of seconds before a player is logged out
    public static int          FLOOR_ITEM_GLOBAL_TIME  = 200;    // 60 secs
    public static int          FLOOR_ITEM_REMOVAL_TIME = Core.getTicksForMinutes(15);    // 2:30 secs
    public static int          COMBAT_PJ_DELAY         = Core.getTicksForSeconds(4);     // 7 seconds
    public static  int    DUMMY_PLAYERS           = 1;
    public static final int    LOG_SPILL_RATE          = 200;
    public static long         lastLogCleanup          = System.currentTimeMillis();
    public static final double COMBAT_XP_MODIFIER      = 100;
    public static final double SKILL_XP_RATE           = 10.11;
    public static final double MELEE_ACCURACY_MODIFIER = 1.15;
    public static final double RANGE_ACCURACY_MODIFIER = 1.35;
    public static final double RANGE_STRENGTH_MODIFIER = 1.05;
    public static final int    POISON_TIME             = 1600;
    public static final int    SUPER_POISON_TIME       = 2000;
    public static boolean      doubleXp                = false;
    public static boolean      doublePk                = false;
    public static boolean      doubleDung              = false;
    public static final int    MEGA_POISON_TIME        = 2400;
    public static int          AUTOSAVE_DELAY          = 100;
    public static boolean      double_drops            = false;
    public static int          PACKET_THROTTLE_MAX     = 5;
    public static final int    PAYMENT_PORT            = 4494;
    public static final int    BANK_RESTORE_RATE       = Core.getTicksForMinutes(60) * 2;
    public static final int    PVP_DELAY_TIME          = Core.getTicksForMinutes(240);
    public static boolean      TEST_MODE               = false;
    public static double[]     XP_MODIFIERS            = new double[25];
    public static String       ACC_SERVER_ADDR         = null;
    public static int          ACC_PORT                = 0;
    public static final double XP_RATE                 = 15;
    public static boolean enable_debugging = true;

    public static final int SERVER_VERSION = 119;


    /** databaseInfo made: 14/08/20 */
    private static Properties databaseInfo;

    /** dev_client_info made: 14/08/20 */
    private static Properties dev_client_info;

    /** dev_server_info made: 14/08/20 */
    private static Properties dev_server_info;

    /** mainInfo made: 14/08/20 */
    private static Properties mainInfo;

    static {
        for (int i = 0; i < XP_MODIFIERS.length; i++) {
            XP_MODIFIERS[i] = 1.00;
        }

        XP_MODIFIERS[4] = 0.68;
        XP_MODIFIERS[21] = 1.8; // HUNTING
        XP_MODIFIERS[20] = 3.8; // RC
        XP_MODIFIERS[23] = 2.8;
        XP_MODIFIERS[24] = 1.00;
        XP_MODIFIERS[21] = 1.9;
        XP_MODIFIERS[5] = 2.23;
        XP_MODIFIERS[6] = 25;
        XP_MODIFIERS[22] = 0.5;
        XP_MODIFIERS[4] = 0.8;
        XP_MODIFIERS[8] = 0.5;
        XP_MODIFIERS[17] = 1.4;
        XP_MODIFIERS[13] = 1.2;
        XP_MODIFIERS[12] = 1.0;
        XP_MODIFIERS[18] = 1.9;

    }

    /**
     * Method getMainInfo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Properties getMainInfo() {
        if (mainInfo == null) {
            try {
                mainInfo = new Properties();
                mainInfo.load(new FileReader("config/hydra.conf"));
                DISCONNECT_GIVE_UP      = Integer.parseInt(mainInfo.getProperty("disconnect_give_up"));
                FLOOR_ITEM_GLOBAL_TIME  = Integer.parseInt(mainInfo.getProperty("floor_item_global_ticks"));
                FLOOR_ITEM_REMOVAL_TIME = Integer.parseInt(mainInfo.getProperty("floor_item_removal_ticks"));
                COMBAT_PJ_DELAY         = Integer.parseInt(mainInfo.getProperty("pj_delay"));
                PACKET_THROTTLE_MAX     = Integer.parseInt(mainInfo.getProperty("packet_throttle_max"));
                doubleDung              = Boolean.parseBoolean(mainInfo.getProperty("double_dung"));
                doublePk                = Boolean.parseBoolean(mainInfo.getProperty("double_pk"));
                doubleXp                = Boolean.parseBoolean(mainInfo.getProperty("double_xp"));
                TEST_MODE               = Boolean.parseBoolean(mainInfo.getProperty("test_mode"));
                AUTOSAVE_DELAY          = Integer.parseInt(mainInfo.getProperty("autosave_delay"));
                double_drops            = Boolean.parseBoolean(mainInfo.getProperty("double_drops"));
                ACC_SERVER_ADDR         = mainInfo.getProperty("ac_addr");
                ACC_PORT                = Integer.parseInt(mainInfo.getProperty("ac_port"));
            } catch (Exception ee) {
                Logger.err("unable to load config");
                System.exit(-1);
            }
        }

        return mainInfo;
    }

    /**
     * Method getServerInfo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Properties getServerInfo() {
        if (dev_server_info == null) {
            try {
                dev_server_info = new Properties();
                dev_server_info.load(new FileReader("push_server.conf"));
            } catch (Exception ee) {
                dev_server_info = null;
            }
        }

        return dev_server_info;
    }

    /**
     * Method getDevClientConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Properties getDevClientConfig() {
        if (dev_client_info == null) {
            try {
                dev_client_info = new Properties();
                dev_client_info.load(new FileReader("dev_client.conf"));
            } catch (Exception ee) {
                dev_client_info = null;
            }
        }

        return dev_client_info;
    }

    /**
     * Method loadLastLogCleanup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadLastLogCleanup() {
        try {
            Buffer file = new Buffer("config/cleanup.dat");

            lastLogCleanup = file.readQWord();
        } catch (Exception ee) {}
    }

    /**
     * Method getDatabaseInfo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Properties getDatabaseInfo() {
        if (databaseInfo == null) {
            try {
                databaseInfo = new Properties();
                databaseInfo.load(new FileReader("database.conf"));
            } catch (Exception e) {
                e.printStackTrace();    // To change body of catch statement use File | Settings | File Templates.
            }
        }

        return databaseInfo;
    }
}
