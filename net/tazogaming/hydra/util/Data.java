
/*
* Data.java
*
* Created on 17-Dec-2007, 21:51:20
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
 */
package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------


/**
 *
 * @author alex
 */
public class Data {


    public static final String[] AVAILABLE_TITLES =
            {"Rookie", "Zerker", "Pure", };


    private static final char[] validChars = {
        '_', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
        'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    };


    public static final int[] BIT_MASK = new int[30];
    static {
        int x = 1;
        BIT_MASK[0] = 1;
        for(int i = 1; i < BIT_MASK.length; i++){
           x <<= 1;
           BIT_MASK[i] = x;
        }
    }
    public static final int
        ATTACK        = 0,
        DEFENCE       = 1,
        STRENGTH      = 2,
        HITPOINTS     = 3,
        RANGE         = 4,
        PRAYER        = 5,
        MAGIC         = 6,
        COOKING       = 7,
        WOODCUTTING   = 8,
        FLETCHING     = 9,
        FISHING       = 10,
        FIREMAKING    = 11,
        CRAFTING      = 12,
        SMITHING      = 13,
        MINING        = 14,
        HERBLORE      = 15,
        AGILITY       = 16,
        THIEVING      = 17,
        SLAYER        = 18,
        FARMING       = 19,
        RUNECRAFTING  = 20,
        DUNGEONEERING = 21,
        HUNTER        = 22,
        SUMMONING     = 23,
        CONSTRUCTION  = 24;

}
