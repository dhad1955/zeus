package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.io.message.PlayerMessage;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;

import org.apache.mina.core.session.IoSession;

//~--- JDK imports ------------------------------------------------------------

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/10/13
 * Time: 18:21
 * Info: handles all messages from player-to-player
 */
public class PlayerMessagingHandler implements Runnable {
    private static int                pm_id                      = 30;
    private LinkedList<PlayerFriends> current_friends_to_process = new LinkedList<PlayerFriends>();
    private LinkedList<PlayerFriends> new_friends_to_process     = new LinkedList<PlayerFriends>();
    private LinkedList<PlayerMessage> yell_queue                 = new LinkedList<PlayerMessage>();
    private LinkedList<PlayerMessage> messagesToProcess          = new LinkedList<PlayerMessage>();
    private LinkedList<PlayerMessage> currentlyProcessing        = new LinkedList<PlayerMessage>();

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String toString() {
        return "toprocess: " + messagesToProcess.size() + " processing: " + currentlyProcessing.size()
               + " yell_queue: " + yell_queue.size();
    }

    /**
     * Method addMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param message
     */
    public void addMessage(PlayerMessage message) {
        synchronized (messagesToProcess) {
            messagesToProcess.add(message);
        }
    }

    /**
     * Method addFriendToProcess
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void addFriendToProcess(Player player) {
        synchronized (new_friends_to_process) {
            new_friends_to_process.add(new PlayerFriends(player));
        }
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (Exception ee) {}

            try {
                updateFriendStatus();
            } catch (Exception ee) {
                ee.printStackTrace();
            }

            if (messagesToProcess.size() == 0) {
                continue;
            }

            synchronized (messagesToProcess) {
                currentlyProcessing.addAll(messagesToProcess);
                messagesToProcess.clear();
            }

            try {
                for (PlayerMessage message : currentlyProcessing) {
                    if (message.getType() == PlayerMessage.TYPE_CLAN_MESSAGE) {
                        ClanChannel c = message.getChannel();

                        for (Player pla : c.getChatters()) {
                            pla.getActionSender().sendClanMessage(c.getName(), message.getSender(),
                                    message.getMessageData_string(), message.getPlayerRights());
                        }
                    }

                    if (message.getType() == PlayerMessage.TYPE_PRIVATE_MESSAGE) {
                        try {
                            Player sendingTo = World.getWorld().getPlayer(message.getTarget());

                            if (sendingTo != null) {
                                synchronized (sendingTo.getIoSession()) {
                                    sendingTo.log(" < " + message.getSender() + " : " + message.getMsg());
                                    sendPm(null, Text.stringToLong(message.getSender()),
                                           message.getPlayerRights(), message.getMessageData(),
                                           message.getMessageData().length);
                                }
                            }
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }

                    if (message.getType() == PlayerMessage.TYPE_YELL_MESSAGE) {
                        yell_queue.add(message);
                    }
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }

            currentlyProcessing.clear();

            LinkedList<Player> players = new LinkedList<Player>();

            try {
                synchronized (World.getWorld().getPlayers()) {
                    players.addAll(World.getWorld().getPlayers());
                }

                for (Player player : players) {
                    for (PlayerMessage message : yell_queue) {
                        try {

                            // yell(player, message.getSender(), message.getMessageData_string());
                        } catch (Exception ee) {}
                    }
                }

                yell_queue.clear();
            } catch (Exception ee) {
                ee.printStackTrace();
            }

            yell_queue.clear();
        }
    }

    /**
     * Method yell
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param name
     * @param msg
     * @param rights
     */
    public void yell(Player player, String name, String msg, int rights) {
        if ((name == null) || (msg == null) || (player == null) || (player.getIoSession() == null)) {
            return;
        }

        // player.getIoSession().write(
        // new MessageBuilder().setSize(Packet.Size.VariableShort).setId(217).addString(name).addString(
        // msg).addByte((byte) rights).toPacket());
    }

    /**
     * Method forcePm
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param rights
     * @param message
     */
    public void forcePm(String sender, int rights, String message) {

        // byte[] message_data = TextInput.textPack()
    }

    private void sendPm(IoSession session, long sender, int rights, byte[] message, int messagesize) {
        if (rights == Player.OFFICIAL_HELPER) {
            rights = 0;
        }

        // MessageBuilder spb = new MessageBuilder().setId(196).setSize(Packet.Size.VariableByte).addLong(
        // sender).addInt(pm_id++).addByte((byte) rights).addBytes(message, 0, messagesize);
        // session.write(spb.toPacket());
    }

    /**
     * Method updateFriendStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateFriendStatus() {
        synchronized (new_friends_to_process) {
            if (new_friends_to_process.size() > 0) {
                current_friends_to_process.addAll(new_friends_to_process);
                new_friends_to_process.clear();
            }
        }

        Iterator<PlayerFriends> iter = current_friends_to_process.iterator();

        while (iter.hasNext()) {
            PlayerFriends friends = iter.next();

            if (!friends.getPlayer().getIoSession().isConnected()) {
                iter.remove();
            } else {
                friends.update();
            }
        }
    }
}
