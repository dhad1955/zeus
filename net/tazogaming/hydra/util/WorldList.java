package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/07/14
 * Time: 18:25
 */
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class WorldList {
    public static final int                  COUNTRY_AUSTRALIA   = 16;
    public static final int                  COUNTRY_BELGIUM     = 22;
    public static final int                  COUNTRY_BRAZIL      = 31;
    public static final int                  COUNTRY_CANADA      = 38;
    public static final int                  COUNTRY_DENMARK     = 58;
    public static final int                  COUNTRY_FINLAND     = 69;
    public static final int                  COUNTRY_IRELAND     = 101;
    public static final int                  COUNTRY_MEXICO      = 152;
    public static final int                  COUNTRY_NETHERLANDS = 161;
    public static final int                  COUNTRY_NORWAY      = 162;
    public static final int                  COUNTRY_SWEDEN      = 191;
    public static final int                  COUNTRY_UK          = 77;
    public static final int                  COUNTRY_USA         = 225;
    public static final int                  FLAG_HIGHLIGHT      = 16;
    public static final int                  FLAG_LOOTSHARE      = 8;
    public static final int                  FLAG_MEMBERS        = 1;
    public static final int                  FLAG_NON_MEMBERS    = 0;
    public static final int                  FLAG_PVP            = 4;
    private static final ArrayList<WorldDef> worldList           = new ArrayList<WorldDef>();

    static {
        worldList.add(new WorldDef(1, 0, FLAG_MEMBERS, "Main World", "sv04.asccio.net", "California", COUNTRY_USA));
        worldList.add(new WorldDef(2, 0, FLAG_MEMBERS, "Classic World", "217.23.3.147", "Netherlands", COUNTRY_NETHERLANDS));

        worldList.add(new WorldDef(3, 0, FLAG_MEMBERS, "Localhost", "localhost", "Netherlands", COUNTRY_NETHERLANDS));
        worldList.add(new WorldDef(4, 0, FLAG_MEMBERS, "Developers world", "dev.enchanta.net", "Netherlands", COUNTRY_NETHERLANDS));

    }

    /**
     * Method getData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param worldConfiguration
     * @param worldStatus
     *
     * @return
     */
    public static Packet getData(boolean worldConfiguration, boolean worldStatus) {
        MessageBuilder bldr = new MessageBuilder(98, Packet.Size.VariableShort);

        bldr.addByte((byte) 1);
        bldr.addByte((byte) 2);
        bldr.addByte((byte) 1);

        if (worldConfiguration) {
            populateConfiguration(bldr);
        }

        if (worldStatus) {
            populateStatus(bldr);
        }

        return bldr.toPacket();
    }

    private static void populateConfiguration(MessageBuilder buffer) {
        addSmart(buffer, worldList.size());
        setCountry(buffer);
        addSmart(buffer, 0);
        addSmart(buffer, (worldList.size() + 1));
        addSmart(buffer, worldList.size());

        for (WorldDef w : worldList) {
            addSmart(buffer, w.getWorldId());
            buffer.addByte((byte) w.getLocation());
            buffer.addInt(w.getFlag());
            buffer.putGJString(w.getActivity());    // activity
            buffer.putGJString(w.getIp());          // ip address
        }
        buffer.addInt(0x94DA4A87);    // != 0
    }

    private static void populateStatus(MessageBuilder buffer) {
        for (WorldDef w : worldList) {
            addSmart(buffer, w.getWorldId());                                 // world id
            buffer.addShort((short) World.getWorld().getPlayers().size() + Config.DUMMY_PLAYERS);    // player count
        }
    }

    private static void addSmart(MessageBuilder buffer, int value) {
        buffer.addSmart(value);
    }

    private static void setCountry(MessageBuilder buffer) {
        for (WorldDef w : worldList) {
            addSmart(buffer, w.getCountry());
            buffer.putGJString(w.getRegion());
        }
    }
}
