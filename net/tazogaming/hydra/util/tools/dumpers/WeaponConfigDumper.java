package net.tazogaming.hydra.util.tools.dumpers;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.Item;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/10/13
 * Time: 17:59
 */
public class WeaponConfigDumper {

    /**
     * Constructs ...
     *
     *
     * @param ids
     *
     * @throws IOException
     */
    public WeaponConfigDumper(int... ids) throws IOException {
        BufferedWriter br = new BufferedWriter(new FileWriter("config/out.txt"));

        for (Integer g : ids) {
            br.write("// " + Item.forId(g).getName() + "");
            br.newLine();
            br.write("weapon " + g + " {");
            br.newLine();
            br.write("type = melee");
            br.newLine();
            br.write("tickspeed = 5");
            br.newLine();
            br.write("style 1 = stab accurate 412");
            br.newLine();
            br.write("style 2 = slash aggressive 451");
            br.newLine();
            br.write("style 3 = slash aggressive 451");
            br.newLine();
            br.write("style 4 = stab defensive 412");
            br.newLine();
            br.write("}");
            br.newLine();
        }

        br.close();
    }
}
