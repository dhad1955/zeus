package net.tazogaming.hydra.util.tools.dumpers;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.io.js5.format.CacheItemDefinition;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/07/14
 * Time: 17:55
 */
public class ItemDumper {
    private static BufferedWriter bw;

    private static void dumpItem(String line) {
        try {
            bw.write(line);
            bw.newLine();
        } catch (Exception ee) {}
    }

    private static boolean getTradeable(int id) {
        try {
            return Item.forId(id).isTradeable();
        } catch (Exception ee) {
            return true;
        }
    }

    /**
     * Method init
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @throws IOException
     */
    public static void init() throws IOException {
        System.out.println("Loading interfaces definitions...");

        FileChannel channel = new RandomAccessFile("data/interfaces/itemDefinitions.bin", "r").getChannel();
        ByteBuffer  buffer  = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
        int         length  = buffer.getShort();

        for (int i = 0; i < length; i++) {
            int id = buffer.getShort();

            if (id == -1) {
                continue;
            }

            int     equipId         = buffer.getShort();
            int     renderEmote     = buffer.getShort();
            String  name            = CacheItemDefinition.readRS2String(buffer);
            boolean noted           = buffer.get() == 0;
            int[]   bonus           = new int[15];
            int[]   absorptionBonus = new int[3];
            boolean tradeable       = true;
            int     attackSpeed     = 5;
            int     equipmentSlot   = -1;
            String  examine         = CacheItemDefinition.readRS2String(buffer);
            double  weight          = 0;
            int     highAlchPrice   = 0,
                    lowAlchPrice    = 0,
                    storePrice      = 0,
                    exchangePrice   = 0;

            if (!noted) {
                weight        = buffer.getDouble();
                equipmentSlot = buffer.get();
                tradeable     = buffer.get() == 1;
                attackSpeed   = buffer.get();

                if (buffer.get() == 1) {
                    for (int x = 0; x < 15; x++) {
                        bonus[x] = buffer.getShort();
                    }
                }

                if (buffer.get() == 1) {
                    for (int x = 0; x < 3; x++) {
                        absorptionBonus[x] = buffer.getShort();
                    }
                }
            }

            highAlchPrice = buffer.getInt();
            lowAlchPrice  = buffer.getInt();
            storePrice    = buffer.getInt();
            exchangePrice = buffer.getInt();

            boolean stackable = buffer.get() == 1;
            boolean hasReq    = buffer.get() == 1;

            if (hasReq) {

                // skillRequirementId = new ArrayList<Integer>();
                // s//killRequirementLvl = new ArrayList<Integer>();
                int size = buffer.get();

                for (int j = 0; j < size; j++) {

                    // skillRequirementId.add((int) buffer.get());
                    // /skillRequirementLvl.add((int) buffer.get());
                    buffer.get();
                    buffer.get();
                }
            }

            boolean dropable  = buffer.get() == 1;
            boolean twoHanded = buffer.get() == 1;

            dumpItem(id + " " + name.replace(" ", "_") + " " + examine.replace(" ", "_") + " " + noted + " "
                     + stackable + " " + highAlchPrice + " " + getTradeable(i) + " " + renderEmote);

            if (id == 15220) {
                bonus[11] = 8;
            }

            if (id == 18819) {
                for (int ii = 0; ii < 15; ii++) {
                    if (ii == 11) {
                        continue;
                    }

                    bonus[ii] = 0;
                }

                bonus[11] = 0;
            }
        }
        ;

        channel.close();
        channel = null;
    }

    /**
     * Method dump
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void dump() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("rs2items.cfg"));

            bw = new BufferedWriter(new FileWriter("rs2items2.cfg", true));

            final int[] splitOffsets = {
                0, 1, 2, 3, 4, 6, 8, 9
            };
            String      line         = null;

            while ((line = reader.readLine()) != null) {
                String[]     split  = line.split(" ");
                StringBuffer buffer = new StringBuffer();

                for (int k = 0; k < splitOffsets.length; k++) {
                    buffer.append(split[splitOffsets[k]] + " ");
                }

                bw.write(buffer.toString());
                bw.newLine();
            }

            bw.close();
            System.exit(-1);

            for (int i = 0; i < 30000; i++) {
                CacheItemDefinition def = CacheItemDefinition.getItemDefinition(i);

                if ((def == null) && (i > 20000)) {
                    bw.close();

                    return;
                } else if (def == null) {
                    continue;
                }

                bw.write("" + def.getId() + " ");

                String name = def.getName();

                if (name == null) {
                    name = "null";
                    Logger.debug("unnoted: " + def.isUnnoted());
                } else {
                    Logger.debug("not null unnoted: " + def.isUnnoted());
                }

                bw.write(name.replace(" ", "_") + " ");

                try {
                    Item item = Item.forId(def.getId());

                    if (item != null) {
                        bw.write(item.getPrice() + " ");
                        bw.write(item.isStackable() + " ");
                        bw.write(item.isNote() + " ");
                        bw.write(item.isTradeable() + " ");
                        bw.write(item.getWeight() + " ");
                    } else {
                        throw new RuntimeException();
                    }
                } catch (Exception ee) {
                    bw.write("0 false " + !def.isUnnoted() + " true 0.0");
                }

                bw.newLine();
            }

            bw.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}
