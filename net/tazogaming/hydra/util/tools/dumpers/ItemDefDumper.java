package net.tazogaming.hydra.util.tools.dumpers;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.Item;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 28/09/13
 * Time: 22:47
 * To change this template use File | Settings | File Templates.
 */
public class ItemDefDumper {
    public static double[]       weights     = new double[30000];
    public static final String[] BONUS_NAMES = {
        "Attack-Stab", "Attack-Slash", "Attack-Crush", "Attack-Magic", "Attack-Range", "Defence-Stab", "Defence-Slash",
        "Defence-Crush", "Defence-Magic", "Defence-Range", "Strength", "Prayer"
    };
    public static BufferedWriter br;

    /**
     * Constructs ...
     *
     *
     * @param path
     *
     * @throws IOException
     */
    public ItemDefDumper(String path) throws IOException {
        Item.loadItemList(true, null);
        br = new BufferedWriter(new FileWriter("config/item/bonus.cfg"));

        BufferedReader reader = new BufferedReader(new FileReader("config/interfaces.cfg"));

        while (true) {
            try {
                String str = reader.readLine();

                if (str == null) {
                    break;
                }

                str = str.replaceAll("\\t", " ");

                String[] split   = str.split(" ");
                Item     i       = Item.forId(Integer.parseInt(split[2]));
                int[]    bonuses = getBonuses(split, 8);

                if ((bonuses != null) && (i != null)) {
                    br.write("// " + i.getName() + "");
                    br.newLine();
                    br.write("bonus " + i.getIndex() + " {");
                    br.newLine();

                    for (int index = 0; index < bonuses.length; index++) {
                        br.write(BONUS_NAMES[index] + " = " + bonuses[index]);
                        br.newLine();
                    }

                    br.write("}");
                    br.newLine();
                    br.newLine();
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        br.close();
        reader.close();

        // definitions[id] = new ItemDefinition(id, name, examine, equipId, renderEmote, bonus, stackable, noted, tradeable, skillRequirementId, skillRequirementLvl, weight, highAlchPrice, lowAlchPrice, storePrice, exchangePrice, attackSpeed, equipmentSlot, absorptionBonus, dropable, twoHanded);
        // defi
    }

    /**
     * Method getBonuses
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param split
     * @param startOffset
     *
     * @return
     */
    public static int[] getBonuses(String split[], int startOffset) {
        int[]   bonuses = new int[BONUS_NAMES.length];
        boolean b       = false;
        int     ind     = 0;

        for (int i = startOffset; i < split.length; i++) {
            bonuses[ind] = Integer.parseInt(split[i]);

            if (bonuses[ind++] > 0) {
                b = true;
            }
        }

        if (!b) {
            return null;
        }

        return bonuses;
    }
}
