package net.tazogaming.hydra.util.tools;

//~--- JDK imports ------------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/03/14
 * Time: 03:22
 */
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;

import java.net.HttpURLConnection;
import java.net.URL;

import java.util.HashMap;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class HTTPost {
    private HashMap<String, String> parameters = new HashMap<String, String>();
    private final String            USER_AGENT = "Mozilla/5.0";
    private String                  url;

    /**
     * Constructs ...
     *
     *
     * @param url
     */
    public HTTPost(String url) {
        this.url = url;
    }

    /**
     * Method put
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param stra
     */
    public void put(String str, String stra) {
        parameters.put(str, stra);
    }

    // HTTP POST request

    /**
     * Method submitRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @throws Exception
     */
    public void submitRequest() throws Exception {
        URL               obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String       urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
        StringBuffer buffer        = new StringBuffer();
        int          c             = 0;

        for (String key : parameters.keySet()) {
            buffer.append(key);
            buffer.append("=");
            buffer.append(parameters.get(key));
            c++;

            if (c != parameters.keySet().size()) {
                buffer.append("&");
            }
        }

        con.setDoOutput(true);

        DataOutputStream wr = new DataOutputStream(con.getOutputStream());

        wr.writeBytes(buffer.toString());
        wr.flush();
        wr.close();

        int            responseCode = con.getResponseCode();
        BufferedReader in           = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String         inputLine;
        StringBuffer   response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();
    }
}
