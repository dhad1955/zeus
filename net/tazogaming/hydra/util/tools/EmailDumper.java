package net.tazogaming.hydra.util.tools;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.build.lexer.Lexer;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/08/14
 * Time: 20:13
 */
public class EmailDumper {

    /** emails made: 14/08/30 **/
    private List<String> emails = new ArrayList<String>();

    /** file made: 14/08/30 **/
    private File file;

    /**
     * Constructs ...
     *
     *
     * @param file
     *
     * @throws FileNotFoundException
     */
    public EmailDumper(String file) throws FileNotFoundException {
        this.file = new File(file);

        if (!this.file.exists()) {
            throw new FileNotFoundException("Error file not found: " + file);
        }
    }

    /**
     * Method extract
     * Created on 14/08/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param output the output file to dump to
     *
     * @throws IOException
     */
    public void extract(String output) throws IOException {
        String      buffer = Text.loadTextFromFileAsString(this.file.getPath());
        Lexer       lexer  = new Lexer(file, buffer, true);
        List<Token> tokens = lexer.getTokens();

        for (Token token : tokens) {
            if (token.getType() == Token.TYPE_STRING_LITERAL) {
                if (token.getData().contains("@") &&!emails.contains(token.getData())) {
                    emails.add(token.getData());
                }
            }
        }

        StringBuffer outputData = new StringBuffer();

        for (String mail : emails) {
            outputData.append(mail).append("\n");
        }

        Text.dumpStringToFile(output, outputData.toString());
        lexer.close();
    }

    /**
     * Method extractGmail
     * Created on 14/08/30
     * Extracts Bluesnap payments from a gmail dump
     * @author Daniel Hadland
     * @param output the output file
     * @throws Exception if there was an error with IO Libaries or any other issues.
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param output
     *
     * @throws Exception
     */
    public void extractBlueSnapFromGmail(String output) throws Exception {
        String   buffer = Text.loadTextFromFileAsString(this.file.getPath());
        String[] data   = buffer.split("\n");

        for (String str : data) {
            if (str.startsWith("Email                :")) {
                String email = str.substring("Email                :".length());

                if (!emails.contains(email)) {
                    emails.add(email);
                }
            }
        }

        StringBuffer outputData = new StringBuffer();

        for (String mail : emails) {
            outputData.append(mail).append("\n");
        }

        Text.dumpStringToFile(output, outputData.toString());
    }
}
