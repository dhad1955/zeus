package net.tazogaming.hydra.util.tools;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.build.lexer.Lexer;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/08/14
 * Time: 23:40
 */
public class EnumCompiler {
    private List<Token>  tokens = new ArrayList<Token>();
    private List<Object> data   = new ArrayList<Object>(10);
    private Lexer        lexer;
    private int          current;

    /**
     * Constructs ...
     *
     *
     * @param file
     */
    public EnumCompiler(String file) {
        this.lexer = new Lexer(new File(file));
    }

    private Token lookAhead(int dir) {
        return this.tokens.get(current + 1);
    }

    private Token next() {
        return this.tokens.get(++current);
    }

    private boolean hasNext() {
        return this.current < this.tokens.size();
    }

    private Map<Integer, Object> matchHashMap() {
        if (lookAhead(1).getType() == Token.TYPE_OPENING_BRACE) {}

        return null;
    }

    /**
     * Method parse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void parse() {
        while (hasNext()) {
            if (lookAhead(1).getType() != Token.TYPE_STRING) {
                throw new RuntimeException("Error unexpected token " + lookAhead(1).getData());
            }

            String type = next().getData();

            if (lookAhead(1).getType() != Token.TYPE_ASSIGNMENT) {
                throw new RuntimeException("Expecting assignment got " + lookAhead(1).getData());
            }

            next();

            if (type.equals("int32")) {
                if (lookAhead(1).getType() != Token.TYPE_NUMBER) {
                    throw new RuntimeException("Error expecting number, got " + lookAhead(1));
                }

                // data.add(new EnumElement(getOpcodeForName(type), new Integer(next().getData())));
            }
        }
    }
}
