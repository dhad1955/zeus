package net.tazogaming.hydra.util.tools;

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

import javax.print.DocFlavor;
import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/09/14
 * Time: 02:28
 */
public class DungDropTableBuilder {

    public static final String[][] TIERS =

    {
        {"novite", "bathus", "marmarous", "salve", "protoleather", "bathus", "wildercress", "subleather", "brightleaf", "paraleather", "kratonite", "roseblood", "archleather"},
        {"katagon", "primal",  "bryll", "dromoleather", "zephyrium", "duskweed", "spinoleather", "argonite", "soulbell", "gallileather", "katagon", "ecto", "stegoleather"},
            {"gorgonite", "megaleather", "promethium", "runic", "spiritbloom", "tyrannoleather", "primal", "celestial", "sagittarian"}

    }                                                                                                                                 ;


    private static boolean ignore(Item item){
        String itemName = item.getName().toLowerCase();
        return
                itemName.contains("void") || itemName.contains("protector") || itemName.contains("(b)") || itemName.contains("arrow") || itemName.contains("(")
                || item.isNote() || item.isStackable() || itemName.contains("bar") || itemName.contains("hide") || itemName.contains("ore")
                || itemName.contains("chaotic")
                || itemName.contains("hatchet") || itemName.contains("pickaxe") || itemName.contains("branches") || itemName.contains("cloth") || itemName.contains("box") || itemName.contains("bag");
    }



    public static void dump() {
        final int START_INDEX = 15753;

        ArrayList[] items = new ArrayList[3];

        for(int i = 0; i < items.length; i++) {
            items[i] = new ArrayList<String>();
        }

        MysqlConnection con = World.getWorld().createDatabaseLink();



        for(int i = START_INDEX; i < 20000; i++){
            Item item = Item.forId(i);
            if(item == null)
                continue;
            if(ignore(item))
                continue;
            for(int x = 0; x < TIERS.length; x++) {
                for(int y = 0; y < TIERS[x].length; y++){
                    if(item.getName().toLowerCase().contains(TIERS[x][y]) && !item.getName().equalsIgnoreCase(TIERS[x][y]))
                            con.executeUpdate("INSERT into npcdrop(tableindex, itemid, amount,rarity, loldrop) VALUES(567, "+item.getIndex()+", 1, "+(x + 1)+", 0)");
                            items[x].add(item.getName()+" - "+item.getIndex());

                }
            }


        }

        for(int i = 0; i < items.length; i++){
            System.err.println("-------------------- tier: "+(i + 1));
            for(String str: (ArrayList<String>)items[i]) {
                System.err.println(str);
            }
        }

    }



}
