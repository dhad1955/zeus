package net.tazogaming.hydra.util.tools;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/08/14
 * Time: 22:18
 */
public class EnumDecompiler {
    private static final String[] TYPES = {
        "char1", "char2", "string", "string", "int32", "string_array", "int32_array"
    };
    public static final int
        TYPE_STRING                     = 3,
        TYPE_INT32                      = 4,
        TYPE_CHAR1                      = 1,
        TYPE_ARRAY_STRING               = 5,
        TYPE_ARRAY_INT32                = 6;
    private Buffer         buffer;
    private BufferedWriter fileWriter;

    /**
     * Constructs ...
     *
     *
     * @param data
     * @param file
     */
    public EnumDecompiler(byte[] data, String file) {
        this.buffer = new Buffer(data);
        buffer.setPtr(0);

        try {
            fileWriter = new BufferedWriter(new FileWriter("enums_decompiled/" + file + ".enum"));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void close() {
        try {
            fileWriter.close();
        } catch (Exception ee) {}
    }

    private void write(String data) {
        try {
            fileWriter.write(data);
        } catch (Exception ee) {}
    }

    /**
     * Method nameForLong
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param commas
     * @param lengthSomething
     * @param data
     * @param bool
     *
     * @return
     */
    static final String nameForLong(int commas, int lengthSomething, long data, boolean bool) {
        try {
            char c    = ',';
            char c_2_ = '.';

            if ((~commas) == -1) {
                c_2_ = ',';
                c    = '.';
            }

            if ((~commas) == -3) {
                c_2_ = '\u00a0';
            }

            boolean bool_3_ = false;

            if (data < 0L) {
                bool_3_ = true;
                data    = -data;
            }

            StringBuffer stringbuffer = new StringBuffer(26);

            if ((~lengthSomething) < -1) {
                for (int i_4_ = 0; i_4_ < lengthSomething; i_4_++) {
                    int i_5_ = (int) data;

                    data /= 10L;
                    stringbuffer.append((char) (-((int) data * 10) + 48 - -i_5_));
                }

                stringbuffer.append(c);
            }

            int i_6_ = 0;

            for (;;) {
                int i_7_ = (int) data;

                data /= 10L;
                stringbuffer.append((char) (i_7_ + (48 - 10 * (int) data)));

                if ((data ^ 0xffffffffffffffffL) == -1L) {
                    break;
                }

                if (bool && (++i_6_ % 3 ^ 0xffffffff) == -1) {
                    stringbuffer.append(c_2_);
                }
            }

            if (bool_3_) {
                stringbuffer.append('-');
            }

            return stringbuffer.reverse().toString();
        } catch (RuntimeException runtimeexception) {
            throw new RuntimeException(runtimeexception.getMessage());
        }
    }

    /**
     * Method extract
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void extract() {
        do {
            while (true) {
                int opcode = buffer.readUnsignedByte();


                if (opcode == 0) {
                    break;
                }

                write(TYPES[opcode] + " = " + extractType(opcode));
                write("\n");
            }
        } while (false);
    }

    private String extractType(int opcode) {
        switch (opcode) {
        case TYPE_STRING :
            return buffer.readString2();

        case TYPE_INT32 :
            return Integer.toString(buffer.readDWord());

        case TYPE_ARRAY_INT32 :
        case TYPE_ARRAY_STRING :
            StringBuilder arrayString = new StringBuilder();

            arrayString.append("[");

            int size = buffer.readShort();

            for (int i = 0; i < size; i++) {
                int    key  = buffer.readDWord();
                String rofl = nameForLong(1, 0, (long) key, false);

                arrayString.append("[" + key + ",");

                if (opcode == TYPE_ARRAY_INT32) {
                    arrayString.append(Integer.toString(buffer.readDWord()));
                } else {
                    arrayString.append("\"").append(buffer.readString2()).append("\"");
                }

                arrayString.append("]");

                if (i != size - 1) {
                    arrayString.append(", ");
                }

                arrayString.append("\n");
            }

            arrayString.append("]");

            return arrayString.toString();

        default :
            return "" + buffer.readSignedByte();
        }
    }
}
