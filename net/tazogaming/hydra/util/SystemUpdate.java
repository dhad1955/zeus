package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.pkleague.PKTournament;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.security.GamblingBanList;
import net.tazogaming.hydra.security.IPFlags;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/12/13
 * Time: 12:02
 */
public class SystemUpdate {

    /** isActive made: 14/10/28 **/
    private static boolean isActive = false;

    /** updateFinishesAt made: 14/10/28 **/
    private static int    updateFinishesAt = -1;
    public static boolean shutdown         = false;

    /** current_task made: 14/10/28 **/
    private static RecurringTickEvent current_task;

    /**
     * Method sendTimer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public static void sendTimer(Player plr) {
        if ((updateFinishesAt == -1) || (getTimeRemaining() == -1)) {
            return;
        }

        plr.getIoSession().write(new MessageBuilder().setId(77).addShort(getTimeRemaining()).toPacket());

//      plr.getIoSession().write(spb.toPacket());
    }

    /**
     * Method setTimer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param minutes
     */
    public static void setTimer(int minutes) {
        if (current_task != null) {
            current_task.terminate();
        }

        if (minutes == -1) {
            updateFinishesAt = -1;

            return;
        }

        isActive         = true;
        updateFinishesAt = Core.currentTime + Core.getTicksForMinutes(minutes);

        for (Player player : World.getWorld().getPlayers()) {
            sendTimer(player);
        }

        current_task = new RecurringTickEvent() {
            @Override
            public void tick() {
                if (SystemUpdate.getTimeRemaining() == 1) {
                    CastleWarsEngine.getSingleton().end();
                    for (Player player : World.getWorld().getPlayers()) {
                        pre_remove(player);
                    }

                    PKTournament.puresLeaderboard.save("config/dailypk.dat");
                    BossTournament.mainsLeaderboard.save("config/boss_lboard.dat");
                    PKTournament.mainsLeaderboard.save("config/mains_lboard.dat");
                    PKTournament.zerkersLeaderboard.save("config/zerkers_lboard.dat");



                    shutdown = true;
                } else if (SystemUpdate.getTimeRemaining() == 0) {
                    while (World.getWorld().getIOPool().getSaver().eventCount() > 0) {
                        Logger.err("event count: " + World.getWorld().getIOPool().getSaver().eventCount());

                        try {
                            Thread.sleep(5000);
                        } catch (Exception ee) {}
                    }

                    World.getWorld().getChannelList().saveChannels();
                    World.getWorld().getServerTaskRegister().notifyShutdown(null);
                    IPFlags.saveFlags();
                    GamblingBanList.save();
                    System.exit(-1);
                }
            }

            // To change body of implemented methods use File | Settings | File Templates.
            @Override
            public boolean terminate() {
                return false;    // To change body of implemented methods use File | Settings | File Templates.
            }
        };
        Core.submitTask(current_task);
    }

    /**
     * Method isActive
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static boolean isActive() {
        return getTimeRemaining() > 0;
    }


    public static boolean isRunning() {
        return isActive;
    }
    // remove players from minigames etc

    /**
     * Method pre_remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void pre_remove(final Player player) {
        player.getActionSender().sendLogout();
        player.setLoggedIn(false);

        if (player.getDungeon() != null) {
            player.getDungeon().destructPlayer(player);
        }

        World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
            @Override
            public boolean compute() {
                synchronized (player) {
                    new UserAccount().build_save_game(player);
                }

                return true;

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void unlink(Player player) {
        World.getWorld().getIOPool().getSaver().addSave(player);
    }

    /**
     * Method getTimeRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static int getTimeRemaining() {
        return updateFinishesAt - Core.currentTime;
    }
}
