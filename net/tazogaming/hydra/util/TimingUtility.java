package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator Enchanta is a Runescape 2 Server emulator which
 * has been designed for educational purposes only Created by Daniel Hadland
 * Date: 30/09/13 Time: 05:04
 */
public class TimingUtility {
	public static final int STOP_FOOD_TIME = 0, POISON_IMMUNE_TIME = 1, FREEZE_IMMUNE_TIME = 2, FROZEN_TIME = 3,
			DRAGON_FIRE_IMMUNE_TIME = 4, ACTION_TIMER = 5, SKULL_TIMER = 6, TELE_BLOCK_TIMER = 7, GOD_SPELL_TIMER = 8,
			YELL_TIMER = 9, SUMMON_TIMER = 10, LEVEL_UP_TIMER = 11, MUTE_TIMER = 12, DOUBLE_XP_TIMER = 13,
			DOUBLE_DROPS_TIMER = 14, DUEL_WAIT_TIMER = 15, POTION_DELAY_TIMER = 16, DOUBLE_XP_TIMER_2 = 17,
			QUEST_TIMER = 18, ACHIEVEMENT_TIMER = 19, MIASMIC_TIMER = 21, OVERLOAD_TIMER = 22, DOUBLE_PK_TIMER = 23,
			LUCKY_TIMER = 24, EATING_COOLDOWN_3 = 25, COMBAT_BLOCK_TIMER = 26, ZOMBIE_PICKUP_TIMER = 27,
			ZOMBIE_POWERUP_TIMER = 28, OVERLOAD_REPLENISH_TIMER = 30, DFS_TIMER = 31; // in
																						// memory
																						// of
																						// nathan
																						// gingerrich
	public static final int DATED_TIMER_MUTE = 0, DATED_TIMER_PIN_RESET = 1, DATED_TIMER_PIN_BLOCK = 2;
	private int[] timers = new int[100];
	private boolean[] saveTimers = new boolean[100];
	private long[] datedTimers = new long[100];

	private int food_prot_time = 0;

	public void addFoodProtTime() {
		this.food_prot_time++;
	}

	public void resetFoodProtTime() {
		this.food_prot_time = 0;
	}

	public int getFoodProtTime() {
		return this.food_prot_time;
	}

	/**
	 * Method remaining Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param time
	 *
	 * @return
	 */
	public int remaining(int time) {
		return timers[time] - Core.currentTime;
	}

	/**
	 * Method setDatedTimerUntilHours Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param hours
	 */
	public void setDatedTimerUntilHours(int id, int hours) {
		datedTimers[id] = System.currentTimeMillis() + (((1000) * 60) * hours);
	}

	/**
	 * Method setDatedTimerUntilMins Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param mins
	 */
	public void setDatedTimerUntilMins(int id, int mins) {
		datedTimers[id] = System.currentTimeMillis() + ((mins * 1000) * 60);
	}

	/**
	 * Method isDated Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public boolean isDated(int id) {
		return getTimeRemaining(id) > 0;
	}

	/**
	 * Method setDatedTimerUntilDays Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param days
	 */
	public void setDatedTimerUntilDays(int id, int days) {
		setDatedTimerUntilHours(id, days * 24);
	}

	/**
	 * Method getMinutesRemaining Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public int getMinutesRemaining(int id) {
		return (getTimeRemaining(id) / 1000) / 60;
	}

	/**
	 * Method getHoursRemaining Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public int getHoursRemaining(int id) {
		return getMinutesRemaining(id);
	}

	/**
	 * Method setTime Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 * @param time
	 */
	public void setTime(int timer, int time) {
		timers[timer] = Core.currentTime + time;
	}

	/**
	 * Method setTime Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 * @param time
	 * @param save
	 */
	public void setTime(int timer, int time, boolean save) {
		timers[timer] = Core.currentTime + time;
		saveTimers[timer] = save;
	}

	/**
	 * Method addTime Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 * @param time
	 */
	public void addTime(int timer, int time) {
		timers[timer] += time;
	}

	/**
	 * Method timerActive Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 *
	 * @return
	 */
	public boolean timerActive(int timer) {
		return timers[timer] > Core.currentTime;
	}

	/**
	 * Method getAbsTime Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 *
	 * @return
	 */
	public int getAbsTime(int i) {
		int time = timers[i] - Core.currentTime;

		if (time < 0) {
			time = 0;
		}

		return time;
	}

	/**
	 * Method updateTimers Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param k
	 */
	public void updateTimers(Killable k) {
		for (int i = 0; i < timers.length; i++) {
			if (timers[i] == Core.currentTime) {
				k.timerUp(i);
			}
		}
	}

	/**
	 * Method resetDated Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void resetDated(int id) {
		datedTimers[id] = 0;
	}

	/**
	 * Method getTimeRemaining Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timerid
	 *
	 * @return
	 */
	public int getTimeRemaining(int timerid) {
		if (datedTimers[timerid] == 0) {
			return 0;
		}

		return (int) (datedTimers[timerid] - System.currentTimeMillis());
	}

	/**
	 * Method resetTimer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void resetTimer(int id) {
		timers[id] = 0;
	}

	/**
	 * Method save Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param data
	 */
	public void save(Buffer data) {
		for (int i = 0; i < timers.length; i++) {
			if (saveTimers[i] && timerActive(i)) {
				data.addByte(i);
				data.writeWord(getAbsTime(i));
			}
		}

		data.addByte(255);
	}

	/**
	 * Method load Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param data
	 */
	public void load(Buffer data) {
		int timer = 0;

		while ((timer = data.readByte()) != 255) {
			setTime(timer, data.readShort(), true);
		}
	}

	/**
	 * Method save_timer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 *
	 * @return
	 */
	public static boolean save_timer(int timer) {
		switch (timer) {
		case SKULL_TIMER:
			return true;
		}

		return false;
	}
}
