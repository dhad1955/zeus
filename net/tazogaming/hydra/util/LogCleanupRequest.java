package net.tazogaming.hydra.util;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/05/14
 * Time: 14:00
 */
public class LogCleanupRequest extends BackgroundServiceRequest {

    // number of days before discarding player log files
    private static final int LOG_CLEANUP_DAYS = 8;

    /**
     * Method compute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean compute() {
        File logFolder = new File("config/logs");
        int  count     = 0;
        long start     = System.currentTimeMillis();

        for (File log_file : logFolder.listFiles()) {
            if ((start - log_file.lastModified()) > (1000 * 60 * 60 * 24 * LOG_CLEANUP_DAYS)) {
                try {
                    if (!log_file.delete()) {
                        Logger.err("Failed to delete log file: " + log_file.getName());
                    } else {
                        count++;
                    }
                } catch (Exception ee) {
                    Logger.err(ee);
                }
            }
        }

        long elapsed = System.currentTimeMillis() - start;

        Logger.err("Log cleanup, cleaned " + count + " player log files in " + elapsed + " ms");

        Buffer file = new Buffer(8);

        file.writeQWord(System.currentTimeMillis());

        try {
            file.save("config/coresettings/cleanup.dat");
        } catch (Exception ee) {}
        return true;
    }
}
