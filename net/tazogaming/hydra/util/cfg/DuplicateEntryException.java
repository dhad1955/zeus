package net.tazogaming.hydra.util.cfg;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/08/14
 * Time: 19:43
 */
public class DuplicateEntryException extends RuntimeException {

    /**
     * Constructs ...
     *
     *
     * @param str
     */
    public DuplicateEntryException(String str) {
        super(str);
    }
}
