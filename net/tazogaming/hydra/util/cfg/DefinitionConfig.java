package net.tazogaming.hydra.util.cfg;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 14:13
 */
import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.build.lexer.Lexer;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.FileNotFoundException;

import java.util.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 14:00
 */
public class DefinitionConfig {
    private static Map<String, String> constants = new HashMap<String, String>();

    static {}

    private HashMap<String, DefinitionGroup> groups = new HashMap<String, DefinitionGroup>();
    private int                                offset = 0;
    private Lexer                              lexer;
    private ArrayList<Token>                   tokens;

    /**
     * Constructs ...
     *
     *
     * @param f
     *
     * @throws FileNotFoundException
     */
    public DefinitionConfig(File f) throws FileNotFoundException {
        if (!f.exists()) {
            throw new FileNotFoundException("Error could not find file: " + f.getPath());
        }

        this.lexer  = new Lexer(f);
        this.tokens = lexer.getTokens();
        parse();
        lexer.close();
    }

    /**
     * Constructs ...
     *
     *
     * @param path
     *
     * @throws FileNotFoundException
     */
    public DefinitionConfig(String path) throws FileNotFoundException {
        this(new File(path));
    }

    private DefinitionGroup getGroupOrCreate(String name) {
        if (!groups.containsKey(name)) {
            groups.put(name, new DefinitionGroup(name));
        }

        return groups.get(name);
    }

    private Token peek(int offset) {
        return tokens.get(this.offset + offset);
    }

    private boolean hasNext() {
        return this.offset < tokens.size();
    }

    private Token next() {
        if (!hasNext()) {
            throw new RuntimeException("Reached end of file whilst parsing");
        }

        return this.tokens.get(offset++);
    }

    private Token current() {
        return tokens.get(offset);
    }

    private String replaceConstants(String str) {
        if (!constants.containsKey(str)) {
            return str;
        }

        return constants.get(str);
    }

    private void matchDefinition(DefinitionGroup group) {
        Token peek = current();

        if ((peek.getType() != Token.TYPE_STRING) && (peek.getType() != Token.TYPE_STRING_LITERAL)
                && (peek.getType() != Token.TYPE_NUMBER)) {
            throw new RuntimeException("Error expecting number, string or string literal at :" + current().position()
                                       + " " + peek.getData());
        }

        Object     key;
        Definition def = null;

        peek = next();

        if (peek.getType() != Token.TYPE_NUMBER) {
            throw new RuntimeException("Error, key must be a number: " + peek.getData() + " " + current().position());
        }

        def = new Definition(Integer.parseInt(peek.getData()));

        if (next().getType() != Token.TYPE_OPENING_BRACE) {
            throw new RuntimeException("Error expecting opening brace at " + current().position() + " got "
                                       + current().getData());
        }

        peek = next();

        while (peek.getType() != Token.CLOSING_BRACE) {
            if (peek.getType() == Token.TYPE_ENDSTATEMENT) {
                continue;    // skip
            }

            if (peek.getType() != Token.TYPE_STRING) {
                throw new RuntimeException("Error expecting string for property name, got " + peek.getData() + " "
                                           + peek.position());
            }

            String property = peek.getData();

            if (next().getType() != Token.TYPE_ASSIGNMENT) {
                throw new RuntimeException("Error expecting assignment operator got " + current().getData() + " at "
                                           + current().position());
            }

            if ((peek.getType() != Token.TYPE_NUMBER) && (peek.getType() != Token.TYPE_STRING)
                    && (peek.getType() != Token.TYPE_STRING_LITERAL)) {
                throw new RuntimeException("Error, expecting number, string or string literal for property " + property
                                           + " " + current().position());
            }

            String str = current().getData();
            Token  t   = null;

            try {
                Integer.parseInt(replaceConstants(str));
                t = new Token(replaceConstants(str), Token.TYPE_NUMBER, current().position());
            } catch (NumberFormatException ee) {
                t = new Token(replaceConstants(str), Token.TYPE_STRING, current().position());
            } catch (Exception ee) {
                ee.printStackTrace();
                System.exit(-1);
            }

            def.set(property, ((t.getType() == Token.TYPE_NUMBER)
                               ? Integer.parseInt(t.getData())
                               : t.getData()));
            next();
            peek = next();
        }

        group.set(def.getValue(), def);
    }

    /**
     * Method parse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void parse() {
        while (hasNext()) {
            Token current = current();

            if ((current.getType() == Token.TYPE_STRING) && current.getData().equalsIgnoreCase("const")) {
                next();

                String constant_name = next().getData();

                if (current().getType() != Token.TYPE_ASSIGNMENT) {
                    throw new RuntimeException("Expecting assignment operator to constant got " + current().getData()
                                               + " " + current.position());
                }

                next();

                String value = next().getData();

                constants.put(constant_name, value);

                continue;
            }

            if (current.getType() == Token.TYPE_STRING) {
                DefinitionGroup group = getGroupOrCreate(current.getData());

                next();
                matchDefinition(group);
            }
        }
    }

    /**
     * Method getGroup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     *
     * @return
     */
    public DefinitionGroup getGroup(String key) {
        return groups.get(key);
    }
}
