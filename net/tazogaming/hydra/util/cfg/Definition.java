package net.tazogaming.hydra.util.cfg;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 14:14
 */
public class Definition {
    private HashMap<String, Object> values = new HashMap<String, Object>();
    private int                     type;

    /**
     * Constructs ...
     *
     *
     * @param value
     */
    public Definition(int value) {
        this.type = value;
    }

    /**
     * Method getValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getValue() {
        return type;
    }

    /**
     * Method hasProperty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param property
     *
     * @return
     */
    public boolean hasProperty(String property) {
        return values.containsKey(property);
    }

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     * @param o
     */
    public void set(String key, Object o) {
        this.values.put(key, o);
    }

    /**
     * Method getInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param value
     *
     * @return
     */
    public int getInt(String value) {
        if (!values.containsKey(value)) {
            throw new RuntimeException("integer not found: " + value);
        }

        try {
            return (Integer) values.get(value);
        } catch (ClassCastException ee) {
            throw new RuntimeException("constant not found: " + values.get(value));
        }
    }

    /**
     * Method getIntArray
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param value
     *
     * @return
     */
    public int[] getIntArray(String value) {
        String str = getString(value);

        if (!str.startsWith("[")) {
            throw new RuntimeException("Error " + value + " is not an int array!");
        }

        String   valueString = str.substring(1, str.length() - 1);
        String[] split       = valueString.split(",");
        int[]    vals        = new int[split.length];

        for (int i = 0; i < split.length; i++) {
            try {
                if (split[i].length() == 0) {
                    vals = new int[0];

                    break;
                }

                vals[i] = Integer.parseInt(split[i].replace(" ", "").replace("\t", "").replace("\\n", ""));
            } catch (NumberFormatException ee) {
                throw new RuntimeException("error value " + split[i] + " is not an integer! at index " + i
                                           + " config id: " + value);
            }
        }

        return vals;
    }

    /**
     * Method getString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     *
     * @return
     */
    public String getString(String key) {
        return (String) values.get(key);
    }
}
