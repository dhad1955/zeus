package net.tazogaming.hydra.util.cfg;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 14:34
 */
public class DefinitionGroup {
    private HashMap<Integer, Definition> configs = new HashMap<Integer, Definition>();
    private String                       name;

    /**
     * Constructs ...
     *
     *
     * @param name
     */
    public DefinitionGroup(String name) {
        this.name = name;
    }

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     * @param def
     */
    public void set(int key, Definition def) {
        if (configs.containsKey(key)) {
            throw new RuntimeException("Error, key already exists: " + key);
        }

        this.configs.put(key, def);
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return configs.size();
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     *
     * @return
     */
    public Definition get(int key) {
        return configs.get(key);
    }

    /**
     * Method asList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Definition> asList() {
        List<Definition> list = new ArrayList<Definition>();

        list.addAll(configs.values());

        return list;
    }
}
