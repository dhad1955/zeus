package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/09/13
 * Time: 23:26
 */
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.RangeUtil;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Represents range ammunition types.
 * @author Emperor
 *
 */
public final class Ammunition {

    /**
     * The ammunition mapping.
     */
    private static final Map<Integer, Ammunition> AMMUNITION = new HashMap<Integer, Ammunition>();
    private int                                   type       = -1;

    /**
     * The ammunition interfaces id.
     */
    private final int itemId;

    /**
     * The start graphics.
     */
    private final int startGraphics;

    /**
     * The start graphics when using Dark bow.
     */
    private final int darkBowGraphics;

    /**
     * The projectile GFX id.
     */
    private final int projectileId;

    /**
     * The poison damage.
     */
    private final int poisonDamage;

    /**
     * Constructs a new {@code Ammunition} object.
     * @param itemId The interfaces id.
     * @param startGraphics The start graphics.
     * @param darkBowGraphics The dark bow start graphics.
     * @param projectileId The projectile GFX id.
     * @param poisonDamage The poison damage the ammunition can do.
     */
    private Ammunition(int itemId, int startGraphics, int darkBowGraphics, int projectileId, int poisonDamage) {
        this.itemId          = itemId;
        this.startGraphics   = startGraphics;
        this.darkBowGraphics = darkBowGraphics;
        this.projectileId    = projectileId;
        this.poisonDamage    = poisonDamage;
        this.type            = getType(itemId);
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static int getType(int id) {
        Item i = Item.forId(id);

        if (id == 4740) {
            return RangeUtil.TYPE_KARIL_BOLT;
        }

        if (i.getName().toLowerCase().contains("bolt")) {
            return RangeUtil.TYPE_BOLT;
        } else if (i.getName().toLowerCase().contains("arrow")) {
            return RangeUtil.TYPE_ARROW;
        } else if (i.getName().toLowerCase().contains("jav")) {
            return RangeUtil.TYPE_JAVELIN;
        } else if (i.getName().toLowerCase().contains("knife")) {
            return RangeUtil.TYPE_KNIFE;
        } else if (i.getName().toLowerCase().contains("dart")) {
            return RangeUtil.TYPE_DART;
        } else if (i.getName().toLowerCase().contains("axe")) {
            return RangeUtil.TYPE_THROWNAXE;
        } else if (i.getName().toLowerCase().contains("chinchompa")) {
            return RangeUtil.TYPE_THROWNAXE;
        }

        return -1;
    }

    /**
     * Loads all the {@code Ammunition} info to the mapping.
     * @return {@code True}.
     */
    public static final boolean initialize() {
        Document doc;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder        builder = factory.newDocumentBuilder();

            doc = builder.parse(new File(Main.getConfigPath() + "/item/ammo.xml"));
        } catch (Throwable e) {
            e.printStackTrace();

            return false;
        }

        NodeList nodeList = doc.getDocumentElement().getChildNodes();
        int      siz      = 0;

        for (short i = 1; i < nodeList.getLength(); i += 2) {
            Node n = nodeList.item(i);

            if (n != null) {
                if (n.getNodeName().equalsIgnoreCase("Ammunition")) {
                    NodeList list            = n.getChildNodes();
                    int      itemId          = 0;
                    int      graphicsId      = 0;
                    int      startGraphics   = -1;
                    int      darkBowGraphics = -1;
                    int      projectileId    = 0;

                    for (int a = 1; a < list.getLength(); a += 2) {
                        Node node = list.item(a);

                        if (node.getNodeName().equalsIgnoreCase("itemId")) {
                            itemId = Integer.parseInt(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("startGraphicsId")) {
                            graphicsId = Integer.parseInt(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("startGraphicsHeight")) {
                            startGraphics = graphicsId;
                        } else if (node.getNodeName().equalsIgnoreCase("darkBowGraphicsId")) {
                            graphicsId = Integer.parseInt(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("darkBowGraphicsHeight")) {
                            darkBowGraphics = graphicsId;
                        } else if (node.getNodeName().equalsIgnoreCase("projectileId")) {
                            projectileId = Integer.parseInt(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("poisonDamage")) {
                            try {
                                siz++;
                                Item.forId(itemId).setAmmoData(new Ammunition(itemId, startGraphics, darkBowGraphics,
                                        projectileId, Integer.parseInt(node.getTextContent())));
                            } catch (Exception ee) {}
                        }
                    }
                }
            }
        }

        Logger.log("[Ammunition data]Loaded " + siz + " ammunition definitions.");

        return true;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Gets an ammunition object from the mapping.
     * @param id The ammo id.
     * @return The ammunition object.
     */
    public static final Ammunition get(int id) {
        return AMMUNITION.get(id);
    }

    /**
     * @return the itemId
     */
    public int getItemId() {
        return itemId;
    }

    /**
     * @return the startGraphics
     */
    public int getStartGraphics() {
        return startGraphics;
    }

    /**
     * @return the darkBowGraphics
     */
    public int getDarkBowGraphics() {
        return darkBowGraphics;
    }

    /**
     * @return the projectileId
     */
    public int getProjectileId() {
        return projectileId;
    }

    /**
     * @return the poisonDamage
     */
    public int getPoisonDamage() {
        return poisonDamage;
    }
}
