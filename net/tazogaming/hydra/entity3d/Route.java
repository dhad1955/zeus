package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;

//~--- JDK imports ------------------------------------------------------------

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class Route {

    // Directions
    public static final int
        NONE      = -1,
        NORTH     = 0,
        NORTHEAST = 2,
        EAST      = 4,
        SOUTHEAST = 6,
        SOUTH     = 8,
        SOUTHWEST = 10,
        WEST      = 12,
        NORTHWEST = 14;

    /** waypoints made: 14/11/03 **/
    private Queue<Point> waypoints = new LinkedBlockingQueue<Point>();

    /** inUseByScript made: 14/11/03 **/
    private boolean inUseByScript = false;

    /** mob made: 14/11/03 **/
    private Mob mob;

    /**
     * Constructs ...
     *
     *
     * @param m
     */
    public Route(Mob m) {
        mob = m;
        resetPath();
    }

    /**
     * Method setInUseByScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setInUseByScript(boolean b) {
        inUseByScript = b;
    }

    /**
     * Method isBeingUsedByScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBeingUsedByScript() {
        return inUseByScript;
    }

    /**
     * Method addPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void addPoint(Point p) {
        if (waypoints.size() > 80) {
            return;
        }

        waypoints.offer(p);
    }

    /**
     * Method isIdle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isIdle() {
        return waypoints.isEmpty();
    }

    /**
     * Method addAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param points
     */
    public void addAll(Queue<Point> points) {
        this.waypoints = points;
    }

    /**
     * Method updatePosition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean updatePosition() {
        if (!waypoints.isEmpty()) {
            setNextPosition();

            return true;
        }

        return false;
    }

    /**
     * Method resetPath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetPath() {
        waypoints.clear();
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return waypoints.size();
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String toString() {
        Iterator<Point> it  = waypoints.iterator();
        StringBuilder   out = new StringBuilder();

        while (it.hasNext()) {
            Point p = it.next();

            out.append("[" + p.getX() + "," + p.getY() + "]");

            if (it.hasNext()) {
                out.append("=>");
            }
        }

        return out.toString();
    }

    /**
     * Method peek
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point peek() {
        Point p = waypoints.peek();
        int   x = getNextCoord(mob.getX(), p.getX());
        int   y = getNextCoord(mob.getY(), p.getY());

        return Point.location(x, y, p.getHeight());
    }

    /**
     * Method setNextPosition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void setNextPosition() {
        Point newPoint = null;
        int   newX     = -1,
              newY     = -1;

        if (!waypoints.isEmpty()) {
            newPoint = waypoints.peek();

            if (newPoint == null) {
                return;
            }

            if (newPoint.equals(mob.getLocation())) {
                waypoints.remove();
                newPoint = waypoints.peek();

                if (newPoint == null) {
                    return;
                }
            }

            newX = getNextCoord(mob.getX(), newPoint.getX());
            newY = getNextCoord(mob.getY(), newPoint.getY());

            if ((newX != -1) && (newY != -1)) {
                Point nextPoint = Point.location(newX, newY, mob.getLocation().getHeight());

                if (nextPoint.equals(mob.getLocation())) {
                    resetPath();

                    return;
                }

                if (!isBeingUsedByScript() && (mob instanceof Player)) {
                    int direction = Movement.getDirectionForWaypoints(mob.getLocation(), nextPoint);

                    if (IfClipped.getMovementStatus(direction, mob.getX(), mob.getY(), mob.getHeight()) == 0) {
                        Player pla = (Player) mob;

                        // Logger.log("noclip detected: "+mob.getX()+" "+mob.getY());
                        if ((pla.getRights() < Player.ADMINISTRATOR) && (pla.getCurrentHouse() == null)
                                && ((Combat.getWildernessLevel(pla) == -1)
                                    || (Combat.getWildernessLevel(pla) > 20))) {
                            pla.getActionSender().sendCloseWalkingFlag();
                            resetPath();

                            return;
                        }
                    }
                }

                mob.setLocation(nextPoint, false);

                if (mob.getHeight() > 0) {
                    if (mob instanceof Player) {
                        Player player = (Player) mob;

                        if (!isBeingUsedByScript()) {
                            Tile t = World.getWorld().getTile(nextPoint);

                            if (t.getWalkTrigger() != null) {
                                World.getWorld().getScriptManager().do_trigger(player, t.getWalkTrigger(),
                                        new ScriptVariableInjector() {
                                    @Override
                                    public void injectVariables(Scope scope) {}
                                });
                                resetPath();

                                return;
                            }
                        }
                    }
                } else if ((mob.getHeight() == 0) &&!isBeingUsedByScript() && (mob instanceof Player)) {
                    Player player = (Player) mob;

                    if ((player.getCurrentHouse() != null)
                            && player.getCurrentHouse().trap_trigger(player, nextPoint)) {
                        resetPath();

                        return;
                    }
                }
            }
        }
    }

    /**
     * Dunno if RS2 directions are the same as RSCs..
     *
     * @param startCoord
     * @param destCoord
     *
     * @return
     */
    public int getNextCoord(int startCoord, int destCoord) {
        if (startCoord == destCoord) {
            return startCoord;
        } else if (startCoord > destCoord) {
            return --startCoord;
        } else if (startCoord < destCoord) {
            return ++startCoord;
        }

        return 0;    // never going to happen.
    }
}
