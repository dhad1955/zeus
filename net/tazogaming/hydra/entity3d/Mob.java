package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.game.cutscene.CutScene;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.map.Directions;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Logger;

/**
 * A mob represents a mobile entity.
 */
public abstract class Mob extends Entity {
    public static final int[][] MOB_SPRITES  = new int[][] {
        { 1, 2, 3 }, { 0, -1, 4 }, { 7, 6, 5 }
    };
    public static final int
        NORTH_WEST                           = 7,
        NORTH                                = 0,
        NORTH_EAST                           = 1,
        EAST                                 = 2,
        WEST                                 = 6,
        SOUTH_WEST                           = 5,
        SOUTH                                = 4,
        SOUTH_EAST                           = 3;
    public static final int[] CLIENT_SPRITES = new int[] {
        1, 2, 4, 7, 6, 5, 3, 0
    };    // stolen from some rs2 server

    /**
     * Method updateSprite
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     *
     * @return
     */
    public static final byte[] DIRECTION_DELTA_X = new byte[] {
        -1, 0, 1, -1, 1, -1, 0, 1
    };
    public static final byte[] DIRECTION_DELTA_Y = new byte[] {
        1, 1, 1, 0, 0, -1, -1, -1
    };

    /** lastSprite, mobSprite, combatLevel made: 14/09/11 */
    private transient int
        lastSprite  = -1,
        mobSprite   = -1,
        combatLevel = 3;

    /** lastKnownDir made: 14/09/11 */
    private int lastKnownDir = -1;

    /** moveDirection made: 14/09/11 */
    private int moveDirection = -1;

    /** nextLocation1 made: 14/09/11 */
    private Point nextLocation1 = null;

    /** nextLocation2 made: 14/09/11 */
    private Point nextLocation2 = null;

    /** isDead made: 14/09/11 */
    private boolean isDead = false;

    /** knownClientLoc made: 14/09/11 */
    private Point knownClientLoc = Point.location(100, 100, 0);

    /** didTeleport made: 14/09/11 */
    private boolean didTeleport = false;

    /** frostWhipTime made: 14/09/11 */
    private int                  frostWhipTime        = 0;
    protected boolean            frost_whip_activated = false;
    int                          step_count           = 0;
    int                          runDirection         = -1;
    int                          walkDirection        = -1;
    int                          update2              = -1,
                                 update1              = -1;
    protected transient Route    route;
    protected transient Viewport viewArea;

    /** cutScene made: 14/09/11 */
    private CutScene cutScene;

    /** lastlocation made: 15/01/12 */
    private Point lastlocation;

    /**
     * Constructs ...
     *
     */
    public Mob() {
        route    = new Route(this);
        viewArea = new Viewport(this);
    }

    protected abstract void onMoved(Point movedFrom, Point movedTo, int direction);

    /**
     * Method setFrostWhipTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     *
     * @return
     */
    public boolean setFrostWhipTime(int time) {
        if (frost_whip_activated) {
            return false;
        }

        frost_whip_activated = true;
        this.frostWhipTime   = Core.currentTime + time;

        return true;
    }

    /**
     * Method frostWhip
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean frostWhip() {
        return Core.currentTime < frostWhipTime;
    }

    /**
     * Method getCutScene
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public CutScene getCutScene() {
        return cutScene;
    }

    /**
     * Method setPathHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void setRoute(Route e) {
        route = e;
    }

    /**
     * Method setCutScene
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cutScene
     */
    public void setCutScene(CutScene cutScene) {
        this.cutScene = cutScene;
    }

    /**
     * Method didTeleport
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean didTeleport() {
        return didTeleport;
    }

    /**
     * Method setDidTeleport
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param didTeleport
     */
    public void setDidTeleport(boolean didTeleport) {
        this.didTeleport = didTeleport;
    }

    /**
     * Method getPendingLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getPendingLocation() {
        return nextLocation1;
    }

    /**
     * Method setKnownClientLoc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void setKnownClientLoc(Point p) {
        this.knownClientLoc = p;
    }

    /**
     * Method getKnownClientLoc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getKnownClientLoc() {
        return knownClientLoc;
    }

    /**
     * Method updatePathLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updatePathLocation() {


        Point nextLocation = null;

        if (nextLocation1 != null) {

            // setLocation(nextLocation1);
            nextLocation  = nextLocation1;
            nextLocation1 = null;
        }

        if (nextLocation2 != null) {
         //   setLocation(nextLocation2);
            nextLocation  = nextLocation2;
            nextLocation2 = null;
        }

        final Point loc = nextLocation;

        if ((knownClientLoc.getX() == 1) && (getLocation() != null)) {
            knownClientLoc = getLocation();
        }

        if ((this instanceof Player) && (nextLocation != null)) {
            ((Player) this).getTickEvents().add(new PlayerTickEvent(((Player) this), true, 2) {
                @Override
                public void doTick(Player owner) {
                    knownClientLoc = loc;
                    terminate();
                }
            });
        }
    }

    /**
     * Method getPathHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Route getRoute() {
        return route;
    }

    /**
     * Method updatePosition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updatePosition() {
        route.updatePosition();
    }

    /**
     * Method resetPath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetPath() {
        route.resetPath();
    }

    /**
     * Method getViewArea
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Viewport getViewArea() {
        return viewArea;
    }

    /**
     * Method setCombatLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param level
     */
    public void setCombatLevel(int level) {
        combatLevel = level;
    }

    /**
     * Method setLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param teleported
     *
     * @return
     */
    public boolean setLocation(Point p, boolean teleported) {
        if (!teleported && (getRunDirection() != -1)) {
            throw new RuntimeException(" " + ((Player) this).getUsername() + " " + Core.currentTime);
        }

        boolean b = false;
        if (!teleported && (location != null)) {
            if (!updateSprite(p)) {
                return false;
            }
        } else {
            b = true;
            this.lastlocation = Point.location(p.getX(), p.getY() + 1, p.getHeight());
        }

        if ((p.getRegionX() == -1) || (p.getRegionY() == -1)) {
            Logger.err("Fatal error, invalid point: " + p);

            for (StackTraceElement e : Thread.currentThread().getStackTrace()) {
                Logger.err(e.toString());
            }

            p = Point.location(3222, 3900, 1);
        }

        if (!teleported) {
            if (nextLocation1 == null) {
                nextLocation1 = p;
            } else {
                nextLocation2 = p;
            }
        } else {
            knownClientLoc = p;
        }


        this.lastMovedLocation1 = lastlocation;
        this.lastlocation = location;
        if(b)
            this.lastlocation = Point.location(p.getX(), p.getY() + 1, p.getHeight());

        setLocation(p);

      if(location != null)
        onMoved(lastlocation, p, Movement.getDirectionForWaypoints(location, p));

        return true;
    }

    /**
     * Method getLastMovedLocation
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getLastMovedLocation() {
        return lastlocation;
    }

    private Point lastMovedLocation1;

    public void setLastMovedLocation1(Point p){
        lastMovedLocation1 = p;
    }

    public Point getLastMovedLocation1(){
        return lastMovedLocation1;
    }


    public void setLastMovedLocation(Point p){
        lastlocation = p;
    }
    /**
     * Method getStepCountEast
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * p
     */
    public int getStepCountEast() {
        return step_count;
    }

    /**
     * Method updateSprite
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @return
     */
    public boolean moveBlocked() {
        return (runDirection != -1) || (update2 == Core.currentTime);
    }

    /**
     * Method updateSprite
     * Created on 15/01/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     *
     * @return
     */
    public boolean updateSprite(Point newLocation) {
        int   dX, dY;
        int   newSprite;
        Point loc = getLocation();

        if (moveBlocked()) {
            throw new RuntimeException("Overflow blockage");
        }

        dX = newLocation.getX() - loc.getX();
        dY = newLocation.getY() - loc.getY();

        if ((dX == 0) && (dY == 0)) {
            return false;
        }

        int dir = Movement.getDirectionForWaypoints(loc, newLocation);

        if ((dX < -1) || (dX > 1) || (dY < -1) || (dY > 1) || ((lastSprite != -1) && (mobSprite != -1))) {
            throw new ArrayIndexOutOfBoundsException("Sprite index values out of range (-1<=i<=1): " + dX + "," + dY);
        }

        if (this.runDirection == -2999) {
            return false;
        }

        if ((walkDirection != -1) && (this.runDirection == -1)) {
            this.update2 = Core.currentTime;
            dX           = newLocation.getX() - getLastLocation().getX();
            dY           = newLocation.getY() - getLastLocation().getY();

            if ((dX == 0) && (dY == 0)) {
                update2 = Core.currentTime;
                setLocation(getLastLocation());
                walkDirection = -1;

                return false;
            }

            Directions.RunningDirection run = Directions.runningDirectionFor(dX, dY);

            if (run == null) {
                runDirection = -2999;
                update2      = Core.currentTime;

                // return false;
                try {
                    walkDirection = Directions.directionFor(dX, dY).intValue();
                } catch (Exception ee) {
                    ee.printStackTrace();
                    this.runDirection = -1;

                    return false;
                }

                return true;
            }

            runDirection       = run.intValue();
            this.walkDirection = -1;

            if (this instanceof NPC) {
                throw new RuntimeException();
            }
        } else {
            if (walkDirection != -1) {
                throw new RuntimeException();
            }

            if (this instanceof NPC) {
                this.walkDirection = Directions.directionFor(dX, dY).npcIntValue();
            } else {
                this.walkDirection = Directions.directionFor(dX, dY).intValue();
            }
        }

        return true;
    }

    /**
     * Method setRunDir
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param idr
     */
    public void setRunDir(int idr) {
        this.runDirection = idr;
    }

    /**
     * Method resetWalk
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetWalk() {
        this.runDirection = walkDirection = -1;
    }

    /**
     * Method moving
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean moving() {
        return (this.runDirection != -1) || (this.walkDirection != -1);
    }

    /**
     * Method getWalkDir
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWalkDir() {
        return this.walkDirection;
    }

    /**
     * Method getRunDirection
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRunDirection() {
        return this.runDirection;
    }

    /**
     * Method getKnownLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getKnownLocation() {
        return getLocation();
    }

    /**
     * Method getLastSprite
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLastSprite() {
        return lastSprite;
    }

    /**
     * Method hasMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasMoved() {
        return walkDirection != -1 || runDirection != -1;
    }

    /**
     * Method resetMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetMoved() {
        resetWalk();
    }

    /**
     * Method getLastKnownDir
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLastKnownDir() {
        return lastKnownDir;
    }

    /**
     * Method setLastKnownDir
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lastKnownDir
     */
    public void setLastKnownDir(int lastKnownDir) {
        this.lastKnownDir = lastKnownDir;
    }
}
