package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.runtime.ServerTaskRegister;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.routefinding.RouteFindingService;
import net.tazogaming.hydra.io.PlayerLoadPool;
import net.tazogaming.hydra.io.PlayerLoadQueue;
import net.tazogaming.hydra.map.*;
import net.tazogaming.hydra.game.EventManager;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.runtime.GameEngine;
import net.tazogaming.hydra.runtime.ObjectManager;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.game.ui.clan.ChannelList;
import net.tazogaming.hydra.util.*;
import net.tazogaming.hydra.util.collections.EntityList;
import net.tazogaming.hydra.util.background.BackgroundService;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public final class World {
    public static final int
        MAX_WIDTH  = 4000,
        MAX_HEIGHT = 12000;

    /** ADVERT_DELAY made: 14/08/18 */
    private static final int ADVERT_DELAY = 600;

    /** worldInstance made: 14/08/18 */
    private static World worldInstance;

    /** players made: 14/08/18 */
    private EntityList<Player> players = new EntityList<Player>();

    /** playersAwaitingLogin made: 14/08/18 */
    private LinkedList<Player> playersAwaitingLogin = new LinkedList<Player>();

    /** fastActionPlayerList made: 14/08/18 */
    private HashMap<Long, Player> fastActionPlayerList = new HashMap<Long, Player>();

    /** activeInstances made: 14/08/18 */
    private ArrayList<Instance> activeInstances = new ArrayList<Instance>();

    /** groundItems made: 14/08/18 */
    private ArrayList<FloorItem> groundItems = new ArrayList<FloorItem>();

    /** npcs made: 14/08/18 */
    private EntityList<NPC> npcs = new EntityList<NPC>(4000);

    /** objectManager made: 14/08/18 */
    private ObjectManager objectManager = new ObjectManager();

    /** tickEvents made: 14/08/18 */
    private ArrayList<WorldTickEvent> tickEvents = new ArrayList<WorldTickEvent>();

    /** scriptManager made: 14/08/18 */
    private ScriptRuntime scriptManager = new ScriptRuntime();

    /** pathService made: 14/08/18 */
    private RouteFindingService pathService = new RouteFindingService();

    /** demand_service made: 14/08/18 */
    private BackgroundService demand_service = new BackgroundService();

    /** channelList made: 14/08/18 */
    private ChannelList channelList = new ChannelList();

    /** lastAdvertisement made: 14/08/18 */
    private int lastAdvertisement = Core.currentTime;

    /** regionManager made: 14/08/18 */
    private RegionManager regionManager;

    /** clientUpdater made: 14/08/18 */
    private Core clientUpdater;

    /** gameEngine made: 14/08/18 */
    private GameEngine gameEngine;

    /** persister made: 14/08/18 */
    private PlayerLoadPool persister;

    /** mainInstance made: 14/08/18 */
    private Instance mainInstance;

    /** mapAssistant made: 14/08/18 */
    private MapFetcher mapAssistant;

    /** messageHandler made: 14/08/18 */
    private PlayerMessagingHandler messageHandler;

    /** database_link made: 14/08/18 */
    private MysqlConnection database_link;

    /** databaseLink made: 14/08/18 */
    private MysqlConnection databaseLink;

    /** serverTaskRegister made: 14/08/18 */
    private ServerTaskRegister serverTaskRegister;

    /**
     * Constructs ...
     *
     */
    private World() {}

    /**
     * Method getServerTaskRegister
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ServerTaskRegister getServerTaskRegister() {
        return serverTaskRegister;
    }

    /**
     * Method getPathService
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RouteFindingService getPathService() {
        return pathService;
    }

    /**
     * Method globalMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param message
     */
    public static void globalMessage(String message) {
        for (Player p : World.getWorld().getPlayers()) {
            p.getActionSender().sendMessage(message);
        }
    }

    /**
     * Method modDebug
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param messages
     */
    public static void modDebug(String... messages) {
        synchronized (World.getWorld().getPlayers()) {
            for (Player player : getWorld().getPlayers()) {
                if (player.getRights() >= Player.MODERATOR) {
                    for (String msg : messages) {
                        player.getActionSender().sendMessage(msg);
                    }
                }
            }
        }
    }

    /**
     * Method getChannelList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ChannelList getChannelList() {
        return channelList;
    }

    /**
     * Method getBackgroundService
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public BackgroundService getBackgroundService() {
        return demand_service;
    }

    /**
     * Method doEvents
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void doEvents() {
        for (Iterator<WorldTickEvent> iter = tickEvents.iterator(); iter.hasNext(); ) {
            WorldTickEvent tickable = iter.next();

            try {
                tickable.update();
            } catch (Exception ee) {
                ee.printStackTrace();
                iter.remove();

                continue;
            }

            if (tickable.isTerminated()) {
                iter.remove();
            }
        }
    }

    /**
     * Method createDatabaseLink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public MysqlConnection createDatabaseLink() {
        Properties dbProperties = Config.getDatabaseInfo();

        return new MysqlConnection(dbProperties.getProperty("DATABASE_HOST"),
                                   dbProperties.getProperty("DATABASE_USER"),
                                   dbProperties.getProperty("DATABASE_PASSWORD"),
                                   dbProperties.getProperty("DATABASE_NAME")).connect();
    }

    /**
     * Method getDatabaseLink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public MysqlConnection getDatabaseLink() {
        Properties dbProperties = Config.getDatabaseInfo();

        if (database_link == null) {
            database_link = new MysqlConnection(dbProperties.getProperty("DATABASE_HOST"),
                    dbProperties.getProperty("DATABASE_USER"), dbProperties.getProperty("DATABASE_PASSWORD"),
                    dbProperties.getProperty("DATABASE_NAME")).connect();
        }

        while (database_link.isClosed()) {
            database_link.connect();
        }

        return database_link;
    }

    /**
     * Method setDatabaseLink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param databaseLink
     */
    public void setDatabaseLink(MysqlConnection databaseLink) {
        this.databaseLink = databaseLink;
    }

    /**
     * Method submitEvent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void submitEvent(WorldTickEvent e) {
        tickEvents.add(e);
    }

    /**
     * Method getObjectManager
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ObjectManager getObjectManager() {
        return objectManager;
    }

    /**
     * Method getScriptManager
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptRuntime getScriptManager() {
        return scriptManager;
    }

    /**
     * Method setScriptManager
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scriptManager
     */
    public void setScriptManager(ScriptRuntime scriptManager) {
        this.scriptManager = scriptManager;
    }

    /**
     * Method registerNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void registerNPC(NPC npc) {
        npcs.add(npc);
    }

    /**
     * Method removeNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void removeNPC(NPC npc) {
        npcs.remove(npc);
    }

    /**
     * Method getNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public EntityList<NPC> getNpcs() {
        return npcs;
    }

    /**
     * Method getMapAssistant
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public MapFetcher getMapAssistant() {
        return mapAssistant;
    }

    /**
     * Method setMapAssistant
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param g
     */
    public void setMapAssistant(MapFetcher g) {
        mapAssistant = g;
    }

    /**
     * Method getFloorItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<FloorItem> getFloorItems() {
        return groundItems;
    }

    /**
     * Method registerFloorItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void registerFloorItem(FloorItem e) {
        groundItems.add(e);
    }

    /**
     * Method getMainInstance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Instance getMainInstance() {
        return mainInstance;
    }

    /**
     * Method getRegionManager
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RegionManager getRegionManager() {
        return regionManager;
    }

    /**
     * Method createInstance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instance
     */
    public void createInstance(Instance instance) {
        activeInstances.add(instance);
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     *
     * @return
     */
    public Player getPlayer(long l) {
        return fastActionPlayerList.get(l);
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param UID
     *
     * @return
     */
    public ArrayList<Player> getPlayers(long UID) {
        ArrayList<Player> possible_players = new ArrayList<Player>();

        for (Player plr : getPlayers()) {
            if (plr.getUID() == UID) {
                possible_players.add(plr);
            }
        }

        return possible_players;
    }

    /**
     * Method isPlayerRegistered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean isPlayerRegistered(Player pla) {
        for (Player check_player : players) {
            if (check_player == pla) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getWorld
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static synchronized World getWorld() {
        return worldInstance;
    }

    /**
     * Method loadWorld
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadWorld() {
        worldInstance               = new World();

        worldInstance.regionManager = new RegionManager();

        PlayerLoadQueue pl = new PlayerLoadQueue();

        pl.setLoader(new UserAccount());


        worldInstance.setPlayerPool(new PlayerLoadPool());

        worldInstance.serverTaskRegister = new ServerTaskRegister();

        new EventManager();

    }

    /**
     * Method setClientUpdater
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param clientUpdater
     */
    public void setClientUpdater(Core clientUpdater) {
        this.clientUpdater = clientUpdater;
    }

    /**
     * Method setGameEngine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param gameEngine
     */
    public void setGameEngine(GameEngine gameEngine) {
        this.gameEngine = gameEngine;
    }

    /**
     * Method getGameEngine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public GameEngine getGameEngine() {
        return gameEngine;
    }

    /**
     * Method getClientUpdater
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Core getClientUpdater() {
        return clientUpdater;
    }

    /**
     * Method getIOPool
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PlayerLoadPool getIOPool() {
        return persister;
    }

    /**
     * Method setPlayerPool
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pl
     */
    public void setPlayerPool(PlayerLoadPool pl) {
        persister = pl;
    }

    /**
     * Method setLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     * @param oldPoint
     * @param newPoint
     */
    public void setLocation(Entity entity, Point oldPoint, Point newPoint) {
        regionManager.setLocation(entity, oldPoint, newPoint);
    }

    /**
     * Method getTile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public Tile getTile(int x, int y, int h) {
        return regionManager.getTile(x, y, h);
    }

    /**
     * Method getTile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     *
     * @return
     */
    public Tile getTile(Point location) {
        return getTile(location.getX(), location.getY(), location.getHeight());
    }

    /**
     * Method registerPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void registerPlayer(Player p) {
        p.setLoggedIn(true);
        players.add(p);
        fastActionPlayerList.put(p.getUsernameHash(), p);
    }

    /**
     * Method getPlayersAwaitingLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Player> getPlayersAwaitingLogin() {
        return playersAwaitingLogin;
    }


    public Player getPlayerByID(int id) {
        for(Player player : getPlayers()) {
            if(player.getId() == id)
                return player;
        }
        return null;
    }
    /**
     * Method addPlayerAwaitingLogin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void addPlayerAwaitingLogin(Player p) {
        synchronized (playersAwaitingLogin) {
            playersAwaitingLogin.add(p);
        }
    }

    /**
     * Method unregisterPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param notify
     */
    public void unregisterPlayer(Player p, boolean notify) {

        p.setLoggedIn(false);
        players.remove(p);
        if(p.getIoSession().isConnected()){
           try {
            p.getIoSession().close();
            p.log("Removed from world");

           }catch (Exception ee) {}
        }
        fastActionPlayerList.remove(p.getUsernameHash());
        setLocation(p, p.getLocation(), null);    // remove player from region
        getIOPool().getSaver().addSave(p);
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public EntityList<Player> getPlayers() {
        return players;
    }

    /**
     * Method countPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int countPlayers() {
        return players.size();
    }

    /**
     * Method hasPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     *
     * @return
     */
    public boolean hasPlayer(Player p) {
        return players.contains(p);
    }
}
