package net.tazogaming.hydra.entity3d.player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 15:44
 */
public class Points {
    public static final int
        ZOMBIE_POINTS        = 0,
        PK_POINTS            = 1,
        DUNGEONEERING_POINTS = 2,
        VOTE_POINTS = 3,
        SLAYER_POINTS        = 4,
        SKILLING_POINTS      = 5,
        PESTCONTROL_POINTS   = 6,
        PK_POINTS_2          = 7,
        MAGEBANK_PTS        = 8,
        BH_POINTS = 9,
        SPIDER_POINTS = 10,
        KNIGHT_POINTS = 11,
        GODWARS_SHOP_POINTS = 12;

    /**
     * Method getNameForId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public static String getNameForId(int name) {
        switch (name) {
        case SKILLING_POINTS :
            return "Skilling";

        case PK_POINTS :
            return "PVP";

        case DUNGEONEERING_POINTS :
            return "Dungeoneering";

        case VOTE_POINTS:
            return "Vote ";

        case SLAYER_POINTS :
            return "Slayer";

        case ZOMBIE_POINTS :
            return "Zombie";

        case PESTCONTROL_POINTS :
            return "Pest-control";

        case PK_POINTS_2 :
            return "PVP";
        case MAGEBANK_PTS:
            return "MageArena points";
            case GODWARS_SHOP_POINTS:
                return "Godwars shop";
            case SPIDER_POINTS:
                return "Spider";
            case KNIGHT_POINTS:
                return "Knight";
        }

        return "Unknown";
    }
}
