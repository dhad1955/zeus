package net.tazogaming.hydra.entity3d.player;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 15:12
 * To change this template use File | Settings | File Templates.
 */
public class TransformNpc {
    private int transformNpcId = -1;
    private int walkAnim       = -1;
    private int standAnim      = -1;

    /**
     * Method isTransformed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTransformed() {
        return transformNpcId != -1;
    }

    /**
     * Method getIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getIndex() {
        return transformNpcId;
    }

    /**
     * Method getWalkAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWalkAnim() {
        return walkAnim;
    }

    /**
     * Method getStandAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStandAnim() {
        return standAnim;
    }

    /**
     * Method transform
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param walkAnim
     * @param standAnim
     */
    public void transform(int id, int walkAnim, int standAnim) {
        this.standAnim      = standAnim;
        this.walkAnim       = walkAnim;
        this.transformNpcId = id;
    }
}
