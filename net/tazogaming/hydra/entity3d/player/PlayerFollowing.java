package net.tazogaming.hydra.entity3d.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.routefinding.RouteFindingResult;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.DynamicClippingMap;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 28/09/13
 * Time: 02:58
 * To change this template use File | Settings | File Templates.
 */
public class PlayerFollowing {
    public static final int
        STANDARD_FOLLOW     = -1,
        COMBAT_BASED_FOLLOW = 2;

    /** movingTo made: 14/10/12 */
    private Point movingTo = null;

    /** nextPeak made: 14/10/12 */
    private Point nextPeak = null;

    /** mode made: 14/10/12 */
    private int mode = -1;

    /** knownFollowLocation made: 14/10/12 */
    private Point knownFollowLocation = null;

    /**
     * Method updateMovement
     * Created on 14/10/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private boolean stopMovement = false;

    /** following made: 14/10/12 */
    private Killable following;

    /** myPlayer made: 14/10/12 */
    private Player myPlayer;

    /** lastMovedFrom made: 14/10/26 */
    private Point lastMovedFrom;

    /** lastMovedTo made: 14/10/26 */
    private Point lastMovedTo;

    /** lastDirection made: 14/10/26 */
    private int lastDirection;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param following
     */
    public PlayerFollowing(Player pla, Killable following) {
        myPlayer       = pla;
        this.following = following;
        myPlayer.getFocus().focus(following);
    }

    /**
     * Method getMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMode() {
        return mode;
    }

    /**
     * Method setMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mode
     */
    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * Method getFollowing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Killable getFollowing() {
        return following;
    }

    /**
     * Method isHouse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isHouse() {
        return (following.getCurrentInstance() instanceof House) && (myPlayer.getCurrentInstance() instanceof House);
    }

    /**
     * Method houseContainsSolid
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public boolean houseContainsSolid(int x, int y, int h) {
        House house = myPlayer.getCurrentHouse();
        Point p     = Point.location(x, y, h);

        return (house.getClippingMap().containsSolid(Point.getLocalX(myPlayer, p), Point.getLocalY(myPlayer, p), h));
    }

    /**
     * Method getFollowDestination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getFollowDestination() {
        Point p = getPrefferedLocation();

        if (!isHouse()) {
            if (!ClippingDecoder.containsSolid(p.getX(), p.getY(), p.getHeight())) {
                return p;
            }
        } else {
            if (houseContainsSolid(p.getX(), p.getY(), p.getHeight())) {
                return p;
            }
        }

        for (int i = 0; i < 8; i++) {
            Point p2 = Movement.getPointForDir(following.getLocation(), i);

            if ((isHouse() &&!houseContainsSolid(p2.getX(), p2.getY(), p2.getHeight()))
                    || (!isHouse() &&!ClippingDecoder.containsSolid(p2.getX(), p2.getY(), p2.getHeight()))) {
                return p2;
            }
        }

        return null;
    }

    /**
     * Method updateFocus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateFocus() {
        int dX = myPlayer.getX() - following.getX();
        int dY = myPlayer.getY() - following.getY();

        if (dX > 1) {
            dX = 1;
        }

        if (dY > 1) {
            dY = 1;
        }

        if (dX < -1) {
            dX = -1;
        }

        if (dY < -1) {
            dY = -1;
        }

        myPlayer.setLastKnownDir(Mob.MOB_SPRITES[dX + 1][dY + 1]);
    }

    /**
     * Method updateMovement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location1
     * @param location2
     *
     * @return
     */
    public static boolean isInline(Point location1, Point location2) {
        int deltaX = location2.getX() - location1.getX();
        int deltaY = location2.getY() - location1.getY();

        if ((deltaX > 1) || (deltaY > 1)) {
            return true;    // ranging
        }

        if (((deltaX == 0) && (deltaY == 0)) || ((deltaX != 0) && (deltaY != 0))) {
            return false;
        }

        return true;
    }

    private void lineUp() {
        if (!myPlayer.moveBlocked()) {
            for (int i = 0; i < 8; i++) {
                Point check = Movement.getPointForDir(myPlayer.getLocation(), i);

                if (isInline(check, following.getLocation())
                        && (getCollisionStatus(i, myPlayer.getX(), myPlayer.getY(), myPlayer.getHeight(), null,
                                               myPlayer) == 1)) {
                    if (myPlayer.moveBlocked()) {
                        return;
                    }

                    myPlayer.setLocation(Movement.getPointForDir(myPlayer.getLocation(), i), false);

                    return;
                } else {}
            }
        } else {}
    }

    private Point getDest() {



        if (following.getLastMovedLocation() == null) {
            return following.getLocation();
        }

        if (Point.equals(following.getLastMovedLocation(), following.getLocation())) {
            return myPlayer.getLocation();
        }

        if (true) {
            return following.getLastMovedLocation();
        }

        if (following.getLastMovedLocation() != null) {
            int dir = getReversedDirection(following);

            if (dir != -1) {
                return Movement.getPointForDir(following.getLocation(), dir);
            }
        }

        return myPlayer.getLocation();
    }

    /**
     * Method updateMovement
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateMovement() {
        updateMovement(null);
    }

    /**
     * Method updateMovement
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dest
     */
    public void updateMovement(Point dest) {
        myPlayer.getRoute().resetPath();

        if (myPlayer.getTimers().timerActive(TimingUtility.FROZEN_TIME) || myPlayer.isActionsDisabled()) {
            return;
        }

        if (myPlayer.isStunned()) {
            return;
        }

        if (myPlayer.moveBlocked()) {
            return;    // can not move.
        }

        if (this.getMode() == COMBAT_BASED_FOLLOW) {


            if(Point.getDistance(myPlayer.getLocation(), getDest()) <= 1 && following.hasMoved())
                return;
            if (Combat.isWithinDistance(myPlayer, following)) {
                if ((Point.getDistance(myPlayer.getLocation(), following.getLocation())) <= 1
                        &&!isInline(myPlayer.getLocation(), following.getLocation()) &&!following.hasMoved()) {
                        this.lineUp();

                    return;
                } else {}

                if (Combat.isMaging(myPlayer) || Combat.isRanging(myPlayer)) {
                    int dist               = Point.getDistance(myPlayer.getLocation(), following.getLocation());
                    int distanceDifference = dist - Combat.getDistance(myPlayer);

                    if (distanceDifference < -2) {
                        return;
                    }
                } else {



                    if (Combat.getDistance(myPlayer) < 1) {
                        return;
                    }

                    return;
                }
            }
        }

        if (myPlayer.moveBlocked()) {
            return;
        }

        Point moveTo = getDest();

        if (Point.equals(moveTo, following.getLocation())) {

           return;
        }

        if (Point.equals(moveTo, myPlayer.getLocation())) {
            return;
        }

        if (myPlayer.getWalkDir() != -1) {
            if ((myPlayer.getCurrentEnergy() <= 1)
                    || (myPlayer.getAccount().getButtonConfig(Account.RUN_STATUS) != 1)) {
                return;
            }

            myPlayer.removeEnergy();
        }

        int direction = Movement.getDirectionForWaypoints(myPlayer.getLocation(), moveTo);
        int distNow   = Point.getDistance(myPlayer.getLocation(), moveTo);

        if (getCollisionStatus(direction, myPlayer.getX(), myPlayer.getY(), myPlayer.getHeight(), following, myPlayer)
                == 0) {
            RouteFindingResult result = World.getWorld().getPathService().getQuickResult(myPlayer, moveTo,
                                            myPlayer.is_con);

            if ((result.getType() == RouteFindingResult.ROUTE_FOUND) && (result.getPoints().size() > 0)) {
                Point p = result.getPoints().poll();

                if ((p.getX() == myPlayer.getX()) && (myPlayer.getY() == p.getY())) {
                    return;
                }

                int dir = Movement.getDirectionForWaypoints(myPlayer.getLocation(), p);

                if (getCollisionStatus(dir, myPlayer.getX(), myPlayer.getY(), myPlayer.getHeight(), following,
                                       myPlayer) == 0) {
                    return;
                }

                myPlayer.setLocation(Movement.getPointForDir(myPlayer.getLocation(), dir), false);
            }

            return;    // do that instead
        }

        if (myPlayer.moveBlocked()) {
            return;
        }

        Point next = Movement.getPointForDir(myPlayer.getLocation(), direction);

        if (Point.equals(following.getLocation(), next)) {
            return;
        }

        myPlayer.setLocation(next, false);
    }

    /**
     * Method getCollisionStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dir
     * @param tileX
     * @param tileY
     * @param h
     * @param following
     * @param myPlayer
     *
     * @return
     */
    public int getCollisionStatus(int dir, int tileX, int tileY, int h, Mob following, Player myPlayer) {
        if (following != null) {
            Point loc = Movement.getPointForDir(Point.location(tileX, tileY, h), dir);

            if ((loc.getX() == following.getX()) && (loc.getY() == following.getY())) {
                return 0;
            }
        }

        if (!isHouse()) {
            if (ClanWar.wallClipped(myPlayer,
                                    Movement.getPointForDir(Point.location(tileX, tileY, myPlayer.getHeight()), dir))) {
                return 0;
            }

            return getMovementStatus(dir, tileX, tileY, h, following);
        }

        int                localX = Point.getLocalX(myPlayer, Point.location(tileX, tileY, h));
        int                localY = Point.getLocalY(myPlayer, Point.location(tileX, tileY, h));
        DynamicClippingMap map    = myPlayer.getCurrentHouse().getClippingMap();

        switch (dir) {
        case Movement.NORTH :
            if (map.blockedNorth(localX, localY, h)) {
                return 0;
            }

            break;

        case Movement.NORTH_WEST :
            if (map.blockedNorthWest(localX, localY, h)) {
                return 0;
            }

            break;

        case Movement.NORTH_EAST :
            if (map.blockedNorthEast(localX, localY, h)) {
                return 0;
            }

            break;

        case Movement.SOUTH :
            if (map.blockedSouth(localX, localY, h)) {
                return 0;
            }

            break;

        case Movement.SOUTH_WEST :
            if (map.blockedSouthWest(localX, localY, h)) {
                return 0;
            }

            break;

        case Movement.SOUTH_EAST :
            if (map.blockedSouthEast(localX, localY, h)) {
                return 0;
            }

            break;

        case Movement.EAST :
            if (map.blockedEast(localX, localY, h)) {
                return 0;
            }

            break;

        case Movement.WEST :
            if (map.blockedWest(localX, localY, h)) {
                return 0;
            }
        }

        return 1;
    }

    /**
     * Method getMovementStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param direction
     * @param tileX
     * @param tileY
     * @param height
     * @param following
     *
     * @return
     */
    public static int getMovementStatus(int direction, int tileX, int tileY, int height, Mob following) {
        Point p = Movement.getPointForDir(Point.location(tileX, tileY, height), direction);

        if (following != null) {
            if ((following.getX() == p.getX()) && (following.getY() == p.getY())) {
                return 0;
            }
        }

        switch (direction) {
        case Movement.NORTH :
            if (ClippingDecoder.blockedNorth(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH :
            if (ClippingDecoder.blockedSouth(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.EAST :
            if (ClippingDecoder.blockedEast(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.WEST :
            if (ClippingDecoder.blockedWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.NORTH_EAST :
            if (ClippingDecoder.blockedNorthEast(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.NORTH_WEST :
            if (ClippingDecoder.blockedNorthWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH_WEST :
            if (ClippingDecoder.blockedSouthWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH_EAST :
            if (ClippingDecoder.blockedSouthEast(tileX, tileY, height)) {
                return 0;
            }

            break;
        }

        return 1;
    }

    /**
     * Method nameForId
     * Created on 14/10/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static String nameForId(int id) {
        switch (id) {
        case Movement.NORTH :
            return "north";

        case Movement.SOUTH :
            return "south";

        case Movement.WEST :
            return "west";

        case Movement.EAST :
            return "east";

        case Movement.SOUTH_EAST :
            return "south-east";

        case Movement.NORTH_EAST :
            return "north-east";

        case Movement.NORTH_WEST :
            return "northwest";

        case Movement.SOUTH_WEST :
            return "south west";
        }

        return "Unknown: " + id;
    }

    private Point getPrefferedLocation(Mob following) {
        return Movement.getPointForDir(following.getLocation(), getReversedDirection(following));
    }

    /**
     * Method getReversedDirection
     * Created on 14/10/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mob
     *
     * @return
     */
    public static int getReversedDirection(Mob mob) {
        if (Point.getDistance(mob.getLastLocation(), mob.getLocation()) > 1) {
            return Movement.NORTH;
        }

        int lastDir = Movement.getDirectionForWaypoints(mob.getLastLocation(), mob.getLocation());

        switch (lastDir) {
        case Movement.NORTH_EAST :
            return Movement.SOUTH_EAST;

        case Movement.NORTH_WEST :
            return Movement.SOUTH_WEST;

        case Movement.EAST :
            return Movement.WEST;

        case Movement.WEST :
            return Movement.EAST;

        case Movement.SOUTH_EAST :
            return Movement.NORTH_EAST;

        case Movement.SOUTH_WEST :
            return Movement.NORTH_WEST;

        case Movement.SOUTH :
            return Movement.NORTH;

        case Movement.NORTH :
            return Movement.SOUTH;
        }

        throw new RuntimeException("error invalid direction: " + lastDir);
    }

    /**
     * Method getPrefferedLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getPrefferedLocation() {
        int dx = 0,
            dy = 0;

        try {
            return getPrefferedLocation(this.following);
        } catch (Exception ignored) {
            return myPlayer.getLocation();
        }
    }
}
