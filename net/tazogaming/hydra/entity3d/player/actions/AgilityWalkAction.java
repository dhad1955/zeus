package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/11/13
 * Time: 08:24
 */
public class AgilityWalkAction extends Action {
    private int     standAnim = -1;
    private Point   destination;
    private boolean running;
    private int     walkingEmote;
    private Scope   wakeupScript;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param destination
     * @param running
     * @param emote
     * @param wakeup_script
     */
    public AgilityWalkAction(Player player, Point destination, boolean running, int emote, Scope wakeup_script) {
        super(-1, player);
        this.running      = running;
        this.walkingEmote = emote;
        this.destination  = destination;
        player.setMovementLock(true);
        player.getAppearance().setChanged(true);
        player.getRoute().resetPath();
        player.getRoute().setInUseByScript(true);
        this.wakeupScript = wakeup_script;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * Method getStandAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStandAnim() {
        return standAnim;
    }

    /**
     * Method setStandAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setStandAnim(int i) {
        standAnim = i;
    }

    /**
     * Method getRenderAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRenderAnim() {
        return walkingEmote;
    }

    @Override
    protected void tick(int time) {
        if (getPlayer().getLocation().equals(destination) && getPlayer().getRoute().isIdle()) {
            getPlayer().getAppearance().setChanged(true);
            terminate();
            getPlayer().setMovementLock(false);

            if (wakeupScript != null) {
                wakeupScript.release(Scope.WAIT_TYPE_MOVEMENT);
            }

            return;
        }

        if (getPlayer().getRoute().isIdle()) {
            getPlayer().getRoute().resetPath();
            getPlayer().getRoute().addPoint(destination);

            return;
        }

        getPlayer().getRoute().updatePosition();

        if (running && (getPlayer().getLastSprite() == -1)) {
            getPlayer().getRoute().updatePosition();
        }
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {
        getPlayer().setMovementLock(false);
        getPlayer().getRoute().setInUseByScript(false);

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
