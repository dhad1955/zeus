package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.entity.CookingEntity;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 22:18
 */
public class CookingAction extends Action {
    private int           amount = 0;
    private CookingEntity curEntity;
    private GameObject    cookingObject;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param entity
     * @param cookingObject
     */
    public CookingAction(Player pla, CookingEntity entity, GameObject cookingObject) {
        super(0, pla);
        curEntity          = entity;
        this.cookingObject = cookingObject;
    }

    @Override
    protected void tick(int time) {
        int c = 1;

        for (int i = 0; i < c; i++) {
            if (getPlayer().getCurStat(7) < curEntity.getLevelrequired()) {
                getPlayer().getActionSender().sendMessage("You need a cooking level of at least "
                        + curEntity.getLevelrequired() + " to cook this.");
                terminate();

                return;
            }

            if (!getPlayer().getInventory().hasItem(curEntity.getRawID(), 1)) {
                getPlayer().getActionSender().sendMessage("You don't have any left");
                terminate();

                return;
            }

            if ((cookingObject != null) && cookingObject.isRemoved()) {
                terminate();

                return;
            }

            if (amount == getActionAmount()) {
                terminate();

                return;
            }

            if (getCurrentTime() % 4 == 0) {
                if (cookingObject == null) {
                    getPlayer().getAnimation().animate(833);
                } else {
                    getPlayer().getAnimation().animate(897);
                }

                if (getCurrentTime() == 0) {
                    return;
                }

                int slot = getPlayer().getInventory().getItemSlot(Item.forId(curEntity.getRawID()));

                if (slot == -1) {
                    return;
                }

                amount++;

                if (!stopBurning() &&!isSuccessfull()) {
                    getPlayer().getInventory().replaceItem(slot, curEntity.getBurnedID());
                    getPlayer().getActionSender().sendMessage("You accidentally burn the "
                            + Item.forId(curEntity.getCookedID()).getName() + " ");

                    return;
                }

                getPlayer().addPoints(Points.SKILLING_POINTS, curEntity.getLevelrequired());
                getPlayer().addXp(7, curEntity.getExpGiven());
                getPlayer().getInventory().replaceItem(slot, curEntity.getCookedID());




                getPlayer().getActionSender().sendMessage("You cook the "
                        + Item.forId(curEntity.getCookedID()).getName() + ".");
            }
        }
    }

    /**
     * Method stopBurning
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean stopBurning() {
        if (getPlayer().isTenth(7)) {
            return true;
        }

        if (getPlayer().getRights() >= Player.DONATOR) {
            return true;
        }

        int dif = curEntity.getLevelrequired() - getPlayer().getCurStat(7);

        return dif <= -11;
    }

    /**
     * Method isSuccessfull
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSuccessfull() {
        double burnBonus = 3.0;

        if (getPlayer().getEquipment().contains(775)) {
            burnBonus = 7.0;
        }

        double       burn_chance = 30.0 - burnBonus;
        final double cook_level  = getPlayer().getCurStat(8);
        final double lev_needed  = curEntity.cookedid;
        final double burn_dec    = burn_chance / lev_needed;
        final double multi_b     = cook_level - lev_needed;

        burn_chance -= multi_b * burn_dec;

        if (cook_level >= 99) {
            burn_chance -= 10;
        }

        final double randNum = Math.random() * 120.0;

        return burn_chance <= randNum;
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
