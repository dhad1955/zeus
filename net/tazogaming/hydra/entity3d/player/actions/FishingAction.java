package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.fishing.Fish;
import net.tazogaming.hydra.game.skill.fishing.FishPool;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.collections.Array;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 05:13
 */
public class FishingAction extends Action {
    private int slot = -1;
    private int pool_id;
    private NPC fishingNPC;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param pool_id
     * @param npc
     * @param slot
     */
    public FishingAction(Player pla, int pool_id, NPC npc, int slot) {
        super(0, pla);
        fishingNPC   = npc;
        this.pool_id = pool_id;
        this.slot    = slot;
    }

    @Override
    protected void tick(int time) {
        Player pla = getPlayer();

        if (!pla.getAccount().hasArray("last_20_fish")) {
            pla.getAccount().addArray("last_20_fish", 20, false);
        }

        if (!FishPool.check_tools(pla, FishPool.POOLS[pool_id].getType())) {
            terminate();

            return;
        }

        if (!FishPool.checkRequirement(pla, pool_id)) {
            terminate();

            return;
        }

        if (pla.getInventory().getFreeSlots(0) == 0) {
            pla.getActionSender().sendMessage("You don't have enough free inventory space.");
            terminate();

            return;
        }

        if (pla.getTimers().timerActive(TimingUtility.ACTION_TIMER)) {
            return;
        }

        if (Point.getDistance(pla.getLocation(), fishingNPC.getLocation()) > 1) {
            terminate();

            return;
        }

        if (getCurrentTime() == 1) {
            pla.getAnimation().animate(FishPool.getAnimForPool(pool_id), 0);
        }

        if (getCurrentTime() % 5 == 0) {
            pla.getTimers().setTime(TimingUtility.ACTION_TIMER, 4);
            pla.getAnimation().animate(FishPool.getAnimForPool(pool_id), 0);

            if (getCurrentTime() == 1) {
                return;
            }

            Fish fish = FishPool.randomFish(pla, pool_id);

            if (fish == null) {
                return;
            }

            int harvestChance = GameMath.getHarvestChance(50, pla.getCurStat(10), fish.getLevel_requirement(), 1.00);

            if (pla.getRandom().nextInt(100) < harvestChance) {
                getPlayer().addAchievementProgress2(Achievement.FISHERMAN, 1);
                getPlayer().addAchievementProgress2(Achievement.FISHERMAN_II, 1);
                getPlayer().addAchievementProgress2(Achievement.PRO_FISHER, 1);
                if(fish.getId() == 359)
                    getPlayer().addAchievementProgress2(Achievement.TUNA_SALAD, 1);

                pla.getInventory().addItem(fish.getId(), pla.isTenth(10)
                        ? 2
                        : 1);
                pla.addXp(10, fish.getExp());

                Array last20 = pla.getAccount().getArray("last_20_fish");

                if (last20.size() >= 20) {
                    last20.removeIndex(0);
                }

                last20.add(fish.getId());

                if (pla.isTenth(10)) {
                    last20.add(fish.getId());
                }


                if (fish.getId() == 319) {

                    // check anchovies achievement
                    int count = 0;

                    for (int i = 0; i < last20.size(); i++) {
                        if (last20.index_of(i) == 319) {
                            count++;
                        }
                    }


                }



                // check shrimp achievement
                pla.getActionSender().sendMessage("You catch a " + Item.forId(fish.getId()).getName());
            }
        }
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {
        getPlayer().getAnimation().animate(65535);
        getPlayer().getFocus().unFocus();
        getPlayer().getAccount().getArray("last_20_fish").clear();
    }
}
