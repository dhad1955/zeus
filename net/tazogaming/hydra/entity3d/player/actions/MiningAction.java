package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.entity.RockEntity;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 02:57
 */
public class MiningAction extends Action {
    public static final int[] gems = {
        1617, 1619, 1621, 1623, 1625, 1627, 1629,
    };

    /*
     *    public static int[] ROCK_ITEMS = {436, 440,
     *  public static int[] ROCK_REQUIREMENTS = {1, 1, 15
     *  public static int[] ROCK_IDS = {2090, 2094, 2093,
     */
    public static int[]                 pickAxeHeads         = {
        480, 482, 484, 486, 488, 490
    };
    public static int                   pickaxeRequirement[] = {
        1, 1, 5, 20, 30, 40, 90,86
    };
    public static final int             anims[]              = {
        625, 626, 627, 628, 629, 624, 12188, 10222
    };
    public static final int[]           pickAxes             = {
        1265, 1267, 1269, 1273, 1271,  1275, 15259, 13661,
    };
    public static final int[]           pickaxeBonus         = {
        0, 0, 1, 2, 3, 4, 5, 6, 7,
    };
    public static ArrayList<RockEntity> rocks                = new ArrayList<RockEntity>();
    private GameObject                  curInteracting;
    private Rock                        curRock;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param interacting
     * @param rock
     */
    public MiningAction(Player pla, GameObject interacting, Rock rock) {
        super(0, pla);
        this.curInteracting = interacting;
        this.curRock        = rock;
    }

    public static enum Rock {
        CLAY(434, 1, 5, 2, new int[] {
            11189, 11190, 11191, 15503, 15504, 15505, 31062, 31063, 31064, 32429, 32430, 32431
        }), COPPER(436, 1, 17.5, 3, new int[] {
            5780, 5779, 5781, 11936, 11937, 11938, 11960, 11961, 11962, 31080, 31081, 31082, 9708,
        }), TIN(438, 1, 17.5, 3, new int[] {
            5776, 5777, 5778, 11933, 11934, 11935, 11957, 11958, 11959, 31077, 31078, 31079
        }), IRON(440, 15, 35, 5, new int[] {
            5773, 5774, 5775, 11954, 11955, 11956, 14856, 14857, 14858, 31071, 31072, 31073, 32441, 32442, 32443, 37307,
            37308, 37309, 9717, 9718, 9719
        }), SILVER(442, 20, 40, 100, new int[] {
            2311, 11165, 11186, 11187, 11188, 11948, 11949, 11950, 15579, 15580, 15581, 31074, 31075, 31076, 32444,
            32445, 32446, 37304, 37305, 37306
        }), GOLD(444, 40, 65, 100, new int[] {
            5768, 5769, 11183, 11184, 11185, 11951, 11952, 11953, 15576, 15577, 15578, 31065, 31066, 31067, 32432,
            32433, 32434, 37310, 37312, 17001, 17002, 17003
        }), COAL(453, 30, 50, 10, new int[] {
            5770, 5771, 5772, 11930, 11931, 11932, 14850, 14851, 14852, 31068, 31069, 31070, 32426, 32427, 32428
        }), MITHRIL(447, 55, 80, 15, new int[] {
            5784, 5786, 11942, 11943, 11944, 11945, 11946, 11947, 14853, 14854, 14855, 31086, 31087, 31088, 32438,
            32439, 32440
        }), ADAMANTITE(449, 70, 95, 25, new int[] {
            5782, 5783, 11939, 11940, 11941, 11963, 11964, 11965, 14862, 14863, 14864, 31083, 31084, 31085, 32435,
            32436, 32437
        }), RUNE(451, 85, 125, 30, new int[] { 14859, 14860, 14861 }), ZEPHYRIUM(17640, 1, 125, 2, new int[] { 49776 });

        private static Map<Integer, Rock> rocks = new HashMap<Integer, Rock>();

        static {
            for (Rock rock : Rock.values()) {
                for (int object : rock.objects) {
                    rocks.put(object, rock);
                }
            }
        }

        private int[]  objects;
        private int    level;
        private int    ore;
        private int    respawnTimer;
        private double experience;

        /**
         * Constructs ...
         *
         *
         * @param ore
         * @param level
         * @param experience
         * @param respawnTimer
         * @param objects
         */
        private Rock(int ore, int level, double experience, int respawnTimer, int[] objects) {
            this.objects      = objects;
            this.level        = level;
            this.experience   = experience;
            this.respawnTimer = respawnTimer;
            this.ore          = ore;
        }

        /**
         * Method forId
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @param object
         *
         * @return
         */
        public static Rock forId(int object) {
            return rocks.get(object);
        }

        /**
         * Method getOreId
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getOreId() {
            return ore;
        }

        /**
         * Method getObjectIds
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int[] getObjectIds() {
            return objects;
        }

        /**
         * Method getRequiredLevel
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getRequiredLevel() {
            return level;
        }

        /**
         * Method getExperience
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public double getExperience() {
            return experience;
        }

        /**
         * Method getRespawnTimer
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getRespawnTimer() {
            return respawnTimer;
        }

        /**
         * Method getRestoreDelay
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getRestoreDelay() {
            return respawnTimer;
        }
    }

    @Override
    protected void tick(int time) {
        int pickaxeId = getPickaxeId(getPlayer());

        if (pickaxeId == -1) {
            getPlayer().getActionSender().sendMessage("You need a pickaxe to mine ore.");
            terminate();

            return;
        }


        if(pickaxeRequirement[pickaxeId] > getPlayer().getCurStat(14)) {
            getPlayer().getActionSender().sendMessage("You need a mining level of atleast "+pickaxeRequirement[pickaxeId]+" to use this pickaxe");
            terminate();
            return;

        }

        if (getPlayer().getInventory().getNextSlot() == -1) {
            getPlayer().getActionSender().sendMessage("You don't have enough free slots.");
            terminate();

            return;
        }

        if (getPlayer().getCurStat(14) < curRock.getRequiredLevel()) {
            getPlayer().getActionSender().sendMessage("You need a mining level of atleast "
                    + curRock.getRequiredLevel() + " to mine this rock.");
            terminate();

            return;
        }

        if (!curInteracting.isDefault()) {
            getPlayer().getActionSender().sendMessage("Somebody else has already mined this rock.");
            terminate();

            return;
        }

        if (getCurrentTime() == 0) {
            getPlayer().getActionSender().sendMessage("You swing your pick at the rock...");
            getPlayer().getAnimation().animate(anims[pickaxeId], 6);

            return;
        }

        if (getCurrentTime() % (9 - pickaxeBonus[pickaxeId]) == 0) {
            int chance = (GameMath.getHarvestChance(80, curRock.getRequiredLevel(), getPlayer().getCurStat(14), 0));

            if (getPlayer().getRandom().nextInt(100) < chance) {
                getPlayer().getActionSender().sendMessage("You mine some rocks..");
                curInteracting.changeIndex(getReplacement(curInteracting.getDefaultId()));
                curInteracting.setLastInteraction(Core.currentTime);
                curInteracting.scheduleRevert(curRock.getRespawnTimer());

                if (getPlayer().isTenth(14) && (GameMath.rand3(1000) < 5)) {
                    int[] rareGems = { 1631, 6571 };
                    int   ran      = GameMath.rand3(rareGems.length);

                    getPlayer().getInventory().addItem(rareGems[ran], 1);
                    getPlayer().getActionSender().sendMessage("You find an extremely rare gem.");
                }


                    if (pickAxes[getPickaxeId(getPlayer())] == 15259) {
                        getPlayer().getInventory().addItem(getReward(curRock.getOreId()), 1);
                        getPlayer().addXp(14, (int) curRock.getExperience());
                        getPlayer().addAchievementProgress2(Achievement.DOUBLE_MINER, 1);
                    }

                    if(curRock.getOreId() == 449)
                        getPlayer().addAchievementProgress2(Achievement.ADAMANT_MINER, 1);
                    if(curRock.getOreId() == 451)
                        getPlayer().addAchievementProgress2(Achievement.RUNE_MINER, 1);






                    getPlayer().addAchievementProgress2(Achievement.MINER_II, 1);
                    getPlayer().addAchievementProgress2(Achievement.MINER_III, 1);
                    getPlayer().addAchievementProgress2(Achievement.MINER, 1);
                    getPlayer().getInventory().addItem(getReward(curRock.getOreId()), 1);
                }

                getPlayer().addXp(14, (int) curRock.getExperience());
                terminate();

        } else {
            getPlayer().getAnimation().animate(anims[pickaxeId], 1);
        }
    }

    /**
     * Method getReward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ore
     *
     * @return
     */
    public int getReward(int ore) {
        if (ore == 909) {
            return gems[GameMath.rand3(gems.length)];
        }

        return ore;
    }

    /**
     * Method getRock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectID
     *
     * @return
     */
    public static Rock getRock(int objectID) {
        if (Rock.rocks.containsKey(objectID)) {
            return Rock.rocks.get(objectID);
        }

        return null;
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {
        getPlayer().getAnimation().animate(65535);
    }

    /**
     * Method getPickaxeId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static int getPickaxeId(Player pla) {
        int id = pla.getEquipment().getId(3);

        if (id != -1) {
            for (int i = 0; i < pickAxes.length; i++) {
                if (id == pickAxes[i]) {
                    return i;
                }
            }
        }

        Item[] inventoryItems = pla.getInventory().getItems();

        for (int i = 0; i < inventoryItems.length; i++) {
            if (inventoryItems[i] != null) {
                for (int k = 0; k < pickAxes.length; k++) {
                    if (inventoryItems[i].getIndex() == pickAxes[k]) {
                        return k;
                    }
                }
            }
        }

        return -1;
    }

    /**
     * Method getReplacement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectID
     *
     * @return
     */
    public static int getReplacement(int objectID) {
        int originalId = objectID;
        int emptyRock  = 0;

        if (originalId == 2311) {
            return 11552;
        }

        // added
        for (int i = 37304; i <= 37312; i++) {
            if ((emptyRock == 11555) || (emptyRock == 0)) {
                emptyRock = 11552;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        /*
         *   Dark brown rocks
         */
        emptyRock = 0;

        for (int i = 11945; i <= 11966; i++) {
            if ((emptyRock == 11558) || (emptyRock == 0)) {
                emptyRock = 11555;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        /*
         *   Very dark brown rocks
         */
        for (int i = 32426; i <= 32446; i++) {
            if ((emptyRock == 33403) || (emptyRock == 0)) {
                emptyRock = 33400;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        /*
         *   Sand-coloured rocks
         */
        emptyRock = 0;

        for (int i = 5779; i <= 5781; i++) {
            if ((emptyRock == 11555) || (emptyRock == 0)) {
                emptyRock = 11552;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        for (int i = 11183; i <= 11191; i++) {
            if ((emptyRock == 11555) || (emptyRock == 0)) {
                emptyRock = 11552;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        /*
         *   Beige coloured rocks
         */
        emptyRock = 0;

        for (int i = 11930; i <= 11944; i++) {
            if ((emptyRock == 11555) || (emptyRock == 0)) {
                emptyRock = 11552;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        /*
         *   Very light coloured sandy rocks
         */
        emptyRock = 0;

        for (int i = 31062; i <= 31088; i++) {
            if ((emptyRock == 31062) || (emptyRock == 0)) {
                emptyRock = 31059;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        /*
         *   Black rocks
         */
        emptyRock = 0;

        for (int i = 14850; i <= 14864; i++) {
            if ((emptyRock == 14835) || (emptyRock == 0)) {
                emptyRock = 14832;
            }

            if (originalId == i) {
                return emptyRock;
            }

            emptyRock++;
        }

        return 11552;
    }
}
