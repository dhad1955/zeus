package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.map.Point;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 15:43
 * To change this template use File | Settings | File Templates.
 */
public class PathMoveAction extends Action {
    private boolean runTo = false;
    private Point   destination;
    private int     walkAnimation;
    private Player  player;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param location
     * @param running
     * @param anim
     */
    public PathMoveAction(Player pla, Point location, boolean running, int anim) {
        super(Action.WALKING_TO_DESTINATION, pla);
        this.destination   = location;
        this.runTo         = running;
        this.walkAnimation = anim;
        this.player        = pla;
    }

    /**
     * Method getWalkAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWalkAnimation() {
        return walkAnimation;
    }

    @Override
    protected void tick(int time) {
        if (player.getLocation() != destination) {
            if (player.getRoute().isIdle()) {
                terminate();

                throw new RuntimeException("Error path is idle?");
            }

            player.getRoute().updatePosition();

            if (runTo) {
                player.getRoute().updatePosition();
            }
        } else {
            terminate();
        }
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {
        player.setActionsDisabled(false);
        player.getRoute().resetPath();
    }
}
