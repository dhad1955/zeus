package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Action;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/02/14
 * Time: 15:19
 */
public class BonesOnAltarAction extends Action {
    private int bones_id   = -1;
    private int experience = -1;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param experience
     * @param player
     */
    public BonesOnAltarAction(int id, int experience, Player player) {
        super(-1, player);
        bones_id        = id;
        this.experience = experience;
    }

    @Override
    protected void tick(int time) {
        if ((getPlayer().getCurrentHouse() == null) || getPlayer().getCurrentHouse().isBuildingMode()) {
            terminate();

            return;
        }

        if (!getPlayer().getInventory().hasItem(bones_id, 1)) {
            terminate();

            return;
        }

        if (time % 4 == 0) {
            getPlayer().getAnimation().animate(1651);
            getPlayer().getInventory().deleteItem(bones_id, 1);
            getPlayer().addXp(5, experience);

            for (Player player : getPlayer().getWatchedPlayers().getAllEntities()) {
                player.getActionSender().sendStillGFX(624, player.getX() + 1, player.getY());
            }

            getPlayer().getActionSender().sendStillGFX(624, getPlayer().getX() + 1, getPlayer().getY());
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
