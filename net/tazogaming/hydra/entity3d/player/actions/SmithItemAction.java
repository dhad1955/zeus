package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.game.skill.smithing.SmithingUtils;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/10/13
 * Time: 04:19
 */
public class SmithItemAction extends Action {
    int         amt = 0;
    private int barId, barAmount, level, experience, made;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param barId
     * @param barAmount
     * @param levelRequirement
     * @param experience
     * @param made
     */
    public SmithItemAction(Player player, int barId, int barAmount, int levelRequirement, int experience, int made) {
        super(-1, player);
        this.barId      = barId;
        this.barAmount  = barAmount;
        this.level      = levelRequirement;
        this.made       = made;
        this.experience = experience;
    }

    /**
     * Method setAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     */
    public void setAmount(int amt) {
        this.setActionAmount(amt);
    }

    @Override
    protected void tick(int time) {
        getPlayer().getGameFrame().close();

        if (!getPlayer().getInventory().hasItem(barId, barAmount)) {
            getPlayer().getActionSender().sendMessage("You don't have enough bars.");
            terminate();

            return;
        }

        if (getPlayer().getCurStat(13) < level) {
            getPlayer().getActionSender().sendMessage("You need a smithing level of atleast " + level
                    + " to make this.");
            terminate();

            return;
        }

        if (getCurrentTime() % 2 == 0) {
            getPlayer().getAnimation().animate(898);
        }

        if ((getCurrentTime() % 3 == 0) && (getCurrentTime() != 0)) {
            getPlayer().getInventory().deleteItem(barId, barAmount);

            int amt = 1;

            if (Item.forId(made).getName().contains("tips") || Item.forId(made).getName().toLowerCase().contains("nails") || Item.forId(made).getName().toLowerCase().contains("knives") || Item.forId(made).getName().toLowerCase().contains("knife") || Item.forId(made).getName().contains("bolt")) {
                amt = 15;
            }

            getPlayer().getInventory().addItem(made, amt);

            int amount = 1;

            getPlayer().getPlayer().addXp(13, experience);

            if (amt == getActionAmount()) {
                terminate();
                SmithingUtils.itemOnObjectInteraction(getPlayer(), barId, -2);
            }
        }
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {
        getPlayer().getAnimation().animate(65535);

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
