package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.Sounds;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;

import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.entity.Tree;
import net.tazogaming.hydra.game.skill.farming2.PlayerGrew;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 00:59
 */
public class WoodcuttingAction extends Action {
    private static ArrayList<Tree> trees             = new ArrayList<Tree>();
    public static final int        AXES[]            = {
        1351, 1349, 1353, 1361, 1355, 1357, 1359, 6739, 13661,
    };
    public static final int        AXE_BREAK[]       = {
        508, 510, 512, 514, 518, 520, 6743, 6743
    };
    public static final int        AXE_ANIM[]        = {
        879, 877, 875, 873, 876, 871, 869, 2846, 10251,
    };
    public static final double     AXE_BONUS[]       = {
        1.01, 1.03, 1.05, 1.10, 1.10, 1.15, 1.20, 1.25, 2.80
    };
    public static final int        AXE_REQUIREMENT[] = {
        1, 1, 6, 6, 21, 31, 41, 61, 99
    };
    public static final int[][]    FIREMAKING_CONFIG = {
        { 1511, 1, 40 }, { 2862, 1, 40 }, { 1521, 15, 40 }, { 1519, 30, 90 }, { 6333, 35, 105 }, { 10810, 42, 125 },
        { 6332, 50, 314 }, { 1515, 60, 404 }, { 1513, 75, 303 }
    };

    /*
     *  13.2 Sec        12.2 Sec        11.2 Sec        10.2 Sec        9.2 Sec         8.2 Sec
     * Willow  13.2 Sec        12.2 Sec        11.2 Sec        10.2 Sec        9.2 Sec         8.2 Sec
     * Maple   60 Sec  55 Sec  50 Sec  45 Sec  40 Sec  35 Sec
     * Yew     97.5 Sec        90 Sec  82.5 Sec        75 Sec  67.5 Sec        60 Sec
     * Magic   190 S
     */
    static {
        Tree normalTree = new Tree();

        normalTree.setLogs(1511);
        normalTree.setRequirement(1);
        normalTree.setActualObjectId(1276, 1278, 1277, 14309, 14308);
        normalTree.setStumpId(1342);
        normalTree.setExp(25);
        normalTree.setRespawnTicks(Core.getTicksForSeconds(13));
        trees.add(normalTree);

        Tree oakTree = new Tree();

        oakTree.setLogs(1521);
        oakTree.setRequirement(15);
        oakTree.setActualObjectId(1281, 8467, 46724);
        oakTree.setExp(37);
        oakTree.setStumpId(1349);
        oakTree.setRespawnTicks(Core.getTicksForSeconds(13));
        trees.add(oakTree);

        Tree willowTree = new Tree();

        willowTree.setLogs(1519);
        willowTree.setRequirement(30);
        willowTree.setActualObjectId(1308, 5552, 5551, 8488, 5553, 5552);
        willowTree.setExp(67);
        willowTree.setRespawnTicks(Core.getTicksForSeconds(13));
        willowTree.setStumpId(7399);
        trees.add(willowTree);

        Tree mapleTree = new Tree();

        mapleTree.setLogs(1517);
        mapleTree.setRequirement(45);
        mapleTree.setActualObjectId(1307,4674 );
        mapleTree.setExp(100);
        mapleTree.setRespawnTicks(Core.getTicksForSeconds(30));
        mapleTree.setStumpId(7400);
        trees.add(mapleTree);

        Tree yewTree = new Tree();

        yewTree.setLogs(1515);
        yewTree.setRequirement(60);
        yewTree.setActualObjectId(1309);
        yewTree.setExp(175);
        yewTree.setStumpId(7401);
        yewTree.setRespawnTicks(Core.getTicksForSeconds(60));
        trees.add(yewTree);

        Tree magicTree = new Tree();

        magicTree.setLogs(1513);
        magicTree.setRequirement(75);
        magicTree.setActualObjectId(1306, 8409);
        magicTree.setExp(250);
        magicTree.setStumpId(7401);
        magicTree.setRespawnTicks(Core.getTicksForSeconds(100));
        trees.add(magicTree);

        Tree teakTree = new Tree();

        teakTree.setLogs(6333);
        teakTree.setRequirement(85);
        teakTree.setActualObjectId(9036);
        teakTree.setExp(211);
        teakTree.setStumpId(9037);
        teakTree.setRespawnTicks(Core.getTicksForSeconds(20));
        trees.add(teakTree);

        Tree mahogTree = new Tree();

        mahogTree.setLogs(6332);
        mahogTree.setRequirement(85);
        mahogTree.setActualObjectId(9034);
        mahogTree.setExp(260);
        mahogTree.setStumpId(9035);
        mahogTree.setRespawnTicks(Core.getTicksForSeconds(20));
        trees.add(mahogTree);
    }

    private GameObject interactingWith;
    private Tree       currentTree;
    private PlayerGrew grewTree;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param theTree
     */
    public WoodcuttingAction(Player pla, PlayerGrew theTree) {
        super(INTERACTING_WITH_OBJECT, pla);
        this.currentTree = getTreeForLog(theTree.getGrew().getHarvest());

        if (this.currentTree == null) {
            throw new RuntimeException("Invalid tree: " + theTree.getGrew().getHarvest());
        }

        this.grewTree = theTree;
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param interactingObject
     * @param tree
     */
    public WoodcuttingAction(Player pla, GameObject interactingObject, Tree tree) {
        super(Action.INTERACTING_WITH_OBJECT, pla);
        this.interactingWith = interactingObject;
        this.currentTree     = tree;

        if ((tree.getLogs() == 1511) && (interactingObject.getHarvestAmount() > 1)) {
            interactingObject.setHarvestAmount((byte) 1);
        }
    }

    /**
     * Method getFiremakeID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static final int getFiremakeID(int id) {
        for (int i = 0; i < FIREMAKING_CONFIG.length; i++) {
            if (FIREMAKING_CONFIG[i][0] == id) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getTaskGroup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */

    /**
     * Method getTreeForLog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param log
     *
     * @return
     */
    public static Tree getTreeForLog(int log) {
        for (Tree t : trees) {
            if (t.getLogs() == log) {
                return t;
            }
        }

        return null;
    }

    /**
     * Method getTree
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectId
     *
     * @return
     */
    public static Tree getTree(int objectId) {
        for (Tree t : trees) {
            for (int i = 0; i < t.getActualObjectId().length; i++) {
                if (t.getActualObjectId()[i] == objectId) {
                    return t;
                }
            }
        }

        return null;
    }

    @Override
    protected void tick(int time) {
        int axeId = getAxeId(getPlayer());

        if (axeId == -1) {
            getPlayer().getActionSender().sendMessage("You need an axe to cut this down.");
            terminate();

            return;
        }

        if (AXE_REQUIREMENT[axeId] > getPlayer().getCurStat(8)) {
            getPlayer().getActionSender().sendMessage("You need a woodcutting level of atleast "
                    + AXE_REQUIREMENT[axeId] + " to use this axe.");
            terminate();

            return;
        }

        if (getPlayer().getCurStat(8) < currentTree.getRequirement()) {
            getPlayer().getActionSender().sendMessage("You need a Woodcutting level of atleast "
                    + currentTree.getRequirement() + " to chop this down.");
            terminate();

            return;
        }


        if ((grewTree == null) && (Point.getDistance(interactingWith.getLocation(), getPlayer().getLocation()) > 3)) {
            terminate();

            return;
        } else if (grewTree != null) {
            if (Point.getDistance(grewTree.getPatch().getX(), grewTree.getPatch().getY(), getPlayer().getX(),
                                  getPlayer().getY()) > 5) {
                terminate();

                return;
            }
        }

        if (getPlayer().getInventory().getFreeSlots(-1) == 0) {
            getPlayer().getActionSender().sendMessage("You don't have enough free inventory slots.");
            terminate();

            return;
        }

        if (getPlayer().getWindowManager().hasWindowOpen()) {
            getPlayer().getWindowManager().closeWindow();
        }

        if ((grewTree == null) &&!interactingWith.isDefault()) {
            terminate();

            return;
        }

        if (grewTree == null) {
            interactingWith.setLastInteraction(Core.currentTime);
        }

        if (getCurrentTime() == 0) {
            getPlayer().getAnimation().animate(AXE_ANIM[getAxeId(getPlayer())]);
            getPlayer().getActionSender().sendMessage("You swing your axe at the tree... ");

            return;
        }

        if ((getCurrentTime() % 4 == 0) && (getCurrentTime() != 0)) {
            getPlayer().getWindowManager().closeWindow();
            getPlayer().getAnimation().animate(AXE_ANIM[getAxeId(getPlayer())]);

            int playerLevel   = getPlayer().getCurStat(8);
            int harvestChance = GameMath.getHarvestChance(36, playerLevel, currentTree.getRequirement(),
                                    AXE_BONUS[getAxeId(getPlayer())]);

            if (getPlayer().getRandom().nextInt(100) < harvestChance) {
                getPlayer().getActionSender().sendMessage("You chop down some logs");

                getPlayer().addAchievementProgress2(Achievement.LUMBERJACK, 1);
                getPlayer().addAchievementProgress2(Achievement.LUMBERJACK_II, 1);
                getPlayer().addAchievementProgress2(Achievement.LUMBERJACK_III, 1);

                if(currentTree.getLogs() == 1513)
                    getPlayer().addAchievementProgress2(Achievement.MAGIC_WOOD, 1);
                if(currentTree.getLogs() == 6332)
                    getPlayer().addAchievementProgress2(Achievement.MAHOGANIST, 1);



                    if ((getFiremakeID(currentTree.getLogs()) > -1) && (getAxeId(getPlayer()) + 1 == AXES.length)
                            && (GameMath.rand(100) < 30)) {
                        int[] info = FIREMAKING_CONFIG[getFiremakeID(currentTree.getLogs())];

                        if (getPlayer().getCurStat(11) >= info[1]) {
                            getPlayer().addXp(11, info[2]);
                            getPlayer().getActionSender().sendMessage(
                                "Your inferno adze automatically burns the logs.");
                        } else {
                            getPlayer().getInventory().addItem(currentTree.getLogs(), 1);
                        }
                    } else {
                        getPlayer().getInventory().addItem(currentTree.getLogs(), 1);

                        if (getPlayer().isTenth(8)) {
                            getPlayer().getInventory().addItem(currentTree.getLogs(), 1);
                        }
                    }


                if (grewTree == null) {
                    boolean b = getPlayer().isTenth(8) && (Core.currentTime % 2 == 0);

                    if (!b) {
                        interactingWith.setHarvestAmount((byte) (interactingWith.getHarvestAmount() - 1));
                    }
                } else {
                    if (grewTree.chopLog(getPlayer())) {
                        terminate();

                        return;
                    }
                }

                getPlayer().addXp(8, currentTree.getExp());
                getPlayer().addPoints(Points.SKILLING_POINTS, currentTree.getRequirement() / 3);

                if ((grewTree == null) && (interactingWith.getHarvestAmount() <= 0)) {
                    interactingWith.changeIndex(currentTree.getStump());
                    Sounds.playAreaSound(interactingWith.getLocation(), 2734);
                    interactingWith.scheduleRevert(currentTree.getRespawnTicks());

                    if (currentTree.getLogs() != 1511) {
                        interactingWith.setHarvestAmount((byte) GameMath.rand(80));
                    } else {
                        interactingWith.setHarvestAmount((byte) 1);
                    }

                    terminate();
                }
            } else {}
        }
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {

        // To change body of implemented methods use File | Settings | File Templates.
        getPlayer().getAnimation().animate(65535);
    }

    /**
     * Method getAxeLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static int getAxeLocation(Player pla) {
        int id = pla.getEquipment().getId(3);

        if (id != -1) {
            for (int i = 0; i < AXES.length; i++) {
                if (id == AXES[i]) {
                    return 1;
                }
            }
        }

        Item[] inventoryItems = pla.getInventory().getItems();

        for (int i = 0; i < inventoryItems.length; i++) {
            if (inventoryItems[i] != null) {
                for (int k = 0; k < AXES.length; k++) {
                    if (inventoryItems[i].getIndex() == AXES[k]) {
                        return 2;
                    }
                }
            }
        }

        return -1;
    }

    /**
     * Method getAxeId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static int getAxeId(Player pla) {
        if (pla.getEquipment() == null) {
            throw new NullPointerException("Equipment null?");
        }

        int id = pla.getEquipment().getId(3);

        if (id != -1) {
            for (int i = 0; i < AXES.length; i++) {
                if (id == AXES[i]) {
                    return i;
                }
            }
        }

        Item[] inventoryItems = pla.getInventory().getItems();

        for (int i = 0; i < inventoryItems.length; i++) {
            if (inventoryItems[i] != null) {
                for (int k = 0; k < AXES.length; k++) {
                    if (inventoryItems[i].getIndex() == AXES[k]) {
                        return k;
                    }
                }
            }
        }

        return -1;
    }
}
