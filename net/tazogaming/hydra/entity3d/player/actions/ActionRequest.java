package net.tazogaming.hydra.entity3d.player.actions;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.DegradeableItem;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;
import net.tazogaming.hydra.entity3d.object.DwarfMultiCannon;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.GraveStone;
import net.tazogaming.hydra.entity3d.player.PlayerFollowing;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.entity3d.projectile.TelegrabImpactEvent;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.game.LootTable;
import net.tazogaming.hydra.game.Sounds;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.game.minigame.agility.AgilityArena;
import net.tazogaming.hydra.game.minigame.barrows.BarrowsGame;
import net.tazogaming.hydra.game.minigame.castlewars.Barricade;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRequest;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.minigame.pc.PCGame;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.game.minigame.zombiesx.ZombieGroundItem;
import net.tazogaming.hydra.game.minigame.zombiesx.Zombies;
import net.tazogaming.hydra.game.skill.combat.AttackStyle;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.construction.costumeroom.CostumeRuleTable;
import net.tazogaming.hydra.game.skill.construction.costumeroom.RoomBox;
import net.tazogaming.hydra.game.skill.construction.obj.ObjectNode;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.dungeoneering.DungeoneeringStaticItem;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorHandler;
import net.tazogaming.hydra.game.skill.entity.CookingEntity;
import net.tazogaming.hydra.game.skill.entity.Tree;
import net.tazogaming.hydra.game.skill.farming2.Farming;
import net.tazogaming.hydra.game.skill.farming2.PatchDefinition;
import net.tazogaming.hydra.game.skill.farming2.SubPatches;
import net.tazogaming.hydra.game.skill.fishing.FishingAssignments;
import net.tazogaming.hydra.game.skill.hunter.HuntingController;
import net.tazogaming.hydra.game.skill.smithing.SmithingUtils;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.game.ui.Trade;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.security.GamblingBanList;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 22:09
 * To change this template use File | Settings | File Templates.
 */
public class ActionRequest {
    public static Entity    DEFAULT_ENTITY = new Entity();
    public static final int
        TYPE_FLOOR_ITEM_PICKUP             = 0,
        TYPE_NPC_ATTACK                    = 1,
        TYPE_PLAYER_ATTACK                 = 2,
        TYPE_MAGE_ON                       = 3,
        TYPE_TRADE_REQUEST                 = 4,
        TYPE_OBJECT_REQUEST                = 5,
        TYPE_NPC_ACTION_OTHER              = 6,
        TYPE_USE_ITEM_ON_OBJECT            = 7,
        TYPE_USE_ITEM_ON_NPC               = 8,
        TYPE_USE_ITEM_ON_PLAYER            = 9,
        TYPE_FLOOR_ITEM_MAGE               = 10,
        TYPE_DUEL_REQUEST                  = 11;
    public static final long OBJ_VAR       = Text.longForName("obj");

    /** requestType made: 14/08/22 */
    private int requestType = -1;

    /** distance made: 14/08/22 */
    private int distance = -1;

    /** parameterOffset made: 14/08/22 */
    private int parameterOffset = 0;

    /** player made: 14/08/22 */
    private Player player;

    /** parameters made: 14/08/22 */
    private int[] parameters;

    /** requestedEntity made: 14/08/22 */
    private Entity requestedEntity;

    /** objectLengthX, getObjectLengthY made: 14/08/22 */
    private int objectLengthX, getObjectLengthY;

    /** requestLocation made: 14/08/22 */
    private Point requestLocation;

    /**
     * Constructs ...
     *
     *
     * @param pla
     */
    public ActionRequest(Player pla) {
        this.player = pla;
    }

    /**
     * Method setObjectLengths
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     */
    public void setObjectLengths(int x, int y) {
        this.getObjectLengthY = x;
        this.objectLengthX    = y;
    }

    /**
     * Method setRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param distance
     * @param ent
     * @param paramters
     */
    public void setRequest(int type, int distance, Entity ent, int... paramters) {
        if ((requestType == TYPE_PLAYER_ATTACK) && (type != requestType)) {
            if (player.getCombatAdapter().isInCombat()) {
                player.getCombatAdapter().terminate();
                player.unFollow();
            }
        }

        this.parameters      = paramters;
        this.requestType     = type;
        this.distance        = distance;
        this.requestedEntity = ent;
    }

    /**
     * Method isCloseToEntity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isCloseToEntity() {
        return isOpen() && isWithinDistance();
    }

    /**
     * Method isOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isOpen() {
        return this.requestType != -1;
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void close() {
        requestType          = -1;
        parameterOffset      = 0;
        this.requestLocation = null;
    }

    /**
     * Method nextToken
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int nextToken() {
        return parameters[parameterOffset++];
    }

    /**
     * Method isWithinDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isWithinDistance() {
        if ((this.requestType == TYPE_NPC_ATTACK) || (requestType == TYPE_PLAYER_ATTACK)
                || (requestType == TYPE_MAGE_ON)) {
            return (Combat.isWithinDistance(player, (Killable) requestedEntity));
        }

        if (this.requestType == TYPE_FLOOR_ITEM_PICKUP) {
            return ((player.getLocation().getX() == this.requestedEntity.getX())
                    && (player.getLocation().getY() == this.requestedEntity.getY()));
        }

        if ((this.requestType == TYPE_USE_ITEM_ON_PLAYER) || (this.requestType == TYPE_DUEL_REQUEST)
                || (this.requestType == TYPE_TRADE_REQUEST)) {
            return (Point.getDistance(player.getX(), player.getY(), requestedEntity.getX(), requestedEntity.getY())
                    <= 1);
        }

        if (this.requestType == TYPE_NPC_ACTION_OTHER) {
            return ((NPC) (requestedEntity)).isWithinDistance(player, 1);
        }

        if ((this.requestType == TYPE_OBJECT_REQUEST) || (this.requestType == TYPE_USE_ITEM_ON_OBJECT)) {
            if (this.requestLocation == null) {
                return false;
            }

            int x     = player.getKnownClientLoc().getX();
            int y     = player.getKnownClientLoc().getY();
            int patch = World.getWorld().getTile(requestedEntity.getX(), requestedEntity.getY(),
                            player.getHeight()).getPatchID();

            if (patch != -1) {
                if (PatchDefinition.forID(patch).getType() == PatchDefinition.ALLOTMENT) {
                    ArrayList<Point> point = SubPatches.sub_patches[PatchDefinition.forID(patch).getSubPatch()];

                    for (Point p : point) {
                        if (Point.getDistance(p, player.getKnownClientLoc()) <= 1) {
                            return true;
                        }
                    }
                }
            }

            return (x == requestLocation.getX()) && (y == requestLocation.getY());
        }

        return Point.getDistance(requestedEntity.getX(), requestedEntity.getY(), player.getKnownClientLoc().getX(),
                                 player.getKnownClientLoc().getY()) <= distance;
    }

    /**
     * Method moveCloser
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void moveCloser() {
        int dir = Movement.getDirectionForWaypoints(player.getLocation(), requestedEntity.getLocation());

        if (PlayerFollowing.getMovementStatus(dir, player.getX(), player.getY(), player.getHeight(),
                (Mob) requestedEntity) == 1) {
            Point p = Movement.getPointForDir(player.getLocation(), dir, 1);

            if (p != requestedEntity.getLocation()) {
                player.getRoute().addPoint(p);
            }
        }
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        if (this.player.isActionsDisabled()) {
            return;
        }

        if (this.requestType != -1) {
            if ((this.requestedEntity.getLocation() == null)
                    || (this.requestedEntity.getLocation().getHeight() != this.player.getLocation().getHeight())) {
                close();

                return;
            }

            if (isWithinDistance()) {
                int save = requestType;

                player.getRoute().resetPath();

                if (this.requestType != TYPE_PLAYER_ATTACK) {
                    close();
                }

                switch (save) {
                case TYPE_USE_ITEM_ON_NPC :
                    final NPC npc_requested = (NPC) this.requestedEntity;

                    if (npc_requested == null) {
                        return;
                    }

                    int usedItem = player.getInventory().getItem(nextToken()).getIndex();

                    if ((usedItem == 590) && (npc_requested.getId() == 1532)) {

                        // burn barricade
                        Barricade b = CastleWarsEngine.getSingleton().getBarricadeForNpc(npc_requested);

                        if ((b != null) &&!b.isBurning()) {
                            b.burn();
                            player.getActionSender().sendMessage("You burn the barricade");

                            return;
                        }
                    }

                    if ((usedItem == 4045) && (npc_requested.getId() == 1532)) {
                        Barricade b = CastleWarsEngine.getSingleton().getBarricadeForNpc(npc_requested);

                        if (b != null) {
                            b.unlink();
                            player.getActionSender().sendMessage("You explode the barricade");
                            player.getInventory().deleteItem(4045, 1);

                            return;
                        }
                    }

                    if (!player.getInventory().hasItem(usedItem, 1)) {
                        return;
                    }

                    if (npc_requested.isSummoned() && (npc_requested.getSummoner() != player)) {
                        player.getActionSender().sendMessage("This isn't your familiar");

                        return;
                    }

                    if (npc_requested.getId() == 6873) {

                        // pack yack
                        if (!Zone.canBank(player)) {
                            player.getActionSender().sendMessage("You can't do that here.");

                            return;
                        }

                        if (player.getBank().getSpace() > 0) {
                            player.getInventory().deleteItem(usedItem, 1);
                            player.getBank().insert(usedItem, 1);
                            player.getActionSender().sendMessage("Item sent to bank.");

                            return;
                        }
                    }

                    if ((npc_requested.getId() == 549) && Item.forId(usedItem).isDegradeable()
                            && (Item.forId(usedItem).getDegradeable().getPolicy() == DegradeableItem.POLICY_DEGRADE)) {
                        int price = (int) (Item.forId(usedItem).getPrice() * 1.3);

                        if ((usedItem >= 4212) && (usedItem <= 4255)) {
                            int dif = 4223 - 4212;

                            price = (100000 / dif);
                        }

                        player.getAccount().setSetting("degrade_price", price);
                        player.getAccount().setSetting("degrade_item", usedItem);
                        player.getAccount().setSetting("degrade_new", Item.forId(usedItem).getDegradeable().getMain());

                        SignedBlock block = World.getWorld().getScriptManager().get_block("horvik-repair");

                        if (block != null) {
                            Scope scr = new Scope();

                            scr.setRunning(block);
                            scr.setController(player);
                            player.getNewScriptQueue().add(scr);

                            return;
                        }
                    } else if (npc_requested.getId() == 549) {
                        player.getActionSender().sendMessage("Horvik doesnt seem interested");

                        return;
                    }

                    ScriptVariableInjector injector = new ScriptVariableInjector() {
                        @Override
                        public void injectVariables(Scope scope) {
                            scope.declare(Text.longForName("npc"), npc_requested);
                        }
                    };

                    if (World.getWorld().getScriptManager().directTrigger(player, injector, Trigger.ITEM_ON_NPC,
                            npc_requested.getId(), usedItem)) {
                        return;
                    } else {
                        player.getActionSender().sendMessage("Nothing interesting happens");
                    }

                    break;

                case TYPE_MAGE_ON :
                    if (this.player.isDead()
                            || (Point.getDistance(player.getX(), player.getY(), requestedEntity.getX(),
                                                  requestedEntity.getY()) >= 15)) {
                        close();
                        player.unFollow();
                        player.getFocus().unFocus();

                        return;
                    }

                    if (World.getWorld().getScriptManager().directTrigger(player, new ScriptVariableInjector() {
                        @Override
                        public void injectVariables(Scope scope) {
                            scope.declare(Text.longForName("entity"), requestedEntity);
                        }
                    }, Trigger.CAST_ON_ENTITY, player.getGameFrame().getSpellbook(), player.getRequestedSpell())) {
                        player.setRequestedSpell(-1);

                        return;
                    }

                    if (player.getCombatAdapter().getFightingWith() != (Killable) requestedEntity) {
                        player.getCombatAdapter().attackOn((Killable) requestedEntity);
                        player.getActionSender().sendCloseWalkingFlag();
                        player.getRoute().resetPath();
                    }

                    break;

                case TYPE_NPC_ACTION_OTHER :
                    int poolId = FishingAssignments.getPool(((NPC) (requestedEntity)).getId(), parameters[0] - 1);

                    if (poolId != -1) {
                        player.setCurrentAction(new FishingAction(player, poolId, (NPC) requestedEntity,
                                parameters[0]));

                        return;
                    }

                    switch (parameters[0]) {
                    case 1 :    // talk
                        final NPC npc = (NPC) requestedEntity;

                        if (npc.blockInteractions() ||!npc.isVisible()) {
                            return;
                        }

                        player.log("Talk to npc: "+npc.getId());

                        if ((npc.getQuest_controller() != null) && (npc.getQuest_controller() != player)) {
                            return;
                        }

                        if (npc instanceof GraveStone) {
                            GraveStone stone = (GraveStone) npc;

                            if (stone.getDeadPerson() == player) {
                                stone.drop(false);
                            } else {
                                Dialog.printStopMessage(player,
                                                        "Here lies " + stone.getDeadPerson().getUsername() + "");
                            }
                        }

                        if ((npc.getSummoner() != null) && (npc.getSummoner() != player)) {
                            player.getActionSender().sendMessage("You can't interact with this familiar.");

                            return;
                        }

                        if ((npc.getHuntEntity() != null) && HuntingController.catchButterfly(npc, player)) {
                            return;
                        }

                        ScriptVariableInjector injector1 = new ScriptVariableInjector() {
                            @Override
                            public void injectVariables(Scope scope) {
                                scope.setInteractingNpc(npc);
                                scope.declare(Text.longForName("npc"), npc);
                                npc.setCurrentController(player);
                            }
                        };

                        if (!World.getWorld().getScriptManager().directTrigger(player, injector1, Trigger.TALK_TO_NPC,
                                requestedEntity.getId())) {
                            player.getActionSender().sendMessage("He doesn't seem interested.");
                            player.getFocus().unFocus();
                        }

                        if (npc.getControllerScript() != null) {
                            npc.getControllerScript().do_trigger(Block.TRIGGER_OPTION_1, player);
                        }

                        break;

                    case 3 :
                        final NPC npc2 = (NPC) requestedEntity;

                        if (npc2.blockInteractions()) {
                            return;
                        }

                        player.log("other slot npc: "+npc2.getId());

                        if ((npc2.getSummoner() != null) && (npc2.getSummoner() != player)) {
                            player.getActionSender().sendMessage("This is not your familiar");

                            return;
                        }

                        injector = new ScriptVariableInjector() {
                            @Override
                            public void injectVariables(Scope scope) {
                                scope.setInteractingNpc(npc2);

                                scope.declare(Text.longForName("npc"), npc2);
                            }
                        };

                        if (!World.getWorld().getScriptManager().directTrigger(player, injector, Trigger.TELE_NPC,
                                requestedEntity.getId())) {
                            player.getActionSender().sendMessage("Nothing happens.");
                            player.getFocus().unFocus();
                        }

                        if (npc2.getControllerScript() != null) {
                            npc2.getControllerScript().do_trigger(Block.TRIGGER_OPTION_3, player);
                        }

                        break;

                    case 4 :
                        final NPC npc3 = (NPC) requestedEntity;

                        injector = new ScriptVariableInjector() {
                            @Override
                            public void injectVariables(Scope scope) {
                                scope.setInteractingNpc(npc3);
                                scope.declare(Text.longForName("npc"), npc3);
                            }
                        };

                        player.log("Talk npc 4 "+npc3.getId());
                        if (!World.getWorld().getScriptManager().directTrigger(player, injector, Trigger.NPC_ACTION_4,
                                requestedEntity.getId())) {
                            player.getActionSender().sendMessage("Nothing happens.");
                            player.getFocus().unFocus();
                        }

                        break;

                    case 2 :    // trade/pickpocket
                        final NPC npc5 = (NPC) requestedEntity;

                        if (npc5.blockInteractions()) {
                            return;
                        }

                        if ((npc5.getSummoner() != null) && (npc5.getSummoner() != player)) {
                            player.getActionSender().sendMessage("This is not your familiar");

                            return;
                        }

                        player.log("Trade/pickpocket npc: "+npc5.getId());

                        if ((npc5.getSummoner() != null) && (player.getAccount().getInt32("bob_max") > 0)) {
                            Summoning.openBOB(player);
                            player.log("Open bob "+player.getAccount().getInt32("bob max"));

                            return;
                        } else if ((npc5.getSummoner() != null) && (npc5.getForageItems().size() > 0)) {
                            int slots = player.getInventory().getFreeSlots(0);
                            int tota  = 0;

                            if (slots == 0) {
                                player.getActionSender().sendMessage("You don't have enough free slots.");

                                return;
                            }

                            for (Iterator<Item> iter = npc5.getForageItems().iterator(); iter.hasNext(); ) {
                                Item i = iter.next();

                                if (tota == slots) {
                                    player.getActionSender().sendMessage(
                                        "You don't have enough space to withdraw all items.");

                                    break;
                                }

                                iter.remove();
                                player.getInventory().addItem(i, 1);
                                tota++;
                            }

                            break;
                        }

                        if (npc5.getControllerScript() != null) {
                            npc5.getControllerScript().do_trigger(Block.TRIGGER_OPTION_2, player);
                        }

                        injector = new ScriptVariableInjector() {
                            @Override
                            public void injectVariables(Scope scope) {
                                scope.declare(Text.longForName("npc"), npc5);
                            }
                        };

                        if (!World.getWorld().getScriptManager().directTrigger(player, injector,
                                Trigger.OTHER_SLOT_NPC, requestedEntity.getId())) {
                            player.getActionSender().sendMessage("Nothing happens.");
                            player.getFocus().unFocus();
                        }

                        break;
                    }

                    break;

                case TYPE_PLAYER_ATTACK :
                    if (this.player.isDead()
                            || (Point.getDistance(player.getX(), player.getY(), requestedEntity.getX(),
                                                  requestedEntity.getY()) >= 15)) {
                        close();
                        player.unFollow();
                        player.getFocus().unFocus();

                        return;
                    }

                    if (player.getCombatAdapter().getFightingWith() != (Killable) requestedEntity) {
                        player.getCombatAdapter().attackOn((Killable) requestedEntity);
                        player.getActionSender().sendCloseWalkingFlag();
                        player.getRoute().resetPath();
                        player.log("attack player "+((Killable)requestedEntity).getPlayer());
                    }

                    return;

                case TYPE_DUEL_REQUEST :
                    Player duel_req = (Player) requestedEntity;

                    if (duel_req.getWindowManager().isTrading() || (duel_req.getGameFrame().getDuel() != null)) {
                        player.getActionSender().sendMessage("Other player is busy at the moment.");

                        return;
                    }

                    player.log("duel requested "+duel_req.getUsername());

                    if (duel_req.isInArea(ClanWar.CLAN_WARS_HOME) && player.isInArea(ClanWar.CLAN_WARS_HOME)) {
                        if (duel_req.getChannel() == null) {
                            player.getActionSender().sendMessage("This player is not in a clan chanel");

                            return;
                        } else if (duel_req.getChannel().getRankForPlayer(duel_req) <= ClanChannel.CAPTAIN) {
                            player.getActionSender().sendMessage("This member is not a captain so cannot start wars");

                            return;
                        }

                        if (player.getChannel() == null) {
                            player.getActionSender().sendMessage("You need to be in a clan channel");

                            return;
                        }

                        if (player.getChannel() == duel_req.getChannel()) {
                            return;
                        }

                        if (duel_req.getChallengeRequest() == player.getIndex()) {
                            new ClanWarRequest(duel_req, player);
                            duel_req.setChallengeRequest(-1);
                            player.setChallengeRequest(-1);

                            return;
                        } else {
                            player.setChallengeRequest(duel_req.getIndex());
                            player.getActionSender().sendMessage("Sending war offer..");
                            duel_req.getActionSender().sendDuelReq(player.getUsername(),
                                    "Wishes to go to war with you");

                            return;
                        }
                    }

                    if (duel_req.getDuelRequest() == player.getIndex()) {
                        if (GamblingBanList.isBanned(player.getUID())) {
                            player.getActionSender().sendMessage(Text.RED("You have been banned from gambling."));

                            return;
                        }

                        if (GamblingBanList.isBanned(duel_req.getUID())) {
                            duel_req.getActionSender().sendMessage(Text.RED("You have been banned from gambling."));

                            return;
                        }

                        new Duel(player, duel_req);
                        duel_req.setDuelRequest(-1);
                        player.setDuelRequest(-1);

                        return;
                    }

                    player.getActionSender().sendMessage("Sending duel offer...");
                    player.setDuelRequest(duel_req.getIndex());
                    duel_req.getActionSender().sendDuelReq(player.getUsername(), "wishes to duel with you");

                    break;

                case TYPE_TRADE_REQUEST :
                    Player requestedPlayer = (Player) requestedEntity;

                    if (requestedPlayer.getWindowManager().isTrading()) {
                        player.getActionSender().sendMessage("Other player is busy at the moment");

                        return;
                    }

                    if (requestedPlayer.getTradeRequest() == player.getIndex()) {
                        if (requestedPlayer.getAccount().hasVar("trade_lock")) {
                            requestedPlayer.getActionSender().sendMessage("Your account is trade-locked");

                            return;
                        }

                        if (player.getAccount().hasVar("trade_lock")) {
                            return;
                        }
                        player.log("trade requested: "+requestedPlayer.getUsername());
                        requestedPlayer.getWindowManager().closeWindow();
                        player.getWindowManager().closeWindow();
                        new Trade(player, requestedPlayer);

                        return;
                    }

                    player.getActionSender().sendMessage("Sending trade offer...");
                    requestedPlayer.getActionSender().sendTradeReq(player.getUsername(), "wishes to trade with you");

                    return;

                case TYPE_NPC_ATTACK :
                    player.getCombatAdapter().attackOn((Killable) requestedEntity);
                    player.getRoute().resetPath();
                    player.getActionSender().sendCloseWalkingFlag();

                    return;

                case TYPE_FLOOR_ITEM_PICKUP :
                    FloorItem item = (FloorItem) requestedEntity;

                    if (item.isRemoved()) {
                        return;
                    }

                    if ((item.getIndex() >= 1038) && (item.getIndex() <= 1050) && (item.getDropper() == player)) {
                        if (item.isGlobal()) {
                            Dialog.printStopMessage(player, "You've dropped it now, theirs no going back.");

                            return;
                        }
                    }

                    if (item.isVisableTo(player) || item.isGlobal()) {
                        if (item.getCurrentInstance() != player.getCurrentInstance()) {
                            return;
                        }

                        if (World.getWorld().getScriptManager().isBlocked(player, Trigger.PICKUP_ITEM,
                                item.getItemId(), item.getX(), item.getY())) {
                            return;
                        }

                        if (item instanceof ZombieGroundItem) {
                            ((ZombieGroundItem) item).onPickup(player);

                            return;
                        }

                        if (player.getInventory().getFreeSlots(item.getItemId()) > 0) {
                            if (!ClueManager.handle_scroll_pickup(player, item.getItemId())) {
                                return;
                            }

                            if (!(item instanceof DungeoneeringStaticItem)) {
                                if (item.getCurrentInstance() instanceof Dungeon) {
                                    Dungeon d = (Dungeon) item.getCurrentInstance();

                                    d.removeFloorItem(item);
                                }

                                item.remove();
                            }

                            Sounds.playSingleSound(2582, player);
                            player.log("picked up: " + Item.forId(item.getItemId()).getName());
                            player.getInventory().addItem(item.getItemId(), item.getAmount());
                        } else {
                            player.getActionSender().sendMessage("You don't have enough free slots.");
                        }
                    }

                    break;

                case TYPE_USE_ITEM_ON_OBJECT :
                    int        objId2 = requestedEntity.getId();
                    GameObject obj2   = World.getWorld().getObjectManager().getDefaultObject(objId2,
                                            requestedEntity.getX(), requestedEntity.getY(),
                                            requestedEntity.getHeight());
                    int tok = nextToken();

                    if (tok == -1) {
                        return;
                    }

                    Item i = player.getInventory().getItem(tok);

                    player.getFacingLocation().focus(requestedEntity.getLocation());

                    if (i == null) {
                        return;
                    }

                    if ((obj2 != null) && (obj2.getCurrentInstance() != player.getCurrentInstance())) {
                        return;
                    }

                    if (CookingEntity.itemAtObject(i, player, obj2, objId2)) {
                        return;
                    }

                    if (SmithingUtils.itemOnObjectInteraction(player, i.getIndex(), objId2)) {
                        return;
                    }

                    if (obj2 instanceof DwarfMultiCannon) {
                        if (!player.getAccount().hasVar("cannon")
                                || (player.getAccount().getVar("cannon").getFullData() != obj2)) {
                            player.getActionSender().sendMessage("This is not your cannon. ");
                        } else {
                            DwarfMultiCannon cannon =
                                (DwarfMultiCannon) player.getAccount().getVar("cannon").getFullData();

                            if (cannon.itemAtObject(i.getIndex(), obj2.getId())) {
                                return;
                            }
                        }
                    }

                    if (CastleWarsEngine.getSingleton().itemAtObject(player, i.getIndex(), objId2,
                            requestedEntity.getX(), requestedEntity.getY(), 0)) {
                        return;
                    }

                    if (Farming.itemAtObject(i.getIndex(), requestedEntity.getX(), requestedEntity.getY(), player)) {
                        return;
                    }

                    if (player.getCurrentHouse() != null) {
                        if (player.getCurrentHouse().bonesOnAltarCheck(i.getIndex(), objId2, player)) {
                            return;
                        }

                        if (ObjectNode.getCostumeBoxID(objId2) != -1) {
                            if (player.getCurrentHouse().getPlayer() != player) {
                                Dialog.printStopMessage(player, "This is not your box.");

                                return;
                            }

                            if (!CostumeRuleTable.get(ObjectNode.getCostumeBoxID(objId2)).isAllowed(i.getIndex())) {
                                Dialog.printStopMessage(player, "You can't put this in here.");

                                return;
                            }

                            RoomBox box = null;

                            if (!player.getAccount().hasRoomBox(ObjectNode.getCostumeBoxID(objId2))) {
                                box = new RoomBox(10, 0);
                                player.getAccount().setRoomBox(ObjectNode.getCostumeBoxID(objId2), box);
                            } else {
                                box = player.getAccount().getRoomBox(ObjectNode.getCostumeBoxID(objId2));
                            }

                            if (box.addItem(i.getIndex(), 1)) {
                                player.getInventory().deleteItem(i.getIndex(), 1);
                                Dialog.printStopMessage(player, "You place the items into the box.");

                                return;
                            } else {
                                return;
                            }
                        }
                    }

                    final GameObject       obj5 = obj2;
                    ScriptVariableInjector inj  = new ScriptVariableInjector() {
                        @Override
                        public void injectVariables(Scope scope) {
                            scope.declare(Text.longForName("obj"), obj5);
                        }
                    };

                    if (!World.getWorld().getScriptManager().directTrigger(player, inj, Trigger.ITEM_AT_OBJECT,
                            i.getIndex(), objId2)) {
                        player.getActionSender().sendMessage("Nothing interesting happens.");

                        return;
                    }

                    break;

                case TYPE_FLOOR_ITEM_MAGE :
                    FloorItem i2 = (FloorItem) this.requestedEntity;

                    if (player.getCurStat(6) < 33) {
                        player.getActionSender().sendMessage("You need a magic level of at least 33 to use this.");

                        return;
                    }

                    if (!player.getInventory().hasItem(556, 1) ||!player.getInventory().hasItem(563, 1)) {
                        player.getActionSender().sendMessage("You need more runes for this spell");

                        return;
                    }

                    if (World.getWorld().getScriptManager().isBlocked(player, Trigger.PICKUP_ITEM, i2.getItemId(),
                            i2.getX(), i2.getY())) {
                        return;
                    }



                    player.getInventory().deleteItem(556, 1);
                    player.getInventory().deleteItem(563, 1);
                    player.addXp(6, 30);

                    Projectile proj = new Projectile(player, i2, 143, Projectile.getWaitspeedForTick(2), 40);

                    proj.setAttribute(2, 0);
                    proj.setOnImpactEvent(new TelegrabImpactEvent(player, i2));
                    player.registerProjectile(proj);
                    player.getRoute().resetPath();
                    player.getActionSender().sendCloseWalkingFlag();
                    player.getFacingLocation().focus(i2.getLocation());
                    player.getAnimation().animate(711);
                    player.getGraphic().set(142, 0);

                    break;

                case TYPE_OBJECT_REQUEST :
                    Tile t = World.getWorld().getTile(requestedEntity.getX(), requestedEntity.getY(),
                                                      requestedEntity.getHeight());
                    int              objId = requestedEntity.getId();
                    final GameObject obj   = World.getWorld().getObjectManager().getDefaultObject(objId,
                                                 requestedEntity.getX(), requestedEntity.getY(),
                                                 requestedEntity.getHeight());
                    player.log("at object: "+objId);
                    player.getFacingLocation().focus(requestedEntity.getLocation());

                    if (player.getRights() >= Player.ADMINISTRATOR) {
                        player.getActionSender().sendMessage("Facing: " + requestedEntity.getLocation());
                        player.getActionSender().sendMessage(player.getFacingLocation().getCurrentId() + " "
                                + player.getKnownViewpointId(player));
                    }

                    ScriptVariableInjector objectInjector = new ScriptVariableInjector() {
                        @Override
                        public void injectVariables(Scope scope) {
                            if (obj != null) {
                                scope.declareVar(OBJ_VAR, new ScriptVar(OBJ_VAR, obj));
                            } else {}
                        }
                    };

                    if ((obj != null) && (obj.getType() == 4)) {
                        int tfX = 0;
                        int tfY = 0;

                        if ((player.getX() != obj.getX()) || (player.getY() != obj.getY())) {
                            return;
                        }

                        switch (obj.getRotation()) {
                        case 1 :
                        case -1 :
                            tfY++;

                            break;

                        case 0 :
                            tfX--;

                            break;

                        case 2 :
                        case -2 :
                            tfX++;

                            break;

                        case -3 :
                        case 3 :
                            tfY--;

                            break;
                        }

                        player.getFacingLocation().focus(requestedEntity.getLocation().transform(tfX, tfY));
                    }

                    int slot = nextToken();

                    if (Farming.objectClicked(requestedEntity.getX(), requestedEntity.getY(), slot, player)) {
                        return;
                    }

                    if (PCGame.objectClick(player, objId, requestedEntity.getX(), requestedEntity.getY())) {
                        return;
                    }

                    if (Summoning.objectClick(objId, slot, player)) {
                        return;
                    }

                    if (obj instanceof DwarfMultiCannon) {
                        if (!player.getAccount().hasVar("cannon")
                                || (player.getAccount().getVar("cannon").getFullData() != obj)) {
                            player.getActionSender().sendMessage("This is not your cannon. ");
                        } else {
                            DwarfMultiCannon cannon =
                                (DwarfMultiCannon) player.getAccount().getVar("cannon").getFullData();

                            if ((slot == 1) && cannon.isReady()) {
                                if (cannon.isFiring()) {
                                    player.getActionSender().sendMessage("Your cannon is already firing");

                                    break;
                                }

                                if (cannon.getAmmo() == 0) {
                                    player.getActionSender().sendMessage("Your cannon has no balls.");

                                    break;
                                }

                                cannon.setFiring(true);

                                break;
                            } else if ((slot == 2) ||!cannon.isReady()) {
                                cannon.destruct(false);

                                break;
                            }
                        }
                    }

                    if ((obj != null) && (obj.getId() == 733)) {
                        if (player.getEquipment().getWeapon() == null) {
                            player.getActionSender().sendMessage(
                                "You need a weapon with a slash style to break through this");

                            break;
                        }

                        Weapon weapon  = player.getEquipment().getWeapon();
                        int    wepAnim = -1;

                        for (int xi = 0; xi < 4; xi++) {
                            if ((weapon.getAttackStyle(xi) != null)
                                    && (weapon.getAttackStyle(xi).getType() == AttackStyle.TYPE_SLASH)) {
                                wepAnim = weapon.getAttackStyle(xi).getAnimation();
                            }
                        }

                        if (wepAnim == -1) {
                            player.getActionSender().sendMessage(
                                "You need a weapon with a slash style to break through this");

                            return;
                        }

                        player.getAnimation().animate(wepAnim);

                        if (GameMath.rand3(100) < 25) {
                            player.getActionSender().sendMessage("You fail to slash the web");

                            return;
                        }

                        obj.changeIndex(0);

                        if (obj.getType() == 10) {
                            ClippingDecoder.removeClippingForSolidObject(obj.getX(), obj.getY(), 0, 1, 1, true, true);
                        } else if (obj.getType() == 0) {
                            ClippingDecoder.removeClippingForVariableObject(obj.getX(), obj.getY(), obj.getHeight(),
                                    obj.getType(), obj.getRotation(), true, true);
                        }

                        World.getWorld().submitEvent(new WorldTickEvent(GameMath.rand(140) + 10) {
                            @Override
                            public void tick() {}
                            @Override
                            public void finished() {
                                obj.changeIndex(733);

                                if (obj.getType() == 10) {
                                    ClippingDecoder.addClippingForSolidObject(obj.getX(), obj.getY(), 0, 1, 1, true,
                                            true);
                                } else {
                                    ClippingDecoder.addWall(obj.getX(), obj.getY(), obj.getHeight(), obj.getType(),
                                                            obj.getRotation(), true, true);
                                }
                            }
                        });
                        player.getActionSender().sendMessage("You slash the web");

                        break;
                    }

                    if (player.getGetCurrentWar() != null) {
                        if (player.getGetCurrentWar().onObjectClick(player, objId, requestedEntity.getX(),
                                requestedEntity.getY())) {
                            return;
                        }
                    }

                    if (player.getCurrentHouse() != null) {
                        player.getCurrentHouse().on_object_click(player, objId, requestedEntity.getX(),
                                requestedEntity.getY(), slot);

                        return;
                    }

                    if (BarrowsGame.onObjectClick(objId, -1, -1, player.getHeight(), player)) {
                        return;
                    }

                    if (player.getZombiesGame() != null) {
                        if (player.getZombiesGame().onObjectClick(objId, requestedEntity.getX(),
                                requestedEntity.getY(), player)) {
                            return;
                        }
                    }

                    if (HuntingController.onObjectClick(objId, requestedEntity.getX(), requestedEntity.getY(),
                            player)) {
                        return;
                    }

                    switch (slot) {
                    case 1 :
                        if ((objId == 4903) || (objId == 4377) || (objId == 4902) || (objId == 4900)
                                || (objId == 4901)) {
                            CastleWarsEngine.getSingleton().flagCheck(player, objId, requestedEntity.getX(),
                                    requestedEntity.getY());

                            return;
                        }

                        if (CastleWarsEngine.getSingleton().atObject(player, objId, requestedEntity.getX(),
                                requestedEntity.getY())) {
                            return;
                        }

                        if (objId == 28213) {
                            if (player.getChannel() == null) {
                                Dialog.printStopMessage(player, "You are not in a clan chat.");

                                return;
                            }

                            if (player.getChannel().getWar() == null) {
                                Dialog.printStopMessage(player, "Your clan is not at war");

                                return;
                            }

                            if (player.getChannel().getWar().gameStarted()
                                    && player.getChannel().getWar().getGameRules().isKnockoutMode()) {
                                player.getActionSender().sendMessage(
                                    "This is a knockout game, and has already started.");

                                return;
                            }

                            player.getChannel().getWar().registerPlayer(player);

                            return;
                        }

                        if ((obj != null) && (obj.getId() == 2360)) {
                            if (player.getDungeon() != null) {
                                FloorHandler floor =
                                    player.getDungeon().getFloor(player.getAccount().getInt32("dung_floor"));

                                if (floor.getNpcs().size() != 0) {
                                    player.getActionSender().sendMessage(
                                        "Please clear the floor before attempting to raid the chest.");

                                    return;
                                }

                                if (floor.isChestOpened()) {
                                    player.getActionSender().sendMessage("This chest has already been raided.");

                                    return;
                                }

                                LootTable loot_table = LootTable.getTable(554);
                                int       chance     = 10;
                                int       floor_id   = floor.getFloorID();

                                chance += floor_id * 2;
                                chance += player.getDungeon().getDifficulty() * 5;

                                Loot lt;

                                if (player.getRandom().nextInt(100) > 85) {
                                    int randomExp = player.getRandom().nextInt((1
                                                        + player.getDungeon().getDifficulty()) * 100);

                                    player.addXp(24, randomExp);

                                    int randPts = GameMath.rand(player.getDungeon().getDifficulty() * 100);

                                    player.addPoints(Points.DUNGEONEERING_POINTS, randPts);
                                    Dialog.printStopMessage(player, "Bonus: " + randPts + " points",
                                                            "Bonus: " + randomExp + " exp");
                                }

                                if (GameMath.rand(99) <= chance) {
                                    lt = loot_table.randomLoot(4);

                                    if (lt != null) {
                                        player.getInventory().addItem(Item.forId(lt.getItemID()), lt.getAmount());
                                        player.getActionSender().sendMessage("You raid the chest and get a "
                                                + Item.forId(lt.getItemID()).getName());
                                        player.getActionSender().sendMessage(
                                            "Increase the difficulty and floor amount for a chance to get a better reward");
                                    }
                                } else {
                                    lt = loot_table.randomLoot(3);

                                    if (lt != null) {
                                        player.getInventory().addItem(Item.forId(lt.getItemID()), lt.getAmount());
                                        player.getActionSender().sendMessage("You raid the chest and get a "
                                                + Item.forId(lt.getItemID()).getName());
                                        player.getActionSender().sendMessage(
                                            "Increase the difficulty and floor amount for a chance to get a better reward");
                                    }
                                }

                                floor.setChestOpened(true);

                                return;
                            }
                        }

                        if (obj != null) {
                            if ((obj.getId() == 2709) && (Zombies.getCurrentGame() != null)
                                    && Zombies.getCurrentGame().isRegistered(player)) {
                                Zombies.getCurrentGame().fromChest(player);

                                return;
                            }
                            if ((obj.getId() == 2183) && (Zombies.getCurrentGame() != null)
                                    && Zombies.getCurrentGame().isRegistered(player)) {
                                Zombies.getCurrentGame().pullPowerupChest(player);

                                return;
                            }
                            Tree tree = WoodcuttingAction.getTree(obj.getDefaultId());

                            if (tree != null) {
                                player.setCurrentAction(new WoodcuttingAction(player, obj, tree));

                                return;
                            }

                            MiningAction.Rock entity = MiningAction.getRock(objId);

                            if (entity != null) {
                                player.setCurrentAction(new MiningAction(player, obj, entity));

                                return;
                            } else {
                                if ((MiningAction.getRock(obj.getDefaultId()) != null)
                                        && (objId == MiningAction.getReplacement(objId))) {
                                    Dialog.printStopMessage(player, "This rock has not respawned yet",
                                                            "You estimate it will respawn in about "
                                                            + (1 + (Core.getSecondsForTicks(obj.getRevertTime()
                                                                - Core.currentTime) / 60) + " min"));

                                    return;
                                }
                            }
                        } else {}

                        if (!World.getWorld().getScriptManager().directTrigger(player, objectInjector,
                                Trigger.AT_OBJECT, objId)) {
                            if (World.getWorld().getScriptManager().directTrigger(player, objectInjector,
                                    Trigger.AT_OBJECT, objId, requestedEntity.getX(), requestedEntity.getY(),
                                    requestedEntity.getHeight())) {
                                return;
                            }
                        } else {
                            return;
                        }

                        if ((t.getDoor() != null)
                                && (t.getDoor().getCurrentInstance() == player.getCurrentInstance())) {
                            if (t.getDoor().getCurrentId() == requestedEntity.getId()) {
                                if (t.getDoor().isOpen()) {
                                    t.getDoor().close();
                                } else {
                                    t.getDoor().open();
                                }
                            }

                            return;
                        }

                        if (AgilityArena.onObjectClick(player, requestedEntity.getId(), requestedEntity.getX(),
                                                       requestedEntity.getY(), requestedEntity.getHeight())) {
                            return;
                        }

                        player.getActionSender().sendMessage("Nothing interesting happens.");

                        break;

                    case 3 :
                        if (!World.getWorld().getScriptManager().directTrigger(player, objectInjector,
                                Trigger.AT_OBJECT_3, requestedEntity.getId(), requestedEntity.getX(),
                                requestedEntity.getY(), requestedEntity.getHeight())) {
                            player.getActionSender().sendMessage("Nothing interesting happens.");
                        }

                        break;

                    case 4 :
                        if (!World.getWorld().getScriptManager().directTrigger(player, objectInjector,
                                Trigger.AT_OBJECT_4, requestedEntity.getId(), requestedEntity.getX(),
                                requestedEntity.getY(), requestedEntity.getHeight())) {
                            player.getActionSender().sendMessage("Nothing interesting happens.");
                        }

                        break;

                    case 2 :
                        final MiningAction.Rock r = MiningAction.getRock(objId);

                        if (r != null) {
                            player.setActionsDisabled(true);
                            player.getActionSender().sendMessage("You look at the rock carefully..");
                            player.getTickEvents().add(new PlayerTickEvent(player, true, 3) {
                                @Override
                                public void doTick(Player owner) {
                                    owner.getActionSender().sendMessage("This rock contains "
                                            + Item.forId(r.getOreId()).getName().replace("ore", "") + ".");
                                    owner.setActionsDisabled(false);
                                    terminate();
                                }
                            });

                            return;
                        }

                        if (!World.getWorld().getScriptManager().directTrigger(player, objectInjector,
                                Trigger.AT_OBJECT_2, objId)) {
                            if (World.getWorld().getScriptManager().directTrigger(player, objectInjector,
                                    Trigger.AT_OBJECT_2, objId, requestedEntity.getX(), requestedEntity.getY(),
                                    requestedEntity.getHeight())) {
                                return;
                            } else {
                                player.getActionSender().sendMessage("Nothing interesting happens...");
                            }

                            break;
                        }
                    }

                    break;
                }
            } else {
                if ((player.getRoute().isIdle() && (requestType == TYPE_NPC_ACTION_OTHER))
                        || (requestType == TYPE_NPC_ATTACK)) {
                    NPC npc = (NPC) requestedEntity;

                    if (!npc.isDead() && (Point.getDistance(npc.getLocation(), player.getLocation()) < 10)) {
                        if (!player.hasMoved()) {
                            player.followNpc(npc);
                        }
                    }
                }
            }
        }
    }

    /**
     * Method getRequestLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getRequestLocation() {
        return requestLocation;
    }

    /**
     * Method setRequestLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param requestLocation
     */
    public void setRequestLocation(Point requestLocation) {
        this.requestLocation = requestLocation;
    }
}
