package net.tazogaming.hydra.entity3d.player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/10/13
 * Time: 00:17
 */
public class PlayerVariable {
    private boolean save    = false;
    private int     intData = -1;
    private String  stringData;
    private String  name;
    private Object  variable;

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param i
     */
    public PlayerVariable(String name, int i) {
        this(name, i, false);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param str
     */
    public PlayerVariable(String name, String str) {
        this(name, str, false);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param i
     * @param save
     */
    public PlayerVariable(String name, int i, boolean save) {
        this.intData = i;
        this.save    = save;
        this.name    = name;
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param data
     * @param save
     */
    public PlayerVariable(String name, Object data, boolean save) {
        this.name     = name;
        this.variable = data;
        this.save     = save;
        if(data instanceof Integer)         {
            this.intData = (Integer)data;
            this.variable = null;
        }

        if(data instanceof String) {
            this.stringData = (String)data;
            this.variable = null;
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param str
     * @param save
     */
    public PlayerVariable(String name, String str, boolean save) {
        this.stringData = str;
        this.name       = name;
        this.save       = save;
    }

    /**
     * Method getFullData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object getFullData() {
        if (variable != null) {
            return variable;
        }

        if (intData != -1) {
            return intData;
        }

        return stringData;
    }

    /**
     * Method getIntData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getIntData() {
        return intData;
    }

    /**
     * Method setIntData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param intData
     */
    public void setIntData(int intData) {
        this.intData = intData;
    }

    /**
     * Method isSave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSave() {
        return save;
    }

    /**
     * Method setSave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param save
     */
    public void setSave(boolean save) {
        this.save = save;
    }

    /**
     * Method getStringData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getStringData() {
        return stringData;
    }

    /**
     * Method setStringData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param stringData
     */
    public void setStringData(String stringData) {
        this.stringData = stringData;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }
}
