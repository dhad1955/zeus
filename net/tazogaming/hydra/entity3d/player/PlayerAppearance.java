
/*
* PlayerAppearance.java
*
* Created on 16-Dec-2007, 21:38:43
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
*/
package net.tazogaming.hydra.entity3d.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.net.packetbuilder.mask.UpdateMask;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class PlayerAppearance extends UpdateMask {

    /** head, torso, arms, hands, legs, feet, hairColour, torsoColour, legColour, feetColour, skinColour, gender, jaw, id made: 14/12/06 **/
    private int head, torso, arms, hands, legs, feet,    // styles
                hairColour, torsoColour, legColour, feetColour, skinColour, gender, jaw,
                id = 0;

    /** hasChanged made: 14/12/06 **/
    private transient boolean hasChanged = true;

    /** transform made: 14/12/06 **/
    private TransformNpc transform = new TransformNpc();

    /** playerLook made: 14/12/06 **/
    private int[] playerLook = new int[20];

    /** playerColors made: 14/12/06 **/
    private int[] playerColors = new int[5];

    /** capeRecolours made: 14/12/06 **/
    private int[][] capeRecolours = new int[2][4];

    /** forceAppearanceRequired made: 14/12/06 **/
    private boolean forceAppearanceRequired = false;

    /** requested made: 14/12/06 **/
    private PlayerAppearance requested;

    /**
     * Constructs ...
     *
     */
    public PlayerAppearance() {
        resetAppearence();
    }

    /**
     * Constructs ...
     *
     *
     * @param i
     */
    public PlayerAppearance(int... i) {
        resetAppearence();
    }

    /**
     * Method isForceAppearanceRequired
     * Created on 14/12/06
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isForceAppearanceRequired() {
        return forceAppearanceRequired;
    }

    /**
     * Method setForceAppearanceRequired
     * Created on 14/12/06
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param forceAppearanceRequired
     */
    public void setForceAppearanceRequired(boolean forceAppearanceRequired) {
        this.forceAppearanceRequired = forceAppearanceRequired;
    }

    /**
     * Method getCapeColour
     * Created on 14/12/06
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param capeType
     * @param color
     *
     * @return
     */
    public int getCapeColour(int capeType, int color) {
        return capeRecolours[capeType][color];
    }

    /**
     * Method setCapeColor
     * Created on 14/12/06
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param c
     * @param cc
     */
    public void setCapeColor(int type, int c, int cc) {
        this.capeRecolours[type][c] = cc;
    }

    /**
     * Method setLook
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param look
     */
    public void setLook(int id, int look) {
        if (look == -1) {
            ++look;
        }

        this.playerLook[id] = look;
    }

    /**
     * Method setColor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param look
     */
    public void setColor(int id, int look) {
        this.playerColors[id] = look;
    }

    /**
     * Method resetAppearence
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetAppearence() {
        this.playerLook   = new int[7];
        this.playerColors = new int[5];
        this.gender       = 0;

        int[] look   = playerLook;
        int[] colour = playerColors;

        look[0]   = 3;     // Hair
        look[1]   = 14;    // Beard
        look[2]   = 18;    // Torso
        look[3]   = 26;    // Arms
        look[4]   = 34;    // Bracelets
        look[5]   = 38;    // Legs
        look[6]   = 42;    // Shoes
        colour[2] = 16;
        colour[1] = 16;

        for (int i = 0; i < 5; i++) {
            colour[2] = 16;
            colour[1] = 16;
            colour[0] = 3;
            gender    = 0;
        }
    }

    /**
     * Method setRequested
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pa
     */
    public void setRequested(PlayerAppearance pa) {
        requested = pa;
    }

    /**
     * Method getRequested
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PlayerAppearance getRequested() {
        return requested;
    }

    /**
     * Method setIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setIndex(int id) {
        this.id = id;
    }

    /**
     * Method getColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getColour() {
        return playerColors;
    }

    /**
     * Method getLook
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getLook() {
        return playerLook;
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     */
    public void save(Buffer data) {
        data.addByte(UserAccount.OPCODE_APPEARANCE);
        data.addByte(getGender());

        for (int i = 0; i < 7; i++) {
            data.writeWord(playerLook[i]);
        }

        for (int i = 0; i < 5; i++) {
            data.writeWord(getColour()[i]);
        }
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     */
    public void load(Buffer d) {
        setGender(d.readByte());

        for (int i = 0; i < 7; i++) {
            playerLook[i] = d.readShort();
        }

        for (int i = 0; i < 5; i++) {
            playerColors[i] = d.readShort();
        }
    }

    /**
     * Method getTransform
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public TransformNpc getTransform() {
        return transform;
    }

    /**
     * Method getHead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHead() {
        return getLook()[0];
    }

    /**
     * Method setGender
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param gender
     */
    public void setGender(int gender) {
        this.gender = gender;
    }

    /**
     * Method setJaw
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param jaw
     */
    public void setJaw(int jaw) {
        this.jaw = jaw;
    }

    /**
     * Method setHead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param head
     */
    public void setHead(int head) {
        this.head = head;
    }

    /**
     * Method getTorso
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTorso() {
        return getLook()[2];
    }

    /**
     * Method setTorso
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param torso
     */
    public void setTorso(int torso) {
        this.torso = torso;
    }

    /**
     * Method getArms
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getArms() {
        return getLook()[3];
    }

    /**
     * Method setArms
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param arms
     */
    public void setArms(int arms) {}

    /**
     * Method getHands
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHands() {
        return getLook()[4];
    }

    /**
     * Method setHands
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hands
     */
    public void setHands(int hands) {
        this.hands = hands;
    }

    /**
     * Method getLegs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLegs() {
        return getLook()[5];
    }

    /**
     * Method setLegs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param legs
     */
    public void setLegs(int legs) {
        this.legs = legs;
    }

    /**
     * Method getGender
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGender() {
        return gender;
    }

    /**
     * Method getJaw
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getJaw() {
        return getLook()[1];
    }

    /**
     * Method getFeet
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFeet() {
        return getLook()[6];
    }

    /**
     * Method setFeet
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param feet
     */
    public void setFeet(int feet) {
        this.feet = feet;
    }

    /**
     * Method getHairColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHairColour() {
        return hairColour;
    }

    /**
     * Method setHairColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hairColour
     */
    public void setHairColour(int hairColour) {
        this.hairColour = hairColour;
    }

    /**
     * Method getTorsoColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTorsoColour() {
        return torsoColour;
    }

    /**
     * Method setTorsoColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param torsoColour
     */
    public void setTorsoColour(int torsoColour) {
        this.torsoColour = torsoColour;
    }

    /**
     * Method getLegColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLegColour() {
        return legColour;
    }

    /**
     * Method setLegColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param legColour
     */
    public void setLegColour(int legColour) {
        this.legColour = legColour;
    }

    /**
     * Method getFeetColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFeetColour() {
        return feetColour;
    }

    /**
     * Method setFeetColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param feetColour
     */
    public void setFeetColour(int feetColour) {
        this.feetColour = feetColour;
    }

    /**
     * Method getSkinColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSkinColour() {
        return skinColour;
    }

    /**
     * Method setSkinColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param skinColour
     */
    public void setSkinColour(int skinColour) {
        this.skinColour = skinColour;
    }
}
