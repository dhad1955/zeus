package net.tazogaming.hydra.entity3d.player;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.HintIcon;
import net.tazogaming.hydra.util.RecurringTickEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/09/14
 * Time: 15:48
 */
public class GraveStone extends NPC implements RecurringTickEvent {


    public static final List<GraveStone> activeNodes = new ArrayList<GraveStone>();

    public static void checkGravestones(Player player) {
        for(GraveStone graveStone : activeNodes) {
            if(graveStone.username == player.getUsernameHash()){
                loadGravestone(player, graveStone);
            }
        }
    }

    public static void unloadGraveStone(Player player){
        int id = player.getGameFrame().getRoot();
        player.getGameFrame().sendInterfaceConfig(player, id, id == 548 ? 12 : 164, false);
        player.getGameFrame().sendInterfaceConfig(player, id, id == 548 ? 13 : 165, false);
        player.getGameFrame().sendInterfaceConfig(player, id, id == 548 ? 14 : 166, false);
        player.getGameFrame().setCurrentHintIcon(null);
    }
    public static void loadGravestone(Player player, GraveStone graveStone) {
        int id = player.getGameFrame().getRoot();
        player.getGameFrame().sendInterfaceConfig(player, id, id == 548 ? 12 : 164, true);
        player.getGameFrame().sendInterfaceConfig(player, id, id == 548 ? 13 : 165, true);
        player.getGameFrame().sendInterfaceConfig(player, id, id == 548 ? 14 : 166, true);
        graveStone.player = player;
        player.getGameFrame().setCurrentHintIcon(new HintIcon(graveStone.getLocation(), 20, 0));
    }

    private int[] items = new int[50];
    private int[] amounts = new int[50];
    private int caret = 0;

    private int timeToLive = Core.getTicksForMinutes(2);

    // who's gravestone is it, supports
    // logging out.
    private long username;
    private Player player;


    public Player getDeadPerson() {
        return player;
    }
    public GraveStone(Player player) {
        super(player.getAccount().hasVar("grave_stone") ? player.getAccount().getInt32("grave_stone") : 6565, player.getLocation());
        setMovement(new Movement(this) {
            @Override
            public void tick(int currentTime) {

            }

            @Override
            public void mobMoved(Point newLocation, int direction) {

            }

            @Override
            public void exceptionCaught(Exception ee) {

            }
        });
        this.username = player.getUsernameHash();
        super.getAnimation().animate(getAnim(getId()));
        loadGravestone(player, this);
        World.getWorld().registerNPC(this);
        activeNodes.add(this);
        timeToLive = Core.getTicksForMinutes(15) + Core.currentTime;
        Core.submitTask(this);

    }


    public int getAnim(int npc) {
        switch (npc){
            case 6565:
            case 6571:
            case 6574:
            case 6577:
            case 6580:
            case 6583:
            case 6592:
            case 6595:
            case 6601:

                return 7394;
            case 6568:
                return 7396;
        }
        return 0;
     }

    public void pushItem(int id, int amt) {
        if(caret >= 50)
            throw new RuntimeException("Error to many items!");

        items[caret] = id;
        amounts[caret ++ ] = amt;
    }

    boolean dropped = false;

    public void drop(boolean global) {
        if(dropped)
            return;

      // if(global)
        //    player.getActionSender().sendMessage("Your gravestone has collapsed.");

        for(int i = 0; i < caret; i++){
            FloorItem item = new FloorItem(items[i], amounts[i], getX(), getY(), getHeight(), global && player.getAccount().getIronManMode() == 0? null : player);
            if(global && Item.forId(item.getItemId()).isTradeable())
                item.globalise();
        }
        dropped = true;
        unlink();
        activeNodes.remove(this);
        unloadGraveStone(player);
        terminated = true;
        checkGravestones(player);
    }

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {

        int ticks = timeToLive  - Core.currentTime;
        int minutes = ticks / 100;
        int seconds = (int) ((ticks - (minutes * 100)) * 0.6);
        String time = (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);


       try {
        if(time.length() < 100) {
            player.getGameFrame().sendString(time, player.getGameFrame().getRoot(), player.getGameFrame().getRoot() == 548 ? 13 : 166);

        }
       }catch (Exception ee) {
           ee.printStackTrace();
           drop(true);

       }
        if(Core.currentTime - timeToLive > 0){
            drop(true);
            player.getActionSender().sendMessage("Your gravestone has dropped.");
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
        return terminated;
    }
    private boolean terminated;
}
