package net.tazogaming.hydra.entity3d.player;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.SummoningMovement;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/09/14
 * Time: 17:21
 */
public class DeathEvent implements RecurringTickEvent {

    private int time = 5;
    private NPC death;
    private Player player;


    public DeathEvent(Player player) {
        death = new NPC(2862, player.getLocation());
        this.player = player;

        Point loc = SummoningMovement.getTeleLocation(death, player);
        if(loc != null) {
            death.setLocation(loc);
        }
        World.getWorld().registerNPC(death);
        Core.submitTask(this);
        death.getFocus().focus(this.player);
        death.getAnimation().animate(791);

        death.setTextUpdate("Your soul is mine "+player.getUsername());
    }


    /**
     * Method tick
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        death.getFocus().focus(player);

        if(--time == 2)      {
            death.setTextUpdate("Muhahahahaha!");
        }
        if(time == 0) {
            death.unlink();
            terminated = true;
        }
    }

    private boolean terminated = false;
    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
       return terminated;
    }
}
