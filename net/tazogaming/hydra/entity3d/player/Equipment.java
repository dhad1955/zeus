
/*
* Equipment.java
*
* Created on 16-Dec-2007, 23:56:41
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
*/
package net.tazogaming.hydra.entity3d.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.DegradeableItem;
import net.tazogaming.hydra.entity3d.item.Degrading;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.game.CompletionistCape;
import net.tazogaming.hydra.game.PKCompletionistCape;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.game.ui.SkillGuide;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Class description Hydrascape 639 Game server Copyright (C) Tazogaming 2014
 *
 *
 * @version Enter version here..., 14/08/18
 * @author Daniel Hadland
 */
public class Equipment {
	public static final int[] HYBRID_ITEMS = new int[] { 4708, 4710, 4712, 4714, 565, 555, 560, 15486, 18355, 6914 };

	public static final int[] MASTER_CAPES = new int[] { 20430, 20432, 20431, 20438, 20433, 20434, 20435, 20448, 20450,
			20443, 20447, 20449, 20442, 20446, 20445, 20440, 20439, 20441, 20444, 20451, 20436, 20453, 20437, 20452 };

	/** punching made: 14/10/19 */
	private static Weapon punching = new Weapon();
	public static final int HAT = 0, CAPE = 1, AMULET = 2, WEAPON = 3, TORSO = 4, SHIELD = 5, LEGS = 7, HANDS = 9,
			FEET = 10, RING = 12, ARROWS = 13;

	static {
	}

	/** swapSlot made: 14/10/19 */
	private int swapSlot = -1;

	/** items made: 14/10/19 */
	private EquippedItem[] items = new EquippedItem[14];

	/** loadout made: 14/10/19 */
	private int[] loadout = new int[14];

	/** loadoutAmt made: 14/10/19 */
	private int[] loadoutAmt = new int[14];

	/** lastWepChange made: 14/10/19 */
	private int lastWepChange = 0;

	/** dungBindedItems made: 14/10/19 */
	private int[] dungBindedItems = new int[14];
	public boolean[] switchReqs = new boolean[28];

	/** player made: 14/10/19 */
	private Player player;

	/**
	 * Constructs ...
	 *
	 *
	 * @param p
	 */
	public Equipment(Player p) {
		this.player = p;
	}

	/**
	 * Method isHybrid Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isHybrid() {
		int hybridScore = 0;

		for (Integer hybridItem : HYBRID_ITEMS) {
			boolean has = player.getEquipment().contains(hybridItem);

			if (has) {
				hybridScore++;
			}

			if (player.getInventory().hasItem(hybridItem, 1) && !has) {
				hybridScore++;
			}
		}

		return hybridScore > 5;
	}

	/**
	 * Method getTotalWearing Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getTotalWearing() {
		int count = 0;

		for (int i = 0; i < 14; i++) {
			if (isEquipped(i)) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Method getLoadout Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getLoadout() {
		return loadout;
	}

	/**
	 * Method setLoadout Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loadout
	 */
	public void setLoadout(int[] loadout) {
		this.loadout = loadout;
	}

	/**
	 * Method getLoadoutAmt Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getLoadoutAmt() {
		return loadoutAmt;
	}

	/**
	 * Method setLoadoutAmt Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loadoutAmt
	 */
	public void setLoadoutAmt(int[] loadoutAmt) {
		this.loadoutAmt = loadoutAmt;
	}

	/**
	 * Method processSwitchReqs Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void processSwitchReqs() {
		for (int i = 0; i < switchReqs.length; i++) {
			if (switchReqs[i]) {
				switchReqs[i] = false;
				wieldItem(i);
			}
		}
	}

	/**
	 * Method isBinded Created on 14/10/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param slot
	 *
	 * @return
	 */
	public boolean isBinded(int slot) {
		return dungBindedItems[slot] != 0;
	}

	/**
	 * Method bind Created on 14/10/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param itemid
	 * @param slot
	 */
	public void bind(int itemid, int slot) {
		this.dungBindedItems[slot] = itemid;
	}

	/**
	 * Method getBindedItems Created on 14/10/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getBindedItems() {
		return dungBindedItems;
	}

	/**
	 * Method getBindedItemsCount Created on 14/10/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getBindedItemsCount() {
		int x = 0;

		for (int i = 0; i < dungBindedItems.length; i++) {
			if (dungBindedItems[i] != 0) {
				x++;
			}
		}

		return x;
	}

	/**
	 * Method setDungBindedItems Created on 14/10/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param bindedItems
	 * @param c
	 */
	public void setDungBindedItems(int[] bindedItems, int c) {
		this.dungBindedItems = bindedItems;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public boolean hasAnyArmour() {
		for (int i = 0; i < 14; i++) {
			if (isEquipped(i) && (i != 3)) {
				return true;
			}
		}

		Item[] items = player.getInventory().getItems();

		for (int i = 0; i < items.length; i++) {
			if ((items[i] != null) && ((items[i].getWieldSlot() != 3) && (items[i].getWieldSlot() != ARROWS))) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public boolean hasLoadout() {
		for (int i = 0; i < loadout.length; i++) {
			if (loadout[i] > 0) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Method description
	 *
	 *
	 * @param file
	 */
	public void saveLoadout(Buffer file) {
		file.writeByte(1); // / version as i plan to change this later

		for (int i = 0; i < 14; i++) {
			file.writeWord(loadout[i]);
			file.writeDWord(loadoutAmt[i]);
		}
	}

	/**
	 * Method description
	 *
	 *
	 * @param file
	 */
	public void loadLoadout(Buffer file) {
		int version = file.readByte();

		if (version == 1) {
			for (int i = 0; i < 14; i++) {
				loadout[i] = file.readShort();
				loadoutAmt[i] = file.readDWord();

			}
		}
	}

	/**
	 * Method description
	 *
	 */
	public void saveLoadout() {
		for (int i = 0; i < loadout.length; i++) {
			if (items[i] == null) {
				loadout[i] = -1;
			} else {
				loadout[i] = items[i].getItemId();
				loadoutAmt[i] = items[i].getAmount();
			}
		}
	}

	/**
	 * Method description
	 *
	 */
	public void loadLoadout() {
		for (int i = 0; i < loadout.length; i++) {
			if (items[i] == null) {
				if (loadout[i] > 0 && loadout[i] != 65535) {
					int amt = (player.getBank().getAmount(Item.forId(loadout[i])));

					if (!checkWield(loadout[i], i)) {
						continue;
					}

					if (loadoutAmt[i] < amt) {
						amt = loadoutAmt[i];
					}

					if (amt > 0) {
						player.getBank().deleteItem(loadout[i], amt);
						items[i] = new EquippedItem(loadout[i], amt);
						updateEquipment(i);
						player.getActionSender()
								.sendMessage("[LOADOUT]: Loaded " + Item.forId(loadout[i]).getName() + " ");
					} else {
						player.getActionSender()
								.sendMessage("Unable to find item in bank: " + Item.forId(loadout[i]).getName());
					}
				}
			}
		}
	}

	/**
	 * Method description
	 *
	 */
	public void checkDegradeables() {
		Item theItem;

		for (int i = 0; i < 14; i++) {
			if (isEquipped(i)) {
				theItem = Item.forId(getId(i)); //

				if (theItem == null) {
					Logger.log("error: " + getId(i));

					continue;
				}

				if (theItem.isDegradeable()) {
					if (theItem.getDegradeable().getMain() == getId(i)) {
						int next = theItem.getDegradeable().next(theItem.getIndex());

						set(next, 1, i);
						updateEquipment(i);
						player.getActionSender().sendMessage("Your " + theItem.getName() + " has taken damage.");
						player.getAccount().addDegrading(new Degrading(getId(i), theItem.getDegradeable().degradeTime));
					} else {
						Degrading d = player.getAccount().getDegrading(theItem.getIndex());

						if (d == null) {
							d = new Degrading(theItem.getIndex(), theItem.getDegradeable().degradeTime);
							player.getAccount().addDegrading(d);
						}

						if (d.next() == 0) {
							int nextItem = theItem.getDegradeable().next(theItem.getIndex());

							if ((nextItem == theItem.getDegradeable().getLast())
									&& (theItem.getDegradeable().getPolicy() == DegradeableItem.POLICY_DEGRADE)) {
								removeItem(i);
								updateEquipment(i);
								player.getInventory().addItemDrop(Item.forId(nextItem), 1);
								player.getAccount().removeDegrading(d);
								player.getActionSender()
										.sendMessage("Your "
												+ Item.forId(Item.forId(nextItem).getDegradeable().getMain()).getName()
												+ " has degraded completely, see horvik to get it repaired");

								continue;
							}

							if (nextItem == -1) {
								player.getActionSender().sendMessage("Your item has been destroyed.");
								player.getAccount().removeDegrading(d);
								removeItem(i);
								updateEquipment(i);
							} else {
								set(nextItem, 1, i);
								updateEquipment(i);
								player.getAccount().removeDegrading(d);
								player.getAccount().addDegrading(
										new Degrading(nextItem, Item.forId(nextItem).getDegradeable().degradeTime));
								player.getActionSender().sendMessage("Your interfaces has degraded a tad more.");
							}
						} else {
						}
					}
				}
			}
		}
	}

	/**
	 * Method description
	 *
	 *
	 * @param old
	 * @param newW
	 */
	public void weaponChanged(int old, int newW) {
		Weapon newWep = Item.forId(newW).getWeaponHandler();

		if (newWep != null) {
			if (player.getAccount().get(43) >= newWep.length()) {
				player.getAccount().set(43, 2, true);
			}
		}

		if (Core.currentTime == lastWepChange) {
			// return; //TODO: Check if this is causing the delay
		}

		if (old == -1) {
			return;
		}

		if (player.getAccount().get(Account.AUTO_RETALIATE) == 1 && newW != 4153) {
			if (player.getCombatAdapter().getFightingWith() != null) {
				player.getCombatAdapter().reset();
				player.unFollow();
				player.getRequest().close();
			} else {
				player.getRequest().close();
			}
		}

		Weapon oldWep = Item.forId(old).getWeaponHandler();

		if (player.getAutoCast() == -1) {
			player.setRequestedSpell(-1);
		}

		lastWepChange = Core.currentTime;

		if ((old != -1) && (newW != -1)) {
			if ((Item.forId(old).getWeaponHandler() != null) && (Item.forId(newW).getWeaponHandler() != null)) {
				player.getCombatAdapter().switchWeapons(Item.forId(old).getWeaponHandler().getSpeed(),
						Item.forId(newW).getWeaponHandler().getSpeed());
			}
		}
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public Weapon getWeapon() {
		if (isEquipped(3)) {
			Weapon r = Item.forId(getId(3)).getWeaponHandler();

			if (r == null) {
				return punching;
			}

			return r;
		}

		return punching;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public boolean fullVeracs() {
		return (getId(HAT) == 4753) && (getId(LEGS) == 4759) && (getId(TORSO) == 4757) && (getId(3) == 4755);
	}

	/**
	 * Method description
	 *
	 *
	 * @param i
	 * @param amount
	 * @param slot
	 */
	public void set(int i, int amount, int slot) {
		items[slot] = new EquippedItem(i, amount);
	}

	/**
	 * Method description
	 *
	 *
	 * @param slot
	 */
	public void removeItem(int slot) {
		items[slot] = null;
	}

	/**
	 * Method description
	 *
	 *
	 * @param id
	 *
	 * @return
	 */
	public boolean contains(int id) {
		for (int i = 0; i < items.length; i++) {
			if (isEquipped(i) && (getId(i) == id)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public boolean wearingVigour() {
		return getId(RING) == 19669;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public int getDamageType() {
		return getWeapon().getAttackStyle(player.getAccount().get(43)).getDamageType();
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public int getCurrentFightStyle() {
		if (getWeapon() == null) {
			return 0;
		}

		if (getWeapon().getAttackStyle(player.getAccount().get(43)) == null) {
			Logger.err("ERROR DODGY ATTACK STYLE: " + getId(3) + " " + player.getAccount().get(43));
			player.getAccount().set(43, 2, true);

			return 0;
		}

		return getWeapon().getAttackStyle(player.getAccount().get(43)).getMode();
	}

	/**
	 * Method getTorvaBonus Created on 14/12/06
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getTorvaBonus() {
		int ret = 0;

		if ((getId(TORSO) == 20141) || (getId(TORSO) == 20163) || (getId(TORSO) == 20151) || (getId(TORSO) == 20139)
				|| (getId(TORSO) == 20151) || (getId(TORSO) == 20161)) {
			ret += 15;
		}

		if ((getId(LEGS) == 20145) || (getId(LEGS) == 20155) || (getId(LEGS) == 20167) || (getId(LEGS) == 20157)
				|| (getId(LEGS) == 20143) || (getId(LEGS) == 20155) || (getId(LEGS) == 20167)) {
			ret += 10;
		}

		if ((getId(HAT) == 20137) || (getId(HAT) == 20136) || (getId(HAT) == 20159) || (getId(HAT) == 20161)
				|| (getId(HAT) == 20147) || (getId(HAT) == 20149) || (getId(HAT) == 20159)) {
			ret += 6;
		}

		return ret;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public int[] getTotalBonuses() {
		int[] bonus = new int[16];
		int dfsBonus = 0;

		if (isEquipped(SHIELD) && (getId(SHIELD) == 11283 || getId(SHIELD) == 11284 || getId(SHIELD) == 11285)) {
			int charges = player.getAccount().getInt32("dfs_charges");

			if (player.getAccount().hasVar("dfs_charges")) {
				dfsBonus += player.getAccount().getInt32("dfs_charges");
			}
		}

		for (int i = 0; i < 14; i++) {
			if (isEquipped(i)) {
				Item item = Item.forId(getId(i));

				if (item != null) {
					if (item.hasBonus()) {
						for (int x = 0; x < 16; x++) {
							bonus[x] += item.getBonus(x);

						}
					}
				}
			}
		}

		for (int x = 0; x < 16; x++) {
			if ((x >= Item.BONUS_DEFENCE_CRUSH) && (x <= Item.BONUS_DEFENCE_STAB)) {
				bonus[x] += dfsBonus;
			}
		}

		return bonus;
	}

	/**
	 * Method description
	 *
	 */
	public void updateWeapon() {
		Weapon w = getWeapon();

		player.getWindowManager().setSidebar(0, w.getWeaponInterface());
		SpecialAttacks.initializeSpecial(player.getWindowManager().getSidebar(0), player);

		if (player.isSpecialHighlighted()) {
			SpecialAttacks.changeHighlight(player);
		} else {
			SpecialAttacks.updateHighlight(player);
		}
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public boolean fullGuthans() {
		return ((getId(WEAPON) == 4915) || (getId(WEAPON) == 4914) || (getId(WEAPON) == 4913) || (getId(WEAPON) == 4912)
				|| (getId(WEAPON) == 4911) || (getId(WEAPON) == 4910) || (getId(WEAPON) == 4726))
				&& ((getId(TORSO) == 4921) || (getId(TORSO) == 4920) || (getId(TORSO) == 4919) || (getId(TORSO) == 4918)
						|| (getId(TORSO) == 4917) || (getId(TORSO) == 4916) || (getId(TORSO) == 4728))
				&& ((getId(LEGS) == 4927) || (getId(LEGS) == 4926) || (getId(LEGS) == 4925) || (getId(LEGS) == 4924)
						|| (getId(LEGS) == 4923) || (getId(LEGS) == 4922) || (getId(LEGS) == 4730))
				&& ((getId(HAT) == 4909) || (getId(HAT) == 4908) || (getId(HAT) == 4907) || (getId(HAT) == 4906)
						|| (getId(HAT) == 4905) || (getId(HAT) == 4904) || (getId(HAT) == 4724));
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public boolean fullDharok() {
		return ((getId(TORSO) == 4720) || (getId(TORSO) == 4892) || (getId(TORSO) == 4893) || (getId(TORSO) == 4894)
				|| (getId(TORSO) == 4895))
				&& ((getId(WEAPON) == 4718) || (getId(WEAPON) == 4886) || (getId(WEAPON) == 4887)
						|| (getId(WEAPON) == 4888) || (getId(WEAPON) == 4889))
				&& ((getId(LEGS) == 4722) || (getId(LEGS) == 4898) || (getId(LEGS) == 4899) || (getId(LEGS) == 4900)
						|| (getId(LEGS) == 4901))
				&& ((getId(HAT) == 4716) || (getId(HAT) == 4880) || (getId(HAT) == 4881) || (getId(HAT) == 4882)
						|| (getId(HAT) == 4883) || (getId(HAT) == 4884));
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public int getArrowsCount() {
		return getAmount(ARROWS);
	}

	/**
	 * Method clear Created on 14/10/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void clear() {
		this.items = new EquippedItem[14];
	}

	/**
	 * Method description
	 *
	 */
	public void removeArrow() {
		if (this.getId(ARROWS) != -1) {
			items[ARROWS].setAmount(getAmount(ARROWS) - 1);

			if (getAmount(ARROWS) <= 0) {
				items[ARROWS] = null;
			}

			updateEquipment(ARROWS);
		}
	}

	/**
	 * Method description
	 *
	 */
	public void removeWeapon() {
		if (this.getId(WEAPON) != -1) {
			items[WEAPON].setAmount(getAmount(WEAPON) - 1);

			if (getAmount(WEAPON) <= 0) {
				items[WEAPON] = null;
				player.getAppearance().setChanged(true);
			}

			updateEquipment(WEAPON);
		}
	}

	/**
	 * Method description
	 *
	 *
	 * @param id
	 * @param name
	 *
	 * @return
	 */
	public static int getInterfaceIdForWeapon(int id, String name) {
		if ((id == -1) || (name == null)) {
			return 5855;
		}

		if (name.contains("whip")) {
			return 12290;
		}

		Weapon w = Item.forId(id).getWeaponHandler();

		if ((w != null) && w.isTwoHanded() && name.contains("sword")) {
			return 4705;
		}

		if (name.contains("maul") || name.contains("hammer") || name.toLowerCase().contains("anchor")) {
			return 425;
		}

		if (name.contains("claws")) {
			return 7762;
		}

		if (name.contains("bow")) {
			return 1764;
		}

		if (name.toLowerCase().contains("staff") || name.toLowerCase().contains("wand")) {
			if (id != 15116) {
				return 6103;
			}
		}

		if ((id == 6522) || name.contains("dart") || name.contains("knife") || name.contains("javelin")
				|| name.contains("thrown") || name.toLowerCase().contains("chinchompa")) {
			return 4446;
		}

		if (name.contains("dagger")) {
			return 2276;
		}

		if (name.contains("pickaxe")) {
			return 5570;
		}

		if (name.contains("dharok") || name.contains("hatchet") || name.contains("axe")) {
			return 1698;
		}

		if (name.contains("halberd")) {
			return 8460;
		}

		if (name.contains("spear")) {
			return 4679;
		}

		if (name.contains("mace") || name.contains("flail")) {
			return 3796;
		}

		return 2423;
	}

	/**
	 * Method description
	 *
	 *
	 * @param amount
	 *
	 * @return
	 */
	public int getItemSlot(int amount) {
		for (int i = 0; i < items.length; i++) {
			if ((items[i] != null) && (items[i].getItemId() == amount)) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public Item[] getList() {
		Item[] list = new Item[this.items.length];

		for (int i = 0; i < this.items.length; i++) {
			if (isEquipped(i)) {
				list[i] = Item.forId(getId(i));
			}
		}

		return list;
	}

	/**
	 * Method description
	 *
	 *
	 * @return
	 */
	public int[] getAmounts() {
		int[] amts = new int[this.items.length];

		for (int i = 0; i < this.items.length; i++) {
			amts[i] = getAmount(i);
		}

		return amts;
	}

	/**
	 * Method description
	 *
	 *
	 * @param slot
	 */
	public void updateEquipment(int slot) {
		if (getId(slot) == -1) {
			player.getActionSender().setItem(null, 0, slot, 94);
		} else {
			this.player.getActionSender().setItem(Item.forId(getId(slot)), getAmount(slot), slot, 94);
		}

		if (slot == 3) {
			updateWeapon();
		}

		if (player.getGameFrame().isWindowOpen(GameFrame.INTER_EQUIPSCREEN)) {
			GameFrame.getInterface(GameFrame.INTER_EQUIPSCREEN).interfaceOpened(player);
		}

		if (slot != ARROWS) {
			player.getAppearance().setChanged(true);
		}
	}

	/**
     * Method description
     *
     *
     * @return
     */
    public double getWeight() {
        double weight = 0;

        for (int i = 0; i < 14; i++) {
        	int id = getId(i);
			if (id == -1) {
				continue;
			}

			Item ix = Item.forId(id);
			weight += ix.getWeight();
		}

        return weight;
    }

	/**
	 * Method description
	 *
	 *
	 * @param slot
	 *
	 * @return
	 */
	public boolean isWearing(int slot) {
		return items[slot] != null;
	}

	/**
	 * Method description
	 *
	 *
	 * @param itemId
	 * @param slot
	 *
	 * @return
	 */
	public boolean checkUnwield(int itemId, int slot) {
		if (World.getWorld().getScriptManager().isBlocked(player, Trigger.UNWIELD_ITEM, itemId)) {
			return false;
		}

		if (slot == HAT || slot == CAPE) {
			if (CastleWarsEngine.getSingleton().isPlaying(player)) {
				return false;
			}
		}

		if (itemId == 4037 || itemId == 4039) {
			this.removeWeapon();
			if (CastleWarsEngine.getSingleton().isPlaying(player)) {
				CastleWarsEngine.getSingleton().notifyFlag(player);
			}
			return false;
		}
		if (itemId == 4900) {
			return false;
		}

		if (player.isInZone("champions_basement") && ((slot != 3) && (slot != Equipment.SHIELD))) {
			player.getActionSender().sendMessage("You cannot switch armour during a champions match.");

			return false;
		}

		if ((player.getGameFrame().getDuel() != null) && player.getGameFrame().getDuel().setting(Duel.FUN_WEAPONS)
				&& (slot == 3)) {
			return false;
		}

		if (player.getZombiesGame() != null) {
			player.getActionSender().sendMessage("You can't switch weapons or equipment during zombie games");

			return false;
		}

		return true;
	}

	/**
	 * Method description
	 *
	 *
	 * @param itemId
	 * @param slot
	 *
	 * @return
	 */
	public boolean checkWield(int itemId, int slot) {
		if ((getId(3) == 4037) && (slot == 3)) {
			return false;
		}

		if (itemId >= MASTER_CAPES[0]) {
			for (int i = 0; i < MASTER_CAPES.length; i++) {
				if (MASTER_CAPES[i] == itemId) {
					if (player.getExps()[i] < 500000000) {
						player.getActionSender()
								.sendMessage("You need atleast 500m exp in " + SkillGuide.SKILL_NAMES[i]);
						return false;
					}
				}
			}
		}
		if (slot == HAT || slot == CAPE) {
			if (CastleWarsEngine.getSingleton().isPlaying(player)) {
				return false;
			}
		}

		if ((itemId == 4413) && !CompletionistCape.canWear(player)) {
			if (!player.getAccount().hasVar("comphax")) {
				CompletionistCape.showStats(player, CompletionistCape.getMenu(player));

				if (player.getRights() < Player.DEVELOPER) {
					return false;
				}
			}
		}
		if ((itemId == 20454) && !PKCompletionistCape.canWear(player)) {
			PKCompletionistCape.showStats(player, PKCompletionistCape.getMenu(player));

			if (player.getRights() < Player.DEVELOPER) {
				return false;
			}
		}

		if (itemId == 4411) {
			for (int i = 0; i < 24; i++) {
				if (player.getMaxStat(i) < 99) {
					player.getActionSender().sendMessage("You need 99 in all skills.");

					return false;
				}
			}
		}

		if ((itemId == 15117) || (itemId == 15118)) {
			int total = (int) GameMath.getPercentFromTotal(player.getAchievementsCompletedTotal(),
					Achievement.getTotal());

			if (total <= 80) {
				player.getActionSender().sendMessage("You neeed to complete atleast 80% of all active achievements");
				player.getActionSender().sendMessage("to wear this cape.");
				player.getActionSender()
						.sendMessage("NOTE: if more achievements are added, you will need to complete them.");

				return false;
			}
		}

		if (player.isInZone("champions_basement") && (slot != 3)) {
			return false;
		}

		if ((player.getCurrentHouse() != null) && player.getCurrentHouse().isInBoxingRing(player)) {
			if ((itemId != 7671) && (itemId != 7673)) {
				player.getActionSender().sendMessage("You can't wear anything but boxing gloves in a boxing ring.");

				return false;
			}
		}

		if ((player.getGameFrame().getDuel() != null) && player.getGameFrame().getDuel().setting(Duel.FUN_WEAPONS)) {
			return false;
		}

		if ((player.getCurrentHouse() != null) && player.getCurrentHouse().isInFencingRing(player)) {
			if (slot != 3) {
				player.getActionSender().sendMessage("You can only wear weapons in a fencing ring..");

				return false;
			}
		}

		if ((player.getGameFrame().getDuel() != null)
				&& (player.getGameFrame().getDuel().getStatus() == Duel.STATUS_IN_FIGHT)) {
			if (!player.getGameFrame().getDuel().canWield(slot)
					|| ((slot == 3) && (Item.forId(itemId).getWeaponHandler() != null)
							&& Item.forId(itemId).getWeaponHandler().isTwoHanded()
							&& !player.getGameFrame().getDuel().canWield(Equipment.SHIELD))) {
				player.getActionSender().sendMessage("The duel settings prevent you from wearing this equipment");

				return false;
			}
		}

		if (!Item.level_check(Item.forId(itemId), player)) {
			return false;
		}

		if ((player.getZombiesGame() != null) && (slot == 3)) {
			player.getActionSender().sendMessage("You cant switch weapons in zombies.");

			return false;
		}

		if (World.getWorld().getScriptManager().isBlocked(player, Trigger.WIELD_ITEM, itemId)) {
			return false;
		}

		return true;
	}

	/**
	 * Method description
	 *
	 *
	 * @param slot
	 *
	 * @return
	 */
	public boolean unwieldItem(int slot) {
		if (!isWearing(slot)) {
			return false;
		}

		Item theItem = Item.forId(items[slot].getItemId());

		if (!checkUnwield(theItem.getIndex(), slot)) {
			return false;
		}

		player.log("unwield item " + Item.forId(getId(slot)));

		if (theItem.isStackable()) {
			int inventorySlot = player.getInventory().getItemSlot(theItem);

			if (inventorySlot != -1) {
				if (player.getInventory().addItem(theItem, getAmount(slot))) {
					items[slot] = null;
					player.getAppearance().setChanged(true);
					updateEquipment(slot);

					// send data here
					return true;
				}

				if (player.getInventory().getNextSlot() != -1) {
					player.getInventory().addItem(theItem, getAmount(slot));
					player.getAppearance().setChanged(true);
					updateEquipment(slot);

					// send data here
					return true;
				}

				player.getActionSender().sendMessage("You don't have enough free slots in your inventory.");

				return false;
			}

			if (player.getInventory().getNextSlot() != -1) {
				player.getInventory().addItem(theItem, getAmount(slot));
				items[slot] = null;
				player.getAppearance().setChanged(true);
				updateEquipment(slot);

				// send data here
			} else {
				player.getActionSender().sendMessage("You don't have enough free slots");

				return false;
			}
		} else {
			if (player.getInventory().getNextSlot() != -1) {
				player.getInventory().addItem(getId(slot), getAmount(slot));
				items[slot] = null;
				updateEquipment(slot);
				player.getAppearance().setChanged(true);

				return true;
			} else {
				player.getActionSender().sendMessage("You don't have enough free slots in your inventory.");
			}
		}

		return false;
	}

	/**
	 * Method description
	 *
	 *
	 * @param slot
	 * @param itemId
	 * @param amt
	 */
	public void forceWield(int slot, int itemId, int amt) {
		this.items[slot] = new EquippedItem(itemId, amt);
		updateEquipment(slot);
	}

	/**
	 * Method description
	 *
	 *
	 * @param slot
	 *
	 * @return
	 */
	public boolean wieldItem(int slot) {
		Item theItem = player.getInventory().getItem(slot);
		int amount = player.getInventory().getCount(slot);

		if (theItem == null) {
			return false;
		}

		if (theItem.isDungItem() && !player.isInArea(Dungeon.dungeoneering)) {
			player.getActionSender().sendMessage("DUNG SMUGGLE DETECTED");
			player.getInventory().deleteItem(theItem.getIndex(), 10000);
			return false;
		}

		if (isWearing(theItem.getWieldSlot()) && !checkUnwield(getId(theItem.getWieldSlot()), theItem.getWieldSlot())) {
			System.err.println("error");
			return false;
		}

		if ((theItem.getIndex() == 4749) && (player.getAppearance().getGender() == 1)) {
			return false;
		}

		swapSlot = slot;

		if ((theItem.getWieldSlot() == 3) && (theItem.getWeaponHandler() == null)) {
			return false;
		}

		if (!checkWield(theItem.getIndex(), theItem.getWieldSlot())) {
			return false;
		}

		player.log("Wield item: " + theItem.getName());

		int targetSlot = theItem.getWieldSlot();
		Weapon w = theItem.getWeaponHandler();

		if (w == null) {
			w = punching;
		}

		if (targetSlot == WEAPON) {
			if (isEquipped(WEAPON)) {
				boolean takeShield = false;

				if (theItem.getWeaponHandler().isTwoHanded() && isEquipped(SHIELD)) {
					if (player.getInventory().getFreeSlots(0) < 1) {
						player.getActionSender().sendMessage("Not enough space to switch");

						return true;
					}

					takeShield = true;
				}

				if (theItem.isStackable() && (getId(3) == theItem.getIndex())) {
					this.items[3].amount += player.getInventory().getCount(slot);
					updateEquipment(targetSlot);
					player.getInventory().deleteItemFromSlot(null, Integer.MAX_VALUE, slot);

					return true;
				}

				int old = getId(targetSlot);
				int tmp = player.getInventory().getCount(slot);

				player.getInventory().replaceItem(slot, getId(3), getAmount(3));
				set(theItem.getIndex(), tmp, targetSlot);
				updateEquipment(targetSlot);
				updateWeapon();
				player.getAppearance().setChanged(true);

				if (old != -1) {
					weaponChanged(old, theItem.getIndex());
				}

				if (takeShield) {
					player.getInventory().addItem(getId(SHIELD), 1);
					removeItem(SHIELD);
					updateEquipment(SHIELD);
				}

				return true;
			}
		} else if ((targetSlot != Equipment.SHIELD)
				|| (!isEquipped(3) || (isEquipped(3) && !Item.forId(getId(3)).getWeaponHandler().isTwoHanded()))) {
			if (isEquipped(targetSlot)) {
				int tmp = getId(targetSlot);
				int tmpAmount = getAmount(targetSlot);

				if ((getId(targetSlot) == theItem.getIndex()) && theItem.isStackable()) {
					items[targetSlot].amount += player.getInventory().getCount(slot);
					updateEquipment(targetSlot);
					player.getInventory().deleteItem(theItem.getIndex(), Integer.MAX_VALUE);

					return true;
				} else {
					if (checkUnwield(getId(targetSlot), targetSlot)) {
						int c = player.getInventory().getCount(slot);

						player.getInventory().replaceItem(slot, getId(targetSlot), getAmount(targetSlot));
						this.set(theItem.getIndex(), c, targetSlot);
						updateEquipment(targetSlot);

						return true;
					} else {
						return false;
					}
				}
			}
		} else {
			if (checkUnwield(getId(3), 3)) {
				int tmp = getId(3);
				int tmpAmount = getAmount(3);

				this.set(theItem.getIndex(), 1, Equipment.SHIELD);
				updateEquipment(Equipment.SHIELD);
				player.getInventory().replaceItem(slot, tmp, tmpAmount);
			} else {
				return false;
			}
		}

		if (theItem.isDungItem() && (player.getDungeon() == null)) {
			player.getInventory().deleteItem(theItem, 1);
			player.getActionSender().sendMessage("Dung smuggle detected Your IP has been sent to customer support.");

			return false;
		}

		if ((targetSlot == 3) && (w != null) && w.isTwoHanded() && isEquipped(SHIELD)) {
			int slotsNeeded = 0;

			if (isEquipped(3)) {
				slotsNeeded++;
			}

			if ((player.getInventory().getFreeSlots(getId(3)) + 1) < slotsNeeded) {
				player.getActionSender().sendMessage("You don't have enough free slots");

				return false;
			}

			int wep = getId(3);

			if (isEquipped(3) && !unwieldItem(3)) {
				return false;
			}

			player.getInventory().deleteItemFromSlot(theItem, player.getInventory().getItemsCount()[slot], slot);

			if (!unwieldItem(SHIELD)) {
				return false;
			}

			weaponChanged(wep, w.getModel());
			set(theItem.getIndex(), amount, targetSlot);
			updateEquipment(targetSlot);

			return true;
		}

		if ((targetSlot == SHIELD) && isWearing(3) && getWeapon().isTwoHanded()) {
			player.getInventory().deleteItemFromSlot(theItem, amount, slot);

			if (!unwieldItem(3)) {
				player.getInventory().addItem(theItem, amount);

				return false;
			}

			set(theItem.getIndex(), amount, targetSlot);
			updateEquipment(targetSlot);

			return true;
		}

		if (isWearing(targetSlot)) {
			if (Item.forId(getId(targetSlot)).isStackable() && (theItem.getIndex() == getId(targetSlot))) {
				this.set(getId(targetSlot), getAmount(targetSlot) + amount, targetSlot);
				player.getInventory().deleteItemFromSlot(null, amount, slot);

				return true;
			}

			if (targetSlot == 3) {
				if (isWearing(SHIELD) && w.isTwoHanded()) {
					if (unwieldItem(SHIELD)) {
						player.getInventory().deleteItemFromSlot(theItem, amount, slot);
						this.items[targetSlot] = new EquippedItem(theItem.getIndex(), amount);
						player.getAppearance().setChanged(true);
						updateEquipment(slot);

						// send data here
						return true;
					}
				}
			} else if ((targetSlot == SHIELD) && isWearing(3) && getWeapon().isTwoHanded()) {
				if (!checkUnwield(getId(3), 3)) {
					return false;
				}

				this.player.getInventory().deleteItemFromSlot(theItem, amount, slot);
				this.items[targetSlot] = new EquippedItem(theItem.getIndex(), amount);
				this.player.getInventory().addItem(this.getId(3), this.getAmount(3));
				this.items[3] = null;
				updateEquipment(3);
				updateEquipment(targetSlot);
				player.getAppearance().setChanged(true);

				// send data here
				return true;
			}

			if (!this.checkUnwield(getId(targetSlot), targetSlot)) {
				return false;
			}

			this.player.getInventory().deleteItemFromSlot(theItem, amount, slot);

			if (isWearing(targetSlot)) {
				this.player.getInventory().addItem(getId(targetSlot), getAmount(targetSlot));

				if (targetSlot == 3) {
					weaponChanged(getId(targetSlot), theItem.getIndex());
				}
			}

			this.items[targetSlot] = new EquippedItem(theItem.getIndex(), amount);
			updateEquipment(targetSlot);
			player.getAppearance().setChanged(true);

			// send data here
			return true;
		}

		this.items[targetSlot] = new EquippedItem(theItem.getIndex(), amount);
		this.player.getInventory().deleteItemFromSlot(theItem, amount, slot);
		this.player.getAppearance().setChanged(true);
		updateEquipment(targetSlot);

		// send data here
		return true;
	}

	/**
	 * Method description
	 *
	 *
	 * @param type
	 *
	 * @return
	 */
	public int getId(int type) {
		if (!isEquipped(type)) {
			return -1;
		}

		return items[type].getItemId();
	}

	/**
	 * Method description
	 *
	 *
	 * @param type
	 *
	 * @return
	 */
	public int getAmount(int type) {
		if (!isEquipped(type)) {
			return -1;
		}

		return items[type].getAmount();
	}

	/**
	 * Method description
	 *
	 *
	 * @param type
	 *
	 * @return
	 */
	public boolean isEquipped(int type) {
		if ((items[type] != null) && (items[type].getItemId() == -1)) {
			items[type] = null;
		}

		return items[type] != null;
	}

	/**
	 * Class description Hydrascape 639 Game server Copyright (C) Tazogaming
	 * 2014
	 *
	 *
	 * @version Enter version here..., 14/08/18
	 * @author Daniel Hadland
	 */
	static class EquippedItem {

		/** itemId, amount made: 14/10/19 */
		private int itemId, amount;

		/**
		 * Constructs ...
		 *
		 *
		 * @param id
		 * @param a
		 */
		public EquippedItem(int id, int a) {
			this.itemId = id;
			this.amount = a;
		}

		/**
		 * Method description
		 *
		 *
		 * @return
		 */
		public int getItemId() {
			return itemId;
		}

		/**
		 * Method description
		 *
		 *
		 * @param itemId
		 */
		public void setItemId(int itemId) {
			this.itemId = itemId;
		}

		/**
		 * Method description
		 *
		 *
		 * @return
		 */
		public int getAmount() {
			return amount;
		}

		/**
		 * Method description
		 *
		 *
		 * @param amount
		 */
		public void setAmount(int amount) {
			this.amount = amount;
		}
	}
}
