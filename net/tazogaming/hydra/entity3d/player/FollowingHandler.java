package net.tazogaming.hydra.entity3d.player;

import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/10/14
 * Time: 00:26
 */
public abstract class FollowingHandler<T extends Killable> {
    private Killable following;
    private T entity;

    protected abstract boolean isWithinDistance(T t, Point location, Killable following);


    private boolean canMove(){
        if(entity.getRunDirection() == -1)
            return true;
        return false;
    }
    public boolean updateMovement() {

        if(!canMove() || isWithinDistance(entity, entity.getLocation(), following)) {
            return false;
        }



        return false;

    }
}
