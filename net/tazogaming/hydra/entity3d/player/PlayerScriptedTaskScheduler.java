package net.tazogaming.hydra.entity3d.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.sched.ScheduledScriptTaskRegister;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/08/14
 * Time: 21:01
 */
public class PlayerScriptedTaskScheduler extends ScheduledScriptTaskRegister<Player> {
    @Override
    protected void saveTasks(byte[] buffer, Player player) {
        player.getAccount().setObject("task_buffer", buffer, false);
    }
}
