package net.tazogaming.hydra.entity3d.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Degrading;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.RentedItem;
import net.tazogaming.hydra.game.Referral;
import net.tazogaming.hydra.game.skill.combat.combat.PVPKill;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.game.ui.PlayerGrandExchange;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.game.minigame.hitman.HitmanGame;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.skill.construction.RoomConfig;
import net.tazogaming.hydra.game.skill.construction.costumeroom.RoomBox;
import net.tazogaming.hydra.game.skill.farming2.PlayerGrew;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.collections.Array;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 14:58
 * To change this template use File | Settings | File Templates.
 */
public class Account {
    public static final int
        SETTING_FIGHT_STYLE            = 43,
        AUTO_RETALIATE                 = 172,
        SPLIT_PRIVATE_CHAT             = 287,
        MOUSE_BUTTONS                  = 170,
        ACCEPT_AID                     = 427,
        BRIGHTNESS                     = 166,
        MOVE_RUN                       = 173,
        MUSIC_LEVEL                    = 168,
        SFX_LEVEL                      = 169,
        BANK_WITHDRAW_MODE             = 115,
        XP_COUNTER                     = 117,
        TUTORIAL_STAGE                 = 281;
    public static final int
        XP_LOCK                        = 0,
        LAST_USED_SLOT                 = 2,
        TAG_BOX_ACTIVE                 = 3,
        INFINITE_RUN                   = 10,
        CHAIR_SITTING_ID               = 11,
        DOUBLE_XP                      = 12,
        DOUBLE_XP_2                    = 13;
    public static final int
        PRAYER_DISABLED                = 0,
        EATING_DISABLED                = 1,
        RUNNING_DISABLED               = 2,
        TELEPORT_DISABLED              = 3,
        TALKING_DISABLED               = 4,
        SPELLS_DISABLED                = 5,
        COMBAT_DISABLED                = 6;
    public static final int
        RUN_STATUS                     = 119,
        PRAYER_TYPE                    = 1584;
    public static final int BOSS_DELAY = Core.getTicksForMinutes(5);
    public static final int
        WILDERNESS                     = 0,
        RED_PORTAL                     = 1,
        WHITE_PORTAL                   = 2;

    private int stakeWins = 0;

    private Point lastBountyLocation;
    private int leftClickSummoningOption = 0;

    public Point getLastBountyLocation() {
        return this.lastBountyLocation;
    }
    public void setLastBountyLocation(Point p){
        this.lastBountyLocation = p;
    }
    private List<Long> restrainedUsers = new ArrayList<Long>();

    public void restrain(Player admin, Player player){
        this.player.getActionSender().sendMessage(Text.RED("Mod "+admin.getUsername()+" has placed a restraining order on you against player "+player.getUsername()));
        this.player.getActionSender().sendMessage(Text.RED("You are now permanently blocked from fighting this player in edgeville and vice versa."));
        this.restrainedUsers.add(player.getUsernameHash());
    }

    public List<Long> getRestraints() {
        return this.restrainedUsers;
    }
    public void removeRestrain(Player plr){
        this.restrainedUsers.remove(plr.getUsernameHash());
    }

    public boolean isRestrained(Player player){
        return this.restrainedUsers.contains(player.getUsernameHash());
    }

    public int getLeftClickSummoningOption() {
        return leftClickSummoningOption;
    }

    public void setLeftClickSummoningOption(int opt){
        this.leftClickSummoningOption = opt;
    }


    private boolean autosaveRequested = false;

    public boolean isSaveRequested() {
        return autosaveRequested;
    }
    public void setSaveRequested(boolean yes){
        this.autosaveRequested = yes;
    }
    private Referral referral;

    public void setReferral(Referral referral){
        this.referral = referral;
    }

    public Referral getReferral() {
        return referral;
    }

    public void addStakeWin() {
        stakeWins ++;
    }

    public void setStakeWins(int wins){
        this.stakeWins = wins;
    }

    public int getStakeWins() {
        return stakeWins;
    }

    public boolean isPkMode() {
        return pkMode;
    }

    public void setPkMode(boolean pkMode) {
        this.pkMode = pkMode;
    }

    private boolean pkMode = false;
    private boolean x10Hits = true;

    public boolean isX10Hits() {
        return x10Hits;
    }

    public void setx10hits(boolean set) {
        this.x10Hits = set;
    }
    /** yellBanTime made: 14/10/02 **/
    private long yellBanTime = 0;

    /** mutedTime made: 14/10/02 **/
    private long mutedTime = 0;

    /** wildyBanTime made: 14/10/02 **/
    private long wildyBanTime = 0;

    /** zombieKills made: 14/09/26 */
    private int zombieKills = 0;

    /** zombieScore made: 14/09/26 */
    private int zombieScore = 0;

    /** playTime made: 14/09/26 */
    private int playTime = 0;

    /** xp_counter made: 14/09/26 */
    private int xp_counter = 0;

    /** clientVersion made: 14/09/26 */
    private int clientVersion = 0;

    /** postCount made: 14/09/26 */
    private int postCount = 0;

    /** house_layout made: 14/09/26 */
    private RoomConfig[][][] house_layout = new RoomConfig[4][13][13];

    /** costume_boxes made: 14/09/26 */
    private RoomBox[] costume_boxes = new RoomBox[10];

    /** unvoted made: 14/09/26 */
    private int unvoted = 0;

    /** degrading made: 14/09/26 */
    private ArrayList<Degrading> degrading = new ArrayList<Degrading>();

    /** rentedItems made: 14/09/26 */
    private ArrayList<RentedItem> rentedItems = new ArrayList<RentedItem>();

    /** action_reason made: 14/09/26 */
    private String[] action_reason = new String[20];

    /** disabled_actions made: 14/09/26 */
    private boolean[] disabled_actions = new boolean[20];

    /** xpCounter made: 14/09/26 */
    private int xpCounter = 0;

    /** otherSettings made: 14/09/26 */
    private Map<Long, PlayerVariable> otherSettings = new HashMap<Long, PlayerVariable>();

    /** clientConfig made: 14/09/26 */
    private short[] clientConfig = new short[505];

    /** miscFlags made: 14/09/26 */
    private byte[] miscFlags = new byte[100];

    /** kills made: 14/09/26 */
    private LinkedList<PVPKill> kills = new LinkedList<PVPKill>();

    /** arrays made: 14/09/26 */
    private Map<String, Array> arrays = new HashMap<String, Array>();

    /** mainSettings made: 14/09/26 */
    private int[] mainSettings = new int[2500];

    /** buttonSettings made: 14/09/26 */
    private int[]                  buttonSettings     = new int[2000];
    public int                     east_step_count    = 0;
    public HashMap<String, Object> attributes         = new HashMap<String, Object>();
    public boolean                 farmUpdateRequired = false;

    public int gmaulTick = -1;

    /** leveledUp[] made: 14/09/26 */
    private boolean leveledUp[] = new boolean[26];

    /** farmPatches made: 14/09/26 */
    private Map<Integer, PlayerGrew> farmPatches = new HashMap<Integer, PlayerGrew>();

    /** referralId made: 14/09/26 */
    private int referralId = 0;

    /** referralPendingCount made: 14/09/26 */
    private int referralPendingCount = 0;

    /** refferalEarnedCOunt made: 14/09/26 */
    private int refferalEarnedCOunt = 0;

    /** boss3ItemTimers made: 14/09/26 */
    private HashMap<Integer, Integer> boss3ItemTimers = new HashMap<Integer, Integer>();

    /** killsDeaths made: 14/10/02 **/
    private int[][] killsDeaths = new int[4][4];

    /** logged_in_since made: 14/09/26 */
    private int logged_in_since;

    /** game made: 14/09/26 */
    private HitmanGame game;

    /** player made: 14/09/26 */
    private Player player;

    private PlayerGrandExchange exchange;

    public PlayerGrandExchange getGrandExchange() {
        return exchange;
    }



    // total pvp damage dealt
    private long pvpDamageDealt = 0;

    public byte getMaxRecordedHit() {
        return maxRecordedHit;
    }

    public void setMaxRecordedHit(byte maxRecordedHit) {
        this.maxRecordedHit = maxRecordedHit;
    }

    public int getTotalPVPPointsReceived() {
        return totalPVPPointsReceived;
    }

    public void setTotalPVPPointsReceived(int totalPVPPointsReceived) {
        this.totalPVPPointsReceived = totalPVPPointsReceived;
    }

    public byte getKillStreak() {
        return killStreak;
    }

    public void setKillStreak(byte killStreak) {
        this.killStreak = killStreak;
    }

    public int getPvpPlayTime() {
        return pvpPlayTime;
    }

    public void setPvpPlayTime(int pvpPlayTime) {
        this.pvpPlayTime = pvpPlayTime;
    }

    public long getPvpDamageReceived() {
        return pvpDamageReceived;
    }

    public void setPvpDamageReceived(long pvpDamageReceived) {
        this.pvpDamageReceived = pvpDamageReceived;
    }

    public long getPvpDamageDealt() {
        return pvpDamageDealt;
    }

    public void setPvpDamageDealt(long pvpDamageDealt) {
        this.pvpDamageDealt = pvpDamageDealt;
    }

    private long pvpDamageReceived = 0;

    private int pvpPlayTime = 0;

    private int fightingTime = 0;

    private byte killStreak = 0;

    private int totalPVPPointsReceived = 0;

    private byte maxRecordedHit = 0;


    private static final long IRON_MAN_HASH = Text.longForName("iron_man");

    public static final int NO_IRONMAN = 0, IRON_MAN_MEDIUM = 1, IRON_MAN_HARD = 2;

    public int getIronManMode() {
        if(this.getVar(IRON_MAN_HASH) != null)
            return this.getVar(IRON_MAN_HASH).getIntData();
        return 0;
    }


    /**
     * Constructs ...
     *
     */
    public Account() {
        house_layout[1][6][6] = House.build_basic_garden();

        for (int i = 0; i < buttonSettings.length; i++) {
            buttonSettings[i] = -1;
            mainSettings[i]   = -1;
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param p
     */
    public Account(Player p) {
        player = p;
        this.exchange = new PlayerGrandExchange(p);
        for (int i = 0; i < buttonSettings.length; i++) {
            buttonSettings[i] = -1;
            mainSettings[i]   = -1;
        }
    }

    /**
     * Method getYellBanTime
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.netWorl
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getYellBanTime() {
        return yellBanTime;
    }

    /**
     * Method setYellBanTime
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param yellBanTime
     */
    public void setYellBanTime(long yellBanTime) {
        this.yellBanTime = yellBanTime;
    }


    public void banYell(int hours) {
          this.yellBanTime = Core.currentTimeMillis() + 1000 * 60 * 60 * hours;
    }

    public void banWild(int hours) {
        this.wildyBanTime = Core.currentTimeMillis() + 1000 * 60 * 60 * hours;
    }

    /**
     * Method getMutedTime
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getMutedTime() {
        return mutedTime;
    }

    /**
     * Method setMutedTime
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mutedTime
     */
    public void setMutedTime(long mutedTime) {
        this.mutedTime = mutedTime;
    }

    /**
     * Method getWildyBanTime
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getWildyBanTime() {
        return wildyBanTime;
    }

    /**
     * Method setWildyBanTime
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param wildyBanTime
     */
    public void setWildyBanTime(long wildyBanTime) {
        this.wildyBanTime = wildyBanTime;
    }

    /**
     * Method isMuted
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isMuted() {
        return Core.currentTimeMillis() - mutedTime < 0;
    }

    /**
     * Method isWildyBanned
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isWildyBanned() {
        return Core.currentTimeMillis() - wildyBanTime < 0;
    }

    /**
     * Method mute
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param length
     */
    public void mute(int length) {
        this.mutedTime = Core.currentTimeMillis() + 1000 * 60 * 60 * length;
    }

    /**
     * Method wildyBan
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param length
     */
    public void wildyBan(int length) {
        this.wildyBanTime = Core.currentTimeMillis() + 1000 * 60 * 60 * length;
    }

    /**
     * Method registerKillCount
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void registerKillCount(int id) {
        killsDeaths[id][0]++;
    }

    /**
     * Method registerDeathCount
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void registerDeathCount(int id) {
        killsDeaths[id][1]++;
    }

    /**
     * Method setKillsDeaths
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param kills
     * @param deaths
     */
    public void setKillsDeaths(int id, int kills, int deaths) {
        killsDeaths[id][0] = kills;
        killsDeaths[id][1] = deaths;
    }

    /**
     * Method getKills
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getKills(int id) {
        return killsDeaths[id][0];
    }

    /**
     * Method getDeaths
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getDeaths(int id) {
        return killsDeaths[id][1];
    }

    /**
     * Method getReferralId
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getReferralId() {
        return referralId;
    }

    /**
     * Method setReferralId
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param referralId
     */
    public void setReferralId(int referralId) {
        this.referralId = referralId;
    }

    /**
     * Method getRefferalEarnedCOunt
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRefferalEarnedCOunt() {
        return refferalEarnedCOunt;
    }

    /**
     * Method setRefferalEarnedCOunt
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param refferalEarnedCOunt
     */

    public long dboxTime = 0;
    public void setRefferalEarnedCOunt(int refferalEarnedCOunt) {
        this.refferalEarnedCOunt = refferalEarnedCOunt;
    }

    /**
     * Method getReferralPendingCount
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getReferralPendingCount() {
        return referralPendingCount;
    }

    /**
     * Method setReferralPendingCount
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param referralPendingCount
     */
    public void setReferralPendingCount(int referralPendingCount) {
        this.referralPendingCount = referralPendingCount;
    }

    /**
     * Method registerBossKill
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bossID
     */
    public void registerBossKill(int bossID) {
        boss3ItemTimers.put(bossID, Core.currentTime + BOSS_DELAY);
        player.getActionSender().sendMessage("You're unable to get a drop from " + NpcDef.FOR_ID(bossID).getName()
                + " for 5 minutes");
    }

    /**
     * Method bossBlocked
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npcID
     *
     * @return
     */
    public boolean bossBlocked(int npcID) {
        return boss3ItemTimers.containsKey(npcID);
    }

    /**
     * Method updateBossTimes
     * Created on 14/09/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateBossTimes() {
        if (boss3ItemTimers.size() > 0) {
            for (Iterator<Integer> bosses = boss3ItemTimers.keySet().iterator(); bosses.hasNext(); ) {
                int boss = bosses.next();

                if (Core.currentTime - boss3ItemTimers.get(boss) > 0) {
                    bosses.remove();
                }
            }
        }
    }

    /**
     * Method getFarmPatches
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Map<Integer, PlayerGrew> getFarmPatches() {
        return farmPatches;
    }

    /**
     * Method setFarmPatches
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param farmPatches
     */
    public void setFarmPatches(Map<Integer, PlayerGrew> farmPatches) {
        this.farmPatches = farmPatches;
    }

    /**
     * Method getHouseRoomCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHouseRoomCount() {
        int count = 0;

        for (int i = 0; i < house_layout.length; i++) {
            for (int x = 0; x < 13; x++) {
                for (int y = 0; y < 13; y++) {
                    if (house_layout[i][x][y] != null) {
                        count++;
                    }
                }
            }
        }

        return count;
    }

    /**
     * Method getLeveledUp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean[] getLeveledUp() {
        return leveledUp;
    }

    /**
     * Method setLevelUp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     * @param value
     */
    public void setLevelUp(int index, boolean value) {
        this.leveledUp[index] = value;
    }

    /**
     * Method setLeveledUp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param leveledUp
     */
    public void setLeveledUp(boolean[] leveledUp) {
        this.leveledUp = leveledUp;
    }

    /**
     * Method getGrewPatch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public PlayerGrew getGrewPatch(int id) {
        if (!farmPatches.containsKey(id)) {
            return null;
        }

        return farmPatches.get(id);
    }

    /**
     * Method setGrew
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param patchID
     * @param grew
     */
    public void setGrew(int patchID, PlayerGrew grew) {
        if (grew == null) {
            this.farmPatches.remove(patchID);

            return;
        }

        this.farmPatches.put(patchID, grew);
    }

    /**
     * Method setAttribute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     * @param obj
     */
    public void setAttribute(String key, Object obj) {
        attributes.put(key, obj);
    }

    /**
     * Method getAttribute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     *
     * @return
     */
    public Object getAttribute(String key) {
        if (attributes.containsKey(key)) {
            return attributes.get(key);
        }

        return null;
    }

    /**
     * Method buttonExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting_id
     *
     * @return
     */
    public boolean buttonExists(int setting_id) {
        return buttonSettings[setting_id] != -1;
    }

    /**
     * Method toggle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting_id
     */
    public void toggle(int setting_id) {
        if (mainSettings[setting_id] == -1) {
            throw new RuntimeException("Error, setting not accounted for.");
        }

        mainSettings[setting_id] = (mainSettings[setting_id] == 1)
                                   ? 0
                                   : 1;
        player.getActionSender().sendVar(setting_id, mainSettings[setting_id]);
    }

    /**
     * Method getMainSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getMainSettings() {
        return mainSettings;
    }

    /**
     * Method getButtonSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getButtonSettings() {
        return buttonSettings;
    }

    /**
     * Method toggleButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting_id
     */
    public void toggleButton(int setting_id) {
        if (buttonSettings[setting_id] == -1) {
            throw new RuntimeException("Error, setting not accounted for.");
        }

        buttonSettings[setting_id] = (buttonSettings[setting_id] == 1)
                                     ? 0
                                     : 1;
        player.getActionSender().sendBConfig(setting_id, buttonSettings[setting_id]);
    }

    /**
     * Method exists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean exists(int id) {
        return mainSettings[id] != -1;
    }

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting
     * @param value
     * @param send
     */
    public void set(int setting, int value, boolean send) {
        mainSettings[setting] = value;

        if (send) {
            player.getActionSender().sendVar(setting, value);
        }
    }

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting
     * @param value
     */
    public void set(int setting, int value) {
        set(setting, value, false);
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting
     *
     * @return
     */
    public int get(int setting) {
        return mainSettings[setting];
    }

    /**
     * Method setB
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting
     * @param value
     * @param send
     */
    public void setB(int setting, int value, boolean send) {
        this.buttonSettings[setting] = value;

        if (send) {
            player.getActionSender().sendBConfig(setting, value);
        }
    }

    /**
     * Method setB
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting
     * @param value
     */
    public void setB(int setting, int value) {
        setB(setting, value, false);
    }

    /**
     * Method getButtonConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getButtonConfig(int id) {
        return this.buttonSettings[id];
    }

    /**
     * Method resetHouse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetHouse() {
        house_layout          = new RoomConfig[4][13][13];
        house_layout[1][6][6] = House.build_basic_garden();
    }

    /**
     * Method saveCostumeRooms
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chunk
     */
    public void saveCostumeRooms(Buffer chunk) {
        for (int i = 0; i < costume_boxes.length; i++) {
            if (costume_boxes[i] == null) {
                chunk.addByte(0);
            } else {
                chunk.addByte(1);
                chunk.writeWord(costume_boxes[i].getItems().length);

                for (int k = 0; k < costume_boxes[i].getItems().length; k++) {
                    if (costume_boxes[i].getItems()[k] == -1) {
                        chunk.writeWord(0);

                        break;
                    }

                    chunk.writeWord(costume_boxes[i].getItems()[k]);
                    chunk.writeWord(costume_boxes[i].getAmts()[k]);
                }
            }
        }

        // chunk.addByte(255);
    }

    /**
     * Method loadCostumes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chunk
     */
    public void loadCostumes(Buffer chunk) {
        for (int i = 0; i < costume_boxes.length; i++) {
            int code = chunk.readByte();

            if (code == 255) {
                break;
            }

            if (code == 0) {
                continue;
            }

            int leng = chunk.readShort();

            costume_boxes[i] = new RoomBox(leng, i);

            for (int k = 0; k < leng; k++) {
                int off = chunk.readShort();

                if (off == 0) {
                    break;
                }

                costume_boxes[i].setItem(k, off, chunk.readShort());
            }
        }
    }

    /**
     * Method enable_all_actions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void enable_all_actions() {
        for (int i = 0; i < disabled_actions.length; i++) {
            disabled_actions[i] = false;
        }
    }

    /**
     * Method getHouseLayout
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RoomConfig[][][] getHouseLayout() {
        return house_layout;
    }

    /**
     * Method hasRoomBox
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean hasRoomBox(int id) {
        return (costume_boxes[id] != null) && (costume_boxes[id].getCount() > 0);
    }

    /**
     * Method setRoomBox
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param box
     */
    public void setRoomBox(int id, RoomBox box) {
        this.costume_boxes[id] = box;
    }

    /**
     * Method getRoomBox
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public RoomBox getRoomBox(int id) {
        return costume_boxes[id];
    }

    /**
     * Method getGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public HitmanGame getGame() {
        return game;
    }

    /**
     * Method setHitmanGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ga
     */
    public void setHitmanGame(HitmanGame ga) {
        this.game = ga;
    }

    /**
     * Method setPostCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     */
    public void setPostCount(int c) {
        this.postCount = c;
    }

    /**
     * Method getPostCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPostCount() {
        return postCount;
    }

    /**
     * Method setUnvoted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param vote
     */
    public void setUnvoted(int vote) {
        this.unvoted = vote;
    }

    /**
     * Method getUnvoted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getUnvoted() {
        return this.unvoted;
    }

    /**
     * Method getRentedItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<RentedItem> getRentedItems() {
        return rentedItems;
    }

    /**
     * Method getClientVersion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getClientVersion() {
        return clientVersion;
    }

    /**
     * Method setClientVersion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param version
     */
    public void setClientVersion(int version) {
        this.clientVersion = version;
    }

    /**
     * Method saveRentals
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param file
     */
    public void saveRentals(Buffer file) {
        file.addByte(UserAccount.OPCODE_RENTALS);
        file.addByte(rentedItems.size());

        for (RentedItem i : rentedItems) {
            file.writeWord(i.getItemID());
            file.writeDWord(i.remaining());
        }
    }

    /**
     * Method loadRentals
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param file
     */
    public void loadRentals(Buffer file) {
        int size = file.readByte();

        for (int i = 0; i < size; i++) {
            rentedItems.add(new RentedItem(file.readShort(), file.readDWord()));
        }
    }

    /**
     * Method hasRented
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public boolean hasRented(int itemId) {
        for (RentedItem i : rentedItems) {
            if (i.getItemID() == itemId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method dumpKills
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void dumpKills(Player player) {
        for (PVPKill kill : kills) {
            player.getActionSender().sendMessage("kill: " + kill.getKilled() + " " + kill.getKilledAt() + " "
                    + kill.remaining());
        }
    }

    /**
     * Method addRented
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amt
     */
    public void addRented(int itemid, int amt) {
        this.rentedItems.add(new RentedItem(itemid, amt));
    }

    /**
     * Method removeRented
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     */
    public void removeRented(int itemid) {
        for (int i = 0; i < rentedItems.size(); i++) {
            if (rentedItems.get(i).getItemID() == itemid) {
                rentedItems.remove(i);
            }
        }
    }

    /**
     * Method updateRented
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateRented() {
        for (int i = 0; i < rentedItems.size(); i++) {
            if (rentedItems.get(i).update()) {

                // locate the interfaces
                if (player.getEquipment().contains(rentedItems.get(i).getItemID())) {
                    for (int k = 0; k < 14; k++) {
                        if (player.getEquipment().getId(k) == rentedItems.get(i).getItemID()) {
                            player.getEquipment().removeItem(k);
                            player.getEquipment().updateEquipment(k);
                            player.getAppearance().setChanged(true);
                        }
                    }
                } else if (player.getInventory().hasItem(rentedItems.get(i).getItemID(), 1)) {
                    player.getInventory().deleteItem(rentedItems.get(i).getItemID(), 1);
                } else {
                    player.getBank().deleteItem(rentedItems.get(i).getItemID(), 1);
                }

                player.getActionSender().sendMessage("Your " + Item.forId(rentedItems.get(i).getItemID()).getName()
                        + " has been returned to the lending company.");
                rentedItems.remove(i);
            }
        }
    }

    /**
     * Method getDegrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public Degrading getDegrading(int item) {
        for (Degrading d : degrading) {
            if (d.getItemId() == item) {
                return d;
            }
        }

        return null;
    }

    /**
     * Method removeDegrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     */
    public void removeDegrading(Degrading d) {
        if (degrading.contains(d)) {
            degrading.remove(d);
        }
    }

    /**
     * Method removeDegrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     */
    public void removeDegrading(int itemid) {
        removeDegrading(itemid, false);
    }

    /**
     * Method removeDegrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param all
     */
    public void removeDegrading(int itemid, boolean all) {
        for (int i = 0; i < degrading.size(); i++) {
            if (degrading.get(i).getItemId() == itemid) {
                degrading.remove(i);

                if (!all) {
                    break;
                }
            }
        }
    }

    /**
     * Method saveDegradeables
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     */
    public void saveDegradeables(Buffer data) {
        data.writeByte(UserAccount.OPCODE_DEGRADEABLES);

        for (Degrading d : degrading) {
            data.writeWord(d.getItemId());
            data.writeDWord(d.getRemaining());
        }

        data.writeWord(65535);
    }

    /**
     * Method loadDegradeables
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     */
    public void loadDegradeables(Buffer data) {
        int i = 0;

        while ((i = data.readShort()) != 65535) {
            degrading.add(new Degrading(i, data.readDWord()));
        }
    }

    /**
     * Method getDegrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Degrading> getDegrading() {
        return degrading;
    }

    /**
     * Method hasDegrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public boolean hasDegrading(int itemId) {
        for (Degrading d : degrading) {
            if (d.getItemId() == itemId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method addDegrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param degrading
     */
    public void addDegrading(Degrading degrading) {
        this.degrading.add(degrading);
    }

    /**
     * Method set_action_disabled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param action
     * @param disabled
     * @param reason
     */
    public void set_action_disabled(int action, boolean disabled, String reason) {
        disabled_actions[action] = disabled;
        action_reason[action]    = reason;
    }

    /**
     * Method isActionDisabled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean isActionDisabled(int id) {
        return disabled_actions[id];
    }

    /**
     * Method getDisabledMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public String getDisabledMessage(int id) {
        return action_reason[id];
    }

    /**
     * Method getXpCounter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getXpCounter() {
        return xpCounter;
    }

    /**
     * Method setXpCounter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param xpCounter
     */
    public void setXpCounter(int xpCounter) {
        this.xpCounter = xpCounter;
    }

    /**
     * Method addXPToCounter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param xp
     */
    public void addXPToCounter(int xp) {
        this.xpCounter += xp;
    }

    /**
     * Method hasArray
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public boolean hasArray(String name) {
        return arrays.containsKey(name);
    }

    /**
     * Method getArray
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public Array getArray(String name) {
        return arrays.get(name);
    }

    /**
     * Method setPlayTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     */
    public void setPlayTime(int time) {
        playTime = time;
    }

    /**
     * Method addArray
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param siz
     * @param save
     */
    public void addArray(String name, int siz, boolean save) {
        arrays.put(name, new Array(siz, save));
    }

    /**
     * Method updatePlaytime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updatePlaytime() {
        playTime++;
        logged_in_since++;
    }

    public void updatePVPTime() {
        pvpPlayTime ++;
    }

    public void updatePVPFightingTime() {
        fightingTime ++;
    }

    /**
     * Method getLoggedInSince
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLoggedInSince() {
        return logged_in_since;
    }

    /**
     * Method getPlayTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPlayTime() {
        return playTime;
    }

    /**
     * Method getKillCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     *
     * @return
     */
    public int getKillCount(long uid) {
        int count = 0;

        for (PVPKill kill : kills) {
            if (kill.getKilled() == uid) {
                count++;
            }
        }

        return count;
    }


    /**
     * Method updateKills
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateKills() {
        if ((Core.currentTime % 20) != 0) {
            return;
        }

        ArrayList<PVPKill> rem = null;

        for (PVPKill k : kills) {
            if (Core.currentTime > k.getKilledAt()) {
                if (rem == null) {
                    rem = new ArrayList<PVPKill>();
                }

                rem.add(k);
            }
        }

        if (rem != null) {
            kills.removeAll(rem);
        }
    }

    /**
     * Method registerKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void registerKilled(Player player) {
        kills.add(new PVPKill(player));
    }

    /**
     * Method getZombieKills
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getZombieKills() {
        return zombieKills;
    }

    /**
     * Method setZombieKills
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param zombieKills
     */
    public void setZombieKills(int zombieKills) {
        this.zombieKills = zombieKills;
    }

    /**
     * Method addZombieScore
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param score
     */
    public void addZombieScore(int score) {
        zombieScore += score;
    }

    /**
     * Method getZombieScore
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getZombieScore() {
        return zombieScore;
    }

    /**
     * Method setZombieScore
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param score
     */
    public void setZombieScore(int score) {
        this.zombieScore = score;
    }

    /**
     * Method setConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param value
     */
    public void setConfig(int id, int value) {
        clientConfig[id] = (short) (value + 1);

        if (id == 43) {
            player.setAutocast(-1);
        }
    }

    /**
     * Method setClientConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param value
     */
    public void setClientConfig(int id, int value) {
        clientConfig[id] = (short) (value + 1);

        if (id == 43) {
            player.setAutocast(-1);
        }
    }

    /**
     * Method isSet
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean isSet(int id) {
        return clientConfig[id] == 2;
    }

    /**
     * Method getClientConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     */

    /**
     * Method setDefaults
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void setDefaults() {
        set(43, 0);
        set(AUTO_RETALIATE, 1);
        set(ACCEPT_AID, 0);
        set(TUTORIAL_STAGE, 0);
        set(PRAYER_TYPE, 0);
        setB(119, 0);
        setSetting("prayer_type", 0, true);
        setSetting("spellbook_type", 0, true);
        setSetting("kills", 0, true);
        setSetting("deaths", 0, true);
        setSetting("pk_streak", 0, true);
    }

    /**
     * Method switchConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void switchConfig(int id) {
        short conf = clientConfig[id];

        if (conf == 0) {
            conf = 1;
        }

        if (conf == 2) {
            conf = 1;
        } else if (conf == 1) {
            conf = 2;
        }

        clientConfig[id] = conf;
    }

    /**
     * Method setSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param stra
     */
    public void setSetting(String str, String stra) {
        otherSettings.put(Text.stringToLong(str), new PlayerVariable(str, stra));
    }

    /**
     * Method setSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param stra
     * @param save
     */
    public void setSetting(String str, String stra, boolean save) {
        otherSettings.put(Text.stringToLong(str), new PlayerVariable(str, stra, save));
    }

    /**
     * Method increaseVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param by
     */
    public void increaseVar(String str, int by) {
        PlayerVariable e = getVar(str);

        e.setIntData(e.getIntData() + by);
    }

    public int getPvpFightTime() {
        return fightingTime;
    }

    public void setFightTime(int time) {
        this.fightingTime = time;
    }

    /**
     * Method hasVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param var
     *
     * @return
     */
    public boolean hasVar(String var) {
        return otherSettings.containsKey(Text.stringToLong(var));
    }

    /**
     * Method decreaseVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param by
     */
    public void decreaseVar(String str, int by) {
        increaseVar(str, -by);
    }

    /**
     * Method removeVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void removeVar(String str) {
        if (hasVar(str)) {
            otherSettings.remove(Text.stringToLong(str));
        }
    }

    /**
     * Method setSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param i
     */
    public void setSetting(String str, int i) {
        otherSettings.put(Text.stringToLong(str), new PlayerVariable(str, i, false));
    }

    /**
     * Method setSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param i
     * @param save
     */
    public void setSetting(String str, int i, boolean save) {
        otherSettings.put(Text.stringToLong(str), new PlayerVariable(str, i, save));
    }

    /**
     * Method setObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     * @param data
     * @param save
     */
    public void setObject(String key, Object data, boolean save) {
        otherSettings.put(Text.stringToLong(key), new PlayerVariable(key, data, save));
    }

    /**
     * Method getInt32
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public int getInt32(String str) {
        PlayerVariable i = getVar(str);

        if (i == null) {
            return -1;
        }

        return i.getIntData();
    }

    /**
     * Method getString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public String getString(String str) {
        PlayerVariable i = getVar(str);

        if (i == null) {
            return "null";
        }

        return i.getStringData();
    }

    /**
     * Method getVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param identifier
     *
     * @return
     */
    public PlayerVariable getVar(String identifier) {
        return otherSettings.get(Text.stringToLong(identifier));
    }

    /**
     * Method getVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param identifier
     *
     * @return
     */
    public PlayerVariable getVar(long identifier) {
        return otherSettings.get(identifier);
    }

    /**
     * Method forceSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param setting
     * @param id
     */
    public static void forceSetting(Player pla, int setting, int id) {
        pla.getAccount().clientConfig[setting] = (short) (id + 1);
    }

    /**
     * Method setFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param b
     */
    public void setFlag(int id, boolean b) {
        miscFlags[id] = b
                        ? (byte) 1
                        : 0;
    }

    /**
     * Method getFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean getFlag(int id) {
        return miscFlags[id] == 1;
    }

    /**
     * Method getSmallSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public int getSmallSetting(int i) {
        return (int) miscFlags[i];
    }

    /**
     * Method putSmall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param g
     */
    public void putSmall(int i, int g) {
        miscFlags[i] = (byte) g;
    }

    /**
     * Method putVariable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param string
     * @param var
     */
    public void putVariable(String string, PlayerVariable var) {
        this.otherSettings.put(Text.stringToLong(string), var);
    }

    /**
     * Method getSaveVars
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<PlayerVariable> getSaveVars() {
        ArrayList<PlayerVariable> vars = new ArrayList<PlayerVariable>();

        for (PlayerVariable e : otherSettings.values()) {
            if (e != null) {
                if (e.isSave()) {
                    vars.add(e);
                }
            }
        }

        return vars;
    }


}
