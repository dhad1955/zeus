package net.tazogaming.hydra.entity3d.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 15:30
 * To change this template use File | Settings | File Templates.
 */
public abstract class Action {
    public static final int
        INTERACTING_WITH_OBJECT      = 1,
        INTERACTING_WITH_NPC         = 2,
        INTERACTING_WITH_PLAYER      = 3,
        INTERACTING_WITH_OBJECT2     = 1,
        DOING_AGILITY_OBSTICAL       = 5,
        DOING_AGILITY_JUMP           = 7,
        CRAFTING_ITEM_FROM_INVENTORY = 8,
        WALKING_TO_DESTINATION       = 9;
    private int     currentTime      = -1;
    private int     actionAmount     = 0;
    private int     actionSlot       = 0;
    private boolean isTerminated     = false;
    private int     type;
    private Player  owner;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param pla
     */
    public Action(int type, Player pla) {
        this.type  = type;
        this.owner = pla;
    }

    /**
     * Method getActionAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getActionAmount() {
        return actionAmount;
    }

    /**
     * Method getActionSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getActionSlot() {
        return actionSlot;
    }

    /**
     * Method setActionSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param actionSlot
     */
    public void setActionSlot(int actionSlot) {
        this.actionSlot = actionSlot;
    }

    protected abstract void tick(int time);

    /**
     * Method setActionAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setActionAmount(int id) {
        actionAmount = id;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return owner;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {
        currentTime++;
        tick(currentTime);
    }

    protected int getCurrentTime() {
        return currentTime;
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void terminate() {
        isTerminated = true;
        onTermination();
        this.getPlayer().setCurrentAction(null);
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void onTermination();

    /**
     * Method isTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminated() {
        return isTerminated;
    }
}
