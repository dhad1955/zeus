package net.tazogaming.hydra.entity3d.player.masks;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.net.packetbuilder.mask.UpdateMask;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 23/09/13
 * Time: 19:38
 * To change this template use File | Settings | File Templates.
 */
public class Interacting extends UpdateMask {
    private int interactingWith = -1;

    /**
     * Method focus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void focus(Player pla) {
        interactingWith = 32768 + pla.getIndex();
        setChanged(true);
    }

    /**
     * Method focus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void focus(NPC npc) {
        interactingWith = npc.getIndex();
        setChanged(true);
    }

    /**
     * Method isFocused
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     *
     * @return
     */
    public boolean isFocused(Killable k) {
        if (k instanceof NPC) {
            return isFocused((NPC) k);
        } else {
            return isFocused((Player) k);
        }
    }

    /**
     * Method focus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     */
    public void focus(Killable k) {
        if (k instanceof Player) {
            focus((Player) k);
        } else {
            focus((NPC) k);
        }
    }

    /**
     * Method isFocused
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isFocused() {
        return interactingWith != -1;
    }

    /**
     * Method isFocused
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public boolean isFocused(NPC npc) {
        return interactingWith == npc.getIndex();
    }

    /**
     * Method isFocused
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean isFocused(Player pla) {
        return pla.getIndex() + 32768 == interactingWith;
    }

    /**
     * Method unFocus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unFocus() {
        interactingWith = -1;
        setChanged(true);
    }

    /**
     * Method getInteractingWith
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getInteractingWith() {
        return interactingWith;
    }
}
