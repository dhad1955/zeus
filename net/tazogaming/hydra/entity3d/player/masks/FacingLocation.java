package net.tazogaming.hydra.entity3d.player.masks;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.packetbuilder.mask.UpdateMask;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 04:01
 */
public class FacingLocation extends UpdateMask {
    private int focusX = -1,
                focusY = -1;
    private int currentId;

    /**
     * Method focus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     */
    public void focus(int x, int y) {
        focusX = x;
        focusY = y;
        currentId++;
    }

    /**
     * Method focus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     */
    public void focus(Point location) {
        focus(location.getX(), location.getY());
    }

    /**
     * Method isSet
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSet() {
        return (focusX != -1) && (focusY != -1);
    }

    /**
     * Method getCurrentId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentId() {
        return currentId;
    }

    /**
     * Method getX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getX() {
        return focusX;
    }

    /**
     * Method getY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getY() {
        return focusY;
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        focusX = -1;
        focusY = -1;
    }
}
