package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcKnownData;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.npc.ai.script.PlayerScriptFunctions;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.entity3d.object.DwarfMultiCannon;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.*;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.entity3d.player.actions.AgilityWalkAction;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.EventManager;
import net.tazogaming.hydra.game.Killcounts;
import net.tazogaming.hydra.game.PKHighscores;
import net.tazogaming.hydra.game.gametick.*;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.gametick.ZoneUpdateTick;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.deathtower.DeathTowerGame;
import net.tazogaming.hydra.game.minigame.hitman.HitmanGame;
import net.tazogaming.hydra.game.minigame.zombiesx.Zombies;
import net.tazogaming.hydra.game.questing.Quest;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.CombatAdapter;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.skill.combat.impact.SoulsplitImpactEvent;
import net.tazogaming.hydra.game.skill.combat.potion.Potion;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.dungeoneering.Party;
import net.tazogaming.hydra.game.skill.farming.patch.Patch;
import net.tazogaming.hydra.game.skill.slayer.SlayerTask;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.ui.*;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.game.ui.rsi.interfaces.SkillDialogue;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning.SummoningTab;
import net.tazogaming.hydra.game.ui.rsi.interfaces.tab.AchievementsTab;
import net.tazogaming.hydra.game.ui.shops.Shop;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.net.ActionSender;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.PacketThrottleFilter;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.net.packetbuilder.mask.ForceMovement;
import net.tazogaming.hydra.net.packethandler.Walking;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.script.runtime.ScriptSuspension;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;
import net.tazogaming.hydra.script.runtime.instr.op.impl.game.BrawlingGloves;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.test.performancetest.BotPlayer;
import net.tazogaming.hydra.util.*;
import net.tazogaming.hydra.util.collections.StatefulEntityCollection;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * A single player.
 */
public class Player extends Killable {
	public static final String RANK_NAMES[] = { "Rookie", "Novi", "Sir", "Hero", "Legend", "Lord", "Elite" };
	public static final int RANK_ROOKIE = 0, RANK_NOV = 1, RANK_SIR = 2, RANK_HERO = 3, RANK_LEGEND = 4, RANK_LORD = 5,
			RANK_ELITE = 6;
	public static DeathHandler defaultHandler = new DefaultDeathHandler();
	public static final int NORMAL_USER = 0, DONATOR = 1, PLATINUM = 3, PATRON = 4, EXTREME_DONATOR = 5, PROMOTER = 6,
			GOLDEN_YOUTUBER = 7, OFFICIAL_HELPER = 8, MODERATOR = 9, COMMUNITY_MANAGER = 10, HEAD_MODERATOR = 11,
			ADMINISTRATOR = 12, DEVELOPER = 13, CONTENT_MANAGER = 14, ROOT_ADMIN = 15;
	public boolean is_con = false;
	public boolean noClip = false;
	public boolean adminAuth = false;

	/** killCounts made: 14/11/19 */
	private Map<Integer, Integer> killCounts = new HashMap<Integer, Integer>();

	/** username, password, lastIP, currentIP made: 14/08/18 */
	private String username, password, lastIP = "0.0.0.0", currentIP = "0.0.0.0";

	/** actionLockSecs made: 14/12/20 */
	private int actionLockSecs = 0;

	/** curStats, exps, prestige, privacySettings made: 14/08/18 */
	private int[] curStats = new int[25], exps = new int[25], prestige = new int[25], privacySettings = new int[5];

	/** equipment made: 14/08/18 */
	private Equipment equipment = new Equipment(this);

	/** isHD, mapAreaChanged, running, loggedIn made: 14/08/18 */
	private transient boolean isHD = false, mapAreaChanged = true, running = false, loggedIn = false;

	/**
	 * scriptedTaskScheduler made: 14/08/18 A task scheduler for scripts, to run
	 * in a certain time
	 */
	private PlayerScriptedTaskScheduler scriptedTaskScheduler = new PlayerScriptedTaskScheduler();

	/** actionSender made: 14/08/18 */
	private transient ActionSender actionSender = new ActionSender(this);

	/** lastChatMessage made: 14/08/18 */
	private transient ChatMessage lastChatMessage = null;

	/** achievementData made: 14/08/18 */
	private int[] achievementData = new int[350];

	/** watchedPlayers made: 14/08/18 */
	private transient StatefulEntityCollection<Player> watchedPlayers = new StatefulEntityCollection<Player>();

	/** watchedNpcs made: 14/08/18 */
	private transient StatefulEntityCollection<NPC> watchedNpcs = new StatefulEntityCollection<NPC>();

	/** watchedObjects made: 14/08/18 */
	private transient StatefulEntityCollection<GameObject> watchedObjects = new StatefulEntityCollection<GameObject>();

	/** knownNpcData made: 14/08/18 */
	private Map<Integer, NpcKnownData> knownNpcData = new HashMap<Integer, NpcKnownData>();

	/** knownAppearanceIds made: 14/08/18 */
	private Map<Player, Integer> knownAppearanceIds = new HashMap<Player, Integer>();

	/** tappers made: 14/08/18 */
	private ArrayList<Player> tappers = new ArrayList<Player>();

	/** knownViewPointIds made: 14/08/18 */
	private Map<Player, Integer> knownViewPointIds = new HashMap<Player, Integer>();

	/** knownObjects made: 14/08/18 */
	private Map<GameObject, Integer> knownObjects = new HashMap<GameObject, Integer>();

	/** knownFacingIds made: 14/08/18 */
	private Map<Player, Integer> knownFacingIds = new HashMap<Player, Integer>();

	/** knownDoorIds made: 14/08/18 */
	private Map<Door, Integer> knownDoorIds = new HashMap<Door, Integer>();

	/** playerFriends made: 14/08/18 */
	private Vector<Long> playerFriends = new Vector<Long>();
	public boolean ninja_on = false;

	/** playerIgnores made: 14/08/18 */
	private LinkedList<Long> playerIgnores = new LinkedList<Long>();

	/** backLog made: 14/08/18 */
	private ArrayList<Scope> backLog = new ArrayList<Scope>();

	/** disconTime made: 14/08/18 */
	private long disconTime = 0;

	/** headIcon made: 14/08/18 */
	private int headIcon = 0;

	/** headIcon2 made: 14/08/18 */
	private int headIcon2 = 0;

	/** windowManager made: 14/08/18 */
	private WindowManager windowManager = new WindowManager(this);

	/** actionDisable made: 14/08/18 */
	private boolean actionDisable = false;

	/** inventory made: 14/08/18 */
	private Inventory inventory = new Inventory(this);

	/** entitiesInView made: 14/08/18 */
	private ArrayList[] entitiesInView = new ArrayList[5];

	/** knownStackCounts made: 14/08/18 */
	private HashMap<FloorItem, Integer> knownStackCounts = new HashMap<FloorItem, Integer>();

	/** actionRequest made: 14/08/18 */
	private ActionRequest actionRequest = new ActionRequest(this);

	/** isTestPilot made: 14/08/18 */
	private boolean isTestPilot = false;

	/** combatHandler made: 14/08/18 */
	private CombatAdapter combatAdapter = new PlayerCombatAdapter(this);

	/** currentHealth made: 14/08/18 */
	private double currentHealth = 1;

	/** settings made: 14/08/18 */
	private Account account = new Account(this);

	/** currentSpecial made: 14/08/18 */
	private double currentSpecial = 10;

	/** specialHighlighted made: 14/08/18 */
	private boolean specialHighlighted = false;

	/** tickEvents made: 14/08/18 */
	private LinkedBlockingDeque<PlayerTickEvent> tickEvents = new LinkedBlockingDeque<PlayerTickEvent>();

	/** currentEnergy made: 14/08/18 */
	private double currentEnergy = 100;

	/** activePrayers made: 14/08/18 */
	private boolean[] activePrayers = new boolean[28];

	/** prayerPoints made: 14/08/18 */
	private double prayerPoints = 1;

	/** prayerBook made: 14/08/18 */
	private PrayerBook prayerBook = new PrayerBook(this);

	/** rights made: 14/08/18 */
	private int rights = 0;

	/** scriptsToProcess made: 14/08/18 */
	private ArrayList<Scope> scriptsToProcess = new ArrayList<Scope>();

	/** itemsInView made: 14/08/18 */
	private StatefulEntityCollection<FloorItem> itemsInView = new StatefulEntityCollection<FloorItem>();

	/** requestedSpell made: 14/08/18 */
	private int requestedSpell = -1;

	/** boostedHealth made: 14/08/18 */
	private int boostedHealth = 0;

	/** knownAreas made: 14/08/18 */
	private ArrayList<Zone> knownZones = new ArrayList<Zone>();

	/** wasAttackedBy made: 14/08/18 */
	private ArrayList<Player> wasAttackedBy = new ArrayList<Player>();

	/** didAttack made: 14/08/18 */
	private ArrayList<Player> didAttack = new ArrayList<Player>();

	/** packets made: 14/08/18 */
	private final ArrayList<Packet> packets = new ArrayList<Packet>();

	/** packetThrottleAmounts made: 14/08/18 */
	private int packetThrottleAmounts = 0;

	/** doorsInView made: 14/08/18 */
	private LinkedList<Door> doorsInView = new LinkedList<Door>();

	/** tradeRequest made: 14/08/18 */
	private int tradeRequest = -1;

	/** summonSpecial made: 14/08/18 */
	private int summonSpecial = -1;

	/** block_anchor made: 14/08/18 */
	private String block_anchor = null;

	/** quest_stages made: 14/08/18 */
	private boolean[][] quest_stages = new boolean[50][150];

	/** uid made: 14/08/18 */
	private long uid = 0;

	/** tele_block made: 14/08/18 */
	private String tele_block = null;

	/** text_update made: 14/08/18 */
	private String text_update = null;

	/** linked_npcs made: 14/08/18 */
	private LinkedList<NPC> linked_npcs = new LinkedList<NPC>();

	/** bobItems made: 14/08/18 */
	private int[] bobItems = new int[30];

	/** bobItemsC made: 14/08/18 */
	private int[] bobItemsC = new int[30];

	/** currSummonPts made: 14/08/18 */
	private int currSummonPts = 0;

	/** duelRequest made: 14/08/18 */
	private int duelRequest = -1;

	/** bank made: 14/08/18 */
	private Bank bank = new Bank(this);

	/** throttleFilter made: 14/08/18 */
	private PacketThrottleFilter throttleFilter = new PacketThrottleFilter();

	/** specialMultiplier made: 14/08/18 */
	private double specialMultiplier = 1.0;

	/** slay_kill_id made: 14/08/18 */

	/** slay_kill_amount made: 14/08/18 */

	/** slay_kills made: 14/08/18 */

	/** summ_spec_pts made: 14/08/18 */
	private int summ_spec_pts = 60;

	/** movementLock made: 14/08/18 */
	private boolean movementLock = false;

	/** known_patches made: 14/08/18 */
	private HashMap<Patch, Integer> known_patches = new HashMap<Patch, Integer>();

	/** curPoints made: 14/08/18 */
	private int[] curPoints = new int[20];

	/** maxStats made: 14/08/18 */
	private int[] maxStats = new int[25];

	/** challengeRequest made: 14/08/18 */
	private int challengeRequest = -1;

	/** followingList made: 14/08/18 */
	private ArrayList<Player> followingList = new ArrayList<Player>();

	/** lastAttackedTime made: 14/08/18 */
	private int lastAttackedTime = 0;

	/** logs made: 14/08/18 */
	private ArrayList<String> logs = new ArrayList<String>(Config.LOG_SPILL_RATE);

	/** autoCast made: 14/08/18 */
	private int autoCast = -1;

	/** functions made: 14/08/18 */
	private PlayerScriptFunctions functions = new PlayerScriptFunctions(this);

	/** xp_counter made: 14/08/18 */
	private int xp_counter = 0;

	/** friendsUpdater made: 14/08/18 */
	private FriendsList friendsUpdater = new FriendsList(this);

	/** renderAnim made: 14/08/28 */
	private int renderAnim = -1;

	/** sendingClanMsg made: 14/08/28 */
	private boolean sendingClanMsg = false;

	/** preventUnlink made: 14/08/28 */
	private boolean preventUnlink = false;

	/** scriptDelays made: 14/08/31 */
	private List<ScriptSuspension> scriptDelays = new ArrayList<ScriptSuspension>();

	/** nextPackets made: 14/11/05 */
	private Queue<Packet> nextPackets = new LinkedBlockingQueue<Packet>();

	/** buyBackShop made: 14/11/19 */
	private Shop buyBackShop = new Shop();
	public boolean packetTracked = false;

	/**
	 * Method registerPacket Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 */
	private long lastPacket = Core.currentTimeMillis();

	/**
	 * Method addXp Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param skillId
	 * @param experience
	 */
	public String hitDebug = "";

	/**
	 * Method onHitBy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param hitBy
	 * @param d
	 */
	public Point lastClientLocation = null;

	/** bHTarget made: 14/12/20 */
	private Player bHTarget;

	/** gameFrame made: 14/08/18 */
	private GameFrame gameFrame;

	/** currentFamiliar made: 14/08/18 */
	private NPC currentFamiliar;

	/** movementMask made: 14/08/18 */
	private ForceMovement movementMask;

	/** responseCode made: 14/08/18 */
	private transient byte responseCode;

	/** knownRegionX, knownRegionY made: 14/08/18 */
	private transient int knownRegionX, knownRegionY;

	/** usernameHash made: 14/08/18 */
	private transient long usernameHash;

	/** ioSession made: 14/08/18 */
	private transient PlaySession ioSession;

	/** appearance made: 14/08/18 */
	private PlayerAppearance appearance;

	/** hashingFrom made: 14/08/18 */
	private Player hashingFrom;

	/** action made: 14/08/18 */
	private Action action;

	/** curDeathHandler made: 14/08/18 */
	private DeathHandler curDeathHandler;

	/** following made: 14/08/18 */
	private PlayerFollowing following;

	/** currentAction made: 14/08/18 */
	private Action currentAction;

	/** zombiesGame made: 14/08/18 */
	private Zombies zombiesGame;

	/** my_dung_party made: 14/08/18 */
	private Party my_dung_party;

	/** getCurrentWar made: 14/08/18 */
	private ClanWar getCurrentWar;

	/** last_attacked made: 14/08/18 */
	private Player last_attacked;

	/** currentChannel made: 14/08/18 */
	private ClanChannel currentChannel;

	/** current_house made: 14/08/18 */
	private House current_house;

	/**
	 * Method setSlayerTask Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npcId
	 * @param amount
	 */
	private SlayerTask task;

	/**
	 * Method setSlayTotal Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param amt
	 */

	/**
	 * Method drainStat Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param stat
	 * @param amount
	 *
	 * @return
	 */
	private Player slayerJoinRequest;

	/** slayerJoinRequested made: 14/12/20 */
	private Player slayerJoinRequested;

	/** currentTask made: 14/08/18 */

	/**
	 * Constructs ...
	 *
	 */
	public Player() {
		isTestPilot = true;

		for (int i = 0; i < entitiesInView.length; i++) {
			entitiesInView[i] = new ArrayList<Entity>();
		}

		// this.settings.loadPane();
	}

	/**
	 * Constructs ...
	 * 
	 * 
	 * @param ios
	 */
	public Player(PlaySession ios) {
		ioSession = ios;

		if (ios.getPlayer() != null) {
			throw new RuntimeException();
		}

		for (int i = 0; i < entitiesInView.length; i++) {
			entitiesInView[i] = new ArrayList<Entity>();
		}

		ios.setPlayer(this);
		Arrays.fill(bobItems, -1);
		this.account.setDefaults();
		this.buyBackShop.setItems(new Item[10]);
		this.buyBackShop.setItemAmount(new int[10]);
	}

	/**
	 * Method setUsername Created on 15/05/12
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * Method getKillCounts Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Map<Integer, Integer> getKillCounts() {
		return killCounts;
	}

	/**
	 * Method setLock Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param secs
	 */
	public void setLock(int secs) {
		this.actionLockSecs = Core.currentTime + secs;
	}

	/**
	 * Method isActionLocked Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isActionLocked() {
		return Core.timeUntil(actionLockSecs) > 0;
	}

	/**
	 * Method getBHTarget Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Player getBHTarget() {
		return bHTarget;
	}

	/**
	 * Method setTarget Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param bhTarget
	 */
	public void setTarget(Player bhTarget) {
		this.bHTarget = bhTarget;
	}

	/**
	 * Method registerKillCount Created on 14/11/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npcKilled
	 */
	public void registerKillCount(int npcKilled) {
		Killcounts.KillCountType k = Killcounts.get(npcKilled);

		if (k != null) {
			if (killCounts.containsKey(npcKilled)) {
				killCounts.put(npcKilled, killCounts.get(npcKilled) + 1);
			} else {
				killCounts.put(npcKilled, 1);
			}
		} else {
		}
	}

	/**
	 * Method hasPacket Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 *
	 */
	public void resetQuests() {
		this.quest_stages = new boolean[50][150];
	}

	/**
	 * Method getBuyBackShop Created on 14/11/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Shop getBuyBackShop() {
		return buyBackShop;
	}

	/**
	 * Method getNextPackets Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Queue<Packet> getNextPackets() {
		return nextPackets;
	}

	@Override
	protected void onMoved(Point movedFrom, Point movedTo, int direction) {
	}

	/**
	 * Method moveFollowList Created on 15/05/12
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void moveFollowList() {
		if (isFollowing()) {
			getFollowing().updateMovement();

			if (getAccount().getButtonConfig(Account.RUN_STATUS) == 1) {
				getFollowing().updateMovement();
			}
		}
	}

	/**
	 * Method delayScript Created on 14/08/31
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param triggerType
	 * @param hash
	 * @param secs
	 */
	public void delayScript(int triggerType, int hash, int secs) {
		scriptDelays.add(new ScriptSuspension(triggerType, hash, secs));
	}

	/**
	 * Method updateDelayedScripts Created on 14/08/31
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateDelayedScripts() {
		ScriptSuspension ban;

		for (Iterator<ScriptSuspension> bans = scriptDelays.iterator(); bans.hasNext();) {
			ban = bans.next();

			if (Core.timeUntil(ban.getDelay()) <= 0) {
				bans.remove();
			}
		}
	}

	/**
	 * Method getScriptedTaskScheduler Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public PlayerScriptedTaskScheduler getScriptedTaskScheduler() {
		return scriptedTaskScheduler;
	}

	/**
	 * Method setGameFrame Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param gameFrame
	 */
	public void setGameFrame(GameFrame gameFrame) {
		this.gameFrame = gameFrame;
	}

	/**
	 * Method getGameFrame Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public GameFrame getGameFrame() {
		return this.gameFrame;
	}

	/**
	 * Method getGetCurrentWar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ClanWar getGetCurrentWar() {
		return getCurrentWar;
	}

	/**
	 * Method setGetCurrentWar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param getCurrentWar
	 */
	public void setGetCurrentWar(ClanWar getCurrentWar) {
		this.getCurrentWar = getCurrentWar;
	}

	/**
	 * Method getChallengeRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getChallengeRequest() {
		return challengeRequest;
	}

	/**
	 * Method setChallengeRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param challengeRequest
	 */
	public void setChallengeRequest(int challengeRequest) {
		this.challengeRequest = challengeRequest;
	}

	/**
	 * Method achievementComplete Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public boolean achievementComplete(int id) {
		return (Achievement.ACHIEVEMENTS[id] != null) && (Achievement.getProgress(id) <= achievementData[id]);
	}

	/**
	 * Method getAchievementsCompletedTotal Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getAchievementsCompletedTotal() {
		int total = 0;

		for (int i = 0; i < 200; i++) {
			if (i < Achievement.ACHIEVEMENTS.length) {
				if (achievementComplete(i)) {
					total++;
				}
			}
		}

		return total;
	}

	/**
	 * Method getAchievementsStartedTotal Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getAchievementsStartedTotal() {
		int total = 0;

		for (int i = 0; i < 200; i++) {
			if (achievementData[i] > 0) {
				total++;
			}
		}

		return total;
	}

	/**
	 * Method getProgress Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public int getProgress(int id) {
		return achievementData[id];
	}

	/**
	 * Method hasStartedAchievement Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public boolean hasStartedAchievement(int id) {
		return getProgress(id) > 0;
	}

	/**
	 * Method forceComplete Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void forceComplete(int id) {
		actionSender.interfaceColor(Achievement.ACHIEVEMENTS[id].getInterfaceID(), 0X3366);
		actionSender.sendAlert("Achievement Complete! " + Achievement.getName(id),
				"You have been rewarded: " + Achievement.getCash(id) + " GP", 4, 400);
		Achievement.giveCash(Achievement.getCash(id), this);
		World.getWorld().getScriptManager().directTrigger(this, null, Trigger.ACHIEVEMENT_COMPLETE, id);
	}

	/**
	 * Method maxPK Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void maxPK() {
		for (int i = 0; i < 7; i++) {
			this.exps[i] = GameMath.getXPForLevel(99);
			this.curStats[i] = 99;
			this.maxStats[i] = 99;
			this.getPrayerBook().setPoints(99);
			this.setCurrentHealth(99);
			getActionSender().sendStat(i);
			getAppearance().setChanged(true);
		}
	}

	/**
	 * Method addAchievementProgress Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param progression
	 */
	public void addAchievementProgress2(int id, int progression) {
		try {
			if (achievementComplete(id)) {
				return;
			}

			boolean update = !hasStartedAchievement(id)
					&& (achievementData[id] + progression) < Achievement.getProgress(id);

			if (update) {
				actionSender.interfaceColor(Achievement.ACHIEVEMENTS[id].getInterfaceID(), 0x33FF66);
				actionSender.sendMessage("<img=" + UserAccount.CROWN_SUPPORT + ">Achievement started: "
						+ Text.BLUE(Achievement.getName(id)));
			}

			achievementData[id] += progression;

			if (update) {
				AchievementsTab.updateAchievementStatus(id, getProgress(id), this);
			}

			if (achievementData[id] > Achievement.getProgress(id)) {
				achievementData[id] = Achievement.getProgress(id);
			}

			if (achievementComplete(id)) {
				actionSender.interfaceColor(Achievement.ACHIEVEMENTS[id].getInterfaceID(), 0X3366);
				actionSender.sendMessage("<img=" + UserAccount.CROWN_SUPPORT + ">Achievement completed: "
						+ ((gameFrame.getRoot() != 548) ? Text.LIGHT_ORANGE(Achievement.getName(id))
								: Text.DARK_ORANGE(Achievement.getName(id)))); //// +
																				//// "
																				//// "
																				//// +
																				//// id
																				//// +
																				//// "
																				//// "
																				//// +
																				//// Achievement.getProgress(id));
				actionSender.sendMessage("<img=" + UserAccount.CROWN_SUPPORT + ">You have been awarded: "
						+ Achievement.getCash(id) + " gp");
				AchievementsTab.updateAchievementStatus(id, getProgress(id), this);
				Achievement.giveCash(Achievement.getCash(id), this);
				World.getWorld().getScriptManager().directTrigger(this, null, Trigger.ACHIEVEMENT_COMPLETE, id);

				if (Achievement.ACHIEVEMENTS[id].getPkp() > 0) {
					addPoints(Points.PK_POINTS_2, Achievement.ACHIEVEMENTS[id].getPkp());
				}

				if (Achievement.ACHIEVEMENTS[id].getType() == 2) {
					World.getWorld().globalMessage("<img=" + UserAccount.CROWN_SUPPORT + ">" + getUsername()
							+ " has just completed hard achievement " + Achievement.ACHIEVEMENTS[id].getTitle());
				}
			}
		} catch (Exception ee) {
			ee.printStackTrace();
		}
	}

	/**
	 * Method setLastFightingWith Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param time
	 */
	public void setLastFightingWith(int time) {
		this.lastAttackedTime = time;
	}

	/**
	 * Method didRush Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 *
	 * @return
	 */
	public boolean didRush(Player player) {
		return Core.timeSince(lastAttackedTime) > 20;
	}

	/**
	 * Method getLastAttacked Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Player getLastAttacked() {
		return last_attacked;
	}

	/**
	 * Method setLastAttacked Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 */
	public void setLastAttacked(Player player) {
		last_attacked = player;
	}

	/**
	 * Method updateFollowList Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateFollowList() {
		for (int i = 0; i < followingList.size(); i++) {
			if ((followingList.get(i).getFollowing() == null)
					|| (followingList.get(i).getFollowing().getFollowing() != this)
					|| (Point.getDistance(followingList.get(i).getLocation(), getLocation()) > 15)) {
				followingList.remove(i);
			}
		}
	}

	/**
	 * Method removeFollower Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 */
	public void removeFollower(Player player) {
		if (followingList.contains(player)) {
			followingList.remove(player);
		}
	}

	/**
	 * Method checkFollow Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void checkFollow() {
		if ((following != null) && (following.getFollowing() instanceof NPC)) {
			following.updateMovement();

			if (isRunning()) {
				following.updateMovement();
			}
		}
	}

	/**
	 * Method moveFollow Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void moveFollow() {
	}

	/**
	 * Method dispatch Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param builder
	 */
	public void dispatch(MessageBuilder builder) {
		getIoSession().write(builder.toPacket());
	}

	/**
	 * Method walkTo Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param entity
	 */
	public void walkTo(Entity entity) {
		if (Walking.blockWalking(this)) {
			getActionSender().sendCloseWalkingFlag();

			return;
		}

		Walking.resetWalking(this);
		this.getRequest().setRequestLocation(entity.getLocation());
		requestWalk(entity.getX(), entity.getY());
	}

	/**
	 * Method requestWalk Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 */
	public void requestWalk(int x, int y) {
		if (Walking.blockWalking(this)) {
			getActionSender().sendCloseWalkingFlag();

			return;
		}

		if (noClip) {
			getRoute().resetPath();
			getRoute().addPoint(Point.location(x, y, getHeight()));

			return;
		}

		Walking.resetWalking(this);
		World.getWorld().getPathService().walkToLocation(this, x, y);
	}

	/**
	 * Method isFollowing Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 *
	 * @return
	 */
	public boolean isFollowing(Player player) {
		return (following != null) && (following.getFollowing() == player);
	}

	/**
	 * Method isTenth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param skillid
	 *
	 * @return
	 */
	public boolean isTenth(int skillid) {
		return (getPrestige(skillid) == 10) && (getCurStat(skillid) >= 99);
	}

	/**
	 * Method newHitmanGame Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param diff
	 */
	public void newHitmanGame(int diff) {
		account.setHitmanGame(HitmanGame.generate(this, diff));
	}

	/**
	 * Method setCurrentHouse Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param h
	 */
	public void setCurrentHouse(House h) {
		this.current_house = h;
	}

	/**
	 * Method getCurrentHouse Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public House getCurrentHouse() {
		return current_house;
	}

	/**
	 * Method getCurrentTask Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */

	/**
	 * Method getTowerGame Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public DeathTowerGame getTowerGame() {
		if (!(getCurrentInstance() instanceof DeathTowerGame)) {
			return null;
		}

		return (DeathTowerGame) getCurrentInstance();
	}

	/**
	 * Method getParty Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Party getParty() {
		return my_dung_party;
	}

	/**
	 * Method setParty Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param party
	 *
	 * @return
	 */
	public Party setParty(Party party) {
		return my_dung_party = party;
	}

	/**
	 * Method getDungeon Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Dungeon getDungeon() {
		if (getCurrentInstance() instanceof Dungeon) {
			return (Dungeon) getCurrentInstance();
		}

		return null;
	}

	/**
	 * Method setChannel Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param c
	 */
	public void setChannel(ClanChannel c) {
		currentChannel = c;
	}

	/**
	 * Method getChannel Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ClanChannel getChannel() {
		return currentChannel;
	}

	/**
	 * Method getFunctions Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public PlayerScriptFunctions getFunctions() {
		return functions;
	}

	/**
	 * Method setAutocast Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param cast
	 */
	public void setAutocast(int cast) {
		if (autoCast != -1) {
		}

		autoCast = cast;

		if (cast == -1) {
			getActionSender().sendVar(108, -1);
			getActionSender().sendVar(43, account.get(43));
		} else {
			getActionSender().sendVar(43, -1);
		}
	}

	/**
	 * Method getAutoCast Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getAutoCast() {
		return autoCast;
	}

	/**
	 * Method getZombiesGame Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Zombies getZombiesGame() {
		return zombiesGame;
	}

	/**
	 * Method setZombiesGame Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param s
	 */
	public void setZombiesGame(Zombies s) {
		zombiesGame = s;
	}

	/**
	 * Method getFarmPatches Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public HashMap<Patch, Integer> getFarmPatches() {
		return known_patches;
	}

	/**
	 * Method setMovementLock Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param b
	 */
	public void setMovementLock(boolean b) {
		movementLock = b;
	}

	/**
	 * Method getRenderAnimation Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getRenderAnimation() {
		if (renderAnim != -1) {
			return renderAnim;
		}

		if (this.getCurrentAction() instanceof AgilityWalkAction) {
			AgilityWalkAction ac = (AgilityWalkAction) this.getCurrentAction();

			if (ac.getRenderAnim() != -1) {
				return ac.getRenderAnim();
			}
		} else if (this.getAppearance().getTransform() != null) {
			if ((this.getAppearance().getTransform() != null)
					&& (this.getAppearance().getTransform().getStandAnim() != -1)) {
				return this.getAppearance().getTransform().getStandAnim();
			}
		}

		return getEquipment().getWeapon().getRenderAnimation();
	}

	/**
	 * Method setRenderAnim Created on 14/08/28
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param renderAnim
	 */
	public void setRenderAnim(int renderAnim) {
		this.renderAnim = renderAnim;
		getAppearance().setChanged(true);
	}

	/**
	 * Method increaseSumSpec Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void increaseSumSpec() {
		if (this.summ_spec_pts < 60) {
			this.summ_spec_pts += 5;
		}

		SummoningTab.updateSpecial(this);
	}

	/**
	 * Method isSendingClanMsg Created on 14/08/28
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isSendingClanMsg() {
		return sendingClanMsg;
	}

	/**
	 * Method setSendingClanMsg Created on 14/08/28
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param sendingClanMsg
	 */
	public void setSendingClanMsg(boolean sendingClanMsg) {
		this.sendingClanMsg = sendingClanMsg;
	}

	/**
	 * Method resetWalking Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void resetWalking() {
		getRoute().resetPath();
		World.getWorld().getPathService().removePathIfExists(this);
	}

	/**
	 * Method resetSumSpec Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void resetSumSpec() {
		this.summ_spec_pts = 60;

		if (summ_spec_pts < 0) {
			summ_spec_pts = 0;
		}

		SummoningTab.updateSpecial(this);
	}

	/**
	 * Method removeSpecPts Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param am
	 */
	public void removeSpecPts(int am) {
		this.summ_spec_pts -= am;

		if (summ_spec_pts < 0) {
			summ_spec_pts = 0;
		}

		SummoningTab.updateSpecial(this);
	}

	/**
	 * Method setTextUpdate Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param str
	 */
	public void setTextUpdate(String str) {
		text_update = str;
	}

	/**
	 * Method getTextUpdate Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String getTextUpdate() {
		return text_update;
	}

	/**
	 * Method isTeleblocked Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isTeleblocked() {
		return tele_block != null;
	}

	/**
	 * Method getTeleBlockMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String getTeleBlockMessage() {
		return tele_block;
	}

	/**
	 * Method setTeleBlock Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param str
	 */
	public void setTeleBlock(String str) {
		if (str.equalsIgnoreCase("null")) {
			str = null;
		}

		this.tele_block = str;
	}

	/**
	 * Method setUID Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param hash
	 */
	public void setUID(long hash) {
		this.uid = hash;
	}

	/**
	 * Method getUID Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public long getUID() {
		return uid;
	}

	/**
	 * Method questStarted Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param questId
	 *
	 * @return
	 */
	public boolean questStarted(int questId) {
		return quest_stages[questId][0];
	}

	/**
	 * Method getCurStage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param questId
	 *
	 * @return
	 */
	public int getCurStage(int questId) {
		for (int i = 0; i < quest_stages[questId].length; i++) {
			if (!quest_stages[questId][i]) {
				return i;
			}
		}

		return -1;
	}

	/**
	 * Method quest_stage_completed Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param questId
	 * @param quest_stage
	 *
	 * @return
	 */
	public boolean quest_stage_completed(int questId, int quest_stage) {
		return quest_stages[questId][quest_stage];
	}

	/**
	 * Method setQuestStage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param q
	 * @param stage
	 */
	public void setQuestStage(int q, int stage) {
		quest_stages[q][stage] = true;
	}

	/**
	 * Method getQuestStages Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean[][] getQuestStages() {
		return quest_stages;
	}

	/**
	 * Method set_quest_stage_completed Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param questId
	 * @param stage
	 */
	public void set_quest_stage_completed(int questId, int stage) {
		quest_stages[questId][stage] = true;

		if (stage == Quest.getQuest(questId).getStages().length - 1) {
			World.getWorld().getScriptManager().directTrigger(this, null, Trigger.QUEST_COMPLETE, questId);
		}
	}

	/**
	 * Method quest_completed Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param questId
	 *
	 * @return
	 */
	public boolean quest_completed(int questId) {
		Quest quest = Quest.getQuest(questId);

		if (quest == null) {
			return false;
		}

		return quest_stages[questId][quest.getStages().length - 1];
	}

	/**
	 * Method setAnchor Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void setAnchor(String id) {
		block_anchor = id;
	}

	/**
	 * Method getAnchor Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String getAnchor() {
		return block_anchor;
	}

	/**
	 * Method getLinked_npcs Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public LinkedList<NPC> getLinked_npcs() {
		return linked_npcs;
	}

	/**
	 * Method unlink_npc Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npc
	 */
	public void unlink_npc(NPC npc) {
		this.getActionSender().markNpc(-1);
		this.linked_npcs.remove(npc);
	}

	/**
	 * Method clear_linked_npcs Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void clear_linked_npcs() {
		for (NPC npc : linked_npcs) {
			npc.unlink();
		}

		actionSender.markNpc(-1);
		linked_npcs.clear();
	}

	/**
	 * Method add_linked_npc Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npc
	 * @param target_indicator
	 */
	public void add_linked_npc(NPC npc, boolean target_indicator) {
		linked_npcs.add(npc);

		if (target_indicator) {
			actionSender.markNpc(npc.getIndex());
		}

		npc.setCurrentInstance(getCurrentInstance());
	}

	/**
	 * Method getBobItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getBobItems() {
		return bobItems;
	}

	/**
	 * Method setBobItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param bobItems
	 */
	public void setBobItems(int[] bobItems) {
		this.bobItems = bobItems;
	}

	/**
	 * Method setBobItem Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param slot
	 * @param amt
	 * @param item
	 */
	public void setBobItem(int slot, int amt, int item) {
		bobItems[slot] = item;
		bobItemsC[slot] = amt;
	}

	/**
	 * Method getBobItemsC Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getBobItemsC() {
		return bobItemsC;
	}

	/**
	 * Method setBobItemsC Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param bobItemsC
	 */
	public void setBobItemsC(int[] bobItemsC) {
		this.bobItemsC = bobItemsC;
	}

	/**
	 * Method getCurrSummonPts Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getCurrSummonPts() {
		return currSummonPts;
	}

	/**
	 * Method setCurrSummonPts Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param currSummonPts
	 */
	public void setCurrSummonPts(int currSummonPts) {
		this.currSummonPts = currSummonPts;
	}

	/**
	 * Method getCurrentFamiliar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public NPC getCurrentFamiliar() {
		return currentFamiliar;
	}

	/**
	 * Method setCurrentFamiliar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param currentFamiliar
	 */
	public void setCurrentFamiliar(NPC currentFamiliar) {
		this.currentFamiliar = currentFamiliar;
	}

	/**
	 * Method getMovementMask Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ForceMovement getMovementMask() {
		return movementMask;
	}

	/**
	 * Method setMovementMask Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param movementMask
	 */
	public void setMovementMask(ForceMovement movementMask) {
		this.movementMask = movementMask;
	}

	/**
	 * Method getDuelRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getDuelRequest() {
		return duelRequest;
	}

	/**
	 * Method setDuelRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param duelRequest
	 */
	public void setDuelRequest(int duelRequest) {
		this.duelRequest = duelRequest;
	}

	/**
	 * Method getSummonSpecPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getSummonSpecPoints() {
		return summ_spec_pts;
	}

	/**
	 * Method setSumSpecPts Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param spec
	 */
	public void setSumSpecPts(int spec) {
		this.summ_spec_pts = spec;
	}

	/**
	 * Method hasSlayerTask Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean hasSlayerTask() {
		return this.task != null;
	}

	/**
	 * Method registerSlayerKill Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 *
	 * @return
	 */
	public SlayerTask getSlayerTask() {
		return this.task;
	}

	/**
	 * Method registerSlayerKill Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 *
	 * @param task
	 * @param p
	 */

	public void taskFinished(SlayerTask task, Player p) {
		int totalPoints = (task.getKillsToComplete() / 5);
		if (task.isDuoTask())
			totalPoints = task.getKillsToComplete() / 3;

		int kills = task.getKills(p);

		p.addPoints(Points.SLAYER_POINTS, totalPoints);
		p.getActionSender().sendMessage("You have completed your duo slayer task");

		if ((task.getTaskId() == 9467) || (task.getTaskId() == 9463) || (task.getTaskId() == 9465)) {
			p.addAchievementProgress2(Achievement.WORM_ENTHUSIAST, 1);
			p.addAchievementProgress2(Achievement.WORM_VETERAN, 1);
		}

		for (int i = 42; i < 46; i++) {
			p.addAchievementProgress2(i, 1);
		}

		p.addAchievementProgress2(Achievement.KURADEL_SENPAI, 1);
		p.addAchievementProgress2(Achievement.KURADEL_SENPAI_II, 1);
		p.addAchievementProgress2(Achievement.KURADEL_SENPAI_III, 1);
		p.addAchievementProgress2(Achievement.SLAYER_MASTER, 1);

		if (task.getKillsToComplete() >= 80) {
			this.addAchievementProgress2(Achievement.IN_IT_FOR_THE_LONG_HAUL, 1);
		}

		p.setSlayerTask(null);
	}

	/**
	 * Method registerSlayerKill Created on 14/12/23
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npc
	 */
	public void registerSlayerKill(NPC npc) {
		if (this.task == null) {
			return;
		}

		this.task.registerKill(this);

		if (npc.getId() == 1620) {
			addAchievementProgress2(Achievement.STARGAZER, 1);
		}

		if (this.task.isFinished()) {
			SlayerTask task = this.task;

			taskFinished(this.task, this);

			if (task.isDuoTask()) {
				Player pla = task.getOpposite(this);

				if (pla != null && pla != this) {
					taskFinished(task, pla);
				}
			}
			this.task = null;
		}
	}

	/**
	 * Method get_slay_kill_id Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 *
	 * @param task
	 */
	public void setSlayerTask(SlayerTask task) {
		this.task = task;
	}

	/**
	 * Method getNumberOfPrestiges Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getNumberOfPrestiges() {
		int c = 0;

		for (int i = 0; i < 25; i++) {
			c += prestige[i];
		}

		return c;
	}

	/**
	 * Method getPrestigesForRank Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param rank
	 *
	 * @return
	 */
	public static int getPrestigesForRank(int rank) {
		switch (rank) {
		case RANK_ROOKIE:
			return 0;

		case RANK_NOV:
			return 4;

		case RANK_SIR:
			return 10;

		case RANK_HERO:
			return 30;

		case RANK_LEGEND:
			return 65;

		case RANK_ELITE:
			return 200;
		}

		return -1;
	}

	/**
	 * Method getRank Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getRank() {
		int prest = getNumberOfPrestiges();

		if (prest < 4) {
			return RANK_ROOKIE;
		}

		if (prest < 10) {
			return RANK_NOV;
		}

		if (prest < 30) {
			return RANK_SIR;
		}

		if (prest < 65) {
			return RANK_HERO;
		}

		if (prest < 120) {
			return RANK_LEGEND;
		}

		if (prest < 200) {
			return RANK_LORD;
		}

		return RANK_ELITE;
	}

	/**
	 * Method getFriendsList Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public FriendsList getFriendsList() {
		return friendsUpdater;
	}

	/**
	 * Method getPlayerIgnores Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public LinkedList<Long> getPlayerIgnores() {
		return playerIgnores;
	}

	/**
	 * Method isIgnored Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param username
	 *
	 * @return
	 */
	public boolean isIgnored(String username) {
		return playerIgnores.contains(Text.longForName(username));
	}

	/**
	 * Method getPlayerFriends Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public synchronized Vector<Long> getPlayerFriends() {
		return playerFriends;
	}

	/**
	 * Method getNewScriptQueue Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ArrayList<Scope> getNewScriptQueue() {
		return backLog;
	}

	/**
	 * Method on_unlink Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void on_unlink() {
		if ((task != null) && task.isDuoTask()) {
			task.leaveTask(this);
			task = null;
		}

		if (getAccount().hasVar("cannon")) {
			DwarfMultiCannon cannon = (DwarfMultiCannon) getAccount().getVar("cannon").getFullData();

			cannon.destruct(true);

			return;
		}

		getGameFrame().closeAll();
		ScriptRuntime.onUnlink(this);

		if (getGameFrame().getDuel() != null) {
			getGameFrame().getDuel().on_leave(this);
		}

		if (currentFamiliar != null) {
			currentFamiliar.unlink();
		}

		if (getCurrentInstance() instanceof Party) {
			Party p = (Party) getCurrentInstance();

			p.unlink(getPlayer());
		}

		if (getCurrentHouse() != null) {
			getCurrentHouse().on_unlink(this);
		}

		if (getCurrentInstance() instanceof Dungeon) {
			getDungeon().destructPlayer(this);
		}

		if (getCutScene() != null) {
			teleport(getCutScene().getLastLocation().getX(), getCutScene().getLastLocation().getY(),
					getCutScene().getLastLocation().getHeight());
		}

		clear_linked_npcs();

		if (getCurrentInstance() != null) {
			getCurrentInstance().unlink_all();
		}

		if (getChannel() != null) {
			getChannel().leave(this);
		}
	}

	/**
	 * Method setBackLog Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param backLog
	 */
	public void setBackLog(ArrayList<Scope> backLog) {
		this.backLog = backLog;
	}

	/**
	 * Method hasPacket Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 *
	 * @return
	 */
	public boolean hasPacket(Packet p) {
		int amount = 0;

		for (int i = 0; i < packets.size(); i++) {
			Packet packet = packets.get(i);

			if (packet.getId() == p.getId()) {
				amount++;
			}

			if ((packet.getId() == p.getId()) && (amount > 8)) {
				packets.set(i, p);

				return true;
			}
		}

		return amount > Config.PACKET_THROTTLE_MAX;
	}

	/**
	 * Method setLastPacket Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param time
	 */
	public void setLastPacket(long time) {
		this.lastPacket = time;
	}

	/**
	 * Method getLastPacket Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public long getLastPacket() {
		return lastPacket;
	}

	/**
	 * Method registerPacket Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 */
	public void registerPacket(Packet p) {
		synchronized (packets) {
			getPackets().add(p);
		}
	}

	/**
	 * Method getPackets Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public synchronized ArrayList<Packet> getPackets() {
		return packets;
	}

	/**
	 * Method getPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public int getPoints(int id) {
		return curPoints[id];
	}

	/**
	 * Method addPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param pts
	 */
	public void addPoints(int id, int pts) {
		if (id == Points.SKILLING_POINTS) {
			return;
		}

		if (id == Points.DUNGEONEERING_POINTS) {
			pts *= 1.4;
		}

		if (((id == Points.PK_POINTS) || (id == Points.PK_POINTS_2)) && (pts > 0)) {
			getAccount().setTotalPVPPointsReceived(getAccount().getTotalPVPPointsReceived() + pts);
		}

		if (pts < 0) {
			curPoints[id] -= Math.abs(pts);

			if (curPoints[id] < 0) {
				curPoints[id] = 0;
			}

			return;
		}

		if ((id == Points.DUNGEONEERING_POINTS) || (id == Points.SLAYER_POINTS) || (id == Points.SKILLING_POINTS)) {
			if (getRights() >= DONATOR) {
				pts *= 1.30;
			}
		}

		if ((id == Points.SLAYER_POINTS) && (EventManager.currentDay() == Calendar.MONDAY)) {
			pts *= 2;
		}

		if ((id == Points.SKILLING_POINTS) && (EventManager.currentDay() == Calendar.SATURDAY)) {
			pts *= 2;
		}

		if (id == Points.DUNGEONEERING_POINTS) {
			pts *= 2;
		}

		if ((id == Points.SLAYER_POINTS) || (id == Points.SKILLING_POINTS)) {

			// pts *= 2;
		}

		curPoints[id] += pts;
		getActionSender().sendMessage("You have earned " + pts + " " + Points.getNameForId(id) + "points.");
	}

	/**
	 * Method hasPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param pts
	 *
	 * @return
	 */
	public boolean hasPoints(int id, int pts) {
		return curPoints[id] >= pts;
	}

	/**
	 * Method subtractPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param pts
	 */
	public void subtractPoints(int id, int pts) {
		curPoints[id] -= pts;

		if (curPoints[id] < 0) {
			curPoints[id] = 0;
		}
	}

	/**
	 * Method setPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param pts
	 */
	public void setPoints(int id, int pts) {
		curPoints[id] = pts;
	}

	/**
	 * Method getCurrentAction Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Action getCurrentAction() {
		return currentAction;
	}

	/**
	 * Method setCurrentAction Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param currentAction
	 */
	public void setCurrentAction(Action currentAction) {
		this.currentAction = currentAction;
	}

	/**
	 * Method getDoorsInView Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public LinkedList<Door> getDoorsInView() {
		return doorsInView;
	}

	/**
	 * Method updateDoorsInView Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateDoorsInView() {
		for (Iterator<Door> iter = doorsInView.iterator(); iter.hasNext();) {
			Door d = iter.next();

			if (Point.getDistance(getLocation(), d.getLocation()) >= 10) {
				iter.remove();
			}
		}

		ArrayList<Door> doors = entitiesInView[5];

		for (Door d : doors) {
			if ((d.getHeight() == getHeight()) && !doorsInView.contains(d)) {
				doorsInView.add(d);
				knownDoorIds.put(d, d.getCurrentOffset() - 1);
			}
		}
	}

	/**
	 * Method setKnownDoorId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param d
	 * @param door
	 */
	public void setKnownDoorId(Door d, int door) {
		knownDoorIds.put(d, door);
	}

	/**
	 * Method getKnownDoorId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param d
	 *
	 * @return
	 */
	public int getKnownDoorId(Door d) {
		return knownDoorIds.get(d);
	}

	/**
	 * Method getTradeRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getTradeRequest() {
		return tradeRequest;
	}

	/**
	 * Method setTradeRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param tradeRequest
	 */
	public void setTradeRequest(int tradeRequest) {
		this.tradeRequest = tradeRequest;
	}

	/**
	 * Method getBank Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Bank getBank() {
		return bank;
	}

	/**
	 * Method wasAttackedBy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 *
	 * @return
	 */
	public boolean wasAttackedBy(Player pla) {
		return wasAttackedBy.contains(pla);
	}

	/**
	 * Method didAttack Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 *
	 * @return
	 */
	public boolean didAttack(Player pla) {
		return didAttack.contains(pla);
	}

	/**
	 * Method addAttackedBy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 */
	public void addAttackedBy(Player pla) {
		wasAttackedBy.add(pla);
	}

	/**
	 * Method addDidAttack Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 */
	public void addDidAttack(Player pla) {
		didAttack.add(pla);
	}

	/**
	 * Method getAreas Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ArrayList<Zone> getAreas() {
		return knownZones;
	}

	/**
	 * Method getBoostedHealth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getBoostedHealth() {
		return boostedHealth;
	}

	/**
	 * Method setBoostedHealth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param health
	 */
	public void setBoostedHealth(int health) {
		this.boostedHealth = health;
	}

	/**
	 * Method getPrayerBook Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public PrayerBook getPrayerBook() {
		return prayerBook;
	}

	/**
	 * Method getRequestedSpell Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getRequestedSpell() {
		return requestedSpell;
	}

	/**
	 * Method setRequestedSpell Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param spell
	 */
	public void setRequestedSpell(int spell) {
		this.requestedSpell = spell;
	}

	/**
	 * Method getRights Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getRights() {
		return rights;
	}

	/**
	 * Method setRights Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param rights
	 */
	public void setRights(int rights) {
		this.rights = rights;
	}

	/**
	 * Method groupAtleast Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param right
	 *
	 * @return
	 */
	public boolean groupAtleast(int right) {
		return rights >= right;
	}

	/**
	 * Method getThrottleFilter Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public PacketThrottleFilter getThrottleFilter() {
		return throttleFilter;
	}

	/**
	 * Method getSpecialMultiplier Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public double getSpecialMultiplier() {
		return specialMultiplier;
	}

	/**
	 * Method isTriggerBanned Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 *
	 * @param type
	 * @param argumentHash
	 *
	 * @return
	 */
	public boolean isTriggerBanned(int type, int argumentHash) {
		for (ScriptSuspension bans : this.scriptDelays) {
			if ((bans.getTriggerType() == type) && (argumentHash == bans.getArgumentHash())) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Method isTriggerAlreadyRunning Created on 14/11/05
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param trigger
	 *
	 * @return
	 */
	public boolean isTriggerAlreadyRunning(Trigger trigger) {
		for (Scope scope : scriptsToProcess) {
			if (scope.getRunning() == trigger) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Method terminateScripts Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean terminateScripts() {
		if (getNewScriptQueue().size() > 0) {
			for (Scope scope : getNewScriptQueue()) {
				if (scope.isActiveAction()) {
					scriptsToProcess.add(scope);
				}
			}
		}

		getNewScriptQueue().clear();

		if (scriptsToProcess.size() == 0) {
			return true;
		}

		int c = 0;

		for (Iterator<Scope> scriptIterator = scriptsToProcess.iterator(); scriptIterator.hasNext();) {
			Scope script = scriptIterator.next();

			if (script.isDelayed()) {
				continue;
			}

			if (script.isActiveAction()) {
				continue;
			}

			c++;
			script.terminate();
		}

		return true;
	}

	/**
	 * Method getScriptsToProcess Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ArrayList<Scope> getScriptsToProcess() {
		return scriptsToProcess;
	}

	/**
	 * Method setSpecialMultiplier Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param specialMultiplier
	 */
	public void setSpecialMultiplier(double specialMultiplier) {
		this.specialMultiplier = specialMultiplier;
	}

	/**
	 * Method loadStandardConfig Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void loadStandardConfig() {
		addDamageFilter("defence", DamageFilter.PLAYER_DEFENCE_DAMAGE_FILTER);
		addDamageFilter("prayer", DamageFilter.PRAYER_DAMAGE_FILTER);
	}

	/**
	 * Method loadFamiliar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void loadFamiliar() {
		if (account.hasVar("familiar_request")) {
			currentFamiliar = new NPC(account.getInt32("familiar_request"), this);
			currentFamiliar.setDeathHandler(Summoning.SUMMONING_DEATH_HANDLER);
			account.removeVar("familiar_requet");
			account.removeVar("summon_time");
			SummoningTab.setFollower(this, account.getInt32("summon_id"));
			World.getWorld().registerNPC(currentFamiliar);
		}
	}

	/**
	 * Method onDisconnect Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void onDisconnect() {
		synchronized (tickEvents) {
			tickEvents.add(new PlayerTickEvent(this, 1) {
				@Override
				public void doTick(Player owner) {
					ScriptRuntime.onDC(owner);
					terminate();
				}
			});
		}
	}

	/**
	 * Method setStartupEvents Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void setStartupEvents() {
		addEventIfAbsent(new PlayerRestorationTick(this, 1, false, true));
		addEventIfAbsent(new GeneralPlayerUpdateTask(this));
		addEventIfAbsent(new ZoneUpdateTick(this));
	}

	/**
	 * Method prestige Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param skill
	 */
	public void prestige(int skill) {
		this.prestige[skill]++;
		this.exps[skill] = 0;
		this.curStats[skill] = 1;
		this.maxStats[skill] = 1;

		if (skill == 3) {
			currentHealth = 10;
			exps[skill] = GameMath.getXPForLevel(10);
			maxStats[skill] = 10;
		} else if (skill == 5) {
			getPrayerBook().setPoints(1);
		} else if (skill == 23) {
			currSummonPts = 1;
		}

		actionSender.sendStat(skill);
		getAppearance().setChanged(true);
	}

	/**
	 * Method removeEnergy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param amt
	 */
	public void removeEnergy(int amt) {
		currentEnergy -= amt;

		if (currentEnergy < 0) {
			currentEnergy = 0;
		}

		getActionSender().updateEnergy();
	}

	/**
	 * Method addEnergy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param energy
	 */
	public void addEnergy(int energy) {
		currentEnergy += energy;

		if (currentEnergy > 100) {
			currentEnergy = 100;
		}

		getActionSender().updateEnergy();
	}

	/**
	 * Method removeEnergy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void removeEnergy() {
		if (currentEnergy > 0) {
			currentEnergy -= GameMath.getEnergyDeplecation(getEquipment().getWeight(), getCurStat(16));
			getActionSender().updateEnergy();
		}
	}

	/**
	 * Method addEventIfAbsent Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param t
	 */
	public void addEventIfAbsent(PlayerTickEvent t) {
		for (PlayerTickEvent events : tickEvents) {
			if (events.getClass() == t.getClass()) {
				return;
			}

			if ((events.getID() != -1) && (events.getID() == t.getID())) {
				return;
			}
		}

		tickEvents.addLast(t);
	}

	/**
	 * Method set_prest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param count
	 */
	public void set_prest(int id, int count) {
		this.prestige[id] = count;
		actionSender.sendStat(id);
	}

	/**
	 * Method getPrestige Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param skillId
	 *
	 * @return
	 */
	public int getPrestige(int skillId) {
		return prestige[skillId];
	}

	/**
	 * Method getMaxStat Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param stat
	 *
	 * @return
	 */
	public int getMaxStat(int stat) {
		return maxStats[stat];
	}

	/**
	 * Method addXp Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 *
	 * @param stat
	 * @param lvl
	 */
	public void setMaxStat(int stat, int lvl) {
		this.maxStats[stat] = lvl;
	}

	/**
	 * Method addXp Created on 15/05/12
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param skillId
	 * @param experience
	 */
	public void addXp(int skillId, int experience) {
		int xp = experience;

		try {
			if (account.getFlag(Account.XP_LOCK) || (getGameFrame().getDuel() != null)) {
				return;
			}
		} catch (Exception ee) {
		}

		if (this.exps[skillId] + experience > 500_000_000) {
			this.exps[skillId] = 500_000_000;
		}

		// it wont display properly, so we'll just make it reset everytime they
		// gain experience

		if (Config.doubleXp) {
			experience *= 2;
		}

		if (account.getFlag(Account.DOUBLE_XP)) {
			experience *= 2;
		}

		if (account.getFlag(Account.DOUBLE_XP_2)) {
			experience *= 2;
		}

		if (getRights() == Player.DONATOR) {
			experience *= 1.05;
		}

		if (getRights() == Player.PLATINUM) {
			experience *= 1.10;
		}

		if ((getRights() == Player.PATRON) || (getRights() == Player.EXTREME_DONATOR)) {
			experience *= 1.15;
		}

		if (getTimers().timerActive(TimingUtility.DOUBLE_XP_TIMER)) {
			experience *= 2;
		}

		if (getTimers().timerActive(TimingUtility.DOUBLE_XP_TIMER_2)) {
			experience *= 2;
		}

		if (getEquipment().getId(Equipment.CAPE) == 15117) {
			experience *= 1.3;
		}

		if (equipment.getId(Equipment.HAT) == 15118) {
			experience *= 1.2;
		}

		int ironman = account.getIronManMode();

		experience *= Config.XP_RATE;

		if (ironman == Account.IRON_MAN_MEDIUM) {
			experience /= 2.5;
		} else if (ironman == Account.IRON_MAN_HARD) {
			experience /= 4;
		}

		// Thread.dumpStack();
		boolean xpBoost = false;

		if (isInWilderness() && (skillId <= 6) && (skillId != 5)) {
			experience = xp;
			xpBoost = true;
		}

		if (isInWilderness() && (skillId == 18)) {
			experience *= 3.5;
			if (getPlayer().getEquipment().getId(Equipment.RING) == 4202) {
				experience *= 1.5;
			}
		} else if ((skillId <= 4) && !isInWilderness()) {
			experience *= Config.COMBAT_XP_MODIFIER;
		}

		int brawlGloves = BrawlingGloves.getSkillID(getEquipment().getId(Equipment.HANDS));

		if (brawlGloves != -1) {
			if ((brawlGloves == -4) && (skillId <= 2)) {
				experience *= 1.5;
			} else if (skillId == brawlGloves) {
				experience *= 1.5;
			}
		}

		int previousLevel = GameMath.getLevelForXP(exps[skillId], skillId);

		account.addXPToCounter(experience);
		xp_counter += experience;
		actionSender.sendVar(1801, xp_counter * 10);

		if (experience < 0) {
			experience = Integer.MAX_VALUE;
		}

		if (!xpBoost) {
			experience *= Config.XP_MODIFIERS[skillId];
		}

		long l = exps[skillId] + experience;

		if ((l > Integer.MAX_VALUE) || (l < 0)) {
			exps[skillId] = 500000000;
			maxStats[skillId] = GameMath.getLevelForXP(exps[skillId], skillId);
		} else {
			exps[skillId] += experience;
		}

		actionSender.sendStat(skillId);

		int newLevel = GameMath.getLevelForXP(exps[skillId], skillId);

		if (previousLevel < newLevel) {

			// level up directTrigger goes here.
			if ((skillId == 3) && (getGameFrame().getDuel() == null)) {

				// this.setCurrentHealth(newLevel);
			} else if ((skillId == 5) && (getGameFrame().getDuel() == null)) {
				this.prayerBook.setPoints(newLevel);
			} else if (skillId == 23) {
				currSummonPts = newLevel;
			}

			this.maxStats[skillId] = newLevel;
			this.setCurStat(skillId, newLevel);
			this.getAppearance().setChanged(true);

			boolean b = (skillId <= 4 && newLevel < 80) && getAccount().getIronManMode() == 0;

			if (!b)
				account.setLevelUp(skillId, true);
			else
				getActionSender().sendMessage("You have just advanced a " + SkillGuide.SKILL_NAMES[skillId] + " level");

			if ((skillId > 6) && (newLevel == 99)) {
				World.globalMessage("<img=5><col=707081>News:</col><col=6B0606>" + getUsername()
						+ " has just reached 99 " + SkillGuide.SKILL_NAMES[skillId]);
			}

			if (gameFrame != null && !b) {
				gameFrame.setDialog(740);
			}
		}

		this.getActionSender().sendStat(skillId);

		if (this.exps[skillId] + experience > 500_000_000) {
			this.exps[skillId] = 500_000_000;
		}

		// at the end, we'll just reset it back to 500m
	}

	/**
	 * Method tap Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 */
	public void tap(Player player) {
		if (!tappers.contains(player)) {
			tappers.add(player);
		}
	}

	/**
	 * Method getXPCounter Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getXPCounter() {
		return xp_counter;
	}

	/**
	 * Method setXPCounter Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param counter
	 */
	public void setXPCounter(int counter) {
		this.xp_counter = counter;
	}

	/**
	 * Method isTapped Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isTapped() {
		return tappers.size() > 0;
	}

	/**
	 * Method log Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param str
	 */
	public void log(String str) {
		for (Player pla : tappers) {
			if (pla.isLoggedIn()) {
				pla.getActionSender().sendMessage("[" + getUsername() + "]: " + str);
			}
		}

		logs.add("[cycle " + Core.currentTime + "]" + getLocation() + "][" + getUID() + "]: " + str);

		if (logs.size() > Config.LOG_SPILL_RATE) {
			UserAccount.saveLogs(this, false);
			logs.clear();
		}
	}

	/**
	 * Method getAchievementProgress Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public int getAchievementProgress(int id) {
		return achievementData[id];
	}

	/**
	 * Method setAchievementProgress Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param prog
	 */
	public void setAchievementProgress(int id, int prog) {
		achievementData[id] = prog;
	}

	/**
	 * Method getLogs Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ArrayList<String> getLogs() {
		return logs;
	}

	/**
	 * Method declineRequest Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void declineRequest() {
		this.slayerJoinRequest = null;
	}

	/**
	 * Method setSlayerJoinRequest Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 */
	public void setSlayerJoinRequest(Player player) {
		this.slayerJoinRequest = player;
		this.slayerJoinRequest.slayerJoinRequested = this;
		actionSender.sendMessage(Text.BLUE(player.getUsername()
				+ " wishes to part up with you for duo slayer, rub your enchanted gem to accept."));
	}

	/**
	 * Method hasInvite Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean hasInvite() {
		return this.slayerJoinRequest != null;
	}

	/**
	 * Method acceptJoinRequest Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int acceptJoinRequest() {
		if (this.slayerJoinRequest == null) {
			return 0; // doesnt exist
		}

		if (this.slayerJoinRequest.getSlayerTask() == null) {
			this.slayerJoinRequest = null;

			return 1; // player doesnt have a task
		}

		if (this.slayerJoinRequest.getSlayerTask().isDuoTask()) {
			this.slayerJoinRequest = null;

			return 2;
		}

		if (this.slayerJoinRequest.slayerJoinRequested != this) {
			this.slayerJoinRequest = null;

			return 3;
		}

		if (!this.slayerJoinRequest.isLoggedIn()) {
			this.slayerJoinRequest = null;

			return 4;
		}

		if (slayerJoinRequest.getSlayerTask().getTotalKills() > 0) {
			this.slayerJoinRequest = null;

			return 5;
		}

		if (this.task != null) {
			if (this.task.isDuoTask()) {
				Player prevPartner = task.getOpposite(this);

				task.leaveTask(this);

				if (prevPartner != null) {
					prevPartner.getActionSender().sendMessage(getUsername() + " has left this slayer task");
				}
			}
		}

		slayerJoinRequest.getSlayerTask().join(this);
		slayerJoinRequest.getActionSender().sendMessage(Text.BLUE(getUsername() + " has joined your slayer task"));
		getActionSender().sendMessage(Text.BLUE("You join " + this.slayerJoinRequest.getUsername() + "'s task"));
		this.task = slayerJoinRequest.task;
		this.slayerJoinRequest = null;

		return 6;
	}

	/**
	 * Method drainStat Created on 14/12/20
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param stat
	 * @param amount
	 *
	 * @return
	 */
	public int drainStat(int stat, int amount) {
		int calc = curStats[stat] - amount;

		if (stat == 5) {
			getPrayerBook().removePoints(1);
			actionSender.sendStat(5);

			return 0;
		}

		curStats[stat] -= amount;

		if (curStats[stat] < 0) {
			curStats[stat] = 0;
		}

		actionSender.sendStat(stat);

		if (calc < 0) {
			return calc;
		}

		return amount;
	}

	/**
	 * Method getCurrentEnergy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public double getCurrentEnergy() {
		return currentEnergy;
	}

	/**
	 * Method setCurrentEnergy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param currentEnergy
	 */
	public void setCurrentEnergy(double currentEnergy) {
		this.currentEnergy = currentEnergy;
	}

	/**
	 * Method followPlayer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 */
	public void followPlayer(Player pla) {
		following = new PlayerFollowing(this, pla);

		if (!pla.followingList.contains(this)) {
			pla.followingList.add(this);
		}
	}

	/**
	 * Method followNpc Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npc
	 */
	public void followNpc(NPC npc) {
		following = new PlayerFollowing(this, npc);
	}

	/**
	 * Method isFollowing Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isFollowing() {
		return following != null;
	}

	/**
	 * Method unFollow Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void unFollow() {
		if (following == null) {
			return;
		}

		if (following.getFollowing() instanceof Player) {
			((Player) following.getFollowing()).getPlayer().removeFollower(this);
		}

		this.getFocus().unFocus();
		this.following = null;
	}

	/**
	 * Method getFollowing Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public PlayerFollowing getFollowing() {
		return following;
	}

	/**
	 * Method getCurDeathHandler Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public DeathHandler getCurDeathHandler() {
		return (curDeathHandler == null) ? defaultHandler : curDeathHandler;
	}

	/**
	 * Method setCurDeathHandler Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param curDeathHandler
	 */
	public void setCurDeathHandler(DeathHandler curDeathHandler) {
		this.curDeathHandler = curDeathHandler;
	}

	/**
	 * Method getTickEvents Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public LinkedBlockingDeque<PlayerTickEvent> getTickEvents() {
		return tickEvents;
	}

	/**
	 * Method updateActions Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateActions() {
		for (Iterator i = tickEvents.iterator(); i.hasNext();) {
			PlayerTickEvent event = (PlayerTickEvent) i.next();

			event.update();

			if (event.isTerminated()) {
				tickEvents.remove(event);
			}
		}
	}

	/**
	 * Method isSpecialHighlighted Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isSpecialHighlighted() {
		return specialHighlighted;
	}

	/**
	 * Method setHighlighted Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param b
	 */
	public void setHighlighted(boolean b) {
		specialHighlighted = b;
	}

	/**
	 * Method getCurrentSpecial Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public double getCurrentSpecial() {
		return currentSpecial;
	}

	/**
	 * Method setCurrentSpecial Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param spec
	 */
	public void setCurrentSpecial(double spec) {
		currentSpecial = spec;
	}

	/**
	 * Method getCombatHandler Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public CombatAdapter getCombatAdapter() {
		return combatAdapter;
	}

	/**
	 * Method getSettings Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Account getAccount() {
		return account;
	}

	/**
	 * Method getRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ActionRequest getRequest() {
		return actionRequest;
	}

	/**
	 * Method isTestPilot Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isTestPilot() {
		return isTestPilot;
	}

	/**
	 * Method isBusy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isBusy() {
		return currentAction != null;
	}

	/**
	 * Method getAction Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Action getAction() {
		return currentAction;
	}

	/**
	 * Method reload Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loadFrom
	 */
	public void reload(Player loadFrom) {
		this.ioSession = loadFrom.getIoSession();
		this.ioSession.setPlayer(this);
		this.setLoggedIn(true);
		this.setLastPacket(System.currentTimeMillis());

		// this.ioSession.getChannel().setAttachment(this);
		this.ioSession.getChannel().getPipeline().getContext(ioSession.getChannel().getPipeline().get("handler"))
				.setAttachment(ioSession);
		this.getAppearance().setChanged(true);
		this.setMapAreaChanged(true);
		this.setDisconTime(0);
		this.setVisible(true);
		this.itemsInView.clear();
		this.getWindowManager().resetKnownVariables();
		this.watchedPlayers = new StatefulEntityCollection<Player>();
		this.watchedNpcs = new StatefulEntityCollection<NPC>();
		this.knownStackCounts.clear();
		this.knownAppearanceIds.clear();
		this.watchedObjects = new StatefulEntityCollection<GameObject>();
		this.itemsInView = new StatefulEntityCollection<FloorItem>();
		this.knownObjects.clear();
		this.knownDoorIds.clear();
		this.doorsInView.clear();
		this.knownViewPointIds.clear();
		this.knownFacingIds.clear();
		this.knownAppearanceIds.clear();
		this.getFarmPatches().clear();
		this.rights = loadFrom.getRights();
	}

	/**
	 * Method getInventory Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public synchronized Inventory getInventory() {
		return inventory;
	}

	/**
	 * Method isActionsDisabled Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isActionsDisabled() {
		return actionDisable || isDead() || movementLock;
	}

	/**
	 * Method getHashingFrom Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Player getHashingFrom() {
		return hashingFrom;
	}

	/**
	 * Method isConnected Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isConnected() {
		if (isTestPilot()) {
			return true;
		}

		if (ioSession == null) {
			return false;
		}

		return ioSession.isConnected();
	}

	/**
	 * Method setActionsDisabled Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param b
	 */
	public void setActionsDisabled(boolean b) {
		this.actionDisable = b;
	}

	/**
	 * Method setDisconTime Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param time
	 */
	public void setDisconTime(long time) {
		disconTime = time;
	}

	/**
	 * Method isPreventUnlink Created on 14/08/28
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isPreventUnlink() {
		return preventUnlink;
	}

	/**
	 * Method setPreventUnlink Created on 14/08/28
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param preventUnlink
	 */
	public void setPreventUnlink(boolean preventUnlink) {
		this.preventUnlink = preventUnlink;
	}

	/**
	 * Method setHeadIcon Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param headIcon
	 */
	public void setHeadIcon(int headIcon) {
		if (this.headIcon != headIcon) {
			getAppearance().setChanged(true);
		}

		this.headIcon = headIcon;
	}

	/**
	 * Method setHeadicon2 Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param headIcon2
	 */
	public void setHeadicon2(int headIcon2) {
		this.headIcon2 = headIcon2;
	}

	/**
	 * Method getHeadicon Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getHeadicon() {
		return headIcon;
	}

	/**
	 * Method resetSummon Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void resetSummon() {
		if (currentFamiliar != null) {
			getTimers().setTime(TimingUtility.SUMMON_TIMER, 0);

			if (account.hasVar("bob_max") && (account.getInt32("bob_max") > 0)) {
				for (int i = 0; i < bobItems.length; i++) {
					if (bobItems[i] > 0) {
						new FloorItem(bobItems[i], bobItemsC[i], this.currentFamiliar.getX(),
								this.currentFamiliar.getY(), this.currentFamiliar.getHeight(), this);
					}
				}
			}

			account.setSetting("bob_max", 0);
			Arrays.fill(bobItems, -1);
			Arrays.fill(bobItemsC, -1);
			this.currentFamiliar.unlink();
			this.currentFamiliar = null;
			actionSender.sendBConfig(168, 4);
			account.removeVar("summon_id");
			SummoningTab.setFollower(this, -1);
		}
	}

	/**
	 * Method datedTimerUp Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 */
	public void datedTimerUp(int timer) {
		switch (timer) {
		}
	}

	/**
	 * Method timerUp Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 */
	public void timerUp(int timer) {
		switch (timer) {

		case TimingUtility.OVERLOAD_REPLENISH_TIMER:
			if (!isInWilderness() && getTimers().timerActive(TimingUtility.OVERLOAD_TIMER)) {
				getTimers().setTime(TimingUtility.OVERLOAD_REPLENISH_TIMER, Core.getTicksForSeconds(15));
				Potion.doOverload(this);

			}
			break;
		case TimingUtility.SKULL_TIMER:
			getAppearance().setChanged(true);

			break;

		case TimingUtility.SUMMON_TIMER:
			if (!isTenth(23)) {
				this.resetSummon();
			}

			break;
		}
	}

	/**
	 * Method getHeadicon2 Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getHeadicon2() {
		if (getTimers().timerActive(TimingUtility.SKULL_TIMER)) {
			if (PKHighscores.isWithinTop10(this)) {
				return 16;
			} else {
				return 1;
			}
		}

		return headIcon2;
	}

	/**
	 * Method getWindowManager Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public WindowManager getWindowManager() {
		return windowManager;
	}

	/**
	 * Method getActionSender Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ActionSender getActionSender() {
		return actionSender;
	}

	/**
	 * Method getLastChatMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public ChatMessage getLastChatMessage() {
		return lastChatMessage;
	}

	/**
	 * Method setLastChatMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param m
	 */
	public void setLastChatMessage(ChatMessage m) {
		lastChatMessage = m;
	}

	/**
	 * Method getEquipment Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public Equipment getEquipment() {
		return equipment;
	}

	/**
	 * Method setHashingFrom Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 */
	public void setHashingFrom(Player pla) {
		hashingFrom = pla;
	}

	/**
	 * Method setEquipment Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param equipment
	 */
	public void setEquipment(Equipment equipment) {
		this.equipment = equipment;
	}

	/**
	 * Method getPrivacySetting Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param idx
	 *
	 * @return
	 */
	public int getPrivacySetting(int idx) {
		return privacySettings[idx];
	}

	/**
	 * Method setPrivacySetting Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param idx
	 * @param i
	 */
	public void setPrivacySetting(int idx, int i) {
		privacySettings[idx] = i;
	}

	/**
	 * Method getCurStats Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getCurStats() {
		return curStats;
	}

	/**
	 * Method getCurStat Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param idx
	 *
	 * @return
	 */
	public int getCurStat(int idx) {
		return curStats[idx];
	}

	/**
	 * Method setCurStats Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param curStats
	 */
	public void setCurStats(int[] curStats) {
		this.curStats = curStats;
	}

	/**
	 * Method setCurStat Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param idx
	 * @param s
	 */
	public void setCurStat(int idx, int s) {
		curStats[idx] = s;
	}

	/**
	 * Method getExps Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getExps() {
		return exps;
	}

	/**
	 * Method setExps Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param exps
	 */
	public void setExps(int[] exps) {
		this.exps = exps;

		for (int i = 0; i < exps.length; i++) {
			maxStats[i] = GameMath.getLevelForXP(exps[i], i);
		}
	}

	/**
	 * Method setPrestiges Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param prestiges
	 */
	public void setPrestiges(int[] prestiges) {
		this.prestige = prestiges;
	}

	/**
	 * Method getUsername Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * Method getUsernameHash Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public long getUsernameHash() {
		return usernameHash;
	}

	/**
	 * Method isLoggedIn Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isLoggedIn() {
		if (this instanceof BotPlayer) {
			return true;
		}

		return loggedIn;
	}

	/**
	 * Method setLoggedIn Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com too many players in that region?? 985 9
	 *         x: 3213, y: 3447
	 *
	 * @param loggedIn
	 */
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	/**
	 * Method getAppearance Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public PlayerAppearance getAppearance() {
		return appearance;
	}

	/**
	 * Method setAppearance Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param appearance
	 */
	public void setAppearance(PlayerAppearance appearance) {
		this.appearance = appearance;
	}

	/**
	 * Method setCredentials Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param u
	 * @param p
	 * @param r
	 */
	public void setCredentials(String u, String p, boolean r) {
		username = u;
		usernameHash = Text.stringToLong(u);
		password = p;
		isHD = r;
	}

	/**
	 * Method isHD Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isHD() {
		return isHD;
	}

	/**
	 * Method getRawPassword Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String getRawPassword() {
		return password;
	}

	/**
	 * Method getDisconTime Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public long getDisconTime() {
		return disconTime;
	}

	/**
	 * Method onPathFinished Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param points
	 */
	public void onPathFinished(Queue<Point> points) {
		if (points != null) {
			synchronized (route) {
				route.addAll(points);
			}
		} else {
			actionSender.sendCloseWalkingFlag();
		}
	}

	/**
	 * Method getLastIP Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String getLastIP() {
		return lastIP;
	}

	/**
	 * Method getIoSession Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public PlaySession getIoSession() {
		return ioSession;
	}

	/**
	 * Method setLastIP Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param lastIP
	 */
	public void setLastIP(String lastIP) {
		this.lastIP = lastIP;
	}

	/**
	 * Method getCurrentIP Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String getCurrentIP() {
		return currentIP;
	}

	/**
	 * Method setCurrentIP Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param currentIP
	 */
	public void setCurrentIP(String currentIP) {
		this.currentIP = currentIP;
	}

	/**
	 * Method getResponseCode Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public byte getResponseCode() {
		return responseCode;
	}

	/**
	 * Method setResponseCode Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param responseCode
	 */
	public void setResponseCode(byte responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * Method isMapAreaChanged Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isMapAreaChanged() {
		return mapAreaChanged;
	}

	/**
	 * Method setMapAreaChanged Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param mapAreaChanged
	 */
	public void setMapAreaChanged(boolean mapAreaChanged) {
		if (is_con) {
			mapAreaChanged = false;
		}

		this.mapAreaChanged = mapAreaChanged;
	}

	/**
	 * Method isRunning Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * Method setRunning Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param running
	 */
	public void setRunning(boolean running) {
		this.running = running;
	}

	/**
	 * Method setKnownObjectId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param obj
	 * @param id
	 */
	public void setKnownObjectId(GameObject obj, int id) {
		knownObjects.put(obj, id);
	}

	/**
	 * Method getWatchedPlayers Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public StatefulEntityCollection<Player> getWatchedPlayers() {
		return watchedPlayers;
	}

	/**
	 * Method getWatchedNPCs Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public StatefulEntityCollection<NPC> getWatchedNPCs() {
		return watchedNpcs;
	}

	/**
	 * Method setKnownRegion Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 */
	public void setKnownRegion(int x, int y) {
		knownRegionX = x;
		knownRegionY = y;
	}

	/**
	 * Method getKnownRegion Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int[] getKnownRegion() {
		return new int[] { knownRegionX, knownRegionY };
	}

	/**
	 * Method populateEntitiesInView Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void populateEntitiesInView() {
		entitiesInView = getViewArea().getEntitiesInView();
	}

	/**
	 * Method teleport Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param height
	 * @param noReset
	 */
	public void teleport(int x, int y, int height, boolean noReset) {
		updatePathLocation();
		setLocation(Point.location(x, y, height), true);

		if ((getLocation().getRegionX() == -1) || (getLocation().getRegionY() == -1)) {
			setLocation(Point.location(3222, 3222, 0));
		}

		getRoute().resetPath();
		unFollow();
		setDidTeleport(true);
		getActionSender().sendCloseWalkingFlag();
		setTradeRequest(-1);

		if (!noReset) {
			getWindowManager().closeWindow();
			terminateScripts();
		}

		getFacingLocation().reset();
		terminateActions();

		if (IfClipped.getMovementStatus(Movement.NORTH, x, y, height) == 0) {
			setLastMovedLocation(Movement.getPointForDir(getLocation(), Movement.NORTH));
		} else {
			for (int i = 0; i < 7; i++) {
				if (IfClipped.getMovementStatus(i, x, y, height) == 0) {
					setLastMovedLocation(Movement.getPointForDir(getLocation(), i));
				}
			}
		}
	}

	/**
	 * Method teleport Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param height
	 */
	public void teleport(int x, int y, int height) {
		teleport(x, y, height, false);
	}

	/**
	 * Method removeWatchedObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param e
	 */
	public void removeWatchedObject(GameObject e) {
		watchedObjects.remove(e);
		knownObjects.remove(e);
	}

	/**
	 * Method getObjectId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public int getObjectId(GameObject id) {
		return knownObjects.get(id);
	}

	/**
	 * Method validatedWatchedObjects Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void validatedWatchedObjects() {
		ArrayList<GameObject> objsToRemove = null;

		for (GameObject e : watchedObjects.getKnownEntities()) {
			if ((e.getCurrentInstance() != getCurrentInstance())
					&& (e.getCurrentInstance() != Instance.GLOBAL_INSTANCE)) {
				removeWatchedObject(e);

				continue;
			}

			if (Point.getDistance(e.getLocation(), getLocation()) > 15) {
				removeWatchedObject(e);

				if (objsToRemove == null) {
					objsToRemove = new ArrayList<GameObject>();
				}

				objsToRemove.add(e);
			} else if (((e.getHeight() == getHeight()) && e.isRemoved()) || (e.getId() == 0)) {
				if ((e.getId() == 0) && (knownObjects.get(e) == e.offset())) {
					continue;
				}

				if (e.getId() == 0) {
					knownObjects.put(e, e.offset());
				}

				removeWatchedObject(e);
			}
		}

		if (objsToRemove != null) {
			getWatchedObjects().getRemovingEntities().removeAll(objsToRemove);
			getWatchedObjects().getKnownEntities().removeAll(objsToRemove);
		}
	}

	/**
	 * Method getWatchedObjects Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public StatefulEntityCollection<GameObject> getWatchedObjects() {
		return watchedObjects;
	}

	/**
	 * Method findNewObjects Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void findNewObjects() {
		ArrayList<GameObject> objects = entitiesInView[4];

		for (GameObject object : objects) {
			if (!watchedObjects.contains(object) && withinRange(object)) {
				watchedObjects.add(object);
				knownObjects.put(object, object.offset());
			}
		}
	}

	/**
	 * Method validatedWatchedPlayers Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void validatedWatchedPlayers() {
		for (Player p : watchedPlayers.getKnownEntities()) {
			if (!withinRange(p) || !p.isLoggedIn() || p.didTeleport()) {
				removeWatchedPlayer(p);
			}
		}
	}

	/**
	 * Method validatedWatchedNpcs Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void validatedWatchedNpcs() {

		for (NPC p : watchedNpcs.getKnownEntities()) {
			if (!withinRange(p) || p.didTeleport()) {
				removeWatchedNpc(p);
			}
		}
	}

	/**
	 * Method validatedFloorItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void validatedFloorItems() {
		for (FloorItem i : itemsInView.getKnownEntities()) {
			if (i.getCurrentInstance() != getCurrentInstance()) {
				itemsInView.remove(i);
				knownStackCounts.remove(i);

				continue;
			}

			if ((i.getHeight() != getHeight()) && (Point.getDistance(i.getX(), i.getY(), getX(), getY()) < 15)) {
				continue; // cant unvalidate yet
			}

			if ((Point.getDistance(i.getX(), i.getY(), getX(), getY()) > 15) || i.isRemoved()) {
				itemsInView.remove(i);
				knownStackCounts.remove(i);
			}
		}
	}

	/**
	 * Method updateFloorItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateFloorItems() {
		ArrayList<FloorItem> itemsInArea = entitiesInView[1];

		for (FloorItem i : itemsInArea) {
			if (withinRange(i) && !itemsInView.isRemoving(i) && !itemsInView.getKnownEntities().contains(i)
					&& (i.isGlobal() || i.isVisableTo(this))) {
				itemsInView.add(i);
				knownStackCounts.put(i, i.getAmount());
			}
		}
	}

	/**
	 * Method getKnownFloorItemStackCount Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 *
	 * @return
	 */
	public int getKnownFloorItemStackCount(FloorItem i) {
		return knownStackCounts.get(i);
	}

	/**
	 * Method getWatchedItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public StatefulEntityCollection<FloorItem> getWatchedItems() {
		return itemsInView;
	}

	/**
	 * Method updateWatchedPlayers Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateWatchedPlayers() {
		ArrayList<Player> playersInArea = entitiesInView[0];

		for (Player p : playersInArea) {
			if (!p.equals(this)) {
				if (withinRange(p)) {
					informOfPlayer(p);
					p.informOfPlayer(this);
				}
			}
		}
	}

	/**
	 * Method updateWatchedNpcs Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateWatchedNpcs() {
		ArrayList<NPC> playersInArea = entitiesInView[3];

		for (NPC p : playersInArea) {
			if (withinRange(p)) {
				informOfNPC(p);
			}
		}
	}

	private void informOfPlayer(Player p) {
		if (!p.isLoggedIn()) {
			return;
		}

		boolean b = false;

		if (watchedPlayers.isRemoving(p)) {
			if (!watchedPlayers.isRefreshing(p)) {
				watchedPlayers.refresh(p);
				b = true;
			}
		}

		/*
		 * if (b || (!watchedPlayers.contains(p) ||
		 * watchedPlayers.isRemoving(p)) && !watchedPlayers.isAdding(p) &&
		 * withinRange(p) && p.isLoggedIn() && !p.didTeleport && !p.ninjaMode) {
		 *
		 */
		if (b || (!watchedPlayers.contains(p) || watchedPlayers.isRemoving(p)) && !watchedPlayers.isAdding(p)
				&& !p.didTeleport() && withinRange(p) && p.isLoggedIn()) {
			watchedPlayers.add(p);
			knownAppearanceIds.put(p, -1);
			knownViewPointIds.put(p, -1);
			knownFacingIds.put(p, -1);
		}
	}

	private void informOfNPC(NPC n) {
		if ((!watchedNpcs.contains(n) || watchedNpcs.isRemoving(n)) && !watchedNpcs.isAdding(n) && withinRange(n)
				&& !n.didTeleport()) {
			watchedNpcs.add(n);
			knownNpcData.put(n.getIndex(), new NpcKnownData());
		}
	}

	/**
	 * Method getKnownViewpointId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 *
	 * @return
	 */
	public int getKnownViewpointId(Player pla) {
		if (knownViewPointIds.containsKey(pla)) {
			return knownViewPointIds.get(pla);
		} else {
			return -1;
		}
	}

	/**
	 * Method setKnownViewpointId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 * @param i
	 */
	public void setKnownViewpointId(Player pla, int i) {
		knownViewPointIds.put(pla, i);
	}

	/**
	 * Method getKnownNpcData Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npc
	 *
	 * @return
	 */
	public NpcKnownData getKnownNpcData(NPC npc) {
		return knownNpcData.get(npc.getIndex());
	}

	/**
	 * Method getKnownAppearanceId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 *
	 * @return
	 */
	public int getKnownAppearanceId(Player p) {
		if (knownAppearanceIds.containsKey(p)) {
			return knownAppearanceIds.get(p);
		} else {
			return -1;
		}
	}

	/**
	 * Method getKnownFacingID Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 *
	 * @return
	 */
	public int getKnownFacingID(Player p) {
		if (knownFacingIds.containsKey(p)) {
			return knownFacingIds.get(p);
		} else {
			return -1;
		}
	}

	/**
	 * Method setKnownFacingID Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 * @param i
	 *
	 * @return
	 */
	public int setKnownFacingID(Player p, int i) {
		knownFacingIds.put(p, i);

		return 0;
	}

	/**
	 * Method setKnownAppearanceId Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 * @param i
	 *
	 * @return
	 */
	public int setKnownAppearanceId(Player p, int i) {
		knownAppearanceIds.put(p, i);

		return 0;
	}

	/**
	 * Method setCombatLevel Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param c
	 */
	@Override
	public void setCombatLevel(int c) {
		super.setCombatLevel(c);
		getAppearance().setChanged(true);
	}

	/**
	 * Method getKnownLocalX Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getKnownLocalX() {
		return getX() - 8 * (getKnownRegion()[0]);
	}

	/**
	 * Method getKnownLocalY Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getKnownLocalY() {
		return getY() - 8 * (getKnownRegion()[1]);
	}

	/**
	 * Method setLocation Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 */
	@Override
	public void setLocation(Point p) {
		if (location != null) { // location could be null if we've just logged
								// in
			int[] knownMapArea = getKnownRegion();
			int[] newMapArea = { p.getRegionX(), p.getRegionY() };
			int[] differences = { Math.abs(newMapArea[0] - knownMapArea[0]),
					Math.abs(newMapArea[1] - knownMapArea[1]) };

			if ((differences[0] >= 3) || (differences[1] >= 3)) {
				setMapAreaChanged(true);
			}
		} else {

			// if it is null we definately need to send the map are packet
			setMapAreaChanged(true);
		}

		super.setLocation(p);
	}

	/**
	 * Method toString Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public String toString() {
		return "['" + username + "',idx=" + index + " " + ((getLocation() != null) ? getLocation() : "null") + "]";
	}

	/**
	 * Method equals Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param o
	 *
	 * @return
	 */
	public boolean equals(Object o) {
		if (o instanceof Player) {
			return ((Player) o).getUsernameHash() == this.getUsernameHash();
		}

		return false;
	}

	/**
	 * Method withinRange Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 * @param rad
	 *
	 * @return
	 */
	public boolean withinRange(Point p, int rad) {
		if (p == null) {
			return false;
		}

		if (p.getHeight() != location.getHeight()) {
			return false;
		}

		int dX = Math.abs(location.getX() - p.getX());
		int dY = Math.abs(location.getY() - p.getY());

		return (dX <= 15) && (dX >= -16) && (dY <= 15) && (dY >= -16);
	}

	/**
	 * Method withinRange Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param e
	 * @param rad
	 *
	 * @return
	 */
	public boolean withinRange(Entity e, int rad) {
		if (e == null) {
			return false;
		} else if ((e.getCurrentInstance() != getCurrentInstance())
				&& (e.getCurrentInstance() != Instance.GLOBAL_INSTANCE)) {
			return false;
		}

		if ((e instanceof Player) && ((Player) e).getPlayer().ninja_on) {
			return false;
		}

		if (!e.isVisible()) {
			return false;
		}

		return withinRange(e.getLocation(), rad);
	}

	/**
	 * Method withinRange Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param e
	 *
	 * @return
	 */
	public boolean withinRange(Entity e) {
		if ((e instanceof FloorItem) && ((FloorItem) e).getDropper() != this
				&& (account.getIronManMode() != Account.NO_IRONMAN)) {
			return false;
		}

		return withinRange(e, 15);
	}

	private void removeWatchedPlayer(Player p) {
		watchedPlayers.remove(p);
		knownAppearanceIds.remove(p);
		knownViewPointIds.remove(p);
		knownFacingIds.remove(p);
	}

	private void removeWatchedNpc(NPC p) {
		watchedNpcs.remove(p);
		knownNpcData.remove(p.getIndex());
	}

	/**
	 * Method getCurrentHealth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	@Override
	public double getCurrentHealth() {
		return Math.round(this.currentHealth * 10d) / 10d; // To change body of
															// implemented
															// methods use File
															// | Settings | File
															// Templates.
	}

	/**
	 * Method getMaxHealth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	@Override
	public double getMaxHealth() {
		return getMaxStat(3) + boostedHealth + equipment.getTorvaBonus()
				+ ((isTenth(3) && (getGameFrame().getDuel() == null)) ? 3 : 0); // To
																				// change
																				// body
																				// of
																				// implemented
																				// methods
																				// use
																				// File
																				// |
																				// Settings
																				// |
																				// File
																				// Templates.
	}

	/**
	 * Method addHealth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param health
	 */
	@Override
	public void addHealth(double health) {

		boolean dontCheck = false;
		if (currentHealth > getMaxHealth())
			return;

		this.currentHealth += health;

		if (currentHealth > getMaxHealth() && !dontCheck) {
			currentHealth = getMaxHealth();
		}

		// To change body of implemented methods use File | Settings | File
		// Templates.
		actionSender.sendStat(3);
	}

	/**
	 * Method removeHealth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param currentHealth
	 */
	@Override
	public void removeHealth(double currentHealth) {

		if (this.currentHealth == 0)
			return;

		this.currentHealth -= currentHealth;

		if (Math.ceil(this.currentHealth) <= 0.0) {
			this.currentHealth = 0.00;
		}

		getActionSender().sendStat(3);
	}

	/**
	 * Method onHitBy Created on 15/05/12
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param hitBy
	 * @param d
	 */
	@Override
	public void onHitBy(Mob hitBy, Damage d) {
		double damage = d.getDamage();

		if (hitBy == this) {
			return;
		}

		/*
		 * TODO: Test to make sure no negative side effects
		 */
		if (this.didTeleport()) {
			damage = 0;
		}

		terminateScripts();

		if ((d.getDamage() > 0) && getAccount().hasVar("v")
				&& ((d.getOrigType() != 8) && (d.getOrigType() != Damage.TYPE_OTHER))) {
			setTextUpdate("Taste vengeance!");

			final Killable k = (Killable) hitBy;
			final double h = k.hit(this, (d.getDamage() * 0.70), Damage.TYPE_OTHER, 0, true);

			getTickEvents().add(new PlayerTickEvent(this, true, 1) {
				@Override
				public void doTick(Player owner) {
					if ((k.getCurrentHealth() <= 0.0) || k.isDead()) {
						addAchievementProgress2(Achievement.VENGEANCE, 1);
						addAchievementProgress2(Achievement.VENGEANCE_II, 1);
						addAchievementProgress2(Achievement.VENGEANCE_III, 1);
						terminate();
					}
				}
			});

			getAccount().removeVar("v");
		}

		if ((getEquipment().getId(Equipment.SHIELD) == 20003) && (d.getDamage() > 0)) {
			int chance = 20;

			if (hitBy instanceof Player) {
				chance = 7;
			}

			if (GameMath.rand(100) < chance) {
				getGraphic().set(1896, Graphic.LOW);
				((Killable) hitBy).getGraphic().set(1897, Graphic.LOW);
				((Killable) hitBy).hit(this, (int) (d.getDamage() * 0.60), 8, 2);
				actionSender.sendMessage("Your dark energy shield recoils damage back to your opponent.");
			}
		}

		if ((getEquipment().getId(Equipment.RING) == 2550) && d.isNormalDamage() && (d.getDamage() > 0)) {
			int recoiled = (int) (d.getDamage() * 0.10);

			if (recoiled == 0) {
				recoiled = 1;
			}

			((Killable) hitBy).hit(this, recoiled, 8, 1);

			if (account.hasVar("recoil_amt")) {
				account.decreaseVar("recoil_amt", 1);

				if (account.getInt32("recoil_amt") == 0) {
					equipment.removeItem(Equipment.RING);
					equipment.updateEquipment(Equipment.RING);
					actionSender.sendMessage("Your ring of recoil has smashed.");
					account.removeVar("recoil_amt");
				} else {
					account.setSetting("recoil_amt", 100, true);
				}
			}
		}

		if ((getAccount().get(Account.AUTO_RETALIATE) == 0) && (getFollowing() == null)
				&& getPlayer().getRoute().isIdle()) {
			if (!getCombatAdapter().isInCombat()) {
				if (!route.isIdle()) {
					route.resetPath();
					actionSender.sendCloseWalkingFlag();
				}

				if (getCombatAdapter().getFightingWith() == null) {
					getCombatAdapter().delay(2);
				}

				getCombatAdapter().attackOn((Killable) hitBy);

				if (hitBy instanceof NPC) {
					getFocus().focus((NPC) hitBy);
					followNpc(((NPC) hitBy).getNpc());
				} else {
					getFocus().focus((Player) hitBy);
					followPlayer((Player) hitBy);
				}

				getFollowing().setMode(PlayerFollowing.COMBAT_BASED_FOLLOW);
			}
		}

		if (damage == 0) {
			int anim = 424;
			if (this.getEquipment().getWeapon() != null && this.getEquipment().getWeapon().isTwoHanded())
				anim = this.getEquipment().getWeapon().getBlockAnimation();
			else if ((this.getEquipment().getId(Equipment.SHIELD) >= 8844
					&& this.getEquipment().getId(Equipment.SHIELD) <= 8850)
					|| this.getEquipment().getId(Equipment.SHIELD) == 20072)
				anim = 4177;
			else if (this.getEquipment().isEquipped(Equipment.SHIELD))
				anim = 1156;
			else
				anim = 424;
			getAnimation().animate(anim, 0, Animation.LOW_PRIORITY);
		}

		if (d.isNormalDamage() && (damage > 0)) {
			if (hitBy instanceof Player) {
				Player pla = (Player) hitBy;

				if (pla.getPrayerBook().prayerActive(PrayerBook.SMITE)) {
					prayerBook.removePoints(damage / 4);
				}

				if (pla.getPrayerBook().curseActive(PrayerBook.SOUL_SPLIT)) {
					Projectile soul_split_projectile = new Projectile(pla, this, 2263, 30, 0);

					soul_split_projectile.setAttribute(1, 21);
					soul_split_projectile.setAttribute(2, 21);
					soul_split_projectile.setOnImpactEvent(new SoulsplitImpactEvent((int) damage));
					pla.registerProjectile(soul_split_projectile);
				}
			}
		}

		if (prayerBook.prayerActive(PrayerBook.REDEMPTION) && (getCurrentHealth() < (getMaxStat(3) / 10))
				&& (getCurrentHealth() > 0)) {
			addHealth(25);
			prayerBook.removePoints(99);
			prayerBook.resetAll();
			getGraphic().set(436, Graphic.HIGH);
		}

		getGameFrame().closeAll();

		if ((hitBy instanceof Player) && (Combat.getWildernessLevel((Player) hitBy) > -1)) {
			((Player) hitBy).getPlayer().getAccount()
					.setPvpDamageDealt(((Player) hitBy).getPlayer().getAccount().getPvpDamageDealt() + (int) damage);
			getAccount().setPvpDamageReceived(getAccount().getPvpDamageReceived() + (int) damage);
		}

		getWindowManager().closeWindow();
	}

	/**
	 * Method terminateActions Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void terminateActions() {
		for (PlayerTickEvent t : tickEvents) {
			t.requestTermination();
		}
		// 964

		if ((currentAction != null) && !isActionsDisabled()) {
			currentAction.terminate();
			currentAction = null;
		}
	}

	/**
	 * Method setCurrentHealth Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param health
	 */
	public void setCurrentHealth(double health) {
		this.currentHealth = health;
	}

	/**
	 * Method restore_all_stats Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void restore_all_stats() {
		for (int i = 0; i < 24; i++) {
			if ((getCurStat(i) < getMaxStat(i)) || (getCurStat(i) > getMaxStat(i))) {
				curStats[i] = getMaxStat(i);
				getActionSender().sendStat(i);
			}
		}
	}

	/**
	 * Method cancelDeaths Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void cancelDeaths() {
		respawn();

		for (Iterator<PlayerTickEvent> events = tickEvents.iterator(); events.hasNext();) {
			PlayerTickEvent t = events.next();

			if (t.getID() == PlayerTickEvent.TYPE_DEATH) {
				events.remove();

				continue;
			}
		}
	}

	/**
	 * Method nexLeave Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void nexLeave() {
	}

	/**
	 * Method entityKilled Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param killedBy
	 */
	@Override
	public void entityKilled(final Mob killedBy) {
		if (getCurrentFamiliar() != null) {
			resetSummon();
		}

		if (getCurDeathHandler() instanceof DefaultDeathHandler) {
			new DeathEvent(this);
		}

		if (killedBy instanceof NPC) {
			NPC npc = (NPC) killedBy;

			if ((npc.getSize() >= 3) || (npc.getId() == 8349)) {
				account.registerBossKill(npc.getId());
			}
		}

		getCombatAdapter().reset();
		this.getProjectiles().clear();
		getFocus().unFocus();
		terminateActions();
		getTimers().setTime(3, 0);

		if (getCurrentAction() instanceof AgilityWalkAction) {
			currentAction = null;
		}

		scriptsToProcess.clear();
		getTimers().setTime(TimingUtility.TELE_BLOCK_TIMER, 0);
		getRoute().resetPath();
		setPoisonTime(0);
		reset_curse_drain();
		prayerBook.reset_leeches();

		if (getCurDeathHandler() instanceof DefaultDeathHandler) {
		}

		log("Died at " + getLocation() + " killed by " + ((killedBy instanceof NPC)
				? "NPC " + ((NPC) killedBy).getNpc().getId() : "Player [" + ((Player) killedBy).toString()));
		log("Stack trace, for debugging purposes:");

		try {
			throw new RuntimeException();
		} catch (RuntimeException fake) {
			for (StackTraceElement element : fake.getStackTrace()) {
				log(element.getClassName() + " method: " + element.getMethodName() + "() line "
						+ element.getLineNumber());
			}
		}

		if (killedBy != null && prayerBook.curseActive(PrayerBook.WRATH)) {
			Killable k = (Killable) killedBy;
			prayerBook.wrath(k);
		}

		if ((killedBy != null) && prayerBook.prayerActive(PrayerBook.RETRIBUTION)) {
			Killable k = (Killable) killedBy;

			k.hit(this, GameMath.rand(10), Damage.TYPE_OTHER, 2);
			k.getGraphic().set(437, Graphic.HIGH);
		}

		if (killedBy instanceof Player) {
			((Player) killedBy).invalidateDamage();
		}

		getTickEvents().addLast(new PlayerTickEvent(this, 5, false, true, PlayerTickEvent.TYPE_DEATH) {
			@Override
			public void doTick(Player owner) {
				owner.getAnimation().animate(65535);
				owner.restore_all_stats();
				owner.setCurrentHealth(owner.getMaxHealth());
				owner.getActionSender().sendStat(3);
				prayerBook.setPoints(getCurStat(5));
				owner.respawn();
				owner.getActionSender().sendStat(5);

				Mob k = pollHighestHitter();

				if (k == null) {
					k = killedBy;
				}

				owner.getCurDeathHandler().handleDeath(owner, (Killable) k);
				prayerBook.resetAll();
				terminate();
			}
		});
		getAnimation().animate(2304);

		getAnimation().animate(836, Animation.HIGH_PRIORITY);

		// To change body of implemented methods use File | Settings | File
		// Templates.
	}

	private boolean darkbowSpecial;

	public void setDarkbowSpecial(boolean b) {
		this.darkbowSpecial = b;
	}

	public boolean isDarkbowSpecial() {
		return darkbowSpecial;
	}
}
