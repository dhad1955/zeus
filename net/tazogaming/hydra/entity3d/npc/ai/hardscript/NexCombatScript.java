// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 
// Source File Name:   NexCombatScript.java



package net.tazogaming.hydra.entity3d.npc.ai.hardscript;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.game.pets.BossPet;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.skill.combat.combat.*;
import net.tazogaming.hydra.entity3d.npc.*;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

//Referenced classes of package net.tazogaming.enchanta.entity3d.npc.ai.hardscript:
//           HardWiredScript

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class NexCombatScript extends HardWiredScript {
    public static HashMap       damage_counter       = new HashMap();
    private static final String RANDOM_DIALOG_TEXT[] = { "All hail Nex!", "Our saviour Nex!" };
    private static final int    NECROMANCER          = 0;
    private static final int    CURSE_BEARER         = 1;
    private static final int    FROST_WEB            = 2;
    private static final int    LEXICUS              = 3;
    private NPC                 gods[];
    int                         current_phase;
    private boolean             awaiting_god_kill;
    private DamageFilter        dmg_snagger;

    private int prayer = 0;

    private int NO_PRAYER = 0, PRAYER_SOULSPLIT = 1, PRAYER_MELEE = 2, PRAYER_WRATH = 3;

    /**
     * Constructs ...
     *
     *
     * @param npc
     */
    public NexCombatScript(NPC npc) {
        super(npc);
        gods              = new NPC[4];
        current_phase     = 0;
        awaiting_god_kill = false;
        dmg_snagger       = new DamageFilter() {
            public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
                if ((damage > 0) && (hitBy instanceof Player)) {
                    recordDamage((Player) hitBy, (int)damage);
                }
                return damage;
            }
            final NexCombatScript this$0;
            {
                this$0 = NexCombatScript.this;
            }
        }
        ;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {


        int random = (Core.currentTime % 10 != 0)
                     ? -1
                     : GameMath.rand3(RANDOM_DIALOG_TEXT.length);
        getMob().setTransform(13447 + prayer);
        if (gods != null) {
            NPC arr$[] = gods;
            int len$   = arr$.length;

            for (int i$ = 0; i$ < len$; i$++) {
                NPC n = arr$[i$];

                if (!n.getFocus().isFocused()) {
                    n.getFocus().focus(getMob());
                }

                if ((random == -1) || awaiting_god_kill) {
                    continue;
                }

                n.setTextUpdate(RANDOM_DIALOG_TEXT[random]);

                if (n.getId() == 10127) {
                    n.getAnimation().animate(13176);
                }

                if (n.getId() != 9855) {
                    n.getAnimation().animate(1670);
                }
            }
        }
    }

    /**
     * Method update_combat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean update_combat() {
        return awaiting_god_kill;
    }

    /**
     * Method notifyLeave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void notifyLeave(Player player) {
        if (damage_counter.containsKey(player)) {
            damage_counter.remove(player);
        }
    }

    /**
     * Method recordDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param dmg
     */
    public void recordDamage(Player player, int dmg) {

        if (damage_counter.containsKey(player)) {
            damage_counter.put(player, Integer.valueOf(((Integer) damage_counter.get(player)).intValue() + dmg));
        } else {
            damage_counter.put(player, Integer.valueOf(dmg));
        }
    }

    private void prepare() {
        damage_counter.clear();
        this.prayer = 0;
        getMob().addHealth(getMob().getMaxHealth());
        current_phase = 0;

        NPC arr$[] = gods;
        int len$   = arr$.length;

        for (int i$ = 0; i$ < len$; i$++) {
            NPC npc = arr$[i$];

            if (npc != null) {
                npc.unlink();
            }
        }

        int range = 11;

        if (!getMob().hasDamageFilter("snagger")) {
            getMob().addDamageFilter("snagger", dmg_snagger);
        }

        getMob().teleport(2924, 5202, 0);
        addGod(0, 10206, getMob().getX() + range, getMob().getY() + range);
        addGod(1, 10113, getMob().getX() + range, getMob().getY() - range);
        addGod(2, 10015, getMob().getX() - range, getMob().getY() + range);
        addGod(3, 9855, getMob().getX() - range, getMob().getY() - range);
        getMob().getControllerScript().injectVar("stage", 0);
        getMob().setDeathHandler(new NpcDeathHandler(new DeathPolicy[] { DeathPolicy.FAST_ACTION,
                DeathPolicy.DONT_DROP_LOOT }) {
            protected void onDeath(NPC killed, Killable killedBy) {
                next();
                killed.setDont_die(true);
            }
        });
    }

    /**
     * Method getPlayersThatHitAbove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     *
     * @return
     */
    public ArrayList getPlayersThatHitAbove(int amt) {
        ArrayList players = new ArrayList();
        Iterator  i$      = damage_counter.keySet().iterator();

        do {
            if (!i$.hasNext()) {
                break;
            }

            Player p = (Player) i$.next();

            if (((Integer) damage_counter.get(p)).intValue() >= amt) {
                players.add(p);
            }
        } while (true);

        return players;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {
        getMob().addHealth(500);
        awaiting_god_kill = true;
        gods[current_phase++].setBlock_interactions(false);

        String names[] = {
            "", "Necromancer", "Cursebearer", "Frostweb", "Lexicus", "", ""
        };

        if(current_phase == 2)
            this.prayer = PRAYER_MELEE;


        getMob().setTextUpdate((new StringBuilder()).append(names[current_phase]).append(" Dont fail me!").toString());

        if (current_phase == 4) {
            getMob().setDeathHandler(new NpcDeathHandler(DeathPolicy.RESPAWN) {
                protected void onDeath(NPC killed, Killable killedBy) {
                    ArrayList killers = getPlayersThatHitAbove(110);
                    Iterator  i$      = killers.iterator();

                    do {
                        if (!i$.hasNext()) {
                            break;
                        }

                        Player player = (Player) i$.next();

                        if(player.getAccount().bossBlocked(13447)){
                            player.getActionSender().sendMessage(Text.RED("You have died to this boss within the last 10 minutes, so you cant receive a drop"));
                            continue;
                        }
                        if(13447 == BossTournament.bossId) {
                            BossTournament.mainsLeaderboard.addPoint(player);
                        }
                        if (player != killedBy) {
                            Loot      loot[] = killed.getDefinition().getLoot().getLoot(1, player);
                            FloorItem i      = new FloorItem(loot[0].getItemID(), loot[0].getAmount(), killed.getX(),
                                                             killed.getY(), killed.getHeight(), player);

                            loot = killed.getDefinition().getLoot().getLootAlways();

                            Loot arr$[] = loot;
                            int  len$   = arr$.length;
                            int  i$0    = 0;
                            NPC npc = killed;
                            Player highestHitter = player;
                            BossPet pet = BossPets.getByBoss(npc.getId());
                            if(pet != null){
                                if(pet.isUnlocked(highestHitter)){
                                    new FloorItem(pet.getItemId(), 1, npc.getX(), npc.getY(), npc.getHeight(), highestHitter);
                                    BossPets.sendNotification(pet, highestHitter);
                                }
                            }
                            while (i$0 < len$) {
                                Loot k = arr$[i$0];

                                if(k.getItemID() > 20130)
                                    player.getAccount().setSetting("nex_drop", 1, true);
                                if (k.getRarity() == 4) {
                                    for (Player player2 : player.getViewArea().getPlayersInView()) {
                                        if (player2 != player) {
                                            player.getActionSender().sendMessage(player.getUsername()
                                                    + " has just got a rare drop, "
                                                    + Item.forId(k.getItemID()).getName() + " x" + k.getAmount());
                                        }
                                    }
                                }

                                if (k != null) {
                                    new FloorItem(k.getItemID(), k.getAmount(), killed.getX(), killed.getY(),
                                                  killed.getHeight(), player);
                                }

                                i$0++;
                            }
                        }
                    } while (true);



                }
            });
        }
    }

    private void addGod(int i, int id, int x, int y) {
        NpcAutoSpawn spawn = new NpcAutoSpawn();

        spawn.setSpawnHeight(getMob().getHeight());
        spawn.setSpawnX(x);
        spawn.setSpawnY(y);
        spawn.setRoamRange(20);
        gods[i] = new NPC(id, Point.location(x, y, getMob().getHeight()), spawn);
        gods[i].setBlock_interactions(true);
        gods[i].setDeathHandler(new NpcDeathHandler(new DeathPolicy[]{DeathPolicy.UNLINK_ON_DEATH}) {
            protected void onDeath(NPC killed, Killable killedBy) {
                godKilled(killed.getNpc());
            }
        });
        gods[i].addDamageFilter("snagger", dmg_snagger);

        gods[i].addDamageFilter("magefix", new DamageFilter() {
            @Override
            public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
                if(!awaiting_god_kill)
                    return 0;
                return damage;
            };
        });
        World.getWorld().registerNPC(gods[i]);
    }

    /**
     * Method on_spawn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void on_spawn() {
        try {
            prepare();
        } catch (Exception ee) {}
    }

    /**
     * Method godKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void godKilled(NPC npc) {
        for (int i = 0; i < gods.length; i++) {
            if (gods[i] == npc) {
                awaiting_god_kill = false;
                getMob().getControllerScript().injectVar("stage", current_phase);
                getMob().setTextUpdate("I will kill you my self!");
            }
        }
    }

    /**
     * Method on_attacked_by
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     *
     * @return
     */
    public boolean on_attacked_by(Killable k) {
        return false;
    }

    /**
     * Method update_movement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean update_movement() {
        return awaiting_god_kill;
    }

    /**
     * Method filter_damage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hit_by
     * @param d
     *
     * @return
     */
    public double filter_damage(Killable hit_by, Damage d) {
        if (current_phase == 5) {
            return d.getDamage();
        }
        if(prayer == PRAYER_MELEE && (d.getType() == Damage.TYPE_MELEE_CRUSH
                || d.getType() == Damage.TYPE_MELEE_SLASH
                || d.getType() == Damage.TYPE_MELEE_STAB))
                    return 0;

        Player pla;

        if (hit_by instanceof NPC) {
            pla = hit_by.getNpc().getSummoner();
        } else {
            pla = hit_by.getPlayer();
        }

        if (awaiting_god_kill) {
            pla.getActionSender().sendMessage("Nex blocks your attack, kill her guardian first.");

            return 0;
        } else {
            return d.getDamage();
        }
    }

    /**
     * Method on_area_clear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean on_area_clear() {
        prepare();

        return false;
    }

    /**
     * Method on_npc_dead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killedBy
     *
     * @return
     */
    public boolean on_npc_dead(Player killedBy) {
        NPC arr$[] = gods;
        int len$   = arr$.length;

        for (int i$ = 0; i$ < len$; i$++) {
            NPC npc = arr$[i$];

            if (npc != null) {
                npc.unlink();
            }
        }

        getMob().unlink();

        return false;
    }
}
