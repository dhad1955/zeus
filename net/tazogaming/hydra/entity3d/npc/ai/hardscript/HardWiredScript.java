package net.tazogaming.hydra.entity3d.npc.ai.hardscript;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/04/14
 * Time: 17:58
 */

//Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//Jad home page: http://www.kpdus.com/jad.html
//Decompiler options: packimports(3)
//Source File Name:   HardWiredScript.java
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.*;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public abstract class HardWiredScript {



    public static final DamageFilter COMBAT_SCRIPT_DMG_FILTER = new DamageFilter() {
        public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
            return myMob.getNpc().getCombatScript().filter_damage(hitBy, new Damage(damage, hitBy, hitType));
        }
    }
    ;
    public static final NpcDeathHandler COMBAT_SCRIPT_DEATH_HANDLER = new NpcDeathHandler() {
        protected void onDeath(NPC killed, Killable killedBy) {
            killed.getNpc().getCombatScript().on_npc_dead(killedBy.getPlayer());
        }
    }
    ;
    private NPC npc;

    /**
     * Constructs ...
     *
     *
     * @param npc
     */
    public HardWiredScript(NPC npc) {
        npc.addDamageFilter("script_dmg", COMBAT_SCRIPT_DMG_FILTER);
        npc.setDeathHandler(COMBAT_SCRIPT_DEATH_HANDLER);
        this.npc = npc;
    }

    /**
     * Method getMob
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC getMob() {
        return npc;
    }

    /**
     * Method update_combat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract boolean update_combat();

    /**
     * Method on_spawn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void on_spawn();

    /**
     * Method on_attacked_by
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killable
     *
     * @return
     */
    public abstract boolean on_attacked_by(Killable killable);

    /**
     * Method update_movement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract boolean update_movement();

    /**
     * Method filter_damage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killable
     * @param damage
     *
     * @return
     */
    public abstract double filter_damage(Killable killable, Damage damage);

    /**
     * Method on_area_clear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract boolean on_area_clear();

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void tick();

    /**
     * Method on_npc_dead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public abstract boolean on_npc_dead(Player player);
}
