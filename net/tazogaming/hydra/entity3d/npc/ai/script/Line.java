package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 21:07
 */
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.instr.Condition;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/10/13
 * Time: 12:32
 */
public class Line {
    private int    lineNumber    = 0;
    private int    currentIndex  = 0;
    private int    statementType = 0;
    private int    jsMode        = -1;
    private String lineData;
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param lineData
     * @param no
     */
    public Line(String lineData, int no) {
        this(lineData, no, -1);
    }

    /**
     * Constructs ...
     *
     *
     * @param lineData
     * @param no
     * @param jsmode
     */
    public Line(String lineData, int no, int jsmode) {
        this.lineData = lineData.trim();
        this.jsMode   = jsmode;

        if (this.lineData.charAt(0) == '+') {
            this.statementType = RuntimeInstruction.TRUE;
        } else if (this.lineData.charAt(0) == '-') {
            this.statementType = RuntimeInstruction.FALSE;
        } else {
            this.statementType = RuntimeInstruction.NONE;
        }

        if (this.statementType != RuntimeInstruction.NONE) {
            next();
        }

        this.lineNumber = no;
    }

    /**
     * Method setController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     *
     * @return
     */
    public Line setPlayer(Player p) {
        this.player = p;

        return this;
    }

    /**
     * Method skip
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param caret
     */
    public void skip(int caret) {
        this.currentIndex += caret;
    }

    private char next() {
        if (currentIndex == lineData.length()) {
            throw new IndexOutOfBoundsException("Error no more chars");
        }

        return lineData.charAt(currentIndex++);
    }

    private boolean hasNext() {
        return currentIndex < lineData.length();
    }

    private char peek() {
        return lineData.charAt(currentIndex);
    }

    private String[] getNextParams() {
        String[]      params       = new String[20];
        int           offset       = 0;
        boolean       withinString = false;
        boolean       reachedEnd   = false;
        StringBuilder curString    = new StringBuilder();

        if (peek() == ')') {
            return null;    // no params
        }

        try {
            while (hasNext() &&!reachedEnd) {
                char c = next();

                if ((c == ')') &&!withinString) {
                    throw new RuntimeException(("Error: " + curString + " " + lineData.charAt(currentIndex) + " ("
                                                + lineData + ")"));
                }

                if (c == '"') {
                    if (withinString) {
                        params[offset++] = curString.toString();
                        curString        = new StringBuilder();
                        withinString     = false;

                        if (peek() == ')') {
                            reachedEnd = true;
                        }
                    } else {
                        withinString = true;
                    }
                } else if (withinString) {
                    curString.append(c);
                } else {
                    if (Character.isLetterOrDigit(c) || (c == '>') || (c == '<' | (c == '%'))
                            || ((c == '.') || (c == '_') || (c == '-'))) {
                        curString.append(c);
                    }

                    if ((curString.length() > 0) && ((c == ',') || (peek() == ')'))) {
                        params[offset++] = curString.toString();
                        curString        = new StringBuilder();

                        if (peek() == ')') {
                            reachedEnd = true;
                        }
                    }
                }
            }

            if (!reachedEnd) {
                throw new RuntimeException("Npc script exception " + lineData + " " + lineNumber);
            }

            String[] ret = new String[offset];

            System.arraycopy(params, 0, ret, 0, offset);

            return ret;
        } catch (Exception ee) {
            ee.printStackTrace();

            if (player != null) {
                player.getActionSender().sendMessage("@red@parse error: " + lineData + "");
            }

            throw new RuntimeException("error");
        }
    }

    /**
     * Method toNpcCall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NpcCall toNpcCall() {
        NpcCall       call          = null;
        StringBuilder currFunct     = new StringBuilder();
        int           conditionType = -1;
        boolean       not           = false;

        if (lineData.equalsIgnoreCase("PLAYER_CALL")) {
            return new NpcCall("PLAYER_CALL", null);
        }

        while (hasNext()) {
            char c = next();

            if (Character.isLetter(c) || (c == '_') || (c == '+') || (c == '-')) {
                currFunct.append(c);
            } else if (c == '(') {
                String[] params = getNextParams();

                if (!hasNext() || (next() != ')')) {
                    throw new RuntimeException("error");
                }

                if (call == null) {
                    call = new NpcCall(currFunct.toString(), params);
                }

                call.setType(statementType);

                if (currFunct.toString().startsWith("if")) {
                    call.insertHead(new Condition(conditionType, not, currFunct.toString(), call.parseParams(params)));
                } else if (conditionType != -1) {
                    call.insertHead(new Condition(conditionType, not, currFunct.toString(), call.parseParams(params)));
                }

                currFunct     = new StringBuilder();
                conditionType = -1;
                not           = false;
            } else if (c == '!') {
                not = true;
            } else if (c == '|') {
                conditionType = Condition.TYPE_OR;
            } else if (c == '&') {
                conditionType = Condition.TYPE_AND;
            } else if (c == '~') {
                String str = "";

                while (hasNext()) {
                    str += next();
                }

                // RuntimeInstruction.setDeclareAs(str);
            }
        }

        return call;
    }

    /**
     * Method checkRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String checkRemaining() {
        return lineData.substring(this.currentIndex, lineData.length());
    }
}
