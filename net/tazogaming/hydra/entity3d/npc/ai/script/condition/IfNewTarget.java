package net.tazogaming.hydra.entity3d.npc.ai.script.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.FollowPlayerMovement;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 11:56
 */
public class IfNewTarget implements ScriptCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(NPC npc, ParameterParser parser, NpcScriptScope script) {
        if (npc.getSpawnData() == null) {
            return false;
        }

        int range = npc.getSpawnData().getRoamRange();

        if (parser.hasNext()) {
            range = parser.nextInt();
        }

        if (npc.getCaves() != null) {
            npc.setMovement(new FollowPlayerMovement(npc, npc.getCaves().getPlayer(), true));
            script.setInteractingPlayer(npc.getCaves().getPlayer());

            return true;
        }

        for (Player pla : npc.getPlayersInRange(range)) {
            if (pla == null) {
                break;
            }

            if (pla.getCurrentInstance() != npc.getCurrentInstance()) {
                continue;
            }

            if (!pla.canBeAttacked(npc) || pla.isDead() ||!pla.withinRange(npc)
                    || ((npc.getQuest_controller() != null) && (npc.getQuest_controller() != pla))) {
                continue;
            }

            if (!npc.canGo(pla.getLocation(), npc.getCombatHandler().getCombatDistance())) {
                continue;
            }

            npc.getCombatHandler().reset();

            if (npc.getMovement() instanceof FollowPlayerMovement) {
                FollowPlayerMovement h = (FollowPlayerMovement) npc.getMovement();

                if (h.getFollowing() != pla) {
                    npc.setMovement(new FollowPlayerMovement(npc, pla, true));
                    script.setInteractingPlayer(pla);

                    return true;
                }
            } else {
                npc.setMovement(new FollowPlayerMovement(npc, pla, true));
                script.setInteractingPlayer(pla);

                return true;
            }
        }

        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
