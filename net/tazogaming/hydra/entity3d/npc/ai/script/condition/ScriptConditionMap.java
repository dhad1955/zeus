package net.tazogaming.hydra.entity3d.npc.ai.script.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.ai.script.condition.pc.IfVoidKnight;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.zombies.IfNewZombieTarget;
import net.tazogaming.hydra.script.runtime.instr.condition.IfFamiliar;
import net.tazogaming.hydra.script.runtime.instr.condition.IfNpcNear;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 12:05
 */
public class ScriptConditionMap {
    private static HashMap<String, ScriptCondition> conditions = new HashMap<String, ScriptCondition>();

    static {
        addCondition("if_combat_time", new IfCombatTime());
        addCondition("if_new_target", new IfNewTarget());
        addCondition("if_time", new IfTime());
        addCondition("if_can_shoot", new IfPathClear());
        addCondition("if_var", new IfVar());

        addCondition("if_var_above", new IfVarAbove());
        addCondition("ifrandom", new IfRandom());
        addCondition("ifisfightingwith", new IfIsFightingWith());
        addCondition("if_damage_type", new IfDamageType());
        addCondition("if_damage_above", new IfDamageAbove());
        addCondition("if_health_below", new IfHealthBelow());
        addCondition("if_spec_points_atleast", new IfSummonPtsAtleast());
        addCondition("if_in_combat", new IfIsInCombatWith());
        addCondition("iffamiliar", new IfFamiliar());
        addCondition("if_distance", new IfDistance());
        addCondition("if_npc_near", new IfNpcNear());

        // zombie conditions
        addCondition("if_new_zombie_target", new IfNewZombieTarget());
        addCondition("if_new_boss_target", new IfNewBossTarget());
        addCondition("if_damage_lock", new IfDamageReduce());
        addCondition("if_idle", new IfIdle());
        addCondition("if_void_knight", new IfVoidKnight());

    }

    /**
     * Method addCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param condition1
     * @param condition
     */
    public static void addCondition(String condition1, ScriptCondition condition) {
        conditions.put(condition1, condition);
    }

    /**
     * Method getCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public static ScriptCondition getCondition(String str) {
        return conditions.get(str);
    }
}
