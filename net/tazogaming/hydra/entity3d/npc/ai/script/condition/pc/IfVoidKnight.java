package net.tazogaming.hydra.entity3d.npc.ai.script.condition.pc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptCondition;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 05/01/14
 * Time: 02:07
 */
public class IfVoidKnight implements ScriptCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(NPC npc, ParameterParser parser, NpcScriptScope script) {
        NPC voidKnight = npc.getPCGame().getVoid_knight();
        int range      = parser.nextInt();

        if (voidKnight == null) {
            return false;
        }

        if (npc.isWithinDistance(voidKnight, range)) {
            script.setInteractingNPC(0, voidKnight);

            return true;
        }

        return false;

        // return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
