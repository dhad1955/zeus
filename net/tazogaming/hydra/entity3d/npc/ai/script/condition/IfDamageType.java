package net.tazogaming.hydra.entity3d.npc.ai.script.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 06:17
 */
public class IfDamageType implements ScriptCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(NPC npc, ParameterParser parser, NpcScriptScope script) {
        Damage d     = script.getCurrentDamage();
        int    check = parser.nextInt();

        if (check == 55) {
            return (d.getType() == Damage.TYPE_RANGE) || (d.getType() == Damage.TYPE_MAGIC);
        }

        if (check == 50) {
            return (d.getType() == Damage.TYPE_MELEE_CRUSH) || (d.getType() == Damage.TYPE_MELEE_SLASH)
                   || (d.getType() == Damage.TYPE_MELEE_STAB);
        }

        return d.getType() == check;
    }
}
