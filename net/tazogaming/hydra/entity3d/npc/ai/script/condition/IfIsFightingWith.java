package net.tazogaming.hydra.entity3d.npc.ai.script.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/10/13
 * Time: 03:15
 */
public class IfIsFightingWith implements ScriptCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(NPC npc, ParameterParser parser, NpcScriptScope script) {
        return (npc.getCombatHandler().getFightingWith() == script.getInteractingPlayer())
               || (npc.getCombatHandler().getFightingWith() == script.getInteractingNpc());
    }
}
