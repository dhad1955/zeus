package net.tazogaming.hydra.entity3d.npc.ai.script.condition.zombies;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptCondition;
import net.tazogaming.hydra.game.minigame.zombiesx.Zombies;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 12:51
 */
public class IfNewZombieTarget implements ScriptCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(NPC npc, ParameterParser parser, NpcScriptScope script) {
        Zombies zombieGame = npc.getZombieGame();

        if (zombieGame == null) {
            return false;
        }

        Player lowest = zombieGame.getEasiestPlayer();

        if (lowest == script.getInteractingPlayer()) {
            return false;
        }

        if (lowest != null) {
            script.setInteractingPlayer(lowest);

            return true;
        }

        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
