package net.tazogaming.hydra.entity3d.npc.ai.script.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/11/13
 * Time: 09:35
 */
public class IfDamageReduce implements ScriptCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(NPC npc, ParameterParser parser, NpcScriptScope script) {
        double cur_damage  = script.getCurrentDamage().getDamage();
        int percentage  = parser.nextInt();
        double currentPerc = GameMath.getPercentFromTotal((int)npc.getCurrentHealth(), (int)npc.getMaxHealth());

        if (currentPerc < percentage) {
            return false;
        }

        double calcPerc = GameMath.getPercentFromTotal(npc.getCurrentHealth() - cur_damage, npc.getMaxHealth());

        if (calcPerc > percentage) {
            return false;
        }

        int wanted_perc = (int) (npc.getMaxHealth() * (percentage / 100d));
       double calculated  = npc.getCurrentHealth() - cur_damage;

        script.setReturnDamage((int)(wanted_perc - calculated));

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
