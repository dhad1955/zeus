package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.runtime.Core;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/10/13
 * Time: 16:31
 */
public class ScriptHandler {
    private HashMap<String, Byte>     variables      = new HashMap<String, Byte>();
    private ArrayList<NpcScriptScope> delayedScripts = new ArrayList<NpcScriptScope>();
    private int                         hardPause      = 0;
    private int[]                       currentTimers  = new int[20];
    private NPC[]                       slaves         = new NPC[10];
    private int[]                       currentVars    = new int[20];
    private NPC                         npc;
    private ControllerScript            script;
    private Player                      summon_master;

    /**
     * Constructs ...
     *
     *
     * @param npc
     */
    public ScriptHandler(NPC npc) {
        this.npc = npc;
    }

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param master
     */
    public ScriptHandler(NPC npc, Player master) {
        this.npc           = npc;
        this.summon_master = master;
    }

    /**
     * Method injectVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param value
     */
    public void injectVar(String name, int value) {
        if (getController() == null) {
            return;
        }

        int index = getController().getVarIndex(name);

        if (index == -1) {
            throw new RuntimeException("Var not found: " + name);
        }

        this.currentVars[index] = value;
    }

    /**
     * Method hasScripts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasScripts() {
        return script != null;
    }

    /**
     * Method updateCombatScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public boolean updateCombatScript(NPC npc) {
        if (this.script == null) {
            return false;
        }

        Block block = this.script.getCombatScript();

        if (npc.isSummoned()) {
            block = this.script.getSummonCombatScript();
        }

        if (block == null) {
            return false;
        }

        if (isDelayed(block)) {
            return false;
        }

        if (isPaused()) {
            return false;
        }

        return true;
    }

    /**
     * Method updateScripts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateScripts() {
        if (npc.isDead()) {
            return;
        }

        if ((this.getController() != null) && (this.getController().getTick_script() != null)) {
            NPCScriptEngine.evaluate(npc, new NpcScriptScope(npc, this.getController().getTick_script()));
        }

        if (isPaused()) {
            return;
        }

        for (int i = 0; i < delayedScripts.size(); i++) {
            NpcScriptScope script = delayedScripts.get(i);

            if (!script.isDelayed()) {
                NPCScriptEngine.evaluate(npc, script);
                delayedScripts.remove(i);
            }
        }
    }

    /**
     * Method do_trigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param interacting
     */
    public void do_trigger(int type, Mob interacting) {
        if (getController() == null) {
            return;
        }

        Block block = this.getController().getTrigger(type);

        if (block == null) {
            return;
        }

        NpcScriptScope script;

        if (interacting instanceof Player) {
            script = new NpcScriptScope(npc, block, ((Player) interacting).getPlayer());
        } else {
            script = new NpcScriptScope(npc, block);
            script.setInteractingNpc((NPC) interacting);
        }

        NPCScriptEngine.evaluate(npc, script);
    }

    /**
     * Method resetAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetAll() {
        this.delayedScripts.clear();
    }

    /**
     * Method delay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param script
     * @param ticks
     */
    public void delay(NpcScriptScope script, int ticks) {
        script.delay(ticks);
        delayedScripts.add(script);
    }

    private boolean isDelayed(Block block) {
        for (NpcScriptScope script : delayedScripts) {
            if (script.isDelayed() && script.is(block)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method setController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param script
     */
    public void setController(ControllerScript script) {
        this.script = script;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ControllerScript getController() {
        return script;
    }

    /**
     * Method isPaused
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isPaused() {
        return Core.currentTime < hardPause;
    }

    /**
     * Method hardPause
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     */
    public void hardPause(int ticks) {
        this.hardPause = Core.currentTime + ticks;
    }

    /**
     * Method getCurrentTimers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getCurrentTimers() {
        return currentTimers;
    }

    /**
     * Method setCurrentTimers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTimers
     */
    public void setCurrentTimers(int[] currentTimers) {
        this.currentTimers = currentTimers;
    }

    /**
     * Method setVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param value
     */
    public void setVar(int id, int value) {
        this.currentVars[id] = value;
    }

    /**
     * Method changeVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param value
     */
    public void changeVar(int id, int value) {
        currentVars[id] += value;
    }

    /**
     * Method getCurrentVars
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getCurrentVars() {
        return currentVars;
    }

    /**
     * Method setCurrentVars
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentVars
     */
    public void setCurrentVars(int[] currentVars) {
        this.currentVars = currentVars;
    }

    /**
     * Method setSlave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     * @param npc
     */
    public void setSlave(int index, NPC npc) {
        slaves[index] = npc;
    }

    /**
     * Method getSlaves
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC[] getSlaves() {
        return slaves;
    }

    /**
     * Method setSlaves
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slaves
     */
    public void setSlaves(NPC[] slaves) {
        this.slaves = slaves;
    }
}
