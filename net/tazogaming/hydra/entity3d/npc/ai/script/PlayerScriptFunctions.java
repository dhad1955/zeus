package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.game.minigame.pc.PCGame;
import net.tazogaming.hydra.game.minigame.pkraffle.PKRaffle;
import net.tazogaming.hydra.game.PKHighscores;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/11/13
 * Time: 23:19
 */
public class PlayerScriptFunctions {
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    public PlayerScriptFunctions(Player player) {
        this.player = player;
    }

    /**
     * Method buyHouse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void buyHouse(Player player) {}

    /**
     * Method lowerStat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param statId
     * @param desiredLevel
     */
    public void lowerStat(int statId, int desiredLevel) {
        int   xp_for_lvl = GameMath.getXPForLevel(desiredLevel);
        int[] xps        = player.getExps();

        xps[statId] = xp_for_lvl;

        int[] curstats = player.getCurStats();

        curstats[statId] = GameMath.getLevelForXP(xps[statId], statId);
        player.setCurStats(curstats);
        player.setExps(xps);
        player.getActionSender().sendStat(statId);
    }

    public void openBuyBack() {
        player.getGameFrame().openShop(player.getBuyBackShop());
    }

    /**
     * Method changeGroup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rights
     * @param usergroupID
     */
    public void changeGroup(int rights, int usergroupID) {
        if (usergroupID == UserAccount.GROUP_ADMIN) {
            return;
        }

        World.getWorld().getDatabaseLink().query("UPDATE `user` set usergroup='" + usergroupID
                + "' where id='" + player.getId() + "'");
        player.setRights(rights);
    }

    /**
     * Method openScores
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void openScores() {
        PKHighscores.render(player);
    }

    /**
     * Method getRafflePoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRafflePoints() {
        return PKRaffle.getEntries(player).size();
    }

    /**
     * Method raffleRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int raffleRemaining() {
        return PKRaffle.remaining();
    }

    /**
     * Method goHouse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buildingMode
     */
    public void goHouse(boolean buildingMode) {
        if ((player.getCurrentHouse() != null) && (player.getCurrentHouse().getPlayer() == player)) {
            player.getCurrentHouse().destruct();
        }

        player.is_con = false;
        player.teleport(400, 400, 1);
        player.setCurrentHouse(null);
        player.getActionSender().sendMapArea();
        new House(player, buildingMode);
        player.teleport(player.getX(), player.getY(), 1);
    }

    /**
     * Method enter_pc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void enter_pc() {
        PCGame.lobby.addPlayer(player);
    }

    /**
     * Method joinHouse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param username
     */
    public void joinHouse(String username) {
        Player plr = World.getWorld().getPlayer(Text.stringToLong(username));

        if (plr == null) {
            Dialog.printStopMessage(player, "That user isn't online.");

            return;
        }

        if (plr.getCurrentHouse() == null) {
            Dialog.printStopMessage(player, "This person isn't home at the moment.");

            return;
        }

        if (plr.getAccount().get(444) == 0) {
            Dialog.printStopMessage(player, "This persons house is locked");

            return;
        }

        if (plr.getCurrentHouse().isBuildingMode()) {
            Dialog.printStopMessage(player, "This person is in building mode.");

            return;
        }

        plr.getCurrentHouse().addPlayer(player);
    }

    /**
     * Fetches the current (boosted or non boosted) stat of the specified player
     * @param stat the skill index
     * @return the current stat
     */
    public int getCurrentStat(int stat) {
        return player.getCurStat(stat);
    }

    /**
     * Fetches the players static level for the stat specified
     *  @param stat the skill index
     * @return the current stat
     */
    public int getMaxStat(int stat) {
        return player.getMaxStat(stat);
    }

    /**
     * get the current points (minigame points or so)
     * @param index the points index, see docs for more info
     * @return the number of points
     */
    public int getPoints(int index) {
        return player.getPoints(index);
    }

    /**
     * Gets the current familiars name
     * @return the familiars name, will return "null" if no familiar is active
     */
    public String getFamiliarName() {
        if (player.getCurrentFamiliar() == null) {
            return "null";
        }

        return player.getCurrentFamiliar().getDefinition().getName();
    }

    /**
     * Gets the players username
     * @return the players username
     */
    public String getUsername() {
        return player.getUsername();
    }

    /**
     * Get the players absolute X
     * @return the players absolute X coordinate
     */
    public int getX() {
        return player.getX();
    }

    /**
     * Get the players absolute Y
     * @return the players absolute Y coordinate
     */
    public int getY() {
        return player.getY();
    }

    /**
     * Get the players current plane level
     * @return the players height
     */
    public int getHeight() {
        return player.getHeight();
    }

    /**
     * Returns the players current health
     * @return the players health
     */
    public double getCurrentHealth() {
        return player.getCurrentHealth();
    }

    /**
     * Checks if a player has a prayer active
     * @param prayerId the prayer index to check, see docs for more info
     * @return 1 if the prayer is active, 0 if its not
     */
    public int isPrayerActive(int prayerId) {
        return player.getPrayerBook().prayerActive(prayerId)
               ? 1
               : 0;
    }

    /**
     * Method getItemCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public int getItemCount(int item) {
        int tot = 0;

        for (int i = 0; i < player.getInventory().getItemsCount().length; i++) {
            if ((player.getInventory().getItem(i) != null) && (player.getInventory().getItem(i).getIndex() == item)) {
                tot += player.getInventory().getCount(i);
            }
        }

        return tot;
    }

    /**
     * Method getGroup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGroup() {
        return player.getRights();
    }

    /**
     * Gets a name for an interfaces
     * @param item
     * @return the interfaces name
     */
    public String getItemName(int item) {
        return Item.forId(item).getName();
    }

    /**
     * Gets the price for an interfaces
     * @param item
     * @return the items price
     */
    public int getItemPrice(int item) {
        return Item.forId(item).getPrice();
    }

    /**
     * Gets the name for players equipment
     * @param slot the slot of the equipment
     * @return the name of the equipment
     *
     */
    public String getEquipmentName(int slot) {
        if (!player.getEquipment().isEquipped(slot)) {
            return "null";
        }

        return getItemName(player.getEquipment().getId(slot));
    }

    /**
     * Gets the players total level
     * @return the players total accumulated level
     */
    public int getTotalLevel() {
        int c = 0;

        for (int i = 0; i < player.getCurStats().length; i++) {
            c += player.getMaxStat(i);
        }

        return c;
    }

    /**
     * Get the players combat level
     * @return the players combat level
     */
    public int getCombatLevel() {
        return player.getCombatLevel();
    }

    /**
     * Get the npc's anme
     * @param id the npc id
     * @return the npc's name from the definition database
     */
    public String getNpcName(int id) {
        return (NpcDef.FOR_ID(id) == null)
               ? "null"
               : NpcDef.FOR_ID(id).getName();
    }

    /**
     * get the players current prestige level
     * @param level the chosen skill
     * @return the prestige level
     */
    public int getPrestige(int level) {
        return player.getPrestige(level);
    }

    /**
     * Fetch the players prayer points
     * @return the players prayer points
     */
    public int getPrayerPoints() {
        return player.getPrayerBook().getPoints();
    }

    /**
     * Get the players rank
     * @return
     */
    public int getRank() {
        return player.getRank();
    }

    /**
     * get the name of the current slayer task
     * @return the npc name
     */
    public String getSlayerName() {
        if(player.hasSlayerTask()) return NpcDef.FOR_ID(player.getSlayerTask().getTaskId()).getName();
        return "Undefined";
    }

    /**
     * return the npc id of the current slayer task
     * @return the npc id
     */
    public int getSlayerKillID() {
        return player.hasSlayerTask() ? player.getSlayerTask().getTaskId() : -1;
    }

    /**
     * Get the total number of slayer kills for the current task
     * @return the number of kills
     *
     */
    public int getSlayerKillTotal() {
        return player.hasSlayerTask() ? player.getSlayerTask().getTotalKills() : -1;
    }

    /**
     * Return the task amount
     * @return
     */
    public int getSlayerKillMax() {
            return player.hasSlayerTask() ? player.getSlayerTask().getKillsToComplete() : -1;

    }

    /**
     * get current enrgy
     * @return the players current energy
     */
    public int getEnergy() {
        return (int) player.getCurrentEnergy();
    }

    /**
     * Return current task percentage
     *
     * @return
     */
}
