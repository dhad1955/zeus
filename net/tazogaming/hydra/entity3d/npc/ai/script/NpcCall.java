package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.IfVar;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.IfVarAbove;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptCondition;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptConditionMap;
import net.tazogaming.hydra.script.runtime.instr.Condition;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 08:42
 */
public class NpcCall {
    public static final int
        ROAM                                   = 0,
        RETREAT                                = 1,
        PURSUE                                 = 2,
        COOLDOWN                               = 3,
        PAUSE                                  = 4,
        MELEE_ATTACK                           = 5,
        DEFAULT_ANIM                           = 7,
        PLAY_ANIM                              = 8,
        GRAPHIC                                = 9,
        FACE_TO                                = 10,
        SAY_WORD                               = 11,
        SET_COMBAT_DISTANCE                    = 12,
        SHOOT_TARGET                           = 13,
        EXEC_PLAYER                            = 14,
        HEAL_HEALTH                            = 15,
        PLACE_OBJ                              = 16,
        PLACE_ITEM                             = 17,
        STILL_GRAPHIC                          = 18,
        UNLINK                                 = 19,
        EXEC                                   = 20,
        JUMP                                   = 21,
        EARTH_QUAKE                            = 22,
        ATT_DEFAULT                            = 23,
        DIE                                    = 24,
        FORK                                   = 25,
        SET_VAR                                = 26,
        SUB_VAR                                = 27,
        ADD_VAR                                = 28,
        RESET_VARS                             = 29,
        DELAY_UPDATES                          = 30,
        BLOCK_INTERACTION                      = 31,
        RANDOM                                 = 32,
        DONT_DIE                               = 33,
        NEW_NPC                                = 44,
        POISON_TARGET                          = 45,
        APPLY_DAMAGE                           = 46,
        REDUCE_DAMAGE                          = 47,
        RECOIL_DAMAGE                          = 48,
        FILTER_DAMAGE                          = 49,
        SUMMON_SHOOT_TARGET                    = 50,
        SUMMON_APPLY_DAMAGE                    = 51,
        APPLY_DAMAGE_RAN                       = 52,
        ACTIVATE_SCROLL                        = 53,
        FORAGE_INV_ADD                         = 54,
        SUMMON_DEFAULT_ATTACK                  = 55,
        APPLY_DAMAGE_WITHIN                    = 56,
        SHOOT_COMPLICATED_PROJECTILE           = 57,
        PATH_PURSUE                            = 58,
        SUMMON_PURSUE                          = 59,
        TELEPORT_TO_TARGET                     = 60,
        LOOP_PLAYERS                           = 70,
        ADD_SLAVE                              = 71,
        CHANGE_MODEL                           = 81,
        TELEPORT                               = 82,
        SET_PROJECTILE_CLIPPING                = 83,
        MOVE_RAND                              = 84,
        SET_FLYING                             = 85;
    private ArrayList<Condition> conditions    = new ArrayList<Condition>();
    private int                  interactingId = -1;
    private boolean              isLoopObject  = false;
    private Parameter[]          params;
    private String               callName;
    private int                  opcode;
    private ScriptCondition      condition;
    private int                  type;
    private String               line;

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param params
     */

    public NpcCall(){
        this.opcode = NpcCall.EXEC_PLAYER;
    }
    public NpcCall(String name, String... params) {
        if (name.startsWith("*")) {
            isLoopObject = true;
            name         = name.substring(1);
        }

        if (name.startsWith("+") || name.startsWith("-")) {
            if (name.startsWith("+")) {
                type = RuntimeInstruction.TRUE;
            } else {
                type = RuntimeInstruction.FALSE;
            }

            name = name.substring(1);
        }

        this.opcode = getOpcodeForName(name);

        if (params != null) {
            this.params = new Parameter[params.length];

            for (int i = 0; i < params.length; i++) {
                this.params[i] = new Parameter();

                if (isString(params[i])) {
                    this.params[i].setType(Parameter.TYPE_STRING);
                    this.params[i].setStringData(params[i]);
                } else {
                    this.params[i].setType(Parameter.TYPE_INT);
                    this.params[i].setData(Integer.parseInt(params[i]));
                }
            }
        }

        this.callName = name;
    }

    /**
     * Method isLoopObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isLoopObject() {
        return isLoopObject;
    }

    /**
     * Method getInteractingId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getInteractingId() {
        return interactingId;
    }

    /**
     * Method setInteractingID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setInteractingID(int id) {
        this.interactingId = id;
    }

    /**
     * Method getLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getLine() {
        return line;
    }

    /**
     * Method setLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     */
    public void setLine(String line) {
        this.line = line;
    }

    /**
     * Method parseParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param params
     *
     * @return
     */
    public static Parameter[] parseParams(String[] params) {
        if (params == null) {
            return null;
        }

        Parameter[] return_params = new Parameter[params.length];

        if (params != null) {
            return_params = new Parameter[params.length];

            for (int i = 0; i < params.length; i++) {
                return_params[i] = new Parameter();

                if (isString(params[i])) {
                    return_params[i].setType(Parameter.TYPE_STRING);
                    return_params[i].setStringData(params[i]);
                } else {
                    return_params[i].setType(Parameter.TYPE_INT);
                    return_params[i].setData(Integer.parseInt(params[i]));
                }
            }
        }

        return return_params;
    }

    /**
     * Method containsCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public boolean containsCondition(String str) {
        if ((conditions == null) || (conditions.size() == 0)) {
            return false;
        }

        int i = 0;

        for (Condition cond : conditions) {
            try {}
            catch (Exception ee) {}

            i++;

            if ((cond.getCondition() instanceof IfVar) || (cond.getCondition() instanceof IfVarAbove)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getConditions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Condition> getConditions() {
        return conditions;
    }

    /**
     * Method getConditionReturnValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param script
     *
     * @return
     */
    public boolean getConditionReturnValue(NPC pla, NpcScriptScope script) {
        boolean returnValue = false;
        boolean last        = false;

        for (Condition c : conditions) {
            boolean read = c.getReturnValue(pla, script);

            if (c.isNot()) {
                read = !read;    // = !returnValue;
            }

            if (c.getType() == Condition.TYPE_OR) {
                returnValue |= read;
            } else if (c.getType() == Condition.TYPE_AND) {
                returnValue &= read;

                if (!last &&!returnValue) {
                    return returnValue;
                }

                last = returnValue;
            } else {
                returnValue = read;
                last        = read;
            }
        }

        return returnValue;
    }

    /**
     * Method replace_param
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param pr
     */
    public void replace_param(int id, Parameter pr) {
        this.params[id] = pr;
    }

    /**
     * Method setParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param params
     */
    public void setParams(Parameter... params) {
        this.params = params;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return callName;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.callName = name;
    }

    /**
     * Method isCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isCondition() {
        return conditions.size() != 0;
    }

    /**
     * Method getCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptCondition getCondition() {
        return condition;
    }

    /**
     * Method getParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Parameter[] getParams() {
        return params;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getOpcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOpcode() {
        return opcode;
    }

    /**
     * Method setCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     */
    public void setCondition(ScriptCondition d) {
        this.condition = d;
    }

    /**
     * Method insertHead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     */
    public void insertHead(Condition c) {
        this.condition = ScriptConditionMap.getCondition(c.getName());
        conditions.add(c);
    }

    private static boolean isString(String str) {
        for (char c : str.toCharArray()) {
            if (Character.isLetter(c) || (!Character.isLetterOrDigit(c))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void setType(int t) {
        this.type = t;
    }

    /**
     * Method getOpcodeForName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public static final int getOpcodeForName(String name) {
        if (name.equalsIgnoreCase("roam")) {
            return ROAM;
        }

        if (name.equalsIgnoreCase("block_interaction") || name.equalsIgnoreCase("block_interactions")) {
            return BLOCK_INTERACTION;
        }

        if (name.equalsIgnoreCase("retreat")) {
            return RETREAT;
        }

        if (name.equalsIgnoreCase("change_model")) {
            return CHANGE_MODEL;
        }

        if (name.equalsIgnoreCase("add_slave")) {
            return ADD_SLAVE;
        }

        if (name.equalsIgnoreCase("summon_pursue")) {
            return SUMMON_PURSUE;
        }

        if (name.equalsIgnoreCase("setflying")) {
            return SET_FLYING;
        }

        if (name.equalsIgnoreCase("pursue")) {
            return PURSUE;
        }

        if (name.equalsIgnoreCase("cooldown")) {
            return COOLDOWN;
        }

        if (name.equalsIgnoreCase("set_projectile_clipping")) {
            return SET_PROJECTILE_CLIPPING;
        }

        if (name.equalsIgnoreCase("shoot_straight_projectile")) {
            return SHOOT_COMPLICATED_PROJECTILE;
        }

        if (name.equalsIgnoreCase("teleport_to_target")) {
            return TELEPORT_TO_TARGET;
        }

        if (name.equalsIgnoreCase("pause")) {
            return PAUSE;
        }

        if (name.equalsIgnoreCase("melee_hit")) {
            return MELEE_ATTACK;
        }

        if (name.equalsIgnoreCase("moverand")) {
            return MOVE_RAND;
        }

        if (name.equalsIgnoreCase("defaultanim")) {
            return DEFAULT_ANIM;
        }

        if (name.equalsIgnoreCase("loop_players")) {
            return LOOP_PLAYERS;
        }

        if (name.equalsIgnoreCase("reduce_damage")) {
            return REDUCE_DAMAGE;
        }

        if (name.equalsIgnoreCase("teleport")) {
            return TELEPORT;
        }

        if (name.equalsIgnoreCase("block_damage")) {
            return FILTER_DAMAGE;
        }

        if (name.equalsIgnoreCase("smart_pursue")) {
            return PATH_PURSUE;
        }

        if (name.equalsIgnoreCase("summon_default_attack")) {
            return SUMMON_DEFAULT_ATTACK;
        }

        if (name.equalsIgnoreCase("forage_inv_add")) {
            return FORAGE_INV_ADD;
        }

        if (name.equalsIgnoreCase("recoil_damage")) {
            return RECOIL_DAMAGE;
        }

        if (name.startsWith("apply_damage_ran")) {
            return APPLY_DAMAGE_RAN;
        }

        if (name.equalsIgnoreCase("apply_damage")) {
            return APPLY_DAMAGE;
        }

        if (name.equalsIgnoreCase("anim")) {
            return PLAY_ANIM;
        }

        if (name.equalsIgnoreCase("summon_shoot_target")) {
            return SUMMON_SHOOT_TARGET;
        }

        if (name.equalsIgnoreCase("apply_damage_within")) {
            return APPLY_DAMAGE_WITHIN;
        }

        if (name.equalsIgnoreCase("summon_apply_damage")) {
            return SUMMON_APPLY_DAMAGE;
        }

        if (name.equalsIgnoreCase("graphic")) {
            return GRAPHIC;
        }

        if (name.equalsIgnoreCase("faceto")) {
            return FACE_TO;
        }

        if (name.equalsIgnoreCase("say")) {
            return SAY_WORD;
        }

        if (name.equalsIgnoreCase("set_dist")) {
            return SET_COMBAT_DISTANCE;
        }

        if (name.equalsIgnoreCase("set_var")) {
            return SET_VAR;
        }

        if (name.equalsIgnoreCase("poison_target")) {
            return POISON_TARGET;
        }

        if (name.equalsIgnoreCase("shoot_target")) {
            return SHOOT_TARGET;
        }

        if (name.equalsIgnoreCase("PLAYER_cALL")) {
            return EXEC_PLAYER;
        }

        if (name.equalsIgnoreCase("heal_health")) {
            return HEAL_HEALTH;
        }

        if (name.equalsIgnoreCase("place_obj")) {
            return PLACE_OBJ;
        }

        if (name.equalsIgnoreCase("place_item")) {
            return PLACE_ITEM;
        }

        if (name.equalsIgnoreCase("still_graphic")) {
            return STILL_GRAPHIC;
        }

        if (name.equalsIgnoreCase("unlink")) {
            return UNLINK;
        }

        if (name.equalsIgnoreCase("exec")) {
            return EXEC;
        }

        if (name.equalsIgnoreCase("jump")) {
            return JUMP;
        }

        if (name.equalsIgnoreCase("EARTH_QUAKE")) {
            return EARTH_QUAKE;
        }

        if (name.equalsIgnoreCase("default_attack")) {
            return ATT_DEFAULT;
        }

        if (name.equalsIgnoreCase("sub_var")) {
            return SUB_VAR;
        }

        if (name.equalsIgnoreCase("add_var")) {
            return ADD_VAR;
        }

        if (name.equalsIgnoreCase("reset_vars")) {
            return RESET_VARS;
        }

        if (name.equalsIgnoreCase("pause_script")) {
            return DELAY_UPDATES;
        }

        if (name.equalsIgnoreCase("random")) {
            return RANDOM;
        }

        if (name.equalsIgnoreCase("die")) {
            return DIE;
        } else if (name.equalsIgnoreCase("fork")) {
            return FORK;
        } else if (name.equalsIgnoreCase("new_npc")) {
            return NEW_NPC;
        } else if (name.equalsIgnoreCase("dont_die")) {
            return DONT_DIE;
        } else if (name.equalsIgnoreCase("activate_scroll")) {
            return ACTIVATE_SCROLL;
        }

        return -1;
    }
}
