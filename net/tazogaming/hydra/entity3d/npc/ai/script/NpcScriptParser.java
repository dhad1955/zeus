package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.IfVar;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.IfVarAbove;
import net.tazogaming.hydra.script.build.ScriptCompiler;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.instr.Condition;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 08:40
 */
public class NpcScriptParser {

    /** script made: 14/09/01 **/
    private ControllerScript script = new ControllerScript();

    /** names made: 14/09/01 **/
    private HashMap<String, Integer> names = new HashMap<String, Integer>();

    /** varIds made: 14/09/01 **/
    private HashMap<String, Integer> varIds = new HashMap<String, Integer>();

    /** curBlockId, curVarId made: 14/09/01 **/
    private int curBlockId = 0,
                curVarId   = 0;

    /**
     * Constructs ...
     *
     *
     * @param file
     * @param pla
     */
    public NpcScriptParser(File file, Player pla) {
        int            lineNum = 0;
        BufferedReader br      = null;

        try {
            get_block_ids(file);
            br = new BufferedReader(new FileReader(file));

            String                        line                      = null;
            boolean                       reading                   = false;
            boolean                       readingPlayerBlock        = false;
            Block                         currentBlock              = null;
            SignedBlock                   curPlayerBlock            = null;
            ArrayList<NpcCall>            calls                     = new ArrayList<NpcCall>();
            ArrayList<RuntimeInstruction> playerRuntimeInstructions = new ArrayList<RuntimeInstruction>();
            boolean                       dmg                       = false;

            while ((line = br.readLine()) != null) {
                lineNum++;
                line = line.trim();

                if (line.startsWith("##") || (line.length() == 0)) {
                    continue;
                }

                if (line.startsWith("DEFINE")) {
                    String[] split = line.split(" ");

                    if (split[1].equalsIgnoreCase("name")) {
                        String name = line.substring("DEFINE name = ".length());

                        script.setIdentifier(name);
                    }

                    continue;
                }

                if (line.contains("{")) {
                    if (reading) {
                        log_error("parse error: line: " + lineNum + " " + file.getName());
                        br.close();

                        return;
                    }

                    try {
                        String name = line.substring(0, line.indexOf("("));

                        currentBlock = new Block(name);
                        currentBlock.setFileName(file.getName());

                        if (name.startsWith("sub")) {
                            name = name.substring(4);
                            currentBlock.setSubID(names.get(name));
                        }

                        if ((line.indexOf(")") - line.indexOf("(")) > 2) {
                            currentBlock.setName(line.substring(line.indexOf("\""), line.lastIndexOf("\"")));
                        }

                        reading = true;

                        continue;
                    } catch (Exception ee) {
                        ee.printStackTrace();

                        if (pla != null) {
                            pla.getActionSender().sendDev("<col=ff0000>parse error: line: " + " " + lineNum + " "
                                                          + file.getName());
                        }

                        br.close();

                        return;
                    }
                }

                if (reading &&!line.contains("}")) {
                    if (line.startsWith("$player->")) {
                        line = line.substring(9);

                        try {
                            playerRuntimeInstructions.add(ScriptCompiler.matchInstructionCall(line, file));
                            calls.add(new NpcCall());
                        } catch (Exception ee) {
                            Logger.debug("error: " + line);
                            System.err.print("file: " + file.getName());
                            ee.printStackTrace();
                        }
                    } else {
                        int interacting = -1;

                        if (line.startsWith("$interacting")) {
                            interacting = Integer.parseInt(line.substring(line.indexOf("[") + 1,
                                    line.indexOf("]", line.indexOf("["))));
                            line = line.substring(("$interacting[" + interacting + "]->").length());
                        }

                        NpcCall call = new Line(line, lineNum).setPlayer(pla).toNpcCall();

                        if (call == null) {
                            continue;
                        }

                        call.setInteractingID(interacting);

                        if ((call.getOpcode() == NpcCall.SHOOT_TARGET)
                                || (call.getOpcode() == NpcCall.SHOOT_COMPLICATED_PROJECTILE)) {
                            if (call.getParams()[call.getParams().length - 1].getType() == Parameter.TYPE_STRING) {
                                Parameter replace = new Parameter();

                                replace.setType(Parameter.TYPE_INT);
                                replace.setData(
                                    names.get(call.getParams()[call.getParams().length - 1].getStringData()));
                                call.replace_param(call.getParams().length - 1, replace);
                            }
                        }

                        if ((call.getOpcode() == NpcCall.JUMP) || (call.getOpcode() == NpcCall.LOOP_PLAYERS)
                                || (call.getOpcode() == NpcCall.FORK)) {
                            call.getParams()[0].setType(Parameter.TYPE_INT);
                            call.getParams()[0].setData(names.get(call.getParams()[0].getStringData()));
                        } else if (((call.getOpcode() == NpcCall.SET_VAR) || (call.getOpcode() == NpcCall.SUB_VAR)
                                    || (call.getOpcode() == NpcCall.RANDOM)
                                    || (call.getOpcode() == NpcCall.ADD_VAR)) || (call.isCondition()
                                       && call.containsCondition(line))) {
                            if (!call.isCondition()) {
                                call.getParams()[0].setType(Parameter.TYPE_INT);
                                call.getParams()[0].setData(varIds.get(call.getParams()[0].getStringData()));
                            } else if (call.isCondition()) {
                                for (Condition c : call.getConditions()) {
                                    Parameter[] params = c.getNpcParams();

                                    if ((c.getCondition() instanceof IfVar)
                                            || (c.getCondition() instanceof IfVarAbove)) {
                                        params[0].setData(varIds.get(params[0].getStringData()));
                                        params[0].setType(Parameter.TYPE_INT);
                                    }
                                }
                            }
                        }

                        call.setLine(line);
                        calls.add(call);
                    }
                }

                if (line.contains("}")) {
                    reading = false;

                    if (currentBlock.getName().contains("damage")) {
                        dmg = true;
                    }

                    currentBlock.setCalls(calls);

                    if (playerRuntimeInstructions.size() > 0) {
                        curPlayerBlock = new SignedBlock(playerRuntimeInstructions);
                        currentBlock.setSynced(curPlayerBlock);
                        curPlayerBlock.setOperations(playerRuntimeInstructions);
                        playerRuntimeInstructions.clear();
                    }

                    script.addSub(currentBlock);
                    calls.clear();
                }
            }
        } catch (Exception ee) {
            if (pla != null) {
                pla.getActionSender().sendMessage("@red@parse error: line: " + " " + lineNum + " " + file.getName());
            }
        } finally {
            try {
                br.close();
            } catch (Exception exception) {}
        }
    }

    private void get_block_ids(File f) throws IOException {
        BufferedReader br    = new BufferedReader(new FileReader(f));
        String         line  = null;
        StringBuilder  sb    = new StringBuilder();
        int            lineC = 0;

        while ((line = br.readLine()) != null) {
            lineC++;

            try {
                if (line.startsWith("sub")) {

                    // String
                    String string = line.substring(line.indexOf("b") + 2);

                    for (int i = 0; i < string.length(); i++) {
                        char c = string.charAt(i);

                        if ((c == ' ') || (c == '(')) {
                            break;
                        }

                        sb.append(c);
                    }

                    names.put(sb.toString(), curBlockId++);
                    sb = new StringBuilder();
                } else {
                    if (line.contains("set_var")
                            || (line.contains("+random") || line.contains("-random") || line.startsWith("random"))) {
                        String        parse = line.substring(line.indexOf("(") + 1);
                        StringBuilder sc    = new StringBuilder();

                        for (char c : parse.toCharArray()) {
                            if (c == '"') {
                                continue;
                            }

                            if ((c == ' ') || (c == ',') || (c == ')')) {
                                break;
                            }

                            sc.append(c);
                        }

                        if (!varIds.containsKey(sc.toString())) {
                            varIds.put(sc.toString(), curVarId++);
                        }
                    }
                }
            } catch (Exception ee) {
                ee.printStackTrace();
                log_error("Parse error: " + lineC + " " + f.getName());
            }
        }

        try {
            br.close();
        } catch (Exception ee) {}

        this.script.setVarMap(varIds);
    }

    /**
     * Method log_error
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void log_error(String str) {
        for (Player pla : World.getWorld().getPlayers()) {
            if (pla.getRights() >= Player.ADMINISTRATOR) {
                pla.getActionSender().sendDev("<col=ff0000>[NPC SCRIPTS]: " + str);
            }
        }

        Logger.err(str);
    }

    /**
     * Method getScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ControllerScript getScript() {
        return script;
    }
}
