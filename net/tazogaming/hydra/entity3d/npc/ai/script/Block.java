package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 08:41
 */
public class Block {
    public static final int
        TYPE_ROAM                   = 1,
        TYPE_COMBAT                 = 2,
        TYPE_BLOCK                  = 3,
        TYPE_TRIGGER                = 4;
    public static final int
        TRIGGER_NPC_DEAD            = 0,
        TRIGGER_NPC_HIT             = 1,
        TRIGGER_TARGET_DEAD         = 2,
        TRIGGER_TARGET_OUT_OF_RANGE = 3,
        TRIGGER_ON_SPAWN            = 4,
        TARGET_AQUIRED              = 6,
        TRIGGER_AREA_CLEAR          = 7,
        ON_PURSUE_GIVE_UP           = 8,
        ON_SUMMON_SPEC              = 8,
        ON_MASTER_ATTACKED_BY       = 9,
        ON_MASTER_DEAD              = 10,
        TRIGGER_SPECIAL_CLICKED     = 11,
        TRIGGER_PATH_BLOCKED        = 12,
        TRIGGER_SLAVE_KILLED        = 13,
        TRIGGER_OPTION_0            = 12,
        TRIGGER_OPTION_1            = 13,
        TRIGGER_OPTION_2            = 14,
        TRIGGER_OPTION_3            = 15;
    private int         subId       = -1;
    private int         triggerType = -1;
    private String      name;
    private NpcCall[]   calls;
    private SignedBlock synced;
    private String      filenName;

    /**
     * Constructs ...
     *
     *
     * @param name
     */
    public Block(String name) {
        this.name = name;

        if (name.startsWith("on_click_")) {
            int option = Integer.parseInt(name.substring("on_click_".length()));

            triggerType = 11 + option;
        }

        if (name.equalsIgnoreCase("on_death")) {
            triggerType = TRIGGER_NPC_DEAD;
        }

        if (getName().equalsIgnoreCase("on_slave_killed")) {
            triggerType = TRIGGER_SLAVE_KILLED;
        }

        if (name.equalsIgnoreCase("on_target_dead")) {
            triggerType = TRIGGER_TARGET_DEAD;
        }

        if (name.equalsIgnoreCase("on_target_outofrange")) {
            triggerType = TRIGGER_TARGET_OUT_OF_RANGE;
        }

        if (name.equalsIgnoreCase("on_target_acquired")) {
            triggerType = TARGET_AQUIRED;
        }

        if (name.equalsIgnoreCase("on_path_blocked")) {
            triggerType = TRIGGER_PATH_BLOCKED;
        }

        if (name.equalsIgnoreCase("on_hit")) {
            triggerType = TRIGGER_NPC_HIT;
        }

        if (name.equalsIgnoreCase("on_spawn")) {
            triggerType = TRIGGER_ON_SPAWN;
        }

        if (name.equalsIgnoreCase("on_area_clear")) {
            triggerType = TRIGGER_AREA_CLEAR;
        }

        if (name.equalsIgnoreCase("on_pursuit_giveup")) {
            triggerType = ON_PURSUE_GIVE_UP;
        }

        if (name.equalsIgnoreCase("on_summon_spec")) {
            triggerType = ON_SUMMON_SPEC;
        }

        if (name.equalsIgnoreCase("on_master_attacking")) {
            triggerType = ON_MASTER_ATTACKED_BY;
        }

        if (name.equalsIgnoreCase("on_master_dead")) {
            triggerType = ON_MASTER_DEAD;
        }
    }

    /**
     * Method setFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setFileName(String name) {
        filenName = name;
    }

    /**
     * Method getFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getFileName() {
        return filenName;
    }

    /**
     * Method getString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getString() {
        StringBuilder sb = new StringBuilder();

        sb.append(name + " " + calls.length);

        for (int i = 0; i < calls.length; i++) {
            sb.append(calls[i].getName() + " | ");
        }

        return sb.toString();
    }

    /**
     * Method setSubID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setSubID(int id) {
        this.subId = id;
    }

    /**
     * Method getSubID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSubID() {
        return subId;
    }

    /**
     * Method setSynced
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param syn
     */
    public void setSynced(SignedBlock syn) {
        synced = syn;
    }

    /**
     * Method getSync
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SignedBlock getSync() {
        return synced;
    }

    /**
     * Method getTriggerType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTriggerType() {
        return triggerType;
    }

    /**
     * Method setTriggerType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param triggerType
     */
    public void setTriggerType(int triggerType) {
        this.triggerType = triggerType;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method length
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        return calls.length;
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public NpcCall get(int id) {
        return calls[id];
    }

    /**
     * Method setCalls
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param calls
     */
    public void setCalls(ArrayList<NpcCall> calls) {
        this.calls = new NpcCall[calls.size()];
        calls.toArray(this.calls);
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }
}
