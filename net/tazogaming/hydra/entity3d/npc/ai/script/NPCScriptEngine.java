package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------


import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.ai.movement.*;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.game.skill.combat.impact.NpcProjectileImpact;
import net.tazogaming.hydra.game.skill.combat.impact.SummonProjectileImpact;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.math.GameMath;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

import java.util.HashMap;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 09:28
 */
public class NPCScriptEngine {
    public static HashMap<String, ControllerScript> scriptList = new HashMap<String, ControllerScript>();

    /**
     * Method loadScripts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     * @param pla
     */
    public static void loadScripts(String path, Player pla) {
        int size = scriptList.size();

        scriptList.clear();

        int        loaded = 0;
        File       file2  = new File(path);
        List<File> files  = (List<File>) FileUtils.listFiles(file2, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

        for (File f : files) {
            if (!f.getPath().endsWith(".cs")) {
                continue;
            }

            try {
                ControllerScript sc = new NpcScriptParser(f, pla).getScript();

                if (sc == null) {
                    throw new NullPointerException("fuck dis");
                }

                if (sc.getIdentifier() == null) {
                    throw new NullPointerException("ffs");
                }

                scriptList.put(sc.getIdentifier(), sc);
                loaded++;
            } catch (Exception ee) {
                System.err.println("error: " + f.getPath());
                ee.printStackTrace();
            }
        }

        if (pla != null) {
            pla.getActionSender().sendDev("Loaded " + loaded + " combat scripts");
        }

        Logger.log("Loaded " + loaded + " npc scripts");
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public static ControllerScript getController(String str) {
        return scriptList.get(str);
    }

    /**
     * Method evaluate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param script
     *
     * @return
     */
    public static boolean evaluate(NPC npc, NpcScriptScope script) {

        if(script == null)
            return true;


        while (script.hasNext()) {
            NpcCall c = script.next();

            try {
                ParameterParser parser = null;

                if (c == null) {
                    throw new NullPointerException();
                }

                if (c.getParams() != null) {
                    parser = new ParameterParser(c.getParams());
                }

                if (script.isConditionFlagActive()) {
                    if ((c.getType() == RuntimeInstruction.TRUE) &&!script.getConditionFlag()) {
                        continue;
                    }

                    if ((c.getType() == RuntimeInstruction.FALSE) && script.getConditionFlag()) {
                        continue;
                    }
                }

                if (c.isCondition()) {
                    script.setConditionFlagActive(true);

                    try {
                        script.setConditionFlag(c.getConditionReturnValue(npc, script));
                    } catch (Exception ee) {

//                      script.getInteractingPlayer().getActionSender().sendMessage("error: "+c.getName()+" "+c.getLine());
                        ee.printStackTrace();
                    }
                }

                if (c == null) {
                    throw new NullPointerException();
                }

                if (c.getOpcode() == NpcCall.EXEC_PLAYER) {
                    RuntimeInstruction player_runtimeInstruction = script.getPlayerSyncBlock().getNext();

                    if (player_runtimeInstruction.isCondition()) {
                        script.setConditionFlagActive(true);
                        script.setConditionFlag(
                            player_runtimeInstruction.evaluateCondition(
                                script.getInteractingPlayer(), script.getPlayerSyncBlock()));

                        continue;
                    }

                    if (script.isConditionFlagActive()) {
                        if ((player_runtimeInstruction.getType() == RuntimeInstruction.FALSE)
                                && script.getConditionFlag()) {
                            continue;
                        } else if ((player_runtimeInstruction.getType() == RuntimeInstruction.TRUE)
                                   &&!script.getConditionFlag()) {
                            continue;
                        }
                    }

                    try {
                       Scope scope = new Scope();

                        ScriptRuntime.executeInstruction(player_runtimeInstruction, script.getInteractingPlayer(),
                                                         script.getPlayerSyncBlock());
                    } catch (Exception ee) {
                      //  ee.printStackTrace();
                      if(script.getInteractingPlayer() != null)
                        script.getInteractingPlayer().getActionSender().sendMessage("Failed to evaluate: "
                                + player_runtimeInstruction.getName());
                    }

                    continue;
                }

                if (c.getInteractingId() != -1) {
                    NPC slave = script.getInteracting(c.getInteractingId());

                    if (slave == null) {
                        slave = npc.getControllerScript().getSlaves()[c.getInteractingId()];

                        if (slave == null) {
                            continue;
                        }
                    }

                    NpcScriptScope scr = new NpcScriptScope(slave, script.getBlock(),
                                               script.getInteractingPlayer());

                    executeLine(c, scr, slave, parser);

                    continue;
                }

                int i = executeLine(c, script, npc, parser);

                if (i == 0) {
                    return true;
                }

                if (i == 1) {
                    return false;
                }
            } catch (Exception ee) {
                Logger.log("ERROR: " + script.getBlock().getName() + " " + script.getBlock().getFileName());
                ee.printStackTrace();

                return true;
            }
        }

        return true;
    }

    /**
     * Method executeLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     * @param script
     * @param npc
     * @param parser
     *
     * @return
     */
    public static int executeLine(NpcCall c, NpcScriptScope script, NPC npc, ParameterParser parser) {
        switch (c.getOpcode()) {
        case NpcCall.SAY_WORD :
            npc.setTextUpdate(parser.nextString());

            break;

        case NpcCall.SUMMON_PURSUE :
            if (script.getInteracting(0) != null) {
                npc.setMovement(new SummonAttackNPCMovement(npc, script.getInteracting(0)));
            }

            break;

        case NpcCall.ADD_SLAVE :
            int          slave_identifier = parser.nextInt();
            int          slave_npc_id     = parser.nextInt();
            int          slave_x          = parser.nextInt();
            int          slave_y          = parser.nextInt();
            int          slave_range      = parser.nextInt();
            NpcAutoSpawn sp               = new NpcAutoSpawn();

            sp.setSpawnHeight(npc.getHeight());
            sp.setSpawnX(slave_x);
            sp.setSpawnY(slave_y);
            sp.setRoamRange(slave_range);

            final NPC master       = npc;
            NPC       slave_to_add = new NPC(slave_npc_id, Point.location(slave_x, slave_y, npc.getHeight()), sp);

            World.getWorld().registerNPC(slave_to_add);
            npc.getControllerScript().setSlave(slave_identifier, slave_to_add);
            slave_to_add.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
                @Override
                protected void onDeath(NPC killed, Killable killedBy) {
                    NPC blah = (NPC) killed;

                    master.getControllerScript().do_trigger(Block.TRIGGER_SLAVE_KILLED, blah);
                }
            });

            break;

        case NpcCall.PATH_PURSUE :
            if (script.getInteractingPlayer() == null) {
                break;
            }

            npc.setMovement(new PathFinding(npc, script.getInteractingPlayer()));

            break;

        case NpcCall.RECOIL_DAMAGE :
        case NpcCall.REDUCE_DAMAGE :
            int    percent    = parser.nextInt();
            double actualPerc = (double) (percent / 100d);
            Damage d          = script.getCurrentDamage();

            if (c.getOpcode() == NpcCall.REDUCE_DAMAGE) {
                if (d != null) {
                    script.setReturnDamage((int) (d.getDamage() * actualPerc));
                }
            } else if (c.getOpcode() == NpcCall.RECOIL_DAMAGE) {
                if (d != null) {
                    if (script.getInteractingPlayer() != null) {
                        script.getInteractingPlayer().hit(npc, (int) (d.getDamage() * actualPerc), Damage.TYPE_OTHER,
                                                          0);
                    }
                }
            }

            break;

        case NpcCall.FORAGE_INV_ADD :
            int inv_add_id = parser.nextInt();
            int inv_max    = parser.nextInt();

            if (npc.getForageItems().size() == inv_max) {
                break;
            }

            npc.getForageItems().add(Item.forId(inv_add_id));

            break;

        case NpcCall.FILTER_DAMAGE :
            script.setReturnDamage(0);

            break;

        case NpcCall.PLAY_ANIM :
            npc.getAnimation().animate(parser.nextInt(), parser.nextInt());

            break;

        case NpcCall.ACTIVATE_SCROLL :

            Player plr = npc  .getSummoner();

            int scroll_id = plr.getAccount().getInt32("scroll_id");
            int pts       = parser.nextInt();

            plr.removeSpecPts(pts);
            plr.getInventory().deleteItem(scroll_id, 1);
            plr.getAnimation().animate(7660);
            plr.getGraphic().set(1316, 0);

            break;

        case NpcCall.DIE :
            return 0;

        case NpcCall.CHANGE_MODEL :
            npc.setTransform(parser.nextInt());

            break;

        case NpcCall.JUMP :
            int   block_id = parser.nextInt();
            Block block    = npc.getControllerScript().getController().getSub(block_id);

            if (block != null) {
                NpcScriptScope scr = new NpcScriptScope(npc, block, script.getInteractingPlayer());

                scr.setInteractingPlayer(script.getInteractingPlayer());
                scr.setInteractingNpc(script.getInteractingNpc());
                evaluate(npc, scr);

                return 0;
            }

            break;

        case NpcCall.FORK :
            block = npc.getControllerScript().getController().getSub(parser.nextInt());

            if (block != null) {
                NpcScriptScope scr = new NpcScriptScope(npc, block, script.getInteractingPlayer());

                scr.setInteractingPlayer(script.getInteractingPlayer());
                evaluate(npc, scr);
            }

            break;

        case NpcCall.LOOP_PLAYERS :
            block = npc.getControllerScript().getController().getSub(parser.nextInt());

            int distance = parser.nextInt();

            for (Player player : npc.getKnownPlayersInRange()) {
                if (!npc.isWithinDistance(player, distance)) {
                    continue;
                }

                if (block != null) {
                    NpcScriptScope scr = new NpcScriptScope(npc, block, script.getInteractingPlayer());

                    scr.setInteractingPlayer(player);
                    evaluate(npc, scr);
                }
            }

            break;

        case NpcCall.POISON_TARGET :
            script.getInteractingPlayer().poison(parser.nextInt());

            break;

        case NpcCall.SET_VAR :
            int varId = parser.nextInt();
            int value = parser.nextInt();

            npc.getControllerScript().setVar(varId, value);

            break;

        case NpcCall.SUB_VAR :
            npc.getControllerScript().changeVar(parser.nextInt(), -parser.nextInt());

            break;

        case NpcCall.RESET_VARS :
            npc.getControllerScript().setCurrentVars(new int[20]);

            break;

        case NpcCall.ADD_VAR :
            npc.getControllerScript().changeVar(parser.nextInt(), parser.nextInt());

            break;

        case NpcCall.GRAPHIC :
            npc.getGraphic().set(parser.nextInt(), parser.nextInt());

            break;

        case NpcCall.ATT_DEFAULT :
            npc.getAnimation().animate(npc.getDefinition().getAttackAnim(), 1);
            npc.getCombatHandler().setCooldownTime(npc.getDefinition().getCooldown());

            if (npc.getCombatHandler().getFightingWith() == npc) {
                throw new RuntimeException();
            }

            if (npc.getCombatHandler().getFightingWith() == null) {
                break;
            }

            npc.getCombatHandler().getFightingWith().registerHit(npc);
            npc.getCombatHandler().getFightingWith().hit(npc,
                    npc.getRandom().nextInt(npc.getDefinition().getMaxHit() + npc.getDefinition().getMaxHitModifier()
                                            + 1), Damage.TYPE_MELEE_CRUSH, 1);

            break;

        case NpcCall.PAUSE :
            npc.getControllerScript().delay(script, parser.nextInt());

            return 1;

        case NpcCall.APPLY_DAMAGE_RAN :
        case NpcCall.APPLY_DAMAGE :
            int dmg = parser.nextInt() + npc.getDefinition().getMaxHitModifier();

            if (c.getOpcode() == NpcCall.APPLY_DAMAGE_RAN) {
                dmg = npc.getRandom().nextInt(dmg);
            }

            if (script.getInteractingPlayer() == null) {
                break;
            }

            // if(npc.getCombatHandler().getFightingWith() == null)
            // break;
            script.getInteractingPlayer().hit(npc, dmg, parser.nextInt(), parser.nextInt());
            script.getInteractingPlayer().registerHit(npc);

            break;

        case NpcCall.ROAM :
            if ((parser != null) && parser.hasNext()) {
                npc.setRoamSpeed(parser.nextInt());
            }

            if (npc.getMovement() instanceof RoamingMovement) {
                npc.getMovement().tick(Core.currentTime);
            } else {
                npc.setMovement(new RoamingMovement(npc));
                npc.getMovement().tick(Core.currentTime);
            }

            break;

        case NpcCall.PURSUE :
            npc.getCombatHandler().reset();

            if (script.getInteractingNpc() != null) {
                npc.setMovement(new SummonAttackNPCMovement(npc, script.getInteractingNpc()));
            } else if (script.getInteractingPlayer() != null) {
                npc.setMovement(new FollowPlayerMovement(npc, script.getInteractingPlayer(), true));
                npc.getFocus().focus(script.getInteractingPlayer());
            }

            break;

        case NpcCall.RANDOM :
            varId = parser.nextInt();

            int max = parser.nextInt();
            int ran = npc.getRandom().nextInt(max) + 1;

            npc.getControllerScript().setVar(varId, ran);

            break;

        // case NpcCall.BLOCK_INTERACTION:
        case NpcCall.MELEE_ATTACK :
            if (npc.getCombatHandler().getFightingWith() != null) {
                npc.getCombatHandler().getFightingWith().hit(npc, npc.getRandom().nextInt(parser.nextInt()),
                        Damage.TYPE_MELEE_CRUSH, 1);
                npc.getCombatHandler().getFightingWith().registerHit(npc);
            }

            break;

        case NpcCall.RETREAT :
            npc.setMovement(new RoamingMovement(npc));
            npc.getCombatHandler().reset();
            npc.getFocus().unFocus();

            break;

        case NpcCall.SET_COMBAT_DISTANCE :
            npc.getCombatHandler().setCombatDistance(parser.nextInt());

            break;

        case NpcCall.COOLDOWN :
            npc.getCombatHandler().setCooldownTime(parser.nextInt());

            break;

        case NpcCall.HEAL_HEALTH :
            npc.addHealth(parser.nextInt());

            break;

        case NpcCall.STILL_GRAPHIC :
            for (Player pla : npc.getPlayersInRange(15)) {
                if (pla == null) {
                    break;
                }

                if (pla.getCurrentInstance() != npc.getCurrentInstance()) {
                    continue;
                }

                pla.getActionSender().sendStillGFX(parser.nextInt(), parser.nextInt(), parser.nextInt());
            }

            break;

        case NpcCall.UNLINK :
            npc.unlink();

            break;

        case NpcCall.FACE_TO :
            npc.getFacingLocation().focus(Point.location(parser.nextInt(), parser.nextInt(), npc.getHeight()));

            break;

        case NpcCall.PLACE_OBJ :
            GameObject obj = new GameObject();

            obj.setIdentifier(parser.nextInt());
            obj.setCurrentRotation(parser.nextInt());
            obj.setLocation(npc.getLocation());
            obj.scheduleRemove(parser.nextInt());

            break;

        case NpcCall.SHOOT_TARGET :
        case NpcCall.SHOOT_COMPLICATED_PROJECTILE :
            int   movingGraphic          = parser.nextInt();
            int   movingGraphicWaitSpeed = parser.nextInt();
            int   movingGraphicSpeed     = parser.nextInt();
            int   start_height           = parser.nextInt();
            int   end_height             = parser.nextInt();
            int   maxHit                 = parser.nextInt() + npc.getDefinition().getMaxHitModifier();
            int   type                   = parser.nextInt();
            Point centerPoint            = npc.getCenterPoint();

            if (script.getInteractingPlayer() == null) {
                throw new NullPointerException("error...");
            }

            Projectile proj = new Projectile(movingGraphic, npc, script.getInteractingPlayer(), centerPoint.getX(),
                                             centerPoint.getY(), script.getInteractingPlayer().getX(),
                                             script.getInteractingPlayer().getY(), movingGraphicSpeed,
                                             movingGraphicWaitSpeed, start_height, end_height);

            proj.setAttribute(1, start_height);
            proj.setAttribute(2, end_height);

            if (c.getOpcode() == NpcCall.SHOOT_COMPLICATED_PROJECTILE) {
                proj.setAttribute(5, 0);
            }

            Block impactBlock = null;

            if (parser.hasNext()) {
                impactBlock = npc.getControllerScript().getController().getSub(parser.nextInt());
            }

            proj.setOnImpactEvent(new NpcProjectileImpact(type, maxHit, npc, impactBlock));
            npc.registerProjectile(proj);
            script.getInteractingPlayer().registerHit(npc);

            break;

        case NpcCall.TELEPORT :
            int     tele_x     = parser.nextInt();
            int     tele_y     = parser.nextInt();
            int     tele_h     = parser.nextInt();
            boolean check_safe = parser.hasNext();
            Point   p          = Point.location(tele_x, tele_y, tele_h);

            if (check_safe) {
                p = Movement.getTeleLocation(npc, p);
            }

            if (p != null) {
                npc.teleport(p.getX(), p.getY(), p.getHeight());
            }

            break;

        case NpcCall.TELEPORT_TO_TARGET :
            Point tele_pt = Movement.getTeleLocation(npc, script.getInteractingPlayer());

            if (tele_pt != null) {
                try {
                    npc.getRoute().resetPath();
                    npc.teleport(tele_pt.getX(), tele_pt.getY(), tele_pt.getHeight());
                } catch (Exception exception) {}
            }

            break;

        case NpcCall.SET_PROJECTILE_CLIPPING :
            npc.setIgnoreProjectileClipping(parser.nextInt() == 0);

            break;

        case NpcCall.SET_FLYING :
            npc.setFlying(parser.nextInt() == 1);

            break;

        case NpcCall.SUMMON_SHOOT_TARGET :
            movingGraphic          = parser.nextInt();
            movingGraphicWaitSpeed = parser.nextInt();
            movingGraphicSpeed     = parser.nextInt();
            start_height           = parser.nextInt();
            end_height             = parser.nextInt();
            maxHit                 = parser.nextInt() + npc.getDefinition().getMaxHitModifier();
            type                   = parser.nextInt();


            proj = new Projectile(npc, script.getInteractingNpc() == null ? script.getInteractingPlayer() : script.getInteractingNpc(), movingGraphic, movingGraphicWaitSpeed,
                                  movingGraphicSpeed);

            SummonProjectileImpact sum_proj_event = new SummonProjectileImpact(npc, script.getInteractingNpc() == null ? script.getInteractingPlayer() : script.getInteractingNpc(),
                                                        npc.getSummoner(), type, npc.getRandom().nextInt(maxHit));

            if (parser.hasNext()) {
                sum_proj_event.setEndGFX(parser.nextInt(), parser.nextInt());
            }

            proj.setOnImpactEvent(sum_proj_event);
            proj.setAttribute(1, start_height);
            proj.setAttribute(2, end_height);
            npc.registerProjectile(proj);

            break;

        case NpcCall.SUMMON_DEFAULT_ATTACK :
            npc.getAnimation().animate(npc.getDefinition().getAttackAnim(), 1);
            npc.getCombatHandler().setCooldownTime(npc.getDefinition().getCooldown());

            if (npc.getCombatHandler().getFightingWith() == npc) {
                throw new UnknownError();
            }

            npc.getCombatHandler().getFightingWith().registerHit(npc);
            if(script.getInteractingNpc() != null)
            script.getInteractingNpc().hit(npc, GameMath.rand(npc.getDefinition().getMaxHit()),
                                           Damage.TYPE_MELEE_CRUSH, 1);
            else if(script.getInteractingPlayer() != null)
                script.getInteractingPlayer().hit(npc, GameMath.rand(npc.getDefinition().getMaxHit()),
                        Damage.TYPE_MELEE_CRUSH, 1);

            break;

        case NpcCall.APPLY_DAMAGE_WITHIN :
            dmg  = npc.getRandom().nextInt(parser.nextInt()) + npc.getDefinition().getMaxHitModifier();
            type = parser.nextInt();

            int delay = parser.nextInt();

            for (Player pla : npc.getPlayersInRange(parser.nextInt())) {
                if (pla != null) {
                    pla.hit(npc, dmg, type, delay);

                    if (pla.getCurrentFamiliar() != null) {
                        pla.getCurrentFamiliar().hit(npc, type, dmg, delay);
                    }
                }
            }

            break;

        case NpcCall.SUMMON_APPLY_DAMAGE :
            dmg   = parser.nextInt() + npc.getDefinition().getMaxHitModifier();
            type  = parser.nextInt();
            delay = parser.nextInt();
            dmg   = npc.getRandom().nextInt(dmg);

           if(script.getInteractingNpc() != null)
            script.getInteractingNpc().hit(npc, dmg, type, delay, false);
            else
           script.getInteractingPlayer().hit(npc, dmg, type, delay, false);

            break;

        case NpcCall.DONT_DIE :
            npc.setDont_die(true);

            break;

        case NpcCall.DELAY_UPDATES :
            npc.getControllerScript().hardPause(parser.nextInt());

            break;

        case NpcCall.NEW_NPC :
            final int npc_id = parser.nextInt();
            int       npc_x  = parser.nextInt();
            int       npc_y  = parser.nextInt();
            int       range  = parser.nextInt();

            if ((npc_x < 10) || (npc_y < 10)) {
                npc_x = npc.getX();
                npc_y = npc.getY();
            }

            final int          spawn_x = npc_x;
            final int          spawn_y = npc_y;
            final NpcAutoSpawn data    = new NpcAutoSpawn();

            data.setNpcId(npc_id);
            data.setSpawnX(npc_x);
            data.setSpawnY(npc_y);
            data.setSpawnHeight(npc.getHeight());
            data.setRoamRange(range);

            final int height = npc.getHeight();

            int time = 0;
            if(parser.hasNext())
                time = parser.nextInt();

            World.getWorld().submitEvent(new WorldTickEvent(time) {
                @Override
                public void tick() {

                }
                @Override
                public void finished() {
                    NPC spawned_npc = new NPC(npc_id, Point.location(spawn_x, spawn_y, height), data);

                    World.getWorld().registerNPC(spawned_npc);
                    terminate();
                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });

            break;

        case NpcCall.BLOCK_INTERACTION :
            npc.setBlock_interactions(parser.nextInt() == 1);

            break;

        case NpcCall.EARTH_QUAKE :
            for (Player pla : npc.getPlayersInRange(15)) {
                if (pla == null) {
                    break;
                }

                if (pla.getCurrentInstance() != npc.getCurrentInstance()) {
                    continue;
                }

                pla.getWindowManager().closeWindow();
                pla.getActionSender().sendCameraShake(parser.nextInt());
                pla.addEventIfAbsent(new PlayerTickEvent(pla, true, parser.nextInt()) {
                    @Override
                    public void doTick(Player owner) {
                        owner.getActionSender().sendCameraShake(0);
                    }
                });
            }

            break;
        }

        return 3;
    }
}
