package net.tazogaming.hydra.entity3d.npc.ai.script.smartnpc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.ArgumentTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 03:09
 */
public class NPCMovementScript {
    private static HashMap<Integer, NPCMovementScript> scripts         = new HashMap<Integer, NPCMovementScript>();
    private int                                          currentFunction = 0;
    private int                                          pauseAt;
    private NCall[]                                      calls;

    /**
     * Method unpackFile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     */
    public void unpackFile(File f) {
        try {
            BufferedReader   reader   = new BufferedReader(new FileReader(f));
            String           line     = null;
            ArrayList<NCall> calls    = new ArrayList<NCall>();
            int              scriptId = -1;

            while ((line = reader.readLine()) != null) {
                if (line.startsWith("//") || (line.length() == 0)) {
                    continue;
                }

                ArgumentTokenizer pr       = new ArgumentTokenizer(line.split(" "));
                String            function = pr.nextString();
                NCall             curCall  = new NCall();

                if (function.startsWith("scriptid")) {
                    scriptId = pr.nextInt();

                    continue;
                }

                if (function.equalsIgnoreCase("walkto")) {
                    curCall.setOpcode(NCall.MOVE_TO);
                    curCall.setParams(new int[] { pr.nextInt(), pr.nextInt() });
                } else if (function.equalsIgnoreCase("pause")) {
                    curCall.setOpcode(NCall.PAUSE);
                    curCall.setParams(new int[] { pr.nextInt() });
                } else if (function.equalsIgnoreCase("say")) {
                    String str = line.substring(4);

                    curCall.setOpcode(NCall.SAY_WORD);
                    curCall.setParams2(new String[] { str });
                } else if (function.equalsIgnoreCase("animation")) {
                    curCall.setOpcode(NCall.PLAY_ANIMATION);
                    curCall.setParams(new int[] { pr.nextInt() });
                } else if (function.equalsIgnoreCase("faceto")) {
                    curCall.setOpcode(NCall.FACE_LOCATION);
                    curCall.setParams(new int[] { pr.nextInt(), pr.nextInt() });
                } else if (function.equalsIgnoreCase("facedir")) {
                    curCall.setOpcode(NCall.FACE_DIR);
                    curCall.setParams(new int[] { pr.nextInt() });
                }

                calls.add(curCall);
            }

            this.calls = new NCall[calls.size()];

            for (int i = 0; i < calls.size(); i++) {
                this.calls[i] = calls.get(i);
            }

            reader.close();

            if (scriptId != -1) {
                scripts.put(scriptId, this);
            }
        } catch (Exception ee) {}
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NCall next() {
        increaseFunction();

        return calls[currentFunction];
    }

    /**
     * Method cur
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NCall cur() {
        return calls[currentFunction];
    }

    private void increaseFunction() {
        currentFunction++;

        if (currentFunction == calls.length) {
            currentFunction = 0;
        }
    }

    /**
     * Method pause
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param secs
     */
    public void pause(int secs) {
        pauseAt = Core.currentTime + secs;
    }

    /**
     * Method isPaused
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isPaused() {
        return Core.timeUntil(pauseAt) >= 0;
    }

    /**
     * Method getForId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static NPCMovementScript getForId(int id) {
        return scripts.get(id);
    }

    /**
     * Method unpackFiles
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dir
     */
    public static void unpackFiles(String dir) {
        List<File> files = (List<File>) FileUtils.listFiles(new File(dir), TrueFileFilter.INSTANCE,
                               TrueFileFilter.INSTANCE);

        for (File f : files) {
            new NPCMovementScript().unpackFile(f);
        }
    }

    /**
     * Method rehash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void rehash(Player player) {
        int size = scripts.size();

        scripts.clear();
        unpackFiles("config/world/npc/smartnpc/");
        player.getActionSender().sendDev("Rehashed movement scripts, loaded " + scripts.size() + " new scripts: "
                                         + (scripts.size() - size));

        for (NPC npc : World.getWorld().getNpcs()) {
            npc.reloadForScript();
        }
    }
}
