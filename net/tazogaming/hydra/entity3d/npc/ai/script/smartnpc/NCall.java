package net.tazogaming.hydra.entity3d.npc.ai.script.smartnpc;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/10/13
 * Time: 04:24
 */
public class NCall {
    public static final int
        MOVE_TO        = 0,
        SAY_WORD       = 1,
        PLAY_ANIMATION = 2,
        FACE_LOCATION  = 3,
        PAUSE          = 4,
        FACE_DIR       = 5;
    private int[]    params;
    private String[] params2;
    private int      opcode;

    /**
     * Method getOpcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOpcode() {
        return opcode;
    }

    /**
     * Method getParam
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public int getParam(int i) {
        return params[i];
    }

    /**
     * Method getParamString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public String getParamString(int i) {
        return params2[i];
    }

    /**
     * Method getParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getParams() {
        return params;
    }

    /**
     * Method setParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param params
     */
    public void setParams(int[] params) {
        this.params = params;
    }

    /**
     * Method getParams2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getParams2() {
        return params2;
    }

    /**
     * Method setParams2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param params2
     */
    public void setParams2(String[] params2) {
        this.params2 = params2;
    }

    /**
     * Method setOpcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param opcode
     */
    public void setOpcode(int opcode) {
        this.opcode = opcode;
    }
}
