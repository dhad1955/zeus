package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 08:39
 */
public class ControllerScript {
    private Block[]          sub_types = new Block[20];
    private ArrayList<Block> blocks    = new ArrayList<Block>();
    private Block            combatScript;
    private Block            idleScript;
    private Block            summon_combatScript;
    private Block            damageFilter;
    private Block            tick_script;
    private String           identifier;
    private String[]         varMap;

    /**
     * Method getVarIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public Integer getVarIndex(String name) {
        for (int i = 0; i < varMap.length; i++) {
            if (varMap[i].equalsIgnoreCase(name)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method setVarMap
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param var_map
     */
    public void setVarMap(HashMap<String, Integer> var_map) {
        String[] varMap = new String[var_map.size()];

        // int i = 0;
        for (String g : var_map.keySet()) {
            varMap[var_map.get(g)] = g;
        }

        this.varMap = varMap;
    }

    /**
     * Method getDamageFilter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Block getDamageFilter() {
        return damageFilter;
    }

    /**
     * Method setDamageFilter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damageFilter
     */
    public void setDamageFilter(Block damageFilter) {
        this.damageFilter = damageFilter;
    }

    /**
     * Method getTick_script
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Block getTick_script() {
        return tick_script;
    }

    /**
     * Method setTick_script
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tick_script
     */
    public void setTick_script(Block tick_script) {
        this.tick_script = tick_script;
    }

    /**
     * Method getIdentifier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * Method setIdentifier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param identifier
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * Method getSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public Block getSub(int id) {
        return sub_types[id];
    }

    /**
     * Method getCombatScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Block getCombatScript() {
        return combatScript;
    }

    /**
     * Method getSummonCombatScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Block getSummonCombatScript() {
        return summon_combatScript;
    }

    /**
     * Method directTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param npc
     * @param interacting
     *
     * @return
     */
    public boolean trigger(int type, NPC npc, Mob interacting) {
        Block script_block = getTrigger(type);

        if (script_block == null) {
            return false;
        }

        NpcScriptScope script = new NpcScriptScope(npc, script_block);

        if ((interacting != null) && (interacting instanceof Player)) {
            script.setInteractingPlayer((Player) interacting);
        } else if ((interacting != null) && (interacting instanceof NPC)) {
            script.setInteractingNpc((NPC) interacting);
        }

        NPCScriptEngine.evaluate(npc, script);

        return true;
    }

    /**
     * Method getIdle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Block getIdle() {
        return idleScript;
    }

    /**
     * Method getSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param trigger
     *
     * @return
     */
    public Block getSub(String trigger) {
        for (Block block : blocks) {
            if (block.getName().equalsIgnoreCase(trigger)) {
                return block;
            }
        }

        return null;
    }

    /**
     * Method getTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     *
     * @return
     */
    public Block getTrigger(int type) {
        for (Block block : blocks) {
            if (block.getTriggerType() == type) {
                return block;
            }
        }

        return null;
    }

    /**
     * Method addSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sub
     */
    public void addSub(Block sub) {
        if (sub.getName().equalsIgnoreCase("do_idle")) {
            idleScript = sub;

            return;
        }

        if (sub.getSubID() != -1) {
            sub_types[sub.getSubID()] = sub;

            return;
        }

        if (sub.getName().equalsIgnoreCase("do_summoncombat")) {
            summon_combatScript = sub;
        } else if (sub.getName().equalsIgnoreCase("do_combat")) {
            combatScript = sub;
        } else if (sub.getName().equalsIgnoreCase("do_tick")) {
            tick_script = sub;
        } else if (sub.getName().equalsIgnoreCase("do_damagefilter")) {
            damageFilter = sub;
        } else {
            blocks.add(sub);
        }
    }
}
