package net.tazogaming.hydra.entity3d.npc.ai.script;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 09:12
 */
public class NpcScriptScope {
    private boolean conditionFlag       = false;
    private boolean conditionFlagActive = false;
    private int     currentOffset       = 0;
    private int     pauseTime           = 0;
    private NPC[]   interactingNpcs     = new NPC[6];
    private int     loop_save_offset    = -1;
    private Block   running;
    private Scope   playerSyncBlock;
    private boolean otherConditionFlag;
    private Player  interactingPlayer;
    private NPC     interactingNpc;
    private Damage  currentDamage;
    private int     returnDamage;

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param running
     */
    public NpcScriptScope(NPC npc, Block running) {
        this(npc, running, null);
    }

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param running
     * @param bind
     */
    public NpcScriptScope(NPC npc, Block running, Player bind) {
        this.running           = running;
        this.interactingPlayer = bind;

        if (running == null) {
            return;
        }

        if (running.getSync() != null) {
            this.playerSyncBlock = new Scope();
            this.playerSyncBlock.setController(bind);
            this.playerSyncBlock.setRunning(running.getSync());
        }
    }

    /**
     * Method isLooping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isLooping() {
        return loop_save_offset != -1;
    }

    /**
     * Method anchor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void anchor() {
        loop_save_offset = currentOffset;
    }

    /**
     * Method unAnchor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unAnchor() {
        loop_save_offset = -1;
    }

    /**
     * Method setInteractingNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param NPC
     */
    public void setInteractingNPC(int i, NPC NPC) {
        interactingNpcs[i] = NPC;
    }

    /**
     * Method getInteracting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public NPC getInteracting(int i) {
        return interactingNpcs[i];
    }

    /**
     * Method getReturnDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getReturnDamage() {
        return returnDamage;
    }

    /**
     * Method setReturnDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param returnDamage
     */
    public void setReturnDamage(int returnDamage) {
        this.returnDamage = returnDamage;
    }

    /**
     * Method setCurrentDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentDamage
     */
    public void setCurrentDamage(Damage currentDamage) {
        this.currentDamage = currentDamage;
        this.returnDamage  = (int)currentDamage.getDamage();
    }

    /**
     * Method getCurrentDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Damage getCurrentDamage() {
        return currentDamage;
    }

    /**
     * Method getBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Block getBlock() {
        return running;
    }

    /**
     * Method getBlockID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBlockID() {
        return running.getSubID();
    }

    /**
     * Method getOffset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOffset() {
        return currentOffset;
    }

    /**
     * Method is
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param block
     *
     * @return
     */
    public boolean is(Block block) {
        return running == block;
    }

    /**
     * Method getPlayerSyncBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Scope getPlayerSyncBlock() {
        return playerSyncBlock;
    }

    /**
     * Method isSynced
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSynced() {
        return playerSyncBlock != null;
    }

    /**
     * Method isConditionFlagActive
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isConditionFlagActive() {
        return conditionFlagActive;
    }

    /**
     * Method setConditionFlagActive
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param conditionFlagActive
     */
    public void setConditionFlagActive(boolean conditionFlagActive) {
        this.conditionFlagActive = conditionFlagActive;
    }

    /**
     * Method getConditionFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean getConditionFlag() {
        return conditionFlag;
    }

    /**
     * Method setConditionFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setConditionFlag(boolean b) {
        conditionFlag = b;
    }

    /**
     * Method getInteractingPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getInteractingPlayer() {
        return interactingPlayer;
    }

    /**
     * Method setInteractingPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interactingPlayer
     */
    public void setInteractingPlayer(Player interactingPlayer) {
        this.interactingPlayer = interactingPlayer;
    }

    /**
     * Method getInteractingNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC getInteractingNpc() {
        return interactingNpc;
    }

    /**
     * Method setInteractingNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interactingNpc
     */
    public void setInteractingNpc(NPC interactingNpc) {
        this.interactingNpc = interactingNpc;
    }

    /**
     * Method hasNext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasNext() {
        return currentOffset < running.length();
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NpcCall next() {
        return running.get(currentOffset++);
    }

    /**
     * Method rewind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void rewind() {
        currentOffset = 0;
    }

    /**
     * Method isDelayed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDelayed() {
        return pauseTime > Core.currentTime;
    }

    /**
     * Method delay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     */
    public void delay(int ticks) {
        pauseTime = Core.currentTime + ticks;
    }
}
