package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Route;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.smartnpc.NCall;
import net.tazogaming.hydra.entity3d.npc.ai.script.smartnpc.NPCMovementScript;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 03:17
 */
public class ScriptMovement extends Movement {
    private NPCMovementScript currentScript;
    private Route route;
    private NPC               curNpc;

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param script
     */
    public ScriptMovement(NPC npc, NPCMovementScript script) {
        super(npc);
        this.currentScript = script;
        this.route = new Route(npc);
        curNpc             = npc;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {
        if (currentScript == null) {
            return;
        }

        NCall currentFunction = currentScript.cur();

        if (curNpc.isBusy(null)) {
            return;    // doing something
        }

        if (currentScript.isPaused()) {
            return;    // wait
        }

        if ((currentFunction.getOpcode() == NCall.MOVE_TO) &&!route.isIdle()) {
            route.updatePosition();

            return;    // wait
        }

        currentFunction = currentScript.next();

        switch (currentFunction.getOpcode()) {
        case NCall.MOVE_TO :
            int x = currentFunction.getParam(0);
            int y = currentFunction.getParam(1);

            route.addPoint(Point.location(x, y, curNpc.getHeight()));
            route.updatePosition();

            return;

        case NCall.PLAY_ANIMATION :
            curNpc.getAnimation().animate(currentFunction.getParam(0));

            return;

        case NCall.FACE_LOCATION :
            curNpc.getFacingLocation().focus(currentFunction.getParam(0), currentFunction.getParam(1));

            return;

        case NCall.FACE_DIR :
            Point p = getPointForDir(curNpc.getLocation(), currentFunction.getParam(0));

            curNpc.getFacingLocation().focus(p);

            return;

        case NCall.PAUSE :
            currentScript.pause(currentFunction.getParam(0));

            return;

        case NCall.SAY_WORD :
            curNpc.setTextUpdate(currentFunction.getParamString(0));

            return;
        }
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
