package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/10/13
 * Time: 10:53
 */
public class SummonAttackNPCMovement extends Movement {
    private Killable attacking_on;

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param move_towards
     */
    public SummonAttackNPCMovement(NPC npc, Killable move_towards) {
        super(npc);
        attacking_on = move_towards;
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        getMob().getFocus().unFocus();

        if (getMob().isSummoned()) {
            getMob().setMovement(new SummoningMovement(getMob(), getMob().getSummoner()));
        } else {
            getMob().setMovement(new RoamingMovement(getMob()));
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {
        getMob().getFocus().focus(attacking_on);    // ;es.

        if(getMob().getTimers().timerActive(TimingUtility.FROZEN_TIME))
            return;

        if (getMob().isSummoned()
                && (Point.getDistance(getMob().getSummoner().getLocation(), getMob().getLocation()) >= 10)) {
            getMob().setMovement(new SummoningMovement(getMob(), getMob().getSummoner()));

            return;
        }

        if (attacking_on.isDead() || getMob().isDead()) {
            reset();

            return;
        }

        if (getMob().getCombatHandler().isInCombat()) {
            return;
        }

        if (Combat.isWithinDistance(getMob(), attacking_on)) {
            getMob().getCombatHandler().attackOn(attacking_on);

            return;
        } else {}

        Tile[] tiles        = NPC.getTiles(getMob(), getMob().getLocation());
        int    dir          = getDirectionForWaypoints(getMob().getLocation(), attacking_on.getLocation());
        Point  nextLocation = getPointForDir(getMob().getLocation(), dir);

        if (!getMob().isSummoned() &&!getMob().canGo(nextLocation)) {
            return;
        }

        if (getMovementStatus(dir, tiles, attacking_on) == 4) {
            applyMovement(dir);

            return;
        } else {
            int nextDir = findClosestDirection(dir, nextLocation, attacking_on, tiles);

            if (nextDir != -1) {
                applyMovement(nextDir);
            } else {
                int peek = peek(getMob().getLocation(), tiles, attacking_on);

                if (peek != -1) {
                    applyMovement(peek);
                }
            }
        }
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
