package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.map.*;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/10/13
 * Time: 10:37
 */
public class SummoningMovement extends Movement {
    private Player master;

    /**
     * Constructs ...
     *
     */
    public SummoningMovement() {
        super(null);
    }

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param master
     */
    public SummoningMovement(NPC npc, Player master) {
        super(npc);
        this.master = master;
    }

    /**
     * Method find_safe_tele
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param master
     *
     * @return
     */
    public Point find_safe_tele(NPC npc, Player master) {
        for (int i = 0; i < 7; i++) {
            if (getMovementStatus(i, master.getLocation(), NPC.getTiles(npc, master.getLocation()), null) == 4) {
                return getPointForDir(master.getLocation(), i);
            }
        }

        return null;
    }

    /**
     * Method teleportToMaster
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void teleportToMaster() {
        Region r = World.getWorld().getRegionManager().getRegion(master.getLocation());
        getMob().setCurrentInstance(master.getCurrentInstance());
        if ((r != null) && (r.getAreas() != null)) {
            for (Zone e : r.getAreas()) {
                if (e == null) {
                    continue;
                }

                if (e.isFamiliarsBanned()) {
                    return;
                }
            }
        }

        for (int i = 0; i < 7; i++) {
            if (getMovementStatus(i, NPC.getTiles(getMob(), master.getLocation())) == 4) {
                Point p = getPointForDir(master.getLocation(), i);

                getMob().teleport(p.getX(), p.getY(), p.getHeight());
                getMob().setCurrentInstance(master.getCurrentInstance());

                return;
            }


        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {
        getMob().getFocus().focus(master);

        int     direction    = getDirectionForWaypoints(getMob().getLocation(), master.getKnownLocation());
        Point   nextLocation = getPointForDir(getMob().getLocation(), direction);
        Tile[]  ourTiles     = NPC.getTiles(getMob(), getMob().getLocation());
        boolean withinRange  = false;

        if(getMob().getTimers().timerActive(TimingUtility.FROZEN_TIME))
            return;


        if ((getMob().getControllerScript().getController() != null)
                && (getMob().getControllerScript().getController().getIdle() != null)) {
            NpcScriptScope scr = new NpcScriptScope(getMob(),
                                       getMob().getControllerScript().getController().getIdle());

            scr.setInteractingPlayer(master);
            NPCScriptEngine.evaluate(getMob(), scr);
        }
         if(getMob().getCurrentInstance() != master.getCurrentInstance()) {
              getMob().setCurrentInstance(master.getCurrentInstance());
         }

        if (Point.getDistance(master.getLocation(), getMob().getLocation()) >= 15) {
            teleportToMaster();

            return;
        }

        if (getMob().isWithinDistance(master, 1)) {
            return;
        }

        if (getMovementStatus(direction, ourTiles, master) == 4) {
            applyMovement(direction);
        } else {
            int nextDir = findClosestDirection(direction, getMob().getLocation(), master, ourTiles);

            if (nextDir != -1) {
                applyMovement(nextDir);
            } else {
                int peek = peek(getMob().getLocation(), ourTiles, master);

                if (peek != -1) {
                    applyMovement(peek);
                } else {}
            }
        }
    }

    /**
     * Method peek
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startLocation
     * @param ourTiles
     * @param movingTo
     *
     * @return
     */
    public int peek(Point startLocation, Tile[] ourTiles, Mob movingTo) {
        int currentDistance   = Point.getDistance(startLocation, movingTo.getLocation());
        int routeFindAttempts = 1;

        for (int i = 0; i < routeFindAttempts; i++) {
            for (int k = 0; k < 8; k++) {
                if (getMovementStatus(k, ourTiles, master) == 4) {
                    Tile[] newTiles = NPC.getTiles(getMob(), getPointForDir(getMob().getLocation(), k));

                    for (int i2 = 0; i2 < 8; i2++) {
                        if (getMovementStatus(i2, newTiles, master) == 4) {
                            Point p = getPointForDir(getPointForDir(getMob().getLocation(), k), i2);

                            if (Point.getDistance(p, movingTo.getLocation()) < currentDistance) {
                                return k;
                            }
                        }
                    }
                }
            }
        }

        return -1;
    }

    /**
     * Method findClosestDirection
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param odlDirection
     * @param oldLocation
     * @param movingTo
     * @param ourTiles
     *
     * @return
     */
    public int findClosestDirection(int odlDirection, Point oldLocation, Mob movingTo, Tile[] ourTiles) {
        int lowestDistance = Point.getDistance(oldLocation, getMob().getLocation());
        int lowestDir      = -1;

        for (int i = 0; i < 8; i++) {
            if ((i != odlDirection) && (getMovementStatus(i, ourTiles, master) == 4)) {
                Point nextLoc  = getPointForDir(getMob().getLocation(), i);
                int   distance = Point.getDistance(nextLoc, movingTo.getLocation());

                if (distance < lowestDistance) {
                    lowestDistance = distance;
                    lowestDir      = i;
                }
            }
        }

        return lowestDir;
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
