package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.hunter.HuntingController;
import net.tazogaming.hydra.game.skill.hunter.Trap;
import net.tazogaming.hydra.game.skill.hunter.TrapStatus;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/11/13
 * Time: 22:57
 */
public class HuntingMovement extends Movement {
    private int  movingTime = 0;
    private Trap moving_to;

    /**
     * Constructs ...
     *
     *
     * @param npc
     */
    public HuntingMovement(NPC npc) {
        super(npc);
    }

    private void roam() {

        if(HUNT_DEBUG){
            getMob().setTextUpdate("Roaming");
        }
        NPC npc = getMob();

        if (npc.getRandom().nextInt(100) > 50) {
            int   moveX       = -1 + npc.getRandom().nextInt(3);
            int   moveY       = -1 + npc.getRandom().nextInt(3);
            Point newLocation = Point.location(npc.getLocation().getX() + moveX, npc.getLocation().getY() + moveY, 0);
            int   dir         = getDirectionForWaypoints(npc.getLocation(), newLocation);

            if (getMovementStatus(dir, NPC.getTiles(npc, npc.getLocation())) != 4) {
                return;
            }

            if (Point.getDistance(newLocation,
                                  Point.location(npc.getSpawnData().getSpawnX(), npc.getSpawnData().getSpawnY(),
                                      npc.getSpawnData().getSpawnHeight())) > npc.getSpawnData().getRoamRange()) {
                return;
            }

            applyMovement(dir);
        }
    }

    private void reset() {

        moving_to.setTarget(null);
        moving_to = null;
        movingTime = 0;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */

    public static  boolean HUNT_DEBUG = false;
    @Override
    public void tick(int currentTime) {
        if ((movingTime != 0) && (moving_to != null) && (Core.timeSince(movingTime) > Core.getTicksForSeconds(25))) {
            reset();

            return;
        }

        if(HUNT_DEBUG){
            getMob().setTextUpdate("Roaming:");
        }

        if (moving_to == null) {
            ArrayList<Trap> traps_in_view = HuntingController.getTrapsInView(getMob());

            if ((traps_in_view == null) ||!getMob().isVisible()) {
                roam();

                return;
            }

            int rand = getMob().getRandom().nextInt(150);

            for (Trap trap : traps_in_view) {
                if (rand > 2 + (trap.getPlayer().getCurStat(21) / 8)) {
                    continue;
                }

                moving_to  = trap;
                movingTime = Core.currentTime;
                trap.setTarget(getMob());

                return;
            }

            if (moving_to == null) {
                roam();
            }
        } else {

            if(HUNT_DEBUG){
                getMob().setTextUpdate("Moving towards: "+moving_to.getLocation());
            }
            if (Point.getDistance(moving_to.getLocation(), getMob().getLocation()) == 0) {
                if (Trap.getHunterFail(getMob().getHuntEntity().getLevel(), getMob().getHuntEntity().getAreaType(),
                                       moving_to.getPlayer()) && Core.currentTime % 3 == 0) {
                    if (Trap.getHunterBonus(moving_to.getPlayer(), getMob().getHuntEntity().getAreaType()) == 0) {
                        moving_to.fall();
                        moving_to.getPlayer().getActionSender().sendMessage(
                            "You are not wearing any hunting gear. This will reduce your chances of getting a catch.");
                        reset();

                        return;
                    }
                }

                moving_to.snag();
                moving_to = null;

                return;
            }

            if (moving_to.getCurrentStatus() != TrapStatus.STATUS_SETUP) {
                reset();

                return;
            }

            int dir = getDirectionForWaypoints(getMob().getLocation(), moving_to.getLocation());

            if (getMovementStatus(dir, NPC.getTiles(getMob(), getMob().getLocation())) == 4) {
                applyMovement(dir);
            }    else{
                if(HUNT_DEBUG){
                    getMob().setTextUpdate("Moving towards [cannot move]: "+moving_to.getLocation());
                    reset();
                }
            }
        }
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
