package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 22:32
 */
public class RetreatMovement extends Movement {
    private NPC coward;
    private Mob retreatingFrom;

    /**
     * Constructs ...
     *
     *
     * @param coward
     * @param retreatingFrom
     */
    public RetreatMovement(NPC coward, Mob retreatingFrom) {
        super(coward);
        this.retreatingFrom = retreatingFrom;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
