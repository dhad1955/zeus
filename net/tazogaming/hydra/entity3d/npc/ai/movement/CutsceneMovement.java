package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.cutscene.CutScene;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/10/13
 * Time: 08:47
 */
public class CutsceneMovement extends Movement {
    private CutScene currCutScene;
    private Point    currentDestination;
    private Scope    wakeupScript;

    /**
     * Constructs ...
     *
     *
     * @param n
     * @param currentCutscene
     */
    public CutsceneMovement(NPC n, CutScene currentCutscene) {
        super(n);
        this.currCutScene = currentCutscene;
    }

    /**
     * Method walk_to
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dest
     * @param cutsceneScript
     */
    public void walk_to(Point dest, Scope cutsceneScript) {
        this.wakeupScript       = cutsceneScript;
        this.currentDestination = dest;
        this.getMob().getRoute().addPoint(dest);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {
        if (!getMob().getRoute().isIdle()) {
            getMob().getRoute().updatePosition();
        } else if ((wakeupScript != null) && (currentDestination != null)) {
            if ((getMob().getLocation().getX() == currentDestination.getX())
                    && (getMob().getLocation().getY() == currentDestination.getY())) {
                currentDestination = null;

                if (wakeupScript != null) {
                    wakeupScript.release(Scope.WAIT_TYPE_CS_NPC_MOVE);
                }
            }
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
