package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.DynamicClippingMap;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;
import net.tazogaming.hydra.game.skill.construction.House;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 22:46
 * To change this template use File | Settings | File Templates.
 */
public abstract class Movement {

    /*
     * 2 Modes, the path mode will use a path handler and
     * the realtime mode will use standard sprite updating
     */
    public static final int
        PATH_MODE     = 0,
        REALTIME_MODE = 1;

    /*
     * Movement directions
     */
    public final static int
        NORTH                        = 0,
        SOUTH                        = 1,
        EAST                         = 2,
        WEST                         = 3,
        NORTH_EAST                   = 4,
        NORTH_WEST                   = 5,
        SOUTH_EAST                   = 6,
        SOUTH_WEST                   = 7;
    static boolean          bx       = false;
    public static final int FACING[] = new int[8];

    static {
        FACING[NORTH]      = 8192;
        FACING[SOUTH]      = 0;
        FACING[WEST]       = 4096;
        FACING[EAST]       = 12288;
        FACING[SOUTH_WEST] = 2048;
        FACING[NORTH_WEST] = 6144;
        FACING[NORTH_EAST] = 10240;
        FACING[SOUTH_EAST] = 14336;
    }

    /*
     * Npc to move
     */
    private NPC    npc;
    private Player following;

    /*
     * Reference to path handler for path handling mode.
     */
    private Route route;

    /**
     * Constructs ...
     *
     *
     * @param n
     */
    public Movement(NPC n) {
        this.npc = n;
    }

    protected void setInteracting(Player p) {
        following = p;
    }

    /**
     * Method getInteracting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getInteracting() {
        return following;
    }

    /**
     * Method getTeleLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param p
     *
     * @return
     */
    public static Point getTeleLocation(NPC npc, Point p) {
        Point p2;

        for (int i = 0; i < 7; i++) {
            if(IfClipped.getMovementStatus(i, p.getX(), p.getY(), p.getHeight()) != 1)
                continue;

            p2 = getPointForDir(p, i);

            if (!World.getWorld().getTile(p2).blocked(npc)) {
                return p2;
            }

        }

        return null;
    }


    public static int traverseDirection(Point startPoint, Point endPoint){
        int direction = Movement.getDirectionForWaypoints(startPoint, endPoint);

        int moveDir = -1;
        switch (direction){
            case NORTH:
                moveDir = SOUTH;
            break;
            case SOUTH:
                moveDir = NORTH;
            break;
            case WEST:
                moveDir = EAST;
            break;
            case EAST:
                moveDir = WEST;
            break;
            case NORTH_EAST:
                moveDir = SOUTH_EAST;
            break;
            case NORTH_WEST:
                moveDir = SOUTH_WEST;
            break;
            case SOUTH_WEST:
                moveDir = NORTH_WEST;
            break;

        }
        return moveDir;
    }


    /**
     * Method getTeleLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param master
     *
     * @return
     */
    public static Point getTeleLocation(NPC npc, Player master) {
        for (int i = 0; i < 7; i++) {
            if (canMoveInDir(npc, i, master.getLocation(), NPC.getTiles(npc, master.getLocation()), null)) {
                return getPointForDir(master.getLocation(), i);
            }
        }

        return null;
    }

    /*
     * Process the movement and make the necessary calculations
     */

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    public abstract void tick(int currentTime);

    /*
     * Post movement notification.
     */

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    public abstract void mobMoved(Point newLocation, int direction);

    /*
     * Notification incase there was an error with movement.
     */

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    public abstract void exceptionCaught(Exception ee);

    /*
     * Returns the mob associated with this handler.
     */

    /**
     * Method getMob
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC getMob() {
        return npc;
    }

    /**
     * Method applyMovement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param direction
     */
    public void applyMovement(int direction) {
        Point p = getPointForDir(npc.getLocation(), direction);

        npc.setLocation(p, false);    // update sprite
        mobMoved(p, direction);
    }

    private static int getMove(int Place1, int Place2) {
        if ((Place1 - Place2) == 0) {
            return 0;
        } else if ((Place1 - Place2) < 0) {
            return 1;
        } else if ((Place1 - Place2) > 0) {
            return -1;
        }

        return 0;
    }

    /*
     *  public Point getFollowDestination(Mob m){
     *
     *    int smallMobSize = 1;
     *    if(m instanceof NPC)
     *        smallMobSize = ((NPC) m).getSize();
     *
     *    int distanceFromTarget = -1;
     *
     *    int distanceRequired = (getMob().getSize() /2) + (smallMobSize / 2);
     *
     *    Point[] possibleLocations = new Point[8];
     *
     *    possibleLocations[NORTH] = Point.location(m.getX(),
     *            m.getY() + distanceRequired, m.getLocation()
     *    .getHeight());
     *
     *    possibleLocations[NORTH_EAST] = Point.location(m.getX() + distanceRequired,
     *            m.getY() + distanceRequired, m.getLocation()
     *            .getHeight());
     *
     *    possibleLocations[NORTH_WEST] = Point.location(m.getX() - distanceRequired,
     *            m.getY() + distanceRequired, m.getLocation()
     *            .getHeight());
     *
     *    possibleLocations[SOUTH] = Point.location(m.getX(),
     *            m.getY() - distanceRequired, m.getLocation()
     *            .getHeight());
     *
     *    possibleLocations[SOUTH_EAST] = Point.location(m.getX() + distanceRequired,
     *            m.getY() - distanceRequired, m.getLocation()
     *            .getHeight());
     *
     *    possibleLocations[SOUTH_WEST] = Point.location(m.getX() - distanceRequired,
     *            m.getY() - distanceRequired, m.getLocation()
     *            .getHeight());
     *
     *    possibleLocations[EAST] = Point.location(m.getX() + distanceRequired,
     *            m.getLocation().getY(), m.getLocation()
     *            .getHeight());
     *
     *    possibleLocations[WEST] = Point.location(m.getX() - distanceRequired,
     *            m.getY(), m.getLocation()
     *            .getHeight());
     *
     *   int smallestDistance = 0;
     *   Point smallestPoint = null;
     *   for(int i = 0; i < possibleLocations.length){
     *       if(smallestPoint == null || (Point.getDistance(possibleLocations[i], getMob().
     *               getLocation()) < Point.getDistance(getMob().
     *               getLocation(), smallestPoint))){
     *           smallestPoint = possibleLocations[i];
     *   }
     *
     *   }
     *    Tile[] tiles = NPC.getTiles(getMob(), smallestPoint);
     *    for(Tile t : tiles){
     *        if(ClippingDecoder.get)
     *    }
     *
     * }
     */

    /**
     * Method getFollowDestination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param m
     *
     * @return
     */
    public Point getFollowDestination(Mob m) {
        Tile[] tiles = NPC.getTiles(npc, m.getLocation());

        for (int i = 0; i < 7; i++) {
            if (getMovementStatus(i, tiles) == 4) {
                return getPointForDir(m.getLocation(), i);
            }
        }

        return null;
    }

    /**
     * Method getMovementStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param direction
     * @param currentTiles
     *
     * @return
     */
    public int getMovementStatus(int direction, Tile[] currentTiles) {
        return getMovementStatus(direction, currentTiles, null);
    }

    /**
     * Method canMoveInDir
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param dir
     * @param startLocation
     * @param myTiles
     * @param dodging
     *
     * @return
     */
    public static boolean canMoveInDir(NPC npc, int dir, Point startLocation, Tile[] myTiles, Mob dodging) {
        for (Tile t : myTiles) {
            if (dodging instanceof NPC) {
                int siz = ((NPC) dodging).getNpc().getSize() - 1;

                if ((t.getX() >= dodging.getX()) && (t.getX() <= dodging.getX() + siz) && (t.getY() >= dodging.getY())
                        && (t.getY() <= dodging.getY() + siz)) {
                    return false;
                }
            }

            if (t.blocked(npc)) {
                return false;
            }

            if ((dodging != null) && (t.getX() == dodging.getX()) && (t.getY() == dodging.getY())) {
                return false;
            }
        }

        for (Tile t : myTiles) {
            int tileX  = t.getX();
            int tileY  = t.getY();
            int height = startLocation.getHeight();

            switch (dir) {
            case NORTH :
                if (ClippingDecoder.blockedNorth(tileX, tileY, height)) {
                    return false;
                }

                break;

            case SOUTH :
                if (ClippingDecoder.blockedSouth(tileX, tileY, height)) {
                    return false;
                }

                break;

            case EAST :
                if (ClippingDecoder.blockedEast(tileX, tileY, height)) {
                    return false;
                }

                break;

            case WEST :
                if (ClippingDecoder.blockedWest(tileX, tileY, height)) {
                    return false;
                }

                break;

            case NORTH_EAST :
                if (ClippingDecoder.blockedNorthEast(tileX, tileY, height)) {
                    return false;
                }

                break;

            case NORTH_WEST :
                if (ClippingDecoder.blockedNorthWest(tileX, tileY, height)) {
                    return false;
                }

                break;

            case SOUTH_WEST :
                if (ClippingDecoder.blockedSouthWest(tileX, tileY, height)) {
                    return false;
                }

                break;

            case SOUTH_EAST :
                if (ClippingDecoder.blockedSouthEast(tileX, tileY, height)) {
                    return false;
                }

                break;
            }
        }

        return true;
    }

    private boolean isHouse() {
        return getMob().getCurrentInstance() instanceof House;
    }

    /**
     * Method getMovementStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param direction
     * @param startLocation
     * @param currentTiles
     * @param dodging
     *
     * @return
     */
    public int getMovementStatus(int direction, Point startLocation, Tile[] currentTiles, Mob dodging) {
        int mx = 0,
            my = 0;

        if ((direction == NORTH) || (direction == NORTH_EAST) || (direction == NORTH_WEST)) {
            my++;
        }

        if ((direction == SOUTH) || (direction == SOUTH_EAST) || (direction == SOUTH_WEST)) {
            my--;
        }

        if ((direction == EAST) || (direction == NORTH_EAST) || (direction == SOUTH_EAST)) {
            mx++;
        }

        if ((direction == WEST) || (direction == SOUTH_WEST) || (direction == NORTH_WEST)) {
            mx--;
        }

        Tile[] myTiles = NPC.getTiles(npc, getPointForDir(startLocation, direction));

        for (Tile t : myTiles) {
            if (dodging instanceof NPC) {
                int siz = ((NPC) dodging).getNpc().getSize() - 1;

                if ((t.getX() >= dodging.getX()) && (t.getX() <= dodging.getX() + siz) && (t.getY() >= dodging.getY())
                        && (t.getY() <= dodging.getY() + siz)) {
                    return 1;
                }
            }

            if (t.blocked(npc)) {
                return 1;
            }

            if ((dodging != null) && (t.getX() == dodging.getX()) && (t.getY() == dodging.getY())) {
                return 1;
            }
        }

        DynamicClippingMap map = null;

        if (isHouse()) {
            map = ((House) getMob().getCurrentInstance()).getClippingMap();
        }

        int localX = -1;    // House.getLocalX(getMob().getLocation().getY());
        int localY = -1;    // House.getLocalY(getMob().getLocation().getX());

        for (Tile t : currentTiles) {
            int tileX = t.getX();
            int tileY = t.getY();

            localX = House.getLocalX(t.getX());
            localY = House.getLocalY(t.getY());

            int height = startLocation.getHeight();

            switch (direction) {
            case NORTH :
                if (isHouse()) {
                    if (map.blockedNorth(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedNorth(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case SOUTH :
                if (isHouse()) {
                    if (map.blockedSouth(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedSouth(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case EAST :
                if (isHouse()) {
                    if (map.blockedEast(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedEast(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case WEST :
                if (isHouse()) {
                    if (map.blockedWest(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedWest(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case NORTH_EAST :
                if (isHouse()) {
                    if (map.blockedNorthEast(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedNorthEast(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case NORTH_WEST :
                if (isHouse()) {
                    if (map.blockedNorthWest(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedNorthWest(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case SOUTH_WEST :
                if (isHouse()) {
                    if (map.blockedSouthWest(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedSouthWest(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case SOUTH_EAST :
                if (isHouse()) {
                    if (map.blockedSouthEast(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedSouthEast(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;
            }
        }

        return 4;
    }

    /**
     * Method getMovementStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param direction
     * @param currentTiles
     * @param dodging
     *
     * @return
     */
    public int getMovementStatus(int direction, Tile[] currentTiles, Mob dodging) {
        int mx = 0,
            my = 0;

        if ((direction == NORTH) || (direction == NORTH_EAST) || (direction == NORTH_WEST)) {
            my++;
        }

        if ((direction == SOUTH) || (direction == SOUTH_EAST) || (direction == SOUTH_WEST)) {
            my--;
        }

        if ((direction == EAST) || (direction == NORTH_EAST) || (direction == SOUTH_EAST)) {
            mx++;
        }

        if ((direction == WEST) || (direction == SOUTH_WEST) || (direction == NORTH_WEST)) {
            mx--;
        }

        Tile[] myTiles = NPC.getTiles(npc, getPointForDir(npc.getLocation(), direction));

        for (Tile t : myTiles) {
            if (dodging instanceof NPC) {
                int siz = ((NPC) dodging).getNpc().getSize() - 1;

                if ((t.getX() >= dodging.getX()) && (t.getX() <= dodging.getX() + siz) && (t.getY() >= dodging.getY())
                        && (t.getY() <= dodging.getY() + siz)) {
                    return 1;
                }
            }

            if (t.blocked(npc)) {
                return 1;
            }

            if ((dodging != null) && (t.getX() == dodging.getX()) && (t.getY() == dodging.getY())) {
                return 1;
            }
        }

        DynamicClippingMap map = null;

        if (isHouse()) {
            map = ((House) getMob().getCurrentInstance()).getClippingMap();
        }

        int localX = -1;
        int localY = -1;

        for (Tile t : currentTiles) {
            int tileX  = t.getX();
            int tileY  = t.getY();
            int height = npc.getHeight();

            localX = House.getLocalX(tileX);
            localY = House.getLocalY(tileY);

            if (((localX > 104) || (localY > 104)) && isHouse()) {
                npc.unlink();

                return 1;
            }

            switch (direction) {
            case NORTH :
                if (isHouse()) {
                    if (map.blockedNorth(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedNorth(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case SOUTH :
                if (isHouse()) {
                    if (map.blockedSouth(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedSouth(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case EAST :
                if (isHouse()) {
                    if (map.blockedEast(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedEast(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case WEST :
                if (isHouse()) {
                    if (map.blockedWest(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedWest(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case NORTH_EAST :
                if (isHouse()) {
                    if (map.blockedNorthEast(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedNorthEast(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case NORTH_WEST :
                if (isHouse()) {
                    if (map.blockedNorthWest(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedNorthWest(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case SOUTH_WEST :
                if (isHouse()) {
                    if (map.blockedSouthWest(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedSouthWest(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;

            case SOUTH_EAST :
                if (isHouse()) {
                    if (map.blockedSouthEast(localX, localY, height)) {
                        return 0;
                    }
                } else {
                    if (ClippingDecoder.blockedSouthEast(tileX, tileY, height)) {
                        return 0;
                    }
                }

                break;
            }
        }

        return 4;
    }

    /**
     * Method getDirectionForWaypoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param start
     * @param end
     *
     * @return
     */
    public static int getDirectionForWaypoints(Point start, Point end) {
        int mx        = 0,
            my        = 0;
        int direction = 0;
        int playerX   = end.getX();
        int playerY   = end.getY();

        if (playerY > start.getY()) {
            my = 1;
        }

        if (playerY < start.getY()) {
            my = -1;
        }

        if (playerX > start.getX()) {
            mx = 1;
        }

        if (playerX < start.getX()) {
            mx = -1;
        }

        if ((my == 1) && (mx == 0)) {
            direction = NORTH;
        }

        if ((my == -1) && (mx == 0)) {
            direction = SOUTH;
        }

        if ((mx == -1) && (my == 0)) {
            direction = WEST;
        }

        if ((mx == 1) && (my == 0)) {
            direction = EAST;
        }

        if ((mx == -1) && (my == -1)) {
            direction = SOUTH_WEST;
        }

        if ((mx == 1) && (my == -1)) {
            direction = SOUTH_EAST;
        }

        if ((mx == -1) && (my == 1)) {
            direction = NORTH_WEST;
        }

        if ((mx == 1) && (my == 1)) {
            direction = NORTH_EAST;
        }

        return direction;
    }

    /**
     * Method getPointForDir
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startDir
     * @param direction
     * @param siz
     *
     * @return
     */
    public static Point getPointForDir(Point startDir, int direction, int siz) {
        int x = startDir.getX();
        int y = startDir.getY();
        int h = startDir.getHeight();

        switch (direction) {
        case NORTH :
            return Point.location(x, y + siz, h);

        case SOUTH :
            return Point.location(x, y - siz, h);

        case EAST :
            return Point.location(x + siz, y, h);

        case WEST :
            return Point.location(x - siz, y, h);

        case NORTH_EAST :
            return Point.location(x + siz, y + siz, h);

        case NORTH_WEST :
            return Point.location(x - siz, y + siz, h);

        case SOUTH_EAST :
            return Point.location(x + siz, y - siz, h);

        case SOUTH_WEST :
            return Point.location(x - siz, y - siz, h);
        }

        return null;
    }

    /**
     * Method getPointForDir
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startDir
     * @param direction
     *
     * @return
     */
    public static Point getPointForDir(Point startDir, int direction) {
        int x = startDir.getX();
        int y = startDir.getY();
        int h = startDir.getHeight();

        switch (direction) {
        case NORTH :
            return Point.location(x, y + 1, h);

        case SOUTH :
            return Point.location(x, y - 1, h);

        case EAST :
            return Point.location(x + 1, y, h);

        case WEST :
            return Point.location(x - 1, y, h);

        case NORTH_EAST :
            return Point.location(x + 1, y + 1, h);

        case NORTH_WEST :
            return Point.location(x - 1, y + 1, h);

        case SOUTH_EAST :
            return Point.location(x + 1, y - 1, h);

        case SOUTH_WEST :
            return Point.location(x - 1, y - 1, h);
        }

        return null;
    }

    /**
     * Method peek
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startLocation
     * @param ourTiles
     * @param movingTo
     *
     * @return
     */
    public int peek(Point startLocation, Tile[] ourTiles, Mob movingTo) {
        int currentDistance   = Point.getDistance(startLocation, movingTo.getLocation());
        int routeFindAttempts = 1;

        for (int i = 0; i < routeFindAttempts; i++) {
            for (int k = 0; k < 7; k++) {
                if (getMovementStatus(k, ourTiles) == 4) {
                    Tile[] newTiles = NPC.getTiles(getMob(), getPointForDir(getMob().getLocation(), k));

                    for (int i2 = 0; i2 < 7; i2++) {
                        if (getMovementStatus(i2, newTiles) == 4) {
                            Point p = getPointForDir(getPointForDir(getMob().getLocation(), k), i2);

                            if (Point.getDistance(p, movingTo.getLocation()) < currentDistance) {
                                return k;
                            }
                        }
                    }
                }
            }
        }

        return -1;
    }

    /**
     * Method findClosestDirection
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param odlDirection
     * @param oldLocation
     * @param movingTo
     * @param ourTiles
     *
     * @return
     */
    public int findClosestDirection(int odlDirection, Point oldLocation, Mob movingTo, Tile[] ourTiles) {
        int lowestDistance = Point.getDistance(oldLocation, getMob().getLocation());
        int lowestDir      = -1;

        for (int i = 0; i < 7; i++) {
            if ((i != odlDirection) && (getMovementStatus(i, ourTiles) == 4)) {
                Point nextLoc  = getPointForDir(getMob().getLocation(), i);
                int   distance = Point.getDistance(nextLoc, movingTo.getLocation());

                if (distance < lowestDistance) {
                    lowestDistance = distance;
                    lowestDir      = i;
                }
            }
        }

        return lowestDir;
    }
}
