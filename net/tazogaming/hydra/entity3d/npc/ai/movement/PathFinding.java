package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.background.BackgroundTaskCompleteListener;
import net.tazogaming.hydra.util.background.PathFindRequest;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 03:28
 */
public class PathFinding extends Movement implements BackgroundTaskCompleteListener {
    private boolean     awaiting_route = false;
    private Point       knownMobPoint  = null;
    private int         lastMovement   = Core.currentTime;
    private Route route_request;
    private Killable    following;

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param m
     */
    public PathFinding(NPC npc, Killable m) {
        super(npc);
        this.following = m;
        setInteracting((Player) following);
        getMob().getFocus().focus(following);
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        this.following = null;
        getMob().getFocus().unFocus();
        getMob().setMovement(new RoamingMovement(getMob()));
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {
        if (awaiting_route && (route_request == null)) {
            return;
        } else if (route_request != null) {
            getMob().setRoute(route_request);
            route_request = null;
        }

        if (following != null) {
            if ((following.getCurrentInstance() != getMob().getCurrentInstance()) ||!following.getPlayer().isLoggedIn()
                    || following.isDead()) {
                reset();

                return;
            }

            if (Combat.isWithinDistance(getMob(), following)) {
                getMob().getRoute().resetPath();
                getMob().getCombatHandler().attackOn(following);
                lastMovement = Core.currentTime;

                return;
            } else {
                if (Core.timeSince(lastMovement) > 50) {
                    lastMovement = Core.currentTime;

                    if (getMob().getControllerScript() != null) {
                        getMob().getControllerScript().do_trigger(Block.ON_PURSUE_GIVE_UP, following);
                    }

                    return;
                }
            }

            if ((knownMobPoint != null) && (knownMobPoint.getX() != following.getX())
                    && (knownMobPoint.getY() != following.getY())) {
                getMob().resetPath();
            }

            if (Combat.clearPath(getMob().getLocation(), following.getLocation())) {
                int dir = getDirectionForWaypoints(getMob().getLocation(), following.getLocation());

                if ((getMovementStatus(dir, NPC.getTiles(getMob(), getMob().getLocation())) == 4)
                        &&!World.getWorld().getTile(getPointForDir(getMob().getLocation(), dir)).blocked(getMob())) {
                    getMob().resetPath();
                    applyMovement(dir);
                    lastMovement = Core.currentTime;

                    return;
                }
            }

            if (!getMob().getRoute().isIdle()) {
                if (!World.getWorld().getTile(getMob().getRoute().peek()).blocked(getMob())) {
                    getMob().getRoute().updatePosition();
                    lastMovement = Core.currentTime;
                } else {
                    getMob().getRoute().resetPath();
                }
            } else {
                getMob().getControllerScript().do_trigger(Block.TRIGGER_PATH_BLOCKED, following);
                new_route(following);
            }
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    private void new_route(Mob n) {
        getMob().blockInteractions();
        knownMobPoint = n.getLocation();

        PathFindRequest request = new PathFindRequest(this, getMob(),
                                      Movement.getTeleLocation(getMob(), (Player) following), false);

        World.getWorld().getBackgroundService().submitRequest(request);
        awaiting_route = true;
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method requestComplete
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param request_result
     */
    @Override
    public void requestComplete(Object request_result) {
        route_request = (Route) request_result;
        getMob().setBlock_interactions(false);
        awaiting_route = false;

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
