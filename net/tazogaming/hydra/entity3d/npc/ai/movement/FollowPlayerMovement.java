package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.runtime.Core;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 16:10
 * To change this template use File | Settings | File Templates.
 */
public class FollowPlayerMovement extends Movement {
    private boolean isAggressive        = false;
    private Point   knownPlayerLocation = null;
    private int     blockedTime         = -1;
    private Player  following;
    private Point   lastMoved;

    /**
     * Constructs ...
     *
     *
     * @param npc
     * @param following
     * @param aggressive
     */
    public FollowPlayerMovement(NPC npc, Player following, boolean aggressive) {
        super(npc);

        if (following == null) {
            throw new NullPointerException();
        }

        this.following    = following;
        this.isAggressive = aggressive;
    }

    /**
     * Method getFollowing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getFollowing() {
        return following;
    }

    private Point getCenterPoint(Point location) {
        return Point.location(location.getX() + getMob().getSize() / 2, location.getY() + getMob().getSize() / 2,
                              getMob().getHeight());
    }

    /**
     * Method moveFrom
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param following
     * @param tiles
     */
    public void moveFrom(Player following, Tile[] tiles) {
        int lowestDis = -1;
        int lowestDir = -1;

        for (int i = 0; i < 7; i++) {
            Point movingTo = getPointForDir(getMob().getLocation(), i);

            if ((lastMoved == null)
                    || ((movingTo.getX() != lastMoved.getX()) && (movingTo.getY() != lastMoved.getY()))) {
                if (((lowestDis == -1)
                        || (Point.getDistance(getCenterPoint(movingTo), following.getLocation())
                            > lowestDis)) && (getMovementStatus(i, tiles) == 4)) {
                    lowestDis = Point.getDistance(getCenterPoint(movingTo), following.getLocation());
                    lastMoved = getMob().getLocation();
                    lowestDir = i;
                }
            }
        }

        if (lowestDir != -1) {
            applyMovement(lowestDir);
        }
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        getMob().getCombatHandler().reset();
        getMob().getFocus().unFocus();

        if ((getMob().getControllerScript() != null) && (getMob().getControllerScript().getController() != null)) {
            Block block = getMob().getControllerScript().getController().getTrigger(Block.TRIGGER_TARGET_OUT_OF_RANGE);

            if ((block != null) &&!getMob().isDead()) {
                NpcScriptScope script = new NpcScriptScope(getMob(), block);

                script.setInteractingPlayer(following);
                NPCScriptEngine.evaluate(getMob(), script);
            }
        }

        blockedTime = -1;
        getMob().setMovement(new RoamingMovement(getMob()));
    }






    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {
        if (knownPlayerLocation == null) {
            knownPlayerLocation = following.getLocation();
        }

        getMob().getFocus().focus(following);

        int     direction    = getDirectionForWaypoints(getMob().getLocation(), following.getKnownLocation());
        Point   nextLocation = getPointForDir(getMob().getLocation(), direction);
        Tile[]  ourTiles     = NPC.getTiles(getMob(), getMob().getLocation());
        boolean withinRange  = false;

        if ((getMob().getCurrentInstance() != following.getCurrentInstance())
                || ((Point.getDistance(following.getLocation(), getMob().getLocation()) >= 15)
                    && (getMob().getCaves() == null)) || following.isDead() ||!following.isLoggedIn()
                        || getMob().isDead() || (getMob().getHeight() != following.getHeight())
                        || (getMob().getCurrentInstance() != following.getCurrentInstance())) {
            reset();

            return;
        }

        /*
         *        for(Tile t: ourTiles) {
         *          following.getActionSender().sendStillGFX(2001, t.getX(), t.getY());
         *      }
         */
        boolean hasMoved = false;

        if (getMob().isWithinTileRange(following.getLocation())) {
            moveFrom(following, ourTiles);

            // return;
            hasMoved = true;
        }

        if (getMob().isWithinDistance(following, 1)) {
            withinRange = true;
        }

        if (isAggressive) {
            if (Combat.isWithinDistance(getMob(), following)) {
                getMob().getCombatHandler().attackOn(following);
                blockedTime = -1;

                return;
            } else {
                if (following.isDead() ||!following.canBeAttacked(getMob())
                        || (Point.getDistance(following.getLocation(), getMob().getLocation())
                            >= (getMob().getSize() / 2) + 15) && (getMob().getCaves() == null)) {
                    reset();
                    blockedTime = -1;

                    return;
                }
            }
        }

        if (hasMoved) {
            return;
        }

        if (!getMob().canGo(nextLocation)) {
            if (blockedTime == -1) {
                blockedTime = Core.currentTime;
            }

            if (Core.currentTime - blockedTime > 10) {
                reset();
                blockedTime = Core.currentTime;
            }

            return;
        }

        if (withinRange) {
            return;
        }

        if (getMovementStatus(direction, ourTiles) == 4) {
            applyMovement(direction);
        } else {

            // failed to move, so the only other alternative is to try and get closer
            int nextDir = findClosestDirection(direction, nextLocation, following, ourTiles);

            if (nextDir != -1) {
                applyMovement(nextDir);
                blockedTime = -1;
            } else {

                // final attempt
                int peek = peek(getMob().getLocation(), ourTiles, following);

                if (peek != -1) {
                    applyMovement(peek);
                    blockedTime = -1;
                } else {
                    if (blockedTime != -1) {
                        blockedTime = Core.currentTime;
                    }

                    if (Core.timeSince(blockedTime) > 20) {
                        if ((getMob().getControllerScript() != null)
                                && (getMob().getControllerScript().getController() != null)) {
                            getMob().getControllerScript().do_trigger(Block.ON_PURSUE_GIVE_UP, following);
                            blockedTime = -1;
                        }
                    }
                }
            }
        }
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
