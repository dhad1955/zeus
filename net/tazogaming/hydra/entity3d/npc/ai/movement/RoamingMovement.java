package net.tazogaming.hydra.entity3d.npc.ai.movement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 22:48
 * To change this template use File | Settings | File Templates.
 */
public class RoamingMovement extends Movement {
    private int   lastDir = -1;
    private Point lastPoint;

    /**
     * Constructs ...
     *
     *
     * @param n
     */
    public RoamingMovement(NPC n) {
        super(n);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTime
     */
    @Override
    public void tick(int currentTime) {
        NPC npc = getMob();
        if (npc.blockInteractions()) {
            if(HuntingMovement.HUNT_DEBUG){
                getMob().setTextUpdate("status:blocked");
            }
            return;
        }



        if ((npc.getKnownPlayersInRange() != null) && (npc.getKnownPlayersInRange().size() == 0)) {
            if(HuntingMovement.HUNT_DEBUG){
                getMob().setTextUpdate("no players in range lol");
            }
            return;
        }

        if (npc.isSummoned()) {
            npc.setMovement(new SummoningMovement(npc, npc.getSummoner()));

            return;
        }

        if (npc.getSpawnData() == null) {
            return;
        }

        if (npc.getSpawnData().getRoamRange() == 0) {
            return;
        }

        if (npc.isBusy(null)) {
            if(HuntingMovement.HUNT_DEBUG){
                getMob().setTextUpdate("status:busy: "+npc.isBusy()+" "+(npc.getCurrentController() == null));
            }
            return;
        }

        if (npc.getRandom().nextInt(100) > npc.getRoamSpeed()) {
            Random r           = new Random();
            Point  newLocation = getPointForDir(getMob().getLocation(), GameMath.rand3(8));
            int    dir         = getDirectionForWaypoints(npc.getLocation(), newLocation);

            if ((lastPoint != null) && (lastPoint.getX() == newLocation.getX())
                    && (lastPoint.getY() == newLocation.getY())) {
                dir = (dir + new Random().nextInt(8)) & 7;
            }

            int status = getMovementStatus(dir, NPC.getTiles(npc, npc.getLocation()));
            if ((status == 0) &&!npc.isFlying()) {
                return;
            } else if ((status == 4) || npc.isFlying()) {
                if (Point.getDistance(newLocation,
                                      Point.location(npc.getSpawnData().getSpawnX(), npc.getSpawnData().getSpawnY(),
                                          npc.getSpawnData().getSpawnHeight())) >= npc.getSpawnData().getRoamRange()) {
                   if(HuntingMovement.HUNT_DEBUG && npc.isFlying())
                    getMob().setTextUpdate("outofrange "+dir+" "+npc.getSpawnData().getRoamRange()+" "+Point.getDistance(Point.location(npc.getSpawnData().getSpawnX(), npc.getSpawnData().getSpawnY(),
                            npc.getSpawnData().getSpawnHeight()), getMob().getLocation()));

                    return;
                }

                lastPoint    = npc.getLocation();
                this.lastDir = dir;
                applyMovement(dir);
            }
        }
    }

    /**
     * Method mobMoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newLocation
     * @param direction
     */
    @Override
    public void mobMoved(Point newLocation, int direction) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method exceptionCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    @Override
    public void exceptionCaught(Exception ee) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
