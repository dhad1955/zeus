package net.tazogaming.hydra.entity3d.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.game.LootTable;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 16:37
 */
public class NpcDef {
    public static NpcDef[]           NPC_CACHE       = new NpcDef[17000];
    private static ArrayList<NpcDef> list            = new ArrayList<NpcDef>();
    public static int                totalNpcs       = 0;
    private int                      blockAnim       = 414;
    private int                      attackAnim      = 404;
    private int                      deathAnim       = 2304;
    private int                      deathTime       = 7;
    private boolean                  isAttackable    = false;
    private boolean                  poisonImmune    = false;
    private int                      startingHealth  = 10;
    private int                      respawnTime     = 7;
    public int[]                     combatStats     = {
        1, 1, 1, 1, 1, 1, 1
    };
    public int[]                     bonuses         = new int[12];
    private int                      maxHit          = 2;
    private String                   controller      = "";
    private int[]                    bindedIds       = new int[1];
    private int                      size            = 1;
    private int                      charmDropAmount = 0;
    private String                   name;
    private int                      maxHitModifier;
    private int                      table;
    private int                      cooldown;

    /**
     * Constructs ...
     *
     *
     * @param id
     */
    public NpcDef(int id) {
        this.bindedIds = new int[] { id };
    }

    /**
     * Constructs ...
     *
     *
     * @param clone
     */
    public NpcDef(NpcDef clone) {
        setName(clone.getName());
        setStartingHealth(clone.getStartingHealth());
        setRespawnTime(clone.getRespawnTime());
        setController(clone.getController());
        setDeathTime(clone.getDeathTime());
        setAttackAnim(clone.getAttackAnim());
        setBlockAnim(clone.getBlockAnim());
        setDeathAnim(clone.getDeathAnim());
        size = clone.size;
        setMaxHit(clone.getMaxHit());
        setCooldown(clone.getCooldown());
        setBonuses(Arrays.copyOf(clone.getBonuses(), clone.getBonuses().length));
        setCombatStats(Arrays.copyOf(clone.getCombatStats(), clone.getCombatStats().length));
        setAttackable(clone.isAttackable);
        setDrops(clone.getLoot());
        setCharmDropAmount(clone.getCharmDropAmount());
    }

    /**
     * Constructs ...
     *
     *
     * @param result
     */
    public NpcDef(ResultSet result) {
        try {
            this.setName(result.getString("name"));
            this.setAttackable(result.getInt("killable") == 1);
            this.setBlockAnim(result.getInt("blockanim"));
            this.setDeathAnim(result.getInt("deathanim"));
            this.setDeathTime(result.getInt("death_time"));
            this.setAttackAnim(result.getInt("attackanim"));
            this.setMaxHit(result.getInt("maxhit"));
            this.setController(result.getString("script_name"));
            this.setStartingHealth(result.getInt("maxhp"));
            this.cooldown = result.getInt("cooldown_time");
            this.setCharmDropAmount(result.getInt("charm_drop_amt"));
            this.setPoisonImmune(result.getInt("poison_immune") == 1);
            this.setRespawnTime(result.getInt("respawn_time"));
            this.bindedIds[0] = result.getInt("npcid");
            this.size         = result.getInt("size");

            if (result.getInt("npcid") > NPC_CACHE.length) {
                return;
            }

            NPC_CACHE[result.getInt("npcid")] = this;

            if (result.getInt("bonusindex") != -1) {
                unpackBonuses(World.getWorld().getDatabaseLink().query("SELECT * FROM bonustable where id='"
                        + result.getInt("bonusindex") + "'"));
            }

            if (result.getInt("lootindex") != -1) {
                table = result.getInt("lootindex");
            }
        } catch (Exception ee) {
            System.err.println("error unpacking npc: "+this.bindedIds[0]);
            ee.printStackTrace();
        }
    }

    /**
     * Method getMaxHitModifier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaxHitModifier() {
        return maxHitModifier;
    }

    /**
     * Method setSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param size
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * Method setMaxHitModifier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param maxHitModifier
     */
    public void setMaxHitModifier(int maxHitModifier) {
        this.maxHitModifier = maxHitModifier;
    }

    /**
     * Method getCharmDropAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCharmDropAmount() {
        return charmDropAmount;
    }

    /**
     * Method setCharmDropAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param charmDropAmount
     */
    public void setCharmDropAmount(int charmDropAmount) {
        this.charmDropAmount = charmDropAmount;
    }

    /**
     * Method getCooldown
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCooldown() {
        return cooldown;
    }

    /**
     * Method setCooldown
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cooldown
     */
    public void setCooldown(int cooldown) {
        this.cooldown = cooldown;
    }    // 2527

    /**
     * Method FOR_ID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static NpcDef FOR_ID(int id) {
        if (NPC_CACHE[id] == null) {
            return new NpcDef(id);
        }

        return NPC_CACHE[id];
    }

    /**
     * Method getTableIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTableIndex() {
        return table;
    }

    /**
     * Method hasLoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasLoot() {
        return (table > 0) && (LootTable.getTable(table) != null);
    }

    /**
     * Method getLoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LootTable getLoot() {
        if (!hasLoot()) {
            return null;
        }

        return LootTable.getTable(table);
    }

    /**
     * Method setDrops
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param table
     */
    public void setDrops(LootTable table) {

        // this.table = table;
    }

    /**
     * Method unpackBonuses
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param set
     *
     * @throws SQLException
     */
    public void unpackBonuses(ResultSet set) throws SQLException {
        if (set.next()) {
            String[] splitz = set.getString("data").split("\n");
            int      i      = 0;

            while (i < splitz.length - 1) {

                // ;
                String line = splitz[i].trim();    // tokenizer.nextToken();

                i++;

                if (line.startsWith("//") || (line.length() == 0)) {
                    continue;
                }

                if (line.length() < 4) {
                    continue;
                }

                String[] split = line.split(" ");

                split[2] = split[2].replaceAll(" ", "").trim();

                if (split[0].equalsIgnoreCase("attack-level")) {
                    combatStats[0] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("defence-level")) {
                    combatStats[1] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("range-level")) {
                    combatStats[4] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("magic-level")) {
                    combatStats[6] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("attack-range")) {
                    bonuses[Item.BONUS_ATTACK_RANGE] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("attack-stab")) {
                    bonuses[Item.BONUS_ATTACK_STAB] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("attack-slash")) {
                    bonuses[Item.BONUS_ATTACK_SLASH] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("attack-crush")) {
                    bonuses[Item.BONUS_ATTACK_CRUSH] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("attack-magic")) {
                    bonuses[Item.BONUS_ATTACK_MAGIC] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("defence-range")) {
                    bonuses[Item.BONUS_DEFENCE_RANGE] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("defence-stab")) {
                    bonuses[Item.BONUS_DEFENCE_STAB] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("defence-slash")) {
                    bonuses[Item.BONUS_DEFENCE_SLASH] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("defence-crush")) {
                    bonuses[Item.BONUS_DEFENCE_CRUSH] = Integer.parseInt(split[2]);
                }

                if (split[0].equalsIgnoreCase("defence-magic")) {
                    bonuses[Item.BONUS_DEFENCE_MAGIC] = Integer.parseInt(split[2]);
                }
            }
        }
    }

    /**
     * Method getSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSize() {
        return size;
    }

    /**
     * Method getBindedIds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getBindedIds() {
        return bindedIds;
    }

    /**
     * Method setBindedIds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bindedIds
     */
    public void setBindedIds(int[] bindedIds) {
        this.bindedIds = bindedIds;
    }

    /**
     * Method getBlockAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBlockAnim() {
        return blockAnim;
    }

    /**
     * Method setBlockAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param blockAnim
     */
    public void setBlockAnim(int blockAnim) {
        this.blockAnim = blockAnim;
    }

    /**
     * Method getAttackAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAttackAnim() {
        return attackAnim;
    }

    /**
     * Method setAttackAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param attackAnim
     */
    public void setAttackAnim(int attackAnim) {
        this.attackAnim = attackAnim;
    }

    /**
     * Method getDeathAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDeathAnim() {
        return deathAnim;
    }

    /**
     * Method setDeathAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param deathAnim
     */
    public void setDeathAnim(int deathAnim) {
        this.deathAnim = deathAnim;
    }

    /**
     * Method getDeathTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDeathTime() {
        return deathTime;
    }

    /**
     * Method setDeathTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param deathTime
     */
    public void setDeathTime(int deathTime) {
        this.deathTime = deathTime;
    }

    /**
     * Method isAttackable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isAttackable() {
        return isAttackable;
    }

    /**
     * Method setAttackable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param attackable
     */
    public void setAttackable(boolean attackable) {
        isAttackable = attackable;
    }

    /**
     * Method isPoisonImmune
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isPoisonImmune() {
        return poisonImmune;
    }

    /**
     * Method setPoisonImmune
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param poisonImmune
     */
    public void setPoisonImmune(boolean poisonImmune) {
        this.poisonImmune = poisonImmune;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method getStartingHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStartingHealth() {
        return startingHealth;
    }

    /**
     * Method setStartingHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startingHealth
     */
    public void setStartingHealth(int startingHealth) {
        this.startingHealth = startingHealth;
    }

    /**
     * Method getRespawnTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRespawnTime() {
        return respawnTime;
    }

    /**
     * Method setRespawnTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param respawnTime
     */
    public void setRespawnTime(int respawnTime) {
        this.respawnTime = respawnTime;
    }

    /**
     * Method getCombatStats
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getCombatStats() {
        return combatStats;
    }

    /**
     * Method setCombatStats
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param combatStats
     */
    public void setCombatStats(int[] combatStats) {
        this.combatStats = combatStats;
    }

    /**
     * Method getBonuses
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getBonuses() {
        return bonuses;
    }

    /**
     * Method setBonuses
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bonuses
     */
    public void setBonuses(int[] bonuses) {
        this.bonuses = bonuses;
    }

    /**
     * Method getMaxHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaxHit() {
        return maxHit;
    }

    /**
     * Method setMaxHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param maxHit
     */
    public void setMaxHit(int maxHit) {
        this.maxHit = maxHit;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getController() {
        return controller;
    }

    /**
     * Method setController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param controller
     */
    public void setController(String controller) {
        this.controller = controller;
    }

    /**
     * Method unpackConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param connection
     * @param player
     */
    public static void unpackConfig(MysqlConnection connection, Player player) {
        totalNpcs = 0;
        list.clear();

        try {
            long      start = System.currentTimeMillis();
            ResultSet r;

            if (!Main.TEST_MODE) {
                r = connection.query("SELECT * FROM npcinfo");
            } else {
                r = connection.query("SELECT * FROM npcinfo where npcid=549 or npcid=1154");
            }

            int amt = 0;

            if (player != null) {
                player.getActionSender().sendDev("Loading " + r.getFetchSize() + " npc definitions");
            }

            while (r.next()) {
                amt++;
                new NpcDef(r);
                totalNpcs++;
            }

            r.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}
