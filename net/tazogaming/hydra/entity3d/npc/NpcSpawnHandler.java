package net.tazogaming.hydra.entity3d.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 03:33
 */
public class NpcSpawnHandler {
    private static ArrayList<NpcAutoSpawn> spawns = new ArrayList<NpcAutoSpawn>();

    /**
     * Method addNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param spawn
     */
    public static void addNpc(NpcAutoSpawn spawn) throws IndexOutOfBoundsException {
        if(spawns.size() > 1400)
            throw new IndexOutOfBoundsException("Spawns must not exceed 1400");
        spawns.add(spawn);

        save("config/world/npc/npcpositions.cfg");
    }

    public static int size() {
        return spawns.size();
    }
    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     */
    public static void load(String path) {
        int loaded = 0;

        spawns.clear();

        try {
            BufferedReader br   = new BufferedReader(new FileReader(path));
            String         line = null;

            while ((line = br.readLine()) != null) {
                ArgumentTokenizer pr    = new ArgumentTokenizer(line.split(" "));
                NpcAutoSpawn      spawn = new NpcAutoSpawn();

                spawn.setNpcId(pr.nextInt());
                spawn.setSpawnX(pr.nextInt());
                spawn.setSpawnY(pr.nextInt());
                spawn.setSpawnHeight(pr.nextInt());
                spawn.setRoamRange(pr.nextInt());
                spawn.setScriptId(pr.nextInt());
                spawns.add(spawn);
                loaded++;

            }


            br.close();
            Logger.log("[NpcDefinition]loaded " + loaded + " npc spawns");
        } catch (Exception ee) {}


    }

    /**
     * Method delete
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param spawn
     */
    public static void delete(NpcAutoSpawn spawn) {
        spawns.remove(spawn);
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     */
    public static void save(String path) {
        try {
            BufferedWriter br = new BufferedWriter(new FileWriter(path));

            for (NpcAutoSpawn e : spawns) {
                br.write(e.getNpcId() + " " + e.getSpawnX() + " " + e.getSpawnY() + " " + e.getSpawnHeight() + " "
                         + e.getRoamRange() + " " + e.getScriptId());
                br.newLine();
            }

            br.close();
        } catch (Exception ee) {}
    }

    /**
     * Method loadToWorld
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadToWorld() {
        for (NpcAutoSpawn s : spawns) {
            Point location = Point.location(s.getSpawnX(), s.getSpawnY(), s.getSpawnHeight());
            NPC   npc      = new NPC(s.getNpcId(), location, s);

            World.getWorld().registerNPC(npc);
        }
    }
}
