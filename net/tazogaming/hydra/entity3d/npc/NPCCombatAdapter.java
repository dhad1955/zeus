package net.tazogaming.hydra.entity3d.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.CombatAdapter;
import net.tazogaming.hydra.net.packethandler.npc.MageNPC;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 09:35
 */
public class NPCCombatAdapter extends CombatAdapter<NPC> {
    private int combatDistance = 1;
    private int cooldownTime   = 0;
    private NPC ourNpc;

    /**
     * Constructs ...
     *
     *
     * @param npc
     */
    public NPCCombatAdapter(NPC npc) {
        super(npc);
        ourNpc = npc;
    }

    /**
     * Method onTargetAcquired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void onTargetAcquired() {}

    /**
     * Method getCombatDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCombatDistance() {
        return combatDistance;
    }

    /**
     * Method setCombatDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param combatDistance
     */
    public void setCombatDistance(int combatDistance) {
        this.combatDistance = combatDistance;
    }

    /**
     * Method getCooldownTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCooldownTime() {
        return cooldownTime;
    }

    /**
     * Method setCooldownTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cooldownTime
     */
    public void setCooldownTime(int cooldownTime) {
        this.cooldownTime = cooldownTime;
    }

    /**
     * Method isWithinBoundry
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean isWithinBoundry() {
        if (getFightingWith().isDead() || getMob().isDead() || (!getFightingWith().canBeAttacked(getMob()))) {
            return false;
        }

        if(getMob().isSummoned() && getFightingWith() instanceof Player && !getFightingWith().isInMultiArea()){
            return false;
        }

        if(getMob().isSummoned() && getFightingWith() instanceof Player && !getFightingWith().isInWilderness())
            return false;


        if(getMob().isSummoned() && getFightingWith() instanceof Player) {
            if(!Combat.canAttack(getMob().getSummoner(), getFightingWith().getPlayer())){
                getMob().getSummoner().getActionSender().sendMessage("Please move deeper into the wilderness");
                getMob().getCombatHandler().reset();
                return false;
            }
        }

        if(!getFightingWith().isVisible()) {
            return false;
        }
        if ((getFightingWith() instanceof Player) &&!getFightingWith().getPlayer().isLoggedIn()) {
            return false;
        }

        if (getFightingWith().getCurrentInstance() != ourNpc.getCurrentInstance()) {
            return false;
        }

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method isWithinDistanceToTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean isWithinDistanceToTarget() {
        if (!Combat.isWithinDistance(getMob(), getFightingWith())) {
            return false;
        }

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void terminate() {
        if (this.ourNpc.getControllerScript() != null) {
            this.ourNpc.getControllerScript().do_trigger(Block.TRIGGER_TARGET_OUT_OF_RANGE, null);
        }
    }

    /**
     * Method executeCombatTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void executeCombatTick() {
        if (ourNpc.isDead()) {
            return;
        }


        ourNpc.setCombatTick(ourNpc.getCombatTick() + 1);

        if (ourNpc.hasCombatScript() && ourNpc.getCombatScript().update_combat()) {
            return;

        }


        if ((ourNpc.getControllerScript() == null) || (ourNpc.getControllerScript().getController() == null)) {
            return;
        }

        if(!getFightingWith().isVisible()) {

            return;

        }

        if(ourNpc.isSummoned() && getFightingWith() == ourNpc.getSummoner())
        {
            reset();
            return;
        }

        if (!ourNpc.isSummoned() && (getFightingWith() instanceof NPC) && ((NPC) getFightingWith()).isSummoned()) {
            NpcScriptScope script = new NpcScriptScope(ourNpc,
                                          ourNpc.getControllerScript().getController().getSummonCombatScript());
            script.setInteractingPlayer(getFightingWith().getNpc().getSummoner());
            script.setInteractingNpc((NPC) getFightingWith());
            NPCScriptEngine.evaluate(ourNpc, script);

            return;
        }

        if (!ourNpc.isSummoned() && (getFightingWith() instanceof NPC)) {
            NpcScriptScope script = new NpcScriptScope(ourNpc,
                                          ourNpc.getControllerScript().getController().getSummonCombatScript());

            script.setInteractingNpc((NPC) getFightingWith());
            NPCScriptEngine.evaluate(ourNpc, script);

            return;
        }

        if (ourNpc.isSummoned() && (ourNpc.getControllerScript().getController().getSummonCombatScript() != null)) {
            if (!ourNpc.familarSpec()) {
                NpcScriptScope script = new NpcScriptScope(ourNpc,
                                              ourNpc.getControllerScript().getController().getSummonCombatScript());

                if(getFightingWith() instanceof NPC) {
                    script.setInteractingPlayer(getFightingWith().getNpc().getSummoner());
                    script.setInteractingNpc(getFightingWith().getNpc());
                }
                else
                    script.setInteractingPlayer(getFightingWith().getPlayer());

                NPCScriptEngine.evaluate(ourNpc, script);
            } else {
                MageNPC.familiarSpec(ourNpc.getSummoner());
                ourNpc.setFamiliarSpec(false);
            }
        } else if ((ourNpc.getControllerScript() != null) && ourNpc.getControllerScript().updateCombatScript(ourNpc)) {
            if (ourNpc.getControllerScript().getController().getCombatScript() != null) {
                NpcScriptScope script = new NpcScriptScope(ourNpc,
                                              ourNpc.getControllerScript().getController().getCombatScript(),
                                              (getFightingWith()) instanceof Player
                                              ? (Player) getFightingWith()
                                              : null);

                if (getFightingWith() instanceof Player) {
                    script.setInteractingPlayer((Player) getFightingWith());
                } else {
                    script.setInteractingNpc((NPC) getFightingWith());
                }
                NPCScriptEngine.evaluate(ourNpc, script);
            }
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
