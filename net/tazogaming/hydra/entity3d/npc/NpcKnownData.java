package net.tazogaming.hydra.entity3d.npc;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */
public class NpcKnownData {
    private int knownFLid      = -1;
    private int changedModel   = -1;
    private int viewX          = -1,
                viewY          = -1;
    private int facingTo       = -1;
    private int knownTransform = -1;

    /**
     * Method getKnownTransform
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getKnownTransform() {
        return knownTransform;
    }

    /**
     * Method setKnownTransform
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setKnownTransform(int i) {
        knownTransform = i;
    }

    /**
     * Method setKnownFacing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void setKnownFacing(int p) {
        facingTo = p;
    }

    /**
     * Method setKnownViewLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     */
    public void setKnownViewLocation(int x, int y) {
        this.viewX = x;
        this.viewY = y;
    }

    /**
     * Method setKnownModel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param md
     */
    public void setKnownModel(int md) {
        this.changedModel = md;
    }

    /**
     * Method getModel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModel() {
        return changedModel;
    }

    /**
     * Method getFacing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFacing() {
        return facingTo;
    }

    /**
     * Method getViewY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getViewY() {
        return viewY;
    }

    /**
     * Method getViewX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getViewX() {
        return viewX;
    }

    /**
     * Method setKnownFLID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setKnownFLID(int id) {
        this.knownFLid = id;
    }

    /**
     * Method getKnownFacingID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getKnownFacingID() {
        return knownFLid;
    }
}
