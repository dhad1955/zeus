package net.tazogaming.hydra.entity3d.npc.death;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.game.pets.BossPet;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.construction.obj.ObjectNode;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/11/13
 * Time: 06:55
 */
public abstract class NpcDeathHandler {




    public static final int[] RARE_ITEMS = {18747, 20054, 18652, 20135,20136,20137,20138,20139,20140,20141,20142,20143,20144,20145, 20146,20147,20148,20149, 20150, 20151, 20152, 20153, 20154, 20155, 20156, 20157, 20158, 20159, 20160, 20162, 20163, 20164, 20165, 20166, 20167, 20168, 20169, 20171, 20172,
    13746, 13747, 13748, 13749, 13752, 13753, 13751, 13752, 11694, 11696, 11698, 11700, 11728, 11724, 11726, 11725, 11732, 11286,11732};


    private static boolean isRare(int item){
        for(Integer i : RARE_ITEMS)
            if(i == item)
                return true;
        return false;
    }
    private DeathPolicy[] policies;

    /**
     * Constructs ...
     *
     *
     * @param policies
     */
    public NpcDeathHandler(DeathPolicy... policies) {
        this.policies = policies;
    }

    /**
     * Method setPolicy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param policies
     */
    public void setPolicy(DeathPolicy... policies) {
        this.policies = policies;
    }

    /**
     * Method policySet
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param policy
     *
     * @return
     */
    public boolean policySet(DeathPolicy policy) {
        for (DeathPolicy p : policies) {
            if (p == policy) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method handleDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param killedBy
     */
    public void handleDeath(NPC npc, Killable killedBy) {

        if (!policySet(DeathPolicy.DONT_DROP_LOOT)) {
            if (killedBy != null) {
                Player highestHitter = killedBy.getPlayer();


                if (npc.getDefinition() == null) {
                    return;
                }

                if (highestHitter != null) {
                    highestHitter.registerKillCount(npc.getId());


                    BossPet pet = BossPets.getByBoss(npc.getId());
                    if(pet != null){
                        if(pet.isUnlocked(highestHitter)){
                            new FloorItem(pet.getItemId(), 1, npc.getX(), npc.getY(), npc.getHeight(), highestHitter);
                            BossPets.sendNotification(pet, highestHitter);
                        }
                    }
                    if(npc.getId() == BossTournament.bossId) {
                        BossTournament.mainsLeaderboard.addPoint(highestHitter);
                    }
                    if (npc.getDefinition().hasLoot() && !highestHitter.getAccount().bossBlocked(npc.getId())) {

                        Loot[] always = npc.getDefinition().getLoot().getLootAlways();

                        if (always != null) {
searchloop:
                            for (Loot l : always) {
                                boolean skip = false;

                                if (l != null) {
                                    for (int i = 0; i < ObjectNode.PRAYER_XP.length; i++) {
                                        if (ObjectNode.PRAYER_XP[i][0] == l.getItemID()) {
                                            if (highestHitter.getInventory().hasItem(18337, 1)) {
                                                highestHitter.addXp(5, (int) (ObjectNode.PRAYER_XP[i][1] * 1.30));
                                                highestHitter.getActionSender().sendMessage(
                                                    "Your bone crusher crushes the bones, you receive "
                                                    + (ObjectNode.PRAYER_XP[i][0] * 1.30));
                                                skip = true;

                                                break;
                                            }
                                        }
                                    }

                                    if (skip) {
                                        continue;
                                    }

                                    String message = null;
                                    if(l.getRarity() == 4)  {
                                        message = "Rare";
                                    }else if(l.getRarity() == 5)
                                        message = "Ultra rare";


                                    if(message != null && npc.getSize() > 2){
                                        for(Player player : highestHitter.getWatchedPlayers().getAllEntities()){
                                            player.getActionSender().sendMessage(highestHitter.getUsername()+" has just got a rare drop "+ Item.forId(l.getItemID())+" x "+l.getAmount());
                                        }
                                    }



                                    new FloorItem(l.getItemID(), ((npc.getId() != 1267)
                                                                  ? l.getAmount() == 0 ? 1 : l.getAmount()
                                                                  : l.getAmount() * 2), npc.getX(), npc.getY(),
                                                                  npc.getHeight(), highestHitter);
                                }
                            }
                        }

                        int lootAmt = 1;

                        if (npc.getTimers().timerActive(TimingUtility.DOUBLE_DROPS_TIMER)) {
                            lootAmt = 2;
                        }

                        if(highestHitter.getEquipment().getId(3) == 15443 && GameMath.rand(100) < 10){
                            highestHitter.getActionSender().sendMessage("Your lucky whip starts glowing");
                            highestHitter.getGraphic().set(2176, Graphic.LOW);
                            lootAmt+=1;

                        }

                        if (Config.double_drops || (highestHitter.getId() == 6042)) {
                            lootAmt += 1;
                        }



                        if (npc.isSlayerNPC() && highestHitter.isTenth(18)) {
                            lootAmt += 1;
                        }

                        Loot[] randomLoot = npc.getDefinition().getLoot().getLoot(lootAmt, highestHitter);


                        if(highestHitter != null){

                            if(highestHitter.isInWilderness()) {
                                highestHitter.addAchievementProgress2(Achievement.WILDERNESS_PVMER_II, 1);
                                highestHitter.addAchievementProgress2(Achievement.WILDERNESS_PVMER, 1);
                                highestHitter.addAchievementProgress2(Achievement.WILDERNESS_PVMER_IIII, 1);
                                highestHitter.addAchievementProgress2(Achievement.WILDERNESS_PVMER_III, 1);

                            }
                            if(npc.getId() == 1158 || npc.getId() == 1160 || npc.getId() == 50 || npc.getId() == 8133 || npc.getId() == 6260 || npc.getId() == 6222){
                                highestHitter.addAchievementProgress2(Achievement.WHOS_THE_BOSS_NOW, 1);
                            }
                            if(npc.getId() == 8133)
                                highestHitter.addAchievementProgress2(Achievement.CORPOREAL_PUNISHMENT, 1);
                            if(npc.getDefinition() != null && npc.getDefinition().getName().toLowerCase().contains("rev"))
                                highestHitter.addAchievementProgress2(Achievement.TIME_TO_CROSSOVER, 1);

                        }
                        for (Loot l : randomLoot) {
                            if(l == null)
                                continue;

                            if(l.getItemID() == 11694 && GameMath.rand(100) > 50)
                                l = new Loot(995, 50000000, 5);
                            if (l != null) {
                                if(isRare(l.getItemID())){
                                    World.globalMessage(Text.PURP(highestHitter.getUsername()+" has just got a "+Item.forId(l.getItemID()).getName()+" drop from "+npc.getDefinition().getName()));


                                }
                                new FloorItem(l.getItemID(), l.getAmount() == 0 ? 1 : l.getAmount(), npc.getX(), npc.getY(), npc.getHeight(),
                                              highestHitter);
                               if(l.getItemID() == 11235 && highestHitter != null)
                                   highestHitter.addAchievementProgress2(Achievement.BOW_AND_THE_BEAST, 1);
                            }
                        }
                    }
                }


                int[] charms = { 12160, 12159, 12163, 12158 };

                if ((Core.currentTime % 2 == 0) && (npc.getDefinition().getCharmDropAmount() > 0)) {
                    new FloorItem(charms[GameMath.rand3(charms.length)], 1, npc.getX(), npc.getY(), npc.getHeight(),
                                  highestHitter);
                }
            }
        }

        onDeath(npc, killedBy);

        if (policySet(DeathPolicy.UNLINK_ON_DEATH)) {
            npc.unlink();
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    protected abstract void onDeath(NPC killed, Killable killedBy);
}
