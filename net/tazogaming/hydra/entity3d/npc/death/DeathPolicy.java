package net.tazogaming.hydra.entity3d.npc.death;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/11/13
 * Time: 06:54
 */
public enum DeathPolicy { DONT_DROP_LOOT, UNLINK_ON_DEATH, RESPAWN, FAST_ACTION }
