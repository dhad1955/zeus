package net.tazogaming.hydra.entity3d.npc.death;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/11/13
 * Time: 06:56
 */
public interface NpcDeathTrigger {

    /**
     * Method npcKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param killed_by
     */
    public void npcKilled(NPC npc, Killable killed_by);
}
