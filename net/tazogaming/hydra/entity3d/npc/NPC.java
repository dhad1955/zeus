package net.tazogaming.hydra.entity3d.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.ai.hardscript.HardWiredScript;
import net.tazogaming.hydra.entity3d.npc.ai.hardscript.NexCombatScript;
import net.tazogaming.hydra.entity3d.npc.ai.movement.*;
import net.tazogaming.hydra.entity3d.npc.ai.script.*;
import net.tazogaming.hydra.entity3d.npc.ai.script.smartnpc.NPCMovementScript;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.entity3d.player.masks.Interacting;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.cutscene.CutScene;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.game.minigame.fight_caves.FightCavesGame;
import net.tazogaming.hydra.game.minigame.pc.PCGame;
import net.tazogaming.hydra.game.minigame.zombiesx.Zombies;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.filter.NPCScriptDamageFilter;
import net.tazogaming.hydra.game.skill.combat.filter.PlayerDefenceDamageFilter;
import net.tazogaming.hydra.game.skill.combat.impact.SoulsplitImpactEvent;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.hunter.HuntingController;
import net.tazogaming.hydra.game.skill.hunter.HuntingEntity;
import net.tazogaming.hydra.game.skill.hunter.Trap;
import net.tazogaming.hydra.game.skill.slayer.SlayKillable;
import net.tazogaming.hydra.game.skill.slayer.Slayer;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 22:42
 * To change this template use File | Settings | File Templates.
 */
public class NPC extends Killable {
    public static final NpcDeathHandler DEFAULT_DEATH_HANDLER = new NpcDeathHandler() {
        @Override
        protected void onDeath(NPC killed, Killable killedBy) {

            // To change body of implemented methods use File | Settings | File Templates.
        }
    };
    public static final NpcDeathHandler UNLINK_ON_DEATH = new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
        @Override
        protected void onDeath(NPC killed, Killable killedBy) {

            // To change body of implemented methods use File | Settings | File Templates.
        }
    };

    /** animation made: 14/11/09 */
    private Animation animation = new Animation();

    /** currentGraphic made: 14/11/09 */
    private Graphic currentGraphic = new Graphic();

    /** textUpdate made: 14/11/09 */
    private String textUpdate = null;

    /** currentHealth made: 14/11/09 */
    private double currentHealth = 4;

    /** maxHealth made: 14/11/09 */
    private double maxHealth = 4;

    /** focus made: 14/11/09 */
    private Interacting focus = new Interacting();

    /** isBusy made: 14/11/09 */
    private boolean isBusy = false;

    /** didTeleport made: 14/11/09 */
    private boolean didTeleport = false;

    /** combatTick made: 14/11/09 */
    private int combatTick = 0;

    /** combatHandler made: 14/11/09 */
    private NPCCombatAdapter combatHandler = new NPCCombatAdapter(this);

    /** currentController made: 14/11/09 */
    private Player currentController = null;

    /** block_interactions made: 14/11/09 */
    private boolean block_interactions = false;

    /** damageImmunity made: 14/11/09 */
    private int damageImmunity = 0;

    /** isRemoved made: 14/11/09 */
    private boolean isRemoved = false;

    /** dont_die made: 14/11/09 */
    private boolean dont_die = false;

    /** forage_items made: 14/11/09 */
    private LinkedList<Item> forage_items = new LinkedList<Item>();

    /** playersInRange made: 14/11/09 */
    private ArrayList<Player> playersInRange = new ArrayList<Player>();

    /** familarSpecReq made: 14/11/09 */
    private boolean familarSpecReq = false;

    /** ignoreProjectileClipping made: 14/11/09 */
    private boolean ignoreProjectileClipping = false;

    /** remove_on_death made: 14/11/09 */
    private boolean remove_on_death = false;

    /** transformId made: 14/11/09 */
    private int transformId = -1;

    /** playersAlreadyHit made: 14/11/09 */
    private LinkedList<Integer> playersAlreadyHit = new LinkedList<Integer>();

    /** isFlying made: 14/11/09 */
    private boolean isFlying = false;

    /** roamSpeed made: 14/11/09 */
    private int roamSpeed = 75;

    /** chatting made: 14/11/09 */
    private List<Player> chatting = new ArrayList<Player>();

    /** movement made: 14/11/09 */
    private Movement movement;

    /** spawnData made: 14/11/09 */
    private NpcAutoSpawn spawnData;

    /** moveScript made: 14/11/09 */
    private NPCMovementScript moveScript;

    /** lastTarget made: 14/11/09 */
    private Killable lastTarget;

    /** controllerScript made: 14/11/09 */
    private ScriptHandler controllerScript;

    /** definition made: 14/11/09 */
    private NpcDef definition;

    /** slayerInfo made: 14/11/09 */
    private SlayKillable slayerInfo;

    /** summon_master made: 14/11/09 */
    private Player summon_master;

    /** quest_controller made: 14/11/09 */
    private Player quest_controller;

    /** huntEntity made: 14/11/09 */
    private HuntingEntity huntEntity;

    /** myZombieGame made: 14/11/09 */
    private Zombies myZombieGame;

    /** myFightCavesGame made: 14/11/09 */
    private FightCavesGame myFightCavesGame;

    /** myGame made: 14/11/09 */
    private PCGame myGame;

    /** combat_script made: 14/11/09 */
    private HardWiredScript combat_script;

    /** deathHandler made: 14/11/09 */
    private NpcDeathHandler deathHandler;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param summoner
     */
    public NPC(int id, Player summoner) {
        setId(id);
        this.summon_master    = summoner;
        this.controllerScript = new ScriptHandler(this, summon_master);
        setMovement(new SummoningMovement(this, summon_master));
        definition = NpcDef.FOR_ID(id);

        Point p = ((SummoningMovement) getMovement()).find_safe_tele(this, summon_master);

        if (p == null) {
            setLocation(Point.location(3222, 3222, 0));
        } else {
            setLocation(p);
        }

        this.maxHealth     = definition.getStartingHealth();
        this.currentHealth = definition.getStartingHealth();

        if (definition.getController() != null) {
            this.controllerScript.setController(NPCScriptEngine.getController(definition.getController()));
        }

        if ((controllerScript != null) && (controllerScript.getController() != null)) {
            controllerScript.do_trigger(Block.TRIGGER_ON_SPAWN, summoner);
        }

        if (definition.isAttackable()) {
            addDamageFilter("def", new PlayerDefenceDamageFilter());
        }

        if ((controllerScript != null) && (controllerScript.getController() != null)
                && (controllerScript.getController().getDamageFilter() != null)) {
            addDamageFilter("idamage", new NPCScriptDamageFilter());
        }

        setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) { //So, it's weird.
                killed.getSummoner().resetSummon();
                killed.getSummoner().getActionSender().sendMessage("Your familiar has died.");
            }
        });

        if (getCombatScript() != null) {
            getCombatScript().on_spawn();

            return;
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param location
     */
    public NPC(int id, Point location) {
        this(id, location, null, null);
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param p
     * @param range
     */
    public NPC(int id, Point p, int range) {
        this(id, p, getPoint(p, range), null);
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param location
     * @param spawn
     */
    public NPC(int id, Point location, NpcAutoSpawn spawn) {
        this(id, location, spawn, null);
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param location
     * @param range
     * @param def
     */
    public NPC(int id, Point location, int range, NpcDef def) {
        this(id, location, getPoint(location, range), def);
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param location
     * @param spawn
     * @param def
     */
    public NPC(int id, Point location, NpcAutoSpawn spawn, NpcDef def) {
        setId(id);
        setLocation(location, true);
        this.spawnData = spawn;

        if (id == 13447) {
            combat_script = new NexCombatScript(this);
        }

        definition            = (def == null)
                                ? NpcDef.FOR_ID(id)
                                : def;
        this.maxHealth        = definition.getStartingHealth();
        this.currentHealth    = definition.getStartingHealth();
        this.controllerScript = new ScriptHandler(this);

        if ((spawn != null) && (spawn.getScriptId() != -1)) {
            NPCMovementScript script = NPCMovementScript.getForId(spawn.getScriptId());

            if (script != null) {
                setMovement(new ScriptMovement(this, script));
            }
        } else {
            setMovement(new RoamingMovement(this));
        }

        if ((this.getMovement() instanceof RoamingMovement) || (getMovement() == null)) {
            if (definition.getController() != null) {
                this.controllerScript.setController(NPCScriptEngine.getController(definition.getController()));
            }
        }

        if ((controllerScript != null) && (controllerScript.getController() != null)) {
            controllerScript.do_trigger(Block.TRIGGER_ON_SPAWN, null);
        }

        if (hasCombatScript()) {
            combat_script.on_spawn();
        }

        if (definition.isAttackable()) {
            addDamageFilter("def", new PlayerDefenceDamageFilter());
        }

        if ((controllerScript != null) && (controllerScript.getController() != null)
                && (controllerScript.getController().getDamageFilter() != null)) {
            addDamageFilter("idamage", new NPCScriptDamageFilter());
        }

        slayerInfo = Slayer.get_slayer_npc_info(this);
        huntEntity = HuntingController.getEntity(this);

        if ((huntEntity != null) && (huntEntity.getType() != Trap.BUTTERFLY)) {
            this.setMovement(new HuntingMovement(this));
        }

        if (deathHandler == null) {
            setDeathHandler(DEFAULT_DEATH_HANDLER);
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param x
     * @param y
     * @param h
     * @param e
     */
    public NPC(int id, int x, int y, int h, CutScene e) {
        setId(id);
        setLocation(Point.location(x, y, h));
        setCurrentInstance(e);
        setCutScene(e);
        setMovement(new CutsceneMovement(this, e));
    }

    /**
     * Method addChatting
     * Created on 14/11/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void addChatting(Player player) {
        if (!chatting.contains(player)) {
            chatting.add(player);
        }

        getFocus().focus(player);
    }

    /**
     * Method removeChatting
     * Created on 14/11/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void removeChatting(Player player) {
        if (chatting.contains(player)) {
            chatting.remove(player);
        }

        if ((chatting.size() == 0) && (getMovement() instanceof RoamingMovement)) {
            getFocus().unFocus();
        }
    }

    /**
     * Method updateChatting
     * Created on 14/11/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateChatting() {
        if (chatting.size() > 0) {
            for (Iterator<Player> chattingPlayers = chatting.iterator(); chattingPlayers.hasNext(); ) {
                Player pla = chattingPlayers.next();

                if (!pla.getFocus().isFocused(this)) {
                    chattingPlayers.remove();
                }
            }
        }

        if ((chatting.size() == 0) && (getMovement() instanceof RoamingMovement)) {
            getFocus().unFocus();
        }
    }

    /**
     * Method familarSpec
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean familarSpec() {
        return familarSpecReq;
    }

    /**
     * Method setFamiliarSpec
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setFamiliarSpec(boolean b) {
        this.familarSpecReq = b;
    }

    /**
     * Method setIgnoreProjectileClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setIgnoreProjectileClipping(boolean b) {
        ignoreProjectileClipping = b;
    }

    /**
     * Method ignoreProjectileClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean ignoreProjectileClipping() {
        return ignoreProjectileClipping;
    }

    /**
     * Method setPCGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param game
     */
    public void setPCGame(PCGame game) {
        this.myGame = game;
    }

    /**
     * Method getPCGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PCGame getPCGame() {
        return myGame;
    }

    /**
     * Method getPlayersAlreadyHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LinkedList<Integer> getPlayersAlreadyHit() {
        return playersAlreadyHit;
    }

    /**
     * Method setTransform
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setTransform(int i) {
        transformId = i;
    }

    /**
     * Method getTransformID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTransformID() {
        return transformId;
    }

    /**
     * Method hasCombatScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasCombatScript() {
        return combat_script != null;
    }

    /**
     * Method setCombatScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scr
     */
    public void setCombatScript(HardWiredScript scr) {
        this.combat_script = scr;
    }

    /**
     * Method getCombatScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public HardWiredScript getCombatScript() {
        return combat_script;
    }

    /**
     * Method setDeathHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param h
     */
    public void setDeathHandler(NpcDeathHandler h) {
        deathHandler = h;
    }

    /**
     * Method setCaves
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param g
     */
    public void setCaves(FightCavesGame g) {
        myFightCavesGame = g;
    }

    /**
     * Method getCaves
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public FightCavesGame getCaves() {
        return myFightCavesGame;
    }

    /**
     * Method setMaxHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param h
     */
    public void setMaxHealth(int h) {
        this.maxHealth     = h;
        this.currentHealth = h;
    }

    /**
     * Method getPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     * @param range
     *
     * @return
     */
    static NpcAutoSpawn getPoint(Point location, int range) {
        NpcAutoSpawn SP = new NpcAutoSpawn();

        SP.setSpawnX(location.getX());
        SP.setSpawnY(location.getY());
        SP.setSpawnHeight(location.getHeight());
        SP.setRoamRange(range);

        return SP;
    }

    /**
     * Method getZombieGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Zombies getZombieGame() {
        return myZombieGame;
    }

    /**
     * Method setZombieGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param z
     */
    public void setZombieGame(Zombies z) {
        myZombieGame = z;
    }

    /**
     * Method isHunter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isHunter() {
        return huntEntity != null;
    }

    /**
     * Method getHuntEntity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public HuntingEntity getHuntEntity() {
        return huntEntity;
    }

    /**
     * Method getForageItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LinkedList<Item> getForageItems() {
        return forage_items;
    }

    /**
     * Method getQuest_controller
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getQuest_controller() {
        return quest_controller;
    }

    /**
     * Method setQuestController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void setQuestController(Player player) {
        quest_controller = player;
    }

    /**
     * Method isSlayerNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSlayerNPC() {
        return slayerInfo != null;
    }

    /**
     * Method getSlayerInfo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SlayKillable getSlayerInfo() {
        return slayerInfo;
    }

    /**
     * Method update_quest_controller
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update_quest_controller() {
        if (quest_controller != null) {
            if (quest_controller.isDead() || (Point.getDistance(quest_controller.getLocation(), getLocation()) >= 30)) {
                unlink();
                quest_controller.unlink_npc(this);
            }
        }
    }

    /**
     * Method setDont_die
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dont_die
     */
    public void setDont_die(boolean dont_die) {
        this.dont_die = dont_die;
    }

    /**
     * Method getCutsceneMovementHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public CutsceneMovement getCutsceneMovementHandler() {
        return (CutsceneMovement) movement;
    }

    /**
     * Method getSummoner
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getSummoner() {
        return summon_master;
    }

    /**
     * Method isSummoned
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSummoned() {
        return summon_master != null;
    }

    /**
     * Method blockInteractions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean blockInteractions() {
        return block_interactions;
    }

    /**
     * Method setBlock_interactions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param block_interactions
     */
    public void setBlock_interactions(boolean block_interactions) {
        this.block_interactions = block_interactions;
    }

    /**
     * Method getDamageImmunity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDamageImmunity() {
        return damageImmunity;
    }

    /**
     * Method setDamageImmunity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damageImmunity
     */
    public void setDamageImmunity(int damageImmunity) {
        this.damageImmunity = damageImmunity;
    }

    /**
     * Method rehash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void rehash() {
        if (!(getCurrentInstance() instanceof Dungeon) &&!isInArea(PCGame.PEST_CONTROL_GAME)) {
            this.definition = NpcDef.FOR_ID(getId());
            this.maxHealth  = definition.getStartingHealth();

            // this.currentHealth = definition.getStartingHealth();
            if (definition.getController() != null) {
                this.controllerScript.resetAll();
                this.controllerScript.hardPause(0);

                for (NPC npc : controllerScript.getSlaves()) {
                    if (npc != null) {
                        npc.unlink();
                    }
                }

                this.controllerScript.setSlaves(new NPC[10]);
                this.controllerScript.setController(NPCScriptEngine.getController(definition.getController()));
            }
        }
    }

    /**
     * Method getDefinition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NpcDef getDefinition() {
        return definition;
    }

    /**
     * Method canGo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param point
     * @param dist
     *
     * @return
     */
    public boolean canGo(Point point, int dist) {
        if (spawnData == null) {
            return true;
        }

        return Point.getDistance(spawnData.getSpawnX(), spawnData.getSpawnY(), point.getX(), point.getY())
               < spawnData.getRoamRange() + (getSize() / 2) + dist;
    }

    /**
     * Method canGo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param point
     *
     * @return
     */
    public boolean canGo(Point point) {
        if (spawnData == null) {
            return true;
        }

        return canGo(point, 1);
    }

    /**
     * Method getCombatTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCombatTick() {
        return combatTick;
    }

    /**
     * Method setCombatTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param combatTick
     */
    public void setCombatTick(int combatTick) {
        this.combatTick = combatTick;
    }

    /**
     * Method getLastTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Killable getLastTarget() {
        return lastTarget;
    }

    /**
     * Method setLastTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lastTarget
     */
    public void setLastTarget(Killable lastTarget) {
        this.lastTarget = lastTarget;
    }

    /**
     * Method getCombatHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPCCombatAdapter getCombatHandler() {
        return combatHandler;
    }

    /**
     * Method setCombatHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param combatHandler
     */
    public void setCombatHandler(NPCCombatAdapter combatHandler) {
        this.combatHandler = combatHandler;
    }

    /**
     * Method getControllerScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptHandler getControllerScript() {
        return controllerScript;
    }

    @Override
    protected void onMoved(Point movedFrom, Point movedTo, int direction) {}

    /**
     * Method didTeleport
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean didTeleport() {
        return didTeleport;
    }

    /**
     * Method setDidTeleport
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param didTeleport
     */
    public void setDidTeleport(boolean didTeleport) {
        this.didTeleport = didTeleport;
    }

    /**
     * Method teleport
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     */
    public void teleport(int x, int y, int h) {
        setLocation(Point.location(x, y, h));
        setDidTeleport(true);
    }

    /**
     * Method getSpawnData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NpcAutoSpawn getSpawnData() {
        return spawnData;
    }

    /**
     * Method getCurrentController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getCurrentController() {
        return currentController;
    }

    /**
     * Method setCurrentController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentController
     */
    public void setCurrentController(Player currentController) {
        if ((currentController == null) && (this.currentController != null)
                && getFocus().isFocused(this.currentController)) {
            this.getFocus().unFocus();
        }

        this.currentController = currentController;
    }

    /**
     * Method reloadForScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reloadForScript() {
        if (this.spawnData != null) {
            if (this.spawnData.getScriptId() == -1) {
                return;
            }

            this.moveScript = NPCMovementScript.getForId(this.spawnData.getScriptId());
            teleport(this.spawnData.getSpawnX(), this.spawnData.getSpawnY(), this.spawnData.getSpawnHeight());
            this.setMovement(new ScriptMovement(this, this.moveScript));
        }
    }

    /**
     * Method updateController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateController() {

        if(this.isSummoned()){
            if(!this.getSummoner().isLoggedIn())
                unlink();
            if(this.getSummoner().getCurrentFamiliar() != this)
                unlink();
        }
        if (currentController != null) {
            if (!currentController.isLoggedIn()
                    || (Point.getDistance(currentController.getLocation(), getLocation()) >= 15) || isDead()) {
                if (getFocus().isFocused(currentController)) {
                    getFocus().unFocus();
                }

                currentController = null;    // reset
            }
        }
    }

    /**
     * Method isFlying
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isFlying() {
        return isFlying;
    }

    /**
     * Method setFlying
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param isFlying
     */
    public void setFlying(boolean isFlying) {
        this.isFlying = isFlying;
    }

    /**
     * Method updateMovement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateMovement() {
        if (getNpc().isStunned()) {
            return;
        }

        if (isDead() || blockInteractions()) {
            return;    //
        }

        if (hasCombatScript() && getCombatScript().update_movement()) {
            return;
        }

        if (getTimers().timerActive(TimingUtility.FROZEN_TIME)) {
            return;
        }

        if (getMovement() instanceof RoamingMovement) {
            if (!this.isSummoned() && ((getControllerScript() != null) && getControllerScript().hasScripts())
                    && (getControllerScript().getController().getIdle() != null) &&!controllerScript.isPaused()) {
                NPCScriptEngine.evaluate(this,
                        new NpcScriptScope(this,
                                getControllerScript().getController().getIdle()));
            } else {
                getMovement().tick(Core.currentTime);
            }
        } else if (getMovement() != null) {
            getMovement().tick(Core.currentTime);
        }
    }

    /**
     * Method getRoamSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRoamSpeed() {
        return roamSpeed;
    }

    /**
     * Method setRoamSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param roamSpeed
     */
    public void setRoamSpeed(int roamSpeed) {
        this.roamSpeed = roamSpeed;
    }

    /**
     * Method isBusy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBusy() {
        return isBusy || (getMovement() instanceof FollowPlayerMovement) || isDead() || getCombatHandler().isInCombat()
               || block_interactions || (chatting.size() > 0);
    }

    /**
     * Method isTalking
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTalking() {
        return currentController != null;
    }

    /**
     * Method isRemoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isRemoved() {
        return isRemoved;
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unlink() {
        if (getId() == 13447) {
        }

        if (isRemoved) {
            return;
        }

        isRemoved = true;
        setLocation(null);

        if ((getQuest_controller() != null) && getQuest_controller().getLinked_npcs().contains(this)) {
            getQuest_controller().getLinked_npcs().remove(this);
        }

        World.getWorld().getNpcs().remove(this);
        setVisible(false);
    }

    /**
     * Method isBusy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean isBusy(Player pla) {
        return ((currentController != null) && (currentController != pla)) || isBusy() || (chatting.size() > 0);
    }

    /**
     * Method setTextUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param text
     */
    public void setTextUpdate(String text) {
        textUpdate = text;
    }

    /**
     * Method getTextUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getTextUpdate() {
        return textUpdate;
    }

    /**
     * Method getGraphic
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Graphic getGraphic() {
        return currentGraphic;
    }

    /**
     * Method timerUp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param timer
     */
    @Override
    public void timerUp(int timer) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getFocus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Interacting getFocus() {
        return focus;
    }

    /**
     * Method setMovement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param h
     */
    public void setMovement(Movement h) {
        movement = h;
    }

    /**
     * Method getMovement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Movement getMovement() {
        return movement;
    }

    /**
     * Method getKnownPlayersInRange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Player> getKnownPlayersInRange() {
        return playersInRange;
    }

    /**
     * Method getPlayersInRange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param range
     *
     * @return
     */
    public Player[] getPlayersInRange(int range) {
        Player[] players = new Player[50];

        if (playersInRange == null) {
            playersInRange = getViewArea().getPlayersInView();
        }


        int c = 0;
        for (Player pla : playersInRange) {
            if ((Point.getDistance(pla.getLocation(), getLocation()) <= range) && (pla.getHeight() == getHeight())
                    && (pla.getCurrentInstance() == getCurrentInstance())) {
                if (c == players.length) {
                    break;
                }
                players[c++] = pla;
            }
        }

        return players;
    }

    /**
     * Method validatePlayersInRange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void validatePlayersInRange() {
        if (isDead()) {
            return;
        }

        for (int i = 0; i < playersInRange.size(); i++) {
            if ((Point.getDistance(playersInRange.get(i).getLocation(), getLocation()) > 20)
                    || !playersInRange.get(i).isLoggedIn()
                    || (playersInRange.get(i).getCurrentInstance() != getCurrentInstance())) {
                playersInRange.remove(i);
            }
        }

        getViewArea().getPlayersInView(15, playersInRange, this);
    }

    /**
     * Method getSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSize() {
        if (definition != null) {
            return (definition.getSize() > 0)
                   ? definition.getSize()
                   : 1;
        }

        return 1;
    }

    /**
     * Method getCenterPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getCenterPoint() {
        return Point.location(getX() + (getSize() / 2), getY() + (getSize() / 2), getHeight());
    }

    /**
     * Method getAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Animation getAnimation() {
        return animation;
    }

    /**
     * Method isNPCWithinDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param distance
     *
     * @return
     */
    public boolean isNPCWithinDistance(NPC npc, int distance) {
        int siz          = getSize();
        int lowerBoundX  = getX() - distance;
        int lowerBoundY  = getY() - distance;
        int higherBoundX = getX() + distance + (siz - 1);
        int higherBoundY = getY() + distance + (siz - 1);

        if (npc.getSize() == 1) {
            return isWithinDistance(npc, distance);
        }

        for (int tile_x = npc.getX(); tile_x < npc.getX() + npc.getSize(); tile_x++) {
            for (int tile_y = npc.getY(); tile_y < npc.getY() + npc.getSize(); tile_y++) {
                if ((tile_x >= lowerBoundX) && (tile_y >= lowerBoundY) && (tile_y <= higherBoundY)
                        && (tile_x <= higherBoundX)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method isWithinTileRange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     *
     * @return
     */
    public boolean isWithinTileRange(Point p) {
        int lowerBoundX = getX();
        int lowerBoundY = getY();
        int higherX     = getX() + getSize() - 1;
        int higherY     = getY() + getSize() - 1;

        return (p.getX() >= lowerBoundX) && (p.getX() <= higherX) && (p.getY() >= lowerBoundY) && (p.getY() <= higherY);
    }

    /**
     * Method isWithinDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param m
     * @param distance
     *
     * @return
     */
    public boolean isWithinDistance(Mob m, int distance) {
        int siz = 1;

        if ((this.getDefinition() != null) && (this.getDefinition().getSize() < 0)) {
            siz = Math.abs(getSize());
        } else {
            siz = getSize();
        }

        Point loc = m.getLocation();

        if (m instanceof Player) {

            // loc = m.getKnownClientLoc();
        }

        int lowerBoundX  = getX() - distance;
        int lowerBoundY  = getY() - distance;
        int higherBoundX = getX() + distance + (siz - 1);
        int higherBoundY = getY() + distance + (siz - 1);

        return (loc.getX() >= lowerBoundX) && (loc.getY() >= lowerBoundY) && (loc.getY() <= higherBoundY)
               && (loc.getX() <= higherBoundX);

        // return false;
    }

    /**
     * Method getTiles
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param location
     *
     * @return
     */
    public static Tile[] getTiles(NPC npc, Point location) {
        int size      = 1,
            tileCount = 0;

        size = npc.getSize();

        Tile[] tiles = new Tile[size * size];

        if (tiles.length == 1) {
            tiles[0] = World.getWorld().getTile(location);
        } else {
            for (int x = 0; x < size; x++) {
                for (int y = 0; y < size; y++) {
                    tiles[tileCount++] = World.getWorld().getTile(location.getX() + x, location.getY() + y,
                            location.getHeight());
                }
            }
        }

        return tiles;
    }

    /**
     * Method getCurrentHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public double getCurrentHealth() {
        return Math.round(this.currentHealth * 10d) / 10d;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getMaxHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public double getMaxHealth() {
        return maxHealth;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method addHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param health
     */
    @Override
    public void addHealth(double health) {
        this.currentHealth += health;

        if (this.currentHealth > maxHealth) {
            this.currentHealth = maxHealth;
        }
    }

    /**
     * Method setHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param max
     */
    public void setHealth(int max) {
        this.currentHealth = max;
        this.maxHealth     = max;
    }

    /**
     * Method removeHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentHealth
     */
    @Override
    public void removeHealth(double currentHealth) {
        if(this.currentHealth == 0)
            return;


        this.currentHealth -= currentHealth;


        if (Math.ceil(this.currentHealth) <= 0.0) {
            this.currentHealth = 0.00;
        }
    }

    /**
     * Method onHitBy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     * @param d
     */
    @Override
    public void onHitBy(Mob hitBy, Damage d) {
        if ((getZombieGame() != null) && (hitBy instanceof Player) && (d.getDamage() > 0)) {
            int added = (int)d.getDamage() * 6;

            if (getZombieGame().timerActive(Zombies.DOUBLE_POINTS)) {
                added *= 2;
            }

            ((Player) hitBy).getAccount().addZombieScore(added);
            ((Player) hitBy).getActionSender().sendZombiePoints(((Player) hitBy).getAccount().getZombieScore());
        }

        if ((controllerScript != null) && (controllerScript.getController() != null)) {
            Block block = controllerScript.getController().getTrigger(Block.TRIGGER_NPC_HIT);

            if (block != null) {
                Player pla = null;

                if (hitBy instanceof Player) {
                    pla = (Player) hitBy;
                }

                NpcScriptScope script = new NpcScriptScope(this, block, pla);

                if (hitBy instanceof Player) {
                    script.setInteractingPlayer((Player) hitBy);
                } else if (hitBy instanceof NPC) {
                    if (((NPC) hitBy).isSummoned()) {
                        script.setInteractingPlayer(((NPC) hitBy).getSummoner());
                    }

                    script.setInteractingNpc((NPC) hitBy);
                }

                NPCScriptEngine.evaluate(this, script);
            }
        }

        int damage = (int)d.getDamage();

        if (d.isNormalDamage() && (damage > 0)) {
            if (hitBy instanceof Player) {
                Player pla = (Player) hitBy;

                if (pla.getPrayerBook().curseActive(PrayerBook.SOUL_SPLIT)) {
                    Projectile soul_split_projectile = new Projectile(pla, this, 2263, 30, 0);

                    soul_split_projectile.setAttribute(1, 21);
                    soul_split_projectile.setAttribute(2, 21);
                    soul_split_projectile.setOnImpactEvent(new SoulsplitImpactEvent(damage));
                    pla.registerProjectile(soul_split_projectile);
                }
            }
        }

        if (damage < 1) {
            if (definition != null) {
                getAnimation().animate(definition.getBlockAnim(), Animation.LOW_PRIORITY);
            }
        }
    }

    /**
     * Method setDefinition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param def
     */
    public void setDefinition(NpcDef def) {
        definition = def;
    }

    /**
     * Method respawn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void respawn() {
        if (isRemoved) {
            return;
        }

        this.currentHealth = getMaxHealth();
        this.setVisible(true);

        if (this.spawnData != null) {
            this.setLocation(Point.location(spawnData.getSpawnX(), spawnData.getSpawnY(), spawnData.getSpawnHeight()),
                             true);
        }

        super.respawn();

        if ((controllerScript != null) && (controllerScript.getController() != null)) {
            controllerScript.do_trigger(Block.TRIGGER_ON_SPAWN, null);
        }

        if (getCombatScript() != null) {
            getCombatScript().on_spawn();
        }
    }

    /**
     * Method entityKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     */
    @Override
    public void entityKilled(Mob hitBy) {
        if ((hitBy instanceof NPC) && ((NPC) hitBy).isSummoned()) {
            hitBy = ((NPC) hitBy).getSummoner();
        }

        if (hitBy instanceof Player) {
            Player plr = null;

            if (plr != null) {
                hitBy = plr;
            }
        }

        if (deathHandler.policySet(DeathPolicy.FAST_ACTION)) {
            deathHandler.handleDeath(this, (Killable) hitBy);

            if (dont_die) {
                super.respawn();
                dont_die = false;

                return;
            }
        }

        if ((getCutScene() == null) && (controllerScript != null) && (controllerScript.getController() != null)) {
            Block block = controllerScript.getController().getTrigger(Block.TRIGGER_NPC_DEAD);

            if (block != null) {
                NpcScriptScope script = new NpcScriptScope(this, block);

                if (hitBy instanceof Player) {
                    script.setInteractingPlayer((Player) hitBy);
                } else if (hitBy instanceof NPC) {
                    script.setInteractingNpc((NPC) hitBy);

                    if (isSummoned()) {
                        script.setInteractingPlayer(getSummoner());
                    }

                    if (((NPC) hitBy).isSummoned()) {
                        script.setInteractingPlayer(((NPC) hitBy).getSummoner());
                    }
                }

                NPCScriptEngine.evaluate(this, script);

                if (dont_die) {
                    dont_die = false;
                    super.respawn();

                    return;
                }
            }
        }

        if ((slayerInfo != null) && (hitBy instanceof Player)) {
            Player pla = ((Player) hitBy);

            if (pla.hasSlayerTask() && getSlayerInfo().is(pla.getSlayerTask().getTaskId())) {
                pla.registerSlayerKill(this);
                pla.addXp(18, slayerInfo.getExperienceGiven() * 3);
            }
        }

        final NPC npc = (NPC) this;

        if (getDefinition() != null) {
            this.getAnimation().animate(definition.getDeathAnim());
        } else {
            this.getAnimation().animate(2304);
        }

        reset_curse_drain();

        Player klr = null;

        if (klr == null) {
            if ((hitBy instanceof NPC) &&!isSummoned()) {
                return;
            }

            if (hitBy instanceof Player) {
                klr = (Player) hitBy;
            }
        }

        final Mob klr2 = klr;
        final Mob m    = npc.pollHighestHitter();

        if (this.getCutScene() == null) {
            this.getCombatHandler().reset();
            this.getFocus().unFocus();
            this.setMovement(new RoamingMovement(this));
        }

        int time = 100;

        if (definition != null) {
            time = definition.getRespawnTime();
        }

        World.getWorld().submitEvent(new WorldTickEvent(time) {
            @Override
            public void tick() {
                Player highestHitter = null;

                if (m instanceof NPC) {
                    highestHitter = (Player) klr2;
                } else {
                    highestHitter = (Player) m;
                }

                if (npc.isSummoned()) {
                    npc.getSummoner().getActionSender().sendMessage("Your familiar has died.");
                    npc.getSummoner().resetSummon();
                    terminate();

                    return;
                }

                if (((definition == null) && (timePassed() == 5))
                        || ((definition != null) && (timePassed() == npc.definition.getDeathTime()))) {
                    npc.setVisible(false);

                    ScriptVariableInjector injector = new ScriptVariableInjector() {
                        @Override
                        public void injectVariables(Scope scope) {
                            scope.declare(Text.longForName("npc"), npc);
                        }
                    };

                    if (highestHitter != null) {
                        Player plr = (Player) highestHitter;

                        if ((npc.getSize() < 2) ||!plr.getAccount().bossBlocked(npc.getId())) {
                            World.getWorld().getScriptManager().directTrigger(plr, injector, Trigger.NPC_KILLED,
                                    npc.getId());
                        }
                    }

                    if (deathHandler != null) {
                        deathHandler.handleDeath(npc, highestHitter);
                    }
                }

                if (npc.isRemoved) {
                    terminate();
                }
            }
            @Override
            public void finished() {
                npc.respawn();

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }
}
