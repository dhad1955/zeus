package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Equipment;

//~--- JDK imports ------------------------------------------------------------

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 12:35
 * To change this template use File | Settings | File Templates.
 */
import java.util.ArrayList;

/**
 * @author Gander
 * @Date: 23-Jul-2008
 * @Time: 08:45:30.
 */
public class EquipmentTypes {
    static final int[] TWO_HANDED = {
        16421, 16965, 18369, 16415, 18373, 16887, 18369, 16419, 20009, 20004, 20007, 20008, 20010, 18332, 15421, 13902,
        16421, 18353, 16909, 6528, 17295, 4153, 11730, 14484, 11700, 7158, 11696, 11694, 11235, 11698, 1237, 1239, 1241,
        4581, 1243, 1245, 1247, 1249, 1307, 1309, 1311, 1313, 1315, 1317, 1319, 841, 843, 849, 853, 857, 861, 839, 845,
        847, 851, 855, 859, 4212, 4718, 4755, 4747, 4726, 4734, 4710
    };
    static int FULL_HELM[] = {
        13263, 13876, 13362, 5574, 11718, 7594, 13864, 14084, 14087, 14090, 14096, 14075, 14078, 14081, 4551, 11335,
        1053, 1055, 1057, 1169, 1149, 3755, 3753, 3751, 3749, 4716, 5574, 6623, 4709, 7534, 4745, 4732, 4753, 6188,
        4511, 4056, 4071, 4724, 6109, 2665, 1153, 1155, 1157, 1159, 1161, 1163, 1165, 2587, 2595, 2605, 2613, 2619,
        2627, 2657, 2673, 3486, 6402, 6394
    };
    static final int[] CHAIN_MAILS = {
        1005, 7122, 7592, 7362, 7370, 7372, 7374, 7376, 7364, 1101, 1103, 1105, 1107, 1109, 1111, 1113, 1135, 2499,
        2501, 2503, 1129, 1131
    };
    static final int PLATEBODY[] = {
        14479, 11724, 577, 6786, 1127, 1005, 5030, 5032, 5034, 5024, 5026, 5028, 6129, 430, 6065, 75, 7134, 7122, 7110,
        7128, 7399, 5575, 6615, 6617, 2503, 7592, 6916, 6654, 7390, 7392, 7362, 7364, 7399, 7374, 7376, 7372, 7370,
        1035, 540, 5553, 4757, 1833, 6388, 6384, 2501, 2499, 4111, 4101, 4091, 6186, 6184, 6180, 3058, 4509, 4504, 4069,
        4728, 4736, 4712, 6107, 2661, 3140, 1101, 1103, 1105, 1107, 1109, 1111, 1113, 1115, 1117, 1119, 1121, 1123,
        1125, 1129, 1131, 1133, 1135, 2499, 2501, 2503, 2583, 2591, 2599, 2607, 2615, 2623, 2653, 2669, 3387, 3481,
        4712, 4720, 4728, 4749, 4892, 4893, 4894, 4895, 4916, 4917, 4918, 4919, 4964, 4965, 4966, 4967, 6107, 6133,
        6322, 6786, 1127, 3387, 5024, 5026, 5028, 5030, 5032, 5034, 6129, 430, 6065, 75, 7390, 7392, 7399, 5575, 6617,
        6916, 6625, 1035, 540, 5553, 4757, 1833, 1835, 6388, 6384, 2501, 2499, 4111, 4101, 4091, 6186, 6184, 6180, 3058,
        4509, 4504, 4069, 4728, 4736, 4712, 6107, 2661, 3140, 1115, 1117, 1119, 1121, 1123, 1125, 2583, 2591, 2599,
        2607, 2615, 2623, 2653, 2669, 3481, 4720, 4728, 4749, 2661
    };
    static int[] fullHelmets;

    static {
        ArrayList<Item> fullHelms = new ArrayList<Item>();

        for (int i = 0; i < 23000; i++) {
            try {
                if (Item.forId(i) == null) {
                    continue;
                }

                String name = Item.forId(i).getName().toLowerCase();

                if (name == null) {
                    continue;
                }

                if (name.contains("full") && name.contains("helm")) {
                    fullHelms.add(Item.forId(i));
                }

                if (Item.forId(i).getName().toLowerCase().contains("hood")
                        &&!Item.forId(i).getName().contains("Robin")) {
                    fullHelms.add(Item.forId(i));
                }

                if (Item.forId(i).getName().contains("coif")) {
                    fullHelms.add(Item.forId(i));
                }

                if (Item.forId(i).getName().contains("mask") || Item.forId(i).getName().contains("cowl")) {
                    if (!name.contains("face")) {
                        fullHelms.add(Item.forId(i));
                    }
                }
            } catch (Exception ee) {
                break;
            }
        }

        int[] fullhelms = {
            4753, 13355, 10828, 13263, 13355, 13350, 19336, 13876, 13362, 5574, 11718, 7594, 13864, 14084, 14087, 14090,
            14096, 14075, 14078, 14081, 4551, 11335, 1053, 1055, 1057, 1169, 1149, 3755, 3753, 3751, 3749, 4716, 5574,
            6623, 4709, 7534, 4745, 4732, 4753, 6188, 4511, 4056, 4071, 4724, 6109, 2665, 1153, 1155, 1157, 1159, 1161,
            1163, 1165, 2587, 2595, 2605, 2613, 2619, 2627, 2657, 2673, 3486, 6402, 6394
        };
        int   size  = fullHelms.size();
        int[] helms = new int[size + fullhelms.length];
        int   off   = 0;

        for (int i = 0; i < fullhelms.length; i++) {
            helms[off] = fullhelms[i];
            off++;
        }

        for (Item i2 : fullHelms) {
            i2.setFullHelm(true);
        }

        for (int i : fullhelms) {
            Item.forId(i).setFullHelm(true);
        }
    }

    /**
     * Method isTwoHanded
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public static boolean isTwoHanded(int item) {
        for (int i : TWO_HANDED) {
            if (i == item) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isFullHelmet
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public static boolean isFullHelmet(int item) {
        if (item == -1) {
            return false;
        }

        if ((item == 3057) || (item == 4164)) {
            return false;
        }




        return Item.forId(item).isFullHelm();
    }

    /**
     * Method needsArms
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
        public static boolean needsArms(int item) {
            Item check = Item.forId(item);

            String name = check.getName().toLowerCase();
            if(name.contains("chainbody") || name.contains("shirt") || (name.contains("hide") && name.contains("body")))
                return true;
        for (int i = 0; i < CHAIN_MAILS.length; i++) {
            if (CHAIN_MAILS[i] == item) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getItemSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public static int getItemSlot(int item) {
        Item theItem = Item.forId(item);

        if (item == 20072) {
            return Equipment.SHIELD;
        }

        if (theItem == null) {
            return 3;
        }

        if ((item == 7671) || (item == 7673)) {
            return 3;
        }

        if (item == 15007) {
            return 3;
        }

        if (theItem.getName().contains("ape") || theItem.getName().contains("attractor")
                || theItem.getName().contains("uiver") || theItem.getName().contains("cloak")
                || theItem.getName().contains("apparatus") || theItem.getName().contains("accumulator")) {
            return Equipment.CAPE;
        }

        if (theItem.getName().contains("oots")) {
            return Equipment.FEET;
        }

        if (theItem.getName().contains("oves") || theItem.getName().contains("gaunt")
                || theItem.getName().contains("vambraces")) {
            return Equipment.HANDS;
        }

        if ((theItem.getIndex() == 6524) || theItem.getName().contains("hield")
                || theItem.getName().toLowerCase().contains("defender") || theItem.getName().contains("book")
                || theItem.getName().contains("kite") || theItem.getName().contains("lantern")) {
            return Equipment.SHIELD;
        }

        if (theItem.getName().toLowerCase().contains("witchwood") || theItem.getName().contains("mulet")
                || theItem.getName().contains("scarf") || theItem.getName().contains("necklace")) {
            return Equipment.AMULET;
        }

        if (theItem.getName().contains("arrow") || theItem.getName().contains("olts") || (theItem.getIndex() == 15243)
                || (theItem.getIndex() == 4740)) {
            return Equipment.ARROWS;
        }

        if (theItem.getName().toLowerCase().contains("ring") &&!theItem.getName().toLowerCase().contains("master") || theItem.getName().toLowerCase().contains("bracelet")) {
            return Equipment.RING;
        }

        if ((theItem.getIndex() == 542) || theItem.getName().contains("slacks")
                || (theItem.getName().contains("robe") && theItem.getName().contains("illager"))
                || (theItem.getIndex() == 8840) || (theItem.getIndex() == 1835) || (theItem.getIndex() == 5576)
                || (theItem.getIndex() == 7398) || theItem.getName().contains("leg") || (theItem.getIndex() == 538)
                || (theItem.getIndex() == 1033) || theItem.getName().contains("chaps") || (theItem.getIndex() == 6108)
                || (theItem.getIndex() == 4070) || (theItem.getIndex() == 4505) || (theItem.getIndex() == 4510)
                || theItem.getName().contains("rousers") || (theItem.getIndex() == 10340)
                || theItem.getName().contains("skirt") || theItem.getName().toLowerCase().contains("shorts")
                || theItem.getName().toLowerCase().contains("tassets") || theItem.getName().contains("bottom")) {
            if (!theItem.getName().contains("shirt")) {
                return Equipment.LEGS;
            }
        }

        if ((theItem.getIndex() == 9070) || theItem.getName().toLowerCase().contains("body")
                || (theItem.getIndex() == 5575) || theItem.getName().toLowerCase().contains("chestplate")
                || theItem.getName().contains("top") || (theItem.getIndex() == 6107)
                || theItem.getName().contains("brassard") || theItem.getName().contains("torso")
                || (theItem.getIndex() == 544) || theItem.getName().equalsIgnoreCase("Runecrafter robe")
                || theItem.getName().toLowerCase().contains("shirt") || isTorso(theItem.getIndex())) {
            return Equipment.TORSO;
        }

        if (theItem.getName().toLowerCase().contains("nose") || theItem.getName().toLowerCase().contains("cowl")
                || (theItem.getName().contains("hat") &&!theItem.getName().contains("hatchet")) || theItem.getName().toLowerCase().equalsIgnoreCase("hat")
                || theItem.getName().contains("eret") || theItem.getName().toLowerCase().contains("cavalier")
                || theItem.getName().toLowerCase().contains("ears") || theItem.getName().contains("isguise")
                || theItem.getName().contains("oif") || theItem.getName().contains("helm")
                || theItem.getName().toLowerCase().contains("sallet") || theItem.getName().contains("Helm")
                || theItem.getName().contains("avlier") || theItem.getName().contains("ask")
                || theItem.getName().contains("uffs") || theItem.getName().contains("ood")
                || theItem.getName().contains("oater") || theItem.getName().toLowerCase().contains("crown")) {
            return Equipment.HAT;
        }

        // Else it is a weapon
        return Equipment.WEAPON;
    }

    /**
     * Method wearingRonaldMask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    boolean wearingRonaldMask(Player pla) {
        return pla.getEquipment().getId(Equipment.HAT) == 10629;
    }

    /**
     * Method isTorso
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    static boolean isTorso(int i) {
        for (Integer p : PLATEBODY) {
            if (p == i) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method wearingHood
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static boolean wearingHood(Player pla) {
        if(pla.getEquipment().getId(Equipment.CAPE) == 4041 || pla.getEquipment().getId(Equipment.CAPE) == 4042)
            return true;

        if (pla.getEquipment().getId(Equipment.HAT) == -1) {
            return false;
        }

        switch (pla.getEquipment().getId(Equipment.HAT)) {
        case 3749 :
        case 3750 :
        case 3751 :
        case 3754 :
        case 3753 :
            case 20147:
        case 10828 :
            return true;
        }

        Item i = Item.forId(pla.getEquipment().getId(Equipment.HAT));

        if(i == null)
            return false;

        if ((i != null) && i.getName().toLowerCase().contains("hood") || i.getName().toLowerCase().contains("coif") || i.getName().contains("med")) {
            return true;
        }

        return false;
    }
}
