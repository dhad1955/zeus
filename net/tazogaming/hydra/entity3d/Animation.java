package net.tazogaming.hydra.entity3d;

import net.tazogaming.hydra.runtime.Core;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 23/09/13
 * Time: 23:22
 * To change this template use File | Settings | File Templates.
 */
public class Animation {
    public static final int
        LOW_PRIORITY              = 0,
        HIGH_PRIORITY             = 1;
    private int     animationId   = -1;
    private int     animationWait = 0;
    private int     priority      = 0;
    private boolean hasChanged    = false;

    /**
     * Method animate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param wait
     * @param priority
     */
    public void animate(int id, int wait, int priority) {


        if ((this.priority > priority) && (animationId != -1)) {
            return;
        }

        animationId   = id;
        animationWait = wait;
        hasChanged    = true;
        this.priority = priority;
    }

    public void reset() {
        this.animationId = -1;
        this.priority = -1;
    }

    /**
     * Method animate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param wait
     */
    public void animate(int id, int wait) {
        animate(id, wait, 1);
    }

    /**
     * Method animate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void animate(int id) {

        animate(id, 0);
    }

    /**
     * Method getIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getIndex() {
        return animationId;
    }

    /**
     * Method getAnimationWait
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAnimationWait() {
        return animationWait;
    }

    /**
     * Method hasChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasChanged() {
        return hasChanged;
    }

    /**
     * Method setChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setChanged(boolean b) {
        hasChanged = b;
    }
}
