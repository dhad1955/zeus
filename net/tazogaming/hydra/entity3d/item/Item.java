package net.tazogaming.hydra.entity3d.item;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Ammunition;
import net.tazogaming.hydra.entity3d.EquipmentTypes;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.skill.combat.potion.Potion;
import net.tazogaming.hydra.game.ui.SkillGuide;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchange;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchangeItemSetsInterface;
import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.io.js5.format.CacheItemDefinition;
import net.tazogaming.hydra.io.tfs.savegame.impl.GrandExchangeSectorCodec;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 01:11
 * To change this template use File | Settings | File Templates.
 */
public class Item {
    public static final int
        BONUS_ATTACK_STAB   = 0,
        BONUS_ATTACK_SLASH  = 1,
        BONUS_ATTACK_CRUSH  = 2,
        BONUS_ATTACK_MAGIC  = 3,
        BONUS_ATTACK_RANGE  = 4,
        BONUS_DEFENCE_STAB  = 5,
        BONUS_DEFENCE_SLASH = 6,
        BONUS_DEFENCE_CRUSH = 7,
        BONUS_DEFENCE_MAGIC = 8,
        BONUS_DEFENCE_RANGE = 9,
        BONUS_STRENGTH      = 10,
        BONUS_PRAYER        = 11,
        BONUS_SOAK_MELEE    = 12,
        BONUS_SOAK_RANGE     = 13,
        BONUS_SOAK_MAGE      = 14,
        BONUS_MAGE_BOOST = 15;

    /** itemCache made: 14/09/19 */
    private static Item[]        itemCache   = new Item[21000];
    public static final String[] BONUS_NAMES = {
        "Attack-Stab", "Attack-Slash", "Attack-Crush", "Attack-Magic", "Attack-Range", "Defence-Stab", "Defence-Slash",
        "Defence-Crush", "Defence-Magic", "Defence-Range", "Strength", "Prayer", "SoakMelee", "SoakRange", "SoakMage", "Magic Damage"
    };

    /** formatWriter made: 14/09/19 */
    private static BufferedWriter formatWriter;

    /** examine made: 14/09/19 */
    private String examine = "It doesn't look that interesting.";

    /** itemName made: 14/09/19 */
    private String itemName = "un-named";

    /** itemIndex made: 14/09/19 */
    private int itemIndex = -1;

    /** itemPrice made: 14/09/19 */
    private int itemPrice = 0;

    /** tradeable made: 14/09/19 */
    private boolean tradeable = false;

    /** noteable made: 14/09/19 */
    private boolean noteable = false;

    /** isStackable made: 14/09/19 */
    private boolean isStackable = false;

    /** wieldSlot made: 14/09/19 */
    private byte wieldSlot = -1;

    /** bonuses made: 14/09/19 */
    private int[] bonuses = new int[16];

    /** amunitionData made: 14/09/19 */
    private Ammunition amunitionData = null;

    /** fullHelm made: 14/09/19 */
    private boolean fullHelm = false;

    /** weight made: 14/09/19 */
    private double weight = 0.0;

    /** isLend made: 14/09/19 */
    private boolean isLend = false;

    /** equipId made: 14/09/19 */
    private int equipId = -1;

    /** isDungItem made: 14/09/19 */
    private boolean isDungItem = false;

    /** potionHandler made: 14/09/19 */
    private Potion potionHandler;

    /** handler made: 14/09/19 */
    private Weapon handler;

    /** renderEmote made: 14/09/19 */
    private int renderEmote;

    /** level_require_ids[] made: 14/09/19 */
    private byte level_require_ids[];

    /** ourItem made: 14/09/19 */
    private DegradeableItem ourItem;

    /** level_require_level[] made: 14/09/19 */
    private byte level_require_level[];

    public int getNotedId() {
        return notedId;
    }

    private int notedId;

    public int getUnNotedId() {
        return unNotedId;
    }

    private int unNotedId;


    /**
     * Method dumpItemDatabase
     * Created on 14/09/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @throws IOException
     */
    public static void dumpItemDatabase() throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("DUMP.sql"));

        writer.write("INSERT INTO itemdb(id, name, value) VALUES");

        for (int i = 0; i < getItems().length; i++) {
            writer.newLine();
            writer.write("('" + itemCache[i].getIndex() + "', '" + itemCache[i].getName().replace("'", "") + "', "
                         + itemCache[i].getPrice() + "),");

            if (i % 100 == 0) {
                System.err.println("Dumped " + i + " - " + GameMath.getPercentFromTotal(i, 20000));
            }
        }

        writer.flush();
        writer.close();
    }

    /**
     * Method loadNewFormat
     * Created on 15/02/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public static void loadNewFormat(Player plr) {
        TextFile file      = TextFile.fromPath("config/item/item_definitions.cfg");
        int      currentId = -1;

        String   data;

        System.err.println("loading item definition");
        for (Line line : file) {
            data = line.getData();
        try {
            if (data.startsWith("[")) {

                if (currentId != -1 && itemCache[currentId] != null) {
                    Item item = itemCache[currentId];
                    if (item.getWieldSlot() == Equipment.HAT) {
                        if (item.getName().toLowerCase().contains("full")
                                || item.getName().toLowerCase().contains("mask")) {
                            item.setFullHelm(true);
                        }
                    }

                    item.isDungItem = (item.getIndex() <= 18014) && (item.getIndex() >= 15753);

                }

                currentId = Integer.parseInt(data.substring(1, data.lastIndexOf("]")));

                if (itemCache[currentId] == null)
                    itemCache[currentId] = new Item();
                itemCache[currentId].itemIndex = currentId;
                continue;

            }

                if (data.startsWith("//") || (data.length() == 0)) {
                    continue;
                }

                String[] modifier = data.split("=", 2);

                if (modifier.length < 2) {
                    System.err.println("unable to parse line: " + line.getLineNumber()+" "+line.getData()+" "+ Arrays.toString(modifier));
                    System.exit(-1);
                    continue;
                }

                if (modifier[0].equalsIgnoreCase("stackable")) {
                    itemCache[currentId].setStackable(Boolean.parseBoolean(modifier[1]));
                } else if (modifier[0].equalsIgnoreCase("tradeable"))
                    itemCache[currentId].setTradeable(Boolean.parseBoolean(modifier[1]));
                else if (modifier[0].equalsIgnoreCase("desc"))
                    itemCache[currentId].examine = modifier[1];
                else if (modifier[0].equalsIgnoreCase("shop_price"))
                    itemCache[currentId].itemPrice = Integer.parseInt(modifier[1]);
                else if (modifier[0].equalsIgnoreCase("exchange_price"))
                    itemCache[currentId].exchangePrice = Integer.parseInt(modifier[1]);
                else if (modifier[0].equalsIgnoreCase("render_emote"))
                    itemCache[currentId].renderEmote = Integer.parseInt(modifier[1]);
                else if (modifier[0].equalsIgnoreCase("wieldslot"))
                    itemCache[currentId].wieldSlot = (byte) Integer.parseInt(modifier[1]);
                else if (modifier[0].equalsIgnoreCase("name"))
                    itemCache[currentId].itemName = modifier[1];
                else if (modifier[0].equalsIgnoreCase("notedid")) {
                    Item notedItem = itemCache[Integer.parseInt(modifier[1])];
                    Item curItem = itemCache[currentId];
                    if (notedItem == null)
                        itemCache[Integer.parseInt(modifier[1])] = notedItem = new Item();

                    notedItem.itemName = curItem.itemName;

                    notedItem.itemPrice = curItem.itemPrice;
                    notedItem.itemIndex = Integer.parseInt(modifier[1]);
                    notedItem.tradeable = curItem.tradeable;
                    notedItem.exchangePrice = curItem.exchangePrice;
                    curItem.notedId = notedItem.getIndex();
                    notedItem.wieldSlot = curItem.wieldSlot;
                    notedItem.weight = curItem.weight;
                    notedItem.examine = curItem.examine;
                    notedItem.isStackable = true;
                    notedItem.noteable = true;
                    notedItem.unNotedId = currentId;
                }



        }catch (Exception ee) {
            ee.printStackTrace();
            System.err.println("error loading line: "+ line.getData()+" "+line.getLineNumber());
            return;
        }
        }


        try {
            loadBonuses(Main.getConfigPath() + "/item/bonus.cfg", plr);

            loadList(new File(Main.getConfigPath() + "/item/level_requirements.cfg"));
        } catch (Exception ee) {
            ee.printStackTrace();
        }

    }

    public void setExchangePrice(int price){
        this.exchangePrice = price;
    }

    int exchangePrice = 0;
    /**
     * Method dumpNewFormat
     * Created on 15/02/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void dumpNewFormat() {
        TextFile textFile = new TextFile();

        for (Item i : itemCache) {
            if ((i != null) &&!i.isNote()) {
                textFile.push("[" + i.getIndex() + "]");
                textFile.push("name=" + i.getName());
                textFile.push("stackable=" + i.isStackable());
                textFile.push("tradeable=" + i.tradeable);
                textFile.push("allowedwild=true");


                textFile.push("shop_price=" + i.getPrice());
                textFile.push("exchange_price=" + i.getPrice());
                textFile.push("desc=" + i.examine);
                textFile.push("render_emote=" + i.renderEmote);
                textFile.push("weight=" + i.weight);
                textFile.push("wieldslot=" + i.getWieldSlot());
                if ((i.getIndex() + 1 < itemCache.length) && (forId(i.getIndex() + 1) != null)
                        && forId(i.getIndex() + 1).isNote()) {
                    int x = i.getIndex() + 1;
                    textFile.push("//noted id must be last option");
                    textFile.push("notedid=" + x);
                }

                textFile.push(" ");
            }
        }

        try {
            textFile.save("newitems.cfg");
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    private static void loadEquipIds() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("config/item/equipids.txt"));

            do {
                String line = reader.readLine();

                if (line == null) {
                    break;
                }

                if (!line.contains(":")) {
                    continue;
                }

                String[] split = line.split(":");

                try {
                    forId(Integer.parseInt(split[0])).equipId = Integer.parseInt(split[1]);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            } while (true);

            reader.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method isLend
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public static boolean isLend(int itemId) {
        switch (itemId) {
        case 13422 :
        case 13444 :
        case 13443 :
        case 13450 :
        case 13451 :
        case 13453 :
        case 13454 :
        case 13405 :
        case 14486 :
        case 15502 :
            return true;
        }

        return false;
    }

    /**
     * Method setLend
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setLend(boolean b) {
        this.isLend = b;
    }

    /**
     * Method isLend
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isLend() {
        return isLend;
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        return getName();
    }

    /**
     * Method isDegradeable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDegradeable() {
        return ourItem != null;
    }

    private static byte[] splitLevelReqs(String str) {
        String[] split      = str.split(",");
        byte[]   returnData = new byte[split.length];

        for (int i = 0; i < split.length; i++) {
            returnData[i] = Byte.parseByte(split[i]);
        }

        return returnData;
    }

    /**
     * Method level_check
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param player
     *
     * @return
     */
    public static boolean level_check(Item item, Player player) {
        if ((item == null) || (item.level_require_ids == null)) {
            return true;
        }

        for (int i = 0; i < item.level_require_ids.length; i++) {
            if (player.getMaxStat(item.level_require_ids[i]) < item.level_require_level[i]) {
                player.getActionSender().sendMessage("You need a " + SkillGuide.SKILL_NAMES[item.level_require_ids[i]]
                        + " level of atleast " + item.level_require_level[i] + " to equip this.");

                return false;
            }
        }

        return true;
    }

    /**
     * Method loadList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     *
     * @throws IOException
     */
    public static void loadList(File f) throws IOException {
        if (!f.exists()) {
            return;
        }

        BufferedReader br = new BufferedReader(new FileReader(f));
        String         line;

        do {
            line = br.readLine();

            if (line == null) {
                break;
            }

            if (line.equals("") || (line.length() == 0) || line.startsWith("//")) {
                continue;
            }

            try {
                String[] split              = line.split(" ");
                int      itemId             = Integer.parseInt(split[0]);
                byte[]   skillids           = splitLevelReqs(split[1]);
                byte[]   levelRequirementss = splitLevelReqs(split[2]);

                Item.forId(itemId).level_require_ids   = skillids;
                Item.forId(itemId).level_require_level = levelRequirementss;
            } catch (Exception ignored) {}
        } while (true);

        br.close();
        loadEquipIds();
    }

    /**
     * Method isFullHelm
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isFullHelm() {
        return fullHelm;
    }

    /**
     * Method getEquipID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getEquipID() {
        return equipId;
    }

    /**
     * Method setFullHelm
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param fullHelm
     */
    public void setFullHelm(boolean fullHelm) {
        this.fullHelm = fullHelm;
    }

    /**
     * Method getPotionHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Potion getPotionHandler() {
        return potionHandler;
    }

    /**
     * Method equals
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public boolean equals(Item item) {
        return item.getIndex() == getIndex();
    }

    /**
     * Method setPotionHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param potionHandler
     */
    public void setPotionHandler(Potion potionHandler) {
        this.potionHandler = potionHandler;
    }

    /**
     * Method setAmmoData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void setAmmoData(Ammunition e) {
        this.amunitionData = e;
    }

    /**
     * Method isAmmunition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isAmmunition() {
        return amunitionData != null;
    }

    /**
     * Method getAmmoData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */

    public int getExchangePrice() {
        return exchangePrice;
    }
    public Ammunition getAmmoData() {
        return amunitionData;
    }

    /**
     * Method getWeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Method setWeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param weight
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    /**
     * Method setWeaponHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param w
     */
    public void setWeaponHandler(Weapon w) {
        handler = w;
    }

    /**
     * Method getWeaponHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Weapon getWeaponHandler() {
        return handler;
    }

    /**
     * Method isStackable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isStackable() {
        return isStackable;
    }

    /**
     * Method isNote
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isNote() {
        return noteable;
    }

    /**
     * Method isTradeable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isTradeable(Player player) {
        if ((player.getRights() == Player.ROOT_ADMIN) || (player.getRights() == Player.ADMINISTRATOR)) {
            return true;
        }

        if(BossPets.getByItem(this.getIndex()) != null)
            return false;

        if(player.getAccount().getIronManMode() > 0 && this.itemIndex == 15014)
            return false;

        if (isDungItem) {
            return false;
        }

        if (isLend(itemIndex)) {
            return false;
        }

        if (isDegradeable()) {
            return ourItem.getTier(itemIndex) == 0;
        }

        if (ClueManager.isClue(getIndex())) {
            return false;
        }

        if (itemIndex == 11283) {
            return true;
        }

        return isTradeable();
    }

    /**
     * Method isTradeable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTradeable() {
        if (isDegradeable()) {
            return ourItem.getTier(itemIndex) == 0;
        }

        return tradeable;
    }

    /**
     * Method getPrice
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPrice() {
        if (isNote() && forId(getIndex() - 1).getName().equalsIgnoreCase(getName())) {
            return forId(getIndex() - 1).getPrice();
        } else {
            return itemPrice;
        }
    }

    /**
     * Method getIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getIndex() {
        return itemIndex;
    }

    /**
     * Method getWieldSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWieldSlot() {
        if (wieldSlot == -1) {
            wieldSlot = (byte) EquipmentTypes.getItemSlot(itemIndex);
        }

        return wieldSlot;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return itemName;
    }

    /**
     * Method hasBonus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasBonus() {
        return bonuses != null;
    }

    /**
     * Method getBonus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getBonus(int id) {
        return bonuses[id];
    }

    /**
     * Method getDegradeable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public DegradeableItem getDegradeable() {
        return ourItem;
    }

    /**
     * Method setupDegradeables
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void setupDegradeables() {

        /*
         *  //Ahrim's hood 100
         * new DegradeableItem(4708, DegradeableItem.POLICY_DEGRADE, 2, 4856,4857,4858,4859,4860);
         * new DegradeableItem(4710, DegradeableItem.POLICY_DEGRADE, 2, 4862,4863,4864,4865,4866);
         * new DegradeableItem(4712, DegradeableItem.POLICY_DEGRADE, 2, 4868,4869,4870,4871,4872);
         * new DegradeableItem(4714, DegradeableItem.POLICY_DEGRADE, 2, 4874,4875,4876,4877,4878);
         * new DegradeableItem(4716, DegradeableItem.POLICY_DEGRADE, 2, 4880,4881,4882,4883,4884);
         * new DegradeableItem(4718, DegradeableItem.POLICY_DEGRADE, 2, 4886,4887,4888,4889,4890);
         * new DegradeableItem(4720, DegradeableItem.POLICY_DEGRADE, 2, 4892,4893,4894,4895,4896);
         * new DegradeableItem(4722, DegradeableItem.POLICY_DEGRADE, 2, 4898,4899,4900,4901,4902);
         * new DegradeableItem(4724, DegradeableItem.POLICY_DEGRADE, 2, 4904,4905,4906,4907,4908);
         * new DegradeableItem(4726, DegradeableItem.POLICY_DEGRADE, 2, 4910,4911,4912,4913,4914);
         * new DegradeableItem(4728, DegradeableItem.POLICY_DEGRADE, 2, 4916,4917,4918,4919,4920);
         * /Guthan's chainskirt 100
         * new DegradeableItem(4730, DegradeableItem.POLICY_DEGRADE, 2, 4922,4923,4924,4925,4926);
         * /Karil's coif 100
         * new DegradeableItem(4732, DegradeableItem.POLICY_DEGRADE, 2, 4928,4929,4930,4931,4932);
         * /Karil's crossbow 100
         * new DegradeableItem(4734, DegradeableItem.POLICY_DEGRADE, 2, 4934,4935,4936,4937,4938);
         * /Karil's top 100
         * new DegradeableItem(4736, DegradeableItem.POLICY_DEGRADE, 2, 4940,4941,4942,4943,4944);
         * /Karil's skirt 100
         * new DegradeableItem(4738, DegradeableItem.POLICY_DEGRADE, 2, 4946,4947,4948,4949,4950);
         * /Torag's helm 100
         * new DegradeableItem(4745, DegradeableItem.POLICY_DEGRADE, 2, 4952,4953,4954,4955,4956);
         * /Torag's hammers 100
         * new DegradeableItem(4747, DegradeableItem.POLICY_DEGRADE, 2, 4958,4959,4960,4961,4962);
         * /Torag's platebody 100
         * new DegradeableItem(4749, DegradeableItem.POLICY_DEGRADE, 2, 4964,4965,4966,4967,4968);
         * /Torag's platelegs 100
         * new DegradeableItem(4751, DegradeableItem.POLICY_DEGRADE, 2, 4970,4971,4972,4973,4974);
         * /Verac's helm 100
         * new DegradeableItem(4753, DegradeableItem.POLICY_DEGRADE, 2, 4976,4977,4978,4979,4980);
         * /Verac's flail 100
         * new DegradeableItem(4755, DegradeableItem.POLICY_DEGRADE, 2, 4982,4983,4984,4985,4986);
         * /Verac's brassard 100
         * new DegradeableItem(4757, DegradeableItem.POLICY_DEGRADE, 2, 4988,4989,4990,4991,4992);
         * /Verac's plateskirt 100
         * new DegradeableItem(4759, DegradeableItem.POLICY_DEGRADE, 2, 4994,4995,4996,4997,4998);
         *
         *
         * // crystal bow        ;
         *
         * new DegradeableItem(4212, DegradeableItem.POLICY_DEGRADE, Core.getTicksForMinutes(30), 4214,4215,4216,4217,4218,4219,4220, 4221, 4222, 4223);
         */
    }

    /**
     * Method setTradeable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setTradeable(boolean b) {
        tradeable = b;
    }

    private static String loadExamine(String data, String name) {
        for (int i = 0; i < data.length(); i++) {
            if (!Character.isLetterOrDigit(data.charAt(i)) && (data.charAt(i) != ' ') && (data.charAt(i) != ' ')
                    && (data.charAt(i) != '!') && (data.charAt(i) != '\'') && (data.charAt(i) != ',')) {
                return "It's a " + name;
            }
        }

        return data;
    }

    /**
     * Method loadItemList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param reload
     * @param player
     *
     * @throws IOException
     */
    public static void loadItemList(boolean reload, Player player) throws IOException {

       loadNewFormat(player);
        if(true)
          return;

        File f           = new File(Main.getConfigPath() + "item/items.cfg");
        int  count       = 0;
        int  currentLine = 0;

        if (f.exists()) {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(f));
            String         line;

            do {
                count++;
                line = bufferedReader.readLine();
                currentLine++;

                if (line == null) {
                    break;
                }

                if ((line.length() == 0) || line.startsWith(" ")) {
                    continue;
                }

                try {
                    String[] split = line.split(" ");
                    Item     item  = new Item();

                    item.itemIndex = Integer.parseInt(split[0]);

                    if (split.length < 2) {
                        continue;
                    }

                    item.itemName    = (split[1].replaceAll("_", " "));
                    item.examine     = split[2].replaceAll("_", " ");
                    item.noteable    = Boolean.parseBoolean(split[3]);
                    item.isStackable = Boolean.parseBoolean(split[4]);
                    item.itemPrice   = Integer.parseInt(split[5]);
                    item.tradeable   = Boolean.parseBoolean(split[6]);
                    item.renderEmote = Integer.parseInt(split[7]);
                    item.weight      = 0.0;

                    String name = item.itemName.toLowerCase();

                    if (item.getName().toLowerCase().endsWith("rune")
                            || item.getName().toLowerCase().endsWith(" knife")
                            || item.getName().toLowerCase().contains("dart")
                            || item.getName().toLowerCase().contains("thrownaxe")
                            || item.getName().toLowerCase().contains("javelin")) {
                        item.isStackable = (true);
                    }

                    if (item.getWieldSlot() == Equipment.HAT) {
                        if (item.getName().toLowerCase().contains("full")
                                || item.getName().toLowerCase().contains("mask")) {
                            item.setFullHelm(true);
                        }
                    }

                    item.isDungItem = (item.getIndex() <= 18014) && (item.getIndex() >= 15753);

                    if (reload) {
                        itemCache[item.getIndex()].isStackable = item.isStackable;
                        itemCache[item.getIndex()].noteable    = item.noteable;
                        itemCache[item.getIndex()].tradeable   = item.isTradeable();
                        itemCache[item.getIndex()].fullHelm    = item.isFullHelm();
                        itemCache[item.getIndex()].itemPrice   = item.getPrice();
                        itemCache[item.getIndex()].wieldSlot   = (byte) EquipmentTypes.getItemSlot(item.getIndex());
                        itemCache[item.getIndex()].itemName    = item.getName();
                    } else {
                        itemCache[item.getIndex()] = item;
                    }

                    item.wieldSlot = (byte) EquipmentTypes.getItemSlot(item.itemIndex);

                    if (item.getIndex() == 20135) {
                        int slot = item.getWieldSlot();
                    }

                    if (item.getWieldSlot() == Equipment.HAT) {
                        if (item.getName().toLowerCase().contains("full")
                                || item.getName().toLowerCase().contains("mask")) {
                            item.setFullHelm(true);
                        }
                    }

                    item.isDungItem = (item.getIndex() <= 18014) && (item.getIndex() >= 15753);
                } catch (Exception ee) {
                    if (player != null) {
                        player.getActionSender().sendDev("Error loading from newitems.txt parse error at line "
                                                         + currentLine);
                    }

                    // system.out.println("Error: loading items "+currentLine);
                }
            } while (true);
        } else {
            throw new FileNotFoundException();
        }

        if (player != null) {
            player.getActionSender().sendDev("Loaded " + count + " item definitions");
        }

        Logger.log("[ItemDefinitions]Loaded: " + count + " bonus definitions");
        loadBonuses(Main.getConfigPath() + "/item/bonus.cfg", player);

        try {
            loadList(new File(Main.getConfigPath() + "/item/level_requirements.cfg"));
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        setupDegradeables();
    }

    /**
     * Method setDegradeable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param degradeable
     */
    public void setDegradeable(DegradeableItem degradeable) {
        this.ourItem = degradeable;
    }

    /**
     * Method isDungItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDungItem() {
        return isDungItem;
    }

    /**
     * Method getExamine
     * Created on 15/02/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getExamine() {
        return examine;
    }

    /**
     * Method loadBonuses
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     * @param player
     *
     * @throws IOException
     */
    public static void loadBonuses(String path, Player player) throws IOException {
        File           file = new File(path);
        String         line;
        int            lineNumber = 0;
        int            tempNum    = -1;
        int            curItemId  = -1;
        int[]          bonuses    = new int[15];
        int            load       = 0;
        BufferedReader br         = new BufferedReader(new FileReader(file));
        boolean        reading    = false;

        do {
            line = br.readLine();

            if (line == null) {
                break;
            }

            lineNumber++;

            if (line.startsWith("bonus")) {
                int index = line.indexOf("{");

                if (index == -1) {
                    throw new IOException("Failed to read bonus (Line: " + lineNumber);
                }

                String[] split = line.split(" ");

                curItemId = Integer.parseInt(split[1]);
                bonuses   = new int[16];
                reading   = true;

                continue;
            }

            if (reading && line.contains("}")) {
                reading                       = false;
                Item.forId(curItemId).bonuses = bonuses;
                bonuses                       = null;
                curItemId                     = -1;
                load++;

                continue;
            }

            if (reading) {
                String[] split = line.split(" ");

                try {
                    String mode  = split[0];
                    String value = split[2];
                    int    val2  = Integer.parseInt(value);
                    int    index = -1;

                    if(mode.equalsIgnoreCase("magic_damage")) {
                        index = BONUS_MAGE_BOOST;
                        System.err.println(mode);

                    }else {
                        for (int i = 0; i < Item.BONUS_NAMES.length; i++) {
                            if (Item.BONUS_NAMES[i].equalsIgnoreCase(mode)) {
                                index = i;

                                break;
                            }
                        }
                    }

                    if (index == -1) {
                        continue;
                    }

                    bonuses[index] = val2;

                    if (Item.forId(curItemId).isDungItem) {
                        if ((index == BONUS_ATTACK_RANGE) || (index == BONUS_ATTACK_SLASH)
                                || (index == BONUS_ATTACK_CRUSH) || (index == BONUS_ATTACK_STAB)) {
                            bonuses[index] += 38;
                        }
                    }
                } catch (Exception er) {
                    if (player != null) {
                        player.getActionSender().sendDev("Error parsing bonuses, parse error at line: " + lineNumber);
                    }

                    // Logger.log("Bonus parse error: "+lineNumber);
                    // System.exit(-1);
                }
            }
        } while (true);

        if (player != null) {
            player.getActionSender().sendDev("Loaded " + load + " bonus configs");
        }

        Logger.log("[ItemDefinitions]Loaded: " + load + " bonus configs");
    }

    /**
     * Method setStackable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param stack
     */
    public void setStackable(boolean stack) {
        this.isStackable = stack;
    }

    /**
     * Method forName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public static Item forName(String str) {
        for (int i = 0; i < itemCache.length; i++) {
            if ((itemCache[i] != null) && itemCache[i].getName().equalsIgnoreCase(str)) {
                return itemCache[i];
            }
        }

        return null;
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Item[] getItems() {
        return itemCache;
    }

    /**
     * Method forId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public static Item forId(int itemId) {
        if (itemId == -1) {
            return null;
        }

        if (itemCache[itemId] == null) {
            itemCache[itemId]           = new Item();
            itemCache[itemId].itemIndex = itemId;
        }

        return itemCache[itemId];
    }
}
