package net.tazogaming.hydra.entity3d.item;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.gameplay.ActionTrigger;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.security.IPFlags;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 19:14
 * To change this template use File | Settings | File Templates.
 */
public class FloorItem extends Killable {
    private int           itemAmount = 0;
    private int           itemId     = 0;
    private Player        visableTo  = null;
    private int           droppedAt  = 0;
    protected boolean     isGlobal   = false;
    private boolean       isRemoved  = false;
    protected boolean     dontRemove = false;
    private int           oldAmount  = 0;
    private ActionTrigger onRemovalActionTrigger;
    private ActionTrigger onPickupActionTrigger;

    /**
     * Constructs ...
     *
     *
     * @param itemId
     * @param amount
     * @param x
     * @param y
     * @param h
     * @param droppedBy
     */
    public FloorItem(int itemId, int amount, int x, int y, int h, Player droppedBy) {
        this.itemId     = itemId;
        this.visableTo  = droppedBy;
        this.itemAmount = amount;
        this.itemId     = itemId;
        this.droppedAt  = Core.currentTime;

    try {
        if (Item.forId(itemId).isDegradeable() && (Item.forId(itemId).getDegradeable().getMain() != itemId)
                && ((Item.forId(itemId).getDegradeable().getLast() != itemId)
                && (Item.forId(itemId).getDegradeable().getPolicy() == DegradeableItem.POLICY_DEGRADE))) {
            droppedBy.getAccount().removeDegrading(itemId);
            this.itemId = Item.forId(itemId).getDegradeable().getLast();
        }

        if (Item.forId(itemId).isStackable()) {
            Tile t = World.getWorld().getTile(x, y, h);

            if ((t != null) && (t.getItems() != null)) {
                for (FloorItem i : t.getItems()) {
                    if (i.getCurrentInstance() != getCurrentInstance()) {
                        Logger.log("found interfaces on this tile.");

                        return;
                    }

                    if ((i.getItemId() == itemId) && (i.isVisableTo(droppedBy) || i.isGlobal)) {
                        long l = ((long) i.getAmount() + (long) amount);

                        if (l >= Integer.MAX_VALUE) {
                            return;
                        }

                        i.updateAmount(i.getAmount() + amount);
                        i.droppedAt = Core.currentTime;

                        return;
                    }
                }
            }
        }

        if (droppedBy != null) {
            setCurrentInstance(droppedBy.getCurrentInstance());
        }

        if (getCurrentInstance() != null) {
            getCurrentInstance().add(this);
        }

        this.setLocation(Point.location(x, y, h));
        World.getWorld().registerFloorItem(this);
    }catch (Exception ee) {}
    }

    /**
     * Method setRemovalActionTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param trigger
     */
    public void setRemovalActionTrigger(ActionTrigger trigger) {
        onRemovalActionTrigger = trigger;
    }

    /**
     * Method setOnPickupActionTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param trigger
     */
    public void setOnPickupActionTrigger(ActionTrigger trigger) {
        onPickupActionTrigger = trigger;
    }

    /**
     * Method getDropper
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getDropper() {
        return visableTo;
    }

    /**
     * Method setDontRemove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setDontRemove(boolean b) {
        this.dontRemove = b;
    }

    /**
     * Method updateAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     */
    public void updateAmount(int amount) {
        oldAmount  = itemAmount;
        itemAmount = amount;
    }

    /**
     * Method getOldAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOldAmount() {
        return oldAmount;
    }

    /**
     * Method isVisableTo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean isVisableTo(Player pla) {
        return visableTo == pla;
    }

    /**
     * Method globalise
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void globalise() {
        isGlobal = true;
    }

    /**
     * Method getAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAmount() {
        return itemAmount;
    }

    /**
     * Method getItemId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getItemId() {
        return this.itemId;
    }

    /**
     * Method isRemoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isRemoved() {
        return isRemoved;
    }

    /**
     * Method isGlobal
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isGlobal() {
        return isGlobal;
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void remove() {
        this.isRemoved = true;

        if (getCurrentInstance() != null) {
            getCurrentInstance().remove(this);
        }
    }

    /**
     * Method setPickupper
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void setPickupper(Player pla) {
        visableTo = pla;
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        try {
            if (Core.timeSince(droppedAt) >= Config.FLOOR_ITEM_GLOBAL_TIME) {
                if ((getItemId() != -1) && (Item.forId(getItemId()) != null) && Item.forId(getItemId()).isTradeable()) {
                    isGlobal = true;

                    if ((visableTo != null) && (visableTo.getAccount().hasVar("trade_lock") || visableTo.getAccount().getIronManMode() > 0)) {
                        isGlobal = false;
                    }

                    if(itemId >= 1038 && itemId <= 1050){
                        isGlobal = false;
                    }


                    if ((itemId >= 1038) && (itemId <= 1050)) {
                        if (getViewArea().getPlayersInView().size() >= 50) {
                            if (!IPFlags.exists(getDropper().getUID(), 8)) {
                                IPFlags.put(getDropper().getUID(), 8);
                            }
                        }
                    }
                }

                if ((Core.timeSince(droppedAt) >= Config.FLOOR_ITEM_REMOVAL_TIME) &&!dontRemove) {
                    if ((onRemovalActionTrigger == null)
                            || ((onRemovalActionTrigger != null)
                                && onRemovalActionTrigger.allowAction(visableTo, this))) {
                        isRemoved = true;
                    }
                }
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method timerUp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param timer
     */
    @Override
    public void timerUp(int timer) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getCurrentHealth
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public double getCurrentHealth() {
        return 0;
    }

    /**
     * Method getMaxHealth
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public double getMaxHealth() {
        return 0;
    }


    /**
     * Method addHealth
     * Created on 14/08/18
     *
     * @param health
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void addHealth(double health) {

    }

    /**
     * Method removeHealth
     * Created on 14/08/18
     *
     * @param currentHealth
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void removeHealth(double currentHealth) {

    }

    /**
     * Method addHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param health
     */

    public void addHealth(int health) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method removeHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentHealth
     */


    /**
     * Method onHitBy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     * @param d
     */
    @Override
    public void onHitBy(Mob hitBy, Damage d) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method entityKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killedBy
     */
    @Override
    public void entityKilled(Mob killedBy) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void onMoved(Point movedFrom, Point movedTo, int direction) {

    }
}
