package net.tazogaming.hydra.entity3d.item;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/12/13
 * Time: 13:27
 */
public class DegradeableItem {
    public static final int
        POLICY_DELETE  = 0,
        POLICY_DEGRADE = 1;
    public int[] tiers;
    public int   policy;
    private int  mainItemId;
    public int   degradeTime;

    /**
     * Constructs ...
     *
     *
     * @param itemId
     * @param policy
     * @param degradeTime
     * @param tiers
     */
    public DegradeableItem(int itemId, int policy, int degradeTime, int... tiers) {
        if (degradeTime == 2) {
            degradeTime = Core.getTicksForMinutes(45);
        }

        this.policy      = policy;
        this.degradeTime = degradeTime;
        this.mainItemId  = itemId;
        Item.forId(itemId).setDegradeable(this);

        for (Integer ig : tiers) {
            Item.forId(ig).setDegradeable(this);
            Item.forId(ig).setTradeable(false);
        }

        int[] newTiers = new int[tiers.length + 1];

        newTiers[0] = mainItemId;

        for (int i = 1; i < newTiers.length; i++) {
            newTiers[i] = tiers[i - 1];
        }

        this.tiers = newTiers;
    }

    /**
     * Method getLast
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLast() {
        return tiers[tiers.length - 1];
    }

    /**
     * Method getPolicy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPolicy() {
        return policy;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     *
     * @return
     */
    public int next(int itemid) {
        int tier = getTier(itemid);

        if ((tier == -1) || (tier + 1) >= tiers.length) {
            return -1;
        }

        return tiers[tier + 1];
    }

    /**
     * Method getTier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     *
     * @return
     */
    public int getTier(int itemid) {
        for (int i = 0; i < tiers.length; i++) {
            if (tiers[i] == itemid) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getMain
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMain() {
        return mainItemId;
    }
}
