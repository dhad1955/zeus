package net.tazogaming.hydra.entity3d.item;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.AttackStyle;
import net.tazogaming.hydra.util.Config;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 15:57
 * To change this template use File | Settings | File Templates.
 */
public class Weapon {
    public static final int
        TYPE_MELEE                        = 0,
        TYPE_CROSSBOW                     = 1,
        TYPE_BOW                          = 2,
        TYPE_STAFF                        = 3,
        TYPE_THROWN                       = 4,
        TYPE_SPECIALBOW                   = 7,
        TYPE_SPECIAL_XBOW                 = 8,
        TYPE_CANNON                       = 9;
    private int           tickSpeed       = 4;
    private int           standAnimation  = 1426;
    private int           runAnimation    = 824;
    private int           walkAnimation   = 819;
    private int           blockAnim       = 424;
    private int           modelId         = 0;
    private boolean       twoHanded       = false;
    private int           weaponInterface = 5855;
    private int           specBarId       = -1;
    private int           type            = TYPE_MELEE;
    private int           poisonTicks     = -1;
    private int attackSound               = -1;
    private boolean       isPoisonous     = false;
    private AttackStyle[] attackStyles    = new AttackStyle[4];

    /**
     * Constructs ...
     *
     */
    public Weapon() {
        attackStyles[0] = new AttackStyle(AttackStyle.TYPE_CRUSH, AttackStyle.MODE_ACCURATE, 422);
        attackStyles[1] = new AttackStyle(AttackStyle.TYPE_CRUSH, AttackStyle.MODE_AGGRESSIVE, 423);
        attackStyles[2] = new AttackStyle(AttackStyle.TYPE_CRUSH, AttackStyle.MODE_DEFENSIVE, 425);
    }

    /**
     * Method length
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        for (int i = 0; i < 5; i++) {
            if ((i == attackStyles.length) || (attackStyles[i] == null)) {
                return i;
            }
        }

        return 3;
    }

    /**
     * Method isPoisonous
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isPoisonous() {
        return isPoisonous;
    }

    /**
     * Method getPoisonTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPoisonTicks() {
        return poisonTicks;
    }

    /**
     * Method setPoisonus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param poisonus
     */
    public void setPoisonus(boolean poisonus) {
        isPoisonous = poisonus;

        if (poisonus) {
            Item i = Item.forId(this.modelId);

            if (i.getName().contains("(+)")) {
                poisonTicks = Config.SUPER_POISON_TIME;
            } else if (i.getName().contains("++")) {
                poisonTicks = Config.MEGA_POISON_TIME;
            } else {
                poisonTicks = Config.POISON_TIME;
            }
        }
    }

    /**
     * Method getBlockAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBlockAnimation() {
        return blockAnim;
    }

    /**
     * Method setSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setSpecial(int id) {
        specBarId = id;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void setType(int t) {
        this.type = t;
    }

    /**
     * Method getRenderAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRenderAnimation() {
        return this.standAnimation;
    }

    /**
     * Method getSpecBarId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSpecBarId() {
        return specBarId;
    }

    /**
     * Method getAttackStyle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting
     *
     * @return
     */
    public AttackStyle getAttackStyle(int setting) {
        if (setting < 0) {
            setting = 0;
        }

        return attackStyles[setting];
    }


    public int getLength() {
        for(int i= 0; i < attackStyles.length; i++)
            if(attackStyles[i] == null)
                return i;
        return 0;
    }

    /**
     * Method setWeaponInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setWeaponInterface(int id) {
        this.weaponInterface = id;
    }

    /**
     * Method setAttackStyle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     * @param t
     */
    public void setAttackStyle(int index, AttackStyle t) {
        attackStyles[index] = t;
    }

    /**
     * Method getWeaponInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWeaponInterface() {
        return weaponInterface;
    }

    /**
     * Method setTwoHanded
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setTwoHanded(boolean b) {
        this.twoHanded = b;
    }

    /**
     * Method setModelIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ind
     */
    public void setModelIndex(int ind) {
        modelId = ind;
    }

    /**
     * Method setBlockAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ind
     */
    public void setBlockAnim(int ind) {
        blockAnim = ind;
    }

    /**
     * Method getStandAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStandAnim() {
        return standAnimation;
    }

    /**
     * Method getWalkAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWalkAnimation() {
        return walkAnimation;
    }

    /**
     * Method getRunAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRunAnimation() {
        return runAnimation;
    }

    /**
     * Method getSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSpeed() {
        return tickSpeed;
    }

    /**
     * Method setRenderAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setRenderAnimation(int i) {
        standAnimation = i;
    }

    /**
     * Method setRunAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setRunAnimation(int i) {
        runAnimation = i;
    }

    /**
     * Method getModel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModel() {
        return modelId;
    }

    /**
     * Method isTwoHanded
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTwoHanded() {
        return twoHanded;
    }

    /**
     * Method setAttackStyles
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param g
     */
    public void setAttackStyles(AttackStyle... g) {
        attackStyles = g;
    }

    /**
     * Method setInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param inter
     */
    public void setInterface(int inter) {
        weaponInterface = inter;
    }

    /**
     * Method setTickSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setTickSpeed(int i) {
        tickSpeed = i;
    }

    /**
     * Method setWalkAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setWalkAnimation(int i) {
        walkAnimation = i;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }
}
