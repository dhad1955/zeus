package net.tazogaming.hydra.entity3d.item;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.AttackStyle;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.io.js5.format.CacheItemDefinition;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 23:46
 * To change this template use File | Settings | File Templates.
 */
public class WeaponLoader {

    /**
     * Method unpackConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param fileLocation
     */
    public static void unpackConfig(String fileLocation, Player player) {
        File f          = new File(fileLocation);
        int  lineNumber = 0;

        if (!f.exists()) {
            throw new RuntimeException("File not found: " + fileLocation);
        }

        int loaded = 0;

        try {
            BufferedReader br            = new BufferedReader(new FileReader(f));
            String         line          = null;
            Weapon         currentWeapon = null;

            while ((line = br.readLine()) != null) {
                lineNumber++;

                String[] split = line.split(" ");

                if (split[0].equalsIgnoreCase("weapon")) {
                    if (currentWeapon != null) {
                        throw new RuntimeException("Parse error in weapon file line " + line);
                    }

                    currentWeapon = new Weapon();
                    currentWeapon.setModelIndex(Integer.parseInt(split[1]));
                }

                if (split[0].startsWith("//")) {
                    continue;
                }

                if (currentWeapon != null) {
                    if (split[0].startsWith("renderAnimation")) {
                        currentWeapon.setRenderAnimation(Integer.parseInt(split[2]));
                    } else if (split[0].startsWith("blockAnimation")) {
                        currentWeapon.setBlockAnim(Integer.parseInt(split[2]));
                    } else if (split[0].equalsIgnoreCase("walkAnimation")) {
                        currentWeapon.setWalkAnimation(Integer.parseInt(split[2]));
                    } else if (split[0].startsWith("runAnimation")) {
                        currentWeapon.setRunAnimation(Integer.parseInt(split[2]));
                    } else if (split[0].startsWith("poisonous")) {
                        currentWeapon.setPoisonus(Boolean.parseBoolean(split[2]));
                    } else if (split[0].startsWith("tickspeed")) {
                        currentWeapon.setTickSpeed(Integer.parseInt(split[2]));
                    } else if (split[0].equalsIgnoreCase("specid")) {
                        currentWeapon.setSpecial(Integer.parseInt(split[2]));
                    } else if (split[0].equalsIgnoreCase("weapontype") || split[0].equalsIgnoreCase("type")) {
                        int type = -1;

                        if (split[2].equalsIgnoreCase("crossbow")) {
                            type = Weapon.TYPE_CROSSBOW;
                        } else if (split[2].equalsIgnoreCase("bow")) {
                            type = Weapon.TYPE_BOW;
                        } else if (split[2].equalsIgnoreCase("melee")) {
                            type = Weapon.TYPE_MELEE;
                        } else if (split[2].equalsIgnoreCase("thrown")) {
                            type = Weapon.TYPE_THROWN;
                        } else if (split[2].equalsIgnoreCase("specialbow")) {
                            type = Weapon.TYPE_SPECIALBOW;
                        } else if (split[2].equalsIgnoreCase("special_xbow")) {
                            type = Weapon.TYPE_SPECIAL_XBOW;
                        } else if (split[2].equalsIgnoreCase("cannon")) {
                            type = Weapon.TYPE_CANNON;
                        }

                        currentWeapon.setType(type);
                    } else if (split[0].equalsIgnoreCase("style")) {
                        int    id    = Integer.parseInt(split[1]);
                        int    anim  = Integer.parseInt(split[5]);
                        String type  = split[3];
                        String mode  = split[4];
                        int    type2 = -1;
                        int    mode2 = -1;

                        if (type.equalsIgnoreCase("crush")) {
                            type2 = AttackStyle.TYPE_CRUSH;
                        } else if (type.equalsIgnoreCase("slash")) {
                            type2 = AttackStyle.TYPE_SLASH;
                        } else if (type.equalsIgnoreCase("stab")) {
                            type2 = AttackStyle.TYPE_STAB;
                        } else if (type.equalsIgnoreCase("range")) {
                            type2 = AttackStyle.TYPE_RANGE;
                        } else if (type.equalsIgnoreCase("mage")) {
                            type2 = AttackStyle.TYPE_MAGIC;
                        }

                        if (mode.equalsIgnoreCase("aggressive")) {
                            mode2 = AttackStyle.MODE_AGGRESSIVE;
                        } else if (mode.equalsIgnoreCase("defensive")) {
                            mode2 = AttackStyle.MODE_DEFENSIVE;
                        } else if (mode.equalsIgnoreCase("controlled")) {
                            mode2 = AttackStyle.MODE_CONTROLLED;
                        } else if (mode.equalsIgnoreCase("LONGRANGE")) {
                            mode2 = AttackStyle.MODE_LONGRANGE;
                        } else if (mode.equalsIgnoreCase("accurate")) {
                            mode2 = AttackStyle.MODE_ACCURATE;
                        } else if (mode.equalsIgnoreCase("rapid")) {
                            mode2 = AttackStyle.MODE_RAPID;
                        }

                        AttackStyle t = new AttackStyle(type2, mode2, anim);

                        currentWeapon.setAttackStyle(id - 1, t);
                    } else if (split[0].equalsIgnoreCase("twohanded")) {
                        currentWeapon.setTwoHanded(Boolean.parseBoolean(split[2]));
                    } else if (split[0].equalsIgnoreCase("}")) {
                        Item.forId(currentWeapon.getModel()).setWeaponHandler(currentWeapon);

                        if (currentWeapon.getRenderAnimation() == 1426) {
                            currentWeapon.setRenderAnimation(
                                CacheItemDefinition.getItemDefinition(currentWeapon.getModel()).getRenderAnimId());

                            if (currentWeapon.getRenderAnimation() <= 0) {
                                currentWeapon.setRenderAnimation(1426);
                            }
                        }

                        currentWeapon.setWeaponInterface(Equipment.getInterfaceIdForWeapon(currentWeapon.getModel(),
                                Item.forId(currentWeapon.getModel()).getName()));
                        currentWeapon = null;
                        loaded++;
                    }
                }
            }

            br.close();
        } catch (Exception ee) {
           if(player != null)
            player.getActionSender().sendMessage(Text.RED("[FATAL ERROR] Error loading weapons error near line "+lineNumber));
            ee.printStackTrace();
            Logger.log("Error: " + lineNumber);
        }

        Logger.log("[ItemDefinitions]Loaded: " + loaded + " weapon configs.");
    }
}
