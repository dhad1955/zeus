package net.tazogaming.hydra.entity3d.item;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/01/14
 * Time: 19:25
 */
public class RentedItem {
    private int ticks;
    private int itemID;

    /**
     * Constructs ...
     *
     *
     * @param item
     * @param time
     */
    public RentedItem(int item, int time) {
        this.ticks  = time;
        this.itemID = item;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {
        this.ticks--;
    }

    /**
     * Method remaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int remaining() {
        return ticks;
    }

    /**
     * Method isTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminated() {
        return ticks <= 0;
    }

    /**
     * Method getItemID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getItemID() {
        return itemID;
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean update() {
        next();

        if (isTerminated()) {
            return true;
        }

        return false;
    }
}
