package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Region;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class Viewport {

    /** entities made: 15/02/14 **/
    private ArrayList[] entities = new ArrayList[6];

    /** mob made: 15/02/14 **/
    private Mob mob;

    /**
     * Constructs ...
     *
     *
     * @param m
     */
    public Viewport(Mob m) {
        mob = m;

        for (int i = 0; i < entities.length; i++) {
            entities[i] = new ArrayList<Entity>();
        }
    }

    /**
     * Method getPlayersInView
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param range
     * @param players
     * @param npc
     */
    public void getPlayersInView(int range, ArrayList<Player> players, NPC npc) {
        Region[] regions = World.getWorld().getRegionManager().getSurroundingRegions(mob.getLocation());
        short    i       = 0;
        Player   player1 = null;

        for (Region r : regions) {
            if (r == null) {
                continue;
            }

            if (r.getPlayers().size() > 200) {
                System.err.println("too many players in that region?? " + r.getPlayers().size() + " " + npc.getId()
                                   + " " + npc.getLocation());
            }

            for (i = 0; i < r.getPlayers().size(); i++) {
                player1 = r.getPlayers().get(i);

                if ((Point.getDistance(npc.getLocation(), player1.getLocation()) <= range)
                        && (player1.getCurrentInstance() == npc.getCurrentInstance()) &&!players.contains(player1)) {
                    players.add(player1);
                }
            }
        }

        if (players.size() > 200) {
            System.err.println("huge amount of players: " + players.size() + " " + npc.getId() + " "
                               + npc.getLocation());
        }

        // But iterator is destroyed here
    }

    /**
     * Method getPlayersInView
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Player> getPlayersInView() {
        if (entities[0] == null) {
            entities[0] = new ArrayList();
        }

        entities[0].clear();

        Region[] regions = World.getWorld().getRegionManager().getSurroundingRegions(mob.getLocation());

        for (Region r : regions) {
            if (r != null) {
                entities[0].addAll(r.getPlayers());
            }
        }

        return entities[0];
    }

    /**
     * Method getTargetsInRange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param range
     *
     * @return
     */
    public ArrayList<Killable> getTargetsInRange(int range) {
        Region[]            regions = World.getWorld().getRegionManager().getSurroundingRegions(mob.getLocation());
        ArrayList<Killable> kl      = new ArrayList<Killable>();

        for (Region r : regions) {
            if (r != null) {
                for (Player pla : r.getPlayers()) {
                    if ((pla != mob) && (Point.getDistance(pla.getX(), pla.getY(), mob.getX(), mob.getY()) <= range)) {
                        kl.add(pla);
                    }
                }

                for (NPC pla : r.getNpcs()) {
                    if ((pla != mob) && (Point.getDistance(pla.getX(), pla.getY(), mob.getX(), mob.getY()) <= range)) {
                        kl.add(pla);
                    }
                }
            }
        }

        return kl;
    }

    /**
     * Method getEntitiesInView
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList[] getEntitiesInView() {
        for (int i = 0; i < entities.length; i++) {
            entities[i].clear();
        }

        Region[] regions = World.getWorld().getRegionManager().getSurroundingRegions(mob.getLocation());

        for (Region r : regions) {
            if (r != null) {
                entities[0].addAll(r.getPlayers());
                entities[1].addAll(r.getItems());
                entities[3].addAll(r.getNpcs());
                entities[4].addAll(r.getObjects());

                if (r.hasDoors()) {
                    entities[5].addAll(r.getDoor());
                }
            }
        }

        return entities;
    }
}
