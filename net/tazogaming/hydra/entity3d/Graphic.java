package net.tazogaming.hydra.entity3d;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 21:24
 * To change this template use File | Settings | File Templates.
 */
public class Graphic {
    public static final int
        HIGH                    = 0,
        LOW                     = 1;
    private int     graphicId   = -1;
    private int     graphicWait = 0;
    private int     graphicType = -1;
    private boolean hasChanged  = false;

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param type
     */
    public void set(int id, int type) {
        graphicId = id;

        if (type == HIGH) {
            graphicWait = 6553600;
        } else {
            graphicWait = 0;
        }

        hasChanged = true;
    }

    /**
     * Method getGraphicWait
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGraphicWait() {
        return graphicWait;
    }

    /**
     * Method setWait
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param wait
     */
    public void setWait(int wait) {
        graphicWait = wait;
    }

    /**
     * Method getIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getIndex() {
        return graphicId;
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        hasChanged = false;
    }

    /**
     * Method hasChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasChanged() {
        return hasChanged;
    }
}
