package net.tazogaming.hydra.entity3d;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.map.*;
import net.tazogaming.hydra.runtime.Core;

/*
* base class, used for iterating etc
* TODO: Needs redone, thanks exemplar. Some things have an index but not an id, some things have an id and not an index, and some have both, some could potentially have neither.
*/

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class Entity {
    public Point         location  = null;
    private boolean      isVisible = true;
    public int           id;
    public transient int index;
    private Instance     currentInstance;
    private Point        lastLocation;

    /**
     * Method setCurrentInstance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instance
     */
    public void setCurrentInstance(Instance instance) {
        this.currentInstance = instance;
    }

    /**
     * Method isInZone
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     *
     * @return
     */
    public boolean isInArea(Zone e) {
        Region r = World.getWorld().getRegionManager().getRegion(location);

        if (!r.hasAreas()) {
            return false;
        }

        for (Zone e2 : r.getAreas()) {


            if (e2 == e && e.isInArea(getLocation())) {
                return true;
            }
        }

        return false;
    }


    /**
     * Method isInMultiArea
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isInMultiArea() {
        Region r = World.getWorld().getRegionManager().getRegion(location);
        if(r == null)
            return false;


        if (!r.hasAreas()) {
            return false;
        }

        for (Zone e : r.getAreas()) {
            if (e.isInArea(this.location) && e.isMulti()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isInZone
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */

    public boolean isInWilderness() {
        Region r  = World.getWorld().getRegionManager().getRegion(location);

        if(r == null)
            return false;

        if(!r.hasAreas())
            return false;
        for(Zone zone : r.getAreas()) {
            if(((zone.isWilderness() || zone.getName().equalsIgnoreCase("wild")) && zone.isInArea(getLocation())))
                return true;
        }
        return false;
    }
    public boolean isInZone(String name) {
        if (name.equalsIgnoreCase("wild")
                && ((this instanceof Player) && (Combat.getWildernessLevel((Player) this) == -1))) {
            return false;
        }

        Region r = World.getWorld().getRegionManager().getRegion(location);

        if (!r.hasAreas()) {
            return false;
        }

        for (Zone e : r.getAreas()) {
            if (e.isInArea(this.location) && e.getName().equalsIgnoreCase(name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getCurrentInstance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Instance getCurrentInstance() {
        return currentInstance;
    }

    /**
     * Method getHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHeight() {
        return location.getHeight();
    }

    /**
     * Method isVisible
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isVisible() {
        return isVisible;
    }

    /**
     * Method enterInstance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instance
     */
    public void enterInstance(Instance instance) {
        currentInstance = instance;
    }

    /**
     * Method getId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Method setId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newid
     */
    public void setId(int newid) {
        id = newid;
    }

    /**
     * Method getIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getIndex() {
        return index;
    }

    /**
     * Method setIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newIndex
     */
    public void setIndex(int newIndex) {
        index = newIndex;
    }

    /**
     * Method setVisible
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param vis
     */
    public void setVisible(boolean vis) {
        isVisible = vis;
    }

    /**
     * Method setLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void setLocation(Point p) {

        if(getLocation() != null && p != null)
        if(p != null && getLocation() != null && Point.equals(getLocation(), p))
            return;

        World.getWorld().setLocation(this, location, p);


        if (p != null) {
            lastLocation = location;
            location     = p;
        }
    }


    public Point getLastLocation() {
        return lastLocation;
    }

    /**
     * Method getLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getLocation() {
        return location;
    }

    /**
     * Method getX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getX() {
        return location.getX();
    }

    /**
     * Method getY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getY() {
        return location.getY();
    }
}
