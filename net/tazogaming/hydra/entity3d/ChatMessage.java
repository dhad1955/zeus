
/*
* ChatMessage.java
*
* Created on 17-Dec-2007, 19:42:20
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
*/
package net.tazogaming.hydra.entity3d;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class ChatMessage {
    private static char decodeBuf[]  = new char[4096];
    public static char  xlateTable[] = {
        ' ', 'e', 't', 'a', 'o', 'i', 'h', 'n', 's', 'r', 'd', 'l', 'u', 'm', 'w', 'c', 'y', 'f', 'g', 'p', 'b', 'v',
        'k', 'x', 'j', 'q', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ' ', '!', '?', '.', ',', ':', ';',
        '(', ')', '-', '&', '*', '\\', '\'', '@', '#', '+', '=', '\243', '$', '%', '"', '[', ']'
    };
    private int    colour, effects;
    private String message;
    private Player sender;
    private int    leng;

    /**
     * Constructs ...
     *
     *
     * @param p
     * @param message
     * @param eff
     */
    public ChatMessage(Player p, String message, int eff) {
        this.sender  = p;
        this.message = message;
        this.effects = eff;
        this.leng    = leng;
        p.log("said: " + toString());
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        return message;
    }

    /**
     * Method textUnpack
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param packedData
     * @param size
     *
     * @return
     */
    public static String textUnpack(byte packedData[], int size) {
        int idx        = 0,
            highNibble = -1;

        for (int i = 0; i < size * 2; i++) {
            int val = packedData[i / 2] >> (4 - 4 * (i % 2)) & 0xf;

            if (highNibble == -1) {
                if (val < 13) {
                    decodeBuf[idx++] = xlateTable[val];
                } else {
                    highNibble = val;
                }
            } else {
                decodeBuf[idx++] = xlateTable[((highNibble << 4) + val) - 195];
                highNibble       = -1;
            }
        }

        return new String(decodeBuf, 0, idx);
    }

    /**
     * Method getColour
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getColour() {
        return colour;
    }

    /**
     * Method getEffects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getEffects() {
        return effects;
    }
}
