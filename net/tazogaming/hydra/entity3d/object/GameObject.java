package net.tazogaming.hydra.entity3d.object;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 25/09/13
 * Time: 18:21
 * To change this template use File | Settings | File Templates.
 */
public class GameObject extends Entity {
    public static final int
        TYPE_GROUND_OBJECT = 11,
        TYPE_SOLID_OBJECT  = 10,
        TYPE_FLAT_OBJECT   = 0,
        TYPE_GROUND_DECO   = 22;

    /*
     * Current rotation
     */
    private byte currentRotation = 0;

    /*
     * Object type, 11 = ground object/fire etc,
     * 22 = floor deco such as floor spikes
     * 10 = World object
     * 0 = door/wall etc
     */
    private byte objectType = 0;

    /*
     * Unique update identifier,
     */
    private int currentId = 0;

    /*
     * Previous object definitions (for changes)
     */
    private byte     lastRotation     = -1;
    private byte     lastObjectId     = -1;
    private byte     lastType         = -1;
    private byte     defaultRotation  = 40;
    private int     defaultId        = -1;
    private boolean scheduledRemoved = false;
    private byte    harvestAmount    = (byte) GameMath.rand(40);

    /*
     * Flag to signal object is removed
     */
    private boolean isRemoved = false;

    /*
     * Flag to change object visibility
     */
    private boolean isVisable       = true;
    private int     revertAt        = -1;
    private boolean isMapObject     = false;
    private int     lastInteraction = -1;
    private boolean isDoor          = false;
    private boolean isOpen          = false;

    public int getAnimationId() {
        return animationId;
    }

    public void setAnimationId(int animationId) {
        this.animationId = (byte)animationId;
    }

    private short animationId = -1;

    /*
     * identifier
     */
    private int objectId;

    /**
     * Method isScheduledRemoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isScheduledRemoved() {
        return scheduledRemoved;
    }

    /**
     * Method getHarvestAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte getHarvestAmount() {
        return harvestAmount;
    }

    /**
     * Method setHarvestAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param harvestAmount
     */
    public void setHarvestAmount(byte harvestAmount) {
        this.harvestAmount = harvestAmount;
    }

    /**
     * Method isDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDoor() {
        return isDoor;
    }

    /**
     * Method setDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param door
     */
    public void setDoor(boolean door) {
        isDoor = door;
    }

    /**
     * Method isOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isOpen() {
        return isOpen;
    }

    /**
     * Method setOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param open
     */
    public void setOpen(boolean open) {
        isOpen = open;
    }

    /**
     * Method scheduleRevert
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     */
    public void scheduleRevert(int ticks) {
        revertAt = Core.currentTime + ticks;
    }

    /**
     * Method getRevertTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRevertTime() {
        return revertAt;
    }

    /**
     * Method scheduleRemove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     */
    public void scheduleRemove(int ticks) {
        revertAt         = Core.currentTime + ticks;
        scheduledRemoved = true;
    }

    /**
     * Method needsRemove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean needsRemove() {
        return Core.currentTime - lastInteraction > 200;
    }

    /**
     * Method setLastInteraction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setLastInteraction(int i) {
        this.lastInteraction = i;
    }

    /**
     * Method setDefault
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setDefault(boolean b) {
        this.isMapObject = true;
    }

    /**
     * Method needsChange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean needsChange() {
        return (revertAt != -1) && (Core.timeUntil(revertAt) <= 0);
    }

    /**
     * Method changeTo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectId
     * @param objectRotation
     * @param objectType
     */
    public void changeTo(int objectId, int objectRotation, int objectType) {
        if (objectId != this.objectId) {
            this.lastObjectId = (byte)this.objectId;
            this.objectId     = objectId;
        }

        if (this.objectType != objectType) {
            this.lastType   = this.objectType;
            this.objectType = (byte)objectType;
        }

        if (this.currentRotation != objectRotation) {
            this.lastRotation    = this.currentRotation;
            this.currentRotation = (byte)objectRotation;
        }
        lastInteraction = Core.currentTime;

        next();
    }

    /**
     * Method setIsMapObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setIsMapObject(boolean b) {
        isMapObject = b;
    }

    /**
     * Method isMapObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isMapObject() {
        return isMapObject;
    }

    /**
     * Method changeIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectId
     */
    public void changeIndex(int objectId) {
        changeTo(objectId, this.currentRotation, this.objectType);
    }

    /**
     * Method changeRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectRot
     */
    public void changeRotation(int objectRot) {
        changeTo(objectId, objectRot, objectType);
    }

    /**
     * Method changeType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectT
     */
    public void changeType(int objectT) {
        changeTo(objectId, currentRotation, objectT);
    }

    /**
     * Method revertId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void revertId() {
        changeTo(this.defaultId, this.currentRotation, this.objectType);
    }

    /**
     * Method revertRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void revertRotation() {
        changeTo(this.objectId, this.defaultRotation, this.objectType);
    }

    /**
     * Method revertType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void revertType() {
        changeTo(this.objectId, this.currentRotation, this.objectType);
    }

    /**
     * Method revertAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void revertAll() {
        this.objectId        = defaultId;
        this.currentRotation = defaultRotation;
        next();
    }

    /**
     * Method offset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int offset() {
        return this.currentId;
    }

    /**
     * Method isDefault
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDefault() {
        return (defaultRotation == currentRotation) && (defaultId == objectId);
    }

    /**
     * Method isRemoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isRemoved() {
        return isRemoved;
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void remove() {
        if (!this.isRemoved) {
            this.isRemoved = true;
            next();
        }
    }

    /**
     * Method hide
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void hide() {
        if (this.isVisable) {
            this.isVisable = false;
            next();
        }
    }

    /**
     * Method show
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void show() {
        if (!this.isVisable) {
            this.isVisable = true;
            next();
        }
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {
        currentId++;
    }

    /**
     * Method setObjectType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setObjectType(int type) {
        this.objectType = (byte)type;
    }

    /**
     * Method setIdentifier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setIdentifier(int id) {




        this.objectId = id;

        if (defaultId == -1) {
            defaultId = id;
        }
    }

    /**
     * Method setCurrentRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rot
     */
    public void setCurrentRotation(int rot) {
        this.currentRotation = (byte)rot;

        if (defaultRotation == 40) {
            defaultRotation = (byte)rot;
        }
    }

    /**
     * Method getDefaultId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDefaultId() {
        return defaultId;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return objectType;
    }

    /**
     * Method getId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return objectId;
    }

    /**
     * Method getRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRotation() {
        return currentRotation;
    }
}
