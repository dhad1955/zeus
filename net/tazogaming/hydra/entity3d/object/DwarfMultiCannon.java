package net.tazogaming.hydra.entity3d.object;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.AttackStyle;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.net.packetbuilder.GPI;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/10/14
 * Time: 16:18
 */
public class DwarfMultiCannon extends GameObject implements RecurringTickEvent {
    public static final int
        ITEM_BASE    = 6,
        ITEM_STAND   = 8,
        ITEM_BARRELS = 10,
        ITEM_FURNACE = 12;

    /** MAX_DAMAGE made: 14/10/12 */
    private static final int MAX_DAMAGE = 30;

    /** FIRE_RANGE made: 14/10/12 */
    private static final int FIRE_RANGE = 9;

    /** MAX_AMMO made: 14/10/12 */
    private static final int MAX_AMMO = 30;

    // 2 ticks, 1.2 seconds per turn.

    /** FIRE_RATE made: 14/10/12 */
    private static final int FIRE_RATE = 2;

    /** CANNON_SETUP, CANNON_BASE, CANNON_STAND, CANNON_BARRELS, REMOVED made: 14/10/12 */
    private static final int
        CANNON_SETUP                  = 6,
        CANNON_BASE                   = 7,
        CANNON_STAND                  = 8,
        CANNON_BARRELS                = 9,
        REMOVED                       = 0;
    public static final int IDLE_TIME = Core.getTicksForMinutes(10);

    /** CLOCK_WISE made: 14/10/13 */
    private static final int[] CLOCK_WISE = {
        Movement.NORTH, Movement.NORTH_EAST, Movement.EAST, Movement.SOUTH_EAST, Movement.SOUTH, Movement.SOUTH_WEST,
        Movement.WEST, Movement.NORTH_WEST
    };

    /** lastInteraction made: 14/10/14 */
    private int lastInteraction = Core.currentTime;

    /** stage made: 14/10/12 */
    private byte stage = 0;

    /** isFiring made: 14/10/12 */
    private boolean isFiring = false;

    /** ammo made: 14/10/12 */
    private int ammo = 0;

    /** direction made: 14/10/12 */
    private int direction = 0;

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private int lastMove = Core.currentTime;

    /** owner made: 14/10/12 */
    private Player owner;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    private DwarfMultiCannon(Player player) {
        setCurrentInstance(player.getCurrentInstance());
        setIdentifier(CANNON_BASE);
        setId(CANNON_BASE);
        setObjectType(10);
        setCurrentRotation(0);
        setLocation(player.getLocation());
        this.owner = player;
        player.getFacingLocation().focus(player.getX() + 1, player.getY() + 1);
        player.getActionSender().sendMessage("You set up the cannon");
        stage = CANNON_BASE;
        Core.submitTask(this);
        World.getWorld().getObjectManager().registerObject(this);
    }

    /**
     * Method setupCannon
     * Created on 14/10/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static final DwarfMultiCannon setupCannon(Player player) {
        if (player.getAccount().hasVar("cannon")) {
            player.getActionSender().sendMessage("You already have a cannon set up.");

            return null;
        }

        // detect any other cannons
        Tile tile = null;

        for (int ourCannonDeltaX = 2; ourCannonDeltaX > 0; ourCannonDeltaX--) {
            for (int ourCannonDeltaY = 2; ourCannonDeltaY > 0; ourCannonDeltaY--) {
                for (int possibleObjectDeltaX = 2; possibleObjectDeltaX > 0; possibleObjectDeltaX--) {
                    for (int possibleObjectDeltaY = 2; possibleObjectDeltaY > 0; possibleObjectDeltaY--) {
                        tile = World.getWorld().getTile(player.getX() + ourCannonDeltaX + possibleObjectDeltaX,
                                                        player.getY() + ourCannonDeltaY + possibleObjectDeltaY,
                                                        player.getHeight());

                        if (tile.hasObjects() || tile.hasMappedObject()) {
                            player.getActionSender().sendMessage("You can't set up a cannon here.");

                            return null;
                        }
                    }
                }
            }
        }

        if (player.getCurrentInstance() != null) {
            player.getActionSender().sendMessage("You can't set up a cannon here.");

            return null;
        }

        for (Zone zone : player.getAreas()) {
            if (zone.isCannonNotAllowed()) {
                player.getActionSender().sendMessage("You can't set up a cannon here.");

                return null;
            }
        }

        for (int x = 0; x < 2; x++) {
            for (int y = 0; y < 2; y++) {
                if (ClippingDecoder.getClipping(player.getX() + x, player.getY() + y, player.getHeight()) != 0) {
                    player.getActionSender().sendMessage("You can't set up a cannon here.");

                    return null;
                }
            }
        }

        player.getInventory().deleteItem(ITEM_BASE, 1);

        return new DwarfMultiCannon(player);
    }

    /**
     * Method itemAtObject
     * Created on 14/10/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemID
     * @param objectID
     *
     * @return
     */
    public boolean itemAtObject(int itemID, int objectID) {
        if (objectID != stage) {
            return false;
        }

        lastInteraction = Core.currentTime;

        if ((itemID == ITEM_STAND) && (stage == CANNON_BASE)) {
            changeStage(CANNON_STAND);
            owner.getInventory().deleteItem(ITEM_STAND, 1);

            return true;
        } else if ((itemID == ITEM_BARRELS) && (stage == CANNON_STAND)) {
            changeStage(CANNON_BARRELS);
            owner.getInventory().deleteItem(ITEM_BARRELS, 1);

            return true;
        } else if ((itemID == ITEM_FURNACE) && (stage == CANNON_BARRELS)) {
            changeStage(CANNON_SETUP);
            owner.getInventory().deleteItem(ITEM_FURNACE, 1);

            return true;
        } else if ((itemID == 2) && (stage == CANNON_SETUP)) {
            int attempt = owner.getInventory().getItemMax(owner.getInventory().getItemSlot(Item.forId(2)), MAX_AMMO);

            if (attempt > MAX_AMMO) {
                attempt = MAX_AMMO;
            }

            int maxBalls = MAX_AMMO - ammo;

            if (maxBalls == 0) {
                return true;
            }

            if (attempt > maxBalls) {
                attempt = maxBalls;
            }

            if (attempt == 0) {
                return true;
            }

            owner.getActionSender().sendMessage("You load the cannon with " + attempt + " balls");
            ammo += attempt;
            owner.getInventory().deleteItem(2, attempt);

            return true;
        }

        return false;
    }

    private void changeStage(int stage) {
        this.stage = (byte) stage;
        this.setId(stage);
        this.setIdentifier(stage);
        this.next();
    }



    private void populatePossibleTargets(Point location, int direction, int range, List<NPC> tmp) {
        int  steps = 0;
        Tile tile  = null;
        if(range >= 15)
            range = 15;

        while (steps < range) {
            Point nxtPoint = Movement.getPointForDir(location, direction);

            if (!Combat.clearPath(location, nxtPoint)) {
                break;
            }

            location = nxtPoint;
            steps++;
            tile = World.getWorld().getTile(location);

            if (tile.containsNPCChunks()) {
                for (NPC npc : tile.getNpcChunk()) {
                    if (!tmp.contains(npc) && npc.canBeAttacked(owner) && owner.canBeAttacked(npc)
                            && npc.getDefinition().isAttackable() && (npc.getCurrentInstance() == getCurrentInstance())
                            && npc.isVisible()) {
                        tmp.add(npc);
                    }
                }
            }
        }
    }

    /**
     * Method getAmmo
     * Created on 14/10/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAmmo() {
        return ammo;
    }

    /**
     * Method destruct
     * Created on 14/10/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bank
     */
    public void destruct(boolean bank) {
        if (isRemoved()) {
            return;
        }

        List<Item> returnItems = new ArrayList<Item>();

        if ((stage >= CANNON_BASE) || (stage == CANNON_SETUP)) {
            returnItems.add(Item.forId(ITEM_BASE));
        }

        if ((stage >= CANNON_STAND) || (stage == CANNON_SETUP)) {
            returnItems.add(Item.forId(ITEM_STAND));
        }

        if ((stage >= CANNON_BARRELS) || (stage == CANNON_SETUP)) {
            returnItems.add(Item.forId(ITEM_BARRELS));
        }

        if (stage == CANNON_SETUP) {
            returnItems.add(Item.forId(ITEM_FURNACE));
        }

        if (owner.getInventory().getFreeSlots(-1) < 4) {
            bank = true;
            owner.getActionSender().sendMessage(
                "Your cannon has been sent to your bank because you did not have enough slots.");
        }

        if (bank) {
            for (Item i : returnItems) {
                owner.getBank().insert(i.getIndex(), 1);
            }
        } else {
            for (Item i : returnItems) {
                owner.getInventory().addItem(i.getIndex(), 1);
            }
        }

        remove();
        owner.getAccount().removeVar("cannon");
    }

    /**
     * Method isReady
     * Created on 14/10/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isReady() {
        return stage == CANNON_SETUP;
    }

    /**
     * Method isFiring
     * Created on 14/10/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isFiring() {
        return isFiring;
    }

    /**
     * Method setFiring
     * Created on 14/10/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param firing
     */
    public void setFiring(boolean firing) {
        this.isFiring = firing;
    }

    private static final int[][] getDelta(int dir) {
        switch (dir) {
        case Movement.NORTH :
        case Movement.SOUTH :
            return new int[][] {
                { -1, 0 }, { 1, 0 }
            };

        case Movement.WEST :
        case Movement.EAST :
            return new int[][] {
                { 0, -1 }, { 0, 1 }
            };

        case Movement.NORTH_WEST :
        case Movement.SOUTH_EAST :
        case Movement.NORTH_EAST :
        case Movement.SOUTH_WEST :
            return new int[][] {
                { 0, 1 }, { 0, -1 }
            };
        }

        return new int[][] {
            { 0, 0 }, { 0, 0 }
        };
    }

    private List<NPC> getTargets(int dir, int range) {
        List<NPC> targets           = new ArrayList<NPC>(20);
        int[][]   translationPoints = getDelta(dir);
        int       x                 = getX() + 1;
        int       y                 = getY() + 1;

        populatePossibleTargets(Point.location(x, y, getHeight()), dir, range, targets);

        for (int i = 0; i < 2; i++) {
            for (int k = 0; k < 2; k++) {
                populatePossibleTargets(Point.location(x + translationPoints[0][0], y + translationPoints[0][1],
                        getHeight()), dir, range, targets);
                populatePossibleTargets(Point.location(x + translationPoints[1][0], y + translationPoints[1][1],
                        getHeight()), dir, range, targets);
            }
        }

        return targets;
    }

    /**
     * Method tick
     * Created on 14/10/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        if (Core.timeSince(lastInteraction) > IDLE_TIME) {
            destruct(true);
            return;
        }

        if (!isReady()) {
            return;
        }

        if (!isFiring && (getClockwiseDirection(direction) != Movement.NORTH)) {
            return;
        }

        if (Core.timeSince(lastMove) > FIRE_RATE) {
            lastMove  = Core.currentTime;
            direction = (1 + direction) % 8;
            int lastDir   = direction;
            int animation = 514 + direction;
            int fireDir   = getClockwiseDirection(lastDir);
            owner.getActionSender().sendAnimateObject(this, animation);
            for (Player player : World.getWorld().getPlayers()) {
                if ((Point.getDistance(player.getLocation(), getLocation()) < 15) && (player != owner)) {
                    player.getActionSender().sendAnimateObject(this, animation);
                }
            }
            if ((fireDir == Movement.NORTH) &&!isFiring) {
                return;
            }
            if (isFiring) {
                List<NPC> npcsInRange = this.getTargets(fireDir, FIRE_RANGE);

                if (npcsInRange != null) {
                    for (NPC npc : npcsInRange) {
                        Projectile cannonBall = new Projectile(53, this.owner, npc, getX() + 1, getY() + 1,
                                                    npc.getCenterPoint().getX(), npc.getCenterPoint().getY(), 37, 8,
                                                    37, 37);

                        cannonBall.setOnImpactEvent(new ImpactEvent() {
                            @Override
                            public void onImpact(Killable sender, Killable target) {
                                double dmg = target.hit(owner, GameMath.rand(30), Damage.TYPE_RANGE, 0);

                                owner.setSpecialMultiplier(1.00);
                                PlayerCombatAdapter.addCombatXP((int)dmg * 3, AttackStyle.TYPE_RANGE, owner);
                            }
                        });
                        owner.registerProjectile(cannonBall);
                        GPI.forceSendProjectile(cannonBall, owner);
                        this.ammo--;

                        if (this.ammo <= 0) {
                            this.ammo     = 0;
                            this.isFiring = false;
                            owner.getActionSender().sendMessage("Your cannon has run out of balls.");
                        }
                    }
                }
            }
        }
    }

    private static final int getClockwiseDirection(int dir) {
        if (true) {
            return CLOCK_WISE[dir];
        }

        for (int i = 0; i < CLOCK_WISE.length; i++) {
            if (CLOCK_WISE[i] == dir) {
                return CLOCK_WISE[i];
            }
        }

        throw new IllegalArgumentException("Invalid direction: " + dir);
    }

    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
        return isRemoved();
    }
}
