package net.tazogaming.hydra.entity3d.projectile;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/10/13
 * Time: 02:06
 */
public class TelegrabImpactEvent extends ImpactEvent {
    private FloorItem item;
    private Player    player;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param i
     */
    public TelegrabImpactEvent(Player pla, FloorItem i) {
        this.item   = i;
        this.player = pla;
    }

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    @Override
    public void onImpact(Killable sender, Killable target) {
        if (item.isRemoved()) {
            return;
        }

        if (player.getInventory().getFreeSlots(item.getIndex()) > 0) {
            if (!ClueManager.handle_scroll_pickup(player, item.getItemId())) {
                return;
            }

            item.setPickupper(player);
            item.remove();

            if (item.getCurrentInstance() instanceof Dungeon) {
                Dungeon d = (Dungeon) item.getCurrentInstance();

                d.removeFloorItem(item);
            }

            player.getInventory().addItem(item.getItemId(), item.getAmount());
        }

        for (Player pla : item.getViewArea().getPlayersInView()) {
            pla.getActionSender().sendStillGFX(144, item.getX(), item.getY());
        }
    }
}
