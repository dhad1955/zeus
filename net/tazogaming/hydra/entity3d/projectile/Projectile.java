package net.tazogaming.hydra.entity3d.projectile;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
public class Projectile {
    public static final int DEFAULT_START_HEIGHT = 36;
    public static final int DEFAULT_END_HEIGHT   = 30;
    private boolean         isFired              = false;
    private int             graphicId            = -1;
    private int             delay                = 0;
    private int             castAt               = 0;
    private int             waitSpeed            = 0;
    private int             distanceFromTarget   = -1;
    private int[]           vars                 = {
        50, 40, 35, 45, 65, 15, 90
    };
    private int             startHeight          = 0;
    private int             endHeight            = 0;
    private int             type                 = 0;
    private Killable        sender;
    private Killable        target;
    private int             moveSpeed;
    private Graphic         impactGraphic;
    private int             offsetX, offsetY;
    private int             impactAt;
    private int             startX, startY;
    private ImpactEvent     onImpactEvent;

    /**
     * Constructs ...
     *
     *
     * @param sender
     * @param target
     * @param gfxId
     * @param waitSpeed
     * @param moveSpeed
     */
    public Projectile(Killable sender, Killable target, int gfxId, int waitSpeed, int moveSpeed) {
        this(sender, target, gfxId, waitSpeed, moveSpeed, DEFAULT_START_HEIGHT, DEFAULT_END_HEIGHT);
    }

    public Projectile(Killable sender, Killable target, int time,  ImpactEvent event){
        this.target = target;
        this.sender = sender;
        this.impactAt = Core.currentTime + time;
        this.onImpactEvent = event;
        this.moveSpeed = -999;
    }

    /**
     * Constructs ...
     *
     *
     * @param sender
     * @param target
     * @param gfxId
     * @param waitSpeed
     * @param moveSpeed
     * @param startHeight
     * @param endHeight
     */
    public Projectile(Killable sender, Killable target, int gfxId, int waitSpeed, int moveSpeed, int startHeight,
                      int endHeight) {
        this(gfxId, sender, target, sender.getKnownLocation().getX(), sender.getKnownLocation().getY(),
             (target instanceof NPC)
             ? ((NPC) target).getCenterPoint().getX()
             : target.getX(), (target instanceof NPC)
                              ? ((NPC) target).getCenterPoint().getY()
                              : target.getY(), waitSpeed, moveSpeed, startHeight, endHeight);
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param sender
     * @param target
     * @param startX
     * @param startY
     * @param endX
     * @param endY
     * @param moveSpeed
     * @param waitspeed
     * @param startHeight
     * @param endHeight
     */
    public Projectile(int id, Killable sender, Killable target, int startX, int startY, int endX, int endY,
                      int moveSpeed, int waitspeed, int startHeight, int endHeight) {
        offsetX            = ((startX - endX) * -1);
        offsetY            = ((startY - endY) * -1);
        distanceFromTarget = Point.getDistance(startX, startY, endX, endY);
        castAt             = Core.currentTime;
        this.sender        = sender;
        this.startX        = startX;
        this.startY        = startY;
        this.target        = target;
        this.startHeight   = startHeight;
        this.endHeight     = endHeight;
        this.type          = type;
        this.graphicId     = id;
        this.moveSpeed     = moveSpeed;
        this.waitSpeed     = waitspeed;


        this.moveSpeed += waitspeed;
        int totalTime = moveSpeed * distanceFromTarget;
        waitspeed = 0;
        totalTime += waitspeed;


        totalTime *= 20;

        int ticks = totalTime / 600;

        ticks -=1;

        if(ticks <= 0)
            ticks = 1;

        this.impactAt = Core.currentTime + ticks;
    }

    public void increaseImpact() {
        this.impactAt += 1;
    }




    /**
     * Method getEndHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getEndHeight() {
        return endHeight;
    }

    /**
     * Method setEndHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param endHeight
     */
    public void setEndHeight(int endHeight) {
        this.endHeight = endHeight;
    }

    /**
     * Method getStartHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStartHeight() {
        return startHeight;
    }

    /**
     * Method setStartHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startHeight
     */
    public void setStartHeight(int startHeight) {
        this.startHeight = startHeight;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Method setVars
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param vars
     */
    public void setVars(int[] vars) {
        this.vars = vars;
    }

    /**
     * Method setAttribute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param g
     */
    public void setAttribute(int i, int g) {
        this.vars[i] = g;
    }

    /**
     * Method getVars
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getVars() {
        return vars;
    }

    /**
     * Method getOnImpactEvent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ImpactEvent getOnImpactEvent() {
        return onImpactEvent;
    }

    /**
     * Method setOnImpactEvent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param onImpactEvent
     */
    public void setOnImpactEvent(ImpactEvent onImpactEvent) {
        this.onImpactEvent = onImpactEvent;
    }

    /**
     * Method getWaitspeedForTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     *
     * @return
     */
    public static int getWaitspeedForTick(int ticks) {
        return ticks * 20;
    }

    /**
     * Method getTickForSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param speed
     *
     * @return
     */
    public static int getTickForSpeed(int speed) {
        return speed / 20;
    }

    /**
     * Method getOffsetX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOffsetX() {
        return offsetX;
    }

    /**
     * Method getSender
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Killable getSender() {
        return sender;
    }

    /**
     * Method setSender
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     */
    public void setSender(Killable sender) {
        this.sender = sender;
    }

    /**
     * Method getTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Killable getTarget() {
        return target;
    }

    /**
     * Method setTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param target
     */
    public void setTarget(Killable target) {
        this.target = target;
    }

    /**
     * Method getOffsetY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOffsetY() {
        return offsetY;
    }

    /**
     * Method getLockon
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLockon() {
        if ((this.target == null) || (target instanceof FloorItem)) {
            return 0;
        }

        if (this.target instanceof NPC) {
            return target.getIndex() + 1;
        } else {
            return -(target.getIndex() + 1);
        }
    }

    /**
     * Method isReady
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isReady() {
        return Core.currentTime >= impactAt;
    }

    /**
     * Method getImpactTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getImpactTick() {
        double duration = (moveSpeed + distanceFromTarget * 5) * 12d;
        int    ticks    = (int) Math.ceil((duration / 600d * 1000d) / 1000d);

        return ticks;
    }

    /**
     * Method getStartX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStartX() {
        return startX;
    }

    /**
     * Method setStartX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startX
     */
    public void setStartX(int startX) {
        this.startX = startX;
    }

    /**
     * Method getStartY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStartY() {
        return startY;
    }

    /**
     * Method setStartY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startY
     */
    public void setStartY(int startY) {
        this.startY = startY;
    }

    /**
     * Method getGraphicId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGraphicId() {
        return graphicId;
    }

    /**
     * Method setGraphicId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param graphicId
     */
    public void setGraphicId(int graphicId) {
        this.graphicId = graphicId;
    }

    /**
     * Method getMoveSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMoveSpeed() {
        return moveSpeed;
    }

    /**
     * Method setMoveSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param moveSpeed
     */
    public void setMoveSpeed(int moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    /**
     * Method getWaitSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWaitSpeed() {
        return waitSpeed;
    }

    /**
     * Method setWaitSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param waitSpeed
     */
    public void setWaitSpeed(int waitSpeed) {
        this.waitSpeed = waitSpeed;
    }
}
