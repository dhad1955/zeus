package net.tazogaming.hydra.security;

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.io.LoadResult;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/12/2014
 * Time: 14:05
 */
public interface Authenticator<T extends Object, M extends Object> {
    public LoadResult getResult(T loadingObject, M loadingParameter);

}
