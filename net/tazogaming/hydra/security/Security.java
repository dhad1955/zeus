package net.tazogaming.hydra.security;

//~--- JDK imports ------------------------------------------------------------

import java.security.MessageDigest;
import java.security.SecureRandom;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/11/2014
 * Time: 12:40
 */
public class Security {
    public static final String[] SALT_TABLE = {
        "A", "{", "$", "%", "£", "@", ";", "<", ">", "/", ";", ":", "'"
    };


    public static final String[] censoredWords = new String[]
            {"fuck", "cock", "dick", "penis", "vagina", "shit", "nigga", "spunk", "cum", "bellend", "asshole", "pricks", "cunt", "anal", "anus", "ass", "ballsack", "balls", "bastard", "bitch", "biatch", "blowjob",
                    "bollock", "bollok", "boner", "boob", "bugger", "buttplug", "clitoris", "cock",
                    "coon", "cunt", "dick", "dildo", "dyke", "fag", "fuck", "fudgepacker", "fudge packer",
                    "homo", "jizz", "labia", "nigger", "nigga", "penis", "piss",
                    "poop", "prick", "pube", "pussy", "queer", "scrotum",
                    "sex", "shit", "sh1t", "slut", "smegma", "tit",
                    "tosser", "twat", "vagina", "wank", "whore",
                    "assmunch", "bimbo", "bukkake", "bunghole", "blue waffle",
                    "faggot", "fisting", "gay sex", "genitals", "futanari", "2g1c",};



    public static final boolean isCensoredWord(String str)
    {
        for(String word : censoredWords)
        {
            if(str.contains(word))
                return true;

        }
        return false;
    }
    /** LETTER_NUMBER_TABLE made: 14/11/26 */
    private static final char[] LETTER_NUMBER_TABLE;

    static {
        StringBuilder tmp = new StringBuilder();

        for (char ch = '0'; ch <= '9'; ++ch) {
            tmp.append(ch);
        }

        for (char ch = 'a'; ch <= 'z'; ++ch) {
            tmp.append(ch);
        }

        LETTER_NUMBER_TABLE = tmp.toString().toCharArray();
    }

    /**
     * Method randomSalt
     * Created on 14/11/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param leng
     *
     * @return
     */
    public static final String randomSalt(int leng) {
        SecureRandom  rando = new SecureRandom();
        StringBuilder salt  = new StringBuilder();

        for (int i = 0; i < leng; i++) {
            if (rando.nextBoolean()) {
                salt.append(SALT_TABLE[rando.nextInt(SALT_TABLE.length)]);
            } else {
                char c = LETTER_NUMBER_TABLE[rando.nextInt(LETTER_NUMBER_TABLE.length)];

                if (rando.nextBoolean()) {
                    c = Character.toUpperCase(c);
                }

                salt.append(c);
            }
        }

        return salt.toString();
    }


    public static final String addSlashes(String data){
        return data.replaceAll("'", "\'");
    }

    public static boolean badUsername(String username) {
        for (char c : username.toCharArray()) {
            if (!Character.isLetterOrDigit(c) && (c != ' ')) {
                return true;
            }
        }

        return username.contains("  ") || (username.charAt(0) == ' ')
                || (username.charAt(username.length() - 1) == ' ');
    }


    /**
     * Method MD5
     * Created on 14/11/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param text
     *
     * @return
     */
    public static String MD5(String text) {
        try {
            MessageDigest md;

            md = MessageDigest.getInstance("MD5");

            byte[] md5hash = new byte[32];

            md.update(text.getBytes("iso-8859-1"), 0, text.length());
            md5hash = md.digest();

            return convertToHex(md5hash);
        } catch (Exception ee) {
            ee.printStackTrace();

            throw new RuntimeException();
        }
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();

        for (int i = 0; i < data.length; i++) {
            int halfbyte  = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;

            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }

                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }

        return buf.toString();
    }
}
