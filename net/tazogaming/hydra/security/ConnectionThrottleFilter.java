package net.tazogaming.hydra.security;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.mina.ConnectionHandler;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/02/2015
 * Time: 19:37
 *
 * @param <T>
 */
public class ConnectionThrottleFilter<T extends Object> {

    /** entries made: 15/02/17 **/
    private Map<T, LinkedList<Long>> entries = new HashMap<T, LinkedList<Long>>();

    /** maxAccounts made: 15/02/17 **/
    private int maxAccounts;

    /** timeLimit made: 15/02/17 **/
    private long timeLimit;

    /**
     * Constructs ...
     *
     *
     * @param maxAccounts
     * @param timeLimit
     */
    public ConnectionThrottleFilter(int maxAccounts, int timeLimit) {
        this.maxAccounts = maxAccounts;
        this.timeLimit   = timeLimit;
    }

    /**
     * Method isThrottled
     * Created on 15/02/17
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     *
     * @return
     */
    public boolean isThrottled(T t) {
        if (!entries.containsKey(t)) {
            return false;
        }

        int              c         = 0;
        LinkedList<Long> entryList = entries.get(t);
        long             l         = 0;
        long             time      = System.currentTimeMillis();
        int              count     = 0;

        for (Iterator<Long> entriesIterator = entryList.iterator(); entriesIterator.hasNext(); ) {
            l = entriesIterator.next();

            if (time - l < this.timeLimit) {
                count++;
            } else {
                entriesIterator.remove();
            }
        }

        return count >= maxAccounts;
    }

    /**
     * Method recordEntry
     * Created on 15/02/17
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     */
    public void recordEntry(T entry) {
        if (entries.containsKey(entry)) {
            entries.get(entry).add(System.currentTimeMillis());
        } else {
            entries.put(entry, new LinkedList<Long>());
        }
    }
}
