package net.tazogaming.hydra.security;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/07/14
 * Time: 19:46
 */

/**
 * @author 'Mystic Flow
 */
public class XTEA {
    private static final int DELTA = -1640531527;
    private static final int SUM   = -957401312;

    /**
     * Method writeInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     * @param index
     * @param buffer
     */
    public static void writeInt(int val, int index, byte[] buffer) {
        buffer[index++] = (byte) (val >> 24);
        buffer[index++] = (byte) (val >> 16);
        buffer[index++] = (byte) (val >> 8);
        buffer[index++] = (byte) val;
    }

    /**
     * Method readInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     * @param buffer
     *
     * @return
     */
    public static int readInt(int index, byte[] buffer) {
        return ((buffer[index++] & 0xff) << 24) | ((buffer[index++] & 0xff) << 16) | ((buffer[index++] & 0xff) << 8)
               | (buffer[index++] & 0xff);
    }

    /**
     * Method decrypt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cryption
     * @param data
     * @param offset
     * @param length
     *
     * @return
     */
    public static byte[] decrypt(int[] cryption, byte[] data, int offset, int length) {
        int k = 0;

        for (int i = 0; i < cryption.length; i++) {
            if (cryption[i] == 0) {
                k++;
            }
        }

        if (k == cryption.length) {
            return data;
        }

        int   numBlocks = (length - offset) / 8;
        int[] dataBlock = new int[2];

        for (int i = 0; i < numBlocks; i++) {
            dataBlock[0] = readInt((i * 8) + offset, data);
            dataBlock[1] = readInt((i * 8) + offset + 4, data);

            long sum   = SUM;
            int  round = 0;

            do {
                dataBlock[1] -= (cryption[(int) ((sum & 0x1933) >>> 11)] + sum
                                 ^ dataBlock[0] + (dataBlock[0] << 4 ^ dataBlock[0] >>> 5));
                sum          -= DELTA;
                dataBlock[0] -= ((dataBlock[1] << 4 ^ dataBlock[1] >>> 5) + dataBlock[1]
                                 ^ cryption[(int) (sum & 0x3)] + sum);
                round++;
            } while (round < 32);

            writeInt(dataBlock[0], (i * 8) + offset, data);
            writeInt(dataBlock[1], (i * 8) + offset + 4, data);
        }

        return data;
    }
}
