package net.tazogaming.hydra.security;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

import java.util.TreeMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/10/13
 * Time: 10:18
 */
public class IPFlags {
    private static TreeMap<Long, Byte> ip_flags = new TreeMap<Long, Byte>();

    /**
     * Method exists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param bitCode
     *
     * @return
     */
    public static boolean exists(long id, int bitCode) {
        if (!ip_flags.containsKey(id)) {
            return false;
        }

        int code = ip_flags.get(id);

        return (code & bitCode) != 0;
    }

    /**
     * Method saveFlags
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void saveFlags() {
        Buffer d = new Buffer(100000);

        for (Long l : ip_flags.keySet()) {
            d.writeQWord(l);
            d.writeByte(ip_flags.get(l));
        }

        d.writeQWord(99);

        try {
            d.save("config/coresettings/ip_flags.dat");
        } catch (Exception ee) {}
    }

    /**
     * Method loadFlags
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadFlags() {
        try {
            Buffer d = new Buffer(new File("config/coresettings/ip_flags.dat"));
            long   l = -1;

            while ((l = d.readQWord()) != 99) {
                IPFlags.put(l, d.readByte());
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method put
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param bitCode
     */
    public static void put(long id, int bitCode) {
        byte base = 0;

        if (ip_flags.size() > 2000) {
            ip_flags.clear();
        }

        if (ip_flags.containsKey(id)) {
            base = ip_flags.get(id);
            ip_flags.remove(id);
        }

        base |= bitCode;
        ip_flags.put(id, base);
        saveFlags();
    }
}
