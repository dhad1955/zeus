package net.tazogaming.hydra.security;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/01/2015
 * Time: 12:13
 */
public class AdminAuthenticator {

    /** validatedUIDS made: 15/01/29 **/
    private static final List<Long> validatedUIDS = new ArrayList<Long>();

    /**
     * Method load
     * Created on 15/01/29
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load() {
        TextFile text = TextFile.fromPath("secure.cfg");

        if (text.exists()) {
            for (Line line : text) {
                validatedUIDS.add(Long.parseLong(line.getData()));
            }
        }
    }

    /**
     * Method addAuth
     * Created on 15/01/29
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     */
    public static void addAuth(long l) {
        validatedUIDS.add(l);

        try {
            save();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Method isAuthenticated
     * Created on 15/01/29
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     *
     * @return
     */
    public static boolean isAuthenticated(long uid) {
        return validatedUIDS.contains(uid);
    }

    /**
     * Method save
     * Created on 15/01/29
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @throws IOException
     */
    public static void save() throws IOException {
        TextFile text = new TextFile();

        for (Long l : validatedUIDS) {
            text.push(Long.toString(l));
        }

        text.save("secure.cfg");
    }
}
