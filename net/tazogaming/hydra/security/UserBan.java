package net.tazogaming.hydra.security;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/12/13
 * Time: 09:42
 */


public class UserBan {
    public enum BanType {
        BLACKLIST, STAKE_BAN, MUTE, WILDERNESS_BAN, NAME_BLACKLIST
    }

    private String username;
    private long   UID;
    private String bannedBy;

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    private String ipAddress = "";

    /**
     * Constructs ...
     *
     *
     * @param username
     * @param bannedBy
     * @param uid
     */
    public UserBan(String username, String bannedBy, long uid, String ipAddress) {
        this.UID      = uid;
        this.username = username;
        this.bannedBy = bannedBy;
        this.ipAddress = ipAddress;
    }
    public String getIpAddress() {
        return this.ipAddress;

    }

    /**
     * Method getUsername
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * Method setUsername
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Method getBannedBy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getBannedBy() {
        return bannedBy;
    }

    /**
     * Method setBannedBy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bannedBy
     */
    public void setBannedBy(String bannedBy) {
        this.bannedBy = bannedBy;
    }

    /**
     * Method getUID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getUID() {
        return UID;
    }

    /**
     * Method setUID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param UID
     */
    public void setUID(long UID) {
        this.UID = UID;
    }
}
