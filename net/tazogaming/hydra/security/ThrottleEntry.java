package net.tazogaming.hydra.security;

import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/02/2015
 * Time: 19:38
 */
public class ThrottleEntry {

    private List<ThrottleEntry> entries = new LinkedList<ThrottleEntry>();
    public ThrottleEntry() {
        this.time = System.currentTimeMillis();
        this.count = 0;
    }

    public int getCount() {
        return count;
    }



    public long getThrottleTime() {
        return time;
    }

    private long time;
    private int count;

}
