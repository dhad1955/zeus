package net.tazogaming.hydra.security;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.LoadResult;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Config;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.net.Socket;
import java.net.UnknownHostException;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/08/14
 * Time: 23:31
 */
public class AccountAuthentication {
    public static final int
        MAX_USERNAME_LENGTH = 12,
        MAX_PASSWORD_LENGTH = 32;

    /** username, password made: 14/08/19 */
    private String username, password;

    /** databaseLink made: 14/08/19 */
    private MysqlConnection databaseLink;

    /**
     * Constructs a new account authenticator
     *
     *
     * @param connection database link to use
     * @param username The given username
     * @param password The given password
     */
    public AccountAuthentication(String username, String password, MysqlConnection connection) {
        this.databaseLink = connection;
        this.username     = username;
        this.password     = password;
    }

    /**
     * Method authenticate
     * Created on 14/08/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LoadResult authenticate() {
        Map<String, Object> returnData = new HashMap<String, Object>();

        if (Main.TEST_MODE) {
            returnData.put("member_id", new Random().nextInt(1000));
            returnData.put("usergroup_id", 2);

            return new LoadResult(LoadResult.OK, returnData);
        }

        if ((username.length() > MAX_USERNAME_LENGTH) || Security.badUsername(username)) {
            return new LoadResult(LoadResult.INVALID_PASS, null);
        }

        ResultSet databaseResult =
            databaseLink.query(
                    "SELECT `name`, `members_pass_hash`, `member_id`, `members_pass_salt`, `member_banned`, `whitekills`, `redkills`, `whitedeaths`, `reddeaths`, `kills`, `deaths`, `streak`, `member_group_id`,  `msg_count_new`, `mgroup_others` "
                            + "FROM `members` WHERE name='" + username + "'");

        try {
            if (!databaseResult.next()) {
                try {
                    System.err.println("User :"+username+" does not exist, creating new member");
                    int result = createMember();

                    if (result > 0) {

                        // time to verify it was all okay
                        ResultSet res =databaseLink.query("SELECT `name`, `member_id` from members where member_id='"
                                + result + "' AND name='" + username + "'");

                        if (!res.next()) {
                            res.close();
                            throw new RuntimeException("Error, user mismatch bad id " + result + " " + username);
                        }
                        res.close();

                        for (int i = 0; i < 25; i++) {
                            databaseLink.query(
                                    "INSERT into hs_skills(userid, skillid,level,prestige,experience) VALUES('" + result
                                            + "', " + i + ", 0, 0, 0);");
                        }

                        returnData.put("usergroup_id", 3);
                        returnData.put("member_id", result);
                        returnData.put("pmunread", 0);
                        returnData.put("othergroups", 0);


                        return new LoadResult(LoadResult.OK, returnData);
                    }

                    throw new RuntimeException("invalid result: " + result);
                } catch (Exception ee) {
                        ee.printStackTrace();

                    return new LoadResult(LoadResult.AUTHENTICATION_SERVER_OFFLINE, null);
                }
            } else {
                String salt     = databaseResult.getString("members_pass_salt");
                String password = databaseResult.getString("members_pass_hash");
                String userhash = Security.MD5(Security.MD5(salt) + Security.MD5(this.password));

                if (!password.equals(userhash)) {
                    return new LoadResult(LoadResult.INVALID_PASS, returnData);
                }

                if (databaseResult.getInt("member_banned") > 0) {
                    return new LoadResult(LoadResult.BANNED, returnData);
                }

                int usergroupid = databaseResult.getInt("member_group_id");

                if (usergroupid == 5) {
                    return new LoadResult(LoadResult.BANNED, returnData);
                }

                int       memberId = databaseResult.getInt("member_id");
                ResultSet result1  = databaseLink.query("SELECT * FROM referrals WHERE refer_id='" + memberId
                        + "' AND STATUS < 2");
                int     completed  = 0;
                int     pending    = 0;
                boolean hasPending = false;

                while (result1.next()) {
                    if (result1.getInt("status") == 0) {
                        pending++;
                    } else if (result1.getInt("status") == 1) {
                        completed++;
                    }
                }

                if (completed > 0) {
                    databaseLink.query("UPDATE referrals set status='2' where status='1' AND refer_id='"
                            + memberId + "'");
                }

                ResultSet myPending =
                    World.getWorld().getDatabaseLink().query("SELECT * FROM referrals WHERE userid='" + memberId
                            + "' AND status='0'");

                hasPending = myPending.next();
                returnData.put("completed_referrals", completed);
                returnData.put("referral_id", hasPending
                                              ? myPending.getInt("refer_id")
                                              : -1);
                returnData.put("has_pending", hasPending);
                returnData.put("pending_referrals", pending);
                returnData.put("usergroup_id", usergroupid);
                returnData.put("member_id", memberId);
                returnData.put("pmunread", databaseResult.getInt("msg_count_new"));
                returnData.put("othergroups", databaseResult.getString("mgroup_others"));
                returnData.put("kills", databaseResult.getInt("kills"));
                returnData.put("deaths", databaseResult.getInt("deaths"));
                returnData.put("whitekills", databaseResult.getInt("whitekills"));
                returnData.put("redkills", databaseResult.getInt("redkills"));
                returnData.put("reddeaths", databaseResult.getInt("reddeaths"));
                returnData.put("whitedeaths", databaseResult.getInt("whitedeaths"));
                returnData.put("streak", databaseResult.getInt("streak"));

                return new LoadResult(LoadResult.OK, returnData);
            }
        } catch (SQLException ee) {
            ee.printStackTrace();
            returnData.put("error", ee);

            return new LoadResult(LoadResult.COULD_NOT_COMPLETE_LOGIN, returnData);
        } catch (Exception ee) {
            ee.printStackTrace();

            return new LoadResult(LoadResult.BAD_SESSION_ID, returnData);
        }
    }




    private int createMember() throws UnknownHostException, IOException {
        System.err.println("connect: "+Config.ACC_SERVER_ADDR+" "+Config.ACC_PORT);
        Socket socket = new Socket(Config.ACC_SERVER_ADDR, Config.ACC_PORT);

        socket.setSoTimeout(15000);

        PrintWriter writer = new PrintWriter(socket.getOutputStream());

        writer.write(username);
        writer.write("|");
        writer.write(password);
        writer.flush();
        socket.getOutputStream().flush();

        InputStreamReader reader     = new InputStreamReader(socket.getInputStream());
        InputStream       in         = socket.getInputStream();
        int               statusCode = in.read();

        if (statusCode == 7) {
            throw new RuntimeException("Account already exists!");
        }

        if (statusCode == 3) {

           throw new RuntimeException("Error, this ip is not permitted to connect to this account server");
        }

        if (statusCode == 8) {
            throw new RuntimeException("Error mysql error at the other end");
        }

        DataInputStream dis = new DataInputStream(in);
        int             val = dis.readInt();

        try {
            socket.close();
            in.close();
            dis.close();
        } catch (IOException ee) {}

        return val;
    }
}
