package net.tazogaming.hydra.security;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/06/2015
 * Time: 21:26
 */
public class RestrainingOrder {


    public RestrainingOrder(long restrainedUser, long restrainedTo) {
        this.restrainedUser = restrainedUser;
        this.restrainedTo = restrainedTo;
    }

    private long restrainedUser;

    public long getRestrainedTo() {
        return restrainedTo;
    }

    public long getRestrainedUser() {
        return restrainedUser;
    }

    private long restrainedTo;
}
