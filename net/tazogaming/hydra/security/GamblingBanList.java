package net.tazogaming.hydra.security;

import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.script.runtime.instr.impl.NumberFormat;
import net.tazogaming.hydra.util.Text;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/11/2014
 * Time: 02:02
 */
public class GamblingBanList {

    public static final String SAVE_PATH = "./config/stakebans.list";

    private static List<Long> bans = new LinkedList<Long>();

    public static boolean isBanned(long l){
        for(Long longz : bans)
            if(longz == l)
                return true;
        return false;
    }

    public static void addBan(long uid){
        bans.add(uid);
    }

    public static void removeBan(long uid){
        bans.remove(uid);
    }

    public static void load() {
        TextFile file = TextFile.fromPath(SAVE_PATH);
        if(file.exists()){
            for(Line line : file){
                try {
                    bans.add(Long.parseLong(line.getData()));
                }catch (NumberFormatException ignored) { }
            }
        }
    }

    public static void save(){
        TextFile bannedList = new TextFile();
        for(Long l : bans){
            bannedList.push(Long.toString(l));
        }
       try {
           bannedList.save(SAVE_PATH);
       }catch (IOException ee) {
           System.err.println("Failed to save gambling ban list");
       }
    }

}
