package net.tazogaming.hydra.security;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/12/13
 * Time: 09:41
 */
public class BanList {
    private static ArrayList<UserBan> banArrayList = new ArrayList<UserBan>();

    /**
     * Method loadBans
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadBans() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("config/blacklisted.users"));
            String         line   = null;

            while ((line = reader.readLine()) != null) {
                if ((line.length() == 0) || line.startsWith("//")) {
                    continue;
                }

                try {
                    String[] split = line.split("\\$");
                    UserBan  b     = new UserBan(split[0], split[1], Long.parseLong(split[2]), split.length > 3 ? split[3] : split[2]);

                    banArrayList.add(b);
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }

            reader.close();
        } catch (IOException e) {
            e.printStackTrace();    // To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * Method saveBans
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void saveBans() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("config/blacklisted.users"));

            for (UserBan b : banArrayList) {
                writer.write(b.getUsername() + "$" + b.getBannedBy() + "$" + b.getUID()+"$"+b.getIpAddress());
                writer.newLine();
            }

            writer.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method addBan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param toBan
     * @param bannedBy
     */
    public static void addBan(Player toBan, Player bannedBy) {
        banArrayList.add(new UserBan(toBan.getUsername(), bannedBy.getUsername(), toBan.getUID(), toBan.getIoSession().getUserIP()));
        toBan.getActionSender().sendLogout();
        bannedBy.getActionSender().sendMessage(toBan.getUsername() + "@" + toBan.getUID() + " has been blacklisted");
        saveBans();
    }

    /**
     * Method addBan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param toBan
     * @param bannedBy
     */
    public static void addBan(Player toBan, String bannedBy) {
        banArrayList.add(new UserBan(toBan.getUsername(), bannedBy, toBan.getUID(), toBan.getIoSession().getUserIP()));
        saveBans();
    }

    /**
     * Method addBan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     * @param bannedBy
     */
    public static void addBan(long uid, Player bannedBy) {
        banArrayList.add(new UserBan("*", bannedBy.getUsername(), uid, ""+uid));
        bannedBy.getActionSender().sendMessage("uid: " + uid + " has been blacklisted");
    }

    /**
     * Method isBanned
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     *
     * @return
     */
    public static boolean isBanned(long uid, String ip) {
        for (UserBan ban : banArrayList) {
            if (ban.getUID() == uid ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method listBans
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public static void listBans(Player plr) {
        for (UserBan ban : banArrayList) {
            plr.getActionSender().sendMessage(ban.getUsername() + "@" + ban.getUID() + " banned by "
                                              + ban.getBannedBy());
        }
    }

    /**
     * Method removeBan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param username
     * @param player
     */
    public static void removeBan(String username, Player player) {
        UserBan b = null;

        for (UserBan ban : banArrayList) {
            if (ban.getUsername().equalsIgnoreCase(username)) {
                b = ban;

                break;
            }
        }

        if (b == null) {
            return;
        }

        banArrayList.remove(b);
        player.getActionSender().sendMessage("User: " + b.getUsername() + "@" + b.getUID() + " has been unblacklisted");
        saveBans();
    }
}
