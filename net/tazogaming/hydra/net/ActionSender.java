package net.tazogaming.hydra.net;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.HintIcon;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.map.MapXTEA;
import net.tazogaming.hydra.map.Palette;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.packetbuilder.GPI;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.util.Text;

/**
 * These classes simplify network tasks, so you shouldn't have to mess about
 * with StaticPacketBuilders yourself. Those packets whose purpose I don't
 * understand are left as 'sendUnknownId'.
 *
 * NOTE: These packets DO NOT modify player state. They simply send data.
 */
public class ActionSender {
	public static int[] vars = { 50, 40, 35, 45, 65, 15, 90 };
	public static int PROJECTILE_TYPE = 0;

	/**
	 * Method createArrow Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param height
	 * @param pos
	 */
	public static int HINT_ICON[] = { 0, 65, -1 };

	/** noMap made: 14/08/23 */
	private boolean noMap = false;

	/** loginSent made: 14/08/23 */
	private boolean loginSent = false;
	boolean sentShit = false;

	/** player made: 14/08/23 */
	private Player player;

	/**
	 * Constructs ...
	 *
	 *
	 * @param player
	 */
	public ActionSender(Player player) {
		this.player = player;
	}

	/**
	 * Method sendItemOnInterface Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 * @param interfaceId
	 * @param child
	 * @param size
	 * @param id
	 */
	public static void sendItemOnInterface(Player player, int interfaceId, int child, int size, int id) {
		player.dispatch(new MessageBuilder(91, Packet.Size.Fixed).addLEShortA(id).addInt(size)
				.addInt2(interfaceId << 16 | child));
	}

	/**
	 * Method sendConstructionStatus Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param status
	 */
	public void sendConstructionStatus(int status) {

		/*
		 * if (status == 0) { sendMinimapState(4); } else if (status == 5) {
		 * sendMinimapState(5); } else { sendMinimapState(3); }
		 */
	}

	/**
	 * Method sendClanWar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 *
	 * @param player
	 * @param soundId
	 * @param volume
	 * @param soundSpeed
	 * @param voice
	 */
	public static void sendSound(Player player, int soundId, int volume, int soundSpeed, boolean voice) {
		player.dispatch(new MessageBuilder(voice ? 7 : 69, Packet.Size.Fixed).addShort(soundId) // sound

				// id
				.addByte((byte) 1) // times played
				.addShort(0) // delay before starting
				.addByte(volume).addShort(soundSpeed) // speed of the sound,

		// 255 seems normal
		);
	}

	/**
	 * Method sendClanWar Created on 14/10/30
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param war
	 */
	public void sendClanWar(ClanWar war) {

		/*
		 * MessageBuilder builder = new
		 * MessageBuilder().setId(6).setSize(Packet.Size.VariableByte);
		 *
		 * if (war == null) { builder.addByte((byte) 0);
		 * player.getIoSession().write(builder.toPacket());
		 *
		 * return; }
		 *
		 * builder.addByte((byte)
		 * (war.getGameRules().isset(ClanWarRules.KNOCK_OUT) ? 1 : (war.isWar()
		 * ? 3 :2)));
		 *
		 * builder.addByte((byte)(war.getGameRules().isset(ClanWarRules.
		 * KEEP_ITEMS) ? 1 : 0)); if
		 * (war.getGameRules().isset(ClanWarRules.KNOCK_OUT)) {
		 * builder.addByte((byte)
		 * war.getTeam(war.getTeamID(player)).getPlayers().size());
		 * builder.addByte((byte) war.getTeam((war.getTeamID(player) + 1) %
		 * 2).getPlayers().size()); } else { int killsToWin =
		 * war.getGameRules().getKillsToWin(); int myKills =
		 * war.getTeam(war.getTeamID(player)).getScore(); int thereKills =
		 * war.getTeam((war.getTeamID(player) + 1) % 2).getScore();
		 *
		 * builder.addShort(killsToWin).addShort(myKills).addShort(thereKills);
		 * }
		 *
		 * player.getIoSession().write(builder.toPacket());
		 */
	}

	/**
	 * Method initClan Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param sc
	 */
	public void initClan(ClanChannel sc) {

		/*
		 * MessageBuilder spb = new
		 * MessageBuilder().setSize(Packet.Size.VariableShort).setId(28);
		 *
		 * spb.addString(sc.getName());
		 * spb.addString(Data.nameForLong(sc.getChannelOwner()));
		 * spb.addShort(1); spb.addByte((byte) (0));
		 *
		 * ArrayList<Player> chatters = sc.getPlayersSorted();
		 *
		 * spb.addByte((byte) chatters.size());
		 *
		 * for (Player pl2 : chatters) { spb.addLong(pl2.getUsernameHash());
		 * spb.addByte((byte) sc.getRankForPlayer(pl2)); }
		 *
		 * player.getIoSession().write(spb.toPacket());
		 */
	}

	/**
	 * Method sendRequestText Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param str
	 * @param anagramId
	 */
	public void sendRequestText(String str, int anagramId) {

		/*
		 * player.getIoSession().write( new
		 * MessageBuilder().setId(187).setSize(Packet.Size.VariableShort).
		 * addString(str).addByte( (byte) anagramId).toPacket());
		 */
	}

	/**
	 * Method sendPlaytime Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendPlaytime() {

		/*
		 * int ticks = player.getSettings().getPlayTime(); int minutes = (ticks
		 * / 100) % 60; int hours = ((ticks / 100) / 60) % 24; int days =
		 * (((ticks / 100) / 60) / 24) % 365; StringBuilder sb = new
		 * StringBuilder();
		 *
		 * if (days > 0) { sb.append(days).append(" day").append((days == 1) ?
		 * "" : "s").append(" "); }
		 *
		 * if (hours > 0) { sb.append(hours + " hour" + ((hours == 1) ? "" :
		 * "s ")); }
		 *
		 * sb.append(minutes).append(" min").append((minutes == 1) ? "" : "s");
		 * player.getActionSender().changeLine("Playtime: @whi@" +
		 * sb.toString(), 39949);
		 */
	}

	/**
	 * Method closeTimers Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void closeTimers() {

		// MessageBuilder spb = new MessageBuilder().setId(116);
		// player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method sendTimer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param secs
	 */
	public void sendTimer(int id, int secs) {

		/*
		 * MessageBuilder spb = new MessageBuilder().setId(115).addByte((byte)
		 * id).addLEShort(secs * 50 / 30);
		 *
		 * if ((player.getIoSession() != null) && (spb != null)) {
		 * player.getIoSession().write(spb.toPacket()); }
		 */
	}

	/**
	 * Method sendZombiePoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param score
	 */
	public void sendZombiePoints(int score) {

		/*
		 * MessageBuilder spb = new MessageBuilder().setId(118).addInt(score);
		 *
		 * player.getIoSession().write(spb.toPacket());
		 */
	}

	/**
	 * Method sendZombieData Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 * @param k
	 */
	public void sendZombieData(int i, int k) {

		/*
		 * MessageBuilder spb = new MessageBuilder().setId(119).addByte((byte)
		 * i).addByte((byte) k);
		 *
		 * player.getIoSession().write(spb.toPacket());
		 */
	}

	/**
	 * Method animateObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param objx
	 * @param objy
	 * @param animationID
	 * @param orientation
	 *
	 * @return
	 */
	public ActionSender animateObject(int objx, int objy, int animationID, int orientation) {

		/*
		 * MessageBuilder itemPacketBuilder = new
		 * MessageBuilder().setId(85).addByteC((objy - 8 *
		 * player.getKnownRegion()[1])).addByteC((objx - 8 *
		 * player.getKnownRegion()[0]));
		 *
		 * player.getIoSession().write(itemPacketBuilder.toPacket());
		 * player.getIoSession().write(new
		 * MessageBuilder().setId(160).addByteB((byte) 0).addByteB((byte) ((10
		 * << 2) + (orientation & 3))).addShortA(animationID).toPacket());
		 */
		return this;
	}

	/**
	 * Method interfaceColor Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param color
	 */
	public void interfaceColor(int id, int color) {

		/*
		 * MessageBuilder p = new MessageBuilder().setId(122);
		 *
		 * p.addLEShortA(id); p.addLEShortA(color);
		 * player.getIoSession().write(p.toPacket());
		 */
	}

	/**
	 * Method markPlayer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param slot
	 */
	public void markPlayer(int slot) {

		/*
		 * player.getIoSession().write( new
		 * MessageBuilder().setId(254).addByte((byte)
		 * 10).addShort(slot).addShort(4).addByte( (byte) 1).toPacket());
		 */
	}

	/**
	 * Method markNpc Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param slot
	 */
	public void markNpc(int slot) {

		/*
		 * player.getIoSession().write( new
		 * MessageBuilder().setId(254).addByte((byte)
		 * 1).addShort(slot).addShort(4).addByte( (byte) 1).toPacket());
		 */
	}

	/**
	 * Method changeSidebar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param sidebarid
	 */
	public void changeSidebar(int sidebarid) {

		// player.getIoSession().write(new
		// MessageBuilder().setId(106).addByteC((byte) sidebarid).toPacket());
	}

	/**
	 * Method moveCameraToLocation Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param location
	 * @param zPos
	 * @param constantSpeed
	 * @param variableSpeed
	 */
	public void moveCameraToLocation(Point location, int zPos, int constantSpeed, int variableSpeed) {
		MessageBuilder bldr = new MessageBuilder().setId(166);

		bldr.addByteS(50);
		bldr.addByteA(0);
		bldr.addByte((byte) (location.getLocalX(player)));
		bldr.addByte((byte) (location.getLocalY(player)));
		bldr.addShort(zPos);
		bldr.addByte((byte) constantSpeed);
		bldr.addByte((byte) variableSpeed);
	}

	/**
	 * Method sendMinimapState Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 */
	public void sendMinimapState(int i) {

		// player.getIoSession().write(new
		// MessageBuilder().setId(99).addByte((byte) i).toPacket());
	}

	/**
	 * Method sendClientScript Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param call
	 */
	public void sendClientScript(CS2Call call) {
		MessageBuilder bldr = new MessageBuilder(16, Packet.Size.VariableShort);

		call.append(bldr);
		player.dispatch(bldr);
	}

	/**
	 * Method flashSidebar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 */
	public void flashSidebar(int i) {
		MessageBuilder b2 = new MessageBuilder();

		b2.setId(24);
		b2.addByte((byte) i);

		// player.getIoSession().write(b2.toPacket());
	}

	/**
	 * Method sendAlert Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param message
	 * @param msgs
	 * @param speed
	 * @param timeout
	 */
	public void sendAlert(String message, String msgs, int speed, int timeout) {
		MessageBuilder builder = new MessageBuilder();

		builder.setId(76);
		builder.setSize(Packet.Size.VariableShort);
		builder.addString(message);
		builder.addString(msgs);
		builder.addByte((byte) speed);
		builder.addShort((timeout));
	}

	/**
	 * Method sendConfigByFile Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param data
	 */
	public void sendConfigByFile(int id, int data) {
		MessageBuilder bldr = new MessageBuilder(27, Packet.Size.Fixed);

		bldr.addInt1(data).addShort(id);
		player.dispatch(bldr);
	}

	/**
	 * Method focusCameraToLocation Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param location
	 * @param zPos
	 * @param constantSpeed
	 * @param variableSpeed
	 */
	public void focusCameraToLocation(Point location, int zPos, int constantSpeed, int variableSpeed) {
		MessageBuilder bldr = new MessageBuilder().setId(177);

		bldr.addByte((byte) (location.getLocalX(player)));
		bldr.addByte((byte) (location.getLocalY(player)));
		bldr.addShort(zPos);
		bldr.addByte((byte) constantSpeed);
		bldr.addByte((byte) variableSpeed);

		// player.getIoSession().write(bldr.toPacket());
	}

	/**
	 * Method resetCamera Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void resetCamera() {

		// player.getIoSession().write(new
		// MessageBuilder().setId(107).toPacket());
	}

	/**
	 * Method runScript Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void runScript(int id) {
		MessageBuilder bldr = new MessageBuilder(98, Packet.Size.VariableShort);

		bldr.addShort(0);
		bldr.addRS2String("");
		bldr.addInt(id);
		player.dispatch(bldr);
	}

	/**
	 * Method sendCameraShake Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param intensity
	 */
	public void sendCameraShake(int intensity) {
		MessageBuilder p = new MessageBuilder().setId(35);

		p.addByte((byte) 0);
		p.addByte((byte) intensity);
		p.addByte((byte) intensity);
		p.addByte((byte) intensity);

		// player.getIoSession().write(p.toPacket());
	}

	/**
	 * Method sendCameraShake Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 * @param intensity
	 */
	public void sendCameraShake(int i, int intensity) {
		MessageBuilder p = new MessageBuilder().setId(35);

		p.addByte((byte) i);
		p.addByte((byte) intensity);
		p.addByte((byte) intensity);
		p.addByte((byte) intensity);

		// player.getIoSession().write(p.toPacket());
	}

	/**
	 * Method hideInter Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 * @param k
	 */
	public void hideInter(int i, int k) {
		MessageBuilder spb = new MessageBuilder().setId(171).addByte((byte) k).addShort(i);

		// player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method replaceColor Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param str
	 * @param key
	 */
	public void replaceColor(String str, String key) {
		MessageBuilder p = new MessageBuilder().setId(179).setSize(Packet.Size.VariableShort);

		p.addString(str).addString(key);

		// player.getIoSession().write(p.toPacket());
	}

	/**
	 * Method sendMulti Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param m
	 */
	public void sendMulti(int m) {

		// player.getIoSession().write(new
		// MessageBuilder().setId(61).addByte((byte) m).toPacket());
	}

	/**
	 * Method playerPic Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param frame
	 */
	public void playerPic(int frame) {
		MessageBuilder packetBuilder = new MessageBuilder();

		packetBuilder.setId(185);
		packetBuilder.addShortB(frame);

		// player.getIoSession().write(packetBuilder.toPacket());
	}

	/**
	 * Method sendClanMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param clanName
	 * @param playername
	 * @param message
	 * @param rights
	 */
	public void sendClanMessage(String clanName, String playername, String message, int rights) {
		if ((clanName == null) || (playername == null) || (message == null) || (rights < 0)) {
			return;
		}

		MessageBuilder spb = new MessageBuilder().setSize(Packet.Size.VariableShort).setId(220);

		spb.addString(clanName).addString(playername).addString(message).addByte((byte) rights);

		// player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method loadpm Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param world
	 * @param friend
	 * @param ranking
	 */
	public void loadpm(int world, long friend, int ranking) {
		if (world == 0) {
			world = 1;
		} else {
			world += 9;
		}

		if (player.getAccount().hasVar("channel_name")) {
			ClanChannel c = World.getWorld().getChannelList().getChannel(player.getAccount().getString("channel_name"));

			if (c != null) {
				ranking = c.getRankForPlayer(friend);
			}
		}

		// player.getIoSession().write(
		// new MessageBuilder().setId(50).addLong(friend).addByte((byte)
		// (world)).addByte(
		// (byte) ranking).toPacket());
	}

	/**
	 * Method setModelRotation Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param rotation1
	 * @param rotation2
	 * @param zoom
	 */
	public void setModelRotation(int id, int rotation1, int rotation2, int zoom) {
		MessageBuilder spb = new MessageBuilder().setId(230);

		spb.addShortA(zoom);
		spb.addShort(id);
		spb.addShort(rotation1);
		spb.addLEShortA(rotation2);

		// player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method npcPic Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param npcID
	 * @param interfaceID
	 */
	public synchronized void npcPic(int npcID, int interfaceID) {
		MessageBuilder packetBuilder = new MessageBuilder();

		packetBuilder.setId(75);
		packetBuilder.addShortB(npcID);

		// packetBuilder.addShortB(interfaceID);
		// player.getIoSession().write(packetBuilder.toPacket());
	}

	/**
	 * Method overlay Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void overlay(int id) {
		MessageBuilder spb = new MessageBuilder().setId(208).addWordBigEndian(id);

		// player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method sendStillGFX Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void sendStillGFX(int id, int x, int y) {
		MessageBuilder p1 = new MessageBuilder().setId(85);

		p1.addByteC((y - 8 * player.getKnownRegion()[1])).addByteC((x - 8 * player.getKnownRegion()[0]));

		// player.getIoSession().write(p1.toPacket());
		MessageBuilder p2 = new MessageBuilder().setId(4);

		p2.addByte((byte) 0);
		p2.addShort(id);
		p2.addByte((byte) 25);
		p2.addShort(0);

		// player.getIoSession().write(p2.toPacket());
	}

	/**
	 * Method sendClanTimer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param timer
	 * @param status
	 */
	public void sendClanTimer(int timer, int status) {
		MessageBuilder br = new MessageBuilder().setId(7).setSize(Packet.Size.Fixed);

		br.addByte((byte) status);
		br.addLEShort(timer);

		// player.getIoSession().write(br.toPacket());
	}

	/**
	 * Method sendModelGrid Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param items
	 * @param itemsCount
	 * @param interfaceid
	 */
	public void sendModelGrid(Item[] items, int[] itemsCount, int interfaceid) {
		MessageBuilder p = new MessageBuilder().setId(53).setSize(Packet.Size.VariableShort);
		Item[] items2;
		int[] itemsCount2;

		if (items.length > 352) {
			items2 = new Item[352];
			itemsCount2 = new int[352];
			System.arraycopy(items, 0, items2, 0, 352);
			System.arraycopy(itemsCount, 0, itemsCount2, 0, 352);
		} else {
			items2 = items;
			itemsCount2 = itemsCount;
		}

		p.addShort(interfaceid);
		p.addShort(items2.length);

		for (int i = 0; i < items2.length; i++) {
			if (itemsCount[i] > 254) {
				p.addByte((byte) 255);
				p.addInt2(itemsCount2[i]);
			} else {
				p.addByte((byte) itemsCount2[i]);
			}

			if (items[i] == null) {
				p.addShortB(0);
			} else {
				p.addShortB(items2[i].getIndex() + 1); // interfaces id
			}
		}

		// player.getIoSession().write(p.toPacket());
	}

	/**
	 * Method sendModelGrid Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param items
	 * @param itemsCount
	 * @param interfaceid
	 */
	public void sendModelGrid(int[] items, int[] itemsCount, int interfaceid) {
		MessageBuilder p = new MessageBuilder().setId(53).setSize(Packet.Size.VariableShort);

		p.addShort(interfaceid);
		p.addShort(items.length);

		for (int i = 0; i < items.length; i++) {
			if (itemsCount[i] > 254) {
				p.addByte((byte) 255);
				p.addInt2(itemsCount[i]);
			} else {
				p.addByte((byte) itemsCount[i]);
			}

			if (items[i] == -1) {
				p.addShortB(0);
			} else {
				p.addShortB(items[i] + 1); // interfaces id
			}
		}

		// player.getIoSession().write(p.toPacket());
	}

	/**
	 * Method sendBagInterface Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param i
	 * @param i2
	 */
	public void sendBagInterface(int i, int i2) {
		MessageBuilder p = new MessageBuilder().setId(248);

		p.addShortA(i);
		p.addShort(i2);

		// player.getIoSession().write(p.toPacket());
	}

	/**
	 * Method requestAmount Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void requestAmount() {

		// player.getIoSession().write(new
		// MessageBuilder().setId(27).toPacket());
	}

	/**
	 * Method createProjectile Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param projectile
	 */
	public void createProjectile(Projectile projectile) {
		int y = projectile.getStartY() - (player.getKnownRegion()[1]) * 8;
		int x = projectile.getStartX() - (player.getKnownRegion()[0]) * 8;
		MessageBuilder bldr = new MessageBuilder(15, Packet.Size.VariableShort);

		bldr.addByte(player.getHeight());
		bldr.addByteC(y >> 3).addByteA(x >> 3);
		bldr.addByte(13);
		x = projectile.getStartX() - ((projectile.getStartX() >> 3) << 3);
		y = projectile.getStartY() - ((projectile.getStartY() >> 3) << 3);
		bldr.addByte((x & 0x7) << 3 | y & 0x7);
		bldr.addByte(projectile.getOffsetX());
		bldr.addByte(projectile.getOffsetY());
		bldr.addShort(projectile.getLockon());
		bldr.addShort(projectile.getGraphicId());
		bldr.addByte(projectile.getStartHeight());
		bldr.addByte(projectile.getEndHeight());
		bldr.addShort(projectile.getWaitSpeed());
		bldr.addShort(projectile.getMoveSpeed());
		bldr.addByte(1);
		bldr.addShort(1);
		player.dispatch(bldr);
	}

	private void sendPlayersOnLogin(MessageBuilder stream) {
		stream.startBitAccess();
		stream.addBits(player.getLocation().getX() << 14 | player.getLocation().getY() & 0x3fff
				| player.getLocation().getHeight() << 28, 30);

		int playerIndex = player.getIndex();

		player.validatedWatchedPlayers();
		player.updateWatchedPlayers();

		for (int index = 1; index < 2048; index++) {
			if (index == playerIndex) {
				continue;
			}

			Player other = World.getWorld().getPlayers().get(index);

			if (other == null) {
				stream.addBits(0, 18);

				continue;
			}

			if (player.getWatchedPlayers().getKnownEntities().contains(other)) {
				stream.addBits(other.getLocation().get18BitsHash(), 18);
			} else {
				stream.addBits(0, 18);
			}
		}

		stream.finishBitAccess();
	}

	/**
	 * Method sendDev Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param msg
	 */
	public void sendDev(String msg) {
		sendChatMessage(99, msg);
	}

	/**
	 * Method textEffectsForRank Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param rank
	 *
	 * @return
	 */
	public static final String textEffectsForRank(int rank) {
		switch (rank) {
		case 0:
			return "";

		case Player.ROOT_ADMIN:
			return " ";
		case Player.MODERATOR:
			return "<col=666699>";

		case Player.ADMINISTRATOR:
			return "<col=660066>";

		case Player.DONATOR:
			return "<col=008A00>";

		case Player.PLATINUM:
			return "<col=3F3F43>";

		case Player.PATRON:
			return " <col=4451>";
		}

		return "";
	}

	/**
	 * Method sendYellMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param rights
	 * @param username
	 * @param msg
	 */
	public synchronized void sendYellMessage(int rights, String username, String title, int ironMode, String msg) {

		int crown = GPI.getCrownForRank(rights, ironMode);
		boolean isAdmin = false;

		if (player.getFriendsList().isIgnored(username)) {
			return;
		}

		if (crown == 1) {
			crown = 0;
		}

		if (crown == 2) {
			crown = 1;
		} else if (crown == 0) {
			crown = 0;
			isAdmin = true;
		}

		String chatCol = "";
		if (player.getGameFrame().getRoot() == 548) {
			chatCol = "0033cc";
		} else {
			chatCol = "8BF53B";
		}

		String yellTitle = title;

		if (yellTitle.length() > 0)
			yellTitle = yellTitle + " ";
		String titleColor = player.getGameFrame().getRoot() == 746 ? "FFFF00" : "6600FF";
		if ((crown > 0) || isAdmin) {
			sendMessage("<col=" + ((player.getGameFrame().getRoot() == 548) ? "0" : "FFFFFF") + ">[<col=" + chatCol
					+ ">Global</col>]<col=" + ((player.getGameFrame().getRoot() == 548) ? "0" : "FFFFFF") + "> <img="
					+ crown + ">" + textEffectsForRank(rights) + "<col=" + titleColor + ">" + yellTitle + "</col>"
					+ username + "<col=0>: <col=" + ((player.getGameFrame().getRoot() == 746) ? "FF6600" : "993030")
					+ ">" + msg);
		} else {
			sendMessage("[<col=" + chatCol + ">Yell</col>]<col="
					+ ((player.getGameFrame().getRoot() == 548) ? "0" : "FFFFFF") + "> " + textEffectsForRank(rights)
					+ "<col=" + titleColor + ">" + yellTitle + "</col> " + username + "<col=0>: <col="
					+ ((player.getGameFrame().getRoot() == 746) ? "FF6600" : "993030") + ">" + msg);
		}
	}

	/**
	 * Method sendPublicChatMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param playerIndex
	 * @param rights
	 * @param fx
	 * @param message
	 */
	public void sendPublicChatMessage(int playerIndex, int rights, int fx, String message) {
		MessageBuilder bldr = new MessageBuilder(62, Packet.Size.VariableByte);

		if (rights < 0) {
			rights = 0;
		}

		bldr.addShort(playerIndex);
		bldr.addShort(fx); // effects, todo later
		bldr.addByte(rights);

		byte[] chatStr = new byte[256];

		chatStr[0] = (byte) message.length();

		int offset = 1 + Text.huffmanCompress(Text.ucFirst(message), chatStr, 1);

		bldr.addBytes(chatStr, 0, offset);
		player.dispatch(bldr);
	}

	/**
	 * Method sendChatMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param textType
	 * @param text
	 */
	public void sendChatMessage(int textType, String text) {
		if ((text == null) || (text.length() < 1)) {
			return;
		}

		MessageBuilder bldr = new MessageBuilder(53, Packet.Size.VariableByte);

		if (text.length() > 245) {
			text = text.substring(0, 245);
		}

		bldr.addByte(textType);
		bldr.addInt(0);
		bldr.addByte(0);
		bldr.addRS2String(text);
		player.dispatch(bldr);
	}

	/**
	 * Method sendSoftMapArea Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendSoftMapArea() {
		if (player.is_con) {
			return;
		}

		if (noMap) {
			return;
		}

		if (player.getLocation() == null) {
			throw new RuntimeException();
		}

		int regionX = player.getLocation().getRegionX();
		int regionY = player.getLocation().getRegionY();
		MessageBuilder spb = new MessageBuilder().setId(80).setSize(Packet.Size.VariableShort);

		spb.addLEShortA(regionY + 6);
		spb.addShortA(player.getLocation().getRegionX() + 6);
		spb.addByte(0);
		spb.addByteA(0); // reload

		for (int xCalc = (regionX) / 8; xCalc <= ((regionX + 12) / 8); xCalc++) {
			for (int yCalc = (regionY) / 8; yCalc <= ((regionY + 12) / 8); yCalc++) {
				int region = yCalc + (xCalc << 8); // 1786653352
				int[] mapData = MapXTEA.getMapData().get((short) region);

				if (mapData == null) {
					mapData = new int[4];

					for (int i = 0; i < 4; i++) {
						mapData[i] = 0;
					}
				}

				spb.addInt(mapData[0]);
				spb.addInt(mapData[1]);
				spb.addInt(mapData[2]);
				spb.addInt(mapData[3]);
			}
		}

		player.setKnownRegion(regionX, regionY);
		player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method sendMapArea Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param first
	 */
	public void sendMapArea(boolean first) {
		if (player.is_con) {
			return;
		}

		if (noMap) {
			return;
		}

		if (player.getLocation() == null) {
			throw new RuntimeException();
		}

		int regionX = player.getLocation().getRegionX();
		int regionY = player.getLocation().getRegionY();
		MessageBuilder spb = new MessageBuilder().setId(80).setSize(Packet.Size.VariableShort);

		if (first) {
			sendPlayersOnLogin(spb);
		}

		spb.addLEShortA(regionY + 6);
		spb.addShortA(player.getLocation().getRegionX() + 6);
		spb.addByte(0);
		spb.addByteA(1); // reload

		for (int xCalc = (regionX) / 8; xCalc <= ((regionX + 12) / 8); xCalc++) {
			for (int yCalc = (regionY) / 8; yCalc <= ((regionY + 12) / 8); yCalc++) {
				int region = yCalc + (xCalc << 8); // 1786653352
				int[] mapData = MapXTEA.getMapData().get((short) region);

				if (mapData == null) {
					mapData = new int[4];

					for (int i = 0; i < 4; i++) {
						mapData[i] = 0;
					}
				}

				spb.addInt(mapData[0]);
				spb.addInt(mapData[1]);
				spb.addInt(mapData[2]);
				spb.addInt(mapData[3]);
			}
		}

		player.setKnownRegion(regionX, regionY);
		player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method sendCloseWalkingFlag Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendCloseWalkingFlag() {
		player.getIoSession().write(new MessageBuilder().setId(122).toPacket());
	}

	/**
	 * Method closeAllWindows Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void closeAllWindows() {
		if (player.isTestPilot()) {
			return;
		}

		// player.getIoSession().write(new
		// MessageBuilder().setId(219).toPacket());
	}

	/**
	 * Method flushGroundItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void flushGroundItems() {

		// player.getIoSession().write(new
		// MessageBuilder().setId(125).toPacket());
	}

	/**
	 * Method updateGroundItem Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param x
	 * @param y
	 * @param oldamount
	 * @param newAmount
	 */
	public void updateGroundItem(int id, int x, int y, int oldamount, int newAmount) {
		if (player.isTestPilot()) {
			return;
		}

		MessageBuilder itemPacketBuilder = new MessageBuilder().setId(85).addByteC((y - 8 * player.getKnownRegion()[1]))
				.addByteC((x - 8 * player.getKnownRegion()[0]));
		MessageBuilder packetBuilder2 = new MessageBuilder().setId(84).addByte((byte) 0).addShort(id)
				.addShort(oldamount).addShort(newAmount);

		// player.getIoSession().write(itemPacketBuilder.toPacket());
		// player.getIoSession().write(packetBuilder2.toPacket());
	}

	/**
	 * Method sendCoords Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param location
	 */
	public void sendCoords(Point location) {
		MessageBuilder builder = new MessageBuilder().setId(114);
		int calcX = location.getX() >> 3;
		int calcY = location.getY() >> 3;
		int x = calcX - player.getKnownRegion()[0];
		int y = calcY - player.getKnownRegion()[1];

		player.getIoSession().write(builder.addByteA(x).addByteC(location.getHeight()).addByteA(y).toPacket());
	}

	/**
	 * Method removeGroundItem Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param x
	 * @param y
	 */
	public void removeGroundItem(int id, int x, int y) {
		if (player.isTestPilot()) {
			return;
		}

		sendCoords(Point.location(x, y, player.getHeight()));

		int deltaX = x - ((x >> 3) << 3);
		int deltaY = y - ((y >> 3) << 3);
		MessageBuilder builder = new MessageBuilder().setId(59);

		builder.addByteS((deltaX & 0x7) << 4 | deltaY & 0x7);
		builder.addShortA(id);
		player.getIoSession().write(builder.toPacket());
	}

	/**
	 * Method sendGroundItem Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param x
	 * @param y
	 * @param amount
	 */
	public void sendGroundItem(int id, int x, int y, int amount) {
		if (player.isTestPilot()) {
			return;
		}

		sendCoords(Point.location(x, y, player.getHeight()));

		int deltaX = x - ((x >> 3) << 3);
		int deltaY = y - ((y >> 3) << 3);
		MessageBuilder builder = new MessageBuilder().setId(29);

		builder.addLEShortA(id);
		builder.addByteS((deltaX & 0x7) << 4 | deltaY & 0x7);
		builder.addLEShortA(amount);
		player.getIoSession().write(builder.toPacket());
	}

	/**
	 * Method sendGroundItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendGroundItems() {
		for (FloorItem i : player.getWatchedItems().getRemovingEntities()) {
			removeGroundItem(i.getItemId(), i.getX(), i.getY());
		}

		for (FloorItem i : player.getWatchedItems().getKnownEntities()) {
			if (!player.getWatchedItems().isRemoving(i) && (player.getKnownFloorItemStackCount(i) != i.getAmount())) {
				updateGroundItem(i.getItemId(), i.getX(), i.getY(), i.getOldAmount(), i.getAmount());
			}
		}

		for (FloorItem i : player.getWatchedItems().getNewEntities()) {
			sendGroundItem(i.getItemId(), i.getX(), i.getY(), i.getAmount());
		}
	}

	/**
	 * Method sendObjects Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendObjects() {
		for (GameObject e : player.getWatchedObjects().getRemovingEntities()) {
			deleteWorldObject(e);
		}

		for (GameObject e : player.getWatchedObjects().getKnownEntities()) {
			if (!player.getWatchedObjects().isRemoving(e) && (e.offset() > player.getObjectId(e))
					&& (e.getHeight() == player.getHeight())) {
				sendObject(e);
				player.setKnownObjectId(e, e.offset());
			} else {
			}
		}

		for (GameObject e : player.getWatchedObjects().getNewEntities()) {
			if (e.getId() != 0) {
				sendObject(e);
			} else {
				if (e.getType() == 0) {
					deleteDoor(e.getX(), e.getY(), e.getRotation(), 0);
				} else {
					deleteWorldObject(e);
				}
			}

			player.setKnownObjectId(e, e.offset());
		}
	}

	/**
	 * Method updateEnergy Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateEnergy() {
		player.getIoSession()
				.write(new MessageBuilder().setId(13).addByte((byte) (int) player.getCurrentEnergy()).toPacket());
	}

	/**
	 * Method sendPlayerCount Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param amount
	 */
	public void sendPlayerCount(int amount) {

		// player.getIoSession().write(new
		// MessageBuilder().setId(110).addShort(amount).toPacket());
	}

	/**
	 * Method putItemOnGrid Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param amount
	 * @param slot
	 * @param interfaced
	 */
	public void putItemOnGrid(int id, int amount, int slot, int interfaced) {
		if (player.isTestPilot()) {
			return;
		}

		putItemOnGrid(Item.forId(id), amount, slot, interfaced);
	}

	/**
	 * Method sendBConfig Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param value
	 */
	public void sendBConfig(int id, int value) {
		MessageBuilder bldr = new MessageBuilder();

		if ((value < Byte.MIN_VALUE) || (value > Byte.MAX_VALUE)) {
			bldr = new MessageBuilder().setId(5).addLEShortA(id).addLEInt(value);
		} else {
			bldr = new MessageBuilder().setId(51).addLEShort(id).addByte((byte) value);
		}

		player.getIoSession().write(bldr.toPacket());
	}

	/**
	 * Method sendItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param type
	 * @param items
	 * @param amounts
	 * @param split
	 */
	public void sendItems(int type, int[] items, int[] amounts, boolean split) {
		MessageBuilder bldr = new MessageBuilder().setId(113).setSize(Packet.Size.VariableShort);

		bldr.addShort(type);
		bldr.addByte((split ? 1 : 0));
		bldr.addShort(items.length);

		for (int i = 0; i < items.length; i++) {
			int item = items[i];
			int id, amt;

			if (item == -1) {
				id = -1;
				amt = 0;
			} else {
				id = item;
				amt = amounts[i];
			}

			bldr.addShortA(id + 1);

			if (amt > 254) {
				bldr.addByteC((byte) 255);
				bldr.addInt1(amt);
			} else {
				bldr.addByteC((byte) amt);
			}
		}

		player.getIoSession().write(bldr.toPacket());
	}

	/**
	 * Method sendItems Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param type
	 * @param items
	 * @param amounts
	 * @param split
	 */
	public void sendItems(int type, Item[] items, int[] amounts, boolean split) {
		MessageBuilder bldr = new MessageBuilder().setId(113).setSize(Packet.Size.VariableShort);

		bldr.addShort(type);
		bldr.addByte((split ? 1 : 0));
		bldr.addShort(items.length);

		for (int i = 0; i < items.length; i++) {
			Item item = items[i];
			int id, amt;

			if (item == null) {
				id = -1;
				amt = 0;
			} else {
				id = items[i].getIndex();
				amt = amounts[i];
			}

			bldr.addShortA(id + 1);

			if (amt > 254) {
				bldr.addByteC((byte) 255);
				bldr.addInt1(amt);
			} else {
				bldr.addByteC((byte) amt);
			}
		}

		player.getIoSession().write(bldr.toPacket());
	}

	/**
	 * Method setItem Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param amount
	 * @param slot
	 * @param interfaceId
	 */
	public void setItem(Item id, int amount, int slot, int interfaceId) {
		setItem(id, amount, slot, interfaceId, false);
	}

	/**
	 * Method setItem Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param amount
	 * @param slot
	 * @param interfaceID
	 * @param split
	 */
	public void setItem(Item id, int amount, int slot, int interfaceID, boolean split) {
		MessageBuilder spb = new MessageBuilder().setId(57).setSize(Packet.Size.VariableShort);

		spb.addShort(interfaceID);
		spb.addByte(split ? 1 : 0);
		spb.addSmart(slot);
		spb.addShort((id == null) ? 0 : id.getIndex() + 1);

		if ((amount > 254) && (id != null)) {
			spb.addByte(255);
			spb.addInt(amount);
		} else {
			if (id != null) {
				spb.addByte(amount);
			}
		}

		player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method putItemOnGrid Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param amount
	 * @param slot
	 * @param interfaced
	 */
	public void putItemOnGrid(Item id, int amount, int slot, int interfaced) {
		if (player.isTestPilot()) {
			return;
		}

		MessageBuilder p = new MessageBuilder().setId(34).setSize(Packet.Size.VariableShort);

		p.addShort(interfaced);
		p.addByte((byte) slot);
		p.addShort((id == null) ? 0 : id.getIndex() + 1);

		if (amount > 254) {
			p.addByte((byte) 255).addInt(amount);
		} else {
			p.addByte((byte) amount);
		}

		// player.getIoSession().write(p.toPacket());
	}

	/**
	 * Method clearInterface Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void clearInterface(int id) {
		MessageBuilder spb = new MessageBuilder().setId(72).addWordBigEndian(id);

		// player.getIoSession().write(spb.toPacket());
	}

	/**
	 * Method deleteObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param rotation
	 * @param type
	 * @param height
	 */
	public void deleteObject(int x, int y, int rotation, int type, int height) {
		sendCoords(Point.location(x, y, height));

		int localX = x - (player.getKnownRegion()[0] - 6) * 8;
		int localY = y - (player.getKnownRegion()[1] - 6) * 8;
		MessageBuilder builder = new MessageBuilder(19, Packet.Size.Fixed);

		builder.addByteC((type << 2) + (rotation & 3));
		builder.addByteS(((localX - ((localX >> 3) << 3)) << 4) | ((localY - ((localY >> 3) << 3)) & 0x7));
		player.dispatch(builder);
	}

	/**
	 * Method deleteWorldObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param object
	 */
	public void deleteWorldObject(GameObject object) {
		deleteObject(object.getX(), object.getY(), object.getRotation(), object.getType(), object.getHeight());
	}

	/**
	 * Method deleteWorldObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param rot
	 */
	public void deleteWorldObject(int x, int y, int rot) {
		deleteObject(x, y, 10, rot, player.getHeight());
	}

	/**
	 * Method deleteWorldObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param rot
	 * @param type
	 */
	public void deleteWorldObject(int x, int y, int rot, int type) {
		deleteObject(x, y, rot, type, player.getHeight());
	}

	/**
	 * Method deleteWorldObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param rot
	 * @param type
	 * @param plane
	 */
	public void deleteWorldObject(int x, int y, int rot, int type, int plane) {
		deleteObject(x, y, rot, type, plane);
	}

	/**
	 * Method deleteDoor Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param rot
	 * @param h
	 */
	public void deleteDoor(int x, int y, int rot, int h) {
		deleteObject(x, y, rot, 0, h);
	}

	/**
	 * Method sendDoor Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param x
	 * @param y
	 * @param rot
	 */
	public void sendDoor(int id, int x, int y, int rot) {
		sendPlainObject(id, x, y, 0, rot);
	}

	/**
	 * Method sendHintIcon Created on 14/08/23
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param icon
	 */
	public void sendHintIcon(HintIcon icon) {
		MessageBuilder bldr = new MessageBuilder().setId(0).setSize(Packet.Size.Fixed);

		if (icon == null) {
			bldr.addByte(0);

			for (int i = 0; i < 11; i++) {
				bldr.addByte(0);
			}

			player.dispatch(bldr);

			return;
		} else {
			bldr.addByte(icon.getTargetType());
			bldr.addByte(icon.getSprite());
			if ((icon.getTargetType() == 1) || (icon.getTargetType() == 10)) {
				bldr.addShort(icon.getFocusEntity().getIndex());
				bldr.addShort(5000);

				for (int i = 0; i < 4; i++) {
					bldr.addByte(0);
				}
			} else {
				bldr.addByte(icon.getLocation().getHeight());
				bldr.addShort(icon.getLocation().getX());
				bldr.addShort(icon.getLocation().getY());
				bldr.addByte(100);
				bldr.addShort(6000);
			}
		}

		bldr.addShort(65535);
		player.dispatch(bldr);
	}

	/**
	 * Method createArrow Created on 14/08/23
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param x
	 * @param y
	 * @param height
	 * @param pos
	 */
	public void createArrow(int x, int y, int height, int pos) {
		MessageBuilder spb = new MessageBuilder().setId(0).setSize(Packet.Size.Fixed);

		spb.addByte(0 << 5 | 2);
		spb.addByte(HINT_ICON[0]);
		spb.addByte(player.getHeight());
		spb.addShort(x).addShort(y).addByte(0);
		spb.addShort(HINT_ICON[1]).addShort(HINT_ICON[2]);
		player.dispatch(spb);
	}

	/*
	 *
	 *
	 * player.getHintIconsManager().addHintIcon(taggedDispenser.getX(),
	 * taggedDispenser.getY(), taggedDispenser.getPlane(), 65, 2, 0, -1, false);
	 *
	 *
	 *
	 * dirs 2 - center 3 - west 4 - east 5 - south 6 - north
	 */

	/*
	 * public static void sendHintIcon(Player p, Icon icon) { MessageBuilder
	 * bldr = new MessageBuilder(0); bldr.writeByte(icon.getSlot() << 5 |
	 * icon.getTargetType()); bldr.writeByte(icon.getArrowId()); if
	 * (icon.getIndex() > -1) { bldr.writeShort(icon.getIndex());
	 * bldr.writeShort(icon.getDistance()); bldr.skip(4); } else if
	 * (icon.getLocation() != null) { bldr.writeByte(1); // TODO: Identify.
	 * bldr.writeShort(icon.getLocation().getX());
	 * bldr.writeShort(icon.getLocation().getY());
	 * bldr.writeByte(icon.getLocation().getZ());
	 * bldr.writeShort(icon.getDistance()); } else { bldr.skip(11); }
	 * bldr.writeShort(icon.getModelId()); p.write(bldr.toMessage()); }
	 */

	/**
	 * Method updateDoors Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateDoors() {

		for (Door d : player.getDoorsInView()) {
			if ((d.getCurrentOffset() > player.getKnownDoorId(d)) && (d.getHeight() == player.getHeight())) {
				d.updateForPlayer(player);
				player.setKnownDoorId(d, d.getCurrentOffset());
			}
		}
	}

	/**
	 * Method sendTradeReq Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param user
	 * @param message
	 */
	public void sendTradeReq(String user, String message) {
		MessageBuilder bldr = new MessageBuilder(53, Packet.Size.VariableByte);

		bldr.addByte(100);
		bldr.addInt(0);
		bldr.addByte(0x1);
		bldr.addRS2String(user);
		bldr.addRS2String(message);
		player.dispatch(bldr);
	}

	/**
	 * Method sendDuelReq Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param user
	 * @param message
	 */
	public void sendDuelReq(String user, String message) {
		MessageBuilder bldr = new MessageBuilder(53, Packet.Size.VariableByte);

		bldr.addByte(101);
		bldr.addInt(0);
		bldr.addByte(0x1);
		bldr.addRS2String(user);
		bldr.addRS2String(message);
		player.dispatch(bldr);
	}

	/**
	 * Method sendConstructMapRegion Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param palette
	 *
	 * @return
	 */
	public ActionSender sendConstructMapRegion(Palette palette) {
		MessageBuilder bldr = new MessageBuilder().setId(31).setSize(Packet.Size.VariableShort); // 241,
																									// Type.VARIABLE_SHORT);
		Point newLocation = Point.location(1907, 5082, 0);

		player.setLocation(newLocation);
		player.is_con = false;
		sendMapArea(false);
		bldr.addByteA(2);
		bldr.addByteA(0);
		bldr.addShortA(newLocation.getRegionY() + 6);
		bldr.addLEShort(newLocation.getRegionX() + 6);
		bldr.addByteA(1);
		bldr.startBitAccess();

		for (int z = 0; z < 4; z++) {
			for (int x = 0; x < 13; x++) {
				for (int y = 0; y < 13; y++) {
					Palette.PaletteTile tile = palette.getTile(x, y, z);

					bldr.addBits((tile != null) ? 1 : 0, 1);

					/*
					 * this.locationHash = x / 8 << 14 | y / 8 << 3 | z % 4 <<
					 * 24 | rot % 4 << 1;
					 */
					if (tile != null) {
						bldr.addBits(tile.getX() << 14 | tile.getY() << 3 | tile.getZ() << 24 | tile.getRotation() << 1,
								26);
					}
				}
			}
		}

		bldr.finishBitAccess();
		player.setKnownRegion(newLocation.getRegionX(), newLocation.getRegionY());
		player.dispatch(bldr);

		return this;
	}

	/**
	 * Method setPlane Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param p
	 */
	public void setPlane(int p) {

		// player.getIoSession().write(new
		// MessageBuilder().setId(40).addByte((byte) p).toPacket());
	}

	/**
	 * Method sendObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param object
	 */
	public void sendObject(GameObject object) {
		if (object.getId() == 0) {
			return;
		}

		sendCoords(object.getLocation());

		int viewportX = object.getX() - (player.getKnownRegion()[0]) * 8;
		int viewportY = object.getY() - (player.getKnownRegion()[1]) * 8;
		MessageBuilder builder = new MessageBuilder(78, Packet.Size.Fixed);

		builder.addByteC(((viewportX - ((viewportX >> 3) << 3)) << 4) | ((viewportY - ((viewportY >> 3) << 3)) & 0x7));
		builder.addLEShort(object.getId());
		builder.addByte((object.getType() << 2) + (object.getRotation() & 3));
		player.dispatch(builder);
	}

	/**
	 * Method sendAnimateObject Created on 14/08/23
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param object
	 * @param animationID
	 */
	public void sendAnimateObject(GameObject object, int animationID) {
		sendCoords(object.getLocation());

		int viewportX = object.getX() - (player.getKnownRegion()[0]) * 8;
		int viewportY = object.getY() - (player.getKnownRegion()[1]) * 8;
		MessageBuilder builder = new MessageBuilder(81, Packet.Size.Fixed);

		builder.addByteS(((viewportX - ((viewportX >> 3) << 3)) << 4) | ((viewportY - ((viewportY >> 3) << 3)) & 0x7));
		builder.addLEShortA(animationID);
		builder.addByteA((object.getType() << 2) + (object.getRotation() & 3));
		player.dispatch(builder);
	}

	/**
	 * Method sendPositionedGraphic Created on 14/10/30
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loc
	 * @param gfxId
	 * @param var1
	 * @param var2
	 * @param var3
	 */
	public void sendPositionedGraphic(Point loc, int gfxId, int var1, int var2, int var3) {
		sendCoords(loc);

		int viewportX = loc.getX() - (player.getKnownRegion()[0]) * 8;
		int viewportY = loc.getY() - (player.getKnownRegion()[1]) * 8;
		MessageBuilder builder = new MessageBuilder(81, Packet.Size.Fixed);

		builder.addByte(((viewportX - ((viewportX >> 3) << 3)) << 4) | ((viewportY - ((viewportY >> 3) << 3)) & 0x7));
		builder.addShort(gfxId);
		builder.addByte(var1);
		builder.addShort(var2);
		builder.addByte(var3);
		player.dispatch(builder);
	}

	/**
	 * Method sendPlainObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param x
	 * @param y
	 * @param type
	 * @param rotation
	 */
	public void sendPlainObject(int id, int x, int y, int type, int rotation) {
		sendPlainObject(id, x, y, type, rotation, player.getHeight());
	}

	/**
	 * Method sendPlainObject Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param x
	 * @param y
	 * @param type
	 * @param rotation
	 * @param h
	 */
	public void sendPlainObject(int id, int x, int y, int type, int rotation, int h) {
		sendCoords(Point.location(x, y, h));

		int viewportX = x - (player.getKnownRegion()[0]) * 8;
		int viewportY = y - (player.getKnownRegion()[1]) * 8;
		MessageBuilder builder = new MessageBuilder(78, Packet.Size.Fixed);

		builder.addByteC(((viewportX - ((viewportX >> 3) << 3)) << 4) | ((viewportY - ((viewportY >> 3) << 3)) & 0x7));
		builder.addLEShort(id);
		builder.addByte((type << 2) + (rotation & 3));
		player.dispatch(builder);
	}

	/**
	 * Method sendDialog Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void sendDialog(int id) {

		// player.getIoSession().write(new
		// MessageBuilder().setId(164).addLEShort(id).toPacket());
	}

	/**
	 * Method showInterface Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void showInterface(int id) {
		if (player.isTestPilot()) {
			return;
		}

		// player.getIoSession().write(new
		// MessageBuilder().setId(97).addShort(id).toPacket());
	}

	/**
	 * Method sendLoginResponse Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loginResponse
	 * @param playerGroup
	 * @param flag
	 */
	public void sendLoginResponse(int loginResponse, int playerGroup, int flag) {
		if (player.isTestPilot()) {
			return;
		}

		loginSent = true;
		player.getIoSession().write(new MessageBuilder().setBare(true).addByte((byte) loginResponse).toPacket());

		MessageBuilder spb = new MessageBuilder().setBare(true);

		/*
		 * spb.addByte((byte) player.getRights() == Player.MODERATOR ? 1 :
		 * (((player.getRights() == Player.ADMINISTRATOR) || (player.getRights()
		 * == Player.ROOT_ADMIN) || (player.getRights() ==
		 * Player.CONTENT_MANAGER)) ? 2 : 0));
		 */
		spb.addByte(13);
		spb.addByte((byte) player.getRights() >= Player.ADMINISTRATOR ? 2 : 0);
		spb.addByte((byte) 0);
		spb.addByte((byte) 0);
		spb.addByte((byte) 0);
		spb.addByte((byte) 1);
		spb.addByte((byte) 0);
		spb.addShort(player.getIndex());
		spb.addByte((byte) 1);
		spb.addMediumInt(0);
		spb.addByte((byte) 1); // members
		player.getIoSession().write(spb.toPacket());

		boolean isCon = player.is_con;

		player.is_con = false;
		sendMapArea(true);
		player.is_con = isCon;
		player.setMapAreaChanged(false);
		player.setKnownRegion(player.getLocation().getRegionX(), player.getLocation().getRegionY());
	}

	/**
	 * Method sendMapArea Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendMapArea() {
		sendMapArea(false);
	}

	/**
	 * Method sendMessage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param s
	 */
	public void sendMessage(String s) {
		if (player.isTestPilot()) {
			return;
		}

		sendChatMessage(0, s);
	}

	/**
	 * Method sendIgnore Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param name
	 */
	public void sendIgnore(String name) {
		MessageBuilder bldr = new MessageBuilder(85, Packet.Size.VariableByte);

		bldr.addByte(0);
		bldr.addRS2String(name);
		bldr.addRS2String(name);
		bldr.addRS2String(name);
		bldr.addRS2String(name);
		player.dispatch(bldr);
	}

	/**
	 * Method sendVar Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param value
	 */
	public void sendVar(int id, int value) {
		MessageBuilder bldr;

		if ((value < 0) || (value >= 128)) {
			bldr = new MessageBuilder().setId(27).addInt1(value).addShort(id);
		} else {
			bldr = new MessageBuilder().setId(21).addShortA(id).addByteS(value);
		}

		player.getIoSession().write(bldr.toPacket());
	}

	/**
	 * Method sendVarBit Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 * @param value
	 */
	public void sendVarBit(int id, int value) {
		MessageBuilder bldr;

		if ((value < 0) || (value >= 128)) {
			bldr = new MessageBuilder().setId(112).addLEShortA(id).addInt(value);
		} else {
			bldr = new MessageBuilder().setId(38).addByteC(value).addShortA(id);
		}

		player.getIoSession().write(bldr.toPacket());
	}

	/**
	 * Method sendFriend Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param Username
	 * @param world
	 * @param writeOnline
	 * @param isIdle
	 */
	public void sendFriend(String Username, int world, boolean writeOnline, boolean isIdle) {
		if (Username.length() > 16) {
			return;
		}

		ClanChannel channel = World.getWorld().getChannelList().getByPlayer(player.getUsername());
		short WorldId = 1;
		MessageBuilder bldr = new MessageBuilder(10, Packet.Size.VariableShort);

		bldr.addByte(0);
		bldr.addRS2String(Username);
		bldr.addRS2String("");
		bldr.addShort(writeOnline ? ((world == WorldId) ? 1 : 2) : 0);
		bldr.addByte((channel != null) ? channel.getRankForPlayer(Text.longForName(Username)) : 0); // clan
																									// rank

		if (writeOnline) {
			bldr.addRS2String(isIdle ? "<col=00FF00>Lobby" : "<col=00FF00>Online");
			bldr.addByte(0);
		}

		player.dispatch(bldr);
	}

	/**
	 * Method sendUnlockFriendList Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendUnlockFriendList() {
		MessageBuilder bldr = new MessageBuilder(10, Packet.Size.VariableShort);

		player.dispatch(bldr);
	}

	/**
	 * Method sendStat Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param stat
	 */
	public void sendStat(int stat) {
		if (player.isTestPilot()) {
			return;
		}

		int currentStat = player.getCurStat(stat);

		if (stat == 3) {
			currentStat = (int) player.getCurrentHealth();
			sendVar(1240, (int) (player.getCurrentHealth() * 10) * 2);
			sendVar(102, player.isPoisoned() ? 1 : 0);
		} else if (stat == 5) {
			currentStat = player.getPrayerBook().getPoints();
		} else if (stat == 23) {
			currentStat = player.getCurrSummonPts();
		}

		MessageBuilder skills = new MessageBuilder().setId(8);

		skills.addInt1(player.getExps()[stat]);
		skills.addByteS(stat);
		skills.addByteS(currentStat);
		player.getIoSession().write(skills.toPacket());
	}

	/**
	 * Method clientCommand Created on 14/10/30
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param command
	 */
	public void clientCommand(String command) {
		player.dispatch(new MessageBuilder(125, Packet.Size.VariableShort).addRS2String(command));
	}

	/**
	 * Method sendSideBarInterface Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param menuId
	 * @param form
	 */
	public void sendSideBarInterface(int menuId, int form) {
		if (player.isTestPilot()) {
			return;
		}

		// player.getIoSession().write(
		// new MessageBuilder().setId(71).addShort(form).addByteA((byte)
		// menuId).toPacket());
	}

	/**
	 * Method sendLogout Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void sendLogout() {
		player.getIoSession().write(new MessageBuilder().setId(23).toPacket());
	}

	/**
	 * Method sendPlayerCommand Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param option
	 * @param top
	 * @param slot
	 */
	public void sendPlayerCommand(String option, boolean top, int slot) {
		MessageBuilder bldr = new MessageBuilder(120, Packet.Size.VariableByte);

		bldr.addShortA(((slot == 1) && top) ? 42 : 65535); // sprite id for the

		// option
		bldr.addByteS(top ? 1 : 0);
		bldr.addRS2String(option);
		bldr.addByteS(slot);
		player.dispatch(bldr);
	}

	/**
	 * Method changeLine Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param s
	 * @param interfaceid
	 */
	public void changeLine(String s, int interfaceid) {

		// player.getIoSession().write(
		// new
		// MessageBuilder().setId(126).setSize(Packet.Size.VariableShort).addString(s).addShortA(
		// interfaceid).toPacket());
	}

	/**
	 * Method sendImage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param MainFrame
	 * @param SubFrame
	 * @param SubFrame2
	 */
	public void sendImage(int MainFrame, int SubFrame, int SubFrame2) {

		// MessageBuilder builder = new MessageBuilder();
		// builder.setId(246).addWordBigEndian(MainFrame).addShort(SubFrame).addShort(SubFrame2);
		// player.getIoSession().write(builder.toPacket());
	}
}
