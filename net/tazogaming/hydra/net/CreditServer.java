package net.tazogaming.hydra.net;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

import java.sql.ResultSet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 05/12/13
 * Time: 22:26
 */
public class CreditServer implements Runnable {
    private static final int
        USER_NOT_ONLINE                      = 0,
        USER_BANK_FULL                       = 1,
        QUERY_ERROR                          = 2,
        OKAY                                 = 3,
        TOKEN_ERROR                          = 4;
    ServerSocket            payment_listener = null;    // = new ServerSocket(4994);
    private boolean         shutdown         = false;
    private MysqlConnection connection;

    /**
     * Constructs ...
     *
     */
    public CreditServer() {
        connection = World.getWorld().createDatabaseLink();
        ((new Thread(this))).start();
    }

    private String fixNumbers(String str) {
        StringBuilder br = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            Character c = str.charAt(i);

            if ((c >= '0') && (c <= '9')) {
                br.append(c);
            }
        }

        return br.toString();
    }

    private int addProduct(int userId, String script) {
        boolean canAdd = false;
        Player  player = null;

        try {
            for (Player p : World.getWorld().getPlayers()) {
                if (p.getId() == userId) {
                    player = p;

                    break;
                }
            }

            if (player == null) {
                return USER_NOT_ONLINE;
            }

            player.getActionSender().sendMessage(Text.DARK_RED("[DONATIONS]<col=0> Your donation has been received and your balance has been deducted."));
            player.getActionSender().sendMessage(Text.DARK_RED("[DONATIONS]<col=0> If your donation was relating to items, check your bank."));


            for (String str : script.split("\n")) {
                while (str.startsWith(" "))
                    str = str.substring(1);
                if(str.length() == 0)
                    continue;;

                ArgumentTokenizer parser = new ArgumentTokenizer(str.split(" "));

                if(!parser.hasNext()) continue;;

                String            next   = parser.nextString();

                if (next.equalsIgnoreCase("additem")) {
                    player.getBank().insert(parser.nextInt(), Integer.parseInt(fixNumbers(parser.nextString())));
                } else if (next.equalsIgnoreCase("message")) {
                    player.getActionSender().sendMessage(str.substring("message".length() + 1));
                } else if (next.equalsIgnoreCase("setvar")) {
                    String varName = parser.nextString();
                    int    value   = Integer.parseInt(fixNumbers(parser.nextString()));

                    player.getAccount().setSetting(varName, value, true);
                } else if (next.equalsIgnoreCase("increasevar")) {
                    String var = parser.nextString();

                    if (!player.getAccount().hasVar(str)) {
                        player.getAccount().setSetting(var, Integer.parseInt(fixNumbers(parser.nextString())), true);
                    } else {
                        player.getAccount().increaseVar(var, Integer.parseInt(fixNumbers(parser.nextString())));
                    }
                } else if (next.equalsIgnoreCase("settimer")) {
                    int timerID = Integer.parseInt(fixNumbers(parser.nextString()));
                    int ticks   = Integer.parseInt(fixNumbers(parser.nextString()));
                    int cur     = player.getTimers().getAbsTime(timerID);

                    player.getTimers().setTime(timerID, cur + ticks, true);
                }
            }
        } catch (Exception ee) {
            ee.printStackTrace();

            return QUERY_ERROR;
        }

        return OKAY;
    }

    private int discount(int usergroupid, int amt) {

        return amt;
    }

    private void newClient(Socket sock) throws IOException {
        InputStream    in     = sock.getInputStream();
        PrintWriter    writer = new PrintWriter(new OutputStreamWriter(sock.getOutputStream()));
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        sock.setSoTimeout(2000);

        do {
            try {

                String    i        = reader.readLine();
                int       order_id = Integer.parseInt(i.substring(4));
                ResultSet result   =
                    connection.query(
                            "SELECT shop_items_new.price, shop_items_new.script, user.tokens as user_tokens,  user.id as member_id, shop_orders.status, shop_orders.userid from shop_orders LEFT JOIN shop_items_new ON shop_items_new.id=shop_orders.productid INNER JOIN user on user.id=shop_orders.userid WHERE orderid='" + order_id + "'");

                if (result.next()) {
                    if (result.getInt("status") == 2) {
                        writer.write("already_bought");
                        writer.flush();

                        throw new RuntimeException();
                    }

                    int result_id   = addProduct(result.getInt("userid"), result.getString("script"));
                    int tokenAmt    = result.getInt("price");
                    int prevBalance = result.getInt("user_tokens") - tokenAmt;

                    switch (result_id) {
                    case OKAY :
                        connection.query("UPDATE user set tokens=tokens-"
                                + discount(2, tokenAmt) + " where id='"
                                + result.getInt("member_id") + "'");
                        connection.query("UPDATE shop_orders set status=2, newbalance='" + prevBalance
                                + "' where orderid='" + order_id + "'");
                        writer.write("success");
                        writer.flush();

                        throw new RuntimeException();

                    case QUERY_ERROR :
                        writer.write("query_error");
                        writer.flush();

                        throw new RuntimeException();

                    case USER_NOT_ONLINE :
                        writer.write("not_online");
                        connection.query("UPDATE shop_orders set status=0 where orderid='" + order_id + "'");
                        writer.flush();

                        throw new RuntimeException();
                    }
                    result.close();
                } else {
                    writer.write("error");
                    writer.flush();
                    writer.close();
                    sock.close();
                }
            } catch (RuntimeException ee) {
                sock.close();
                writer.close();
                return;
            } catch (Exception ee) {
                ee.printStackTrace();
                writer.close();
                sock.close();

                return;
            }
        } while (true);
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        Thread.currentThread().setPriority(Thread.NORM_PRIORITY);

        try {
            payment_listener = new ServerSocket(Integer.parseInt(Config.getMainInfo().getProperty("payment_api_port")));
            payment_listener.setSoTimeout(100);

            while (!shutdown) {
                Socket paymentSocket = null;

                try {
                    paymentSocket = payment_listener.accept();
                } catch (SocketTimeoutException ee) {
                    try {
                        Thread.sleep(3500);

                        continue;
                    } catch (InterruptedException ignored) {}
                }

                try {
                    newClient(paymentSocket);
                    paymentSocket = null;
                } catch (Exception error) {
                    try {
                        paymentSocket.close();
                    }catch (Exception ee){}

                    error.printStackTrace();
                    paymentSocket = null;
                }
            }
        } catch (Exception ee) {
            ee.printStackTrace();
            Logger.err("Unable to start payment listener");
            System.exit(-1);
        }
    }
}
