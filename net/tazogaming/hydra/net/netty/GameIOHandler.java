package net.tazogaming.hydra.net.netty;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.runtime.Core;

import org.jboss.netty.channel.*;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/09/14
 * Time: 21:06
 */
public class GameIOHandler extends SimpleChannelHandler {

    /** session made: 14/09/07 */
    private PlaySession session;

    /**
     * Method channelConnected
     * Created on 14/09/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ctx
     * @param e
     *
     * @throws Exception
     */
    @Override
    public void channelConnected(ChannelHandlerContext ctx, ChannelStateEvent e) throws Exception {
      try {

      }catch (Exception ee) {}
    }

    /**
     * Method channelDisconnected
     * Created on 14/09/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ctx
     * @param e
     */
    @Override
    public void channelDisconnected(ChannelHandlerContext ctx, ChannelStateEvent e) {
        session = (PlaySession) ctx.getAttachment();


        if ((session != null) && (session.getPlayer() != null)) {
          if(e != null)
            session.getPlayer().setDisconTime(System.currentTimeMillis());
            session.getPlayer().onDisconnect();
        }
    }

    /**
     * Method exceptionCaught
     * Created on 14/09/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ctx
     * @param e
     *
     * @throws Exception
     */
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, ExceptionEvent e) throws Exception {
        session = (PlaySession) ctx.getAttachment();
        if(session != null && session.getPlayer() != null)
        {

        }

    }

    /**
     * Method messageReceived
     * Created on 14/09/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ctx
     * @param e
     */
    @Override
    public void messageReceived(ChannelHandlerContext ctx, MessageEvent e) {
        if (e.getMessage() instanceof Packet) {
            Packet packet = (Packet) e.getMessage();

            session = (PlaySession) ctx.getAttachment();

            if (session == null) {
                return;
            }

            session.addPacket(packet.getId());
            if (packet.getId() == 85) {
                session.getPlayer().getActionSender().clientCommand("pong jjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj");

                return;
            }
            Player pla = (Player) session.getPlayer();

            pla.setLastPacket(Core.currentTimeMillis());

            for (int i = 0; i < SILENT_PACKETS.length; i++) {
                if (packet.getId() == SILENT_PACKETS[i]) {
                    return;
                }
            }

            synchronized (pla.getPackets()) {
                if (!pla.hasPacket(packet)) {
                    pla.registerPacket(packet);
               }
            }
        }
    }
    public static final int SILENT_PACKETS[] = {
            29, 246, 12, 30, 26, 23, 62, 59, 75
    };
}
