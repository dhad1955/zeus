package net.tazogaming.hydra.net.netty.codec;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.net.mina.codec.RS2LoginProtocolDecoder;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.security.BanList;
import net.tazogaming.hydra.security.ConnectionThrottleFilter;
import net.tazogaming.hydra.security.XTEA;
import net.tazogaming.hydra.util.Logger;

import net.tazogaming.hydra.util.Text;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import java.net.SocketAddress;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/09/14
 * Time: 21:46
 */
public class NettyLoginDecoder extends FrameDecoder {


    public  static ConnectionThrottleFilter<String> floodThrottle = new ConnectionThrottleFilter<String>(5, 1000);

    /**
     * Constructs ...
     *
     */

    private LoginStage stage = LoginStage.HANDSHAKE;

    private void checkpoint(LoginStage stage) {
        this.stage = stage;
    }
    public NettyLoginDecoder() {
        checkpoint(LoginStage.HANDSHAKE);
    }

    private void sendBadLogin(final PlaySession sesh, int code) {
        sesh.write(new MessageBuilder().setBare(true).addByte((byte) code).toPacket()).addListener(
            new ChannelFutureListener() {
            @Override
            public void operationComplete(ChannelFuture channelFuture) throws Exception {
                sesh.getChannel().close();
            }
        });
        checkpoint(LoginStage.DISCONNECTED);
    }

    /**
     * Method readableBytes
     * Created on 14/09/08
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buffer
     *
     * @return
     */
    public int readableBytes(ChannelBuffer buffer) {
        return buffer.writerIndex() - buffer.readerIndex();
    }

    @Override
    protected Object decode(ChannelHandlerContext channelHandlerContext, Channel channel, ChannelBuffer in)
            throws Exception {


        LoginStage loginStage = this.stage;
        if (loginStage == LoginStage.DISCONNECTED) {
            channel.close();
            return null;
        }



        PlaySession session;
        if (channelHandlerContext.getAttachment() == null) {
            session = new PlaySession(channel);


            channelHandlerContext.setAttachment(session);
        } else {
            session = (PlaySession)channelHandlerContext.getAttachment();
        }
        SocketAddress remoteAddress = channel.getRemoteAddress();
        session.setUserIP(remoteAddress.toString().replace("/", "").replace("\\", ""));
        session.setUserIP(session.getUserIP().substring(0, session.getUserIP().indexOf(":")));
        int packetSize = session.getLoginPacketSize();
        switch (loginStage) {
        case HANDSHAKE :

            if(in.readableBytes() < 1)
            {
                in.resetReaderIndex();
                return null;
            }
            int protocol = in.readByte() & 0xFF;

            if (protocol != 14) {
                channel.close();
                sendBadLogin(session, RS2LoginProtocolDecoder.ERROR_LOADING_PROFILE);

                System.err.println("warning invalid login protocol");
                return null;
            }

            session.write(new MessageBuilder().setBare(true).addByte((byte) 0).toPacket());
            checkpoint(LoginStage.PRE_STAGE);

             return true;

        case PRE_STAGE :

            in.markReaderIndex();
            if (in.readableBytes() >= 7) {
                int loginType = in.readByte() & 0xFF;

                session.setLoginPacketSize(in.readShort() & 0xFFFF);

                int  version          = in.readInt();
                long serverSessionKey = ((long) (java.lang.Math.random() * 99999999D) << 32)
                                        + (long) (java.lang.Math.random() * 99999999D);
                MessageBuilder s1Response = new MessageBuilder();

                s1Response.setBare(true).addByte((byte) 0).addLong(serverSessionKey);
                session.setSessionKey(serverSessionKey);

                // session.write(s1Response.toPacket());
                if ((loginType == 18) || (loginType == 16)) {
                    checkpoint(LoginStage.LOGIN);
                } else {
                    checkpoint(LoginStage.LOBBY_REQUEST);
                }

                return true;
            } else {
                in.resetReaderIndex();


                checkpoint(LoginStage.PRE_STAGE);
                return null;
            }

        case LOBBY_REQUEST :
            int size = packetSize - 4;

            in.markReaderIndex();
            if (in.readableBytes() < size) {
                in.resetReaderIndex();

                return null;
            }

            in.skipBytes(3);

            int[] keys2 = new int[4];

            for (int i = 0; i < 4; i++) {
                keys2[i] = in.readInt();
            }

            in.skipBytes(8);

            byte[] buffer = new byte[size - (3 + 16 + 8)];
            Packet p2     = new Packet(buffer);

            in.readBytes(buffer);

            String lobbyPass = p2.readRS2String();

            p2.skip(16);

            byte[] block2 = p2.getRemainingData();

            XTEA.decrypt(keys2, block2, 0, block2.length);
            p2 = new Packet(block2);

            String lobbyUser = p2.readRS2String();
            Player player    = new Player(session);

            player.setCredentials(lobbyUser, lobbyPass, false);
            World.getWorld().getIOPool().getLoader().addLobbyLoad(player);
            channelHandlerContext.getPipeline().replace("decoder", "decoder", new RSProtocolDecoder());
             return true;

        case LOGIN :
            size = packetSize - 4;

            in.markReaderIndex();
            if (size > in.readableBytes()) {
                in.resetReaderIndex();

                return null;
            }

            byte[] payload = new byte[size];

            in.readBytes(payload);

            Packet                          p                      = new Packet(session, -1, payload);
            @SuppressWarnings("unused") int loginEncryptPacketSize = size - (36 + 1 + 1 + 2);    // can't be negative

            p.readShort();                                                                       // no idea

            int rsaHeader = p.readByte();

            session.setVersion(rsaHeader);

            int[] keys = new int[4];

            for (int i = 0; i < keys.length; i++) {
                keys[i] = p.readInt();
            }


            long UID = p.readLong();


            String password = p.readRS2String();

            long hax = p.readLong();

            if(hax != 388651241){
                System.err.println("invalid rsa header");
                channel.close();

                return true;

            }
            p.readLong();

            byte[] block = p.getRemainingData();

            XTEA.decrypt(keys, block, 0, block.length);
            p = new Packet(block);

            String username = p.readRS2String();

            p.readByte();

            int displayMode = p.readByte();
            int screenSizeX = p.readShort();
            int screenSizeY = p.readShort();

            p.readByte();
            p.skip(24);
            player = new Player(session);
            player.setUID(UID);
            player.setCredentials(Text.ensureUC(username), password, displayMode != 1);
            if (BanList.isBanned(UID, session.getUserIP())) {
                session.getChannel().close();
                System.err.println("Warning "+username+"@"+session.getUserIP()+" is blacklisted [uid: "+UID+"]");
                return true;
            }

            World.getWorld().getIOPool().getLoader().addLoad(player);
            channelHandlerContext.getPipeline().replace("decoder", "decoder", new RSProtocolDecoder());

            channelHandlerContext.getPipeline().getContext("handler").setAttachment(player.getIoSession());
            return true;
        }


        channel.close();
        return null;
    }
}
