package net.tazogaming.hydra.net.netty.codec;

import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.net.mina.codec.Cryption;
import net.tazogaming.hydra.net.mina.codec.RS2CodecFactory;
import net.tazogaming.hydra.util.WorldList;
import org.apache.mina.core.buffer.IoBuffer;
import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.frame.FrameDecoder;

import java.util.Arrays;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/09/14
 * Time: 21:37
 */
public class RSProtocolDecoder extends FrameDecoder {



    public RSProtocolDecoder() {
        RS2CodecFactory.PACKET_SIZES[127] = -1;
    }
    private byte[] data;

    public int readableBytes(ChannelBuffer buffer) {
        return buffer.writerIndex() - buffer.readerIndex();
    }
    @Override
    protected Object decode(ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer) throws Exception {
           try {
            buffer.markReaderIndex();
            if(channel.isReadable())  {
                if(buffer.readableBytes() >= 1){
                    int opcode = buffer.readUnsignedByte();

                    if (opcode == 84) {
                        ctx.getChannel().write(WorldList.getData(true, true));
                    }


                    int length = RS2CodecFactory.PACKET_SIZES[opcode];
                   if(opcode == 85)
                       length = 30;


                    if((length == -2 && buffer.readableBytes() < 2) || (length == -1 && buffer.readableBytes() < 1)){
                        buffer.resetReaderIndex();
                        return null;
                    }

                    if(length == -2)
                        length = buffer.readUnsignedShort();
                    else if(length == -1)
                        length = buffer.readUnsignedByte();


                    if(length == -3){
                        channel.close();
                        System.err.println("WARNING_________ INVALID OPCODE: "+opcode);
                        PlaySession sesh = (PlaySession)ctx.getAttachment();
                        if(sesh != null) {
                            System.err.println("PLAYER: "+sesh.getPlayer().getUsername());
                            System.err.println("OPCODE: "+length);
                            System.err.println("last 10 opcodes");
                            for(Integer packet : sesh.getLast10Packets()){
                                System.err.println("packet: "+packet);
                            }
                        }


                        return null;
                    }
                    if(buffer.readableBytes() < length) {
                        buffer.resetReaderIndex();
                        PlaySession sesh = (PlaySession)ctx.getAttachment();

                        return null;
                    }


                    data = new byte[length];
                    buffer.readBytes(data);
                    return new Packet(opcode, length, data);
                }

            }
           }catch (Exception ee) {
               System.err.println("ERROR DECODING FRAGMENT:");
               if(ctx.getAttachment() != null) {
                    PlaySession session = (PlaySession)ctx.getAttachment();
                    if(session.getPlayer() != null) {
                        System.err.println("woot :"+session.getPlayer());
                        System.err.println(Arrays.toString(session.getLast10Packets()));
                    }
               }
               ee.printStackTrace();
           }
            return null;
        }
    }
