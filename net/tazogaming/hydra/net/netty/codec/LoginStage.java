package net.tazogaming.hydra.net.netty.codec;

public enum LoginStage {
    HANDSHAKE, PRE_STAGE, LOGIN, LOBBY_REQUEST, DISCONNECTED

}
