package net.tazogaming.hydra.net.netty.codec;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/09/14
 * Time: 21:08
 */
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.net.packethandler.Command;
import net.tazogaming.hydra.net.packethandler.Command;
import net.tazogaming.hydra.util.Logger;

import org.apache.mina.core.session.IoSession;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;

/**
 * Encodes the high level <code>Packet</code> class into the proper
 * protocol data required for transmission.
 */
public class GameProtocolEncoder extends OneToOneEncoder {



    private ChannelBuffer response;


    protected Object encode(ChannelHandlerContext channelHandlerContext, Channel channel, Object o) throws Exception {
        Packet p = (Packet) o;

        if (Command.WRITE_STOP) {
            return null;
        }

        try {
            byte[] data       = p.getData();
            int    dataLength = p.getLength();

            if (p.getLength() > 50000) {
                System.err.println("unable to write data chunk to large "+p.getLength()+" "+p.getId());

                return null;
            }

            if (!p.isBare()) {
                int siz = 0;

                siz      += (p.getSize() == Packet.Size.VariableShort)
                            ? 2
                            : 1;
                siz      += (p.getId() > 128)
                            ? 2
                            : 1;
                response = ChannelBuffers.buffer(siz + p.getLength());

                int id          = p.getId();

                if (id< 128) {
                    response.writeByte(id);
                } else {
                    response.writeByte((byte) ((id >> 8) + 128));
                    response.writeByte((byte) (id & 0xFF));
                }

                if (p.getSize() != Packet.Size.Fixed) {    // variable length
                    if (p.getSize() == Packet.Size.VariableByte) {
                        if (dataLength > 255) {            // trying to send more data then we can represent with 8 bits!
                            throw new IllegalArgumentException("Tried to send packet length " + dataLength
                                                               + " in 8 bits [pid=" + p.getId() + "]");
                        }

                        response.writeByte((byte) dataLength);
                    } else if (p.getSize() == Packet.Size.VariableShort) {
                        if (dataLength > 50000) {
                            throw new IllegalArgumentException("Tried to send packet length to big: " + id);
                        }

                        response.writeShort((short) dataLength);
                    }
                }
            } else {
                response = ChannelBuffers.buffer(dataLength);
            }

            response.writeBytes(p.getData());

            return response;
        } catch (Exception e) {
            Logger.err("Error handling message: " + p);
            Logger.err(e);
        }

        return null;
    }

    /**
     * Releases all resources used by this encoder.
     *
     * @param session The IO session
     */
    public void dispose(IoSession session) {}
}
