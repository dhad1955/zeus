package net.tazogaming.hydra.net.packetbuilder;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.Packet;

/**
 * A mutable sequence of bytes used to construct the immutable
 * <code>Packet</code> objects.
 * By default, methods use big endian byte ordering.
 */
public class MessageBuilder implements PacketBuilder {

    /**
     * Default capacity
     */
    private static final int DEFAULT_SIZE = 300;

    /**
     * Bitmasks for <code>addBits()</code>
     */
    private static int bitmasks[] = {
        0, 0x1, 0x3, 0x7, 0xf, 0x1f, 0x3f, 0x7f, 0xff, 0x1ff, 0x3ff, 0x7ff, 0xfff, 0x1fff, 0x3fff, 0x7fff, 0xffff,
        0x1ffff, 0x3ffff, 0x7ffff, 0xfffff, 0x1fffff, 0x3fffff, 0x7fffff, 0xffffff, 0x1ffffff, 0x3ffffff, 0x7ffffff,
        0xfffffff, 0x1fffffff, 0x3fffffff, 0x7fffffff, -1
    };

    /**
     * Bit mask array.
     */
    public static final int[] BIT_MASK_OUT = new int[32];

    /**
     * Creates the bit mask array.
     */
    static {}

    /**
     * Current index into the buffer by bits
     */
    public int          bitPosition = 0;
    private Packet.Size size        = Packet.Size.Fixed;

    /**
     * Whether this packet does not use the standard packet header
     */
    private boolean bare      = false;
    public int      anInt1407 = 0;

    /**
     * The payload buffer
     */
    private byte[] payload;

    /**
     * Current number of bytes used in the buffer
     */
    private int curLength;

    /**
     * ID of the packet
     */
    private int id;

    /**
     * Constructs ...
     *
     */
    public MessageBuilder() {
        this(DEFAULT_SIZE);
    }

    /**
     * Constructs a packet builder with no data and an initial capacity
     * of <code>capacity</code>.
     *
     * @param capacity The initial capacity of the buffer
     */
    public MessageBuilder(int capacity) {
        payload = new byte[capacity];
    }

    /**
     * Constructs ...
     *
     *
     * @param opcode
     * @param size
     */
    public MessageBuilder(int opcode, Packet.Size size) {
        this(DEFAULT_SIZE);
        setId(opcode);
        setSize(size);
    }

    /**
     * Constructs a packet builder with no data and an initial capacity
     * of <code>DEFAULT_SIZE</code>.
     *
     * @see DEFAULT_SIZE
     *
     * @param id
     *
     * @return
     */
    public static MessageBuilder create(int id) {
        return new MessageBuilder().setId(id).setSize(Packet.Size.Fixed);
    }

    /**
     * Method putGJString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void putGJString(String str) {
        addByte(0);
        addBytes(str.getBytes());
        addByte(0);
    }

    /**
     * Ensures that the buffer is at least <code>minimumBytes</code> bytes.
     *
     * @param minimumCapacity The size needed
     */
    private void ensureCapacity(int minimumCapacity) {
        if (minimumCapacity >= payload.length) {
            expandCapacity(minimumCapacity);
        }
    }

    /**
     * Expands the buffer to the specified size.
     *
     * @param minimumCapacity The minimum capacity to which to expand
     * @see java.lang.AbstractStringBuilder#expandCapacity(int)
     */
    private void expandCapacity(int minimumCapacity) {
        int newCapacity = (payload.length + 1) * 2;

        if (newCapacity < 0) {
            newCapacity = Integer.MAX_VALUE;
        } else if (minimumCapacity > newCapacity) {
            newCapacity = minimumCapacity;
        }

        byte[] newPayload = new byte[newCapacity];

        try {
            while (curLength > payload.length) {
                curLength--;
            }

            System.arraycopy(payload, 0, newPayload, 0, curLength);
        } catch (Exception e) {}

        payload = newPayload;
    }

    /**
     * Sets this packet as bare. A bare packet will contain only the payload
     * data, rather than having the standard packet header prepended.
     *
     * @param bare Whether this packet is to be sent bare
     *
     * @return
     */
    public MessageBuilder setBare(boolean bare) {
        this.bare = bare;

        return this;
    }

    /**
     * Sets the ID for this packet.
     *
     * @param id The ID of the packet
     *
     * @return
     */
    public MessageBuilder setId(int id) {
        this.id = id;

        return this;
    }

    /**
     * Method setSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     *
     * @return
     */
    public MessageBuilder setSize(Packet.Size s) {
        size = s;

        return this;
    }

    /**
     * TODO needs a proper description.
     *
     * @return
     */
    public PacketBuilder startBitAccess() {
        bitPosition = curLength * 8;

        return this;
    }

    /**
     * Finishes bit access.
     * @return The PacketBuilder puresLeaderboard, for chaining.
     */
    public PacketBuilder finishBitAccess() {
        curLength = ((bitPosition + 7) / 8);

        return this;
    }

    /**
     * Writes some bits.
     * @param numBits The number of bits to write.
     * @param value The value.
     * @return The PacketBuilder puresLeaderboard, for chaining.
     */
    public PacketBuilder putBits(int value, int numBits) {
        int bytes = (int) Math.ceil((double) numBits / 8D) + 1;

        this.ensureCapacity(this.curLength + (bitPosition + 7) / 8 + bytes);

        byte[] buffer    = this.payload;
        int    bytePos   = bitPosition >> 3;
        int    bitOffset = 8 - (bitPosition & 7);

        bitPosition += numBits;

        for (; numBits > bitOffset; bitOffset = 8) {
            buffer[bytePos]   &= ~BIT_MASK_OUT[bitOffset];
            buffer[bytePos++] |= (value >> (numBits - bitOffset)) & BIT_MASK_OUT[bitOffset];
            numBits           -= bitOffset;
        }

        if (numBits == bitOffset) {
            buffer[bytePos] &= ~BIT_MASK_OUT[bitOffset];
            buffer[bytePos] |= value & BIT_MASK_OUT[bitOffset];
        } else {
            buffer[bytePos] &= ~(BIT_MASK_OUT[numBits] << (bitOffset - numBits));
            buffer[bytePos] |= (value & BIT_MASK_OUT[numBits]) << (bitOffset - numBits);
        }

        return this;
    }

    /**
     * Method method419
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param j
     *
     * @return
     */
    public int method419(int i, int j) {

        // /anInt1407 -= i;
        int k  = anInt1407 >> 3;
        int l  = 8 - (anInt1407 & 7);
        int i1 = 0;

        anInt1407 += i;

        for (; i > l; l = 8) {
            i1 += (payload[k++] & bitmasks[l]) << i - l;
            i  -= l;
        }

        if (i == l) {
            i1 += payload[k] & bitmasks[l];
        } else {
            i1 += payload[k] >> l - i & bitmasks[i];
        }

        return i1;
    }

    /**
     * Method addBits
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param value
     * @param numBits
     *
     * @return
     */
    public MessageBuilder addBits(int value, int numBits) {
        int bytePos   = bitPosition >> 3;
        int bitOffset = 8 - (bitPosition & 7);

        bitPosition += numBits;
        anInt1407   = bitPosition;
        curLength   = (bitPosition + 7) / 8;
        ensureCapacity(curLength);

        for (; numBits > bitOffset; bitOffset = 8) {
            payload[bytePos]   &= ~bitmasks[bitOffset];    // mask out the desired area
            payload[bytePos++] |= (value >> (numBits - bitOffset)) & bitmasks[bitOffset];
            numBits            -= bitOffset;
        }

        if (numBits == bitOffset) {
            payload[bytePos] &= ~bitmasks[bitOffset];
            payload[bytePos] |= value & bitmasks[bitOffset];
        } else {
            payload[bytePos] &= ~(bitmasks[numBits] << (bitOffset - numBits));
            payload[bytePos] |= (value & bitmasks[numBits]) << (bitOffset - numBits);
        }

        // int i = method419(numBits, 0);
        // if(i != value){
        // Logger.err("ERROR: "+i);
        // }
        return this;
    }

    /**
     * Adds the contents of <code>byte</code> array <code>data</code>
     * to the packet. The size of this packet will grow by the length of
     * the provided array.
     *
     * @param data The bytes to add to this packet
     * @return A reference to this object
     */
    public MessageBuilder addBytes(byte[] data) {
        return addBytes(data, 0, data.length);
    }

    /**
     * Adds the contents of <code>byte</code> array <code>data</code>,
     * starting at index <code>offset</code>. The size of this packet will
     * grow by <code>len</code> bytes.
     *
     * @param data   The bytes to add to this packet
     * @param offset The index of the first byte to append
     * @param len       The number of bytes to append
     * @return A reference to this object
     */
    public MessageBuilder addBytes(byte[] data, int offset, int len) {
        int newLength = curLength + len;

        ensureCapacity(newLength);
        System.arraycopy(data, offset, payload, curLength, len);
        curLength = newLength;

        return this;
    }

    /**
     * Method addBytes128
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     * @param offset
     * @param len
     */
    public void addBytes128(byte[] data, int offset, int len) {
        ensureCapacity(this.curLength + len);

        for (int k = offset; k < len; k++) {
            addByte((byte) (data[k] + 128));
        }
    }

    /**
     * Method addByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public MessageBuilder addByte(int t) {
        addByte((byte) t);
        return this;
    }

    /**
     * Method addBytesReversed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param array
     * @param endOffset
     * @param startOffset
     *
     * @return
     */
    public MessageBuilder addBytesReversed(byte[] array, int endOffset, int startOffset) {
        for (int i = (startOffset + endOffset) - 1; i >= startOffset; i--) {
            addByte(array[i]);
        }

        return this;
    }

    /**
     * Method addWordBigEndian
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     *
     * @return
     */
    public MessageBuilder addWordBigEndian(int k) {
        ensureCapacity(curLength + 2);
        addByte((byte) k);
        addByte((byte) (k >> 8));

        return this;
    }

    /**
     * Method addShortA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addShortA(int i) {
        ensureCapacity(curLength + 2);
        addByte((byte) (i >> 8), false);
        addByte((byte) (i + 128), false);

        return this;
    }

    /**
     * Method addLEShortA2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public MessageBuilder addLEShortA2(int val) {
        ensureCapacity(curLength + 2);
        addByte((byte) (val + 128), false);
        addByte((byte) (val >> 8 & 0xFF), false);

        return this;
    }

    /**
     * Method addLEShortA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public MessageBuilder addLEShortA(int val) {
        ensureCapacity(curLength + 2);
        addByte((byte) (val + 128), false);
        addByte((byte) (val >> 8), false);

        return this;
    }

    /**
     * Method addShortB
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addShortB(int i) {
        ensureCapacity(curLength + 2);
        addByte((byte) (i + 128), false);
        addByte((byte) (i >> 8), false);

        return this;
    }

    /**
     * Adds a <code>byte</code> to the data buffer. The size of this
     * packet will grow by one byte.
     *
     * @param val The <code>byte</code> value to add
     * @return A reference to this object
     */
    public MessageBuilder addByte(byte val) {
        return addByte(val, true);
    }

    /**
     * Method addByteA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addByteA(int i) {
        return addByte((byte) (i + 128), true);
    }

    /**
     * Method addByteC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addByteC(int i) {
        ensureCapacity(curLength + 1);

        return addByte((byte) -i);
    }

    /**
     * Method addByteS
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addByteS(int i) {
        return addByteB(i);
    }

    /**
     * Method addByteB
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addByteB(int i) {
        return addByte((byte) (128 - i), true);
    }

    /**
     * Adds a <code>byte</code> to the data buffer. The size of this
     * packet will grow by one byte.
     *
     * @param val                  The <code>byte</code> value to add
     * @param checkCapacity Whether the buffer capacity should be checked
     * @return A reference to this object
     */
    private MessageBuilder addByte(byte val, boolean checkCapacity) {
        if (true) {
            ensureCapacity(curLength + 1);
        }

        payload[curLength++] = val;

        return this;
    }

    /**
     * Method addSmart
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addSmart(int i) {
        if (i >= 128) {
            addShort(i + 32768);
        } else {
            addByte((byte) i, false);
        }

        return this;
    }

    /**
     * Adds a <code>short</code> to the data stream. The size of this
     * packet will grow by two bytes.
     *
     * @param val The <code>short</code> value to add
     * @return A reference to this object
     */
    public MessageBuilder addShort(int val) {
        ensureCapacity(curLength + 2);
        addByte((byte) (val >> 8), false);
        addByte((byte) val, false);

        return this;
    }

    /**
     * Method addLEShort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public MessageBuilder addLEShort(int val) {
        ensureCapacity(curLength + 2);
        addByte((byte) val, false);
        addByte((byte) (val >> 8), false);

        return this;
    }

    /**
     * Method addMediumInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public MessageBuilder addMediumInt(int i) {
        addByte((byte) (i >> 16));
        addByte((byte) (i >> 8));
        addByte((byte) (i));

        return this;
    }

    /**
     * Method setShort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     * @param offset
     *
     * @return
     */
    public MessageBuilder setShort(int val, int offset) {
        payload[offset++] = (byte) (val >> 8);
        payload[offset++] = (byte) val;

        if (curLength < offset + 2) {
            curLength += 2;
        }

        return this;
    }

    /**
     * Adds a <code>int</code> to the data stream. The size of this
     * packet will grow by four bytes.
     *
     * @param val The <code>int</code> value to add
     * @return A reference to this object
     */
    public MessageBuilder addInt(int val) {
        ensureCapacity(curLength + 4);
        addByte((byte) (val >> 24), false);
        addByte((byte) (val >> 16), false);
        addByte((byte) (val >> 8), false);
        addByte((byte) val, false);

        return this;
    }

    /**
     * Method addInt1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public MessageBuilder addInt1(int val) {
        ensureCapacity(curLength + 4);
        addByte((byte) (val >> 8), false);
        addByte((byte) val, false);
        addByte((byte) (val >> 24), false);
        addByte((byte) (val >> 16), false);

        return this;
    }

    /**
     * Method addInt2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public MessageBuilder addInt2(int val) {
        ensureCapacity(curLength + 4);
        addByte((byte) (val >> 16), false);
        addByte((byte) (val >> 24), false);
        addByte((byte) val, false);
        addByte((byte) (val >> 8), false);

        return this;
    }

    /**
     * Method addLEInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public MessageBuilder addLEInt(int val) {
        ensureCapacity(curLength + 4);
        addByte((byte) val, false);
        addByte((byte) (val >> 8), false);
        addByte((byte) (val >> 16), false);
        addByte((byte) (val >> 24), false);

        return this;
    }

    /**
     * Adds a <code>long</code> to the data stream. The size of this
     * packet will grow by eight bytes.
     *
     * @param val The <code>long</code> value to add
     * @return A reference to this object
     */
    public MessageBuilder addLong(long val) {
        addInt((int) (val >> 32));
        addInt((int) (val & -1L));

        return this;
    }

    /**
     * Method addLELong
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public MessageBuilder addLELong(long val) {
        addLEInt((int) (val & -1L));
        addLEInt((int) (val >> 32));

        return this;
    }

    /**
     * Method addRS2String
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     *
     * @return
     */
    public MessageBuilder addRS2String(String s) {
        ensureCapacity(curLength + s.length() + 1);
        s.getBytes(0, s.length(), payload, curLength);
        curLength            += s.length();
        payload[curLength++] = 0;

        return this;
    }

    /**
     * Method addString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     *
     * @return
     */
    public MessageBuilder addString(String s) {
        ensureCapacity(curLength + s.length() + 1);
        s.getBytes(0, s.length(), payload, curLength);
        curLength            += s.length();
        payload[curLength++] = 10;

        return this;
    }

    /**
     * Method getLength
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLength() {
        return curLength;
    }

    /**
     * Returns a <code>Packet</code> object for the data contained
     * in this builder.
     *
     * @return A <code>Packet</code> object
     */
    public Packet toPacket() {
        byte[] data = new byte[curLength];

        System.arraycopy(payload, 0, data, 0, curLength);

        return new Packet(null, id, data, bare, size);
    }
}
