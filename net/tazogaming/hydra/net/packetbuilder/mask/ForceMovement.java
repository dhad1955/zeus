package net.tazogaming.hydra.net.packetbuilder.mask;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/10/13
 * Time: 07:14
 */
public class ForceMovement {
    private boolean hasRendered   = false;
    private int     startX        = -1,
                    startY        = -1;
    private int     endX          = -1,
                    endY          = -1;
    private int     faceDirection = -1;
    private int     waitSpeed     = -1;
    private int     moveSpeed     = -1;
    private Player  ourPlayer;
    private int     stopTime;
    private int     newX, newY;
    private Scope   wakeup;

    /**
     * Constructs ...
     *
     *
     * @param dir
     * @param steps
     * @param waitSpeed
     * @param moveSpeed
     * @param player
     * @param faceDir
     * @param wakeup_script
     */
    public ForceMovement(int dir, int steps, int waitSpeed, int moveSpeed, Player player, int faceDir,
                         Scope wakeup_script) {
        int actualLocX = player.getX();
        int actualLocY = player.getY();
        int locX       = player.getX();
        int locY       = player.getY();
        int newAbsX    = player.getX();
        int newAbsY    = player.getY();

        faceDir &= 3;

        if (dir == 0) {    // north
            locY    += steps;
            newAbsY += steps;
        }

        if (dir == 2) {    // south
            locY    -= steps;
            newAbsY -= steps;
        }

        if (dir == 1) {
            locX    += steps;
            newAbsX += steps;
        }

        if (dir == 3) {
            locX    -= steps;
            newAbsX -= steps;
        }

        int totalTime = steps * moveSpeed;

        totalTime += waitSpeed;

        int ticks = (totalTime / 30);// + 1;

        this.waitSpeed     = waitSpeed;
        this.moveSpeed     = totalTime;
        this.startX        = actualLocX;
        this.startY        = actualLocY;
        this.faceDirection = faceDir;
        this.ourPlayer     = player;
        this.endX          = locX;
        this.endY          = locY;
        this.newX          = newAbsX;
        this.newY          = newAbsY;
        this.stopTime      = Core.currentTime + ticks;
        this.wakeup        = wakeup_script;
    }

    /**
     * Constructs ...
     *
     *
     * @param stx
     * @param sty
     * @param endX
     * @param endY
     * @param waitSpeed
     * @param moveSpeed
     * @param dir
     * @param owner
     */
    public ForceMovement(int stx, int sty, int endX, int endY, int waitSpeed, int moveSpeed, int dir, Player owner) {
        this.ourPlayer     = owner;
        this.faceDirection = dir;
        this.endX          = endX;
        this.endY          = endY;
        this.startX        = stx;
        this.startY        = sty;
        this.moveSpeed     = moveSpeed;
        this.waitSpeed     = waitSpeed;
    }

    /**
     * Method getWakeupScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Scope getWakeupScript() {
        return wakeup;
    }

    /**
     * Method isActive
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isActive() {
        return !isCompleted();
    }

    private boolean isCompleted() {
        return Core.currentTime >= stopTime;
    }

    /**
     * Method hasRendered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasRendered() {
        return hasRendered;
    }

    /**
     * Method setRendered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setRendered(boolean b) {
        hasRendered = b;
    }

    /**
     * Method getLocalY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param movingToY
     *
     * @return
     */
    public int getLocalY(Player pla, int movingToY) {
        int delta = movingToY - pla.getY();

        return pla.getKnownLocalY() + delta;
    }

    /**
     * Method getLocalX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param movingToX
     *
     * @return
     */
    public int getLocalX(Player pla, int movingToX) {
        int delta = movingToX - pla.getX();

        return pla.getKnownLocalX() + delta;
    }

    /**
     * Method isHasRendered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isHasRendered() {
        return hasRendered;
    }

    /**
     * Method setHasRendered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hasRendered
     */
    public void setHasRendered(boolean hasRendered) {
        this.hasRendered = hasRendered;
    }

    /**
     * Method getOurPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getOurPlayer() {
        return ourPlayer;
    }

    /**
     * Method setOurPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ourPlayer
     */
    public void setOurPlayer(Player ourPlayer) {
        this.ourPlayer = ourPlayer;
    }

    /**
     * Method getStartX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStartX() {
        return startX;
    }

    /**
     * Method setStartX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startX
     */
    public void setStartX(int startX) {
        this.startX = startX;
    }

    /**
     * Method getStartY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStartY() {
        return startY;
    }

    /**
     * Method setStartY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startY
     */
    public void setStartY(int startY) {
        this.startY = startY;
    }

    /**
     * Method getEndX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getEndX() {
        return endX;
    }

    /**
     * Method setEndX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param endX
     */
    public void setEndX(int endX) {
        this.endX = endX;
    }

    /**
     * Method getEndY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getEndY() {
        return endY;
    }

    /**
     * Method setEndY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param endY
     */
    public void setEndY(int endY) {
        this.endY = endY;
    }

    /**
     * Method getFaceDirection
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFaceDirection() {
        return faceDirection;
    }

    /**
     * Method setFaceDirection
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param faceDirection
     */
    public void setFaceDirection(int faceDirection) {
        this.faceDirection = faceDirection;
    }

    /**
     * Method getWaitSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWaitSpeed() {
        return waitSpeed;
    }

    /**
     * Method setWaitSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param waitSpeed
     */
    public void setWaitSpeed(int waitSpeed) {
        this.waitSpeed = waitSpeed;
    }

    /**
     * Method getMoveSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMoveSpeed() {
        return moveSpeed;
    }

    /**
     * Method setMoveSpeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param moveSpeed
     */
    public void setMoveSpeed(int moveSpeed) {
        this.moveSpeed = moveSpeed;
    }

    /**
     * Method getStopTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStopTime() {
        return stopTime;
    }

    /**
     * Method setStopTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param stopTime
     */
    public void setStopTime(int stopTime) {
        this.stopTime = stopTime;
    }

    /**
     * Method getNewX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNewX() {
        return newX;
    }

    /**
     * Method setNewX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newX
     */
    public void setNewX(int newX) {
        this.newX = newX;
    }

    /**
     * Method getNewY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNewY() {
        return newY;
    }

    /**
     * Method setNewY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param newY
     */
    public void setNewY(int newY) {
        this.newY = newY;
    }
}
