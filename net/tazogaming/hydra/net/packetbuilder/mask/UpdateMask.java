package net.tazogaming.hydra.net.packetbuilder.mask;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 23/09/13
 * Time: 19:26
 * To change this template use File | Settings | File Templates.
 */
public abstract class UpdateMask {
    private int     currentId  = 0;
    private boolean hasChanged = false;

    /**
     * Method setChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param changed
     */
    public void setChanged(boolean changed) {
        hasChanged = changed;

        if (changed) {
            currentId++;
        }
    }

    /**
     * Method setCurrentID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setCurrentID(int id) {
        currentId = id;
    }

    /**
     * Method hasChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasChanged() {
        return hasChanged;
    }

    /**
     * Method getCurrentId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentId() {
        return currentId;
    }
}
