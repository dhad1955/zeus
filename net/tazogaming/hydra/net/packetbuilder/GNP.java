package net.tazogaming.hydra.net.packetbuilder;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.util.collections.StatefulEntityCollection;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/07/14
 * Time: 00:12
 */
public class GNP implements PacketBuilder {
    public static final int
        HIT            = 0x4,
        GRAPHICS       = 0x2,
        SWITCH         = 0x80,
        FACE_ENTITY    = 0x8,
        ANIMATION      = 0X10,
        FACE_DIRECTION = 0x40,
        FORCE_TEXT     = 0X1;

    /**
     * Method getPacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public Packet getPacket(Player player) {
        MessageBuilder                gni         = new MessageBuilder(6, Packet.Size.VariableShort);
        MessageBuilder                updateBlock = new MessageBuilder().setBare(true);
        StatefulEntityCollection<NPC> npcs        = player.getWatchedNPCs();

        gni.startBitAccess();
        gni.addBits(npcs.getKnownEntities().size(), 8);

        for (NPC npc : npcs.getKnownEntities()) {
            for (Projectile proj : npc.getProjectiles()) {
                player.getActionSender().createProjectile(proj);
            }

            if (npcs.isRemoving(npc)) {
                gni.addBits(1, 1);
                gni.addBits(3, 2);
            } else {
                int direction = npc.getWalkDir();
                int mask      = getUpdateMask(npc, player);

                if (direction == -1) {
                    if (mask > 0) {
                        gni.addBits(1, 1);
                        gni.addBits(0, 2);
                    } else {
                        gni.addBits(0, 1);
                    }
                } else {
                    gni.addBits(1, 1);
                    gni.addBits(1, 2);
                    gni.addBits(direction, 3);
                    gni.addBits((mask > 0)
                                ? 1
                                : 0, 1);
                }

                if (mask > 0) {
                    appendUpdateBlock(npc, player, mask, updateBlock);
                }
            }
        }

        for (NPC npc : npcs.getNewEntities()) {
            gni.addBits(npc.getIndex(), 15);
            gni.addBits(npc.getId(), 14);
            gni.addBits(1, 1);

            int mask = getUpdateMask(npc, player);
            int y    = npc.getLocation().getY() - player.getLocation().getY();
            int x    = npc.getLocation().getX() - player.getLocation().getX();

            if (x < 0) {
                x += 32;
            }

            if (y < 0) {
                y += 32;
            }

            gni.addBits(x, 5);
            gni.addBits(y, 5);
            gni.addBits(npc.getHeight(), 2);
            gni.addBits((mask > 0)
                        ? 1
                        : 0, 1);
            gni.addBits(0, 3);    // face direction

            if (mask > 0) {
                appendUpdateBlock(npc, player, mask, updateBlock);
            }
        }

        gni.addBits(32767, 15);
        gni.finishBitAccess();
        gni.addBytes(updateBlock.toPacket().getData());

        return gni.toPacket();
    }

    /**
     * Method getUpdateMask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param player
     *
     * @return
     */
    public static int getUpdateMask(NPC npc, Player player) {
        int mask = 0;

        if (((npc.getDamage1() != null) || (npc.getDamage2() != null))) {
            mask |= HIT;
        }

        if (npc.getGraphic().hasChanged()) {
            mask |= GRAPHICS;
        }

        if (npc.getTransformID() != -1) {
            mask |= SWITCH;
        }

        if (player.getKnownNpcData(npc).getKnownFacingID() != npc.getFocus().getCurrentId()) {
            mask |= FACE_ENTITY;
            player.getKnownNpcData(npc).setKnownFacing(npc.getFocus().getCurrentId());
        }

        if (npc.getAnimation().hasChanged()) {
            mask |= ANIMATION;
        }

        if (npc.getFacingLocation().isSet()) {
            mask |= FACE_DIRECTION;
        }

        if (npc.getTextUpdate() != null) {
            mask |= FORCE_TEXT;
        }

        return mask;
    }

    private void appendDamageUpdate(NPC npc, Player watching, MessageBuilder updateblock) {
        Damage[] dmg = new Damage[(npc.getDamage2() != null)
                                  ? 2
                                  : 1];

        updateblock.addByteC(dmg.length);
        dmg[0] = npc.getDamage1();

        if (dmg.length > 1) {
            dmg[1] = npc.getDamage2();
        }

        for (Damage damage : dmg) {
            int type = damage.getClientType();

            if (damage.getDamage() < 1) {
                type = 8;    // miss
            }

            if (damage.isCrit()) {
                type += 10;
            }

            if (damage.getSoakedAmount() > 0) {
                updateblock.addSmart(32767);
            }

            if ((damage.getDamager() == watching) || true) {
                updateblock.addSmart(type);
            } else {
                updateblock.addSmart(type + 14);
            }

            updateblock.addSmart((watching.getAccount().isX10Hits()
                                  ? (int)(damage.getDamage() * 10)
                                  : (int)damage.getDamage()));

            if (damage.getSoakedAmount() > 0) {
                updateblock.addSmart(5);
                updateblock.addSmart(watching.getAccount().isX10Hits() ? (int)(damage.getSoakedAmount() * 10) :
                        (int)damage.getSoakedAmount());
            }

            updateblock.addSmart(0);
            updateblock.addByte(((int)(npc.getCurrentHealth() * 255 / npc.getMaxHealth())));
        }
    }

    /**
     * Method appendUpdateBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param player
     * @param mask
     * @param updateblock
     */
    public void appendUpdateBlock(NPC npc, Player player, int mask, MessageBuilder updateblock) {
        int maskData = mask;

        if (maskData > 128) {
            maskData |= 0x20;
        }

        updateblock.addByte(maskData);

        if (maskData > 128) {
            updateblock.addByte(maskData >> 8);
        }

        if ((mask & HIT) != 0) {
            appendDamageUpdate(npc, player, updateblock);
        }

        if ((mask & GRAPHICS) != 0) {
            updateblock.addShortA(npc.getGraphic().getIndex());
            updateblock.addLEInt(0);
            updateblock.addByteC(npc.getGraphic().getGraphicWait());    // height
        }

        if ((mask & FACE_ENTITY) != 0) {
            updateblock.addShort(npc.getFocus().getInteractingWith());
        }

        if ((mask & SWITCH) != 0) {
            updateblock.addLEShort(npc.getTransformID());
        }

        if ((mask & ANIMATION) != 0) {
            for (int i = 0; i < 4; i++) {
                updateblock.addShortA(npc.getAnimation().getIndex());
            }

            updateblock.addByteS(0);
        }

        if ((mask & FACE_DIRECTION) != 0) {
            updateblock.addLEShortA((npc.getFacingLocation().getX() << 1) + 1);
            updateblock.addLEShortA((npc.getFacingLocation().getY() << 1) + 1);
        }

        if ((mask & FORCE_TEXT) != 0) {
            updateblock.addRS2String(npc.getTextUpdate());
        }
    }
}
