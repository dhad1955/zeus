package net.tazogaming.hydra.net.packetbuilder;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.EquipmentTypes;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.mask.ForceMovement;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.collections.StatefulEntityCollection;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/07/14
 * Time: 01:20
 */
public class GPI implements PacketBuilder {
    public static final int
        APPEARANCE           = 0x40,
        ANIMATION            = 0x10,
        DAMAGE_1             = 0x8,
        GRAPHIC              = 0x4000,
        TELEPORT_UPDATE      = 0x2000,
        TEXT_UPDATE          = 0x8000,
        CHAT_MESSAGE         = 0x80,
        FACE_COORD           = 0x4,
        FOCUS                = 0x2,
        MOVEMENT_TYPE        = 0x1,
        FORCE_MOVEMENT       = 0x200;
    public static int FACING = 0;

    /**
     * Method getCrownForGroup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param group
     *
     * @return
     */
    public static final int getCrownForGroup(int group) {
        if ((group == Player.ADMINISTRATOR) || (group == Player.ROOT_ADMIN)) {
            return 2;
        } else if ((group == Player.MODERATOR) || (group == Player.COMMUNITY_MANAGER)) {
            return 1;
        }

        return 0;
    }

    /**
     * Method forceSendProjectile
     * Created on 14/10/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param projectile
     * @param player
     */
    public static void forceSendProjectile(Projectile projectile, Player player) {
        player.getActionSender().createProjectile(projectile);

        for (Player pla : player.getWatchedPlayers().getAllEntities()) {
            pla.getActionSender().createProjectile(projectile);
        }
    }

    private void sendProjectiles(Player sendingTo, Player pla) {
        for (Projectile p : pla.getProjectiles()) {
           if(p.getMoveSpeed() != -999)
            sendingTo.getActionSender().createProjectile(p);
        }
    }

    /**
     * Method getFacingDirection
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param coordX
     * @param coordY
     * @param faceX
     * @param faceY
     *
     * @return
     */
    public static int getFacingDirection(int coordX, int coordY, int faceX, int faceY) {
        if (faceX > coordX) {
            if (faceY > coordY) {
                return 10240;
            } else if (faceY < coordY) {
                return 14336;
            } else {
                return 12288;
            }
        } else if (faceX >= coordX) {
            if (coordY < faceY) {
                return 8192;
            } else if (faceY < coordY) {
                return 0;
            }
        } else if (faceY <= coordY) {
            if (faceY < coordY) {
                return 2048;
            } else {
                return 4096;
            }
        } else {
            return 6144;
        }

        return -1;
    }

    /**
     * Method applyFacing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param x
     * @param y
     * @param updateBlock
     */
    public void applyFacing(Player player, int x, int y, MessageBuilder updateBlock) {
        if ((x == -1) || (y == -1)) {
            updateBlock.addLEShort(0);

            return;
        }

        int dir = Movement.getDirectionForWaypoints(player.getLocation(), Point.location(x, y, player.getHeight()));

        if (dir != -1) {
            updateBlock.addLEShort(Movement.FACING[dir]);
        } else {
            updateBlock.addLEShort(0);
        }
    }

    /**
     * Method getPacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public synchronized Packet getPacket(final Player player) {



        MessageBuilder mainPacket  = new MessageBuilder().setId(70).setSize(Packet.Size.VariableShort);
        MessageBuilder updateBlock = new MessageBuilder().setBare(true);

        sendProjectiles(player, player);

        if (player.getLastChatMessage() != null) {
            player.getActionSender().sendPublicChatMessage(player.getIndex(), getCrownForRank(player.getRights(), player.getAccount().getIronManMode()),
                    player.getLastChatMessage().getEffects(), player.getLastChatMessage().toString());
        }

        mainPacket.startBitAccess();

        int mask = getUpdateMask(player, player);

        if (mask > 0) {
            appendUpdateMask(player, player, mask, updateBlock, mainPacket.getLength());
            mainPacket.addBits(1, 1);
            applyLocalUpdate(mainPacket, player);
        } else {
            mainPacket.addBits(0, 1);
        }

        StatefulEntityCollection<Player> players = player.getWatchedPlayers();

        for (Player pla : players.getKnownEntities()) {
            sendProjectiles(pla, player);

            if (players.isRemoving(pla)) {
                mainPacket.addBits(1, 1);
                mainPacket.addBits(pla.getIndex(), 11);
                removeLocalPlayer(mainPacket, pla);
            } else if (!players.isAdding(pla)) {
                mask = getUpdateMask(pla, player);

                if (pla.getLastChatMessage() != null) {
                   if(!player.getFriendsList().isIgnored(pla.getUsername()))
                    player.getActionSender().sendPublicChatMessage(pla.getIndex(), getCrownForRank(pla.getRights(), pla.getAccount().getIronManMode()),
                            pla.getLastChatMessage().getEffects(), pla.getLastChatMessage().toString());
                }

                if (mask > 0) {
                    this.appendUpdateMask(pla, player, mask, updateBlock, mainPacket.getLength());
                }

                if (pla.hasMoved() || pla.didTeleport() || (mask > 0)) {
                    mainPacket.addBits(1, 1);
                    mainPacket.addBits(pla.getIndex(), 11);
                    applyLocalUpdate(mainPacket, pla);

                    continue;
                }

                mainPacket.addBits(0, 1);
            }
        }

        for (Player pla : players.getNewEntities()) {
            addLocalPlayer(mainPacket, pla, player);
            mask = getUpdateMask(pla, player);

            if (mask > 0) {
                appendUpdateMask(pla, player, mask, updateBlock, mainPacket.getLength());
            }
        }

        mainPacket.addBits(0, 1);
        mainPacket.finishBitAccess();

        if (updateBlock.getLength() > 0) {
            mainPacket.addBytes(updateBlock.toPacket().getData());
        }

        return mainPacket.toPacket();
    }

    /**
     * Method sendEquipment
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param pla
     * @param builder
     */
    public void sendEquipment(int id, Player pla, MessageBuilder builder) {
        id = pla.getEquipment().getId(id);

        if (id == -1) {
            builder.addByte((byte) 0);
        } else {
            id = Item.forId(id).getEquipID();
            builder.addShort(32768 + id);
        }
    }

    private void appendAppearance(Player player, MessageBuilder spb, Player watching) {
        MessageBuilder appearance   = new MessageBuilder().setBare(true);
        int            miscSettings = 0;

        miscSettings |= player.getAppearance().getGender() & 0x1;

        // hash |= 0x4;    // combat coloring
        appearance.addByte(miscSettings);
        int compSettings = 0;

        int capeType = (player.getEquipment().contains(4413) ? 0 : ((player.getEquipment().contains(4411) ? 1 : -1)));
        if(capeType > -1)
            compSettings |= 1;
        if(player.getAppearance().isForceAppearanceRequired())
            compSettings |= 2;

        appearance.addByte(compSettings);

        if((compSettings & 1) != 0){
            for(int i = 0; i < 4; i++)
                appearance.addShort(player.getAppearance().getCapeColour(capeType, i));
        }

        if (watching.getAccount().hasVar("hidetitles")) {
            appearance.addRS2String("");
        } else {
            if (player.getAccount().hasVar("title")) {
                appearance.addRS2String("<col=E65C00>" + player.getAccount().getString("title") + "</col>");
            } else {
                appearance.addRS2String("");
            }
        }



        appearance.addByte(0);    // titles, come back to this later

        // headicons
        appearance.addByte((byte) (byte) (player.getTimers().timerActive(TimingUtility.SKULL_TIMER)
                                          ? 0
                                          : -1));    // mask

        // 1 = red skull
        appearance.addByte((byte) player.getHeadicon() - 1);
        appearance.addByte((byte) 0);

        if (player.getAppearance().getTransform().isTransformed()) {
            appearance.addShort(-1);
            appearance.addShort(player.getAppearance().getTransform().getIndex());
            appearance.addByte(0);
        } else {
            Equipment e = player.getEquipment();

            sendEquipment(Equipment.HAT, player, appearance);
            sendEquipment(Equipment.CAPE, player, appearance);
            sendEquipment(Equipment.AMULET, player, appearance);
            sendEquipment(Equipment.WEAPON, player, appearance);

            if (e.isEquipped(Equipment.TORSO)) {
                sendEquipment(Equipment.TORSO, player, appearance);
            } else {
                appearance.addShort(256 + player.getAppearance().getTorso());
            }

            sendEquipment(Equipment.SHIELD, player, appearance);

            if (!e.isEquipped(Equipment.TORSO)
                    || (e.isEquipped(Equipment.TORSO) && EquipmentTypes.needsArms(e.getId(Equipment.TORSO)))) {
                int arms = player.getAppearance().getArms();
                if(arms <= 0)
                    arms = 599;

                appearance.addShort(256 + arms);
            } else {
                appearance.addByte((byte) 0);
            }

            if (e.isEquipped(Equipment.LEGS)) {
                appearance.addShort(32768 + Item.forId(e.getId(Equipment.LEGS)).getEquipID());
            } else {
                appearance.addShort(256 + player.getAppearance().getLegs());
            }

            if (!EquipmentTypes.isFullHelmet(e.getId(Equipment.HAT)) &&!EquipmentTypes.wearingHood(player)) {
                appearance.addShort(256 + player.getAppearance().getHead());
            } else {
                appearance.addByte((byte) 0);
            }

            if (e.isEquipped(Equipment.HANDS)) {
                appearance.addShort(32768 + Item.forId(e.getId(Equipment.HANDS)).getEquipID());
            } else {
                appearance.addShort(256 + player.getAppearance().getHands());
            }

            if (e.isEquipped(Equipment.FEET)) {
                appearance.addShort(32768 + Item.forId(e.getId(Equipment.FEET)).getEquipID());
            } else {
                appearance.addShort(256 + player.getAppearance().getFeet());
            }

            boolean jaw = true;

            if (EquipmentTypes.isFullHelmet(player.getEquipment().getId(Equipment.HAT))
                    &&!EquipmentTypes.wearingHood(player)) {
                jaw = false;
            }

            if (jaw && (player.getAppearance().getGender() == 0)) {
                appearance.addShort(256 + player.getAppearance().getJaw());
            } else {
                appearance.addByte((byte) 0);
            }

        }

        for (int i = 0; i < 5; i++) {
            appearance.addByte((byte) player.getAppearance().getColour()[i]);
        }

        appearance.addShort(player.getRenderAnimation());    // render animation

        // username
        appearance.addRS2String(player.getUsername());
        appearance.addByte((byte) player.getCombatLevel());
        appearance.addByte((byte) player.getCombatLevel());
        appearance.addByte(0);
        appearance.addByte(0);

        Packet appearancePacket = appearance.toPacket();

        spb.addByteA(appearancePacket.getLength());
        spb.addBytes(appearancePacket.getData(), 0, appearancePacket.getLength());
    }

    /**
     * Method getCrownForRank
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rank
     *
     * @return
     */
    public static final int getCrownForRank(int rank, int ironmode) {
        return UserAccount.GET_CROWN(rank, ironmode);
    }

    private void appendDamageUpdate(Player player, Player watching, MessageBuilder updateblock) {
        Damage[] dmg = new Damage[(player.getDamage2() != null)
                                  ? 2
                                  : 1];

        updateblock.addByte(dmg.length);
        dmg[0] = player.getDamage1();

        if (dmg.length > 1) {
            dmg[1] = player.getDamage2();
        }

        for (Damage damage : dmg) {
            int type = damage.getClientType();

            if (damage.getDamage() < 1) {
                type = 8;    // miss
            }


            if (damage.isCrit() && type != 8) {
                type += 10;
            }

            if (damage.getSoakedAmount() > 0) {
                updateblock.addSmart(32767);
            }

            if ((damage.getDamager() == watching) || true) {
                updateblock.addSmart(type);
            } else {
                updateblock.addSmart(type + 14);
            }

            updateblock.addSmart((watching.getAccount().isX10Hits()
                                  ? (int)(damage.getDamage() * 10)
                                  : (int)damage.getDamage()));
            if (damage.getSoakedAmount() > 0) {
                updateblock.addSmart(5);
                updateblock.addSmart(watching.getAccount().isX10Hits() ? (int)(damage.getSoakedAmount() * 10) : (int)damage.getSoakedAmount());
            }
            updateblock.addSmart(0);
            updateblock.addByte((((player.getCurrentHealth() > player.getMaxHealth())
                                  ? (int)player.getMaxHealth()
                                  : (int)player.getCurrentHealth()) * 255 / (int)player.getMaxHealth()));
        }
    }

    private int getUpdateMask(Player target, Player player) {
        int updateMask = 0;

        if (target.getGraphic().hasChanged()) {
            updateMask |= GRAPHIC;
        }

        if ((target.getMovementMask() != null) &&!target.getMovementMask().hasRendered()) {
            updateMask |= FORCE_MOVEMENT;
        }

        if (player.getKnownViewpointId(target) < target.getFacingLocation().getCurrentId()) {
            updateMask |= FACE_COORD;
            player.setKnownViewpointId(target, target.getFacingLocation().getCurrentId());
        }

        if (player.getKnownFacingID(target) < target.getFocus().getCurrentId()) {
            updateMask |= FOCUS;
            player.setKnownFacingID(target, target.getFocus().getCurrentId());
        }

        if ((target.getDamage2() != null) || (target.getDamage1() != null)) {
            updateMask |= DAMAGE_1;
        }

        if (target.getAnimation().hasChanged()) {
            updateMask |= ANIMATION;
        }

        if (player.getKnownAppearanceId(target) < target.getAppearance().getCurrentId()) {
            updateMask |= APPEARANCE;
            player.setKnownAppearanceId(target, target.getAppearance().getCurrentId());
        }

        if (target.getTextUpdate() != null || (target.hitDebug.length() > 0 && player.getRights() > Player.ADMINISTRATOR)) {
            updateMask |= 0x8000;
        }

        if (target.didTeleport()) {
            updateMask |= TELEPORT_UPDATE;
        }
        if ((target.getRunDirection() != -1) || (target.getWalkDir() != -1)) {
            updateMask |= MOVEMENT_TYPE;
        }

        return updateMask;
    }

    private void animationUpdate(Player player, MessageBuilder builder) {
        for (int i = 0; i < 4; i++) {
            builder.addLEShortA(player.getAnimation().getIndex());
        }

        builder.addByte(0);
    }

    private void appendUpdateMask(Player player, Player playerWatching, int mask, MessageBuilder updateBlock, int len) {
        int oldMask  = mask;
        int maskData = mask;

        if (maskData > 128) {
            maskData |= 0x20;
        }

        if (maskData > 32768) {
            maskData |= 0x800;
        }

        updateBlock.addByte(maskData);

        if (maskData > 128) {
            updateBlock.addByte(maskData >> 8);
        }

        if (maskData > 32768) {
            updateBlock.addByte(maskData >> 16);
        }

        if ((oldMask & GRAPHIC) != 0) {
            appendGraphicUpdate(player.getGraphic(), updateBlock);
        }

        if ((oldMask & TELEPORT_UPDATE) != 0) {
            updateBlock.addByteA(127);
        }

        if ((oldMask & FORCE_MOVEMENT) != 0) {
            appendForceMovement(playerWatching, player, player.getMovementMask(), updateBlock);
        }

        if ((oldMask & FACE_COORD) != 0) {
            applyFacing(player, player.getFacingLocation().getX(), player.getFacingLocation().getY(), updateBlock);
        }

        if ((oldMask & FOCUS) != 0) {
            int id = player.getFocus().getInteractingWith();

            updateBlock.addLEShort(id);
        }

        if ((oldMask & DAMAGE_1) != 0) {
            appendDamageUpdate(player, playerWatching, updateBlock);
        }

        if ((oldMask & ANIMATION) != 0) {
            animationUpdate(player, updateBlock);
        }

        if ((oldMask & APPEARANCE) != 0) {
            appendAppearance(player, updateBlock, playerWatching);
        }

        if ((oldMask & TEXT_UPDATE) != 0) {
           if(player.hitDebug.length() >0 && playerWatching.getRights() > Player.ADMINISTRATOR)
               updateBlock.addRS2String(player.hitDebug);
            else
            updateBlock.addRS2String(player.getTextUpdate());
        }

        if ((oldMask & MOVEMENT_TYPE) != 0) {


            updateBlock.addByteA((player.getWalkDir() != -1 && player.getRunDirection() != -2999)
                                 ? 1
                                 : player.getRunDirection() != -2999 ? 3 : 2);
        }
    }

    private void appendGraphicUpdate(Graphic graphic, MessageBuilder black) {
        black.addLEShortA(graphic.getIndex());
        black.addInt2(graphic.getGraphicWait());
        black.addByte(0);    // rotation rofl
    }

    private void sendLocalPlayerStatus(MessageBuilder packet, int type, boolean status) {
        packet.addBits(status
                       ? 1
                       : 0, 1);
        packet.addBits(type, 2);
    }

    private void sendLocalPlayerTeleport(MessageBuilder packet, Player other) {
        sendLocalPlayerStatus(packet, 3, true);
        packet.addBits(1, 1);
        packet.addBits(other.getLocation().getY() | other.getLocation().getHeight() << 28
                       | other.getLocation().getX() << 14, 30);
    }

    /**
     * Method appendForceMovement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param watching
     * @param whom
     * @param movement
     * @param updateblock
     */
    public void appendForceMovement(Player watching, Player whom, ForceMovement movement, MessageBuilder updateblock) {
        int deltaFromX = 0;    // movement.getStartX() - movement.getEndX();
        int deltaFromY = 0;    // movement.getStartY() - movement.getEndY();
        int deltaToX   = movement.getEndX() - movement.getStartX();
        int deltaToY   = movement.getEndY() - movement.getStartY();

        updateblock.addByteC(deltaFromX).addByteA(deltaFromY);
        updateblock.addByteC(deltaToX).addByteC(deltaToY);
        updateblock.addLEShort(movement.getWaitSpeed());
        updateblock.addLEShortA(movement.getMoveSpeed());
        updateblock.addByteA(movement.getFaceDirection());
    }

    private void applyLocalUpdate(MessageBuilder packet, Player other) {
        if (other.didTeleport()) {
            sendLocalPlayerTeleport(packet, other);

            return;
        }

        int walkDir = other.getWalkDir();
        int runDir  = other.getRunDirection();



        sendLocalPlayerStatus(packet, (walkDir > -1)
                                      ? 1
                                      : (runDir > -1)
                                        ? 2
                                        : 0, true);

        if ((walkDir < 0) && (runDir < 0)) {
            return;
        }

        packet.addBits((walkDir > -1)
                       ? walkDir
                       : runDir, (walkDir > -1)
                                 ? 3
                                 : 4);
    }

    private void removeLocalPlayer(MessageBuilder packet, Player other) {
        sendLocalPlayerStatus(packet, 0, false);
        packet.addBits(0, 1);
    }

    private void addLocalPlayer(MessageBuilder packet, Player other, Player watching) {
        packet.addBits(1, 1);
        packet.addBits(other.getIndex(), 11);
        packet.addBits(0, 2);

        boolean updateHash = false;

        packet.addBits(updateHash
                       ? 0
                       : 1, 1);

        if (!updateHash) {
            packet.addBits(3, 2);
            packet.addBits(other.getLocation().get18BitsHash(), 18);
        }

        packet.addBits(other.getLocation().getX() - ((other.getLocation().getX() >> 3) << 6), 6);
        packet.addBits(other.getLocation().getY() - ((other.getLocation().getY() >> 3) << 6), 6);
        packet.addBits(1, 1);
    }
}
