package net.tazogaming.hydra.net.packetbuilder.gi.descriptor;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.net.packetbuilder.gi.EntityDescriptor;
import net.tazogaming.hydra.util.collections.StatefulEntityCollection;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 01/07/14
 * Time: 01:38
 * To change this template use File | Settings | File Templates.
 */
public class WatchedPlayersDescriptor extends EntityDescriptor<Player> {
    private MessageBuilder mainPacket;
    private Player         player;

    /**
     * Constructs ...
     *
     *
     * @param players
     * @param mainPacket
     * @param arePlayer
     */
    public WatchedPlayersDescriptor(StatefulEntityCollection<Player> players, MessageBuilder mainPacket,
                                    Player arePlayer) {
        super(players);
        this.mainPacket = mainPacket;
        this.player     = arePlayer;
    }

    private void applySkip(MessageBuilder packet, int amount) {
        try {
            packet.addBits((amount == 0)
                           ? 0
                           : (amount > 255)
                             ? 3
                             : ((amount > 31)
                                ? 2
                                : 1), 2);

            if (amount > 0) {
                packet.addBits(amount, (amount > 255)
                                       ? 11
                                       : ((amount > 31)
                                          ? 8
                                          : 5));
            }
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
    }

    @Override
    protected boolean requiresUpdate(Player entity) {
        return entity.didTeleport() || entity.hasMoved() || this.getCollection().isRemoving(entity);
    }

    private void sendLocalPlayerStatus(int type, boolean status) {
        try {
            mainPacket.addBits(status
                               ? 1
                               : 0, 1);
            mainPacket.addBits(type, 2);
        } catch (Exception ignored) {}
    }

    @Override
    protected void update(Player entity) {}

    @Override
    protected void skip(int amount) {
        applySkip(mainPacket, amount);
    }

    @Override
    protected void load() {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
