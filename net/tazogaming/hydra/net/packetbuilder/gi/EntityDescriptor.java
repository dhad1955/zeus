package net.tazogaming.hydra.net.packetbuilder.gi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.util.collections.StatefulEntityCollection;

//~--- JDK imports ------------------------------------------------------------

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 01/07/14
 * Time: 01:00
 * To change this template use File | Settings | File Templates.
 *
 * @param <T>
 */
public abstract class EntityDescriptor<T extends Entity> {
    private T[]                      items;
    private int                      caret;
    private StatefulEntityCollection collection;

    /**
     * Constructs ...
     *
     *
     * @param collection
     */
    public EntityDescriptor(StatefulEntityCollection collection) {
        this.collection = collection;
        load();
    }

    /**
     * Method hasNext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasNext() {
        return caret < items.length;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public T next() {
        return items[caret++];
    }

    protected StatefulEntityCollection getCollection() {
        return this.collection;
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void run() {
        int MAX     = 2048;
        int offset  = 1;
        int curSkip = 0;

        offset  = 0;
        curSkip = 0;

        T n = null;

        while (hasNext()) {
            n       = next();
            curSkip += (n.index - offset);
            offset  += (n.index - offset);

            if (requiresUpdate(n)) {
                if (curSkip > 0) {
                    skip(curSkip);
                }
            }

            curSkip = 0;
            update(n);
        }

        if (offset < MAX) {
            skip(MAX - offset);
        }
    }

    protected void fill(Collection<T> entities) {
        this.items = (T[]) new Entity[entities.size()];
        entities.toArray(this.items);
        sort();
    }

    private void sort() {
        for (int j = 0; j < items.length - 1; j++) {
            boolean isSorted = true;

            for (int i = 1; i < items.length - j; i++) {
                if ((items[i] != null) && (items[i].getIndex() < items[i - 1].getIndex())) {
                    T tmpItem = items[i];

                    items[i]     = items[i - 1];
                    items[i - 1] = tmpItem;
                    isSorted     = false;
                }
            }

            if (isSorted) {
                break;
            }
        }
    }

    protected abstract boolean requiresUpdate(T entity);

    protected abstract void update(T entity);

    protected abstract void skip(int amount);

    protected abstract void load();
}
