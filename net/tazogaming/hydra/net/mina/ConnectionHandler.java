package net.tazogaming.hydra.net.mina;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.PacketQueue;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.runtime.GameEngine;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

/**
 * Handles the protocol events fired from MINA.
 */
public class ConnectionHandler extends IoHandlerAdapter {
    public static final int SILENT_PACKETS[] = {
        29, 246, 12, 30, 26, 23, 62, 59, 75
    };

    /**
     * The game engine
     */
    private GameEngine engine;

    /**
     * A reference to the game engine's packet queue
     */
    private PacketQueue packets;

    /**
     * Creates a new connection handler for the given engine.
     *
     * @param engine The engine in use
     */
    public ConnectionHandler(GameEngine engine) {
        this.engine = engine;

        // packets     = engine.getPacketQueue();
    }

    /**
     * Invoked whenever an exception is thrown by MINA or this IoHandler.
     *
     * @param session The associated session
     * @param cause   The exception thrown
     */
    public void exceptionCaught(IoSession session, Throwable cause) {

        // Player p = (Player) session.getAttachment();
        // Logger.err(cause);
        cause.printStackTrace();
    }

    /**
     * Invoked whenever a packet is ready to be added to the queue.
     *
     * @param session The IO session on which the packet was received
     * @param message The packet
     */
    public void messageReceived(IoSession session, Object message) {
        Packet p   = (Packet) message;
        Player pla = (Player) session.getAttachment();

        pla.setLastPacket(Core.currentTimeMillis());
        for (int i = 0; i < SILENT_PACKETS.length; i++) {
            if (p.getId() == SILENT_PACKETS[i]) {
                return;
            }
        }

      //  if (!pla.hasPacket(p)) {
            pla.registerPacket(p);
       // }
    }

    /**
     * Invoked whenever a packet is sent.
     *
     * @param session The associated session
     * @param message The packet sent
     */
    public void messageSent(IoSession session, Object message) {}

    /**
     * Method sessionClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param session
     */
    public void sessionClosed(IoSession session) {
        Player p = (Player) session.getAttachment();

        p.setDisconTime(System.currentTimeMillis());
        p.onDisconnect();
    }

    /**
     * Invoked whenever an IO session is created.
     *
     * @param session The session opened
     */
    public void sessionCreated(IoSession session) {}

    /**
     * Invoked when the idle status of a session changes.
     *
     * @param session The session in question
     * @param status  The new idle status
     */
    public void sessionIdle(IoSession session, IdleStatus status) {
        Player p = (Player) session.getAttachment();

        // session.close();
    }

    /**
     * Invoked when a new session is opened.
     *
     * @param session The session opened
     */
    public void sessionOpened(IoSession session) {
      /*  Player p = new Player(session);

        session.setAttachment(p);
        session.getFilterChain().addLast("protocolFilter", new ProtocolCodecFilter(new LoginCodecFactory()));
        session.getConfig().setIdleTime(IdleStatus.READER_IDLE, 8);
        session.getConfig().setIdleTime(IdleStatus.WRITER_IDLE, 8);
        */
    }
}
