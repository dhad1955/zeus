package net.tazogaming.hydra.net.mina.codec;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.util.WorldList;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

/**
 * A decoder for the RSC protocol. Parses the incoming data from an
 * IoSession and outputs it as a <code>Packet</code> object.
 */
public class RS2ProtocolDecoder extends CumulativeProtocolDecoder {

    /**
     * Parses the data in the provided byte buffer and writes it to
     * <code>out</code> as a <code>Packet</code>.
     *
     * @param session The IoSession the data was read from
     * @param in          The buffer
     * @param out        The decoder output stream to which to write the <code>Packet</code>
     * @return Whether enough data was available to create a packet
     */
    public static final boolean DEBUG_PACKETS = true;
    private boolean             prioritySet   = false;
    private int[]               packetLengths;

    /**
     * Constructs ...
     *
     *
     * @param plen
     */
    public RS2ProtocolDecoder(int[] plen) {
        this.packetLengths = plen;
    }

    protected boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        if (!prioritySet) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            prioritySet = true;
        }

        synchronized (session) {
            Cryption inCipher = (Cryption) session.getAttribute("CRYPTION_IN");
            int      opcode   = (Integer) session.getAttribute("opcode", -1);
            int      size     = (Integer) session.getAttribute("size", -1);

            if (opcode == -1) {
                if (in.remaining() >= 1) {
                    opcode = in.get() & 0xFF;    // & 0xFF;

                    if (opcode == 84) {
                        session.write(WorldList.getData(true, true));
                    }

                    size = packetLengths[opcode];
                    session.setAttribute("opcode", opcode);
                    session.setAttribute("size", size);
                } else {
                    return false;
                }
            }

            if ((size <= -1) && (size != -3)) {
                if (in.remaining() >= 1) {
                    size = in.get() & 0xFF;
                    session.setAttribute("size", size);
                } else {
                    return false;
                }
            }

            if (size == -3) {
                return true;
            }

            if (in.remaining() >= size) {
                byte[] data = new byte[size];

                in.get(data);

                IoBuffer payload = IoBuffer.allocate(data.length);

                payload.put(data);
                payload.flip();
                //out.write(new Packet(session, opcode, payload.array()));
                session.setAttribute("lv", "" + opcode);
                session.setAttribute("opcode", -1);
                session.setAttribute("size", -1);

                return true;
            }

            return false;
        }
    }

    /*
     * public boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) {
     *       try {
     *       if (in.remaining() >= 1){
     *
     *
     *           int id = in.get() & 0xff;
     *
     *           int key = 0;
     *           Cryption c = null;
     *                           if((c = (Cryption)session.getAttribute("CRYPTION_IN")) != null) {
     *
     *
     *               int originalId = id;
     *               if(session.getAttribute("LAST_KEY") == null)
     *                    key = c.getNextKey();
     *               else {
     *                   key = Integer.parseInt(session.getAttribute("LAST_KEY").toString());
     *                   session.setAttribute("LAST_KEY", null);
     *                  // Logger.err("Decrypted from last key: "+key+" id w/o key: "+(id - c.getNextKey() & 0xff));
     *               }
     *               id = id - key & 0xff;
     *
     *               if(id == 0)
     *                   return true;
     *
     *               session.setAttribute("LAST_PACKET_ID", id);
     *               // **** ("decrypted "+originalId+"=>"+id);
     *                           } else {
     *                           }
     *           int len = packetLengths[id];
     *       ;
     *           if(len == -1) { //variable length sucka.
     *                                   if(in.remaining() >= 1) {
     *                                           len = in.get() & 0xff;
     *               } else {
     *                   in.rewind();
     *
     *                   session.resumeRead();
     *                   return false;
     *                                   }
     *                           } else{
     *           }
     *                           if(len < 0) {
     *                                   Logger.err("Negative length. uh-oh. stream is gonna fail.");
     *               session.resumeRead();
     *               return true;
     *                           }
     *
     *           if (in.remaining() >= len) {
     *
     *
     *               byte[] payload = new byte[len];
     *               in.get(payload);
     *
     *               Packet p;
     *               p = new Packet(session, id, payload);
     *               out.write(p);
     *
     *               session.resumeRead();
     *                                   return true;
     *                           } else {
     *               in.rewind();
     *
     *               session.setAttribute("LAST_KEY", key);
     *               session.resumeRead();
     *               return false;
     *                           }
     *                   }else{
     *       }
     *           } catch(Exception e) {
     *                   Logger.err(e);
     *           }
     *   session.resumeRead();
     *   return false;
     *   }
     */

    /**
     * Releases the buffer used by the given session.
     *
     * @param session The session for which to release the buffer
     * @throws Exception if failed to dispose all resources
     */
    public void dispose(IoSession session) throws Exception {
        super.dispose(session);
    }
}
