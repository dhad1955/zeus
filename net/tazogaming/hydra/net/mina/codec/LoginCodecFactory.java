package net.tazogaming.hydra.net.mina.codec;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

/**
 * Provides access to the protocol encoders and decoders
 * for the RSC protocol.
 */
public class LoginCodecFactory implements ProtocolCodecFactory {

    /**
     * The protocol encoder in use
     */
    private static ProtocolEncoder encoder = new RS2ProtocolEncoder(null);

    /**
     * The protocol decoder in use
     */
    private static ProtocolDecoder decoder = new RS2LoginProtocolDecoder();

    /**
     * Provides the encoder to use to parse incoming data.
     *
     *
     * @param ioe
     * @return A protocol encoder
     */
    public ProtocolEncoder getEncoder(IoSession ioe) {
        return encoder;
    }

    /**
     * Provides the decoder to use to format outgoing data.
     *
     *
     * @param ioe
     * @return A protocol decoder
     */
    public ProtocolDecoder getDecoder(IoSession ioe) {
        return decoder;
    }
}
