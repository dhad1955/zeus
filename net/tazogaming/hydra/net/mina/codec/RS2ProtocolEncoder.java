package net.tazogaming.hydra.net.mina.codec;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.Command;
import net.tazogaming.hydra.util.Logger;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolEncoder;
import org.apache.mina.filter.codec.ProtocolEncoderOutput;

/**
 * Encodes the high level <code>Packet</code> class into the proper
 * protocol data required for transmission.
 */
public class RS2ProtocolEncoder implements ProtocolEncoder {
    private int[] packetLengths;

    /**
     * Constructs ...
     *
     *
     * @param plen
     */
    public RS2ProtocolEncoder(int[] plen) {
        this.packetLengths = plen;
    }

    /**
     * Converts a <code>Packet</code> object into the raw data needed
     * for transmission.
     *
     * @param session The IO session associated with the packet
     * @param message A <code>Packet</code> to encode
     * @param out        The output stream to which to write the data
     */
    public void encode(IoSession session, Object message, ProtocolEncoderOutput out) {
        Packet p = (Packet) message;

        if(Command.WRITE_STOP)
            return;

        try {
            byte[]   data       = p.getData();
            int      dataLength = p.getLength();


            if(p.getLength() > 2048)
                return;

            IoBuffer buffer;

            if (!p.isBare()) {
                buffer = IoBuffer.allocate(dataLength + 3);


                int id          = p.getId();
                int encryptedId = id;

                if (encryptedId < 128) {
                    buffer.put((byte) encryptedId);
                } else {
                    buffer.put((byte) ((encryptedId >> 8) + 128));
                    buffer.put((byte) (encryptedId & 0xFF));
                }

                if (p.getSize() != Packet.Size.Fixed) {    // variable length
                    if (p.getSize() == Packet.Size.VariableByte) {
                        if (dataLength > 255) {            // trying to send more data then we can represent with 8 bits!
                            throw new IllegalArgumentException("Tried to send packet length " + dataLength
                                                               + " in 8 bits [pid=" + p.getId() + "]");
                        }

                        buffer.put((byte) dataLength);
                    } else if (p.getSize() == Packet.Size.VariableShort) {
                        if (dataLength > 2048) {
                            throw new IllegalArgumentException("Tried to send packet length to big: " + id);
                        }

                        buffer.put((byte) (dataLength >> 8));
                        buffer.put((byte) dataLength);
                    }
                }
            } else {
                buffer = IoBuffer.allocate(dataLength);
            }

            buffer.put(data, 0, dataLength);
            out.write(buffer.flip());
        } catch (Exception e) {
            Logger.err("Error handling message: " + p);
            Logger.err(e);
        }
    }

    /**
     * Releases all resources used by this encoder.
     *
     * @param session The IO session
     */
    public void dispose(IoSession session) {}
}
