package net.tazogaming.hydra.net.mina.codec;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/07/14
 * Time: 17:11
 */
public class LobbyCodecFactory implements ProtocolCodecFactory {
    private ProtocolDecoder silentDecoder = new ProtocolDecoder() {
        @Override
        public void decode(IoSession ioSession, IoBuffer ioBuffer, ProtocolDecoderOutput protocolDecoderOutput)
                throws Exception {
            ioBuffer.clear();
        }
        @Override
        public void finishDecode(IoSession ioSession, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {}
        @Override
        public void dispose(IoSession ioSession) throws Exception {}
    };
    private ProtocolEncoder silentEncoder = new ProtocolEncoder() {
        @Override
        public void encode(IoSession ioSession, Object o, ProtocolEncoderOutput protocolEncoderOutput)
                throws Exception {}
        @Override
        public void dispose(IoSession ioSession) throws Exception {}
    };

    /**
     * Method getEncoder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ioSession
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
        return silentEncoder;
    }

    /**
     * Method getDecoder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ioSession
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
        return silentDecoder;
    }
}
