package net.tazogaming.hydra.net.mina.codec;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

/**
 * Provides access to the protocol encoders and decoders
 * for the RS2 protocol
 */
public class RS2CodecFactory implements ProtocolCodecFactory {
    public static final int[] packetSizes = {
        0, 0, 0, 0, 6, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, -2, 4, 3,
        0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 6, 0, 0, 9, 0, 0, -2, 0, 0, 0, 0, 0, 0, -2, 1, 0, 0, 2, -2, 0, 0, 0, 0, 6, 3, 2,
        4, 2, 4, 0, 0, 0, 4, 0, -2, 0, 0, 7, 2, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 1, 0, 2, 0, 0, -1, 4, 1, 0, 0, 0,
        1, 0, 0, 0, 2, 0, 0, 15, 0, 0, 0, 4, 4, 0, 0, 0, -2, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0,
        14, 0, 0, 0, 4, 0, 0, 0, 0, 3, 0, 0, 0, 4, 0, 0, 0, 2, 0, 6, 0, 0, 0, 0, 3, 0, 0, 5, 0, 10, 6, 0, 0, 0, 0, 0, 0,
        0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 3, 0, 2, 0, 0, 0, 0, 0, -2, 7, 0, 0, 2, 0, 0,
        1, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, -2, 0, 0, 0, 0, 6, 0, 4, 3, 0, 0, 0, -1, 6, 0, 0
    };

    /**
     * The protocol encoder in use
     */
    private static ProtocolEncoder encoder;

    /**
     * The protocol decoder in use
     */
    private static ProtocolDecoder decoder;

    // { 0, 86, 121, 3, 241, 77, 226, 78, 246},
    static {
        int[] packetLengthsIn = new int[257];
        int[] leng            = new int[500];

        leng[59]  = 3;
        leng[33]  = 3;
        leng[169] = 3;
        leng[40]  = 3;
        leng[151] = -1;
        leng[196] = 4;
        leng[2]   = 3;
        leng[217] = 6;
        leng[71]  = 0;
        leng[94]  = 1;
        leng[168] = 16;
        leng[212] = -1;    // contains string
        leng[155] = -1;
        leng[234] = -1;
        leng[224] = -2;    // cotnains string, either -2 or -1
        leng[211] = -2;    // containz stirng
        leng[251] = 18;    // woah wtf is that
        leng[41]  = 4;
        leng[6]   = 4;
        leng[74]  = 0;
        leng[166] = 2;
        leng[9]   = 8;
        leng[231] = 8;
        leng[246] = 8;
        leng[5]   = 8;
        leng[153] = 8;
        leng[144] = 8;
        leng[145] = 8;
        leng[233] = 4;
        leng[88]  = 8;
        leng[60]  = 8;
        leng[13]  = -1;
        leng[104] = 8;
        leng[107] = 1;
        leng[196] = 4;
        leng[151] = -2;    // string but also sends the length of the string? wierd ?
        leng[86]  = -2;    // same as above, must be the same thing just a different option
        leng[125] = 2;
        leng[81]  = 7;     // think it may be examine but not sure
        leng[172] = 3;     // think its something to do with players, short for player id then 1,0 flag?
        leng[100] = 3;     // no idea its 3 bytes long though 1 short and 1 bytea
        leng[218] = 2;
        leng[53]  = 7;     // hmm ?
        leng[32]  = 6;     // no idea, possibly items
        leng[156] = 4;     // something to do with npcs possibly ?
        leng[103] = 7;
        leng[69]  = 3;     // hm, also relates to npcs?
        leng[31]  = 2;     // something simple
        leng[52]  = 7;     // not a clue
        leng[2]   = 3;     // no idea at all
        leng[169] = 3;
        leng[134] = 9;
        leng[11]  = 7;     // another big one
        leng[121] = 3;
        leng[226] = -1;
        leng[51]  = 7;     // another 7, must be related to the other 7's
        leng[192] = 12;    // huge
        leng[229] = 10;
        leng[114] = 7;
        leng[135] = 7;
        leng[132] = 10;
        leng[193] = 9;
        leng[141] = 7;
        leng[234] = -1;
        leng[129] = -1;
        leng[155] = -1;
        leng[40]  = 3;
        leng[45]  = 1;
        leng[248] = 3;
        leng[216] = 3;
        leng[59]  = 3;
        leng[160] = 3;
        leng[174] = 3;
        leng[33]  = 3;
        leng[167] = 2;
        leng[61]  = -1;
        leng[79] = -1;
        leng[209] = -1;




    }

    public static final int[] PACKET_SIZES = new int[] {
            8, -1, -1, 16, 6, 2, 8, 6, 3, -1, 16, 15, 0, 8, 11, 8, -1, -1, 3, 2, -1, -1, 7, 2, -1, 7, -1, 3, 3, 6, 4, 3,
            0, 3, 4, 5, -1, -1, 7, 8, 4, -1, 4, 7, 3, 15, 8, 3, 2, 4, 18, -1, 1, 3, 7, 7, 4, -1, 8, 2, 7, -1, 1, -1, 3,
            2, -1, 8, 3, 2, 3, 7, 3, 8, -1, 0, 7, -1, 11, -1, 3, 7, 8, 12, 4, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
            -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
            -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
            -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
            -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
            -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3,
            -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3
    };

    /**
     * Provides the encoder to use to parse incoming data.
     *
     *
     * @param e
     * @return A protocol encoder
     */
    public ProtocolEncoder getEncoder(IoSession e) {
        return encoder;
    }

    /**
     * Provides the decoder to use to format outgoing data.
     *
     *
     * @param e
     * @return A protocol decoder
     */
    public ProtocolDecoder getDecoder(IoSession e) {
        return decoder;
    }
}
