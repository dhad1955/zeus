package net.tazogaming.hydra.net.mina.codec;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;

//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.util.LinkedList;
//import java.util.Queue;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.security.BanList;
import net.tazogaming.hydra.security.XTEA;
import net.tazogaming.hydra.util.Logger;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

/**
 * Login protocol decoder.
 *
 * @author Graham
 */
public class RS2LoginProtocolDecoder extends CumulativeProtocolDecoder {
    public static final int[] uKeys = {
        0xff, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0xe8, 0x3f, 0x47, 0x6f, 0x2a, 0x00, 0x00, 0x01, 0x97, 0x09, 0xdd,
        0x3d, 0x8d, 0x00, 0x00, 0x00, 0xf6, 0x2e, 0x65, 0x1a, 0x4b, 0x00, 0x00, 0x01, 0x76, 0x17, 0x6c, 0x6f, 0xf3,
        0x00, 0x00, 0x02, 0xf7, 0x20, 0x94, 0x65, 0x4f, 0x00, 0x00, 0x00, 0xda, 0x02, 0xc1, 0x84, 0xd9, 0x00, 0x00,
        0x03, 0x06, 0x43, 0xcb, 0x98, 0x8e, 0x00, 0x00, 0x00, 0x37, 0xb5, 0x4d, 0xb1, 0x08, 0x00, 0x00, 0x04, 0x03,
        0x12, 0x54, 0xbb, 0xc5, 0x00, 0x00, 0x00, 0xef, 0xec, 0x96, 0x39, 0x4f, 0x00, 0x00, 0x01, 0x0a, 0x29, 0x76,
        0x27, 0x6a, 0x00, 0x00, 0x00, 0x05, 0x1c, 0xce, 0xf9, 0x27, 0x00, 0x00, 0x00, 0x20, 0xa3, 0x95, 0x74, 0x7b,
        0x00, 0x00, 0x03, 0x37, 0x0f, 0x29, 0xa0, 0x81, 0x00, 0x00, 0x00, 0x21, 0xdc, 0x6e, 0xc6, 0x2d, 0x00, 0x00,
        0x00, 0x1b, 0x17, 0x30, 0xff, 0xae, 0x00, 0x00, 0x00, 0x06, 0xb7, 0xac, 0xbb, 0x2a, 0x00, 0x00, 0x02, 0x8c,
        0x05, 0x96, 0x2e, 0xec, 0x00, 0x00, 0x01, 0x37, 0xcb, 0x9e, 0x64, 0x80, 0x00, 0x00, 0x01, 0x8d, 0xbe, 0xee,
        0x69, 0xc3, 0x00, 0x00, 0x01, 0x3c, 0x0e, 0xd2, 0xff, 0xf9, 0x00, 0x00, 0x05, 0x18, 0x3f, 0x5b, 0x5d, 0x9c,
        0x00, 0x00, 0x00, 0x7d, 0x23, 0x26, 0xf1, 0x63, 0x00, 0x00, 0x00, 0x96, 0x76, 0x12, 0x0f, 0xf4, 0x00, 0x00,
        0x00, 0xa0, 0xff, 0x16, 0x20, 0x63, 0x00, 0x00, 0x00, 0x37, 0x53, 0xcc, 0xe6, 0xcc, 0x00, 0x00, 0x00, 0x05,
        0x7e, 0x88, 0xad, 0xd4, 0x00, 0x00, 0x00, 0x7b, 0xf6, 0x5b, 0x23, 0x76, 0x00, 0x00, 0x00, 0x05, 0xf7, 0xa9,
        0xfd, 0x01, 0x00, 0x00, 0x00, 0x03,
    };
    public static final int
        LOGIN_HANDSHAKE                            = 0,
        LOGIN_PRE_STAGE                            = 1,
        LOGIN                                      = 2,
        LOGIN_LOBBY_REQUEST                        = 3;
    public final static byte MAKE_ACC              = 5;
    public final static byte REMOVE_ID             = 100;
    public static final byte LOGIN_OK              = 2;
    public static final byte INVALID_PASSWORD      = 3;
    public static final byte BANNED                = 4;
    public static final byte ALREADY_ONLINE        = 5;
    public static final byte WORLD_FULL            = 7;
    public static final byte TRY_AGAIN             = 11;
    public static final byte ERROR_LOADING_PROFILE = 24;




    private void sendBadLogin(IoSession sesh, int code) {
        sesh.write(new MessageBuilder().setBare(true).addByte((byte) code).toPacket()).addListener(
            new org.apache.mina.core.future.IoFutureListener<org.apache.mina.core.future.IoFuture>() {
            @Override
            public void operationComplete(org.apache.mina.core.future.IoFuture ioFuture) {
                ioFuture.getSession().close();
            }
        });
    }

    /**
     * Method doDecode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param session
     * @param in
     * @param out
     *
     * @return
     */
    @Override
    public boolean doDecode(IoSession session, IoBuffer in, ProtocolDecoderOutput out) {
        try {
            Object loginStageObj = session.getAttribute("LOGIN_STAGE");
            int    loginStage    = 0;

            if (loginStageObj != null) {
                loginStage = (Integer) loginStageObj;
            }

            switch (loginStage) {
            case LOGIN_LOBBY_REQUEST :
                int siz = (Integer) session.getAttribute("packet_size") - 4;

                if (siz > in.remaining()) {
                    in.rewind();

                    return false;
                }

                in.skip(3);

                int[] keys2 = new int[4];

                for (int i = 0; i < 4; i++) {
                    keys2[i] = in.getInt();
                }

                in.skip(8);

                byte[] buffer = new byte[siz - (3 + 16 + 8)];
                Packet p2     = new Packet(buffer);

                in.get(buffer);

                String lobbyPass = p2.readRS2String();

                p2.skip(16);

                byte[] block2 = p2.getRemainingData();

                XTEA.decrypt(keys2, block2, 0, block2.length);
                p2 = new Packet(block2);

                String lobbyUser = p2.readRS2String();
                Player player    = new Player(null);
                player.setCredentials(lobbyUser, lobbyPass, false);

                session.setAttachment(player);
               if(!session.getFilterChain().contains("protocolFilter"))
                session.getFilterChain().addLast("protocolFilter", new ProtocolCodecFilter(new RS2CodecFactory()));

                // session.write(WorldList.getData(true, true));
                World.getWorld().getIOPool().getLoader().addLobbyLoad(player);

                break;

            case LOGIN_HANDSHAKE :
                int protocol = in.get() & 0xFF;

                if (protocol != 14) {
                    session.close();
                    Logger.err("wrong protocol: " + protocol);
                    sendBadLogin(session, ERROR_LOADING_PROFILE);

                    return true;
                }

                session.setAttribute("LOGIN_STAGE", LOGIN_PRE_STAGE);
                session.write(new MessageBuilder().setBare(true).addByte((byte) 0).toPacket());

                return false;

            case LOGIN_PRE_STAGE :    // first login packets
                if (in.remaining() >= 7) {
                    int loginType       = in.get() & 0xFF;
                    int loginPacketSize = in.getShort() & 0xFFFF;
                    int version         = in.getInt();

                    session.setAttribute("login_type", loginType);
                    session.setAttribute("packet_size", loginPacketSize);

                    /*
                     *    if (protocolId != 14) {
                     *      Logger.err("Error invalid protocol id: " + protocolId + " closing session");
                     *      session.close();
                     *
                     *      return true;
                     *  }
                     */
                    long serverSessionKey = ((long) (java.lang.Math.random() * 99999999D) << 32)
                                            + (long) (java.lang.Math.random() * 99999999D);
                    MessageBuilder s1Response = new MessageBuilder();

                    s1Response.setBare(true).addByte((byte) 0).addLong(serverSessionKey);
                    session.setAttribute("SERVER_SESSION_KEY", serverSessionKey);

                    //session.write(s1Response.toPacket());
                    if ((loginType == 18) || (loginType == 16)) {
                        session.setAttribute("LOGIN_STAGE", LOGIN);
                    } else {
                        session.setAttribute("LOGIN_STAGE", LOGIN_LOBBY_REQUEST);
                    }

                    return true;
                } else {
                    in.rewind();

                    return false;
                }
            case LOGIN :    // here's where we get the username and password
                boolean                         hd              = false;
                @SuppressWarnings("unused") int loginType       = (Integer) session.getAttribute("login_type");
                int                             loginPacketSize = (Integer) session.getAttribute("packet_size") - 4;

                if (loginPacketSize > in.remaining()) {
                    in.rewind();

                    return false;
                }

                if (loginPacketSize <= in.remaining()) {
                    byte[] payload = new byte[loginPacketSize];

                    in.get(payload);

                    Packet                          p                      = new Packet(null, -1, payload);
                    @SuppressWarnings("unused") int loginEncryptPacketSize = loginPacketSize - (36 + 1 + 1 + 2);    // can't be negative

                    p.readShort();    // no idea

                    int rsaHeader = p.readByte();

                    if (rsaHeader != 10) {
                        Logger.err("invalid rsa header: " + rsaHeader);
                    }

                    int[] keys = new int[4];

                    for (int i = 0; i < keys.length; i++) {
                        keys[i] = p.readInt();
                    }

                    long UID = p.readLong();


                    String password = p.readRS2String();

                    p.readLong();
                    p.readLong();

                    byte[] block = p.getRemainingData();

                    XTEA.decrypt(keys, block, 0, block.length);
                    p = new Packet(block);

                    String username = p.readRS2String();

                    p.readByte();

                    int displayMode = p.readByte();
                    int screenSizeX = p.readShort();
                    int screenSizeY = p.readShort();

                    p.readByte();
                    p.skip(24);
                  //  player = new Player(session);
                    //session.setAttachment(player);
                    //player.setUID(UID);
                    //player.setCredentials(username, password, displayMode != 1);
                    //World.getWorld().getIOPool().getLoader().addLoad(player);
                    session.getFilterChain().remove("protocolFilter");
                    session.getFilterChain().addLast("protocolFilter", new ProtocolCodecFilter(new RS2CodecFactory()));

                    return true;
                } else {
                    in.rewind();

                    return false;
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
            // logger.stackTrace(e);
        }

        return false;
    }

//  private ByteBuffer getFile(int requestCache, int requestId) throws IOException {
//          ByteBuffer buffer = ByteBuffer.allocate(520);
//          buffer.put((byte) requestCache);
//          buffer.putShort((short) requestId);
//          byte[] cache = CACHE.read(CACHE.index(requestCache, requestId));
//          int len = (((cache[1] & 0xff) << 24)+((cache[2] & 0xff) << 16)+((cache[3] & 0xff) << 8)+(cache[4] & 0xff)) + 9;
//          if(cache[0] == 0) {
//                  len -= 4;
//          }
//          int c = 3;
//          for(int i = 0; i < len; i++) {
//                  if(c == 512) {
//                          buffer.put((byte) 0xFF);
//                          c = 1;
//                  }
//                  buffer.put(cache[i]);
//          }
//          return buffer.flip();
//  }

    /**
     * Releases the buffer used by the given session.
     *
     * @param session The session for which to release the buffer
     * @throws Exception if failed to dispose all resources
     */
    @Override
    public void dispose(IoSession session) throws Exception {
        super.dispose(session);
    }
}
