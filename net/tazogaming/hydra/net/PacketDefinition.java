package net.tazogaming.hydra.net;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 09:51
 */
public class PacketDefinition {
    private int[]                bindIds;
    private Class<PacketHandler> packet;

    /**
     * Constructs ...
     *
     *
     * @param packet
     * @param bindIds
     */
    public PacketDefinition(Class<PacketHandler> packet, int... bindIds) {
        this.bindIds = bindIds;
        this.packet  = packet;
    }

    /**
     * Method getBinds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getBinds() {
        return this.bindIds;
    }

    /**
     * Method getPacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public PacketHandler getPacket() throws IllegalAccessException, InstantiationException {
        return packet.newInstance();
    }
}
