package net.tazogaming.hydra.net.mysql;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 04:38
 */
public class MysqlInsert {
    private String begginingString;
    private String valueString;

    /**
     * Constructs a new Mysql Insert query
     * @param tablename the table name to insert to
     */
    public MysqlInsert(String tablename) {
        begginingString = "INSERT into " + tablename + " (";
        valueString     = "(";
    }

    /**
     * Sets a specified value
     * @param setting the setting to set
     * @param value the value to set
     * @return this MysqlInsert object
     */
    public MysqlInsert set(String setting, Object value) {
        begginingString += setting + ", ";

        if (value.toString().equalsIgnoreCase("null")) {
            valueString += "" + value.toString() + ",";
        } else {
            valueString += "'" + value.toString() + "',";
        }

        return this;
    }

    /**
     * Returns as string
     * @return  the statement
     */
    public String toString() {
        int lastIndex = begginingString.lastIndexOf(",");

        if (lastIndex != -1) {
            begginingString = begginingString.substring(0, lastIndex);
        }

        lastIndex = valueString.lastIndexOf(",");

        if (lastIndex != -1) {
            valueString = valueString.substring(0, lastIndex);
        }

        return begginingString + ") VALUES " + valueString + ");";
    }
}
