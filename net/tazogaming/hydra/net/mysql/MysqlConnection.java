package net.tazogaming.hydra.net.mysql;

//~--- non-JDK imports --------------------------------------------------------

import com.mysql.jdbc.exceptions.jdbc4.CommunicationsException;

import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.sql.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 00:38
 */
public class MysqlConnection {
    private String     username, password, server, database;
    private Connection dbConnection;

    /**
     * Constructs ...
     *
     *
     * @param server
     * @param username
     * @param password
     * @param database
     */
    public MysqlConnection(String server, String username, String password, String database) {
        this.username = username;
        this.password = password;
        this.server   = server;
        this.database = database;

        // **** ("Db connection loaded");
    }


    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return this.dbConnection.prepareStatement(sql);
    }

    public PreparedStatement prepareStatement(String sql, int code) throws SQLException {
        return this.dbConnection.prepareStatement(sql, code);
    }

    /**
     * Method setDatabase
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void setDatabase(String str) {
        this.database = str;
    }

    /**
     * Method isClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isClosed() {
        try {
            return this.dbConnection.isClosed();
        } catch (Exception er) {
            return false;
        }
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void close() {
        try {
            this.dbConnection.close();
        } catch (Exception eir) {}
    }

    /**
     * Method makeStatement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws SQLException
     */
    public Statement makeStatement() throws SQLException {
        return dbConnection.createStatement();
    }

    /**
     * Method connect
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public MysqlConnection connect() {
        try {
            System.err.println("[Database] connecting to "+server);
            long start = System.currentTimeMillis();

            Class.forName("com.mysql.jdbc.Driver");
            dbConnection = DriverManager.getConnection("jdbc:mysql://" + server + ":3306/" + database + "?user="
                    + username + "&password=" + password + "&autoReconnect=true");
        } catch (Exception ex) {
            System.err.println("Could not connect to the database: " + ex.getMessage());
            connect();

            try {
                Thread.sleep(4000);
            } catch (Exception ee) {}
        }

        return this;
    }

    /**
     * Method executeUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param query
     */
    public void executeUpdate(String query) {
        try {
            query(query);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    private void resetConnection() {
        Logger.err("Resetting DB Connection");

        try {
            this.dbConnection.close();
            Logger.err("DB Connection close");
        } catch (Exception ee) {
            Logger.err("Failed to close connection");
        }

        try {
            Logger.err("Starting new connection");
            this.connect();
            Logger.err("New connection loaded");
        } catch (Exception ee) {
            Logger.err("Failed to reconnect");
            ee.printStackTrace();
        }
    }

    /*
     * Won't stop giving up until connection has been re-established
     * This will block any threads.
     */

    /**
     * Method query
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param query
     *
     * @return
     */
    public ResultSet query(String query) {
        try {
            java.sql.Statement stm = dbConnection.createStatement();


            if (!query.toLowerCase().startsWith("select")) {
                try {
                    stm.executeUpdate(query);
                    stm.close();
                } catch (CommunicationsException ee) {
                    resetConnection();

                    return query(query);
                }
            } else {
                try {
                    ResultSet r = stm.executeQuery(query);

                    return r;
                } catch (CommunicationsException ee) {
                    resetConnection();

                    return query(query);
                }
            }
        } catch (Exception ee) {

            ee.printStackTrace();
           System.err.println("************************ MYSQL ERROR ***************************");
           System.err.println("** ERROR MESSAGE: "+ee.getClass().getName()+": "+ee.getMessage());
           System.err.println(query);

           System.err.println("****************************************************************");

        }

        return null;
    }
}
