package net.tazogaming.hydra.net.server.codec;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.Config;

import org.apache.mina.core.buffer.IoBuffer;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.CumulativeProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolDecoderOutput;

//~--- JDK imports ------------------------------------------------------------

import java.nio.charset.Charset;

import java.util.Arrays;
import java.util.Properties;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/12/13
 * Time: 09:03
 */
public class DevProtocolDecoder extends CumulativeProtocolDecoder {
    public static final int   ACCESS_CODE[] = { 123, 5, 9 };
    private static Properties push_info     = Config.getServerInfo();

    /**
     * Method doDecode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ioSession
     * @param in
     * @param out
     *
     * @return
     *
     * @throws Exception
     */
    @Override
    public boolean doDecode(IoSession ioSession, IoBuffer in, ProtocolDecoderOutput out) throws Exception {
        int   pos         = in.position();
        int[] access_code = { Byte.parseByte(push_info.getProperty("magic_byte_1")),
                              Byte.parseByte(push_info.getProperty("magic_byte_2")),
                              Byte.parseByte(push_info.getProperty("magic_byte_3")) };

        if (!Arrays.equals(new int[] { in.get(), in.get(), in.get() }, access_code)) {
            System.out.println("Error invalid access code: " + Arrays.toString(access_code));
            ioSession.close();

            return true;
        }

        int    file_size   = in.getInt();
        String file_path   = in.getPrefixedString(Charset.forName("utf-8").newDecoder());
        String patch_notes = in.getPrefixedString(Charset.forName("utf-8").newDecoder());
        String username    = in.getPrefixedString(Charset.forName("utf-8").newDecoder());

        if ((in.remaining() > 0) && (in.remaining() < file_size)) {
            in.position(pos);

            return false;
        }

        byte[] file_buffer = new byte[file_size];

        in.get(file_buffer, 0, file_size);

        FileRequest request = new FileRequest(file_path, username, patch_notes, file_buffer, file_size);

        out.write(request);
        ioSession.close();

        return true;
    }

    /**
     * Method finishDecode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ioSession
     * @param protocolDecoderOutput
     *
     * @throws Exception
     */
    @Override
    public void finishDecode(IoSession ioSession, ProtocolDecoderOutput protocolDecoderOutput) throws Exception {}

    /**
     * Method dispose
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ioSession
     *
     * @throws Exception
     */
    @Override
    public void dispose(IoSession ioSession) throws Exception {}
}
