package net.tazogaming.hydra.net.server.client;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

import org.apache.mina.core.buffer.IoBuffer;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.OutputStream;

import java.net.Socket;

import java.nio.charset.Charset;

import java.util.Properties;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/12/13
 * Time: 09:22
 */
public class FilePoster extends BackgroundServiceRequest {
    private static Properties push_info = Config.getDevClientConfig();
    private Player            player;
    private String            filePath;
    private String            patchNotes;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param filePath
     * @param patchNotes
     */
    public FilePoster(Player player, String filePath, String patchNotes) {
        this.player     = player;
        this.filePath   = filePath;
        this.patchNotes = patchNotes;
    }

    /**
     * Method compute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean compute() {
        try {
            File file = new File("config/" + this.filePath);

            if (!file.exists()) {
                player.getActionSender().sendMessage("error: " + file.getPath() + " does not exist.");

                return true;
            }

            Buffer data = new Buffer(file);

            player.getActionSender().sendMessage("Loading file: " + file.getPath());

            if (push_info == null) {
                throw new NullPointerException("error no config loaded");
            }

            int    port   = Integer.parseInt(push_info.getProperty("push_server_port"));
            Socket socket = new Socket(push_info.getProperty("push_server_addr"),
                                       Integer.parseInt(push_info.getProperty("push_server_port")));

            if (socket.isConnected()) {
                IoBuffer to_send = IoBuffer.allocate(file.getPath().length() + data.getBuffer().length + 1);

                to_send.setAutoExpand(true);
                to_send.put(new byte[] { Byte.parseByte(push_info.getProperty("magic_byte_1")),
                                         Byte.parseByte(push_info.getProperty("magic_byte_2")),
                                         Byte.parseByte(push_info.getProperty("magic_byte_3")) });
                to_send.putInt(data.getBuffer().length);
                to_send.putPrefixedString(this.filePath, Charset.forName("utf-8").newEncoder());
                to_send.putPrefixedString(this.patchNotes, Charset.forName("utf-8").newEncoder());
                to_send.putPrefixedString(this.player.getUsername(), Charset.forName("utf-8").newEncoder());
                to_send.put(data.getBuffer(), 0, data.getBuffer().length);
                to_send.position(0);

                byte[] encoded_data = new byte[to_send.remaining()];

                to_send.get(encoded_data);

                OutputStream out = socket.getOutputStream();

                out.write(encoded_data);
                out.flush();
                socket.close();
                player.getActionSender().sendMessage("Data was pushed to main server");
            }
        } catch (Exception ee) {
            player.getActionSender().sendMessage("Something went wrong");
            ee.printStackTrace();
        }
        return true;
    }
}
