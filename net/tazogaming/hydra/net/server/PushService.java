package net.tazogaming.hydra.net.server;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Logger;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

import java.net.InetSocketAddress;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/12/13
 * Time: 08:53
 */
public class PushService {
    public static final int   SERVICE_PORT = Integer.parseInt(Config.getServerInfo().getProperty("listen"));
    private InetSocketAddress address      = new InetSocketAddress(SERVICE_PORT);
    private IoAcceptor        acceptor;

    /**
     * Constructs ...
     *
     *
     * @throws IOException
     */
    public PushService() throws IOException {
        acceptor = new NioSocketAcceptor();
        acceptor.setHandler(new PushRequestHandler());
        acceptor.bind(address);
        Logger.log("Push service:, listening on " + SERVICE_PORT);
    }
}
