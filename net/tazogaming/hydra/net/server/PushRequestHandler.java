package net.tazogaming.hydra.net.server;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.net.server.codec.DevProtocolDecoder;
import net.tazogaming.hydra.net.server.codec.FileRequest;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/12/13
 * Time: 08:58
 */
public class PushRequestHandler extends IoHandlerAdapter {
    private static final DevProtocolDecoder main_decoder = new DevProtocolDecoder();

    private static String format(FileRequest request) {
        DateFormat dateFormat = new SimpleDateFormat("MM/dd/yy HH:mm");
        Date       date       = new Date();

        System.out.println(dateFormat.format(date));

        return "[" + request.getUsername() + " " + (dateFormat.format(date)) + "] " + request.getFilePath();
    }

    /**
     * Method messageReceived
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param session
     * @param message
     */
    public void messageReceived(IoSession session, Object message) {
        FileRequest request = (FileRequest) message;
        Buffer      data    = new Buffer(request.length());

        data.writeBytes(request.getData(), request.length(), 0);

        File attempt_file = new File("config/" + request.getFilePath());

        data.setSavePath(attempt_file.getPath());

        // delete any previous files
        String filename = "";
        String path     = request.getFilePath();

        if (path.contains("/")) {
            String[] split = path.split("/");

            filename = split[split.length - 1];
        } else {
            filename = path;
        }

        List<File> files = (List<File>) FileUtils.listFiles(new File("config/"), TrueFileFilter.INSTANCE,
                               TrueFileFilter.INSTANCE);

        for (File f : files) {
            if (f.getName().equalsIgnoreCase(filename)) {
                f.delete();

                break;
            }
        }

        if (!attempt_file.exists()) {
            try {
                data.mkdirs();
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        ArrayList<String> lines = new ArrayList<String>();

        lines.add(format(request));
        lines.add("* " + request.getPatchNotes());
        lines.add("");

        try {
            FileUtils.writeLines(new File("config/patchnotes.txt"), lines, true);
            data.save(attempt_file.getPath());
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        session.close();
    }

    /**
     * Method sessionCreated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param session
     */
    public void sessionCreated(IoSession session) {
        session.getFilterChain().addFirst("decoder", new ProtocolCodecFilter(new ProtocolCodecFactory() {
            @Override
            public ProtocolEncoder getEncoder(IoSession ioSession) throws Exception {
                return null;
            }
            @Override
            public ProtocolDecoder getDecoder(IoSession ioSession) throws Exception {
                return main_decoder;
            }
        }));
    }
}
