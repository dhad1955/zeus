package net.tazogaming.hydra.net;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 04:17
 */
public class PacketThrottleFilter {
    private static int[] UNTHROTTLED_PACKETS = {
        71, 88, 236, 167, 98, 248, 185, 145, 117, 43, 135, 129, 135, 214, 41, 87, 95, 40, 122, 75,
    };
    private int          lastPacketId        = -1;
    private long         lastPacketTime;

    /**
     * Rune War Game Server
     * http://hydra.tazogaming.org
     * Builder {$build}
     * net.tazo_.server.net
     *
     * @author Gander
     *         Date: 13-Sep-2008 {12:09:53}
     *
     * @param id
     *
     * @return
     */
    public boolean packetThrottled(int id) {
        for (int i = 0; i < UNTHROTTLED_PACKETS.length; i++) {
            if (id == UNTHROTTLED_PACKETS[i]) {
                return false;
            }
        }

        return true;
    }

    /**
     * Method isBlocked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean isBlocked(int id) {
        if (!packetThrottled(id)) {
            return false;
        }

        if (lastPacketId == id) {
            if ((System.currentTimeMillis() - lastPacketTime) < 500) {
                return true;
            }
        }

        lastPacketId   = id;
        lastPacketTime = System.currentTimeMillis();

        return false;
    }
}
