package net.tazogaming.hydra.net;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;

/*The entry point for RSC server.*/
import net.tazogaming.hydra.net.mina.codec.RS2CodecFactory;
import net.tazogaming.hydra.net.netty.GameIOHandler;
import net.tazogaming.hydra.net.netty.codec.GameProtocolEncoder;
import net.tazogaming.hydra.net.netty.codec.NettyLoginDecoder;
import net.tazogaming.hydra.runtime.GameEngine;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.*;
import org.jboss.netty.channel.socket.ServerSocketChannelFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

//~--- JDK imports ------------------------------------------------------------

import java.net.InetSocketAddress;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class Server {
    private static ThreadGroup group;
    private static Server      server;
    private GameEngine               engine;
    private ServerBootstrap bootStrap = new ServerBootstrap();
    private ServerSocketChannelFactory factory;
    private ChannelPipelineFactory pipeline;
    private static List<Thread> startedThreads = new ArrayList<Thread>();
    /**
     * Constructs ...
     *
     */
    public Server() {
        if (World.getWorld() == null) {
            throw new IllegalArgumentException("Cannot start server until World has been loaded.");
        }

        server   = this;
        engine   = World.getWorld().getGameEngine();
         }

    /**
     * Method getInstance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Server getInstance() {
        return server;
    }

    /**
     * Method start
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param port
     *
     * @throws Exception
     */
    public void start(int port) throws Exception {
        Executor executor = Executors.newCachedThreadPool();

        this.factory = new NioServerSocketChannelFactory(executor, executor, 4);
        this.bootStrap.setOption("localAddress", new InetSocketAddress(port));
        this.bootStrap.setOption("child.tcpNoDelay", true);
        this.bootStrap.setOption("reuseAddress", true);
        this.bootStrap.setOption("child.keepAlive", true);
        this.bootStrap.setOption("child.TcpAckFrequency", true);
        this.bootStrap.setPipelineFactory(new ChannelPipelineFactory() {
            @Override
            public ChannelPipeline getPipeline() throws Exception {
                ChannelPipeline pipe = new DefaultChannelPipeline();
                pipe.addFirst("decoder", new NettyLoginDecoder());
                pipe.addLast("encoder", new GameProtocolEncoder());
                pipe.addLast("handler", new GameIOHandler());

                return pipe;
            }
        });
        bootStrap.setFactory(factory);
        bootStrap.bind(new InetSocketAddress(port));
        System.err.println("[BOOTSTRAP] Netty service running on port: "+port);
    }

    /**
     * Method newThread
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param r
     * @param name
     * @param priority
     */
    public static void newThread(Runnable r, String name, int priority) {
        Thread t = new Thread(group, r, name);

        t.setPriority(priority);
        t.start();
        startedThreads.add(t);
    }

}
