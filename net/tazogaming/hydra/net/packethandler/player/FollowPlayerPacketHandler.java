package net.tazogaming.hydra.net.packethandler.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 29/09/13
 * Time: 02:08
 * To change this template use File | Settings | File Templates.
 */
public class FollowPlayerPacketHandler implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {
        int    playerIndex = p.readLEShortA();
        Player following   = World.getWorld().getPlayers().get(playerIndex);
        int    shit        = p.readByte() + 128;

        if (following == null) {
            return;
        }

        if (pla.isActionsDisabled() || pla.isDead()) {
            return;
        }

        if (pla.getCombatAdapter().isInCombat()) {
            pla.getCombatAdapter().reset();
        }

        pla.getRequest().close();
        pla.getFocus().focus(following);
        pla.followPlayer(following);
        pla.getActionSender().sendCloseWalkingFlag();
        pla.getRoute().resetPath();
    }
}
