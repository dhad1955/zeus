package net.tazogaming.hydra.net.packethandler.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.PlayerFollowing;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.runtime.Core;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 23:34
 * To change this template use File | Settings | File Templates.
 */
public class AttackPlayer implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.NEXT_TICK;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {
        int    pid = p.readLEShort();
        Player pl2 = World.getWorld().getPlayers().get(pid);

        pla.getRoute().resetPath();
        if (pl2 != null) {
            if (pl2.isActionsDisabled()) {
                return;
            }

            if ((pla.isInZone("duel_lobby") && pl2.isInZone("duel_lobby") &&!pla.isInZone("duel"))
                    || (pla.isInArea(ClanWar.CLAN_WARS_HOME) && pl2.isInArea(ClanWar.CLAN_WARS_HOME))) {
                pla.getFocus().focus(pl2);

                if (Point.getDistance(pla.getLocation(), pl2.getLocation()) > 1) {
                    pla.requestWalk(pl2.getX(), pl2.getY());
                }

                pla.getRequest().setRequest(ActionRequest.TYPE_DUEL_REQUEST, 1, pl2);

                return;
            }

            if (Combat.isWithinDistance(pla, pl2)) {
                pla.getRoute().resetPath();
                pla.getActionSender().sendCloseWalkingFlag();
            }

            pla.getFocus().focus(pl2);
            pla.getRequest().setRequest(ActionRequest.TYPE_PLAYER_ATTACK, -1, pl2);
            pla.followPlayer(pl2);
            pla.getFollowing().setMode(PlayerFollowing.COMBAT_BASED_FOLLOW);
        }
    }
}
