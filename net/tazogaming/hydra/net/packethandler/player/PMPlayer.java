package net.tazogaming.hydra.net.packethandler.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.security.AdminAuthenticator;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/07/14
 * Time: 04:56
 */
public class PMPlayer implements PacketHandler {
    private static int messageCounter = 100;

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {

       Player player = pla;

       if(player.getRights() >= Player.ADMINISTRATOR) {
           if (!AdminAuthenticator.isAuthenticated(player.getUID())) {
               player.getActionSender().sendMessage("You are on a strange machine");
               player.getActionSender().sendMessage("Please authenticate using ::adminauth");
               player.getActionSender().sendMessage("Type: ::adminauth <password>");
               return;
           }
       }
        if(pla.getAccount().isMuted()){
            pla.getActionSender().sendMessage("You are muted.");
            return;
        }
        String msgUsername = p.readRS2String();
         player      = (World.getWorld().getPlayer(Text.stringToLong(msgUsername)));

        if (player == null) {
            return;
        }

        int    numChars = p.readUByte();
        byte[] data     = p.getRemainingData();


        String msg = Text.decompressHuffman(data, numChars);

        pla.log("msg to "+player.getUsername()+" > "+msg);
        player.log("msg from "+pla.getUsername()+" "+msg);

        receivePrivateMessage(player, pla.getUsername(), pla.getUsername(), (pla.getRights() == Player.ADMINISTRATOR)
                ? 2
                : (((pla.getRights() == Player.MODERATOR) || (pla.getRights() == Player.COMMUNITY_MANAGER))
                   ? 1
                   : 0), data, numChars);
        sendPrivateMessage(pla, player.getUsername(), data, numChars);
    }

    /**
     * Method sendPrivateMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param username
     * @param message
     * @param leng
     */
    public static void sendPrivateMessage(Player player, String username, byte[] message, int leng) {
        MessageBuilder bldr = new MessageBuilder(76, Packet.Size.VariableByte);

        bldr.addRS2String(Text.ucFirst(username));
        bldr.addByte(leng);
        bldr.addBytes(message, 0, message.length);
        player.dispatch(bldr);
    }

    /**
     * Method receivePrivateMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param username
     * @param displayName
     * @param rights
     * @param message
     * @param leng
     */
    public static void receivePrivateMessage(Player player, String username, String displayName, int rights,
            byte[] message, int leng) {
        long           id   = (long) (++messageCounter
                                      + ((Math.random() * Long.MAX_VALUE) + (Math.random() * Long.MIN_VALUE)));
        MessageBuilder bldr = new MessageBuilder(30, Packet.Size.VariableByte);

        bldr.addByte(0);    // has a previous name?
        bldr.addRS2String(Text.ucFirst(username));
        bldr.addShort((int) (id >> 32));
        bldr.addMediumInt((int) (id - ((id >> 32) << 32)));
        bldr.addByte(rights);
        bldr.addByte(leng);
        bldr.addBytes(message, 0, message.length);
        player.dispatch(bldr);
    }
}
