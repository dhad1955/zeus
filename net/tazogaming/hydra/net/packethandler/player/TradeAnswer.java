package net.tazogaming.hydra.net.packethandler.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/10/13
 * Time: 04:01
 */
public class TradeAnswer implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {
        int    type   = p.readByte();
        int    pIndex = p.readShort();
        Player pla2   = World.getWorld().getPlayers().get(pIndex);

        pla.getGameFrame().close();

        if ((pla2.getDuelRequest() == pla.getIndex()) || (pla2.getChallengeRequest() == pla.getIndex())) {
            pla.getFocus().focus(pla2);
            pla.setDuelRequest(pla2.getIndex());
            pla.getRequest().setRequest(
                net.tazogaming.hydra.entity3d.player.actions.ActionRequest.TYPE_DUEL_REQUEST, 1, pla2);
            pla.requestWalk(pla2.getX(), pla2.getY());

            return;
        }

        if (pla2.getTradeRequest() == pla.getIndex()) {
            pla.getFocus().focus(pla2);
            pla.requestWalk(pla2.getX(), pla2.getY());
            pla.setTradeRequest(pla2.getIndex());
            pla.getRequest().setRequest(
                net.tazogaming.hydra.entity3d.player.actions.ActionRequest.TYPE_TRADE_REQUEST, 1, pla2);
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
