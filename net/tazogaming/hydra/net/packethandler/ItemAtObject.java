package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.util.Config;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 16:20
 */
public class ItemAtObject implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param packet
     * @param player
     */
    @Override
    public void handlePacket(Packet packet, Player player) {
        packet.readLEInt();    // not sure

        int itemSlot = packet.readLEShortA();

        packet.skip(2);

        final int objx = packet.readShort();

        packet.readByte();

        int                   objectid      = packet.readShortA();
        final int             objectY       = packet.readLEShortA();
        Player                pla           = (Player) player;
        CacheObjectDefinition def           = CacheObjectDefinition.forId(objectid);
        Entity                defaultEntity = new Entity();

        if (Config.getMainInfo().getProperty("load_maps").equalsIgnoreCase("true")) {
            Tile t = World.getWorld().getTile(objx, objectY, pla.getHeight());

            if (!pla.is_con && (t.getMappedObjectId() != objectid) &&!t.isHasFarmingPatch() &&  objectid != 4446 &&
                    World.getWorld().getObjectManager().getObject(objectid,objx,objectY, pla.getHeight()) == null) {
                System.err.println("not mapped");
                return;

            }
        }

        if (Walking.blockWalking(pla)) {
            pla.getActionSender().sendCloseWalkingFlag();

            return;
        }

        Walking.resetWalking(pla);
        defaultEntity.setId(objectid);
        defaultEntity.setLocation(Point.location(objx, objectY, pla.getHeight()));
        pla.getRequest().setRequest(ActionRequest.TYPE_USE_ITEM_ON_OBJECT, -1, defaultEntity, itemSlot);
        World.getWorld().getPathService().walkToObject(pla, objectid, objx, objectY);

        if (pla.getRights() >= Player.ADMINISTRATOR) {
            pla.getActionSender().sendMessage("objectId: " + objectid + " " + objx + " " + objectY + " " + itemSlot);
        }

        if (def != null) {
            pla.getRequest().setObjectLengths(def.sizeX, def.sizeY);
        }
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
