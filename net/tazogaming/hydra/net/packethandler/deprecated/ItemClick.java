package net.tazogaming.hydra.net.packethandler.deprecated;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.minigame.barrows.BarrowsGame;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRules;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.skill.hunter.HuntingController;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 01:21
 */
public class ItemClick implements PacketHandler {


    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player plr) {
        int    interfaceiD = p.readLEShort();
        int    itemSlot    = p.readShortA();
        Item   i           = plr.getInventory().getItem(itemSlot);

        if (i == null) {
            return;
        }

        if (plr.isActionsDisabled()) {
            return;
        }

        if(plr.isStunned())
            return;

        plr.terminateScripts();
        plr.terminateActions();

        if (i.getPotionHandler() != null) {
            handlePotion(itemSlot, plr);

            return;
        }

        if (i.getIndex() == 952) {
            if (BarrowsGame.onDig(plr)) {
                return;
            }
        }


        if (HuntingController.onItemClick(itemSlot, plr)) {
            return;
        }

        if (i.getIndex() == 11169) {
            if (plr.getAccount().getGame() == null) {
                Dialog.printStopMessage(plr, "You don't have a target active",
                                        "Speak to the stranger in edgeville to get a new targt");
            } else {
                plr.getAccount().getGame().build_interface(plr);
            }

            return;
        }

        if (i.getIndex() == 4277) {
            if (plr.getAccount().getGame() == null) {
                Dialog.printStopMessage(plr, "You don't have a target active",
                                        "Speak to the stranger in edgeville to get a new targt");
            } else {
                plr.getAccount().getGame().trackTarget();
            }

            return;
        }

        if ((i.getIndex() == 4053) && (plr.getZombiesGame() != null)) {
            plr.getZombiesGame().setupBarricade(plr);
            plr.getInventory().deleteItem(i, 1);

            return;
        }

        if (ClueManager.showClue(plr, i)) {
            return;
        }

        plr.getAccount().putSmall(Account.LAST_USED_SLOT, itemSlot);

        if (i.getIndex() == 952) {
            plr.getAnimation().animate(830);
            plr.getTickEvents().add(new PlayerTickEvent(plr, 2, true, false) {
                @Override
                public void doTick(Player owner) {
                    owner.getAnimation().animate(65535);

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
            plr.getActionSender().sendMessage("You dig your spade into the ground.");

            if (World.getWorld().getScriptManager().directTrigger(plr, null, Trigger.DIG, plr.getX(), plr.getY())) {
                return;
            } else {
                plr.getActionSender().sendMessage("You find nothing.");
            }
        }

        if (World.getWorld().getScriptManager().forceTrigger(plr, Trigger.ITEM_CLICK, null, i.getIndex())) {
            return;
        } else {
            plr.getActionSender().sendMessage("Nothing interesting happens...");
        }
    }

    private void handlePotion(int itemSlot, Player pla) {
        Item potion   = pla.getInventory().getItem(itemSlot);
        int  curDose  = potion.getPotionHandler().getRemaining(potion.getIndex());
        Duel cur_duel = pla.getGameFrame().getDuel();

        if ((cur_duel != null) && (cur_duel.getStatus() == Duel.STATUS_IN_FIGHT)) {
            if (cur_duel.setting(Duel.NO_DRINKS)) {
                pla.getActionSender().sendMessage("Potion drinking has been disabled during this duel.");

                return;
            }
        }

        if (pla.getGetCurrentWar() != null) {
            if (pla.getGetCurrentWar().getGameRules().settingActive(ClanWarRules.POTS_DISABLED)) {
                pla.getActionSender().sendMessage("Potions are not allowed in this war.");

                return;
            }
        }

        if (pla.getTimers().timerActive(TimingUtility.POTION_DELAY_TIMER)) {
            return;
        }

        pla.getTimers().setTime(TimingUtility.POTION_DELAY_TIMER, 3);
        pla.getAnimation().animate(829);

        curDose--;

        Item newItem = null;

        if (curDose != -1) {
            newItem = Item.forId(potion.getPotionHandler().getDose(curDose));
        } else {
            newItem = Item.forId(229);
        }

        String name = potion.getName().substring(0, potion.getName().indexOf("("));

        pla.getActionSender().sendMessage("You drink some of your "
                                          + potion.getName().substring(0, potion.getName().indexOf("(")));

        if (curDose == -1) {
            pla.getActionSender().sendMessage("You have finished your " + name);
        } else {
            pla.getActionSender().sendMessage("You now have " + (curDose + 1) + " doses left.");
        }

        potion.getPotionHandler().getDoseHandler().drink(pla);
        pla.getInventory().replaceItem(itemSlot, newItem.getIndex());
    }
}
