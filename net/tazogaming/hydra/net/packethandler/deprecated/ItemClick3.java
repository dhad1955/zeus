package net.tazogaming.hydra.net.packethandler.deprecated;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/12/13
 * Time: 22:28
 */
public class ItemClick3 implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player plr) {

        int    itemId = p.readShortA();
        int    a1     = p.readLEShortA();
        int    a2     = p.readLEShortA();
        Item   item   = plr.getInventory().getItem(a1);

        if (plr.isActionsDisabled()) {
            return;
        }

        if (item.isDungItem()) {
            if (plr.getDungeon() != null) {
                if (plr.getCurStat(21) < 40) {
                    plr.getActionSender().sendMessage("You need a dungeoneering level of atleast 40 to this.");

                    return;
                }

                if (plr.getPoints(Points.DUNGEONEERING_POINTS) < 10) {
                    plr.getActionSender().sendMessage("You need atleast 10 dungeoneering points to bind this weapon.");

                    return;
                }

                plr.getActionSender().sendMessage(
                    item.getIndex() + " is now binded, you will start with this weapon every time in dungeoneering.");
                plr.addPoints(Points.DUNGEONEERING_POINTS, -10);
                plr.getAccount().setSetting("dung_bind", item.getIndex(), true);
            }
        } else {
            if (World.getWorld().getScriptManager().forceTrigger(plr, Trigger.ITEM_ACTION_4, null, item.getIndex())) {
                return;
            } else {
                plr.getActionSender().sendMessage("Nothing interesting happens...");
            }
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
