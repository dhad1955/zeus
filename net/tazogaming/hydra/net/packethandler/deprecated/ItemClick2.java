package net.tazogaming.hydra.net.packethandler.deprecated;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/10/13
 * Time: 07:15
 */
public class ItemClick2 implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {
        int[]  anInt       = new int[3];
        int    interfaceId = p.readShortA();
        int    slot        = p.readLEShort();
        int    itemId      = p.readShortA();
        Item   theItem     = pla.getInventory().getItem(slot);

        if (pla.isActionsDisabled()) {
            return;
        }

        if (World.getWorld().getScriptManager().forceTrigger(pla, Trigger.ITEM_ACTION2, null, theItem.getIndex())) {
            return;
        } else {
            pla.getActionSender().sendMessage("Nothing interesting happens...");
        }
    }
}
