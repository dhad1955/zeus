package net.tazogaming.hydra.net.packethandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/10/14
 * Time: 19:31
 */
public enum DelayPolicy  {
    INSTANT, NEXT_TICK
}
