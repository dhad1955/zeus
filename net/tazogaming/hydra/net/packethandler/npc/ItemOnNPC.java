package net.tazogaming.hydra.net.packethandler.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/11/13
 * Time: 01:11
 */
public class ItemOnNPC implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player player) {
        int readone   = p.readShortA();
        int readtwo   = p.readShortA();
        int readthree = p.readLEShort();
        int readfour  = p.readShortA();
        System.err.println("is this a real packet");
        NPC npc       = World.getWorld().getNpcs().get(readtwo);

        if (npc == null) {
            return;
        }


        player.getFocus().focus(npc);
        player.getRequest().setRequest(ActionRequest.TYPE_USE_ITEM_ON_NPC, 1, npc, readone);

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
