package net.tazogaming.hydra.net.packethandler.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 00:01
 */
public class NpcAction implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    @Override
    public void handlePacket(Packet p, Player player) {
        int    npcIndex = -1;
        int    slot     = -1;

        if (player.getCutScene() != null) {
            return;
        }

        switch (p.getId()) {
        case 28 :
            p.readByte();
            npcIndex = p.readLEShort();
            slot     = 1;

            break;

        case 33 :
            slot     = 2;
            npcIndex = p.readShort();

            break;

        case 31 :
            slot     = 3;
            npcIndex = p.readShort();

            break;

        // 345
        case 72 :
            slot     = 4;
            npcIndex = p.readShortBigEndian();

            break;
        }

        if (npcIndex == -1) {
            return;
        }

      try {
          NPC npc = World.getWorld().getNpcs().get(npcIndex);

          if (npc == null) {
              return;
          }

          player.getActionSender().sendDev("npc position: " + npc.getLocation());

          if (player.isActionsDisabled()) {
              return;
          }

          if (!player.getRequest().isOpen() || !player.getRequest().isWithinDistance()) {
              if (!npc.isWithinDistance(player, 1)) {
                  player.requestWalk(npc.getX(), npc.getY());
              }
          }

          player.getRequest().setRequest(ActionRequest.TYPE_NPC_ACTION_OTHER, 0, npc, slot);
          player.getFocus().focus(npc);
      }catch (Exception ee) { }

    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
