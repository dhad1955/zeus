package net.tazogaming.hydra.net.packethandler.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 23:03
 * To change this template use File | Settings | File Templates.
 */
public class AttackNPCHandler implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {
        int    npcIndex = p.readShort();
        NPC    npc      = World.getWorld().getNpcs().get(npcIndex);

        if (npc == null) {
            return;
        }

        if (pla.getCutScene() != null) {
            return;
        }

        if (!pla.quest_stage_completed(0, 0)) {}

        if (pla.isActionsDisabled()) {
            return;
        }

        pla.getFocus().focus(npc);

        if (npc != null) {

            if (Combat.isWithinDistance(pla, npc)) {
                pla.getRoute().resetPath();
                pla.getActionSender().sendCloseWalkingFlag();
            }else{
                pla.requestWalk(npc.getX(), npc.getY());
            }
            pla.getRequest().setRequest(ActionRequest.TYPE_NPC_ATTACK, Combat.getDistance(npc), npc);

        }
    }
}
