package net.tazogaming.hydra.net.packethandler.npc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcSpawnHandler;
import net.tazogaming.hydra.entity3d.npc.ai.movement.SummonAttackNPCMovement;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.Command;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.game.skill.summoning.Summoning;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/10/13
 * Time: 18:27
 */
public class MageNPC implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player player) {
        int    idTAG         = p.readInt1();
        int    int1          = p.readLEShort();
        int    npcIndex      = p.readShort();
        int    b             = p.readByte() + 128;
        int    mainInterface = idTAG >> 16;
        int    spell         = idTAG - (mainInterface << 16);

        if (player.getRights() >= Player.ADMINISTRATOR) {
            player.getActionSender().sendMessage("SpellID: " + spell+" "+mainInterface+" "+spell+" "+int1+" "+b);
        }

        NPC npc = World.getWorld().getNpcs().get(npcIndex);

        if (Command.hookId != -1) {
            npc.getSpawnData().setScriptId(Command.hookId);
            npc.reloadForScript();
            Command.hookId = -1;
            NpcSpawnHandler.save("config/world/npc/npcpositions.cfg");
        }


        if(npc != null && mainInterface == 149){
            player.getRoute().resetPath();
            player.getRequest().setRequest(ActionRequest.TYPE_USE_ITEM_ON_NPC, 1, npc, int1);
            player.getFocus().focus(npc);
            player.followNpc(npc);
            return;

        }
        if (Command.deleteOn && (player.getRights() >= Player.ADMINISTRATOR)) {
            NpcSpawnHandler.delete(npc.getSpawnData());
            npc.unlink();
            NpcSpawnHandler.save("config/world/npc/npcpositions.cfg");
            Command.deleteOn = false;
            player.getActionSender().sendMessage("Npc unlinked");

            return;
        }

        if ((npc != null)
                && (((mainInterface == 747) && (spell == 14 || spell == 17)) || ((mainInterface == 662) && (spell == 65))
                    || ((mainInterface == 662) && (spell == 74)))) {
            if (!npc.getDefinition().isAttackable() && !Summoning.PVPCheck(npc, player)) {
                player.getActionSender().sendMessage("You can't attack this NPC");

                return;
            }

            if (npc == player.getCurrentFamiliar()) {
                return;
            }

            if (npc.canBeAttacked(player)) {
                if (player.getCurrentFamiliar() != null) {
                    player.getCurrentFamiliar().setMovement(
                            new SummonAttackNPCMovement(player.getCurrentFamiliar(), npc));
                    player.getRoute().resetPath();

                    if (spell == 74 || spell == 17) {
                        player.getCurrentFamiliar().setFamiliarSpec(true);
                    }

                    return;
                } else {
                    player.getActionSender().sendMessage("This npc is already under attack.");

                    return;
                }
            }
        }

        if ((npc != null) && player.withinRange(npc)) {
            if (npc.canBeAttacked(player)) {
                player.getFocus().unFocus();
                player.unFollow();
                player.getFocus().focus(npc);
                player.getRequest().setRequest(ActionRequest.TYPE_MAGE_ON, Combat.getDistance(player), npc,
                                               null);
                player.setRequestedSpell(spell);

                if (Combat.isWithinDistance(player, npc)) {
                    player.getRoute().resetPath();
                }
            } else {
                player.getActionSender().sendMessage("This mob is already under attack.");
            }
        }
    }

    /**
     * Method familiarSpec
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void familiarSpec(Player pla) {
        if (pla.getCurrentFamiliar() == null) {
            pla.getActionSender().sendMessage("You don't have a familiar active.");

            return;
        }

        NPC kill;
        int id = Summoning.getScrollID(pla);

        if (id == -1) {
            pla.getActionSender().sendMessage("error");

            return;
        }

        pla.getAccount().setSetting("scroll_id", id);

        if (!pla.getInventory().hasItem(id, 1)) {
            pla.getActionSender().sendMessage("You need a " + Item.forId(id).getName() + " to use this special.");

            return;
        }

        NpcScriptScope scr = new NpcScriptScope(
                                   pla.getCurrentFamiliar(),
                                   pla.getCurrentFamiliar().getControllerScript().getController().getTrigger(
                                       Block.ON_SUMMON_SPEC), pla);

        if (pla.getCombatAdapter().getFightingWith() instanceof NPC) {
            scr.setInteractingNPC(0, pla.getCombatAdapter().getFightingWith().getNpc());
        }

        if(pla.getCurrentFamiliar().getCombatHandler().getFightingWith() instanceof Player){
            scr.setInteractingPlayer((Player)pla.getCurrentFamiliar().getCombatHandler().getFightingWith());
        }

        if (pla.getCurrentFamiliar().getCombatHandler().getFightingWith() instanceof NPC) {
            scr.setInteractingNpc(pla.getCurrentFamiliar().getCombatHandler().getFightingWith().getNpc());
        }

        NPCScriptEngine.evaluate(pla.getCurrentFamiliar(), scr);
    }
}
