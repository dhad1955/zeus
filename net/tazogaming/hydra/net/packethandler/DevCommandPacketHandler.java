package net.tazogaming.hydra.net.packethandler;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;

/**
 * Created by Dan on 08/01/2016.
 */
public class DevCommandPacketHandler implements PacketHandler {
    @Override
    public void handlePacket(Packet p, Player player) {
        p.readShort();
        Command.handlePacket(player, p.readRS2String());
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return null;
    }
}
