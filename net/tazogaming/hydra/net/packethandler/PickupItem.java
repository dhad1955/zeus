package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.net.Packet;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 22:22
 * To change this template use File | Settings | File Templates.
 */
public class PickupItem implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param io
     */
    public void handlePacket(Packet p, Player io) {
        int    itemX  = p.readLEShort();
        int    itemId = p.readLEShort();
        int    itemY  = p.readShortA();
        Player player = (Player) io;

        if (player.getCutScene() != null) {
            return;
        }

        Tile t = World.getWorld().getTile(itemX, itemY, player.getLocation().getHeight());

        if (t.getItems() != null) {
            for (FloorItem i : t.getItems()) {
                if ((i.getItemId() == itemId) && (i.getCurrentInstance() == player.getCurrentInstance())
                        && (i.isVisableTo(player) || i.isGlobal())) {
                    player.walkTo(i);
                    player.getRequest().setRequest(ActionRequest.TYPE_FLOOR_ITEM_PICKUP, 0, i, null);

                    return;
                }
            }
        }
    }
    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
