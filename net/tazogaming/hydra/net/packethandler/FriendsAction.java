package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/10/13
 * Time: 20:57
 */
public class FriendsAction implements PacketHandler {
    public static final int
        ADD_FRIEND    = 2,
        REMOVE_FRIEND = 77,
        ADD_IGNORE    = 74,
        REMOVE_IGNORE = 20;

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    @Override
    public void handlePacket(Packet p, Player player) {
        Player pla    = (Player) player;
        String friend = p.readRS2String();

        switch (p.getId()) {
        case ADD_FRIEND :
            pla.getFriendsList().addFriend(friend);

            break;

        case REMOVE_FRIEND :
            pla.getFriendsList().deleteFriend(friend);

            break;

        case ADD_IGNORE :
            pla.getFriendsList().addIgnore(friend);

            break;

        case REMOVE_IGNORE :
            pla.getFriendsList().removeIgnore(friend);

            break;
        }
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
