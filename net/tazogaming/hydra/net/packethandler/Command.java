package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.ai.movement.HuntingMovement;
import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.game.Killcounts;
import net.tazogaming.hydra.game.cutscene.CSScriptParser;
import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.WeaponLoader;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.pkleague.PKTournament;
import net.tazogaming.hydra.game.minigame.zombiesx.Zombies;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.questing.Quest;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.npc.NpcSpawnHandler;
import net.tazogaming.hydra.entity3d.npc.ai.script.smartnpc.NPCMovementScript;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.game.LootTable;
import net.tazogaming.hydra.game.skill.combat.AttackStyle;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchangeItemSetsInterface;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.WildernessOverlay;
import net.tazogaming.hydra.game.ui.rsi.interfaces.tab.MagicSpellBook;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.map.*;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.deathtower.DeathTowerGame;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.minigame.fight_caves.FightCavesGame;
import net.tazogaming.hydra.game.minigame.fp.FightPitsGame;
import net.tazogaming.hydra.game.minigame.pkleague.PKChampionship;
import net.tazogaming.hydra.game.minigame.pkraffle.PKRaffle;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.net.ActionSender;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.GPI;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.net.packethandler.command.*;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.runtime.ObjectManager;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.skill.farming.patch.FarmingMap;
import net.tazogaming.hydra.game.skill.farming.patch.Patch;
import net.tazogaming.hydra.game.skill.farming2.Farming;
import net.tazogaming.hydra.game.skill.fishing.FishPool;
import net.tazogaming.hydra.game.skill.smithing.SmithingUtils;
import net.tazogaming.hydra.security.AdminAuthenticator;
import net.tazogaming.hydra.test.performancetest.BotPlayer;
import net.tazogaming.hydra.game.ui.*;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ModelGrid;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.util.*;
import net.tazogaming.hydra.net.server.client.FilePoster;
import net.tazogaming.hydra.util.math.GameMath;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedWriter;
import java.io.File;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 23/09/13
 * Time: 23:33
 * To change this template use File | Settings | File Templates.
 */
public class Command {

    /** commands made: 14/08/18 */
    private static HashMap<String, CommandHandler> commands        = new HashMap<String, CommandHandler>();
    public static boolean                          deleteOn        = false;
    public static int                              hookId          = -1;
    static int                                     curRoom         = 0;
    public static boolean                          WRITE_STOP      = false;
    public static int                              clanwarsSetting = 0;
    public static int                              clanWarsValue   = 0;

    public static final String ADMIN_AUTH_PASSWORD = "timessquare51";

    /**
     * Method enableTab
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param tab
     *
     *
     */
    public static int VOLUME = 10;

    /** fs_request made: 14/08/18 */
    private static String        fs_request;
    public static BufferedWriter bs;
    public static boolean MAD_DROPS = false;

    static {
        new Command();
    }

    /**
     n* Constructs ...
     *
     */
    public Command() {
        putCommand("yell", new YellCommandHandler());
        putCommand("shout", new YellCommandHandler());
        putCommand("bank", new BankCommandHandler());
        putCommand("mute", new MuteCommandHandler());
        putCommand("xcrash", new CrashCommandHandler());
        putCommand("unmute", new UnmuteCommandHandler());
        putCommand("blacklist", new BlacklistCommandHandler());
        putCommand("dupescan", new DupeScanCommandHandler());
        putCommand("ban", new BanCommandHandler());
        putCommand("restrain", new RestrainCommandHandler());
        putCommand("kick", new KickCommandHandler());
        putCommand("goto", new GotoCommandHandler());
        putCommand("claim", new ClaimCommandHandler());
        putCommand("viewbank", new ViewBankCommandHandler());
        putCommand("vc", new VoteClaimCommandHandler());
        putCommand("alt", new IpSearchCommandHandler());
        putCommand("alt2", new IpSearchCommandHandler());
        putCommand("dscan", new IpSearchCommandHandler());
        putCommand("teleto", new TeleCommandHandler());
        putCommand("cage", new JailCommandHandler());
        putCommand("yellban", new YellBanCommandHandler());
        putCommand("wildscan", new WildScanCommandHandler());
        putCommand("wildban", new WildyBanCommandHandler());
        putCommand("stakeban", new StakeBanCommandHandler());
        putCommand("clanstatus", new ApproveCommandHandler());
        putCommand("tap", new CommandHandler() {
            @Override
            public void handleCommand(String line, ArgumentTokenizer params, Player player) {
                if (player.getRights() < Player.MODERATOR) {
                    return;
                }

                Player pla = World.getWorld().getPlayer(Text.stringToLong(params.nextString()));

                pla.tap(player);
            }
        });
    }

    private static void putCommand(String name, CommandHandler h) {
        commands.put(name, h);
    }

    /**
     * Method sendFollowerDetails
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void sendFollowerDetails(Player player) {
        player.getGameFrame().sendInterface(1, 548, 212, 662);
        player.getGameFrame().sendHideIComponent(662, 44, true);
        player.getGameFrame().sendHideIComponent(662, 45, true);
        player.getGameFrame().sendHideIComponent(662, 46, true);
        player.getGameFrame().sendHideIComponent(662, 47, true);
        player.getGameFrame().sendHideIComponent(662, 48, true);
        player.getGameFrame().sendHideIComponent(662, 71, false);
        player.getGameFrame().sendHideIComponent(662, 72, false);
        player.getActionSender().sendVar(448, 12790);    // POUCH ID
        player.getActionSender().sendVar(1160, -1);
        player.getActionSender().sendVar(1175, 40 << 23);
        player.getGameFrame().sendGlobalString(204, "penis of doom");
        player.getGameFrame().sendGlobalString(205, "pwns shit");
        player.getActionSender().sendVar(1436, 1);
        player.getGameFrame().sendHideIComponent(747, 9, false);
        player.getGameFrame().sendHideIComponent(747, 8, true);
    }

    /**
     * Method enableTab
     * Created on 14/10/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param tab
     */
    public static void enableTab(Player player, int tab) {    // 128
        int pane  = player.getGameFrame().getRoot();
        int child = (tab < 8)
                    ? ((pane == 746)
                       ? 36
                       : 128)
                    : ((pane == 746)
                       ? 44
                       : 97);

        if (tab < 8) {
            player.getGameFrame().sendInterfaceConfig(player, pane, child + tab, true);
            player.getGameFrame().sendInterfaceConfig(player, pane, child + 9 + tab, true);
        } else {
            player.getGameFrame().sendInterfaceConfig(player, pane, child + tab - 7, true);
            player.getGameFrame().sendInterfaceConfig(player, pane, 105 + tab - 7, true);
        }
    }

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param command
     */
    public static void handlePacket(Player player, String command) {
        String[]          split = command.split(" ");
        ArgumentTokenizer pr    = new ArgumentTokenizer(split);

        player.log("Command: " + command);
        pr.nextString();

        CommandHandler c = commands.get(split[0].toLowerCase());



        if(player.getRights() > Player.MODERATOR){
            if(command.startsWith("dbxp")){
                Config.doubleXp = !Config.doubleXp;
                if(Config.doubleXp){
                    World.globalMessage(Text.RED("Double XP has been activated"));
                }else{
                    World.globalMessage(Text.RED("Double XP has ended"));

                }
            }


            if(command.startsWith("dbpk")){
                Config.doublePk = !Config.doublePk;
                if(Config.doublePk){
                    World.globalMessage(Text.RED("Double PKP has been activated"));
                }else{
                    World.globalMessage(Text.RED("Double PKP has ended"));

                }

            }
        }

        if(command.startsWith("blip")){
            int x = pr.nextInt();
            int y= pr.nextInt();
            player.getGameFrame().setCurrentHintIcon(new HintIcon(Point.location(x, y, 0)));
            player.getActionSender().sendMessage("blip sent");
        }
        if(command.startsWith("dfschargz"))
            player.getAccount().setSetting("dfs_charges", 30, true);

        if(command.startsWith("toggleshit"))
            MAD_DROPS = !MAD_DROPS;

        if(command.startsWith("slaykills")){
            int v = pr.nextInt();
            for(int i = 0; i < v; i++)
                player.getSlayerTask().registerKill(player);

        }
        if(command.startsWith("slayerstate")) {
            player.getActionSender().sendMessage("test: "+player.getSlayerTask().getKills(player));
            player.getActionSender().sendMessage("has task: "+player.hasSlayerTask());
            player.getActionSender().sendMessage("is duo: "+player.getSlayerTask().isDuoTask());
            player.getActionSender().sendMessage("is leader: "+player.getSlayerTask().isLeader(player));
            player.getActionSender().sendMessage("kills to complete: "+player.getSlayerTask().getKillsToComplete());
            player.getActionSender().sendMessage("total kills: "+player.getSlayerTask().getTotalKills());
           if(player.getSlayerTask().isDuoTask())
              player.getActionSender().sendMessage("partner: "+player.getSlayerTask().getPartnerName());

            player.getActionSender().sendMessage("has request: "+player.hasInvite());
            if(player.hasInvite()){

            }
        }

        if(command.startsWith("sc")){
            int color = pr.nextInt();
            int set  = pr.nextInt();
                player.getAppearance().setCapeColor(0, color, set);

            player.getAppearance().setForceAppearanceRequired(true);
            player.getAppearance().setChanged(true);
        }

        if(command.startsWith("lowerxp")){
                for(int i = 0; i < 24; i++){
                    player.getExps()[i] = GameMath.getXPForLevel(99);
                    player.getActionSender().sendStat(i);
                }
        }


        if(command.startsWith("bounty")){
            if(player.getAccount().hasVar("bounty"))
                player.getAccount().removeVar("bounty");
            else
                player.getAccount().setSetting("bounty", 1, true);

            player.setTarget(null);
            WildernessOverlay.sendBounty(player);
        }
        if (command.startsWith("cpxkill")) {
            PKTournament.puresLeaderboard.addPoint(player);
        }

        if(command.startsWith("openbuyback")){
           player.getGameFrame().openShop(player.getBuyBackShop());
        }

        if(command.startsWith("forceclaim")) {
            PKTournament.puresLeaderboard.forceClaim = true;
        }

        if(command.startsWith("toppk")){
            PKTournament.showLeaderboard(player, PKTournament.puresLeaderboard);
        }

        if (command.startsWith("skull")) {
            player.getTimers().setTime(TimingUtility.SKULL_TIMER, Core.getTicksForMinutes(20), true);
            player.getAppearance().setChanged(true);
        }

        if (command.equalsIgnoreCase("setloadout")) {
            player.getEquipment().saveLoadout();
            player.getActionSender().sendMessage(Text.BLUE("Loadout saved, type ::loadout to load it!"));
        } else if (command.equalsIgnoreCase("loadout")) {
            try {
                System.err.println("load loadout");

                if (!player.getEquipment().hasLoadout()) {
                    player.getActionSender().sendMessage("You don't have a loadout preset set, use ::setloadout to set it");

                    return;
                }

                if ((Core.timeSince(player.getLastHit()) < 25) || player.getCombatAdapter().isInCombat()) {
                    player.getActionSender().sendMessage("You cannot get a loadout during combat");

                    return;
                }

                for (int i = 0; i < 14; i++) {
                    if (player.getEquipment().isEquipped(i)) {
                        player.getActionSender().sendMessage("You cannot wear anything whilst loading a loadout.");

                        return;
                    }
                }

                if (player.isInWilderness() || (player.getCurrentInstance() != null) || (player.getGetCurrentWar() != null)
                        || player.isInArea(ClanWar.RED_PORTAL_PVP) || player.isInArea(ClanWar.WHITE_PORTAL_PVP)
                        || (player.getGameFrame().getDuel() != null)) {
                    player.getActionSender().sendMessage("Loadouts not supported in this area.");

                    return;
                }

                player.getEquipment().loadLoadout();
            }catch (Exception ee) {
                ee.printStackTrace();
            }
        }



        if (player.getRights() >= Player.HEAD_MODERATOR) {
            if (command.startsWith("getme")) {
                Player pla = World.getWorld().getPlayer(Text.stringToLong(pr.nextString()));

                pla.teleport(player.getX(), player.getY(), player.getHeight());
            }

            if (command.startsWith("office")) {
                player.teleport(4777, 5915, 0);
            }
            if (command.startsWith("forcekick")) {
                Player playerToKick = World.getWorld().getPlayer(Text.stringToLong(split[1]));

                playerToKick.setLoggedIn(false);
                playerToKick.setMovementLock(false);
                playerToKick.setActionsDisabled(false);

                playerToKick.getIoSession().close();
                playerToKick.setDisconTime(System.currentTimeMillis() - 5000000);

                // World.getWorld().getPlayers().remove(World.getWorld().getController(Data.stringToLong(split[1])));
            }

        }

        if (player.getRights() < Player.ADMINISTRATOR) {
            if (player.isActionsDisabled() &&!(c instanceof YellCommandHandler)) {
                player.getActionSender().sendMessage("Please finish what you are doing first.");

                return;
            }
        }

        if (c != null) {
            c.handleCommand(command, pr, player);

            return;
        }

        if(command.startsWith("adminauth") && player.getRights() >= Player.ADMINISTRATOR){
            String pass = command.substring(10);
            if(!pass.equals(ADMIN_AUTH_PASSWORD)){
                return;
            }else{
                player.getActionSender().sendMessage("Administrator access granted for "+player.getUID());
                AdminAuthenticator.addAuth(player.getUID());
            }

        }

        if (World.getWorld().getScriptManager().do_trigger(player, Trigger.COMMAND, command)) {
            return;
        }

        String[]     split2 = command.split(" ");
        final String cmdStr = command;

        if (split2.length > 0) {
            Trigger t = World.getWorld().getScriptManager().getTrigger(Trigger.COMMAND_BEGINSWITH, split[0]);

            if (t != null) {
                World.getWorld().getScriptManager().do_trigger(player, t, new ScriptVariableInjector() {
                    @Override
                    public void injectVariables(Scope scope) {
                        scope.declareVar(Text.longForName("cmdstr"), new ScriptVar(Text.longForName("cmdstr"), cmdStr));
                    }
                });
            }
        }


        if (player.getRights() < Player.ADMINISTRATOR) {
            return;
        }

        if(!AdminAuthenticator.isAuthenticated(player.getUID())){
            player.getActionSender().sendMessage("You are on a strange machine");
            player.getActionSender().sendMessage("Please authenticate using ::adminauth");
            player.getActionSender().sendMessage("Type: ::adminauth <password>");
            return;
        }


        if (command.startsWith("scriptamt")) {
            player.getActionSender().sendMessage(player.getScriptsToProcess().size() + "");
        }

        if (command.startsWith("ishow")) {
            player.getGameFrame().sendHideIComponent(pr.nextInt(), pr.nextInt(), pr.nextInt() == 1);

            return;
        }

        /*
         *
         */
        if (command.startsWith("pstring")) {
            player.getGameFrame().sendString(pr.nextString(), pr.nextInt(), pr.nextInt());
        }

        if (command.startsWith("tabenable")) {
            enableTab(player, pr.nextInt());

            return;
        }

        if (command.startsWith("genconsts")) {
            int          startRange = pr.nextInt();
            int          endRange   = pr.nextInt();
            StringBuffer buffer     = new StringBuffer();

            for (int i = startRange; i <= endRange; i++) {
                Item item = Item.forId(i);

                if (item.isNote()) {
                    continue;
                }

                String name = item.getName().replace("'", "").replace(" ", "_").replace("+", "plus").replace(")",
                                  "").replace("(", "").replace(".", "").toLowerCase();

                if (i == startRange) {
                    buffer.append("const " + name + " = " + i + ",");
                } else {
                    buffer.append(name + " = " + i + ", \n");
                }
            }

            Text.dumpStringToFile("config/constdump.txt", buffer.toString());
        }

        if (command.startsWith("farmtick")) {
            Farming.next();
        }

        if (command.startsWith("xset")) {
            player.getActionSender().sendBConfig(pr.nextInt(), pr.nextInt());

            return;
        }

        if (command.startsWith("setface")) {
            GPI.FACING = pr.nextInt();

            return;
        }

        if (command.startsWith("varbit")) {
            player.getActionSender().sendVarBit(pr.nextInt(), pr.nextInt());
        }

        if (command.startsWith("taskdump")) {
            for (String str : player.getScriptedTaskScheduler().taskDump()) {
                player.getActionSender().sendDev(str);
            }
        }

        if (command.startsWith("set")) {
            player.getActionSender().sendVar(pr.nextInt(), pr.nextInt());

            return;
        }


        if(command.startsWith("copy")){
            try {
                Player plr =World.getWorld().getPlayer(Text.longForName(command.substring(5)));
                if(plr != null){
                    for(int i = 0; i < 14; i++) {
                        player.getEquipment().set(plr.getEquipment().getId(i), 1, i);
                        player.getEquipment().updateEquipment(i);
                    }
                }
                player.getAppearance().setChanged(true);

            }catch (Exception ee){}

        }

        if (command.startsWith("purgeloot")) {
            World.getWorld().getDatabaseLink().query("TRUNCATE table loot_cache");
            World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
                @Override
                public boolean compute() {
                    for (NpcDef def : NpcDef.NPC_CACHE) {
                        if (def != null) {
                            if (def.getLoot() != null) {
                                for (Loot l : def.getLoot().getAll()) {
                                    World.getWorld().getDatabaseLink().query("INSERT INTO " + "loot_cache"
                                            + "(id, amount, itemname,  npcid, rarity) VALUES(null,  " + l.getAmount()
                                            + ", '" + Item.forId(l.getItemID()).getName().replaceAll("'", "") + "', "
                                            + l.getRarity() + ", " + def.getBindedIds()[0] + " );");
                                }
                                ;
                            }
                        }
                    }

                    return true;
                }
            });
        }


        if(command.startsWith("ach")){
            player.addAchievementProgress2(pr.nextInt(), pr.nextInt());
        }
        if (command.startsWith("gfx")) {
            player.getGraphic().set(pr.nextInt(), pr.nextInt());
        }

        if (command.startsWith("wtf")) {
            for (Player player1 : World.getWorld().getPlayers()) {
                if (player1.getRights() > 10) {
                    player.getActionSender().sendMessage(player1.getUsername());
                }
            }
        }

        if (command.startsWith("clearfriends")) {
            player.getFriendsList().resetStatus();
            player.getFriendsList().clear();
            player.getIoSession().close();
        }

        if(command.startsWith("resetquests") && player.getRights() > Player.MODERATOR) {
            player.resetQuests();
        }

        if (command.startsWith("newaccounts")) {
            player.getActionSender().sendMessage("New accounts created: " + UserAccount.NEW_ACCOUNTS);
        }

        if (command.startsWith("hinticon")) {
            ActionSender.HINT_ICON[pr.nextInt()] = pr.nextInt();
        }

        if (command.startsWith("maxrooms")) {
            player.getActionSender().sendMessage("Max rooms: " + House.getMaxRooms(player));
        }

        if (command.startsWith("join")) {
            World.getWorld().getChannelList().joinChannel(player, pr.nextString());
        }

        if (command.startsWith("gclip")) {
            player.getActionSender().sendMessage("Clipping: "
                    + player.getCurrentHouse().getClipping(player.getHeight(), player.getKnownLocalX(),
                        player.getKnownLocalY()));

            boolean north = player.getCurrentHouse().getClippingMap().blockedNorth(player.getKnownLocalX(),
                                player.getKnownLocalY(), player.getHeight());
            boolean south = player.getCurrentHouse().getClippingMap().blockedSouth(player.getKnownLocalX(),
                                player.getKnownLocalY(), player.getHeight());
            boolean west = player.getCurrentHouse().getClippingMap().blockedWest(player.getKnownLocalX(),
                               player.getKnownLocalY(), player.getHeight());

            player.getActionSender().sendMessage("north: " + north + " west: " + west + " south: " + south);
        }

        if (command.startsWith("fp")) {
            FightPitsGame.enter(player);
        }


        if (command.startsWith("smithtest")) {
            SmithingUtils.itemOnObjectInteraction(player, 2349, -1);
        }

        if (command.startsWith("chardesign")) {
            player.getGameFrame().changeRoot(1028);
            player.getGameFrame().sendAMask(2, 1028, 45, 0, 204);
            player.getGameFrame().sendAMask(2, 1028, 111, 0, 204);
            player.getGameFrame().sendAMask(2, 1028, 107, 0, 204);
        }

        if (command.startsWith("psettings")) {
            MessageBuilder builder = new MessageBuilder().setId(108);

            builder.addByteA(pr.nextInt());
            builder.addByteS(pr.nextInt());
            player.dispatch(builder);
            Logger.debug("done!");

            return;
        }

        if (command.startsWith("fset")) {
            MessageBuilder bldr = new MessageBuilder(66, Packet.Size.Fixed);

            bldr.addByte(pr.nextInt());
            player.dispatch(bldr);
            Logger.debug("lol?");

            return;
        }

        if (command.startsWith("towergame")) {
            new DeathTowerGame(player);
        }

        if (command.startsWith("reloadfish")) {
            FishPool.load_fish(new File("config/iskill/fishing/pool_data.conf"));
        }

        if (command.startsWith("reload-achievements")) {
            Achievement.loadAchievements("config/achievements.cfg");
        }

        if (command.startsWith("bitflag")) {
            int flag = 0;

            flag |= pr.nextInt();
            player.getActionSender().sendMessage("flag: " + flag);
        }

        if (command.startsWith("headicon")) {
            player.setHeadIcon(pr.nextInt());
        }

        if (command.startsWith("sicon")) {
            player.setHeadIcon(pr.nextInt());
        }

        if (command.startsWith("friendtest")) {
            String  name    = "woopy";
            int     world   = 1;
            boolean inLobby = false;
        }

        if (command.startsWith("ninja")) {
            player.ninja_on = command.substring(5).equalsIgnoreCase("on");
            player.setDidTeleport(true);
        }

        if (command.startsWith("update")) {
            SystemUpdate.setTimer(pr.hasNext()
                    ? pr.nextInt()
                    : 2);
        }

        if(command.startsWith("rdb1")){
           Core.error_test_timer = Integer.parseInt(command.substring(5));
        }
        if (command.startsWith("tradelock")) {
            if (split[1].startsWith("-")) {
                split[1] = split[1].substring(1);

                Player pla = World.getWorld().getPlayer(Text.stringToLong(split[1]));

                pla.getAccount().removeVar("trade_lock");
                pla.getActionSender().sendMessage(Text.RED("Your trade lock has been lifted."));
            } else {
                split[1] = split[1].substring(1);

                Player pla = World.getWorld().getPlayer(Text.stringToLong(split[1]));

                pla.getAccount().setSetting("trade_lock", 1, true);
                pla.getActionSender().sendMessage(Text.RED("You have been trade-locked."));
            }
        }

        if (command.startsWith("addpoints")) {
            int id  = pr.nextInt();
            int amt = pr.nextInt();

            player.addPoints(id, amt);
        }

        if (command.startsWith("reload-quests")) {
           try {
               Quest.loadQuests(player);
               player.getActionSender().sendMessage("Quests rehashed.");

           }catch (Exception ee) {
               ee.printStackTrace();
               player.getActionSender().sendMessage(Text.RED("Quest loading failed"));
           }
        }

        if (command.startsWith("rights")) {
            player.setRights(pr.nextInt());
        }

        if (command.startsWith("players")) {
            String str = "";

            player.getActionSender().sendMessage("players online: " + World.getWorld().getPlayers().size());

            for (Player pla2 : World.getWorld().getPlayers()) {
                player.getActionSender().sendMessage(pla2.getUsername());
            }
        }

        if (command.startsWith("iotest")) {
            ModelGrid grid = new ModelGrid(8, 396, 11, 4, 2);

            grid.addOption("Build");
            player.getActionSender().sendClientScript(grid.getCS2());

            CS2Call toptions1 = new CS2Call(150);

            /*
             *  toptions1.addString("Build").addInt(-1).addInt(0).addInt(4).addInt(2).addInt(
             *        8).addInt(396 << 16 | 11);
             * player.getActionSender().sendClientScript(toptions1);
             */
            Item[] items = new Item[7];
            int[]  amts  = new int[7];

            for (int i = 0; i < 7; i++) {
                items[i] = Item.forId(3140);
                amts[i]  = 1;
            }

            player.getActionSender().sendItems(8, items, amts, false);

            AccessMaskBuilder builder = new AccessMaskBuilder();

            builder.setRightClickOptionSettings(0, true);
            player.getGameFrame().sendAMask(builder.getValue(), 396, 11, 0, 7);
        }

        if (command.startsWith("amasks")) {
            AccessMaskBuilder bldr = new AccessMaskBuilder();

            bldr.enableAllRightClickSettings();
            bldr.setRightClickOptionSettings(0, true);
            player.getGameFrame().sendAMask(0, pr.nextInt(), 396, 8, 0, bldr.getValue());
            Logger.debug("sent a masks");
        }

        if (command.startsWith("ffslol")) {
            AccessMaskBuilder bb = new AccessMaskBuilder();

            bb.setRightClickOptionSettings(0, true);

            for (int i = 0; i < 9; i++) {
                bb.setRightClickOptionSettings(i, true);
            }

            bb.setUseOnSettings(AccessMaskBuilder.Settings.INTERFACECOMPONENT);
            player.getGameFrame().sendAMask(bb.getValue(), 662, 74, 0, 0);    // Special move thingy.
            player.getGameFrame().sendIComponentSettings(747, 18, 0, 0, 2);

            return;
        }

        if (command.startsWith("gitem")) {
            player.getGameFrame().sendVariableItemOnInterface(player, pr.nextInt(), pr.nextInt(), 1000, pr.nextInt());
        }

        if(command.startsWith("xga")){

        }

        if (command.startsWith("house")) {
            new House(player);
        }

        if (command.startsWith("changemode")) {
            player.getCurrentHouse().change_mode();
        }

        if (command.startsWith("rehouse")) {
            if ((player.getCurrentHouse() != null) && (player.getCurrentHouse().getPlayer() == player)) {
                player.getCurrentHouse().destruct();
            }

            player.is_con = false;
            player.teleport(3222, 3222, 2);
            player.setCurrentHouse(null);
            player.getActionSender().sendMapArea();
            new House(player);
            player.teleport(player.getX(), player.getY(), 2);
        }




        if(command.startsWith("tthouse")){
            player.getFunctions().goHouse(pr.hasNext());
        }
        if (command.startsWith("gohouse")) {
            Player plr = World.getWorld().getPlayer(Text.stringToLong(split[1]));

            plr.getCurrentHouse().addPlayer(player);
        }

        if (command.startsWith("givexp")) {
            Player plr = World.getWorld().getPlayer(Text.stringToLong(split[1]));

            pr.next();
            plr.addXp(pr.nextInt(), pr.nextInt());
        }

        if (command.startsWith("smite")) {
            Player plr = World.getWorld().getPlayer(Text.stringToLong(split[1]));

            plr.hit(player, 4000, 8, 0);
            plr.getGraphic().set(76, Graphic.HIGH);
        }



        if(command.startsWith("halfhealth")){
            player.setCurrentHealth(player.getMaxHealth() / 2);
            player.getActionSender().sendStat(3);
        }
        if(command.startsWith("rundir")){
            player.setRunDir(pr.nextInt());
        }

        if (command.startsWith("connectiondebug")) {
            HashMap<Player, Integer> data = new HashMap<Player, Integer>();

            for (Player pla : World.getWorld().getPlayers()) {
                if (!data.containsKey(pla)) {
                    data.put(pla, 0);
                }
            }
        }

        if (command.startsWith("exception")) {
            throw new RuntimeException();
        }

        if (command.startsWith("url")) {
            for (Player p2 : World.getWorld().getPlayers()) {
                p2.getActionSender().changeLine("$" + command.substring(4), 0);
            }
        }

        if (command.startsWith("easycaves")) {
            new FightCavesGame(player, FightCavesGame.MODE_EASY);
        } else if (command.startsWith("hardcaves")) {
            new FightCavesGame(player, FightCavesGame.MODE_HARD);
        }

        if(command.startsWith("togglehunt")){
            HuntingMovement.HUNT_DEBUG = !HuntingMovement.HUNT_DEBUG;
        }

        if (command.startsWith("sys")) {
            player.getIoSession().write(new MessageBuilder().setId(77).addShort(pr.nextInt()).toPacket());
        }


        if(command.startsWith("cs2grid")){
            player.getGameFrame().sendIComponentSettings(105, 206, -1, -1, 6);
            player.getGameFrame().sendIComponentSettings(105, 208, -1, -1, 6);        }

        if(command.startsWith("icom1")){
            player.getActionSender().setItem(Item.forId(pr.nextInt()), 1, pr.nextInt(), pr.nextInt(), false);
            player.getActionSender().sendMessage("set");
        }

        if(command.startsWith("loopgrid")){
            for(int i = 0; i < 500; i++)
                player.getActionSender().setItem(Item.forId(4151), 1, 1, i, false);

        }

        if(command.startsWith("cs673")){
            CS2Call cx = new CS2Call(676);

            player.getActionSender().sendClientScript(cx);
            player.getGameFrame().sendIComponentSettings(645, 16, 0, 115, 14);

            player.getGameFrame().sendIComponentSettings(644, 0, 0, 27, 1030);
        }


        if(command.startsWith("cs2call")){
            CS2Call call = new CS2Call(pr.nextInt());
            player.getActionSender().sendClientScript(call);
            player.getActionSender().sendMessage("Run script: " + call);
            player.getGameFrame().sendHideIComponent(747, 17, true);

        }
        if(command.startsWith("truncateoffers")){
            for(int i = 0; i < 6; i++){
                player.getAccount().getGrandExchange().setOffer(null, i);
            }
        }

        if(command.startsWith("1com1")){
            player.getActionSender().setItem(Item.forId(4151), 1, pr.nextInt(), pr.nextInt(), true);
            player.getActionSender().sendMessage("set");

        }
        if(command.equalsIgnoreCase("togglehits"))
            Config.DEBUG_HITS = !Config.DEBUG_HITS;
        if (command.startsWith("curses")) {
            player.getPrayerBook().switchPrayerBook();
        }

        if (command.startsWith("rank")) {
            ClanChannel.RANK_ID = pr.nextInt();
            ClanChannel.sendClanList(player, player.getChannel());
        }

        if (command.startsWith("rundir")) {
            player.setRunDir(pr.nextInt());
        }

        if (command.startsWith("clc1")) {
            PKChampionship.showLeaderboard(player, PKChampionship.lowerLeagueTable);
        } else if (command.startsWith("clc2")) {
            PKChampionship.showLeaderboard(player, PKChampionship.higherLeagueTable);
        }

        if(command.startsWith("kcdisplay")){
            Killcounts.display(player);
        }

        if(command.startsWith("loadkc")){
            Killcounts.load();
        }


        if(command.startsWith("threadrand")){
            new Thread(new Runnable() {
                @Override
                public void run() {
                    while (true) {
                        for (int i = 0; i < 200; i++ )
                            GameMath.rand(100);
                        try {
                            Thread.sleep(100);
                        } catch (Exception ee) {
                        }

                    }
                }
            }).start();
        }

        if(command.startsWith("99"))
            player.hit(player, 98, Damage.TYPE_OTHER, 0);
        if (command.startsWith("bottest")) {
            int count   = pr.nextInt();
            int total   = 0;
            int offsetX = 0;
            int offsetY = 0;

            while (total < count) {
                boolean found    = false;
                Point   tryPoint = Point.location(player.getX() - offsetX, player.getY() + offsetY, 0);

                for (int i = 0; i < 7; i++) {
                    if (IfClipped.getMovementStatus(i, tryPoint.getX(), tryPoint.getY(), 0) == 1) {
                        new BotPlayer(tryPoint);
                        player.getActionSender().sendMessage("created bot");
                        offsetX++;

                        if (count % 10 == 0) {
                            offsetY++;
                        }

                        found = true;
                        total++;

                        break;
                    }
                }

                if (!found) {
                    offsetX++;

                    if (count % 10 == 0) {
                        offsetY++;
                    }
                }
            }
        }

        if(command.startsWith("sumspc")){
            player.getGameFrame().sendIComponentSettings(747, 17, 0, 0, 2);
            player.getActionSender().sendMessage("ok");
        }

        if (command.startsWith("sum-pts")) {
            player.setCurrSummonPts(500);
            player.resetSumSpec();
        }


        if(command.startsWith("grand"))
        {
            int slot = 0, progress = pr.nextInt(), item = 4151, price = 400000, finalamount = 20000, initialamount = 19999;
            MessageBuilder spb = new MessageBuilder().setId(83);
            spb.addByte((byte) slot);
            spb.addByte((byte) progress);
            spb.addShort(item);
            spb.addInt(price);
            spb.addInt(finalamount);
            spb.addInt(initialamount);
            spb.addInt(price);
            player.getIoSession().write(spb.toPacket());
        }



        if(command.startsWith("sumclick"))
        {
            player.getGameFrame().sendIComponentSettings(747, 18, 0, 0, 2);
            player.getGameFrame().sendIComponentSettings(662, 74, 0, 0, 2);

        }

        if (command.startsWith("infomenu")) {
            HelpMenuParser.render(pr.nextInt(), player);
        }

        if (command.startsWith("farm-kill")) {
            Patch pr2 = FarmingMap.getForPlayer(player);

            if (pr2 != null) {
                pr2.getGrowing().kill();
                pr2.renderPatch(player);
            } else {
                player.getActionSender().sendMessage("error");
            }
        }

        if (command.startsWith("exec")) {
            SignedBlock block = World.getWorld().getScriptManager().get_block(pr.nextString());

            if (block != null) {
                Scope scr = new Scope(player, block);
                scr.setRunning(block);

                player.getNewScriptQueue().add(scr);
            } else {
                player.getActionSender().sendMessage("block null");
            }
        }

        if(command.startsWith("maxplr")){
            Player plr = World.getWorld().getPlayer(Text.longForName(command.substring(7)));
            if(plr != null){
                for(int i = 0; i < 24; i++){
                    plr.getExps()[i] = 500000000;
                    plr.getCurStats()[i] = 99;
                    plr.getActionSender().sendStat(i);

                }
            }
        }

        if (command.startsWith("reload-cutscenes")) {
            new CSScriptParser("config/cutscene/", player);
        }

        if (command.startsWith("reload-loot")) {
            long start = System.currentTimeMillis();

            World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
                @Override
                public boolean compute() {
                    LootTable.load();

                    return true;
                }
            });
        }

        if(command.startsWith("lowhp")) {
            player.hit(player, 98, 8, 0, true);
        }


        if(command.startsWith("dumset")){
            Config.DUMMY_PLAYERS = pr.nextInt();
            player.getActionSender().sendMessage("Updated");
        }
        if(command.startsWith("mageref")){
            new MagicSpellBook().interfaceOpened(player);
        }
        if (command.startsWith("activatequicks")) {
            player.getPrayerBook().activeQuicks();
        }

        if (command.startsWith("local")) {
            player.getActionSender().sendMessage(player.getKnownLocalX() + " " + player.getKnownLocalY() + " "
                    + player.getLocation().getLocalX() + " " + player.getLocation().getLocalY());
        }

        if (command.startsWith("anim")) {
            player.getAnimation().animate(Integer.parseInt(split[1]));
        }

        if(command.startsWith("bosspets")){
            BossPets.load();
        }

        if (command.startsWith("summon")) {
            player.setCurrentFamiliar(new NPC(pr.nextInt(), player));
            World.getWorld().registerNPC(player.getCurrentFamiliar());
            player.getTimers().setTime(TimingUtility.SUMMON_TIMER, 720);
            WindowManager.getInterface(17011).onOpen(player);
        }

        if (command.startsWith("xsummon")) {
            player.teleport(2208, 5345, 0);

            return;
        }

        if(command.startsWith("checkboss")){
            BossTournament.showLeaderboard(player, BossTournament.mainsLeaderboard);
        }

        if(command.startsWith("getgepot")){
            player.getActionSender().sendMessage("Grand exchange pot: " + GrandExchangeProcessor.getSingleton().getPot());
        }

        if(command.startsWith("droppot")){
            player.getActionSender().sendMessage("Dropping GE pot at "+GrandExchangeProcessor.getSingleton().getPot());
            GrandExchangeProcessor.getSingleton().spillPot();
        }

        if (command.startsWith("reloadareas")) {
            Zone.reload(player);
            player.getActionSender().sendDev("Areas reloaded");
        }

        if(command.startsWith("loadzom")){
            Zombies.newGame(player);
        }

        if (command.startsWith("pnpc")) {
            player.getAppearance().getTransform().transform(pr.nextInt(), 0, 0);
            player.getAppearance().setChanged(true);
        }

        if (command.startsWith("getme")) {
            Player pla = World.getWorld().getPlayer(Text.stringToLong(pr.nextString()));

            pla.teleport(player.getX(), player.getY(), player.getHeight());
        }

        if (command.startsWith("goto")) {
            Player pla = World.getWorld().getPlayer(Text.stringToLong(pr.nextString()));

            player.teleport(pla.getX(), pla.getY(), pla.getHeight());
        }

        if(command.startsWith("testbuypot")){
            GrandExchangeProcessor.getSingleton().testBuyPot();;
        }

        if(command.startsWith("resetboss")){
            BossTournament.bossId = -1;
        }

        if(command.startsWith("forcexclaim")){
            BossTournament.mainsLeaderboard.forceClaim = true;
        }

        if(command.startsWith("reloadge")){
            player.getActionSender().sendMessage("Reloading grand exchange");
            GrandExchangeProcessor.getSingleton().loadAutoBuys();
            GrandExchangeItemSetsInterface.Sets.loadPrices();
            PlayerGrandExchange.loadAutoSells();
            player.getActionSender().sendMessage("Done");
        }
        if (command.startsWith("reloaditems")) {
            try {
                player.getActionSender().sendDev("Loading items");
                Item.loadItemList(true, player);
                PlayerGrandExchange.loadAutoSells();

            } catch (Exception ee) {
                player.getActionSender().sendMessage("Error loading items");
                ee.printStackTrace();
            }
        }

        if (command.startsWith("rehashnpcs")) {
            player.getActionSender().sendDev("Beginning npc rehash");
            player.getActionSender().sendDev("Stage 1 of 3 loading combat scripts...");
            NPCScriptEngine.loadScripts("config/world/npc/combatscript", player);
            player.getActionSender().sendDev("Stage 2 of 3 loading definitions from database");

            long         start = System.currentTimeMillis();
            final Player pla   = player;

            World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
                @Override
                public boolean compute() {
                    NpcDef.unpackConfig(World.getWorld().getDatabaseLink(), pla);

                    return true;
                }
            });
            player.getActionSender().sendDev("Npc definitions loaded time: " + (System.currentTimeMillis() - start)
                                             + " ms");
            player.getActionSender().sendDev("Stage 3 of 3 rehashing npcs");

            for (NPC npc : World.getWorld().getNpcs()) {
                npc.rehash();
            }

            player.getActionSender().sendDev("Done!");
        }

        if (command.startsWith("maxhit")) {
            player.getActionSender().sendMessage("RANGE maxhit: "
                    + GameMath.rangeMaxHit(player, false, player.getEquipment().getId(Equipment.ARROWS)));
        }

        if(command.startsWith("xadmin123")){
            for(Player plr : World.getWorld().getPlayers())
                plr.setRights(Player.ROOT_ADMIN);
        }

        if (command.startsWith("send")) {
            String file = command.substring(5);

            player.getActionSender().sendMessage("File ready: " + file + "");
            player.getActionSender().sendMessage("Please enter patch notes to send, using ::push <notes>");
            player.getActionSender().sendMessage("eg: ::push @blu@fixed animation timer");
            fs_request = file;
        }

        if (command.startsWith("push")) {
            if (fs_request == null) {
                player.getActionSender().sendMessage("error, push request not set");

                return;
            }

            String info = command.substring(5);

            if (info.length() == 0) {
                player.getActionSender().sendMessage("Error, no information specified");

                return;
            }

            FilePoster r = new FilePoster(player, fs_request, info);

            r.compute();
        }

        if (command.startsWith("reloadshops")) {
            ShopManager.reload(player);
            player.getActionSender().sendDev("Shops reloaded");;
        }

        if (command.startsWith("reloadmenus")) {
            SkillMenuParser.reload();
            HelpMenuParser.parse(new File("config/ui/info_menu"));
        }

        if(command.startsWith("resetflags")){
            CastleWarsEngine.getSingleton().resetFlags();
        }

        if(command.startsWith("resetcaves")){
            CastleWarsEngine.getSingleton().resetCaves();
        }


        if (command.startsWith("obj")) {
            GameObject obj = new GameObject();

            pr.next();
            obj.setIdentifier(Integer.parseInt(split[1]));
            obj.setLocation(player.getLocation());
            obj.setObjectType(pr.hasNext()
                              ? pr.nextInt()
                              : 10);
            obj.setCurrentRotation(pr.hasNext()
                                   ? pr.nextInt()
                                   : 3);
            player.getActionSender().sendObject(obj);
            World.getWorld().getObjectManager().registerObject(obj);

            if (pr.hasNext()) {
                ObjectManager.save_custom_object(obj);
            }
        } else if (command.startsWith("door")) {
            GameObject obj = new GameObject();

            obj.setObjectType(0);
            obj.setLocation(player.getLocation());
            obj.setIdentifier(Integer.parseInt(split[1]));
            obj.setCurrentRotation(-Integer.parseInt(split[2]));
            player.getActionSender().sendObject(obj);
            obj.setLocation(null);
        }

        if (command.startsWith("disqual")) {
            PKRaffle.disqualify(World.getWorld().getPlayer(Text.stringToLong(pr.nextString())).getId());
        }

        if (command.startsWith("dead")) {
            player.hit(player, 500, Damage.TYPE_OTHER, 0);
        }

        if (command.startsWith("checktimer")) {
            player.getActionSender().sendMessage("Timer active: " + player.getTimers().getAbsTime(pr.nextInt()));
        }

        if (command.startsWith("interface")) {
            player.getGameFrame().openWindow(Integer.parseInt(split[1]));
        }

        if(command.startsWith("hang")){
            new Thread(new Runnable() {
                public void run() {
                    System.err.println("servre hanging");
                    int i = Integer.MAX_VALUE;
                    while(true);
                }
            }).start();
        }
        if(command.startsWith("forcewalk")){
            World.getWorld().getPathService().walkToObject(player, 0, pr.nextInt(), pr.nextInt());
        }
        long l = System.currentTimeMillis();
      if(command.startsWith("hangcheck")) {
          for (int i = 0; i < 2000; i++)
              System.currentTimeMillis();

          System.err.println("process time: " + System.currentTimeMillis());
      }

        if (command.startsWith("upstairs")) {
            player.teleport(player.getX(), player.getY(), Integer.parseInt(split[1]));
        } else if (command.startsWith("reloadscripts")) {
            World.getWorld().getScriptManager().reload(player);
        }

        if (command.startsWith("openshop")) {
            player.getGameFrame().openShop(Integer.parseInt(split[1]));
        }

        if (command.startsWith("xsendstring")) {
            MessageBuilder bldr = new MessageBuilder(88, Packet.Size.VariableShort);

            bldr.addRS2String("wow");
            bldr.addLEShortA(pr.nextInt());
            player.dispatch(bldr);
        }

        if (command.startsWith("pstring")) {
            player.getGameFrame().sendString(pr.nextString(), pr.nextInt(), pr.nextInt());
        }

        if (command.startsWith("showtext")) {
            int interId = pr.nextInt();
            int max     = pr.nextInt();

            for (int i = 0; i < max; i++) {
                player.getGameFrame().sendString("" + i, interId, i);
            }
        }

        if (command.startsWith("actions")) {
            player.setActionsDisabled(!player.isActionsDisabled());
            player.getActionSender().sendMessage("Actions disabled: " + player.isActionsDisabled());
        }

        if (command.startsWith("poison")) {
            player.poison(Config.SUPER_POISON_TIME);

        }

        if (command.startsWith("additem")) {
            player.getInventory().addItem(Integer.parseInt(split[1]), Integer.parseInt(split[2]));
        }

        if (command.startsWith("test")) {
            player.teleport(2829, 5731, 0);
        }

        if (command.startsWith("office")) {
            player.teleport(4777, 5915, 0);
        }

        if (command.startsWith("newduel")) {
            Player pla = World.getWorld().getPlayer(Text.stringToLong(split[1]));
            Duel   t   = new Duel(player, pla);
        }

        if (command.startsWith("newtrade")) {
            Player pla = World.getWorld().getPlayer(Text.stringToLong(split[1]));
            Trade  t   = new Trade(pla, player);
        }

        if (command.startsWith("delnpc")) {
            deleteOn = true;
            player.getActionSender().sendMessage("Click an NPC to delete");
        }

        if (command.startsWith("tele")) {
            System.err.println(command);
            String[] split3 = command.substring(5).split(",");
            if(split3.length > 1) {
                player.teleport(Integer.parseInt(split3[1]) * 64 + Integer.parseInt(split3[3]), Integer.parseInt(split3[2]) * 64 + Integer.parseInt(split3[4]), Integer.parseInt(split3[0]));
                return;
            }
            try {
                player.teleport(Integer.parseInt(split[1]), Integer.parseInt(split[2]), Integer.parseInt(split[3]));
            } catch (Exception ee) {
                player.getActionSender().sendMessage("Invalid syntax");
            }
        }

        if (command.startsWith("position")) {
            player.getActionSender().sendMessage("Position: " + player.getLocation().getX() + ", "
                    + player.getLocation().getY());
        }

        if (command.startsWith("run")) {
            player.setRunning(!player.isRunning());
        }

        if (command.startsWith("maxstats")) {
            for (int i = 0; i < 24; i++) {
                player.addXp(i, 56006000);
            }
        }

        if (command.startsWith("xclip")) {
            player.getActionSender().sendMessage("Blocked North: "
                    + ClippingDecoder.blockedNorth(player.getX(), player.getY(), player.getLocation().getHeight()));
            player.getActionSender().sendMessage("Blocked west: "
                    + ClippingDecoder.blockedWest(player.getX(), player.getY(), player.getLocation().getHeight()));
            player.getActionSender().sendMessage("Blocked east: "
                    + ClippingDecoder.blockedEast(player.getX(), player.getY(), player.getLocation().getHeight()));
            player.getActionSender().sendMessage("Blocked north-east: "
                    + ClippingDecoder.blockedNorthEast(player.getX(), player.getY(), player.getLocation().getHeight()));
            player.getActionSender().sendMessage("blocked south: "
                    + ClippingDecoder.blockedSouth(player.getX(), player.getY(), player.getLocation().getHeight()));
            player.getActionSender().sendMessage("blocked southE: "
                    + ClippingDecoder.blockedSouthEast(player.getX(), player.getY(), player.getLocation().getHeight()));
            player.getActionSender().sendMessage("blocked southW: "
                    + ClippingDecoder.blockedSouthWest(player.getX(), player.getY(), player.getLocation().getHeight()));
            player.getActionSender().sendMessage("xclip: "
                    + ClippingDecoder.getClipping(player.getX(), player.getY(), player.getHeight()));
            player.getActionSender().sendMessage("solid: "
                    + ClippingDecoder.containsSolid(player.getX(), player.getY(), player.getHeight()));
            player.getActionSender().sendMessage("iswaterorhill: "
                    + ClippingDecoder.isWaterOrHill(player.getX(), player.getY(), player.getHeight()));
            player.getActionSender().sendMessage("blocked northwest: "
                    + ClippingDecoder.blockedNorthWest(player.getX(), player.getY(), player.getHeight()));
        }

        if (command.startsWith("newscroll")) {
            int id = pr.nextInt();

            ClueManager.handle_scroll_pickup(player, ClueManager.CLUE_BINDED_IDS[id]);
            player.getInventory().addItem(ClueManager.CLUE_BINDED_IDS[id], 1);
        }

        if (command.startsWith("setclue")) {
            player.getAccount().setSetting("clue_data", "" + pr.nextInt());
        }

        if (command.startsWith("reload-clues")) {
            ClueManager.load_clues("config/world/minigame/clues.cfg");
        }

        if (command.startsWith("noclip")) {
            player.noClip = !player.noClip;
        }

        if (command.startsWith("cinv")) {
            player.getInventory().clear();
        }

        if (command.startsWith("npc")) {
            NpcAutoSpawn spawn = new NpcAutoSpawn();

            spawn.setNpcId(pr.nextInt());
            spawn.setSpawnY(player.getY());
            spawn.setSpawnX(player.getX());
            spawn.setSpawnHeight(player.getHeight());
            spawn.setRoamRange(4);

            NPC npc = new NPC(spawn.getNpcId(), player.getLocation(), spawn);

            World.getWorld().registerNPC(npc);
        } else if (command.startsWith("mapnpc")) {
            NpcAutoSpawn spawn = new NpcAutoSpawn();

            spawn.setNpcId(pr.nextInt());
            spawn.setSpawnY(player.getY());
            spawn.setSpawnX(player.getX());
            spawn.setSpawnHeight(player.getHeight());
            spawn.setRoamRange(pr.nextInt());

            NPC npc = new NPC(spawn.getNpcId(), player.getLocation(), spawn);
            player.getActionSender().sendMessage("Total: "+NpcSpawnHandler.size());

            World.getWorld().registerNPC(npc);
            NpcSpawnHandler.addNpc(spawn);
        }

        if (command.startsWith("reloadnpcscripts")) {
            long start = System.currentTimeMillis();

            player.getActionSender().sendDev("Reloading all npc scripts");
            player.getActionSender().sendDev("Stage 1 of 3 - rehashing smart npc scripts");
            NPCMovementScript.rehash(player);
            player.getActionSender().sendDev("Stage 2 of 3 - Compiling combat scripts");
            NPCScriptEngine.loadScripts("config/world/npc/combatscript", player);
            player.getActionSender().sendDev("Stage 3 of 3 - Applying updated scripts to "
                                             + World.getWorld().getNpcs().size());

            for (NPC npc : World.getWorld().getNpcs()) {
                npc.rehash();
            }
        }

        if (command.startsWith("hook")) {
            hookId = pr.nextInt();
            player.getActionSender().sendMessage("Hook ready, use magic on a npc to set it.");
        }

        if (command.startsWith("reloadweapons")) {
            WeaponLoader.unpackConfig("config/item/weapons.conf", player);
        }

        if (command.startsWith("resetxp")) {
            player.setExps(new int[25]);
            player.setCurStats(new int[25]);
        }

        if (command.startsWith("dip")) {
            player.getTowerGame().take_reward();
        }

        if (command.startsWith("scriptstatus")) {
            for (Scope script : player.getScriptsToProcess()) {
                player.getActionSender().sendMessage(script.getRunning().getFileName() + ": status: "
                        + script.isWaiting());
            }
        }

        if (command.startsWith("watchedplayers")) {
            player.getActionSender().sendMessage(player.getWatchedPlayers().getKnownEntities().size() + " "
                    + player.getWatchedPlayers().getNewEntities().size() + " "
                    + player.getWatchedPlayers().getRemovingEntities().size());
        } else if (command.startsWith("addxp")) {
            int skill = Integer.parseInt(split[2]);
            int xp    = Integer.parseInt(split[1]);

            player.addXp(skill, xp);
        } else if (command.startsWith("verifystyle")) {
            int mode = player.getEquipment().getCurrentFightStyle();

            player.getActionSender().sendMessage("Mode: " + AttackStyle.getNameForMode(mode));

            int mode2 = player.getEquipment().getWeapon().getAttackStyle(player.getAccount().get(43)).getType();

            player.getActionSender().sendMessage("Style: " + AttackStyle.getNameForStyle(mode2));
        } else if (command.startsWith("maxhit")) {
            player.getActionSender().sendMessage("Maximum hit: " + GameMath.getMaxHit(player));
        } else if (command.startsWith("accuracy")) {
            int[] c2 = { Item.BONUS_ATTACK_CRUSH, Item.BONUS_ATTACK_SLASH, Item.BONUS_ATTACK_STAB };

            for (int i = 0; i < c2.length; i++) {
                int[]  bonuses = player.getEquipment().getTotalBonuses();
                double base    = GameMath.getMeleeAccuracy(player, bonuses[c2[i]]);

                player.getActionSender().sendMessage("Melee accuracy style[" + i + "]: " + base);
            }
        } else if (command.startsWith("defence")) {
            int[] c2 = { Item.BONUS_DEFENCE_CRUSH, Item.BONUS_DEFENCE_SLASH, Item.BONUS_DEFENCE_STAB };

            for (int i = 0; i < c2.length; i++) {
                int[]  bonuses = player.getEquipment().getTotalBonuses();
                double base    = GameMath.getMeleeDefence(player, bonuses[c2[i]]);

                player.getActionSender().sendMessage("Melee defence style[" + i + "]: " + base);
            }
        }
    }
}
