package net.tazogaming.hydra.net.packethandler;//~--- non-JDK imports --------------------------------------------------------


import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.util.Text;

/**
 * @author 'Mystic Flow <Steven@rune-server.org>
 */
public final class ClanPacketHandler implements PacketHandler {
    public static final int
        JOIN     = 1,
        RANK     = 51,
        MESSAGE  = 52,
        KICK_BAN = 36;

    /**
     * Method handlePacket
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param packet
     * @param ioe
     */
    @Override
    public void handlePacket(Packet packet, Player player) {
        switch (packet.getId()) {
        case JOIN :
            String owner = "";

            if (packet.remaining() > 0) {
                owner = packet.readRS2String().toLowerCase();
            }

            if(owner.length() == 0){
                if(player.getChannel() != null) {
                    player.getChannel().leave(player);
                }
                return;
            }



            ClanChannel c = World.getWorld().getChannelList().getByPlayer(owner);

            if(c == null){
                player.getActionSender().sendMessage("Error players channel not found");
                return;
            }



            if(!c.canJoin(player))                                              {
                player.getActionSender().sendMessage("You cant join this channel");
                return;
            }

            if(player.getChannel() != null) {
                player.getChannel().leave(player);
            }
            c.addPlayer(player);
            break;

        case RANK :
            handleRank(player, packet);

            break;

        case MESSAGE :
            player.setSendingClanMsg(packet.readByte() > 0);

            break;

        case KICK_BAN :
            handleBan(player, packet);
        }
    }

    private void handleBan(Player player, Packet packet) {
        String playerName = packet.readRS2String().toLowerCase();

         if(player.getChannel() != null && player.getChannel().getRankForPlayer(player) >= player.getChannel().getRankKick()){
             if(!player.getUsername().equals(playerName)) {
                   player.getChannel().banUser(player, World.getWorld().getPlayer(Text.longForName(playerName)));
             }
         }
        // World.getWorld().getClanManager().banMember(player, playerName);
    }

    private void handleRank(Player pla, Packet packet) {

        // something is sent before the string i think, ignore it though
        String playerName = packet.readRS2String().toLowerCase();
        int    rank       = packet.readByte() + 128;



        ClanChannel sc = World.getWorld().getChannelList().getByPlayer(pla.getUsername());

        if (sc == null) {
            pla.getActionSender().sendMessage("Error you dont have a clan active");

            return;
        }

        if ((rank > -1) && (rank < ClanChannel.rankings.length)) {
            sc.updateClanRankForPlayer(Text.longForName(playerName), rank, pla);
            pla.getFriendsList().updateFriend(playerName);
        }
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
