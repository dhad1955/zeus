
/*
* Walking.java
*
* Created on 23-Dec-2007, 20:03:40
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
 */
package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.TimingUtility;

/**
 *
 * @author alex
 */
public class Walking implements PacketHandler {

    /**
     * Method blockWalking
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean blockWalking(Player player) {

        if(player.isStunned()){
            player.getActionSender().sendMessage("You are stunned");
            return true;
        }
        if (player.isActionsDisabled() || player.isDead() || (player.getCutScene() != null)
                || player.getTimers().timerActive(TimingUtility.FROZEN_TIME)) {
            player.getActionSender().sendCloseWalkingFlag();

            return true;
        }

        if (player.getGameFrame().getDuel() != null) {
            Duel duel = player.getGameFrame().getDuel();

            if (duel.setting(Duel.NO_MOVEMENT)) {
                player.getActionSender().sendCloseWalkingFlag();

                return true;
            }
        }

        return false;
    }

    /**
     * Method resetWalking
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void resetWalking(Player player) {
        if (player.getCurrentHouse() != null) {
            player.getCurrentHouse().leave_draw_mode();

            if (player.getAccount().getSmallSetting(Account.CHAIR_SITTING_ID) > 0) {
                player.getCurrentHouse().changeNode(player,
                        player.getAccount().getSmallSetting(Account.CHAIR_SITTING_ID), 0, 0);
                player.getAccount().putSmall(Account.CHAIR_SITTING_ID, 0);
            }
        }

        player.getRoute().resetPath();

        if (player.getTimers().timerActive(TimingUtility.FROZEN_TIME)) {
            player.getActionSender().sendMessage("You're frozen for another "
                    + Core.getSecondsForTicks(player.getTimers().getAbsTime(TimingUtility.FROZEN_TIME)) + " seconds");
            player.getActionSender().sendCloseWalkingFlag();
            if(player.isFollowing()){
                player.unFollow();
                player.getCombatAdapter().reset();
            }

            return;
        }

        if (player.getGameFrame().getDuel() != null) {
            Duel duel = player.getGameFrame().getDuel();

            if (duel.setting(Duel.NO_MOVEMENT)) {
                player.getActionSender().sendCloseWalkingFlag();

                return;
            }
        }

        player.setRequestedSpell(player.getAutoCast());
        player.setTradeRequest(-1);
        player.getGameFrame().close();
        player.terminateScripts();
        player.terminateActions();
        player.getFacingLocation().reset();
        player.getRequest().close();
        player.getCombatAdapter().reset();

        if (player.getFocus().getInteractingWith() != -1) {
            player.getFocus().unFocus();
        }

       // player.getAnimation().animate(65535);

        if (player.isFollowing()) {
            player.unFollow();
        }

    }

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    public void handlePacket(Packet p, Player player) {

        if (player.isActionsDisabled() || player.isDead() || (player.getCutScene() != null)) {
            player.getActionSender().sendCloseWalkingFlag();

            return;
        }


        resetWalking(player);

        int x = p.readShort();
        int y = p.readShort();

        player.requestWalk(x, y);
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
