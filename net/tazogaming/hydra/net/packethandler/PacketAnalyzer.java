package net.tazogaming.hydra.net.packethandler;

import net.tazogaming.hydra.net.Packet;

/**
 * Created by Danny on 27/12/2015.
 */
public interface PacketAnalyzer {

    public DelayPolicy getDelayPolicy(Packet p);
}
