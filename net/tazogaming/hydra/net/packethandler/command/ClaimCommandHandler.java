package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;
import net.tazogaming.hydra.util.background.BackgroundTaskCompleteListener;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/01/14
 * Time: 05:50
 */
public class ClaimCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, final Player player) {
        if(true)
            return;

        if (player.getAccount().hasVar("fb_l")) {
            player.getActionSender().sendAlert(
                "Item already claimed!",
                "You've already liked us and claimed  the reward. _ Don't worry there will be more offers like this soon!",
                2, 300);

            return;
        }

        if (player.getAccount().hasVar("cl_await")) {
            player.getActionSender().sendMessage("please wait");

            return;
        }

        player.getActionSender().sendMessage("sending claim");
        player.getAccount().setSetting("cl_await", 1);
        World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest(player) {
            @Override
            public boolean compute() {
                setRequest(new BackgroundTaskCompleteListener() {
                    @Override
                    public void requestComplete(Object request_result) {
                        player.getAccount().removeVar("cl_await");
                    }
                });

                Player player1 = (Player) getParam();

                try {
                    ResultSet r =
                        World.getWorld().getDatabaseLink().query("SELECT userid from user_likes where userid='"
                                + player.getId() + "'");

                    if (r.next()) {
                        player1.getAccount().setSetting("fb_l", 1, true);
                        player.getActionSender().sendMessage("items claimed");
                        player.getBank().insert(995, 1500000);
                        player1.getActionSender().sendAlert("Congratulations, you've received 1.5m",
                                "The money has been wired to your bank account.", 2, 400);
                        player1.getActionSender().sendMessage("Your money has been sent to your bank account.");
                        World.getWorld().getDatabaseLink().query(
                                "UPDATE user_likes set status='1' where userid='" + player.getId() + "'");
                    } else {
                        player.getActionSender().sendMessage(
                            "Unable to find, type ::fb again then try typing this command.");
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                return true;
            }
        });
    }
}
