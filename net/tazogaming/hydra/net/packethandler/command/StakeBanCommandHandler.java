package net.tazogaming.hydra.net.packethandler.command;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.security.GamblingBanList;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/11/2014
 * Time: 02:58
 */
public class StakeBanCommandHandler implements CommandHandler {
    /**
     * Method handleCommand
     * Created on 14/08/18
     *
     * @param line
     * @param params
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
            if(player.getRights() < Player.MODERATOR)
                return;

            Player playerToBan = World.getWorld().getPlayer(Text.longForName(line.substring("stakeban ".length())));
            if(playerToBan != null){
                GamblingBanList.addBan(playerToBan.getUID());
                playerToBan.getActionSender().sendMessage(Text.RED("You have been permanently banned from staking."));
                player.getActionSender().sendMessage("Player banned from staking");
            }


    }
}
