package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/03/14
 * Time: 14:09
 */
public class ResetPinCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() < Player.MODERATOR) {
            return;
        }

        try {
            Player plr_to_mute = World.getWorld().getPlayer(Text.longForName(params.nextString()));

            if (plr_to_mute == null) {
                player.getActionSender().sendMessage("Unable to find the player.");

                return;
            }

            plr_to_mute.getAccount().removeVar("bank_pin");
            plr_to_mute.getActionSender().sendMessage("Your bank pin has been reset");
        } catch (Exception ee) {}
    }
}
