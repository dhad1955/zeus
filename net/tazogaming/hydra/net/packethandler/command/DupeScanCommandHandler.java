package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.util.ArgumentTokenizer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/12/13
 * Time: 00:07
 */
public class DupeScanCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() < Player.MODERATOR) {
            return;
        }

        int cashAmount = 100;

        for (Player plr : World.getWorld().getPlayers()) {
            double torvaCount = 0.0;
            int    total      = 0;

            if (plr.getInventory().hasItem(18652, 1)) {
                total += plr.getInventory().getStackCount(18652);
            }

            if (plr.getInventory().hasItem(5608, 1)) {
                player.getActionSender().sendMessage("Fox detected :" + plr.getUsername());
            }

            torvaCount += (player.getInventory().countItems(13362) * 0.33);
            torvaCount += (player.getInventory().countItems(13358) * 0.33);
            torvaCount += (player.getInventory().countItems(13360) * 0.35);

            if (plr.getBank().hasItem(18652, 1)) {
                total += plr.getBank().getAmount(Item.forId(18652));
            }

            int c = 0;

            for(int x = 1038; x <= 1050; x++){
                c+= plr.getBank().getAmount(Item.forId(x));
            }
            if(c > 5)
                player.getActionSender().sendMessage("TOTAL RARES: "+c+" "+plr.getUsername());

            if (torvaCount > 1) {
                player.getActionSender().sendMessage("Torva detected[" + plr.getUsername() + "]");
            }

            if (plr.getBank().hasItem(5608, 1)) {
                player.getActionSender().sendMessage("FOX DETECTED[BANK]: " + plr.getUsername());
            }

            int santaAmt = plr.getBank().getAmount(Item.forId(13537));

            if(santaAmt > 0)
                player.getActionSender().sendMessage("[BLACK PHATS DETECTED] x "+santaAmt+" "+plr.getUsername());
             santaAmt = plr.getBank().getAmount(Item.forId(19449));

            if(santaAmt > 0)
                player.getActionSender().sendMessage("[BLACK SANTA DETECTED] x "+santaAmt+" "+plr.getUsername());
            if (total >= cashAmount) {
                player.getActionSender().sendMessage(plr.getUsername() + " has " + total + " bank notes");
            }
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
