package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.ui.TextInputRequest;
import net.tazogaming.hydra.security.BanList;
import net.tazogaming.hydra.security.Security;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/12/13
 * Time: 09:58
 */
public class BlacklistCommandHandler implements CommandHandler {
    public static boolean BLACKLIST_ON = false;

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() < Player.MODERATOR) {
            return;
        }

        String str = params.nextString();

        if (str.startsWith("*")) {
            str = str.substring(1);
            BanList.addBan(Long.parseLong(str), player);

            return;
        }

        if (str.startsWith("-") && (player.getRights() >= Player.COMMUNITY_MANAGER)) {
            str = str.substring(1);
            BanList.removeBan(str.replaceAll("_", " "), player);

            return;
        }

        if (str.equalsIgnoreCase("list")) {
            BanList.listBans(player);

            return;
        }

        final String name = str;

        player.getGameFrame().setRequest(new TextInputRequest() {
            @Override
            public void onResponse(Player player, String response) {
                Player plr = World.getWorld().getPlayer(Text.longForName(name));



                World.getWorld().getDatabaseLink().query("UPDATE user set banned=1 where id='" + plr.getId() + "'");
                World.getWorld().getDatabaseLink().query("INSERT Into userban (id, userid, bannedby, date, reason, proof, lifton) VALUES(null, " + plr.getId() + ", '" + player.getId() + "', NOW(), '"+ Security.addSlashes(response)+"', 'NO PROOF', 0)");
                BanList.addBan(plr, player);
            }
        }, "Please enter a reason for blacklist");




        // To change body of implemented methods use File | Settings | File Templates.
    }
}
