package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/01/14
 * Time: 19:03
 */
public class VoteClaimCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, final Player player) {
        if (player.getAccount().hasVar("vote_wait")) {
            player.getActionSender().sendMessage("Please wait.");

            return;
        }

        player.getActionSender().sendMessage("Checking votes");
        World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
            @Override
            public boolean compute() {

                try {
                    ResultSet result = World.getWorld().getDatabaseLink().query("SELECT * FROM vote "
                            + "where userid='" + player.getId() + "' and claimed='1'");
                    int count = 0;

                    while (result.next()) {
                        count++;
                    }

                    player.getActionSender().sendMessage("Total votes: "+count);
                    if (count > 0) {
                        World.getWorld().getDatabaseLink().executeUpdate(
                            "UPDATE vote set claimed='2' where claimed='1' and userid='" + player.getId() + "'");


                        if(player.getRights() >= Player.PATRON)
                            count *= 2;

                        player.addPoints(Points.VOTE_POINTS, count);
                        player.getActionSender().sendMessage(
                            "You have added " + count
                            + " vote points to your vote balance you now have "+player.getPoints(3)+" vote points");
                        player.getActionSender().sendMessage("Head to varrock to find the wise old man to claim your rewards.");


                       Scope scope = new Scope();
                       scope.setController(player);
                       scope.setRunning(World.getWorld().getScriptManager().get_block("vote_reminder"));
                       World.getWorld().getScriptManager().runScript(scope);
                    } else {
                        player.getActionSender().sendMessage(
                            "Unable to find your votes in the system, are you sure you voted?");
                    }
                    result.close();
                    return true;
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                return true;
            }
        });

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
