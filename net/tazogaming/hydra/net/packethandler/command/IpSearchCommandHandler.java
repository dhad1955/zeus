package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/05/14
 * Time: 16:03
 */
public class IpSearchCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, final Player player) {
        if (player.getRights() < Player.MODERATOR) {
            return;
        }

        long uid = 0;

        if (line.startsWith("dscan")) {
            player.getActionSender().sendMessage("Searching logins for username: " + line.substring(6));




            final String ll = line;



            World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
                @Override
                public boolean compute() {
                    ResultSet r =
                        World.getWorld().createDatabaseLink().query("SELECT * from logins where username='"
                                + ll.substring(6) + "" + "'");

                    try {
                        boolean found = false;

                        while (r.next()) {
                            found = true;
                            player.getActionSender().sendMessage("@dre@[SCAN]@bla@: " + r.getString("username")
                                    + "@" + r.getString("ip"));

                            ResultSet ar =
                                World.getWorld().getDatabaseLink().query("SELECT * from logins where uid='"
                                        + r.getString("uid") + "'");

                            while (ar.next()) {
                                player.getActionSender().sendMessage("> " + ar.getString("username"));
                            }

                            ar.close();
                        }

                        if (!found) {
                            player.getActionSender().sendMessage("Could not find any logins for Username: "
                                    + ll.substring(5));
                        }
                    } catch (Exception ee) {
                        ee.printStackTrace();
                        player.getActionSender().sendMessage("Error");
                    } finally {
                        try {
                            r.close();
                        } catch (Exception ee) {}
                    }
                    return true;
                }
            });

            return;
        }

        if (!line.startsWith("alt2")) {
            final Player target_player = World.getWorld().getPlayer(Text.stringToLong(line.substring(4)));

            if (target_player == null) {
                player.getActionSender().sendMessage(
                    "Target player doesn't exist, use ::alt2 to search UID from database");

                return;
            } else {
                uid = target_player.getUID();
            }
        } else {
            player.getActionSender().sendMessage("Searching database entry for user.");

            ResultSet r =
                World.getWorld().createDatabaseLink().query("SELECT ipaddress from user where username='"
                        + line.substring(5) + "'");

            try {
                if (r.next()) {
                    uid = Long.parseLong(r.getString("ipaddress"));
                } else {
                    player.getActionSender().sendMessage("Unable to find entry in database: " + line.substring(4));
                    r.close();

                    return;
                }
            } catch (Exception ee) {
                player.getActionSender().sendMessage("Unable to find player");
            } finally {
                try {
                    r.close();
                } catch (Exception ee) {}
            }
        }

        final long uid2 = uid;

        player.getActionSender().sendMessage("Submitting request");
        World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
            @Override
            public boolean compute() {
                ResultSet r = World.getWorld().createDatabaseLink().query("SELECT * from logins where uid='"
                        + uid2 + "'");

                try {
                    boolean found = false;

                    while (r.next()) {
                        found = true;
                        player.getActionSender().sendMessage("@dre@[SCAN]@bla@: " + r.getString("username"));
                    }

                    if (!found) {
                        player.getActionSender().sendMessage("Could not find any logins for UID: " + uid2);
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                    player.getActionSender().sendMessage("Error");
                } finally {
                    try {
                        r.close();
                    } catch (Exception ee) {}
                }
                return true;
            }
        });
    }
}
