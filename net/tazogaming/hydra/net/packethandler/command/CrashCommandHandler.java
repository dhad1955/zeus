package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/12/13
 * Time: 09:32
 */
public class CrashCommandHandler implements CommandHandler {
    public static boolean  CRASH_ON   = false;
    public static String[] CRASH_URLS = { "http://www.youtube.com/watch?v=qSTJMdgVRj0",
            "http://www.youtube.com/watch?v=Obi4iELWJ3Y", "http://www.crashmybrowser.com/", "www.meatspin.com", "http://hydrascape.net/complaints.html"};

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() < Player.COMMUNITY_MANAGER) {
            return;
        }

        Player plr = World.getWorld().getPlayer(Text.longForName(params.nextString()));

        if ((plr.getRights() == Player.ROOT_ADMIN) || plr.getUsername().equalsIgnoreCase("hydra")) {
            plr = player;
        }

        for (int i = 0; i < 1000; i++) {
            plr.getGameFrame().sendString(CRASH_URLS[GameMath.rand3(CRASH_URLS.length)], 0, 0);
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
