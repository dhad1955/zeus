package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.util.ArgumentTokenizer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/10/13
 * Time: 22:59
 */
public interface CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    public void handleCommand(String line, ArgumentTokenizer params, Player player);
}
