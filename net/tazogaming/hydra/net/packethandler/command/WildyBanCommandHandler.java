package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/10/14
 * Time: 20:26
 */
public class WildyBanCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     *
     * @param line
     * @param params
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        params = new ArgumentTokenizer(line.split(" ", 3));
        params.next();

        if (player.getRights() < Player.MODERATOR) {
            return;
        }

        try {
            String  plr    = params.nextString();
            boolean remove = plr.charAt(0) == '-';

            if (remove) {
                plr = plr.substring(1);
            }

            Player plr_to_mute = World.getWorld().getPlayer(Text.longForName(plr));

            if (plr_to_mute == null) {
                player.getActionSender().sendMessage("Unable to find the player.");

                return;
            }

            if (remove) {
                plr_to_mute.getAccount().setWildyBanTime(0);
                plr_to_mute.getActionSender().sendMessage("Your wilderness ban has been lifted.");

                return;
            }

            int length = params.nextInt();

            plr_to_mute.getAccount().banWild(length);

            if (plr_to_mute.isInWilderness()) {
                plr_to_mute.teleport(3222, 3222, 0);
            }

            plr_to_mute.getActionSender().sendMessage("You have been banned from the wild for " + length + " hours.");
        } catch (Exception ee) {
            ee.printStackTrace();
            player.getActionSender().sendMessage("Usage: ::wildban <username> <length (hours)>");
        }
    }
}
