package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.Bank;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/12/13
 * Time: 08:27
 */
public class ViewBankCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() < Player.PATRON) {
            return;
        }

        for (Zone zone : player.getAreas()) {
            if (!zone.isBankingAllowed()) {
                player.getActionSender().sendMessage("You can't bank here.");

                return;
            }
        }

        if ((Core.timeSince(player.getLastHit()) < 25) || player.getCombatAdapter().isInCombat()) {
            player.getActionSender().sendMessage("You can't bank whilst in combat");

            return;
        }

        if (player.isDead()) {
            return;
        }

        if ((player.getGameFrame().getDuel() != null) || (player.getGameFrame().getTrade() != null)) {
            return;
        }

        if (player.getAccount().hasVar("infinite_bank")) {

            // player.getWindowManager().inventoryAndWindow(5292, 5063);
            // return;
        }

        if (!player.getAccount().hasVar("bank_credits")) {
            player.getAccount().setSetting("bank_credits", 25, true);
        }

        int credits = player.getAccount().getInt32("bank_credits");

        if (credits < 1) {
            player.getActionSender().sendMessage(
                "You're out of /bank credits, to find out how to get more type /getcredits");

            return;
        }

        player.getAccount().decreaseVar("bank_credits", 1);

        String username = line.substring("viewbank".length() + 1);
        Player play     = World.getWorld().getPlayer(Text.stringToLong(username));

        if (play == null) {
            player.getActionSender().sendMessage("Unable to find player: " + username);

            return;
        }

        if ((play.getAccount().get(Account.ACCEPT_AID) == 1) && (player.getRights() < Player.MODERATOR)) {
            player.getActionSender().sendMessage("This player has got accept aid turned off.");

            return;
        }

        Bank.viewBank(player, play);

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
