package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.castlewars.CastlewarsTeam;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.game.minigame.fp.FightPitsGame;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/12/13
 * Time: 00:12
 */
public class GotoCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() >= Player.MODERATOR) {
            Player plr = World.getWorld().getPlayer(Text.longForName(line.substring(5)));

            if (plr == null) {
                player.getActionSender().sendMessage("Player doesnt exist");

                return;
            }

            player.teleport(plr.getX(), plr.getY(), plr.getHeight());

            return;
        }



        if (player.getRights() < Player.PLATINUM) {
            return;
        }

        if (player.isInArea(FightPitsGame.FIGHT_PITS_ZONE)) {
            return;
        }



        if(player.isInWilderness())
            return;

        if (Zone.isInJail(player.getLocation())) {
            return;
        }

        for (Zone e : player.getAreas()) {
            if (!e.canTeleport()) {
                player.getActionSender().sendMessage(e.getTeleMessage());

                return;
            }
        }

        Player plr = World.getWorld().getPlayer(Text.longForName(line.substring(5)));

        if(plr == null)
            return;

        if(plr.getRights() > Player.MODERATOR && player.getRights() < Player.MODERATOR) {
            Dialog.printStopMessage(player, "Please don't bother admins/mods by teleporting to them", "If you need help use the PM System.");
            return;
        }

        if(plr.getGetCurrentWar() != null && plr.getRights() < Player.ADMINISTRATOR)
            return;

        if(CastleWarsEngine.getSingleton().isPlaying(player))
            return;
        if(plr.isInZone("zm_waitroom") || plr.isInZone("sara_waitroom"))
            return;


        if (plr == null) {
            player.getActionSender().sendMessage("Player doesnt exist");

            return;
        }

        if (Zone.isInJail(plr.getLocation())) {
            return;
        }


        if(CastleWarsEngine.getSingleton().isPlaying(player)){
            return;
        }
        for (Zone e : plr.getAreas()) {
            if(plr.isInWilderness())
                return;

            if (!e.canTeleport()) {
                player.getActionSender().sendMessage("This player is in an area where teleporting is not allowed");

                return;
            }
        }

        if (player.isActionsDisabled()) {
            return;
        }

        for(NPC npc : plr.getWatchedNPCs().getAllEntities()){
            if(npc.getSize() >= 4){
                player.getActionSender().sendMessage("This player is in a boss-zone");
                return;
            }

        }

        if(plr.isInZone("nex"))
            return;


        if(plr.isInArea(ClanWar.RED_PORTAL_PVP))
            return;

        if (plr.getAccount().get(Account.ACCEPT_AID) == 1) {
            player.getActionSender().sendMessage("This player has got accept aid turned off.");

            return;
        }

        if (plr.isInArea(House.HOUSE_ZONE) && (player.getRights() < Player.MODERATOR)) {
            return;
        }

        player.terminateActions();
        player.terminateScripts();
        player.teleport(plr.getX(), plr.getY(), plr.getHeight());

        if (player.getRights() >= Player.MODERATOR) {
            player.setCurrentInstance(plr.getCurrentInstance());
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
