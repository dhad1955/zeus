package net.tazogaming.hydra.net.packethandler.command;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/06/2015
 * Time: 21:40
 */
public class RestrainCommandHandler implements CommandHandler {
    /**
     * Method handleCommand
     * Created on 14/08/18
     *
     * @param line
     * @param params
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if(player.getRights() < Player.MODERATOR)
            return;

        String playername = params.nextString();
        String playerToRestrain = params.nextString();

        boolean remove = false;
        if(playerToRestrain.startsWith("-")){
            remove = true;
            playerToRestrain = playerToRestrain.substring(1);
        }
        Player plr = World.getWorld().getPlayer(Text.longForName(playername));
        Player restraint = World.getWorld().getPlayer(Text.longForName(playerToRestrain));


        if(!remove) {
            restraint.getAccount().restrain(player, plr);
            player.getActionSender().sendMessage("Restraining order placed..");

        }else {
            restraint.getAccount().removeRestrain(plr);
            player.getActionSender().sendMessage("Restraint removed");
            restraint.getActionSender().sendMessage("You are no longer restrained from fighting "+plr.getUsername());
        }

    }
}
