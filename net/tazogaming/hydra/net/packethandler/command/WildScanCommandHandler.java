package net.tazogaming.hydra.net.packethandler.command;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/10/14
 * Time: 13:55
 */
public class WildScanCommandHandler implements CommandHandler {
    /**
     * Method handleCommand
     * Created on 14/08/18
     *
     * @param line
     * @param params
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if(player.getRights() < Player.MODERATOR) {
            return;
        }
        for(Player player2 : World.getWorld().getPlayers()) {
            if(Combat.getWildernessLevel(player2) > 1 && player2.isInWilderness()) {
                player.getActionSender().sendMessage(Text.RED(player2.getUsername())+" "+Text.BLACK(" - ")+" "+Text.BLUE("Level: "+ Combat.getWildernessLevel(player2)));
            }
        }
    }
}
