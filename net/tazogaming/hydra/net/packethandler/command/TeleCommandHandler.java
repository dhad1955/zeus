package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.instr.condition.IfTeleBlock;
import net.tazogaming.hydra.util.ArgumentTokenizer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/06/14
 * Time: 00:02
 */
public class TeleCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if ((player.getRights() < Player.COMMUNITY_MANAGER) && !player.getAccount().hasVar("tele_command")) {
            return;
        }

        String[] split  = line.split(" ");
        int      tele_x = Integer.parseInt(split[1]);
        int      tele_y = Integer.parseInt(split[2]);

        if (new IfTeleBlock().onCondition(player, null, null)) {
            return;
        }

       // player.teleport(tele_x, tele_y, 0);
    }
}
