package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/10/14
 * Time: 20:31
 */
public class ApproveCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     *
     * @param line
     * @param params
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() < Player.MODERATOR) {
            return;
        }

        ClanChannel channel = World.getWorld().getChannelList().getByPlayer(params.nextString());

        if (channel == null) {
            player.getActionSender().sendMessage("Players channel does not exist");

            return;
        }

        channel.setApproved(params.nextString().equalsIgnoreCase("true"));

        if (channel.isApproved()) {
            Core.globalMessages.add(Text.DARK_RED(channel.getName() + " Is now an approved Hydrascape clan!"));
            channel.message("ClanServer", 11, channel.getName() + " is now an approved clan! Congratulations");
        } else {
            Core.globalMessages.add(Text.DARK_RED("The clan '" + channel.getName()
                    + "' has been removed from the list of approved clans."));
        }
    }
}
