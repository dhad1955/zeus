package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/12/13
 * Time: 09:20
 */
public class MuteCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        params = new ArgumentTokenizer(line.split(" ", 3));
        params.next();

        if (player.getRights() < Player.OFFICIAL_HELPER) {
            return;
        }

        try {
            Player plr_to_mute = World.getWorld().getPlayer(Text.longForName(params.nextString()));

            if (plr_to_mute == null) {
                player.getActionSender().sendMessage("Unable to find the player.");

                return;
            }

            int length = params.nextInt();

            if(length > 48)
                length = 48;
            if(length > 1 && player.getRights() == Player.OFFICIAL_HELPER)
            {
                player.getActionSender().sendMessage("You can only mute people for a maximum of one hour.");
                return;

            }
            plr_to_mute.getAccount().mute(length);
            plr_to_mute.getActionSender().sendMessage("You have been muted for "+length+" hours.");
        } catch (Exception ee) {
            ee.printStackTrace();
            player.getActionSender().sendMessage("Usage: ::mute <username> <length (hours)>");
        }
    }
}
