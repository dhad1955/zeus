package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/11/13
 * Time: 07:54
 */
public class BankCommandHandler implements CommandHandler {

    /**
     * Method getMaxBankCredits
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static int getMaxBankCredits(Player player) {
        switch (player.getRights()) {
        case Player.NORMAL_USER :
            return 25;

        case Player.DONATOR :
            return 80;

        case Player.PLATINUM :
            return 150;

        case Player.PATRON :
        case Player.OFFICIAL_HELPER :
        case Player.EXTREME_DONATOR :
            return 250;

        case Player.MODERATOR :
            return 254;
        }

        return 25;
    }

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if(player.getRights() < Player.ADMINISTRATOR)
            return;

        if (player.getRights() != Player.ADMINISTRATOR) {
            for (Zone zone : player.getAreas()) {
                if (!zone.isBankingAllowed() &&!player.isInArea(ClanWar.CLAN_WARS_HOME)) {
                    player.getActionSender().sendMessage("You can't bank here.");

                    return;
                }
            }

            if (!player.isInArea(ClanWar.CLAN_WARS_HOME) && player.isInWilderness()) {
                return;
            }

            if ((Core.timeSince(player.getLastHit()) < 25) || player.getCombatAdapter().isInCombat()) {
                player.getActionSender().sendMessage("You can't bank whilst in combat");

                return;
            }

            if (player.isDead()) {
                return;
            }

            if ((player.getGameFrame().getDuel() != null) || (player.getGameFrame().getTrade() != null)) {
                return;
            }

            if (player.getAccount().hasVar("infinite_bank")) {
                player.getWindowManager().inventoryAndWindow(5292, 5063);

                return;
            }

            if (!player.getAccount().hasVar("bank_credits")) {
                player.getAccount().setSetting("bank_credits", 25, true);
            }

            int credits = player.getAccount().getInt32("bank_credits");

            if (credits < 1) {
                player.getActionSender().sendMessage(
                    "You're out of /bank credits, to find out how to get more type /getcredits");

                return;
            }

            player.getAccount().decreaseVar("bank_credits", 1);
            player.getActionSender().sendMessage("You now have " + credits + "/" + getMaxBankCredits(player)
                    + " bank credits remaining.");
            player.getActionSender().changeLine("Open bank (" + player.getAccount().getInt32("bank_credits")
                    + " remaining", 39959);

            if (!player.getAccount().hasVar("pin_entered") && player.getAccount().hasVar("bank_pin")) {
                if (player.getTimers().isDated(TimingUtility.DATED_TIMER_PIN_BLOCK)) {
                    Dialog.printStopMessage(
                        player,
                        "You have been stopped from using the bank for "
                        + (1 + player.getTimers().getMinutesRemaining(TimingUtility.DATED_TIMER_PIN_BLOCK)) + " mins");

                    return;
                }

                player.getWindowManager().showWindow(7424);

                return;
            }
        }

        player.getGameFrame().openWindow(762);
    }
}
