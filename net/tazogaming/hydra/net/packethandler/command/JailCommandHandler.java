package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/06/14
 * Time: 02:07
 */
public class JailCommandHandler implements CommandHandler {

    /**
     * Method handleCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     * @param params
     * @param player
     */
    @Override
    public void handleCommand(String line, ArgumentTokenizer params, Player player) {
        if (player.getRights() >= Player.MODERATOR) {
            Player plr = World.getWorld().getPlayer(Text.longForName(line.substring(5)));

            if (plr == null) {
                player.getActionSender().sendMessage("Player doesnt exist");

                return;
            }

            plr.getRoute().resetPath();
            plr.teleport(2606, 3105, 0);
            plr.getActionSender().sendMessage("You have been jailed");
        }
    }
}
