package net.tazogaming.hydra.net.packethandler.command;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.SystemUpdate;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator Enchanta is a Runescape 2 Server emulator which
 * has been designed for educational purposes only Created by Daniel Hadland
 * Date: 21/10/13 Time: 23:00
 */
public class YellCommandHandler implements CommandHandler {

	/**
	 * Method handleCommand Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param line
	 * @param params
	 * @param player
	 */
	@Override
	public void handleCommand(String line, ArgumentTokenizer params, Player player) {
		String chat = line.substring(5);
		if (player.getAccount().isMuted()) {
			player.getActionSender().sendMessage("You are muted.");
			return;
		}

		if (SystemUpdate.isActive() && player.getRights() < Player.OFFICIAL_HELPER) {
			player.getActionSender().sendMessage("You cannot yell whilst an update is due.");
			return;
		}

		if (player.getTimers().timerActive(TimingUtility.YELL_TIMER) && player.getRights() < Player.EXTREME_DONATOR || player.getRights() == Player.GOLDEN_YOUTUBER || player.getRights() == Player.PROMOTER) {
			player.getActionSender().sendMessage("You can only yell once every 45 seconds.");
			player.getActionSender().sendMessage("Join Clan-chat 'hydra' for general chat.");
			return;
		}

		if (Core.currentTimeMillis() - player.getAccount().getYellBanTime() < 0) {
			player.getActionSender().sendMessage("You are banned from yell");
			return;
		}

		if (chat.contains("bug") && player.getRights() < Player.OFFICIAL_HELPER) {
			player.getActionSender().sendMessage("Please do not report bugs in yell, use the forums for help!");
			return;
		}

		if (chat.contains("fuck") || chat.contains("shit")) {
			player.getActionSender().sendMessage("Please do not use vulgar language in yell.");
			return;
		}

		if (chat.contains("quit")) {
			Dialog.printStopMessage(player, "Don't announce if your quitting in yell",
					"Say your goodbyes on the forums.");
			return;
		}

		if (chat.contains("<col") || chat.contains("<shad") || chat.contains("<img")) {
			return;
		}

		Core.yell(player.getUsername(), player.getRights(), player.getAccount().getIronManMode(), chat,
				player.getAccount().hasVar("y_title") ? player.getAccount().getString("y_title") : "");
		player.getTimers().setTime(TimingUtility.YELL_TIMER, 75);

	}
}
