package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketAnalyzer;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/07/14
 * Time: 16:58
 */
public class InterfaceAction implements PacketHandler, PacketAnalyzer {


    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.NEXT_TICK;
    }
    /** BUTTON_PACKET_IDS made: 14/08/21 */
    private static final int[] BUTTON_PACKET_IDS = {
        6, 13, 0, 15, 46, 67, 82, 39, 73, 58
    };

    private int getMenuOptionIndex(int opcode) {
        for (int i = 0; i < BUTTON_PACKET_IDS.length; i++) {
            if (BUTTON_PACKET_IDS[i] == opcode) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     * @param packet
     * @param player
     */
    @Override
    public void handlePacket(Packet packet, Player player) {
        final int interfaceId = packet.readShort();
        final int buttonId    = packet.readShort();
        final int slot        = packet.readLEShortA();
        final int itemId      = packet.readShort();

        if(packet.getId() == 58){
            if(interfaceId == 747 && buttonId == 7){
                player.getGameFrame().setTab(8, 880, true);
                player.getActionSender().sendBConfig(168, 8);
            }
            if(itemId > 0){
                Item i = Item.forId(itemId);
                if(i != null){
                   if(interfaceId == GameFrame.INTER_INVENTORY){
                       if(i.isStackable()){
                           player.getActionSender().sendMessage(i.getName()+" x "+player.getInventory().getStackCount(i.getIndex()));
                           return;
                       }
                   }
                    player.getActionSender().sendMessage(i.getExamine());
                    return;
                }
            }
        }

        if (player.getRights() >= Player.ADMINISTRATOR) {
            player.getActionSender().sendDev("interfaceid: " + interfaceId + " child: " + buttonId + ", slot: " + slot
                                             + ", context-slot: " + getMenuOptionIndex(packet.getId()));
        }

        if (player.isActionsDisabled()) {
            return;
        }

        final int contextSlot = getMenuOptionIndex(packet.getId());


        if(interfaceId == 1070 && buttonId == 7){
            player.getGameFrame().setTab(3, 1071, true);
            return;
        }
        if (interfaceId == 750) {
            if (contextSlot == 0) {
                if (player.getAccount().getButtonConfig(119) == 0) {
                    player.getAccount().setB(119, 1, true);
                } else {
                    player.getAccount().setB(119, 0, true);
                }
            }
        } else if (interfaceId == 916) {
            player.getGameFrame().getCurrentProductRequest().onButton(buttonId);
        } else if (interfaceId == 751) {
            int pSetting = -1;
            if (buttonId == 18) {
                pSetting = 3;
            } else if (buttonId == 12) {
                pSetting = 4;
            }

            if ((pSetting != -1) && (contextSlot > 0)) {
                player.setPrivacySetting(pSetting, contextSlot - 1);

                return;
            }
        } else if (interfaceId == 548) {
            if (buttonId == 0) {
                if (contextSlot == 7) {
                    player.setXPCounter(0);
                    player.getActionSender().sendVar(1801, 0);
                }
            }
        } else if ((interfaceId == 746) && (buttonId == 225)) {
            if (contextSlot == 7) {
                player.setXPCounter(0);
                player.getActionSender().sendVar(1801, 0);
            }
        }

        if(interfaceId == 662 && buttonId == 74 || (interfaceId == 747 && buttonId == 17)){
            if(player.getCurrentFamiliar() != null)
            {
                Summoning.summonSpecialClick(player.getCurrentFamiliar(), player);
                return;
            }
        }
        if (interfaceId == 749) {
            if (contextSlot == 1) {
                if (player.getAccount().getButtonConfig(181) == 0) {
                    player.getAccount().setB(181, 1, true);
                    player.getAccount().setB(168, 6, true);
                    player.getGameFrame().sendAMask(0, 29, 271, 42, 0, 2);
                } else {
                    player.getAccount().setB(181, 0, true);
                }
            } else if (contextSlot == 0) {
                player.getPrayerBook().activeQuicks();
            }
        }

        if (itemId != -1 && interfaceId != 105) {
            player.getGameFrame().itemClick(interfaceId, itemId, slot, getMenuOptionIndex(packet.getId()), player);
        } else {
            player.getGameFrame().actionButton(interfaceId, buttonId, slot, getMenuOptionIndex(packet.getId()), player);


            Trigger trigger = World.getWorld().getScriptManager().getInterfaceTrigger(interfaceId, buttonId,
                                  contextSlot, slot);

            if (trigger != null) {
                World.getWorld().getScriptManager().do_trigger(player, trigger, new ScriptVariableInjector() {
                    @Override
                    public void injectVariables(Scope scope) {
                        scope.declareVar(ScriptRuntime.ARGS[0], new ScriptVar(ScriptRuntime.ARGS[0], interfaceId));
                        scope.declareVar(ScriptRuntime.ARGS[1], new ScriptVar(ScriptRuntime.ARGS[1], buttonId));
                        scope.declareVar(ScriptRuntime.ARGS[2], new ScriptVar(ScriptRuntime.ARGS[2], contextSlot));
                        scope.declareVar(ScriptRuntime.ARGS[0], new ScriptVar(ScriptRuntime.ARGS[3], slot));
                    }
                });
            }
        }
    }

    @Override
    public DelayPolicy getDelayPolicy(Packet packet) {
        final int interfaceId = packet.readShort();
        final int buttonId    = packet.readShort();
        final int slot        = packet.readLEShortA();
        final int itemId      = packet.readShort();
        packet.reset();

        if(itemId != -1) {
            return DelayPolicy.NEXT_TICK;
        } else {
            return DelayPolicy.NEXT_TICK;        }


    }
}
