package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.script.runtime.instr.ConditionMap;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/10/13
 * Time: 18:51
 */
public class MagicOnInvItem implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player player) {
        int    slot     = p.readShort();
        int    item     = p.readShortA();
        int    e3       = p.readShort();
        int    spell    = p.readShortA();
        Item   item_def = player.getInventory().getItem(slot);

        player.terminateActions();
        player.terminateScripts();
        player.unFollow();
        player.getFocus().unFocus();

        if (player.isInZone("champions_basement")) {
            return;
        }

        Item i = item_def;

        if (i == null) {
            return;
        }

        if (i.isDungItem() || ClueManager.isClue(i.getIndex()) || i.isDegradeable() || i.isLend()) {
            player.getActionSender().sendMessage("Alching this would be a very foolish thing to do.");

            return;
        }

        if ((spell == 1178) || (spell == 1162)) {
            if (highAlchemy(player, slot, (spell == 1178)
                                          ? 1
                                          : 0)) {
                return;
            }
        }

        if (!World.getWorld().getScriptManager().directTrigger(player, null, Trigger.MAGIC_ON_ITEM, spell,
                item_def.getIndex())) {
            player.getActionSender().sendMessage("Nothing interesting happens..");
        }
    }

    private boolean highAlchemy(final Player player, int slot, int type) {
        Item i = player.getInventory().getItem(slot);

        if (player.getTimers().timerActive(5)) {
            return true;
        }

        int req = 55;

        if (type == 0) {
            req = 21;
        }

        if (player.getCurStat(6) < req) {
            player.getActionSender().sendMessage("You need a magic level of atleast " + req + " to cast this spell.");

            return true;
        }

        player.getTimers().setTime(5, 4);
        player.setActionsDisabled(true);
        player.getTickEvents().add(new PlayerTickEvent(player, 4) {
            @Override
            public void doTick(Player owner) {
                player.getActionSender().changeSidebar(7);
                player.setActionsDisabled(false);
                terminate();
            }
        });

        int fireRuneAmount = 5;

        if (type == 0) {
            fireRuneAmount = 3;
        }

        if (!player.getInventory().hasItem(561, 1)) {
            player.getActionSender().sendMessage("You don't have enough nature runes to cast this spell");
            player.setActionsDisabled(false);

            return true;
        }

        if (!player.getInventory().hasItem(554, fireRuneAmount)
                &&!ConditionMap.get("hasfirestaff").onCondition(player, null, null)) {
            player.getActionSender().sendMessage("You don't have enough fire runes to cast this spell.");

            return true;
        }

        if (i != null) {
            if ((i.getIndex() == 995) || (i.getPrice() == 0)) {
                player.setActionsDisabled(false);
                player.getActionSender().sendMessage("You can't cast this spell on this item.");

                return true;
            }
        }

        player.getInventory().deleteItem(561, 1);
        player.getInventory().deleteItem(554, fireRuneAmount);

        if ((player.getInventory().getFreeSlots(995) == 0) && (player.getInventory().getStackCount(slot) > 1)) {
            player.getActionSender().sendMessage("You don't have enough space.");

            return true;
        }

        player.getInventory().deleteItemFromSlot(i, 1, slot);

        int amt = i.getPrice();

        if (type == 0) {
            amt /= 2;
        } else {}

        int val = 1500 + (i.getPrice() / 10);

        if (val > 30000) {
            val = 30000;
        }

        player.addXp(6, val);
        player.getInventory().addItem(995, amt);
        player.getGraphic().set(113, 1);
        player.getAnimation().animate(712, 9);

        return true;
    }
}
