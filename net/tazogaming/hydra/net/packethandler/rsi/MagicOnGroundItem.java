package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/10/13
 * Time: 18:51
 */
public class MagicOnGroundItem implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {
        int    itemY   = p.readShortBigEndian();
        int    itemId  = p.readShort();
        int    itemX   = p.readShortBigEndian();
        int    spellId = p.readShortA();
        Tile   t       = World.getWorld().getTile(itemX, itemY, pla.getHeight());

        if ((t != null) && (t.getItems() != null) && (t.getItems().size() > 0)) {
            for (FloorItem item : t.getItems()) {
                if (item.getItemId() == itemId) {
                    pla.getRequest().setRequest(ActionRequest.TYPE_FLOOR_ITEM_MAGE, 7, item);
                }
            }

            pla.getRoute().resetPath();
            pla.getActionSender().sendCloseWalkingFlag();
        }
    }
}
