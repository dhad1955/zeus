package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.script.runtime.instr.ConditionMap;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/10/13
 * Time: 03:40
 */
public class ItemOnItem implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player plr) {


        /*
         *               int interfaceId1 = packet.readShort();
         *       packet.readShort();
         *       int usedWithSlot = packet.readShort();
         *       int itemUsed = packet.readLEShort();
         *       int usedSlot = packet.readShort();
         *       int interfaceId2 = packet.readShort();
         *       int childId = packet.readShort();
         *       int itemUsedWith = packet.readLEShortA(
         */
        p.readShort();
        p.readShort();

        int  usedSlot     = p.readShort();
        int  itemId       = p.readLEShort();
        int  usedWithSlot = p.readShort();
        int  interfaceId2 = p.readShort();
        int  childId      = p.readShort();
        int  itemUsedWith = p.readLEShortA();
        Item i            = null;

        if (usedSlot >= 0) {
            i = plr.getInventory().getItem(usedSlot);
        }

        Item i2 = null;

        if (usedWithSlot >= 0) {
            i2 = plr.getInventory().getItem(usedWithSlot);
        }

        if(usedSlot == usedWithSlot)
            return;


        if ((usedSlot == -1) && (interfaceId2 == 192) && (childId == 38 || childId == 59)) {
            alchemy(plr, usedWithSlot, childId);

            return;
        } else if (interfaceId2 != 149) {
            if (plr.getRights() >= Player.ADMINISTRATOR) {
                plr.getActionSender().sendDev("Spell id: " + childId+" "+interfaceId2);
            }

            if (!World.getWorld().getScriptManager().directTrigger(plr, null, Trigger.MAGIC_ON_ITEM, interfaceId2, childId,
                    i2.getIndex())) {
                plr.getActionSender().sendMessage("Nothing interesting happens..");
            }

            return;
        }

        if ((i == null) || (i2 == null)) {
            return;
        }

        if (plr.isActionsDisabled()) {
            return;
        }

        if (handlePotions(usedSlot, usedWithSlot, plr)) {
            return;
        }

        if (handlePotions(usedWithSlot, usedSlot, plr)) {
            return;
        }

        plr.terminateScripts();

        if (plr.getCombatAdapter().isInCombat() || plr.isFollowing()) {
            plr.getFocus().unFocus();
            plr.unFollow();
            plr.getCombatAdapter().terminate();
        }

        if (!World.getWorld().getScriptManager().directTrigger(plr, null, Trigger.ITEM_ON_ITEM, i.getIndex(),
                i2.getIndex())) {
            if (!World.getWorld().getScriptManager().directTrigger(plr, null, Trigger.ITEM_ON_ITEM, i2.getIndex(),
                    i.getIndex())) {
                plr.getActionSender().sendMessage("Nothing interesting happens.");
            }
        }
    }

    /**
     * Method handlePotions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemSlot
     * @param usedWithSlot
     * @param pla
     *
     * @return
     */
    public boolean handlePotions(int itemSlot, int usedWithSlot, Player pla) {
        Item i  = pla.getInventory().getItem(itemSlot);
        Item i2 = pla.getInventory().getItem(usedWithSlot);

        if (i.getPotionHandler() == null) {
            return false;
        }

        if (i2.getPotionHandler() == null) {
            return false;
        }

        if (i2.getPotionHandler() != i.getPotionHandler()) {
            return false;
        }

        int itemDose  = i.getPotionHandler().getRemaining(i.getIndex()) + 1;
        int usedDose  = i2.getPotionHandler().getRemaining(i2.getIndex()) + 1;
        int savedDose = usedDose;

        if ((itemDose == 4) || (usedDose == 4) || (itemDose == 0) || (usedDose == 0)) {
            return false;
        }

        usedDose += itemDose;
        itemDose = 0;

        if (usedDose > 4) {
            itemDose = usedDose - 4;
            usedDose = 4;
        }

        pla.getInventory().replaceItem(itemSlot, i2.getPotionHandler().getDose(itemDose - 1));
        pla.getInventory().replaceItem(usedWithSlot, i2.getPotionHandler().getDose(usedDose - 1));

        return true;
    }

    private boolean alchemy(final Player player, int slot, int type) {
        Item i = player.getInventory().getItem(slot);


        if(player.isInZone("dung"))
            return false;


        if(i == null)
            return false;

        if (player.getTimers().timerActive(5)) {
            return true;
        }

        int req = 55;


        if (type == 38) {
            req = 21;
        }

        if (player.getCurStat(6) < req) {
            player.getActionSender().sendMessage("You need a magic level of atleast " + req + " to cast this spell.");

            return true;
        }

        player.addAchievementProgress2(Achievement.ALCHEMIST, 1);
        player.addAchievementProgress2(Achievement.MEGA_ALCHEMIST, 1);
        player.addAchievementProgress2(Achievement.PRO_ALCHEMIST, 1);

        player.getTimers().setTime(5, 3);
        player.setActionsDisabled(true);
        player.getTickEvents().add(new PlayerTickEvent(player, 3) {
            @Override
            public void doTick(Player owner) {
                player.getActionSender().sendBConfig(168, 7);
                player.setActionsDisabled(false);
                terminate();
            }
        });

        int fireRuneAmount = 5;

        if (type != 59) {
            fireRuneAmount = 3;
        }

        if (!player.getInventory().hasItem(561, 1)) {
            player.getActionSender().sendMessage("You don't have enough nature runes to cast this spell");
            player.setActionsDisabled(false);

            return true;
        }

        if (!player.getInventory().hasItem(554, fireRuneAmount)
                &&!ConditionMap.get("hasfirestaff").onCondition(player, null, null)) {
            player.getActionSender().sendMessage("You don't have enough fire runes to cast this spell.");

            return true;
        }

        if (i != null) {
            if ((i.getIndex() == 995) || (i.getPrice() == 0)) {
                player.setActionsDisabled(false);
                player.getActionSender().sendMessage("You can't cast this spell on this item.");

                return true;
            }
        }



        player.getInventory().deleteItem(561, 1);
        player.getInventory().deleteItem(554, fireRuneAmount);

        if ((player.getInventory().getFreeSlots(995) == 0) && (player.getInventory().getStackCount(slot) > 1)) {
            player.getActionSender().sendMessage("You don't have enough space.");

            return true;
        }

        player.getInventory().deleteItemFromSlot(i, 1, slot);

        int amt = i.getPrice();

        if (type != 59) {
            amt /= 2;
        } else {}

        int val = 1500 + (i.getPrice() / 10);

        if (val > 30000) {
            val = 30000;
        }
        if(i.getIndex() == 20139)
            player.addAchievementProgress2(Achievement.BILL_BUSTER_I, 1);
        if(i.getIndex() >= 1038 && i.getIndex() <= 1048)
            player.addAchievementProgress2(Achievement.BILL_BUSTER_II, 1);

        player.addXp(6, 55 + (val / 10));
        player.getInventory().addItem(995, amt);
        player.getGraphic().set(113, 1);
        player.getAnimation().animate(712, 9);

        return true;
    }
}
