package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 12:10
 * To change this template use File | Settings | File Templates.
 */
public class MoveItems implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param packet
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet packet, Player pla) {
        int    fromInterfaceHash = packet.readInt();
        int    fromInterfaceId   = fromInterfaceHash >> 16;
        int    toItemId          = packet.readShortA();
        int    fromItemId        = packet.readShort();
        int    toInterfaceHash   = packet.readLEInt();
        int    toInterfaceId     = toInterfaceHash >> 16;
        int    tabId             = (toInterfaceHash & 0xFF);
        int    fromId            = packet.readLEShortA();
        int    toId              = packet.readLEShort();



        if ((tabId == 93) && (fromInterfaceId == 762)) {
            pla.getBank().moveItems(fromId, toId);

            return;
        } else if ((tabId >= 44) && (tabId <= 62) && (fromInterfaceId == 762)) {
            pla.getBank().dragItemToTab(fromId, tabId);

            return;
        }

        if (fromInterfaceId == toInterfaceId) {
            InterfaceAdapter i = pla.getGameFrame().getActiveInterface(toInterfaceId);

            i.itemsSwitched(fromId, toId, pla);
        }
    }
}
