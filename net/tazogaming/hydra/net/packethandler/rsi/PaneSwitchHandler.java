package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.game.ui.GameFrame;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/07/14
 * Time: 19:25
 */
public class PaneSwitchHandler implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player player) {
        int    mode   = p.readByte();
        int    w      = p.readShort();
        int    h      = p.readShort();

        if ((player.getGameFrame().getRoot() != GameFrame.FIXED)
                && (player.getGameFrame().getRoot() != GameFrame.RESIZABLE)) {
            return;
        }

        if ((mode < 2) && (player.getGameFrame().getRoot() == GameFrame.FIXED)) {
            return;
        } else if ((mode >= 2) && (player.getGameFrame().getRoot() == GameFrame.RESIZABLE)) {
            return;
        }

        if ((mode == 1) || (mode == 0)) {
            player.getGameFrame().changeRoot(GameFrame.FIXED);
        } else {
            player.getGameFrame().changeRoot(GameFrame.RESIZABLE);
        }
    }
}
