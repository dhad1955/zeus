package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 23:46
 */
public class DialogActionHandler implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player pla) {
        int    shit   = p.readShort();
        int    sub_id = p.readLEShort();
        int    id     = p.readLEShort();

        pla.getActionSender().sendDev("shit: "+shit+" "+id+ " "+sub_id);

        ScriptRuntime runtime = World.getWorld().getScriptManager();

        if(runtime.trigger(pla, Trigger.DIALOG_INTERFACE_BTN, id, sub_id)) {
            return;
        }

        if (id == 740) {
            pla.getGameFrame().setDialog(-1);

            return;
        }

        if (pla.getRights() >= Player.ADMINISTRATOR) {
            pla.getActionSender().sendDev("id1: " + shit + " sub_id: " + sub_id + " id2: " + id);
        }

        if ((((id >= 210) && (id <= 214))) || ((id >= 63) && (id <= 67)) || ((id >= 240) && (id <= 244))) {
            if (pla.getGameFrame().getDialog() != null) {
                if (pla.getGameFrame().getDialog().isEnd()) {
                    pla.getGameFrame().closeDialog();
                } else {
                    pla.getGameFrame().getDialog().next();
                }
            } else {
                pla.getGameFrame().closeDialog();
            }
        } else {
            switch (id) {
            case 226 :
            case 228 :
            case 230 :
            case 232 :
            case 234 :
                int option = sub_id - 1;

                if (pla.getGameFrame().getDialog() != null) {
                    if (option != -1) {
                        pla.getGameFrame().getDialog().next();
                        pla.getGameFrame().getDialog().getScriptToUnlock().setOption(option);
                        pla.getAccount().setSetting("opt", option);
                    }
                } else {
                    pla.getGameFrame().closeDialog();
                }

                break;

            default :
                pla.getGameFrame().getActiveInterface(id).actionButton(sub_id, 0, pla, 0);

                break;
            }
        }
    }
}
