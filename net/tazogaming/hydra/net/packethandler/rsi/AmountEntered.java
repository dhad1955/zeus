package net.tazogaming.hydra.net.packethandler.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 21:44
 */

//208
public class AmountEntered implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
    @Override
    public void handlePacket(Packet p, Player player) {
        int    amount = p.readInt();

        if (!player.getGameFrame().amountEntered(amount)) {
            for (Scope c : player.getScriptsToProcess()) {
                if (c.isAwaitingAmount()) {
                    c.amountEntered(player, -1, amount);
                }
            }
        }
    }
}
