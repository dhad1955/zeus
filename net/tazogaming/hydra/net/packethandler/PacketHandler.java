package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;

public interface PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    public void handlePacket(Packet p, Player player);
    public DelayPolicy getDelayPolicy();

}
