package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.Packet;

import org.apache.mina.core.session.IoSession;

/**
 * This basic handler must navigate the login process and then add the player to the
 * playerloadqueue.
 */
public class PlayerLogin {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param session
     */
    public void handlePacket(Packet p, final IoSession session) {
        Player player = (Player) session.getAttachment();

        World.getWorld().getIOPool().getLoader().addLoad(player);
    }
}
