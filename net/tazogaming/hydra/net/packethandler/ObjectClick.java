package net.tazogaming.hydra.net.packethandler;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.Packet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/10/13
 * Time: 15:16
 */
public class ObjectClick implements PacketHandler {
    public static int INTERFACE_LOOP_DEST = -1;
    public static int interfaceCount      = 0;

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    @Override
    public void handlePacket(Packet p, Player pla) {
        int    objectX  = -1;
        int    objectId = -1;
        int    objectY  = -1;
        int    slot     = -1;

        if (pla.getCutScene() != null) {
            return;
        }

        if (pla.isActionsDisabled()) {
            return;
        }

        if (p.getId() == 76) {
            slot     = 1;
            objectId = p.readLEShort();
            p.readByte();
            objectX = p.readLEShortA();
            objectY = p.readShortA();
        } else if (p.getId() == 55) {
            slot     = 2;
            objectY  = p.readShort();
            objectId = p.readLEShort();
            objectX  = p.readShort();
        } else if (p.getId() == 48) {

            /*
             *   objectX  = p.readShortBigEndian();
             *  objectY  = p.readShort();
             *  objectId = p.readLEShortA();
             *  slot     = 3;
             */
            objectId = p.readShort();

            CacheObjectDefinition def = CacheObjectDefinition.forID(objectId);

            pla.getActionSender().sendDev("id: " + objectId + " sizeX: " + def.sizeX + " sizeY: " + def.sizeY + " id: "
                                          + p.getId());    // rot: "+World.getWorld().getTile(objectX, objectY, pla.getHeight()).getMappedRotation());
        } else if (p.getId() == 25) {
            objectX  = p.readShortA();
            objectId = p.readLEShortA();
            p.readByte();
            objectY = p.readLEShortA();
            slot    = 4;
        } else if (p.getId() == 81) {
            objectX  = p.readLEShort();
            objectY  = p.readLEShort();
            objectId = p.readShortA();
            slot     = 5;
        }

        if (objectId < 0) {
            objectId = objectId + 0x10000;
        }



        CacheObjectDefinition def = CacheObjectDefinition.forId(objectId);
        if (pla.getRights() >= Player.ADMINISTRATOR) {
            pla.getActionSender().sendDev("id: " + objectId + " x: "+objectX+" y:"+objectY+" slot:"+slot+" sizeX: " + def.sizeX + " sizeY: " + def.sizeY + " id: "
                    + p.getId());    // rot: "+World.getWorld().getTile(objectX, objectY, pla.getHeight()).getMappedRotation());
        }
        if (def == null) {
            return;
        }

        if (Point.getDistance(objectX, objectY, pla.getX(), pla.getY()) > 20) {

            return;
        }

        if (pla.isActionsDisabled() || Walking.blockWalking(pla)) {
            pla.getActionSender().sendCloseWalkingFlag();

            return;
        }

        Walking.resetWalking(pla);

        Entity defaultEntity = new Entity();

        defaultEntity.setId(objectId);
        defaultEntity.setLocation(Point.location(objectX, objectY, pla.getHeight()));
        pla.getRequest().setRequest(ActionRequest.TYPE_OBJECT_REQUEST, -1, defaultEntity, slot);
        World.getWorld().getPathService().walkToObject(pla, objectId, objectX, objectY);
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
