package net.tazogaming.hydra.net.packethandler;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.ge.GEUnsellables;
import net.tazogaming.hydra.net.Packet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/01/2015
 * Time: 12:16
 */
public class GrandExchangeItemSelect implements PacketHandler {
    /**
     * Method handlePacket
     * Created on 14/08/18
     *
     * @param p
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void handlePacket(Packet p, Player player) {
        int itemID = p.readShort();
        if(GEUnsellables.isFiltered(itemID) || !Item.forId(itemID).isTradeable()){
            player.getActionSender().sendMessage("You cannot purchase this item on the grand exchange");
            return;
        }
        if(Item.forId(itemID).getExchangePrice() == 0){
            player.getActionSender().sendMessage("[Bug] item does not have price set please report it, item code: "+itemID);
            return;
        }
        player.getAccount().getGrandExchange().setCurItem(itemID);
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
