package net.tazogaming.hydra.net;

//~--- non-JDK imports --------------------------------------------------------

import org.apache.mina.core.session.IoSession;

/**
 * An immutable packet object.
 */

//XXX: immutable classes should be final.
public class Packet {
    public static final int     MAX_SIZE = 54;
    private static final char[] hex      = "0123456789ABCDEF".toCharArray();

    /**
     * The current index into the payload buffer for reading
     */
    private int     caret  = 0;
    private Size    size   = Size.Fixed;
    private boolean ignore = false;

    /**
     * The associated IO session
     */
    private PlaySession session;

    /**
     *            read
     * The ID of the packet
     */
    private int pID;

    /**
     * The length of the payload
     */
    private int pLength;

    /**
     * The payload
     */
    private byte[] pData;

    /**
     * Whether this packet is without the standard packet header
     */
    private boolean bare;

    public static enum Size { Fixed, VariableByte, VariableShort }

    /**
     * Constructs ...
     *
     *
     * @param data
     */
    public Packet(byte[] data) {
        this.pData = data;
        this.caret = 0;
        this.pLength = data.length;
    }

    /**
     * Creates a new packet with the specified parameters. The packet
     * is considered not to be a bare packet.
     *
     * @param session The session to associate with the packet
     * @param pID        The ID of the packet
     * @param pData   The payload the packet
     */
    public Packet(PlaySession session, int pID, byte[] pData) {
        this(session, pID, pData, false);
    }

    public Packet(int pId, int leng, byte[] data) {
        this.pLength = leng;
        this.pData = data;
        this.pID = pId;
    }

    public void reset() {
        this.caret = 0;
    }

    /**
     * Creates a new packet with the specified parameters.
     *
     * @param session The session to associate with the packet
     * @param pID        The ID of the packet
     * @param pData   The payload of the packet
     * @param bare      Whether this packet is bare, which means that it does
     *                not include the standard packet header
     */
    public Packet(PlaySession session, int pID, byte[] pData, boolean bare) {
        this(session, pID, pData, bare, Size.Fixed);
    }

    /**
     * Constructs ...
     *
     *
     * @param session
     * @param pID
     * @param pData
     * @param bare
     * @param s
     */
    public Packet(PlaySession session, int pID, byte[] pData, boolean bare, Size s) {
        this.session = session;
        this.pID     = pID;
        this.pData   = pData;
        this.pLength = pData.length;
        this.bare    = bare;
        this.size    = s;
    }

    /**
     * Method readLEInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readLEInt() {
        return readUByte() + (readUByte() << 8) + (readUByte() << 16) + (readUByte() << 24);
    }

    /**
     * Method readInt1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readInt1() {
        return (readUByte() << 8) + readUByte() + (readUByte() << 24) + (readUByte() << 16);
    }

    /**
     * Method insertBytes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buf2
     */
    public void insertBytes(byte[] buf2) {
        byte[] newbuff = new byte[buf2.length + pData.length];
        int    offset  = 0;

        for (int i = 0; i < pData.length; i++) {
            newbuff[i] = pData[i];
            offset     = i;
        }

        for (int i = 0; i < buf2.length; i++) {
            newbuff[offset + i] = buf2[i];
        }

        this.pData = newbuff;
    }

    /**
     * Method gsmarts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int gsmarts() {
        int i = pData[caret] & 0xff;

        if (i < 128) {
            return readByte() & 0xFF;
        } else {
            return g2() - 32768;
        }
    }

    /**
     * Method g2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int g2() {
        caret += 2;

        return ((pData[caret - 2] & 0xff) << 8) + (pData[caret - 1] & 0xff);
    }

    /**
     * Returns the IO session associated with the packet, if any.
     *
     * @return The <code>IoSession</code> object, or <code>null</code>
     *         if none.
     */
    public PlaySession getSession() {
        return session;
    }

    /**
     * Checks if this packet is considered to be a bare packet, which
     * means that it does not include the standard packet header (ID
     * and length values).
     *
     * @return Whether this packet is a bare packet
     */
    public boolean isBare() {
        return bare;
    }

    /**
     * Method getSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Size getSize() {
        return size;
    }

    /**
     * Returns the packet ID.
     *
     * @return The packet ID
     */
    public int getId() {
        return pID;
    }

    /**
     * Returns the length of the payload of this packet.
     *
     * @return The length of the packet's payload
     */
    public int getLength() {
        return pLength;
    }

    /**
     * Returns the entire payload data of this packet.
     *
     * @return The payload <code>byte</code> array
     */
    public byte[] getData() {
        return pData;
    }

    /**
     * Returns the remaining payload data of this packet.
     *
     * @return The payload <code>byte</code> array
     */
    public byte[] getRemainingData() {
        byte[] data = new byte[pLength - caret];

        for (int i = 0; i < data.length; i++) {
            data[i] = pData[i + caret];
        }

        caret += data.length;

        return data;
    }

    /**
     * Reads the next <code>byte</code> from the payload.
     *
     * @return A <code>byte</code>
     */
    public byte readByte() {
        return pData[caret++];
    }

    /**
     * Method readUByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readUByte() {
        return pData[caret++] & 0xFF;
    }

    /**
     * Method readSmart
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readSmart() {
        int i = 0xff & pData[caret];

        if ((i ^ 0xffffffff) <= -129) {
            return readShort() - 32768;
        }

        return readByte();
    }

    /**
     * Reads the next <code>short</code> from the payload.
     *
     * @return A <code>short</code>
     */
    public short readShort() {
        return (short) ((short) ((pData[caret++] & 0xff) << 8) | (short) (pData[caret++] & 0xff));
    }

    /**
     * Method getCaret
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCaret() {
        return this.caret;
    }

    /**
     * Method readLEShortA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readLEShortA() {
        int i = ((pData[caret++] - 128 & 0xff)) + ((pData[caret++] & 0xff) << 8);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readLEShort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readLEShort() {
        int i = ((pData[caret++] & 0xff)) + ((pData[caret++] & 0xff) << 8);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readShortA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readShortA() {
        caret += 2;

        return ((pData[caret - 2] & 0xff) << 8) + (pData[caret - 1] - 128 & 0xff);
    }

    /**
     * Method readBEShortA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readBEShortA() {
        skip(2);

        int i = ((pData[caret - 1] & 0xff) << 8) + (pData[caret - 2] - 128 & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readShortBigEndian
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readShortBigEndian() {
        skip(2);

        int i = ((pData[caret - 1] & 0xff) << 8) + (pData[caret - 2] & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readSignedWordA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readSignedWordA() {
        skip(2);

        int i = ((pData[caret - 2] & 0xff) << 8) + (pData[caret - 1] - 128 & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Reads the next <code>int</code> from the payload.
     *
     * @return An <code>int</code>
     */
    public int readInt() {
        return ((pData[caret++] & 0xff) << 24) | ((pData[caret++] & 0xff) << 16) | ((pData[caret++] & 0xff) << 8)
               | (pData[caret++] & 0xff);
    }

    /**
     * Reads the next <code>long</code> from the payload.
     *
     * @return A <code>long</code>
     */
    public long readLong() {
        return (long) ((long) (pData[caret++] & 0xff) << 56) | ((long) (pData[caret++] & 0xff) << 48)
               | ((long) (pData[caret++] & 0xff) << 40) | ((long) (pData[caret++] & 0xff) << 32)
               | ((long) (pData[caret++] & 0xff) << 24) | ((long) (pData[caret++] & 0xff) << 16)
               | ((long) (pData[caret++] & 0xff) << 8) | ((long) (pData[caret++] & 0xff));
    }

    /**
     * Reads the string which is formed by the unread portion
     * of the payload.
     *
     * @return A <code>String</code>
     */
    public String readString() {
        return readString(pLength - caret);
    }

    /**
     * Method readRS2String
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String readRS2String() {
        int start = caret;

        while (pData[caret++] != 0);

        return new String(pData, start, caret - start - 1);
    }

    /**
     * Method rewind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chars
     */
    public void rewind(int chars) {
        this.caret -= chars;
    }

    /**
     * Method readBytes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buf
     * @param off
     * @param len
     */
    public void readBytes(byte[] buf, int off, int len) {
        for (int i = 0; i < len; i++) {
            buf[off + i] = pData[caret++];
        }
    }

    /**
     * Reads a string of the specified length from the payload.
     *
     * @param length The length of the string to be read
     * @return A <code>String</code>
     */
    public String readString(int length) {
        String rv = new String(pData, caret, length);

        caret += length;

        return rv;
    }

    /**
     * Skips the specified number of bytes in the payload.
     *
     * @param x The number of bytes to be skipped
     */
    public void skip(int x) {
        caret += x;
    }

    /**
     * Method remaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int remaining() {
        return pData.length - caret;
    }

    /**
     * Returns this packet in string form.
     *
     * @return A <code>String</code> representing this packet
     */
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("[id=" + pID + ",len=" + pLength + ",data=0x");

        for (int x = 0; x < pLength; x++) {
            sb.append(byteToHex(pData[x], true));
        }

        sb.append("]");

        return sb.toString();
    }

    private static String byteToHex(byte b, boolean forceLeadingZero) {
        StringBuilder out = new StringBuilder();
        int           ub  = b & 0xff;

        if ((ub / 16 > 0) || forceLeadingZero) {
            out.append(hex[ub / 16]);
        }

        out.append(hex[ub % 16]);

        return out.toString();
    }

    /**
     * Method setIgnore
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setIgnore(boolean b) {
        this.ignore = b;
    }
}
