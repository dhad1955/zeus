package net.tazogaming.hydra.net;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.Core;

import net.tazogaming.hydra.test.performancetest.BotPlayer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;

//~--- JDK imports ------------------------------------------------------------

import java.util.Deque;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/09/14
 * Time: 21:55
 */
public class PlaySession {
    public static final int MAX_OUTPUT_PER_TICK = 12000;
    public static int       PLAYER_ID           = 0;
    public boolean          isBot               = false;

    /** maxOutput made: 14/11/20 */
    private int maxOutput = MAX_OUTPUT_PER_TICK;

    /** ioWrite made: 14/11/20 */
    private int ioWrite = 0;

    /** loginPacketSize made: 14/11/20 */
    private int loginPacketSize = -1;

    /** last10Packets made: 14/11/20 */
    private Deque<Integer> last10Packets = new LinkedBlockingDeque<Integer>();
    public int             pId           = 0;

    /** channel made: 14/11/20 */
    private Channel channel;

    /** player made: 14/11/20 */
    private Player player;

    /** displayMode made: 14/11/20 */
    private int displayMode;

    /** inLobby made: 14/11/20 */
    private boolean inLobby;

    /** userIP made: 14/11/20 */
    private String userIP;

    /** version made: 14/11/20 */
    private int version;

    /** sessionKey made: 14/11/20 */
    private long sessionKey;

    /**
     * Constructs ...
     *
     *
     * @param isBot
     */
    public PlaySession(boolean isBot) {
        this.isBot = true;
    }

    /**
     * Constructs ...
     *
     *
     * @param channel
     */
    public PlaySession(Channel channel) {
        this.channel = channel;
    }

    /**
     * Method getUserIP
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getUserIP() {
        return userIP;
    }

    /**
     * Method setUserIP
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userIP
     */
    public void setUserIP(String userIP) {
        this.userIP = userIP;
    }

    /**
     * Method getVersion
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     * Method setVersion
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param version
     */
    public void setVersion(int version) {
        this.version = version;
    }

    /**
     * Method setMaxOutput
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param maxOutput
     */
    public void setMaxOutput(int maxOutput) {
        this.maxOutput = maxOutput;
    }

    /**
     * Method getSessionKey
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getSessionKey() {
        return sessionKey;
    }

    /**
     * Method setSessionKey
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sessionKey
     */
    public void setSessionKey(long sessionKey) {
        this.sessionKey = sessionKey;
    }

    /**
     * Method getLoginPacketSize
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLoginPacketSize() {
        return loginPacketSize;
    }

    /**
     * Method setLoginPacketSize
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param loginPacketSize
     */
    public void setLoginPacketSize(int loginPacketSize) {
        this.loginPacketSize = loginPacketSize;
    }

    /**
     * Method setChannel
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param channel
     */
    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    /**
     * Method addPacket
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param opcode
     */
    public void addPacket(int opcode) {
        last10Packets.offerFirst(opcode);

        if (last10Packets.size() > 10) {
            last10Packets.pop();
        }
    }

    /**
     * Method getLast10Packets
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Integer[] getLast10Packets() {
        Integer[] arr = new Integer[last10Packets.size()];

        last10Packets.toArray(arr);

        return arr;
    }

    /**
     * Method resetOutputCounter
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetOutputCounter() {
        ioWrite = 0;
    }

    /**
     * Method getChannel
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Method write
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param message
     *
     * @return
     */
    public ChannelFuture write(Packet message) {
        ioWrite++;

        if(player instanceof BotPlayer)
            return null;

        if (!channel.isConnected()) {

            return null;
        }




        if ((channel != null) && channel.isConnected()) {

            return channel.write(message);
        }

        return null;
    }

    /**
     * Method setPlayer
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void setPlayer(Player player) {
        pId         = PLAYER_ID++;
        this.player = player;
    }

    /**
     * Method getPlayer
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method isDisconnected
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDisconnected() {
        if (isBot) {
            return false;
        }

        if (channel == null) {
            return true;
        }

        return !channel.isConnected();
    }

    /**
     * Method getDisplayMode
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDisplayMode() {
        return displayMode;
    }

    /**
     * Method setDisplayMode
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mode
     */
    public void setDisplayMode(int mode) {
        this.displayMode = mode;
    }

    /**
     * Method setInLobby
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param inLobby
     */
    public void setInLobby(boolean inLobby) {
        this.inLobby = inLobby;
    }

    /**
     * Method isInLobby
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isInLobby() {
        return inLobby;
    }

    /**
     * Method close
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void close() {
        this.getChannel().close();

        if (player != null) {
            player.setDisconTime(System.currentTimeMillis());
        }

        ioWrite = 0;
    }

    /**
     * Method isConnected
     * Created on 14/11/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isConnected() {
        if (Core.currentTimeMillis() - player.getLastPacket() > 7000) {
            return false;
        }

        return !isDisconnected();
    }
}
