package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 30/06/14
 * Time: 14:56
 * To change this template use File | Settings | File Templates.
 */

//dont know what this stupid shit is, something required for maps so copied it out of the spawnscape server or w/e
public class MapXTEA {
    private final static Map<Short, int[]> mapData = new HashMap<Short, int[]>();

    /**
     * Constructs ...
     *
     */
    public MapXTEA() {
        final File packedFile = new File("./mapdata/packed.dat");

        if (!packedFile.exists()) {
            pack();
        } else {
            load();
        }
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static boolean load() {
        try {
            File file = new File("config/mapxtea.bin");

            if (!file.exists()) {
                return false;
            }

            RandomAccessFile raf    = new RandomAccessFile(file, "rw");
            ByteBuffer       buffer = raf.getChannel().map(FileChannel.MapMode.READ_ONLY, 0, raf.length());

            while (buffer.remaining() > 0) {
                int   id  = buffer.getShort() & 0xFFFF;
                int[] key = new int[4];

                for (int i2 = 0; i2 < 4; i2++) {
                    key[i2] = buffer.getInt();
                }

                mapData.put((short) id, key);
            }
        } catch (Exception ee) {
            Logger.err("Fatal error, unable to load XTEA keys");
            ee.printStackTrace();
            System.exit(-1);
        }

        return true;
    }

    /**
     * Method pack
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void pack() {
        Logger.log("Packing mapdata...");

        try {
            final DataOutputStream out      = new DataOutputStream(new FileOutputStream("./mapdata/packed.dat"));
            final File             unpacked = new File("./mapdata/unpacked/");
            final File[]           Data     = unpacked.listFiles();

            for (File region : Data) {
                final String name = region.getName();

                if (!name.contains(".txt")) {
                    continue;
                }

                final short    regionId = Short.parseShort(name.replace(".txt", ""));
                BufferedReader in       = new BufferedReader(new FileReader(region));

                out.writeShort(regionId);

                final int[] Key = new int[4];

                for (int j = 0; j < 4; j++) {
                    Key[j] = Integer.parseInt(in.readLine());
                    out.writeInt(Key[j]);
                }

                getMapData().put(regionId, Key);
                in.close();
            }

            out.flush();
            out.close();
            Logger.log("Complete.");
        } catch (IOException e) {
            final File Failedpacked = new File("./mapdata/packed.dat");

            if (Failedpacked.exists()) {
                Failedpacked.delete();
            }

            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Method getMapData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Map<Short, int[]> getMapData() {
        return mapData;
    }
}
