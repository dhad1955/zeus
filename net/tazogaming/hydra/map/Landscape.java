package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.net.Packet;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 25/09/13
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */
public class Landscape {
    public static final ArrayList<Integer> failed     = new ArrayList<Integer>();
    private List<GameObject>[][][]         objects    = new ArrayList[4][64][64];
    private byte[][][]                     tile_flags = new byte[4][64][64];
    private int                            landscapeX, landscapeY;

    /**
     * Constructs ...
     *
     *
     * @param landscapeX
     * @param landscapeY
     * @param block_data
     * @param terrainData
     * @param ids
     */
    public Landscape(int landscapeX, int landscapeY, byte[] block_data, byte[] terrainData, int... ids) {
        this(landscapeX, landscapeY, block_data, terrainData, -1, ids);
    }

    /**
     * Constructs ...
     *
     *
     * @param landscapeX
     * @param landscapeY
     * @param block_data
     * @param terrainData
     * @param rotation
     * @param id
     */
    public Landscape(int landscapeX, int landscapeY, byte[] block_data, byte[] terrainData, int rotation, int... id) {
        boolean unpacking = false;
        if(id[1] < -1){
            unpacking = true;



        }
        try {
            unpack_terrain(terrainData);
        } catch (Exception ee) {}

        for (int i = 0; i < objects.length; i++) {
            for (int k = 0; k < objects[i].length; k++) {
                for (int y = 0; y < objects[i][k].length; y++) {
                    objects[i][k][y] = new ArrayList<GameObject>();
                }
            }
        }
        if(unpacking){
            System.err.println("unpacking");
        }


        boolean fail = false;

        try {
label0:
            {
                Packet p = new Packet(block_data);

                this.landscapeX = landscapeX;
                this.landscapeY = landscapeY;

                int object_id = -1;

                do {
                    int delta_id = p.gsmarts();

                    if (delta_id == 0) {
                        break label0;
                    }

                    int pos = 0;

                    object_id += delta_id;

                    do {
                        int delta_pos = p.gsmarts();

                        if (delta_pos == 0) {
                            break;
                        }

                        pos += delta_pos - 1;

                        int tile_y             = pos & 0x3f;
                        int tile_x             = pos >> 6 & 0x3f;
                        int tile_z             = pos >> 12;
                        int object_info        = p.readByte();
                        int object_type        = object_info >> 2;
                        int object_orientation = object_info & 3;

                        if ((tile_x < 0) || (tile_y < 0) || (tile_x > 64) || (tile_y > 64)) {
                            continue;
                        }


                        if ((tile_z < 0) || (tile_z > 3)) {
                            break;
                        }

                        if (rotation != -1) {
                            CacheObjectDefinition defin = CacheObjectDefinition.forId(object_id);

                            if (defin != null) {
                                int regionX = tile_x / 8;
                                int regionY = tile_y / 8;
                                int old_x   = tile_x;
                                int object_width;
                                int object_length;

                                switch (object_orientation) {
                                case 3 :
                                case 1 :
                                    object_width  = defin.sizeY;
                                    object_length = defin.sizeX;

                                    break;

                                default :
                                    object_width  = defin.sizeX;
                                    object_length = defin.sizeY;

                                    break;
                                }

                                tile_x = (regionX * 8)
                                         + rotate_object_block_x(tile_x & 7, tile_y & 7, object_width, object_length,
                                                                 rotation);
                                tile_y = (regionY * 8)
                                         + rotate_object_block_y(old_x & 7, tile_y & 7, object_width, object_length,
                                                                 rotation);
                                object_orientation = object_orientation + rotation & 3;
                            }
                        }

                        GameObject nextObject = new GameObject();

                      PACK = unpacking;
                        nextObject.setIdentifier(object_id);
                        nextObject.setCurrentRotation(object_orientation);
                        nextObject.setObjectType(object_type);
                        this.objects[tile_z][tile_x][tile_y].add(nextObject);
                    } while (true);
                } while (true);
            }
        } catch (Exception ee) {}
    }

    private boolean PACK = false;

    /**
     * Method getObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<GameObject>[][][] getObjects() {
        return objects;
    }

    /**
     * Method rotate_object_block_x
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param z
     * @param width
     * @param height
     * @param orientation
     *
     * @return
     */
    public static int rotate_object_block_x(int x, int z, int width, int height, int orientation) {
        orientation &= 3;

        if (orientation == 0) {
            return x;
        }

        if (orientation == 1) {
            return z;
        }

        if (orientation == 2) {
            return 7 - x - (width - 1);
        } else {
            return 7 - z - (height - 1);
        }
    }

    /**
     * Method rotate_object_block_y
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param z
     * @param width
     * @param height
     * @param orientation
     *
     * @return
     */
    public static int rotate_object_block_y(int x, int z, int width, int height, int orientation) {
        orientation &= 3;

        if (orientation == 0) {
            return z;
        }

        if (orientation == 1) {
            return 7 - x - (width - 1);
        }

        if (orientation == 2) {
            return 7 - z - (height - 1);
        } else {
            return x;
        }
    }

    /**
     * Method unpack_terrain
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     */
    public void unpack_terrain(byte[] data) {
        Packet p = new Packet(data);

        for (int i = 0; i < 4; i++) {
            for (int x = 0; x < 64; x++) {
                for (int y = 0; y < 64; y++) {
                    load_terrain_tile(i, x, y, p);
                }
            }
        }
    }

    private void load_terrain_tile(int tile_y, int tile_x, int tile_z, Packet stream) {
        if ((tile_x >= 0) && (tile_x < 64) && (tile_z >= 0) && (tile_z < 64)) {
            tile_flags[tile_y][tile_x][tile_z] = 0;

            do {
                int value = stream.readByte() & 0xff;

                if (value == 0) {
                    return;
                }

                if (value == 1) {
                    stream.readByte();

                    return;
                }

                if (value <= 49) {
                    stream.readByte();
                } else if (value <= 81) {
                    tile_flags[tile_y][tile_x][tile_z] = (byte) (value - 49);
                }
            } while (true);
        }
    }

    /**
     * Method storeToCache
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param def
     *
     * @return
     */
    public boolean storeToCache(CacheObjectDefinition def) {
        String name = def.name.toLowerCase();

        return (((name.contains("tree") || name.contains("battlements") || name.contains("lever") || name.contains("switch") || name.contains("web")
                  || name.contains("oak") || name.contains("willow") || name.contains("yew")
                  || name.contains("maple")) &&!name.contains("stump")) || name.contains("rocks")
                      || name.contains("stall") || name.contains("flax"));
    }

    /**
     * Method loadToRegions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param manager
     */
    public void loadToRegions(RegionManager manager) {
        for (int plane = 0; plane < tile_flags.length; plane++) {
            for (int x = 0; x < tile_flags[plane].length; x++) {
                for (int y = 0; y < tile_flags[plane][x].length; y++) {
                    int    regionX      = ((landscapeX * 8) - 6) + (x / 8);
                    int    regionY      = ((landscapeY * 8) - 6) + (y / 8);
                    int    regionLocalX = (x % 8);
                    int    regionLocalY = (y % 8);
                    int    absoluteX    = ((regionX + 6) * 8) + regionLocalX;
                    int    absoluteY    = ((regionY + 6) * 8) + regionLocalY;
                    Region r            = manager.getRegion(regionX, regionY);

                    if (r == null) {
                        r = manager.addRegion(regionX, regionY);
                    }

                    if ((tile_flags[plane][x][y] & 1) == 1) {
                        int k1 = plane;

                        if ((tile_flags[1][x][y] & 2) == 2) {
                            k1--;
                        }

                        if (k1 >= 0) {

                            ClippingDecoder.addClipping(absoluteX, absoluteY, k1, 0x20000);
                        }
                    }
                }
            }
        }

        for (int plane = 0; plane < objects.length; plane++) {
            for (int x = 0; x < objects[plane].length; x++) {
                for (int y = 0; y < objects[plane][x].length; y++) {
                    int regionX      = ((landscapeX * 8) - 6) + (x / 8);
                    int regionY      = ((landscapeY * 8) - 6) + (y / 8);
                    int regionLocalX = (x % 8);
                    int regionLocalY = (y % 8);
                    int logicH       = plane;

                    if ((tile_flags[1][x][y] & 2) == 2) {
                        logicH--;
                    }

                    for (GameObject obj : objects[plane][x][y]) {
                        int    absoluteX = ((regionX + 6) * 8) + regionLocalX;
                        int    absoluteY = ((regionY + 6) * 8) + regionLocalY;
                        Region r         = manager.getRegion(regionX, regionY);

                        if (r == null) {
                            r = manager.addRegion(regionX, regionY);
                        }

                        CacheObjectDefinition objectDef = CacheObjectDefinition.forId(obj.getId());

                        if (objectDef != null) {
                            String name = objectDef.name.toLowerCase();

                            if (((obj.getType() == 0) || (obj.getType() == 9))
                                    && (name.contains("door") || name.contains("gate"))) {
                                if ((objectDef != null) && (objectDef.hasActions()) && (objectDef.options[0] != null)
                                        && objectDef.options[0].equalsIgnoreCase("close")) {
                                    Door d = new Door(obj.getId(), absoluteX, absoluteY, plane, obj.getRotation(),
                                                      true);
                                } else if ((objectDef != null) && (objectDef.hasActions())
                                           && (objectDef.options[0] != null)
                                           && objectDef.options[0].equalsIgnoreCase("open")) {
                                    Door d = new Door(obj.getId(), absoluteX, absoluteY, plane, obj.getRotation(),
                                                      false);
                                }
                            }

                            if (logicH >= 0) {
                                ClippingDecoder.addObject(obj.getId(), absoluteX, absoluteY, logicH, obj.getType(),
                                                          obj.getRotation(), true);
                            }


                            if(obj.getId() == 5328)
                                continue;


                            if(obj.getType() == 22 && !objectDef.hasOptions())
                                continue;


                            if (storeToCache(objectDef) || (obj.getId() == 3580 ||(obj.getType() == 22 && !World.getWorld().getTile(absoluteX, absoluteY,
                                    plane).hasMappedObject()) || obj.getId() == 3582)
                                    || (objectDef.hasOptions()
                                        || (((obj.getType() == 10) || (obj.getType() == 11))
                                            &&!World.getWorld().getTile(absoluteX, absoluteY,
                                                plane).hasMappedObject()))) {
                                World.getWorld().getTile(absoluteX, absoluteY, (logicH > -1)
                                        ? logicH
                                        : plane).mapObject(obj.getId(), obj.getType(), obj.getRotation());
                            }
                        }
                    }
                }
            }
        }
    }
}
