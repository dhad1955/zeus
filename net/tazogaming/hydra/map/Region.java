package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.entity3d.object.GameObject;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */
public class Region {
    private ArrayList<Player>     players = null;
    private ArrayList<FloorItem>  items   = null;
    private ArrayList<GameObject> objects = null;
    private ArrayList<NPC>        npcs    = null;
    private ArrayList<Door>       doors   = null;
    private Tile[][][]            tiles   = new Tile[4][8][8];
    private int                   regionX, regionY;
    private ArrayList<Zone>       areasCovered;    // = new ArrayList<Area>();

    /**
     * Constructs ...
     *
     *
     * @param x
     * @param y
     */
    public Region(int x, int y) {
        regionX = x;
        regionY = y;
    }

    /**
     * Method hasDoors
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasDoors() {
        return doors != null;
    }

    /**
     * Method getDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Door> getDoor() {
        return doors;
    }

    /**
     * Method addAreaIfAbsent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void addAreaIfAbsent(Zone e) {
        if (areasCovered == null) {
            areasCovered = new ArrayList<Zone>();
        }

        if (!areasCovered.contains(e)) {
            areasCovered.add(e);
        }

        e.addRegion(this);
    }

    /**
     * Method getAreas
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Zone> getAreas() {
        return areasCovered;
    }

    /**
     * Method hasAreas
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasAreas() {
        return areasCovered != null;
    }

    /**
     * Method registerObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     * @param rotation
     * @param obj
     */
    public void registerObject(int x, int y, int z, int rotation, GameObject obj) {
        obj.setLocation(Point.location(x, y, z));
        getTile(x, y, z).add(obj);
    }

    /**
     * Method getNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<NPC> getNpcs() {
        if (npcs == null) {
            npcs = new ArrayList<NPC>();
        }

        return npcs;
    }

    /**
     * Method getObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<GameObject> getObjects() {
        if (objects == null) {
            objects = new ArrayList<GameObject>();
        }

        return objects;
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<FloorItem> getItems() {
        if (items == null) {
            items = new ArrayList<FloorItem>();
        }

        return items;
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Player> getPlayers() {
        if (players == null) {
            players = new ArrayList<Player>(30);
        }

        return players;
    }

    /**
     * Method isEmpty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isEmpty() {
        return (players.size() == 0) && (items.size() == 0) && (objects.size() == 0) && (npcs.size() == 0);
    }

    /**
     * Method getLocalTile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public Tile getLocalTile(int x, int y, int h) {
        if (tiles[h][x][y] == null) {
            tiles[h][x][y] = new Tile(((regionX + 6) * 8) + x, ((regionY + 6) * 8) + y, h);
        }

        return tiles[h][x][y];
    }

    public boolean tileExists(int x, int y, int h){
        return tiles[h][x][y] != null;
    }

    /**
     * Method getTile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public Tile getTile(int x, int y, int h) {
        int locationX = ((regionX + 6) * 8);    // - regionX;
        int locationY = ((regionY + 6) * 8);

        return getLocalTile(x - locationX, y - locationY, h);
    }

    public boolean badTile(int x, int y, int h) {
        int locationX = ((regionX + 6) * 8);    // - regionX;
        int locationY = ((regionY + 6) * 8);

        return tileExists(x - locationX, y - locationY, h);
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void remove(Entity e) {
        int locationX = e.getX() - ((regionX + 6) * 8);    // - regionX;
        int locationY = e.getY() - ((regionY + 6) * 8);

        tiles[e.getLocation().getHeight()][locationX][locationY].remove(e);

        if (e instanceof Player) {
            players.remove((Player) e);
        } else if (e instanceof FloorItem) {
            items.remove(e);
        } else if (e instanceof NPC) {
            npcs.remove(e);
        } else if (e instanceof GameObject) {
            objects.remove(e);
        } else if (e instanceof Door) {
            doors.remove(e);
        }
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     * @param location
     */
    public void add(Entity e, Point location) {
        int locationX = location.getX() - ((regionX + 6) * 8);    // - regionX;
        int locationY = location.getY() - ((regionY + 6) * 8);

        getLocalTile(locationX, locationY, location.getHeight()).add(e);

        if (e instanceof Player) {
            if (players == null) {
                players = new ArrayList<Player>(50);
            }


            players.add((Player) e);
        } else if (e instanceof FloorItem) {
            if (items == null) {
                items = new ArrayList<FloorItem>();
            }

            items.add((FloorItem) e);
        } else if (e instanceof NPC) {
            if (npcs == null) {
                npcs = new ArrayList<NPC>();
            }

            npcs.add((NPC) e);
        } else if (e instanceof GameObject) {
            if (objects == null) {
                objects = new ArrayList<GameObject>();
            }

            objects.add((GameObject) e);
        } else if (e instanceof Door) {
            if (doors == null) {
                doors = new ArrayList<Door>();
            }

            doors.add((Door) e);
        }
    }
}
