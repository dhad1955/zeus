package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.npc.NPC;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 16:58
 * To change this template use File | Settings | File Templates.
 */
public class RegionManager {
    private HashMap<RegionCoordinates, Region> activeRegions = new HashMap<RegionCoordinates, Region>();
    private Region[][]                         regions       = new Region[1000][1500];

    /**
     * Method getRegion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     *
     * @return
     */
    public Region getRegion(Point location) {
        if (location == null) {
            return null;
    }

        if ((location.getRegionX() == -1) || (location.getRegionY() == 1)) {
            return null;
        }

        return regions[location.getRegionX()][location.getRegionY()];
    }

    /**
     * Method getRegion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public Region getRegion(int x, int y) {
        return regions[x][y];
    }

    /**
     * Method getRegionIfAbsent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     *
     * @return
     */
    public Region getRegionIfAbsent(Point location) {
        Region r = getRegion(location);

        if (r == null) {
            r = new Region(location.getRegionX(), location.getRegionY());
        }

        regions[location.getRegionX()][location.getRegionY()] = r;

        return r;
    }

    /**
     * Method setLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     * @param oldLocation
     * @param location
     */
    public void setLocation(Entity e, Point oldLocation, Point location) {
        Region r = getRegion(oldLocation);

        if ((e instanceof NPC) && (oldLocation != null)) {
            NPC    npc      = (NPC) e;
            Tile[] oldTiles = NPC.getTiles(npc, oldLocation);

            for (Tile t : oldTiles) {
                t.removeNPCCHunk((npc));
            }
        }

        if (r != null) {
            r.remove(e);
        }

        if (location == null) {
            return;
        }

        if (e instanceof NPC) {
            Tile[] newTiles = NPC.getTiles((NPC) e, location);
            NPC    npc      = (NPC) e;

            for (Tile t : newTiles) {
                t.addNpcChunk(npc);
            }
        }

        r = getRegionIfAbsent(location);
        r.add(e, location);
    }

    /**
     * Method addRegion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public Region addRegion(int x, int y) {
        Region r = new Region(x, y);

        regions[x][y] = r;

        return r;
    }

    /**
     * Method getTile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public Tile getTile(int x, int y, int h) {
        return getRegionIfAbsent(Point.location(x, y, h)).getTile(x, y, h);
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return activeRegions.size();
    }

    /**
     * Method getSurroundingRegions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     *
     * @return
     */
    public Region[] getSurroundingRegions(Point location) {
        int      regionX     = location.getRegionX();
        int      regionY     = location.getRegionY();
        Region[] surrounding = new Region[28];

        surrounding[0]  = getRegion(regionX, regionY);
        surrounding[1]  = getRegion(regionX - 1, regionY - 1);
        surrounding[2]  = getRegion(regionX + 1, regionY + 1);
        surrounding[3]  = getRegion(regionX - 1, regionY);
        surrounding[4]  = getRegion(regionX, regionY - 1);
        surrounding[5]  = getRegion(regionX + 1, regionY);
        surrounding[6]  = getRegion(regionX, regionY + 1);
        surrounding[7]  = getRegion(regionX - 1, regionY + 1);
        surrounding[8]  = getRegion(regionX + 1, regionY - 1);
        surrounding[9]  = getRegion(regionX + 2, regionY);
        surrounding[10] = getRegion(regionX - 2, regionY);
        surrounding[11] = getRegion(regionX - 2, regionY - 1);
        surrounding[12] = getRegion(regionX - 2, regionY - 2);
        surrounding[13] = getRegion(regionX - 1, regionY - 2);
        surrounding[14] = getRegion(regionX, regionY - 2);
        surrounding[15] = getRegion(regionX + 1, regionY - 2);
        surrounding[16] = getRegion(regionX + 2, regionY - 2);
        surrounding[17] = getRegion(regionX + 2, regionY - 1);
        surrounding[18] = getRegion(regionX + 2, regionY + 1);
        surrounding[19] = getRegion(regionX + 2, regionY + 2);
        surrounding[20] = getRegion(regionX + 2, regionY + 1);
        surrounding[21] = getRegion(regionX + 1, regionY + 2);
        surrounding[22] = getRegion(regionX + 2, regionY + 1);
        surrounding[23] = getRegion(regionX, regionY + 2);
        surrounding[24] = getRegion(regionX - 1, regionY + 2);
        surrounding[25] = getRegion(regionX + 2, regionY + 1);
        surrounding[26] = getRegion(regionX - 2, regionY + 2);
        surrounding[27] = getRegion(regionX - 2, regionY + 1);

        return surrounding;
    }
}
