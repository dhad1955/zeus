package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.duel.DuelArena;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.ArgumentTokenizer;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 15:43
 */
public class Zone {

    /** BARROWS_AREA made: 14/08/18 **/
    private static final Zone BARROWS_ZONE = new Zone("barrows",  new Zone(
                                                     3524, 3270, 3584, 3309, -1, false, false,
                                                     "barrows_digsite").setBankingAllowed(false).setStatic(
                                                         true), new Zone(
                                                         3520, 9678, 3583, 9719, 3, false, false,
                                                         "barrows_tomb").setBankingAllowed(false).setTeleportAllowed(
                                                             false).setTeleportMessage(
                                                             "You can't teleport out of a tomb."));

    /** knownAreas made: 14/08/18 **/
    private static ArrayList<Zone> knownZones = new ArrayList<Zone>();

    /** loadPlayer made: 14/08/18 **/
    private static Player loadPlayer;

    /** isMulti made: 14/08/18 **/
    private boolean isMulti = false;

    /** familiarsBanned made: 14/08/18 **/
    private boolean familiarsBanned = false;

    /** teleport_allowed made: 14/08/18 **/
    private boolean teleport_allowed = true;

    /** teleport_message made: 14/08/18 **/
    private String teleport_message = "";

    /** banking_allowed made: 14/08/18 **/
    private boolean banking_allowed = true;

    /** areaName made: 14/08/18 **/
    private String areaName = "";

    public boolean isCannonNotAllowed() {
        return cannonAllowed;
    }

    public void setCannonAllowed(boolean cannonAllowed) {
        this.cannonAllowed = cannonAllowed;
    }

    private boolean cannonAllowed = false;

    private boolean is_wild_zone = false;

    private int wild_lvl = 0;


    /** regions made: 14/08/18 **/
    private ArrayList<Region> regions = new ArrayList<Region>();

    /** bob_allowed made: 14/08/18 **/
    private boolean bob_allowed = false;

    /** staticArea made: 14/08/18 **/
    private boolean staticArea = false;

    /** lowerBoundX, lowerboundY, higherBoundX, higherBoundY, height made: 14/08/18 **/
    private int lowerBoundX, lowerboundY, higherBoundX, higherBoundY, height;

    /** childAreas made: 14/08/18 **/
    private Zone[] childZones;

    private List<ZoneLeaveEvent> leaveEvents = new LinkedList<ZoneLeaveEvent>();


    public void addLeaveEvent(ZoneLeaveEvent<? extends Entity> event) {
        this.leaveEvents.add(event);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param children
     */

    public boolean isWilderness() {
        return is_wild_zone;
    }

    public int getWildernessLevel() {
        return this.wild_lvl;
    }
    public Zone(String name, Zone... children) {
        this.areaName   = name;
        this.childZones = children;

        for (Zone e : childZones) {
            for (int x_bound = e.lowerBoundX; x_bound <= e.higherBoundX; x_bound++) {
                for (int y_bound = e.lowerboundY; y_bound <= e.higherBoundY; y_bound++) {
                    World.getWorld().getRegionManager().getRegionIfAbsent(Point.location(x_bound, y_bound,
                            0)).addAreaIfAbsent(this);
                }
            }
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param lowerBoundX
     * @param lowerY
     * @param bigX
     * @param bigY
     * @param height
     * @param multi
     * @param noFamiliar
     * @param name
     */
    public Zone(int lowerBoundX, int lowerY, int bigX, int bigY, int height, boolean multi, boolean noFamiliar,
                String name) {
        this.lowerBoundX     = lowerBoundX;
        this.lowerboundY     = lowerY;
        this.higherBoundX    = bigX;
        this.higherBoundY    = bigY;
        this.areaName        = name;
        this.isMulti         = multi;
        this.height          = height;
        this.familiarsBanned = noFamiliar;

        for (int i = lowerBoundX; i <= higherBoundX; i++) {
            for (int y = lowerboundY; y <= higherBoundY; y++) {
                Point p = Point.location(i, y, 0);

                World.getWorld().getRegionManager().getRegionIfAbsent(p).addAreaIfAbsent(this);
            }
        }
    }

    /**
     * Method isInJail
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     *
     * @return
     */
    public static boolean isInJail(Point location) {
        return (location.getX() == 2606) && (location.getY() == 3105);
    }

    /**
     * Method isBobAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBobAllowed() {
        return bob_allowed;
    }

    /**
     * Method setStatic
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     *
     * @return
     */
    public Zone setStatic(boolean b) {
        staticArea = true;

        return this;
    }

    /**
     * Method isStaticArea
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isStaticArea() {
        return staticArea;
    }

    /**
     * Method setBobAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     *
     * @return
     */
    public Zone setBobAllowed(boolean b) {
        bob_allowed = b;

        return this;
    }

    /**
     * Method canTeleport
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean canTeleport() {
        return teleport_allowed;
    }

    /**
     * Method getTeleMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getTeleMessage() {
        return teleport_message;
    }

    /**
     * Method canBank
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean canBank(Player player) {
        if (player.isInArea(ClanWar.CLAN_WARS_HOME)) {
            return true;
        }

        for (Zone e : player.getAreas()) {
            if (!e.isBankingAllowed()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Method canBob
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean canBob(Player player) {
        for (Zone e : player.getAreas()) {
            if (!e.isBobAllowed()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Method setTeleportMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public Zone setTeleportMessage(String str) {
        teleport_message = str;

        return this;
    }

    /**
     * Method setTeleportAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     *
     * @return
     */
    public Zone setTeleportAllowed(boolean b) {
        teleport_allowed = b;

        return this;
    }

    /**
     * Method setBankingAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     *
     * @return
     */
    public Zone setBankingAllowed(boolean b) {
        banking_allowed = b;

        return this;
    }

    /**
     * Method isBankingAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBankingAllowed() {
        return banking_allowed;
    }

    private void rehash() {
        for (int i = lowerBoundX; i <= higherBoundX; i++) {
            for (int y = lowerboundY; y <= higherBoundY; y++) {
                Point p = Point.location(i, y, 0);

                World.getWorld().getRegionManager().getRegionIfAbsent(p).addAreaIfAbsent(this);
            }
        }
    }

    /**
     * Method isFamiliarsBanned
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isFamiliarsBanned() {
        return familiarsBanned;
    }

    /**
     * Method isInZone
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     *
     * @return
     */
    public boolean isInArea(Point p) {

         if(heightSensitive && height != p.getHeight() && height != -1)
                 return false;
        if (this.childZones != null) {
            for (Zone child_zone : childZones) {
                if (child_zone.isInArea(p)) {
                    return true;
                }
            }
        }

        return ((p.getX() >= lowerBoundX) && (p.getX() <= higherBoundX) && (p.getY() >= lowerboundY)
                && (p.getY() <= higherBoundY));
    }

    /**
     * Method setMulti
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param multi
     */
    public void setMulti(boolean multi) {
        this.isMulti = multi;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return areaName;
    }

    /**
     * Method loadAreas
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */

    public boolean heightSensitive = false;

    public Zone setHeightSensitive(boolean b){
        this.heightSensitive = b;
    return this;
    }
    public static boolean loadAreas() {
        Document doc;

        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder        builder = factory.newDocumentBuilder();

            doc = builder.parse(new File(Main.getConfigPath() + "/world/areas.xml"));
        } catch (Throwable e) {
            e.printStackTrace();

            return false;
        }

        NodeList nodeList = doc.getDocumentElement().getChildNodes();
        int      siz      = 0;

        for (short i = 1; i < nodeList.getLength(); i += 2) {
            Node n = nodeList.item(i);

            if (n != null) {
                if (n.getNodeName().equalsIgnoreCase("Area")) {
                    Zone e;
                    int      lowerX           = 0,
                             lowerY           = 0,
                             higherX          = 0,
                             higherY          = 0,
                            wild_lvl = 0;
                    String   name             = null;
                    String   teleportMessage  = "The god's stop you from teleporting";
                    boolean  familiarsBlocked = false,
                             teleportAllowed  = true,
                             bankingBlocked   = false,
                            wild = false,

                             multi_combat     = false;
                    NodeList list             = n.getChildNodes();

                    for (int a = 1; a < list.getLength(); a += 2) {
                        Node node = list.item(a);

                        if (node.getNodeName().equalsIgnoreCase("boundry")) {
                            String   text  = node.getTextContent();
                            String[] split = text.split(" ");

                            lowerX  = Integer.parseInt(split[0]);
                            lowerY  = Integer.parseInt(split[1]);
                            higherX = Integer.parseInt(split[2]);
                            higherY = Integer.parseInt(split[3]);
                        }

                        if (node.getNodeName().equalsIgnoreCase("familiarsBlocked")) {
                            familiarsBlocked = Boolean.parseBoolean(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("multi")) {
                            multi_combat = Boolean.parseBoolean(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("bankingBlocked")) {
                            bankingBlocked = Boolean.parseBoolean(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("teleportBlocked")) {
                            teleportAllowed = !Boolean.parseBoolean(node.getTextContent());
                        } else if (node.getNodeName().equalsIgnoreCase("name")) {
                            name = node.getTextContent();
                        } else if (node.getNodeName().equalsIgnoreCase("teleportMessage")) {
                            teleportMessage = node.getTextContent();
                        } else if(node.getNodeName().equalsIgnoreCase("isWild")){
                            wild = Boolean.parseBoolean(node.getTextContent());
                        }else if(node.getNodeName().equalsIgnoreCase("wildlvl"))
                            wild_lvl = Integer.parseInt(node.getTextContent());
                    }

                    e                  = new Zone(lowerX, lowerY, higherX, higherY, -1, multi_combat, familiarsBlocked,
                                                  name);
                    e.banking_allowed  = !bankingBlocked;
                    e.teleport_allowed = teleportAllowed;
                    e.teleport_message = teleportMessage;
                    e.wild_lvl = wild_lvl;
                    e.is_wild_zone = wild;
                    knownZones.add(e);
                }
            }
        }

        DuelArena.load_areas();
        ClanWar.CLAN_WARS_HOME.canTeleport();


        return true;
    }

    /**
     * Method loadsAreas
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadsAreas() {
        int lineN = 0;

        try {
            BufferedReader br   = new BufferedReader(new FileReader("config/areas.cfg"));
            String         line = null;

            while ((line = br.readLine()) != null) {
                lineN++;

                if ((line.length() == 0) || line.startsWith("//")) {
                    continue;
                }

                String[]          split = line.split(" ");
                ArgumentTokenizer PR    = new ArgumentTokenizer(split);

                knownZones.add(new Zone(PR.nextInt(), PR.nextInt(), PR.nextInt(), PR.nextInt(), PR.nextInt(),
                                        PR.nextString().equals("true"), PR.nextString().equals("true"),
                                        PR.nextString()));
            }

            br.close();

            BufferedWriter writer = new BufferedWriter(new FileWriter("config/world/areas.xml"));

            for (Zone e : knownZones) {
                writer.write("<Area>");
                writer.newLine();
                writer.write("<name>" + e.areaName + "</name>");
                writer.newLine();
                writer.write("<boundry>" + e.lowerBoundX + " " + e.lowerboundY + " " + e.higherBoundX + " "
                             + e.higherBoundY + "</boundry>");
                writer.newLine();
                writer.write("<familiarsBlocked>" + e.familiarsBanned + "</familiarsBlocked>");
                writer.newLine();
                writer.write("<multi>" + e.isMulti + "</multi>");
                writer.newLine();
                writer.write("<teleportBlocked>false</teleportBlocked>");
                writer.newLine();
                writer.write("<bankingBlocked>false</bankingBlocked>");
                writer.newLine();
                writer.write("</Area>");
                writer.newLine();
                writer.newLine();
            }

            writer.close();
        } catch (Exception ee) {
            if (loadPlayer != null) {
                loadPlayer.getActionSender().sendMessage("@red@ERROR areas.cfg parse error at " + lineN);
                loadPlayer.getActionSender().sendMessage(
                    "@red@Please get this fixed ASAP area's are crucial to the server");
            }

            ee.printStackTrace();
        }

        /*
         * Load core area's here
         */
        DuelArena.load_areas();
    }

    /**
     * Method addRegion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param r
     */
    public void addRegion(Region r) {
        regions.add(r);
    }

    /**
     * Method reload
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void reload(Player pla) {
        ArrayList<Zone> staticZones = new ArrayList<Zone>();

        for (Zone e : knownZones) {
            if (e.isStaticArea()) {
                staticZones.add(e);
            }

            for (Region r : e.regions) {
                r.getAreas().clear();
            }
        }

        for (Player p : World.getWorld().getPlayers()) {
            p.getAreas().clear();
        }

        knownZones.clear();
        loadAreas();

        for (Zone e : staticZones) {
            knownZones.add(e);
            e.rehash();
        }

        loadPlayer = pla;
    }

    /**
     * Method isMulti
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isMulti() {
        return isMulti;
    }
}
