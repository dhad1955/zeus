package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.io.js5.CacheManager;
import net.tazogaming.hydra.io.IoUtils;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 25/09/13
 * Time: 19:04
 * To change this template use File | Settings | File Templates.
 */
public class MapFetcher {
    public static final MapXTEA       xtea       = new MapXTEA();
    private static ArrayList<Integer> nulledMaps = new ArrayList<Integer>();
    private static int[][]            mapIndicies;

    /**
     * Constructs ...
     *
     */
    public MapFetcher() {
        loadIndicies();



        for (int i = 0; i < (World.MAX_WIDTH / 64); i++) {
            for (int k = 0; k < (World.MAX_HEIGHT) / 64; k++) {
                Landscape r = null;

                r = getLandscapeRaw(i, k, false);

                if(i % 100 ==0 && k % 100 == 0)
                    System.gc();

                if (r != null) {
                    try {
                        r.loadToRegions(World.getWorld().getRegionManager());
                    } catch (Exception ee) {
                        ee.printStackTrace();
                        Logger.err("Error, encountered encrypted map file: " + i + ", " + k);
                    }
                }
            }
        }
    }

    /**
     * Method loadIndicies
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadIndicies() {
        byte[] dataBuffer = IoUtils.ReadFile(Main.getConfigPath() + "world/map_index.dat");

        if (dataBuffer == null) {
            throw new RuntimeException("Could not find map_index");
        }

        Packet p    = new Packet(dataBuffer);
        int    size = dataBuffer.length / 6;

        mapIndicies = new int[3][size];

        int c = 0;

        for (int i2 = 0; i2 < size; i2++) {
            for (int i = 0; i < 3; i++) {
                mapIndicies[i][i2] = p.readShort();
                c++;
            }
        }

        int loaded = 0;
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param rot
     *
     * @return
     */
    public static Landscape get(int x, int y, int rot) {
        if (mapIndicies == null) {
            loadIndicies();
        }

        return getLandscapeRaw(x, y, rot, false);
    }

    /**
     * Method getLandscapeRaw
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param landscapeX
     * @param landscapeY
     * @param decrypt
     *
     * @return
     */
    public static Landscape getLandscapeRaw(int landscapeX, int landscapeY, boolean decrypt) {
        return getLandscapeRaw(landscapeX, landscapeY, -1, decrypt);
    }

    /**
     * Method getLandscapeRaw
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param landscapeX
     * @param landscapeY
     * @param rot
     * @param decrypt
     *
     * @return
     */
    public static Landscape getLandscapeRaw(int landscapeX, int landscapeY, int rot, boolean decrypt) {
        int mapIndex     = getMapIndex(1, landscapeX, landscapeY);
        int terrainIndex = getMapIndex(0, landscapeX, landscapeY);


        mapIndex = CacheManager.getArchiveName(5, "l"+landscapeX+"_"+landscapeY);
        terrainIndex = CacheManager.getArchiveName(5, "m"+landscapeX+"_"+landscapeY);
        if (mapIndex == -1 || mapIndex == 0) {
            return null;
        }

        byte[] buffer        = loadMap(mapIndex);
        byte[] terrainBuffer = loadMap(terrainIndex);


        if(mapIndex == 3555 || mapIndex == 3573 || mapIndex == 3556 || mapIndex == 3547 || mapIndex == 3555 || mapIndex == 3549 || mapIndex == 3548) {//|| mapIndex == 3579 || mapIndex == 3580 || mapIndex == 3580 || mapIndex == 3576 ||  mapIndex == 3560 || mapIndex == 3561 || mapIndex == 3548)
       // {
            mapIndex = -mapIndex;
        }
        if (buffer == null) {
            return null;
        }

        try {
            return new Landscape(landscapeX, landscapeY, buffer, terrainBuffer, rot, new int[] { terrainIndex,
                    mapIndex });
        } catch (Exception ee) {
            if(mapIndex < 0)
                ee.printStackTrace();

        }

        return null;
    }

    /**
     * Method get_terrain_index
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param regx
     * @param regy
     */
    public void get_terrain_index(int regx, int regy) {
        int landscapeX   = (regx + 6) / 8;
        int landscapeY   = (regy + 6) / 8;
        int mapIndex     = getMapIndex(1, landscapeX, landscapeY);
        int terrainIndex = getMapIndex(0, landscapeX, landscapeY);
    }

    /**
     * Method getLandscape
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param regionX
     * @param regionY
     *
     * @return
     */
    public Landscape getLandscape(int regionX, int regionY) {
        int landscapeX   = (regionX + 6) / 8;
        int landscapeY   = (regionY + 6) / 8;
        int mapIndex     = getMapIndex(1, landscapeX, landscapeY);
        int terrainIndex = getMapIndex(0, landscapeX, landscapeY);


        terrainIndex = CacheManager.getArchiveName(5, "l_" + landscapeX + "_" + landscapeY);
        if (mapIndex == -1) {
            return null;
        }

        byte[] buffer        = loadMap(mapIndex);
        byte[] terrainBuffer = loadMap(terrainIndex);

        if (buffer == null) {
            return null;
        }

        return new Landscape(landscapeX, landscapeY, buffer, terrainBuffer);
    }

    private static byte[] loadMap(int id) {
        return IoUtils.ReadFile(Main.getConfigPath() + "mapdata/" + id);
    }

    private static int getMapIndex(int type, int regionX, int regionY) {
        int i1 = (regionX << 8) + regionY;

        for (int i = 0; i < mapIndicies[0].length; i++) {
            if (mapIndicies[0][i] == i1) {
                if (type == 0) {
                    return (mapIndicies[1][i] > 5550)
                           ? -1
                           : mapIndicies[1][i];
                } else {
                    return (mapIndicies[2][i] > 5500)
                           ? -1
                           : mapIndicies[2][i];
                }
            }
        }

        return -1;
    }
}
