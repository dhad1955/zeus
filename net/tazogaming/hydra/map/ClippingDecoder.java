package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 25/09/13
 * Time: 23:02
 * To change this template use File | Settings | File Templates.
 */
public class ClippingDecoder {
    public static final int FLAG_WEST  = 0x1280108;
    public static final int FLAG_SOUTH = 0x1280102;
    public static final int FLAG_EAST  = 0x1280180;
    public static final int FLAG_NORTH = 0x1280120;

    /**
     * Rune War Official Source
     * Created for http://hydra.tazogaming.org}
     *
     * @author MDMA                           Enchanta
     *         Date: 14-Aug-2010 {17:21:48}
     *
     * @param x
     * @param y
     * @param h
     * @param shift
     */

    // /  public static int[][][] clippingData = new int[2][5000][12000];
    public static void removeClipping(int x, int y, int h, int shift) {
        Tile t    = World.getWorld().getRegionManager().getTile(x, y, h);
        int  data = t.getClippingData();

        data &= 0xffffff - shift;
        t.setClippingData(data);
    }

    /**
     * Method removeClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     */
    public static void removeClipping(int x, int y, int h) {
        Tile t = World.getWorld().getRegionManager().getTile(x, y, h);

        t.setClippingData(0);
    }

    /**
     * Method allowProjectileClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tx
     * @param ty
     * @param h
     */
    public void allowProjectileClipping(int tx, int ty, int h) {
        World.getWorld().getRegionManager().getTile(tx, ty, h).setProjectileClipped(false);
    }

    /**
     * Method containsSolid
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public static boolean containsSolid(int x, int y, int h) {
        return getClipping(x, y, h) == 0x40000;
    }

    /**
     * Method isWaterOrHill
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public static boolean isWaterOrHill(int x, int y, int h) {
        return (getClipping(x, y, h) & 0x20000) != 0;
    }

    /**
     * Method addClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     * @param shift
     */
    public static void addClipping(int x, int y, int h, int shift) {


        Tile t    = World.getWorld().getRegionManager().getTile(x, y, h);
        int  data = t.getClippingData();

        data |= shift;

        t.setClippingData(data);

    }

    /**
     * Method unReachable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public static boolean unReachable(int x, int y, int h) {
        return blockedNorth(x, y, h) && blockedEast(x, y, h) && blockedWest(x, y, h) && blockedSouth(x, y, h)
               && blockedSouthEast(x, y, h) && blockedNorthEast(x, y, h) && blockedNorthWest(x, y, h)
               && blockedSouthWest(x, y, h);
    }

    /**
     * Method getClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public static int getClipping(int x, int y, int h) {
        if ((x == -1) || (y == -1)) {
            return 0;
        }

        Tile t = World.getWorld().getRegionManager().getTile(x, y, h);

        return t.getClippingData();
    }

    /**
     * Method unclip
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param h
     */
    public static void unclip(int x, int y, int h) {
        int data = World.getWorld().getTile(x, y, h).getClippingData();

        data &= 0xdfffff;
        World.getWorld().getTile(x, y, h).setClippingData(data);
    }

    /**
     * Method addObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objectId
     * @param x
     * @param y
     * @param height
     * @param type
     * @param direction
     * @param ignoreObjects
     */
    public static void addObject(int objectId, int x, int y, int height, int type, int direction,
                                 boolean ignoreObjects) {
        if ((objectId == 0) && (type == 10)) {
            World.getWorld().getTile(x, y, height).setClippingData(0);

            return;
        }



        if (!ignoreObjects && (objectId == -1)) {
            removeObject(x, y, height);
        }

        if (objectId == -1) {
            return;
        }

        CacheObjectDefinition def = CacheObjectDefinition.forId(objectId);

        if (def == null) {
            return;
        }

        int xLength;
        int yLength;

        if ((direction != 1) && (direction != 3)) {
            xLength = def.sizeX;
            yLength = def.sizeY;
        } else {
            xLength = def.sizeY;
            yLength = def.sizeX;
        }

        boolean objectAdded = false;

        if (type == 22) {
            if (def.getClippingType() > 0) {
                if (!ignoreObjects) {

                    // removeObject(x, y, height);
                }

                // if(def.getClippingType() == 1)
                // addClipping(x, y, height, 0x200000);
            }
        } else if ((type >= 9) && (type <= 11)) {
            if (def.getClippingType() != 0) {

                if(objectId != 53406)
                    addClippingForSolidObject(x, y, height, xLength, yLength, !def.isProjectileClipped(), true);
            }
        } else if ((type >= 0) && (type <= 3)) {
            if (def.getClippingType() != 0) {
                if (!ignoreObjects) {
                    removeObject(x, y, height);
                }

                addWall(x, y, height, type, direction, def.isProjectileClipped(), true);
            }
        }
    }

    static int objectID = -1;

    /*
     *           if(ClippingDecoder.reachedFacingObject(to.getY(), to.getX(), curAbsX, size_y, rotationInfo, size_x, curAbsY))
     *
     */

    /**
     * Method reachedFacingObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dest_x
     * @param dest_y
     * @param check_x
     * @param height
     *
     * @return
     */

    public static boolean reachedWallDecoration(int dest_x, int dest_y, int check_y, int check_x, int obj_rotation, int obj_type, int height)
    {
        if(check_x == dest_x && check_y == dest_y)
            return true;

        if(obj_type == 6 || obj_type == 7)
        {
            if(obj_type == 7)
                obj_rotation = obj_rotation + 2 & 3;
            if(obj_rotation == 0)
            {
                if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height)  & 0x80) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height)  & 2) == 0)
                    return true;
            } else
            if(obj_rotation == 1)
            {
                if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height)  & 8) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height)  & 2) == 0)
                    return true;
            } else
            if(obj_rotation == 2)
            {
                if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height)  & 8) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height)  & 0x20) == 0)
                    return true;
            } else
            if(obj_rotation == 3)
            {
                if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height)  & 0x80) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height)  & 0x20) == 0)
                    return true;
            }
        }
        if(obj_type == 8)
        {
            if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height)  & 0x20) == 0)
                return true;
            if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height)  & 2) == 0)
                return true;
            if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height)  & 8) == 0)
                return true;
            if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height)  & 0x80) == 0)
                return true;
        }
        return false;
    }


    public boolean reachedWallObject(int dest_x, int dest_y, int check_y, int check_x, int obejct_type, int object_rotation, int height)
    {
        if(check_x == dest_x && check_y == dest_y)
            return true;

        if(obejct_type == 0)
            if(object_rotation == 0)
            {
                if(check_x == dest_x - 1 && check_y == dest_y)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height) & 0x1280120) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height) & 0x1280102) == 0)
                    return true;
            } else
            if(object_rotation == 1)
            {
                if(check_x == dest_x && check_y == dest_y + 1)
                    return true;
                if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280108) == 0)
                    return true;
                if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280180) == 0)
                    return true;
            } else
            if(object_rotation == 2)
            {
                if(check_x == dest_x + 1 && check_y == dest_y)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height) & 0x1280120) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height) & 0x1280102) == 0)
                    return true;
            } else
            if(object_rotation == 3)
            {
                if(check_x == dest_x && check_y == dest_y - 1)
                    return true;
                if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280108) == 0)
                    return true;
                if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280180) == 0)
                    return true;
            }
        if(obejct_type == 2)
            if(object_rotation == 0)
            {
                if(check_x == dest_x - 1 && check_y == dest_y)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1)
                    return true;
                if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280180) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height) & 0x1280102) == 0)
                    return true;
            } else
            if(object_rotation == 1)
            {
                if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280108) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1)
                    return true;
                if(check_x == dest_x + 1 && check_y == dest_y)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height) & 0x1280102) == 0)
                    return true;
            } else
            if(object_rotation == 2)
            {
                if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280108) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height)& 0x1280120) == 0)
                    return true;
                if(check_x == dest_x + 1 && check_y == dest_y)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1)
                    return true;
            } else
            if(object_rotation == 3)
            {
                if(check_x == dest_x - 1 && check_y == dest_y)
                    return true;
                if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height) & 0x1280120) == 0)
                    return true;
                if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x1280180) == 0)
                    return true;
                if(check_x == dest_x && check_y == dest_y - 1)
                    return true;
            }
        if(obejct_type == 9)
        {
            if(check_x == dest_x && check_y == dest_y + 1 && (getClipping(check_x,check_y, height) & 0x20) == 0)
                return true;
            if(check_x == dest_x && check_y == dest_y - 1 && (getClipping(check_x,check_y, height) & 2) == 0)
                return true;
            if(check_x == dest_x - 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 8) == 0)
                return true;
            if(check_x == dest_x + 1 && check_y == dest_y && (getClipping(check_x,check_y, height) & 0x80) == 0)
                return true;
        }
        return false;
    }

    public static boolean reachedFacingObject(int dest_y, int dest_x, int check_x, int size_y, int rotationInfo,
            int size_x, int current_y, int height) {
        int l1 = (dest_x + size_x) - 1;
        int i2 = (dest_y + size_y) - 1;

        if ((check_x >= dest_x) && (check_x <= l1) && (current_y >= dest_y) && (current_y <= i2)) {
            return true;
        }

        if ((check_x == dest_x - 1) && (current_y >= dest_y) && (current_y <= i2)
                && (getClipping(check_x, current_y, height) & 8) == 0 && (rotationInfo & 8) == 0) {
            return true;
        }

        if ((check_x == l1 + 1) && (current_y >= dest_y) && (current_y <= i2)
                && (getClipping(check_x, current_y, height) & 0x80) == 0 && (rotationInfo & 2) == 0) {
            return true;
        }

        return ((current_y == dest_y - 1) && (check_x >= dest_x) && (check_x <= l1)
                && (getClipping(check_x, current_y, height) & 2) == 0
                && (rotationInfo & 4) == 0) || ((current_y == i2 + 1) && (check_x >= dest_x) && (check_x <= l1)
                    && (getClipping(check_x, current_y, height) & 0x20) == 0 && (rotationInfo & 1) == 0);
    }

    /**
     * Method getClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param height
     * @param moveTypeX
     * @param moveTypeY
     *
     * @return
     */
    public static boolean getClipping(int x, int y, int height, int moveTypeX, int moveTypeY) {
        try {
            if (height > 3) {
                height = 0;
            }

            int checkX = (x + moveTypeX);
            int checkY = (y + moveTypeY);

            if ((moveTypeX == -1) && (moveTypeY == 0)) {
                return (getClipping(x, y, height) & 0x1280108) == 0;
            } else if ((moveTypeX == 1) && (moveTypeY == 0)) {
                return (getClipping(x, y, height) & FLAG_EAST) == 0;
            } else if ((moveTypeX == 0) && (moveTypeY == -1)) {
                return (getClipping(x, y, height) & 0x1280102) == 0;
            } else if ((moveTypeX == 0) && (moveTypeY == 1)) {
                return (getClipping(x, y, height) & 0x1280120) == 0;
            } else if ((moveTypeX == -1) && (moveTypeY == -1)) {
                return ((getClipping(x, y, height) & 0x128010e) == 0
                        && (getClipping(checkX - 1, checkY, height) & 0x1280108) == 0
                        && (getClipping(checkX - 1, checkY, height) & 0x1280102) == 0);
            } else if ((moveTypeX == 1) && (moveTypeY == -1)) {
                return ((getClipping(x, y, height) & 0x1280183) == 0
                        && (getClipping(checkX + 1, checkY, height) & FLAG_EAST) == 0
                        && (getClipping(checkX, checkY - 1, height) & 0x1280102) == 0);
            } else if ((moveTypeX == -1) && (moveTypeY == 1)) {
                return ((getClipping(x, y, height) & 0x1280138) == 0
                        && (getClipping(checkX - 1, checkY, height) & 0x1280108) == 0
                        && (getClipping(checkX, checkY + 1, height) & 0x1280120) == 0);
            } else if ((moveTypeX == 1) && (moveTypeY == 1)) {
                return ((getClipping(x, y, height) & 0x12801e0) == 0
                        && (getClipping(checkX + 1, checkY, height) & FLAG_EAST) == 0
                        && (getClipping(checkX, checkY + 1, height) & 0x1280120) == 0);
            } else {

                // System.out.println("[FATAL ERROR]: At getClipping: "+x+", "+y+", "+height+", "+moveTypeX+", "+moveTypeY);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();

            return true;
        }
    }

    /**
     * Method blockedNorth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedNorth(int x, int y, int z) {
        return (getClipping(x, y + 1, z) & FLAG_NORTH) != 0 || isWaterOrHill(x, y + 1, z);
    }

    /**
     * Method blockedEast
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedEast(int x, int y, int z) {
        return (getClipping(x + 1, y, z) & FLAG_EAST) != 0 || isWaterOrHill(x + 1, y, z);
    }

    /**
     * Method blockedSouth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedSouth(int x, int y, int z) {
        return (getClipping(x, y - 1, z) & FLAG_SOUTH) != 0 || isWaterOrHill(x, y - 1, z);
    }

    /**
     * Method blockedWest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedWest(int x, int y, int z) {
        return (getClipping(x - 1, y, z) & FLAG_WEST) != 0 || isWaterOrHill(x - 1, y, z);
    }

    /*
     *           && (ai[j3 + 1][k3 + 1] & 0x12801e0) == 0 && (ai[j3 + 1][k3] & 0x1280180) == 0
     * && (ai[j3][k3 + 1] & 0x1280120) == 0) {
     */

    /*
     *
     *           if ((j3 < byte0 - 1) && (k3 < byte1 - 1) && (anIntArrayArray901[j3 + 1][k3 + 1] == 0)
     *                   && (ai[j3 + 1][k3 + 1] & 0x12801e0) == 0 && (ai[j3 + 1][k3] & 0x1280180) == 0
     *                   && (ai[j3][k3 + 1] & 0x1280120) == 0) {
     *               bigX[l3]                           = j3 + 1;
     *               bigY[l3]                           = k3 + 1;
     */

    /**
     * Method blockedNorthEast
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedNorthEast(int x, int y, int z) {
        return (getClipping(x + 1, y + 1, z) & 0x12801e0) != 0 || (getClipping(x + 1, y, z) & FLAG_EAST) != 0
               || (getClipping(x, y + 1, z) & 0x1280120) != 0 || isWaterOrHill(x + 1, y + 1, z);
    }

    /*
     * if ((j3 > 0) && (k3 < byte1 - 1) && (anIntArrayArray901[j3 - 1][k3 + 1] == 0)
     *                   && (ai[j3 - 1][k3 + 1] & 0x1280138) == 0 && (ai[j3 - 1][k3] & 0x1280108) == 0
     *                   && (ai[j3][k3 + 1] & 0x1280120) == 0) {
     */

    /**
     * Method blockedNorthWest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedNorthWest(int x, int y, int z) {
        return (getClipping(x - 1, y + 1, z) & 0x1280138) != 0 || (getClipping(x - 1, y, z) & 0x1280108) != 0
               || (getClipping(x, y + 1, z) & 0x1280120) != 0 || isWaterOrHill(x - 1, y + 1, z);
    }

    /**
     * Method blockedSouthEast
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedSouthEast(int x, int y, int z) {
        return (getClipping(x + 1, y - 1, z) & 0x1280183) != 0 || (getClipping(x + 1, y, z) & FLAG_EAST) != 0
               || (getClipping(x, y - 1, z) & 0x1280102) != 0 || isWaterOrHill(x + 1, y - 1, z);
    }

    //
    // (clipData[j3 - 1][k3] & 0x1280108) == 0 && (clipData[j3][k3 - 1] & 0x1280102) == 0) {

    /**
     * Method blockedSouthWest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param z
     *
     * @return
     */
    public static boolean blockedSouthWest(int x, int y, int z) {
        return (getClipping(x - 1, y - 1, z) & 0x128010e) != 0 || (getClipping(x, y - 1, z) & 0x1280102) != 0
               || (getClipping(x - 1, y, z) & 0x1280108) != 0 || isWaterOrHill(x - 1, y - 1, z);
    }

    /**
     * Method addWall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param height
     * @param type
     * @param direction
     * @param projectiles
     * @param flag2
     */
    public static void addWall(int x, int y, int height, int type, int direction, boolean projectiles, boolean flag2) {
        if (type == 0) {
            if (direction == 0) {
                addClipping(x, y, height, 128);
                addClipping(x - 1, y, height, 8);
            } else if (direction == 1) {
                addClipping(x, y, height, 2);
                addClipping(x, y + 1, height, 32);
            } else if (direction == 2) {
                addClipping(x, y, height, 8);
                addClipping(x + 1, y, height, 128);
            } else if (direction == 3) {
                addClipping(x, y, height, 32);
                addClipping(x, y - 1, height, 2);
            }
        } else if ((type == 1) || (type == 3)) {
            if (direction == 0) {
                addClipping(x, y, height, 1);
                addClipping(x - 1, y + 1, height, 16);
            } else if (direction == 1) {
                addClipping(x, y, height, 4);
                addClipping(x + 1, y + 1, height, 64);
            } else if (direction == 2) {
                addClipping(x, y, height, 16);
                addClipping(x + 1, y - 1, height, 1);
            } else if (direction == 3) {
                addClipping(x, y, height, 64);
                addClipping(x - 1, y - 1, height, 4);
            }
        } else if (type == 2) {
            if (direction == 0) {
                addClipping(x, y, height, 130);
                addClipping(x - 1, y, height, 8);
                addClipping(x, y + 1, height, 32);
            } else if (direction == 1) {
                addClipping(x, y, height, 10);
                addClipping(x, y + 1, height, 32);
                addClipping(x + 1, y, height, 128);
            } else if (direction == 2) {
                addClipping(x, y, height, 40);
                addClipping(x + 1, y, height, 128);
                addClipping(x, y - 1, height, 2);
            } else if (direction == 3) {
                addClipping(x, y, height, 160);
                addClipping(x, y - 1, height, 2);
                addClipping(x - 1, y, height, 8);
            }
        }

        if (projectiles) {
            if (type == 0) {
                if (direction == 0) {
                    addClipping(x, y, height, 65536);
                    addClipping(x - 1, y, height, 4096);
                } else if (direction == 1) {
                    addClipping(x, y, height, 1024);
                    addClipping(x, y + 1, height, 16384);
                } else if (direction == 2) {
                    addClipping(x, y, height, 4096);
                    addClipping(x + 1, y, height, 65536);
                } else if (direction == 3) {
                    addClipping(x, y, height, 16384);
                    addClipping(x, y - 1, height, 1024);
                }
            }

            if ((type == 1) || (type == 3)) {
                if (direction == 0) {
                    addClipping(x, y, height, 512);
                    addClipping(x - 1, y + 1, height, 8192);
                } else if (direction == 1) {
                    addClipping(x, y, height, 2048);
                    addClipping(x + 1, y + 1, height, 32768);
                } else if (direction == 2) {
                    addClipping(x, y, height, 8192);
                    addClipping(x + 1, y + 1, height, 512);
                } else if (direction == 3) {
                    addClipping(x, y, height, 32768);
                    addClipping(x - 1, y - 1, height, 2048);
                }
            } else if (type == 2) {
                if (direction == 0) {
                    addClipping(x, y, height, 66560);
                    addClipping(x - 1, y, height, 4096);
                    addClipping(x, y + 1, height, 16384);
                } else if (direction == 1) {
                    addClipping(x, y, height, 5120);
                    addClipping(x, y + 1, height, 16384);
                    addClipping(x + 1, y, height, 65536);
                } else if (direction == 2) {
                    addClipping(x, y, height, 20480);
                    addClipping(x + 1, y, height, 65536);
                    addClipping(x, y - 1, height, 1024);
                } else if (direction == 3) {
                    addClipping(x, y, height, 81920);
                    addClipping(x, y - 1, height, 1024);
                    addClipping(x - 1, y, height, 4096);
                }
            }
        }

        if (flag2 && false) {
            if (type == 0) {
                if (direction == 0) {
                    addClipping(x, y, height, 0x20000000);
                    addClipping(x - 1, y, height, 0x2000000);
                }

                if (direction == 1) {
                    addClipping(x, y, height, 0x800000);
                    addClipping(x, y + 1, height, 0x8000000);
                }

                if (direction == 2) {
                    addClipping(x, y, height, 0x2000000);
                    addClipping(x + 1, y, height, 0x20000000);
                }

                if (direction == 3) {
                    addClipping(x, y, height, 0x8000000);
                    addClipping(x, y - 1, height, 0x800000);
                }
            }

            if ((type == 1) || (type == 3)) {
                if (direction == 0) {
                    addClipping(x, y, height, 4194304);
                    addClipping(x - 1, y + 1, height, 0x4000000);
                }

                if (direction == 1) {
                    addClipping(x, y, height, 0x1000000);
                    addClipping(1 + x, 1 + y, height, 0x10000000);
                }

                if (direction == 2) {
                    addClipping(x, y, height, 0x4000000);
                    addClipping(x + 1, -1 + y, height, 0x400000);
                }

                if (direction == 3) {
                    addClipping(x, y, height, 0x10000000);
                    addClipping(-1 + x, y - 1, height, 0x1000000);
                }
            }

            if (type == 2) {
                if (direction == 0) {
                    addClipping(x, y, height, 0x20800000);
                    addClipping(-1 + x, y, height, 0x2000000);
                    addClipping(x, 1 + y, height, 0x8000000);
                }

                if (direction == 1) {
                    addClipping(x, y, height, 0x2800000);
                    addClipping(x, 1 + y, height, 0x8000000);
                    addClipping(x + 1, y, height, 0x20000000);
                }

                if (direction == 2) {
                    addClipping(x, y, height, 0xa000000);
                    addClipping(1 + x, y, height, 0x20000000);
                    addClipping(x, y - 1, height, 0x800000);
                }

                if (direction == 3) {
                    addClipping(x, y, height, 0x28000000);
                    addClipping(x, y - 1, height, 0x800000);
                    addClipping(-1 + x, y, height, 0x2000000);
                }
            }
        }
    }

    private static void removeObject(int x, int y, int height) {}

    /**
     * Method removeObject2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param h
     */
    public static void removeObject2(int id, int x, int y, int h) {

        /*
         *       GameObject oldObj = World.getWorld().getObject(id, x, y, h);
         *
         *     if (oldObj != null) {
         *         CacheObjectDefinition def = CacheObjectDefinition.getQuest(oldObj.getId());
         *         int xLength;
         *         int yLength;
         *         if (Math.abs(oldObj.getDirection()) != 1 && Math.abs(oldObj.getDirection()) != 3) {
         *             xLength = def.xLength();
         *             yLength = def.yLength();
         *         } else {
         *             xLength = def.yLength();
         *             yLength = def.xLength();
         *         }
         *         if (oldObj.getType() == 22) {
         *             if (def.clippingType() == 1) {
         *                 removeClipping(x, y, h, 0x200000);
         *             }
         *         } else if (oldObj.getType() >= 9) {
         *             if (def.clippingType() != 0) {
         *                 removeClippingForSolidObject(x, y, h, xLength, yLength, def.solid());
         *             }
         *         } else if (oldObj.getType() >= 0 && oldObj.getType() <= 3) {
         *             if (def.clippingType() != 0) {
         *                 removeClippingForVariableObject(x, y, h, oldObj.getType(), Math.abs(oldObj.getDirection()), def.solid());
         *             }
         *         }
         *     }
         */
    }

    /**
     * Method removeClippingForVariableObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param height
     * @param type
     * @param direction
     * @param flag
     * @param flag2
     */
    public static void removeClippingForVariableObject(int x, int y, int height, int type, int direction, boolean flag,
            boolean flag2) {
        flag2 = false;

        if (type == 0) {
            if (direction == 0) {
                removeClipping(x, y, height, 128);
                removeClipping(x - 1, y, height, 8);
            } else if (direction == 1) {
                removeClipping(x, y, height, 2);
                removeClipping(x, y + 1, height, 32);
            } else if (direction == 2) {
                removeClipping(x, y, height, 8);
                removeClipping(x + 1, y, height, 128);
            } else if (direction == 3) {
                removeClipping(x, y, height, 32);
                removeClipping(x, y - 1, height, 2);
            }
        } else if ((type == 1) || (type == 3)) {
            if (direction == 0) {
                removeClipping(x, y, height, 1);
                removeClipping(x - 1, y, height, 16);
            } else if (direction == 1) {
                removeClipping(x, y, height, 4);
                removeClipping(x + 1, y + 1, height, 64);
            } else if (direction == 2) {
                removeClipping(x, y, height, 16);
                removeClipping(x + 1, y - 1, height, 1);
            } else if (direction == 3) {
                removeClipping(x, y, height, 64);
                removeClipping(x - 1, y - 1, height, 4);
            }
        } else if (type == 2) {
            if (direction == 0) {
                removeClipping(x, y, height, 130);
                removeClipping(x - 1, y, height, 8);
                removeClipping(x, y + 1, height, 32);
            } else if (direction == 1) {
                removeClipping(x, y, height, 10);
                removeClipping(x, y + 1, height, 32);
                removeClipping(x + 1, y, height, 128);
            } else if (direction == 2) {
                removeClipping(x, y, height, 40);
                removeClipping(x + 1, y, height, 128);
                removeClipping(x, y - 1, height, 2);
            } else if (direction == 3) {
                removeClipping(x, y, height, 160);
                removeClipping(x, y - 1, height, 2);
                removeClipping(x - 1, y, height, 8);
            }
        }

        if (flag) {
            if (type == 0) {
                if (direction == 0) {
                    removeClipping(x, y, height, 65536);
                    removeClipping(x - 1, y, height, 4096);
                } else if (direction == 1) {
                    removeClipping(x, y, height, 1024);
                    removeClipping(x, y + 1, height, 16384);
                } else if (direction == 2) {
                    removeClipping(x, y, height, 4096);
                    removeClipping(x + 1, y, height, 65536);
                } else if (direction == 3) {
                    removeClipping(x, y, height, 16384);
                    removeClipping(x, y - 1, height, 1024);
                }
            }

            if ((type == 1) || (type == 3)) {
                if (direction == 0) {
                    removeClipping(x, y, height, 512);
                    removeClipping(x - 1, y + 1, height, 8192);
                } else if (direction == 1) {
                    removeClipping(x, y, height, 2048);
                    removeClipping(x + 1, y + 1, height, 32768);
                } else if (direction == 2) {
                    removeClipping(x, y, height, 8192);
                    removeClipping(x + 1, y + 1, height, 512);
                } else if (direction == 3) {
                    removeClipping(x, y, height, 32768);
                    removeClipping(x - 1, y - 1, height, 2048);
                }
            } else if (type == 2) {
                if (direction == 0) {
                    removeClipping(x, y, height, 66560);
                    removeClipping(x - 1, y, height, 4096);
                    removeClipping(x, y + 1, height, 16384);
                } else if (direction == 1) {
                    removeClipping(x, y, height, 5120);
                    removeClipping(x, y + 1, height, 16384);
                    removeClipping(x + 1, y, height, 65536);
                } else if (direction == 2) {
                    removeClipping(x, y, height, 20480);
                    removeClipping(x + 1, y, height, 65536);
                    removeClipping(x, y - 1, height, 1024);
                } else if (direction == 3) {
                    removeClipping(x, y, height, 81920);
                    removeClipping(x, y - 1, height, 1024);
                    removeClipping(x - 1, y, height, 4096);
                }
            }
        }

        if (flag2) {
            if (type == 0) {
                if (direction == 0) {
                    removeClipping(x, y, height, 0x20000000);
                    removeClipping(-1 + x, y, height, 0x2000000);
                }

                if (direction == 1) {
                    removeClipping(x, y, height, 0x800000);
                    removeClipping(x, 1 + y, height, 0x8000000);
                }

                if (direction == 2) {
                    removeClipping(x, y, height, 0x2000000);
                    removeClipping(x + 1, y, height, 0x20000000);
                }

                if (direction == 3) {
                    removeClipping(x, y, height, 0x8000000);
                    removeClipping(x, -1 + y, height, 0x800000);
                }
            }

            if ((type == 1) || (type == 3)) {
                if (direction == 0) {
                    removeClipping(x, y, height, 0x400000);
                    removeClipping(x - 1, y + 1, height, 0x4000000);
                }

                if (direction == 1) {
                    removeClipping(x, y, height, 0x1000000);
                    removeClipping(1 + x, 1 + y, height, 0x10000000);
                }

                if (direction == 2) {
                    removeClipping(x, y, height, 0x4000000);
                    removeClipping(x + 1, -1 + y, height, 0x400000);
                }

                if (direction == 3) {
                    removeClipping(x, y, height, 0x10000000);
                    removeClipping(-1 + x, y - 1, height, 0x1000000);
                }
            }

            if (type == 2) {
                if (direction == 0) {
                    removeClipping(x, y, height, 0x20800000);
                    removeClipping(-1 + x, y, height, 0x2000000);
                    removeClipping(x, y + 1, height, 0x8000000);
                }

                if (direction == 1) {
                    removeClipping(x, y, height, 0x2800000);
                    removeClipping(x, y + 1, height, 0x8000000);
                    removeClipping(1 + x, y, height, 0x20000000);
                }

                if (direction == 2) {
                    removeClipping(x, y, height, 0xa000000);
                    removeClipping(x + 1, y, height, 0x20000000);
                    removeClipping(x, y - 1, height, 0x800000);
                }

                if (direction == 3) {
                    removeClipping(x, y, height, 0x28000000);
                    removeClipping(x, -1 + y, height, 0x800000);
                    removeClipping(x - 1, y, height, 0x2000000);
                }
            }
        }
    }

    /**
     * Method addClippingForSolidObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param height
     * @param xLength
     * @param yLength
     * @param flag
     * @param flag2
     */
    public static void addClippingForSolidObject(int x, int y, int height, int xLength, int yLength, boolean flag,
            boolean flag2) {
        int clipping = 256;

        if (flag) {

            clipping += 0x20000;
        }

        if (flag2) {
            clipping |= 0x40000000;
        }

        for (int i = x; i < x + xLength; i++) {
            for (int i2 = y; i2 < y + yLength; i2++) {
                addClipping(i, i2, height, clipping);
            }
        }
    }

    /**
     * Method removeClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param height
     * @param xLength
     * @param yLength
     */
    public static void removeClipping(int x, int y, int height, int xLength, int yLength) {
        for (int i = x; i < x + xLength; i++) {
            for (int i2 = y; i2 < y + yLength; i2++) {
                removeClipping(i, i2, height);
            }
        }
    }

    /**
     * Method removeClippingForSolidObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param height
     * @param xLength
     * @param yLength
     * @param flag
     * @param flag2
     */
    public static void removeClippingForSolidObject(int x, int y, int height, int xLength, int yLength, boolean flag,
            boolean flag2) {
        int clipping = 256;

        if (flag) {
            clipping |= 0x20000;
        }

        if (flag2) {
            clipping |= 0x40000000;
        }

        for (int i = x; i < x + xLength; i++) {
            for (int i2 = y; i2 < y + yLength; i2++) {
                removeClipping(i, i2, height, clipping);
            }
        }
    }
}
