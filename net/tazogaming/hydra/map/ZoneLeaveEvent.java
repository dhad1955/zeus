package net.tazogaming.hydra.map;

import net.tazogaming.hydra.entity3d.Entity;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/11/2014
 * Time: 00:20
 */
public interface ZoneLeaveEvent<T extends Entity> {
    public void zoneLeft(T entity);
}
