package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;

/**
 * Immutable class representing a Point on the game.
 */
public final class Point {
    private static final PointCache pointCache;

    static {
        pointCache = new PointCache();
    }

    private byte    height = 0;
    private short[] absolute;
    private int[]   mapRegion;
    private byte[]  local;


    public static boolean equals(Point p, Point i){
        return p.getX() == i.getX() && p.getY() == i.getY() && p.getHeight() == i.getHeight();
    }
    /**
     * Constructs ...
     *
     *
     * @param x
     * @param y
     * @param height
     */
    private Point(int x, int y, int height) {
        this.absolute  = new short[] { (short) x, (short) y };
        this.mapRegion = new int[] { (short) ((absolute[0] >> 3)) - 6, (absolute[1] >> 3) - 6 };


        if(this.mapRegion[0] < 0 || this.mapRegion[1] < 0)
            throw new RuntimeException("Error invalid location: "+x+" "+y);
        int localX = absolute[0] - 8 * mapRegion[0];
        int localY = absolute[1] - 8 * mapRegion[1];

        local       = new byte[] { (byte) localX, (byte) localY };
        this.height = (byte) height;
    }

    /**
     * Method getLocal
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param abs
     * @param regionX
     *
     * @return
     */
    public static final int getLocal(int abs, int regionX) {
        return abs - 8 * regionX;
    }

    /**
     * @param x the absolute x coordinate
     * @param y the absolute y coordinate
     * @param height
     * @return a new Point object for the specified coordinates.
     */
    public static Point location(int x, int y, int height) {
        if ((x < 0) || (y < 0)) {
            throw new IllegalArgumentException("Point may not contain non negative values x:" + x + " y:" + y);
        }

        return new Point(x, y, height);
    }

    /**
     * Method getRegion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Region getRegion() {
        return World.getWorld().getRegionManager().getRegion(getRegionX(), getRegionY());
    }

    /**
     * Method getDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plaX
     * @param plaY
     * @param cliX
     * @param cliY
     *
     * @return
     */
    public static int getDistance(int plaX, int plaY, int cliX, int cliY) {
        int delX = plaX - cliX;
        int delY = plaY - cliY;

        return (int) Math.pow(delX * delX + delY * delY, 0.5);
    }

    /**
     * Method get30BitsHash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int get30BitsHash() {
        return getY() | ((getHeight() << 28) | (getX() << 14));
    }

    private int knownRegionX() {
        return getX() >> 3;
    }

    private int knownRegionY() {
        return getY() >> 3;
    }

    /**
     * Method get18BitsHash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int get18BitsHash() {
        int thisregionId = ((knownRegionX() / 8) << 8) + (knownRegionY() / 8);

        return (((thisregionId & 0xff) * 64) >> 6) | (getHeight() << 16) | ((((thisregionId >> 8) * 64) >> 6) << 8);
    }

    /**
     * Method transform
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public Point transform(int x, int y) {
        return Point.location(getX() + x, getY() + y, getHeight());
    }

    /**
     * Method getDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     *
     * @return
     */
    public static int getDistance(Point from, Point to) {
        int plaX = from.getX();
        int plaY = from.getY();
        int cliX = to.getX();
        int cliY = to.getY();
        int delX = plaX - cliX;
        int delY = plaY - cliY;

        return (int) Math.pow(delX * delX + delY * delY, 0.5);
    }

    /**
     * Method getLocalX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param p
     *
     * @return
     */
    public static int getLocalX(Player player, Point p) {
        int plrX   = player.getKnownLocalX();
        int deltaX = 0;

        deltaX = p.getX() - player.getX();

        return plrX + deltaX;
    }

    /**
     * Method getLocalY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param p
     *
     * @return
     */
    public static int getLocalY(Player player, Point p) {
        int plrY   = player.getKnownLocalY();
        int deltaY = 0;    // deltaY;

        deltaY = p.getY() - player.getY();

        return plrY + deltaY;
    }

    /**
     * Method getLocalX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public int getLocalX(Player player) {
        int plrX   = player.getKnownLocalX();
        int deltaX = 0;

        deltaX = getX() - player.getX();

        return plrX + deltaX;
    }

    /**
     * Method getLocalY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public int getLocalY(Player player) {
        int plrY   = player.getKnownLocalY();
        int deltaY = 0;    // deltaY;

        deltaY = getY() - player.getY();

        return plrY + deltaY;
    }

    /**
     * @return the absolute x coordinate of this point.
     */
    public final int getX() {
        return absolute[0];
    }

    /**
     * @return the absolute y coordinate of this point.
     */
    public final int getY() {
        return absolute[1];
    }

    /**
     * @return the x region coordinate of this point
     */
    public final int getRegionX() {
        return mapRegion[0];
    }

    /**
     * @return the y region coordinate of this point
     */
    public final int getRegionY() {
        return mapRegion[1];
    }

    /**
     * @return the local y coordinate of this point
     */
    public final int getLocalX() {
        return local[0];
    }

    /**
     * @return the local x coordinate of this point
     */
    public final int getLocalY() {
        return local[1];
    }

    /**
     * Method getHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHeight() {
        return height;
    }

    /**
     * Method equals
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param o
     *
     * @return
     */
    @Override
    public final boolean equals(Object o) {
        if (o instanceof Point) {
            return java.util.Arrays.equals(((Point) o).absolute, this.absolute);
        }

        return false;
    }

    /**
     * Method hashCode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public int hashCode() {
        return hashCode(absolute[0], absolute[1]);
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String toString() {
        return "x: " + absolute[0] + ", y: " + absolute[1];
    }

    /**
     * Returns a unique hashcode for a Point (assumes a point can only be represented
     * by 16 bits).
     *
     * @param x
     * @param y
     *
     * @return
     */
    public static int hashCode(int x, int y) {
        return x << 16 | y;
    }

    /**
     * Method abs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param localX
     * @param localY
     * @param mapRegionX
     * @param mapRegionY
     * @param h
     *
     * @return
     */
    public static Point abs(int localX, int localY, int mapRegionX, int mapRegionY, int h) {
        int posX = localX + 8 * mapRegionX;
        int posY = localY + 8 * mapRegionY;

        return Point.location(posX, posY, h);
    }

    /**
     * Useful little number saves on having to recreate new points for everything.
     * Saves a bit of memory for the most part, and object creation times.
     * Not sure if it's really necessary. Would be worth bencharking it.
     */
    static class PointCache {

        /**
         * Stores each point we have created, with the key as it's hashcode.
         */
        private Map<Integer, Point> points = new HashMap<Integer, Point>();

        /**
         * Method getPoint
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @param code
         *
         * @return
         */
        public Point getPoint(int code) {
            return points.get(code);
        }
    }
}
