package net.tazogaming.hydra.map;

/**
 * Rune War Official Source
 * Created for http://hydra.tazogaming.org}
 *
 * @author MDMA
 *         Date: 22-Feb-2010 {15:45:11}
 */
public class RegionCoordinates {

    /**
     * X coordinate.
     */
    private int x;

    /**
     * Y coordinate.
     */
    private int y;

    /**
     * Creates the region coordinate.
     * @param x The x coordinate.
     * @param y The y coordinate.
     */
    public RegionCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets the x coordinate.
     * @return The x coordinate.
     */
    public int getX() {
        return x;
    }

    /**
     * Gets the y coordinate.
     * @return The y coordinate.
     */
    public int getY() {
        return y;
    }

    /**
     * Method hashCode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public int hashCode() {
        final int prime  = 31;
        int       result = 1;

        result = prime * result + x;
        result = prime * result + y;

        return result;
    }

    /**
     * Method equals
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj
     *
     * @return
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        RegionCoordinates other = (RegionCoordinates) obj;

        if (x != other.x) {
            return false;
        }

        if (y != other.y) {
            return false;
        }

        return true;
    }
}
