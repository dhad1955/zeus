package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class Tile {
    private List<Player>     players                 = null;
    private List<FloorItem>  items                   = null;
    private List<GameObject> objects                 = null;
    private List<NPC>        npcs                    = null;
    private List<NPC>        npcChunk                = null;
    private int              mappedObjectId          = -1;
    private byte             mappedObjectType        = -1;
    private byte             getMappedObjectRotation = -1;
    private short            farmingPatchID          = -1;
    private int              clippingData            = 0;
    private boolean          projectileClipped       = true;
    private Point            loc;
    private Trigger          walk_trigger;
    private Door             door;

    /**
     * Constructs ...
     *
     *
     * @param p
     */
    public Tile(Point p) {
        this.loc = p;
    }

    /**
     * Constructs ...
     *
     *
     * @param x
     * @param y
     * @param height
     */
    public Tile(int x, int y, int height) {
        this(Point.location(x, y, height));
    }

    /**
     * Method addObjectTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param trigger
     */
    public void addObjectTrigger(Trigger trigger) {}

    /**
     * Method isHasFarmingPatch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isHasFarmingPatch() {
        return farmingPatchID != -1;
    }

    /**
     * Method setHasFarmingPatch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setHasFarmingPatch(int id) {
        this.farmingPatchID = (short) id;
    }

    /**
     * Method getPatchID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPatchID() {
        return farmingPatchID;
    }

    /**
     * Method setWalkTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void setWalkTrigger(Trigger t) {
        this.walk_trigger = t;
    }

    /**
     * Method getWalkTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Trigger getWalkTrigger() {
        return walk_trigger;
    }

    /**
     * Method setProjectileClipped
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setProjectileClipped(boolean b) {
        projectileClipped = b;
    }

    /**
     * Method isProjectileClipped
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isProjectileClipped() {
        return projectileClipped;
    }

    /**
     * Method hasMappedObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasMappedObject() {
        return mappedObjectId != -1;
    }

    /**
     * Method getDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Door getDoor() {
        return door;
    }

    /**
     * Method setDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     */
    public void setDoor(Door d) {
        this.door = d;
    }

    /**
     * Method getMappedRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMappedRotation() {
        return getMappedObjectRotation;
    }

    /**
     * Method getMappedType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMappedType() {
        return mappedObjectType;
    }

    /**
     * Method mapObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param type
     * @param rotation
     */
    public void mapObject(int id, int type, int rotation) {
        this.mappedObjectId          = id;
        this.mappedObjectType        = (byte) type;
        this.getMappedObjectRotation = (byte) rotation;
    }

    /**
     * Method getMappedObjectId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMappedObjectId() {
        return mappedObjectId;
    }

    /**
     * Method getClippingData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getClippingData() {
        return clippingData;
    }

    /**
     * Method setClippingData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param clippingData
     *
     * @return
     */
    public int setClippingData(int clippingData) {
        this.clippingData = clippingData;

        return clippingData;
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     */
    public void add(Entity entity) {
        if (entity instanceof Door) {
            door = (Door) entity;

            return;
        }

        if (entity instanceof Player) {
            if (players == null) {
                players = new ArrayList<Player>();
            }

            players.add((Player) entity);
        } else if (entity instanceof FloorItem) {
            if (items == null) {
                items = new ArrayList<FloorItem>();
            }

            items.add((FloorItem) entity);
        } else if (entity instanceof GameObject) {
            if (objects == null) {
                objects = new ArrayList<GameObject>();
            }

            objects.add((GameObject) entity);
        } else if (entity instanceof NPC) {
            if (npcs == null) {
                npcs = new ArrayList<NPC>();
            }

            npcs.add((NPC) entity);
        }
    }

    /**
     * Method addNpcChunk
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void addNpcChunk(NPC npc) {
        if (this.npcChunk == null) {
            this.npcChunk = new ArrayList<NPC>();
        }

        this.npcChunk.add(npc);
    }

    /**
     * Method removeNPCCHunk
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param NPC
     */
    public void removeNPCCHunk(NPC NPC) {
        if (npcChunk == null) {
            return;
        }


        this.npcChunk.remove(NPC);
    }

    public List<NPC> getNpcChunk() {
        return this.npcChunk;
    }

    /**
     * Method containsNPCChunks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean containsNPCChunks() {
        return (npcChunk != null) && (this.npcChunk.size() > 0);
    }

    /**
     * Method blocked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public boolean blocked(NPC npc) {
        if (!containsNPCChunks()) {
            return false;
        }

        if (npc == null) {
            return true;
        }

        for (NPC npz : npcChunk) {
            if ((npz.getCurrentInstance() == npc.getCurrentInstance()) &&!npz.isSummoned()) {
                if (npz != npc) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method hasChunk
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public boolean hasChunk(NPC npc) {
        return npcChunk.contains(npc);
    }

    /**
     * Method getObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<GameObject> getObjects() {
        return this.objects;
    }

    /**
     * Method hasObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasObjects() {
        return (objects != null) && (objects.size() > 0);
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<FloorItem> getItems() {
        return items;
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     */
    public void remove(Entity entity) {
        if (entity instanceof Door) {
            door = null;

            return;
        }

        if (entity instanceof Player && players != null) {
            getPlayers().remove(entity);
            if(getPlayers().size() == 0)
                players = null;
        } else if (entity instanceof FloorItem && items != null) {
            items.remove(entity);
            if(items.size() == 0)
                items = null;

        } else if (entity instanceof GameObject && objects != null) {
            objects.remove(entity);
            if(objects.size() == 0)
                objects = null;
        } else if (entity instanceof NPC && npcs != null) {
            npcs.remove(entity);
            if(npcs.size() == 0)
                npcs = null;
        }
    }

    /**
     * Method hasPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasPlayers() {
        return (players != null) && (players.size() > 0);
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Player> getPlayers() {
        if (players == null) {
            players = new ArrayList<Player>();
        }

        return players;
    }

    /**
     * Method getX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getX() {
        return loc.getX();
    }

    /**
     * Method getY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getY() {
        return loc.getY();
    }

    /**
     * @returns true if the tile can be cleaned up.
     *
     * @return
     */
    public boolean isEmpty() {
        return false;
    }

    /**
     * Method getLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getLocation() {
        return loc;
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String toString() {
        return "[loc=" + loc.toString() + ",players=" + hasPlayers() + "]";
    }
}
