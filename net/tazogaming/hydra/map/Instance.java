package net.tazogaming.hydra.map;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.GameObject;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
public class Instance {
    public static final Instance  GLOBAL_INSTANCE = new Instance();
    private ArrayList<Player>     players         = new ArrayList<Player>();
    private ArrayList<GameObject> objects         = new ArrayList<GameObject>();
    private ArrayList<NPC>        npcs            = new ArrayList<NPC>();
    private ArrayList<FloorItem>  items           = new ArrayList<FloorItem>();




    /**
     * Method unlink_all
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unlink_all() {
        for (NPC npc : npcs) {
            npc.unlink();
        }

        for (GameObject obj : objects) {
            obj.remove();
        }

        try {
            for (FloorItem i : items) {
                i.remove();
            }
        } catch (Exception ee) {}
    }

    /**
     * Method remove_for_player
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void remove_for_player(Player player) {
        for (GameObject obj : objects) {
            player.getActionSender().deleteWorldObject(obj);
        }

        for (FloorItem i : items) {
            player.getActionSender().removeGroundItem(i.getItemId(), i.getX(), i.getY());
        }
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void add(Entity e) {
        if (e instanceof Player) {
            players.add((Player) e);
        } else if (e instanceof NPC) {
            npcs.add((NPC) e);
        } else if (e instanceof GameObject) {
            objects.add((GameObject) e);
        } else if (e instanceof FloorItem) {
            items.add((FloorItem) e);
        }
    }

    /**
     * Method remove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void remove(Entity e) {
        if (e instanceof Player) {
            players.remove((Player) e);
        } else if (e instanceof NPC) {
            npcs.remove((NPC) e);
        } else if (e instanceof GameObject) {
            objects.remove((GameObject) e);
        } else if (e instanceof FloorItem) {
            items.remove((FloorItem) e);
        }
    }
}
