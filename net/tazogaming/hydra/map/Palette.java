package net.tazogaming.hydra.map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/10/13
 * Time: 08:38
 */
public class Palette {

    /**
     * Normal direction.
     */
    public static final int DIRECTION_NORMAL = 0;

    /**
     * Rotation direction clockwise by 0 degrees.
     */
    public static final int DIRECTION_CW_0 = 0;

    /**
     * Rotation direction clockwise by 90 degrees.
     */
    public static final int DIRECTION_CW_90 = 1;

    /**
     * Rotation direction clockwise by 180 degrees.
     */
    public static final int DIRECTION_CW_180 = 2;

    /**
     * Rotation direction clockwise by 270 degrees.
     */
    public static final int DIRECTION_CW_270 = 3;

    /**
     * The array of tiles.
     */
    private PaletteTile[][][] tiles = new PaletteTile[13][13][4];

    /**
     * Gets a tile.
     * @param x X position.
     * @param y Y position.
     * @param z Z position.
     * @return The tile.
     */
    public PaletteTile getTile(int x, int y, int z) {
        return tiles[x][y][z];
    }

    /**
     * Sets a tile.
     * @param x X position.
     * @param y Y position.
     * @param z Z position.
     * @param tile The tile.
     */
    public void setTile(int x, int y, int z, PaletteTile tile) {
        tiles[x][y][z] = tile;
    }

    /**
     * Method fill
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void fill(PaletteTile t) {
        for (int i = 0; i < tiles.length; i++) {
            for (int k = 0; k < tiles[i].length; k++) {
                for (int c = 0; c < tiles[i][k].length; c++) {
                    tiles[i][k][c] = t;
                }
            }
        }
    }

    /**
     * Represents a tile to copy in the palette.
     * @author Graham Edgecombe
     *
     */
    public static class PaletteTile {
        public static final int
            LIGHT_GREY               = 1,
            WHITE                    = 9,
            WOODEN_FLOORING          = 5,
            POSH_RED                 = 13,
            DINGY_BROWN              = 14,
            MARBLE_WORKS             = 20,
            SLATE_GREY               = 33,
            MYSTICAL_MOVING_FLOORING = 39,
            RICH_RED_CARPET          = 118,
            ASH_FLOORING             = 120,
            MADISON                  = 119;
        private int color            = 13;

        /**
         * X coordinate.
         */
        private int x;

        /**
         * Y coordinate.
         */
        private int y;

        /**
         * Z coordinate.
         */
        private int z;

        /**
         * Rotation.
         */
        private int rot;

        /**
         * Creates a tile.
         * @param x The x coordinate.
         * @param y The y coordinate.
         */
        public PaletteTile(int x, int y) {
            this(x, y, 0);
        }

        /**
         * Creates a tile.
         * @param x The x coordinate.
         * @param y The y coordinate.
         * @param z The z coordinate.
         */
        public PaletteTile(int x, int y, int z) {
            this(x, y, z, DIRECTION_NORMAL, 0);
        }

        /**
         * Creates a tile.
         * @param x The x coordinate.
         * @param y The y coordinate.
         * @param z The z coordinate.
         * @param rot The rotation.
         * @param color
         */
        public PaletteTile(int x, int y, int z, int rot, int color) {
            this.x     = x;
            this.y     = y;
            this.z     = z;
            this.rot   = rot;
            this.color = color;
        }

        /**
         * Method getColor
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getColor() {
            return color;
        }

        /**
         * Gets the x coordinate.
         * @return The x coordinate.
         */
        public int getX() {
            return x / 8;
        }

        /**
         * Gets the y coordinate.
         * @return The y coordinate.
         */
        public int getY() {
            return y / 8;
        }

        /**
         * Gets the z coordinate.
         * @return The z coordinate.
         */
        public int getZ() {
            return z % 4;
        }

        /**
         * Gets the rotation.
         * @return The rotation.
         */
        public int getRotation() {
            return rot % 4;
        }
    }
}
