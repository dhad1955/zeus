package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.Server;
import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.net.CreditServer;
import net.tazogaming.hydra.net.server.PushService;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 11:15
 */
public class ServiceLoader extends BootTask {

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        try {
            log("Loading payment server");
          if(!Main.TEST_MODE)
            new CreditServer();

            Server s = new Server();

            if (Config.getServerInfo() != null) {
                log("Loading push service");
                new PushService();
            }

            log("Trying to bind to port: " + Config.getMainInfo().get("service_port"));

            int port = Integer.parseInt(Config.getMainInfo().getProperty("service_port"));

            s.start(port);
            log("Server started successfully.");
        } catch (Exception ee) {
            ee.printStackTrace();
            System.exit(-1);
        }
    }
}
