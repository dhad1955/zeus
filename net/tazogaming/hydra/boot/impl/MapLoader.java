package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.MapFetcher;
import net.tazogaming.hydra.map.MapXTEA;
import net.tazogaming.hydra.util.Config;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 09:41
 */
public class MapLoader extends BootTask {

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        log("Loading Map XTEA keys");
        MapXTEA.load();

        long start = System.currentTimeMillis();

        if (Config.getMainInfo().get("load_maps").toString().equals("true")) {
            log("Loading maps");
            World.getWorld().setMapAssistant(new MapFetcher());

            long end = System.currentTimeMillis() - start;

            log("Maps loaded in " + end + " ms");
        } else {
            log("Map loading skipped");
        }

        log("Loading custom doors");
        Door.modDoor(2657, 2585, 3, 2);
        Door.modDoor(2643, 2592, 0, 3);
        Door.modDoor(2670, 2593, 2, 1);

        Door.modDoor(2373, 3119, 3, 2);

        Door.modDoor(2426, 3088, 1, 0);

        log("Loading areas");
        Zone.loadAreas();
        log("Loading custom objects");
        World.getWorld().getObjectManager().load_custom_objects(new File("config/world/object_placements.cfg"));
        System.gc();
    }
}
