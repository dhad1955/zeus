package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.entity3d.npc.ai.script.smartnpc.NPCMovementScript;
import net.tazogaming.hydra.script.runtime.ScriptParseException;
import net.tazogaming.hydra.script.build.SourcePosition;
import net.tazogaming.hydra.script.build.ScriptCompiler;
import net.tazogaming.hydra.script.build.ScriptCompilerOutput;
import net.tazogaming.hydra.script.runtime.instr.op.impl.RuntimeOperationMap;
import net.tazogaming.hydra.util.Logger;

/**
 * Copyright (C) Tazogaming ltd Official website: http://www.tazogaming.net Zeus
 * Runescape 2 Emulator Zeus is a Runescape 2 Server emulator which has been
 * designed for educational purposes only Created by Daniel Hadland Date:
 * 12/07/14 Time: 11:09
 */
public class ScriptLoader extends BootTask {

	/**
	 * Method run Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	@Override
	public void run() {
		RuntimeOperationMap.load(this);
		log("Compiling npc scripts");
		NPCScriptEngine.loadScripts("config/world/npc/combatscript", null);
		log("Compiling smartnpc scripts");
		NPCMovementScript.unpackFiles("config/world/npc/smartnpc/");
		log("Compiling player scripts");
		ScriptCompiler.compile(new ScriptCompilerOutput() {
			@Override
			public void exceptionCaught(Exception exception) {
				if (exception != null) {
					Logger.err(exception.getMessage());

					if (exception instanceof ScriptParseException) {
						ScriptParseException parseError = (ScriptParseException) exception;

						if (parseError.position() != null) {
							Logger.err("file: " + parseError.position().getFileName() + " at line "
									+ parseError.position().getRow());
						}
					}
				}
			};

			@Override
			public void messageSent(SourcePosition position, String message) {
				log(message);
			}
		}, "config/scripts");
	}
}
