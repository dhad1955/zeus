package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.entity3d.Ammunition;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.WeaponLoader;
import net.tazogaming.hydra.game.ui.PlayerGrandExchange;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchangeItemSetsInterface;
import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 09:37
 */
public class ItemLoader extends BootTask {

    /**
     * Method run
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        try {
            log("Loading interfaces definitions, bonuses and requirements.");
            Item.loadItemList(false, null);
            WeaponLoader.unpackConfig("config/item/weapons.conf", null);
            log("Loading ammunition data for range skill");
            Ammunition.initialize();

            GrandExchangeProcessor.getSingleton().loadAutoBuys();
            PlayerGrandExchange.loadAutoSells();
            GrandExchangeItemSetsInterface.Sets.loadPrices();

         /*   TextFile output = new TextFile();
            TextFile file = TextFile.fromPath("config/item/742items.txt");
            for(Line l : file){
                String line = l.getData();
                if(line.startsWith("item")){
                    String[] split = line.split("\t");
                    String[] split2 = split[0].split(" ");
                    String absMelee = split[19];
                    String absMage = split[20];
                    String absRange = split[21];
                    int itemID = Integer.parseInt(split2[2]);
                    Item item = Item.forId(itemID);
                   boolean render = false;
                    for(int i = 0; i < 12; i++){
                        if(item.getBonus(i) > 0){
                            render = true;
                            break;
                        }
                    }
                    if(render){
                        System.err.println("push: "+item.getName());
                        output.push("// "+item.getName());
                        output.push("bonus "+item.getIndex()+" { ");
                        for(int i = 0; i < 12; i++){
                            output.push(Item.BONUS_NAMES[i]+" = "+item.getBonus(i));
                        }
                        output.push("SoakMelee = "+absMelee);
                        output.push("SoakRange = "+absRange);
                        output.push("SoakMage = "+absMage);
                        output.push("}");
                        output.push("");

                    }
                }
                output.save("config/item/newbonus.cfg");
                  */

        } catch (IOException ER) {
            ER.printStackTrace();
        }
    }
}
