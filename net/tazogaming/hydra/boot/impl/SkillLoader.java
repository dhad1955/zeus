package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.game.skill.combat.potion.Potion;
import net.tazogaming.hydra.game.skill.construction.RoomNode;
import net.tazogaming.hydra.game.skill.construction.costumeroom.CostumeRuleTable;
import net.tazogaming.hydra.game.skill.entity.CookingEntity;
import net.tazogaming.hydra.game.skill.farming2.Growable;
import net.tazogaming.hydra.game.skill.farming2.PatchDefinition;
import net.tazogaming.hydra.game.skill.farming2.SubPatches;
import net.tazogaming.hydra.game.skill.fishing.FishPool;
import net.tazogaming.hydra.game.skill.slayer.Slayer;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 09:26
 */
public class SkillLoader extends BootTask {

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        log("Loading skills");
        log("Loading skill: Farming");

        try {
            SubPatches.loadMultiPatches("config/iskill/farming/subpatches.cfg");
            PatchDefinition.loadPatchDefinitions("config/iskill/farming/patchlocations.cfg", this);
            Growable.load("config/iskill/farming/growable.cfg");
        } catch (Exception ee) {
            ee.printStackTrace();
            Logger.debug("Farming failed");
            System.exit(-1);
        }

        log("Loading skill: herblore");
        Potion.init();
        log("Loading skill: slayer");
        Slayer.load_list(null);
        Slayer.load_masters(null);
        log("Loading skill: cooking");

        try {
            CookingEntity.load("config/iskill/cooking/cooking_ids.cfg");
        } catch (Exception ee) {
            log("Failed to load cooking: " + ee.getMessage());
        }

        log("Loading skill: summoning");
        Summoning.load();
        log("Loading skill: fishing");
        FishPool.load_fish(new File("config/iskill/fishing/pool_data.conf"));
        log("Loading skill: construction");
        RoomNode.load_room_config();
        CostumeRuleTable.loadList(new File("config/iskill/construction/costume_room_config.conf"));
    }
}
