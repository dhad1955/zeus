package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.net.Server;
import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.io.js5.Cache;
import net.tazogaming.hydra.net.PacketDefinition;
import net.tazogaming.hydra.net.packethandler.*;
import net.tazogaming.hydra.net.packethandler.npc.AttackNPCHandler;
import net.tazogaming.hydra.net.packethandler.npc.MageNPC;
import net.tazogaming.hydra.net.packethandler.npc.NpcAction;
import net.tazogaming.hydra.net.packethandler.player.*;
import net.tazogaming.hydra.net.packethandler.rsi.*;
import net.tazogaming.hydra.runtime.GameEngine;
import net.tazogaming.hydra.script.runtime.HangMonitor;
import net.tazogaming.hydra.security.BanList;
import net.tazogaming.hydra.security.IPFlags;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 09:45
 */
public class CoreLoader extends BootTask {
    private ArrayList<PacketDefinition> packetList = new ArrayList<PacketDefinition>();

    /**
     * Method addPacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param ids
     */
    public void addPacket(Class name, int... ids) {
        packetList.add(new PacketDefinition(name, ids));
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        try {
            log("Loading boot config");
            Config.getMainInfo();
            log("Loading jagex file system");
            Cache.init();
            log("Preparing world");
            World.loadWorld();
            log("Loading game engine");

            GameEngine engine = new GameEngine();

            GrandExchangeProcessor.initialize();
            Server.newThread(engine, "GameEngine", Thread.MAX_PRIORITY);
            World.getWorld().setGameEngine(engine);
            log("Getting packet list");
            populatePackets();
            log("Total packets: " + packetList.size());

            for (PacketDefinition packet : packetList) {
                World.getWorld().getGameEngine().registerPacketHandler(packet.getPacket(), packet.getBinds());
            }

            log("Loading ban list");
            BanList.loadBans();
            log("Loading ip flags");
            IPFlags.loadFlags();

            World.getWorld().getBackgroundService().submitRequest(new HangMonitor());
        } catch (Exception ee) {
            ee.printStackTrace();
            Logger.err("Core loading failed, shutting down.");
            System.exit(-1);
        }
    }

    private void populatePackets() {
        addPacket(Walking.class, 35, 50);
        addPacket(PublicChat.class, 16);
        addPacket(InterfaceAction.class, 6, 13, 0, 15, 46, 67, 82, 39, 73, 58);
        addPacket(PickupItem.class, 54);
        addPacket(MoveItems.class, 10);
        addPacket(DialogActionHandler.class, 4);
        addPacket(AttackNPCHandler.class, 18);
        addPacket(NpcAction.class, 28, 33, 31, 72, 5);
        addPacket(PaneSwitchHandler.class, 7);
        addPacket(ItemOnItem.class, 3);
        addPacket(AmountEntered.class, 34);
        addPacket(ObjectClick.class, 76, 55, 48, 25, 81);
        addPacket(ItemAtObject.class, 11);
        addPacket(PrivacySettingsChange.class, 44);
        addPacket(AttackPlayer.class, 70);
        addPacket(FollowPlayerPacketHandler.class, 80);
        addPacket(TradeRequest.class, 27);
        addPacket(InterfaceClosed.class, 32);
        addPacket(TradeAnswer.class, 47);
        addPacket(FriendsAction.class, 2, 77, 74, 20);
        addPacket(PMPlayer.class, 41);
        addPacket(MageNPC.class, 14);
        addPacket(TextEntered.class, 37, 63);
        addPacket(MagePlayer.class, 78);
        addPacket(ClanPacketHandler.class, 1,51,52,36);
        addPacket(GrandExchangeItemSelect.class, 19);
        addPacket(DevCommandPacketHandler.class, 79);
    }
}
