package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NpcSpawnHandler;
import net.tazogaming.hydra.game.Killcounts;
import net.tazogaming.hydra.game.minigame.SkeletalHorror;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.game.minigame.pc.PCGame;
import net.tazogaming.hydra.game.minigame.pkleague.PKTournament;
import net.tazogaming.hydra.game.minigame.pkraffle.PKRaffle;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.game.EventManager;
import net.tazogaming.hydra.game.pets.BossPet;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.questing.Quest;
import net.tazogaming.hydra.game.ui.ge.GEUnsellables;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.net.Server;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.HelpMenuParser;
import net.tazogaming.hydra.game.ui.SkillMenuParser;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.security.GamblingBanList;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.game.LootTable;
import net.tazogaming.hydra.game.leaderboard.BoardBackgroundTask;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.IOException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 11:13
 */
public class ContentLoader extends BootTask {

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        if (Config.getMainInfo().get("load_loot").toString().equalsIgnoreCase("true")) {
            log("Loading loot tables");
            LootTable.load();
        } else {
            log("Loot loading skipped");
        }

        log("Loading shops");
        ShopManager.load();
        log("Loading clan channels");
        World.getWorld().getChannelList().loadChannels();
        log("Loading PK Raffle");
        PKRaffle.loadRaffle();
        log("Loading clue scrolls");
        ClueManager.load_clues("config/world/minigame/clues.cfg");
        log("Loading skill menus");
        SkillMenuParser.loadMenus();
        log("Loading misc menus");
        HelpMenuParser.parse(new File("config/ui/info_menu"));
        log("Loading npc positions");
        NpcSpawnHandler.load("config/world/npc/npcpositions.cfg");
        log("Mapping npcs");
        NpcSpawnHandler.loadToWorld();
        PCGame.staticV();
        Core.submitTask(EventManager.instance);
        World.getWorld().getScriptManager().runBootTriggers();

        Killcounts.load();
        Achievement.loadAchievements("config/achievements.cfg");

       try {
           Quest.loadQuests(null);
       }catch (Exception ioe){
           ioe.printStackTrace();
       }

        try {
            BossPets.load();
            Core.submitTask(PKTournament.puresLeaderboard);
            Core.submitTask(BossTournament.mainsLeaderboard);
            Core.submitTask(PKTournament.zerkersLeaderboard);
            Core.submitTask(PKTournament.mainsLeaderboard);
            GamblingBanList.save();

            GEUnsellables.load();
            BossTournament.mainsLeaderboard.loadBosses();;
            BossTournament.mainsLeaderboard.loadRewards();


            BossTournament.mainsLeaderboard.load(new Buffer(new File("config/boss_lboard.dat")));
            PKTournament.puresLeaderboard.load(new Buffer(new File("config/dailypk.dat")));
            PKTournament.mainsLeaderboard.load(new Buffer(new File("config/mains_lboard.dat")));
            PKTournament.zerkersLeaderboard.load(new Buffer(new File("config/zerkers_lboard.dat")));



        }catch (Exception ignored) {
            ignored.printStackTrace();
        }

        // leaderboards
       if(!Main.TEST_MODE)
        World.getWorld().getBackgroundService().submitRequest(new BoardBackgroundTask());
        if(!Config.getMainInfo().getProperty("load_ge").equalsIgnoreCase("false"))
            Server.newThread(GrandExchangeProcessor.getSingleton(), "Grand Exchange", Thread.MIN_PRIORITY);
        SkeletalHorror.init();

    }
}
