package net.tazogaming.hydra.boot.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.util.Config;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 11:05
 */
public class NpcLoader extends BootTask {

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        if (Config.getMainInfo().getProperty("load_npcs").toString().equalsIgnoreCase("true")) {
            log("Loading npcs definitions..");

            long start = System.currentTimeMillis();

            NpcDef.unpackConfig(World.getWorld().getDatabaseLink(), null);

            long end = System.currentTimeMillis();

            log("Npc loading complete, loaded: " + NpcDef.totalNpcs + " definitions");
        } else {
            log("Npc Loading skipped");
        }
    }
}
