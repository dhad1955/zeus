package net.tazogaming.hydra.boot;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 11:17
 */
public class BootStrapList {
    private Queue<BootTask> taskList = new LinkedBlockingQueue<BootTask>();
    int                     caret    = 0;

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param info
     */
    public void add(Class info) {
        try {
            BootTask task = (BootTask) info.newInstance();

            task.setExecutor(this);
            taskList.add(task);
        } catch (Exception ee) {
            ee.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Method percent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int percent() {
        return (int)GameMath.getPercentFromTotal(caret, taskList.size());
    }

    /**
     * Method boot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void boot() {
        for (BootTask task : taskList) {
            caret++;
            task.run();
        }
    }
}
