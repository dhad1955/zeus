package net.tazogaming.hydra.game.gametick;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.pets.BossPet;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.ai.hardscript.NexCombatScript;
import net.tazogaming.hydra.game.skill.summoning.SummonEntity;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Region;
import net.tazogaming.hydra.game.minigame.agility.AgilityArena;
import net.tazogaming.hydra.game.minigame.barrows.BarrowsGame;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRules;
import net.tazogaming.hydra.game.minigame.deathtower.DeathTowerGame;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.minigame.pc.PCGame;
import net.tazogaming.hydra.game.minigame.util.ilobby.Lobby;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 16:36
 */
public class ZoneUpdateTick extends PlayerTickEvent {
    public static final int[] WILDERNESS_BANNED_ITEMS = {
        19904, 20003, 16877, 11165, 16273, 16275, 16277, 16279, 16281, 16283, 16285, 16287, 16290, 16293, 16339, 16341,
        16343, 16345, 16347, 16349, 16351, 16353, 16355, 16357, 16359, 18747,14636
    };

    /**
     * Constructs ...
     *
     *
     * @param pla
     */
    public ZoneUpdateTick(Player pla) {
        super(pla, 1, false, true);
    }

    private static final boolean bannedWild(int bannd) {
        for (int i = 0; i < WILDERNESS_BANNED_ITEMS.length; i++) {
            if (WILDERNESS_BANNED_ITEMS[i] == bannd) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method doTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param owner
     */
    @Override
    public void doTick(Player owner) {

        if(owner.isInWilderness() && owner.getAccount().getSmallSetting(5) != Combat.getWildernessLevel(owner)) {
            owner.getAccount().putSmall(5, Combat.getWildernessLevel(owner));
            owner.getGameFrame().sendString("Level: "+ Combat.getWildernessLevel(owner), 381, 2);
            owner.getGameFrame().sendString("Level: "+ Combat.getWildernessLevel(owner), 381, 5);


        } else if (!owner.isInWilderness()) {
        	owner.getAccount().putSmall(5, 0);
        }
        for (int i = 0; i < getPlayer().getAreas().size(); i++) {
            Zone e = getPlayer().getAreas().get(i);

            if (!e.isInArea(owner.getLocation())) {
                getPlayer().getAreas().remove(i);
                areaLeft(owner, e);
            }
        }

        Region r = getPlayer().getLocation().getRegion();

        List<Zone> toAdd = null;

        if ((r != null) && r.hasAreas()) {
            for (Zone e : r.getAreas()) {
                if (!getPlayer().getAreas().contains(e) && e.isInArea(getPlayer().getLocation())) {
                    areaEntered(getPlayer(), e);
                    toAdd = new ArrayList<Zone>();
                    toAdd.add(e);
                }
            }
        }
        if(toAdd != null) {
            getPlayer().getAreas().addAll(toAdd);
        }

        if (owner.isInZone("snow")) {
            if ((Core.currentTime % 20) == 0) {
                for (int i = 0; i < 24; i++) {
                    getPlayer().drainStat(i, 1);
                }
            }
        }
    }


    public static void zoneRefreshed(Player pla, Zone e){
        if(e == ClanWar.RED_PORTAL_PVP) {
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
            pla.getGameFrame().openOverlay(265);
            pla.getGameFrame().sendHideIComponent(265, 23, false);
            pla.getGameFrame().sendHideIComponent(265, 0, false);
            pla.getActionSender().sendVar(1305, ClanWarRules.SettingBit.values()[ClanWarRules.ITEMS_ON_DEATH].getFlag());
        }else if(e == ClanWar.WHITE_PORTAL_PVP){
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
            pla.getGameFrame().openOverlay(265);
            pla.getGameFrame().sendHideIComponent(265, 23, false);
            pla.setCurDeathHandler(new DeathHandler() {
                @Override
                public void handleDeath(Killable killed, Killable killedBy) {
                    if(!killed.isInArea(ClanWar.WHITE_PORTAL_PVP))
                        new DefaultDeathHandler().handleDeath(killed, killedBy);
                    else
                        killed.getPlayer().teleport(2994, 9678, 0);

                    killedBy.getPlayer().getAccount().registerKillCount(Account.WHITE_PORTAL);
                    killed.getPlayer().getAccount().registerDeathCount(Account.WHITE_PORTAL);

                    killed.getPlayer().setCurDeathHandler(new DefaultDeathHandler());
                }
            });
            pla.getGameFrame().sendHideIComponent(265, 0, false);
            pla.getActionSender().sendVar(1305, 0);
        }
    }

    private void areaEntered(Player pla, Zone e) {
        if (e.isMulti()) {
            pla.getGameFrame().setMulti(true);    // .sendMulti(1);
        }

        if(e.getName().equalsIgnoreCase("wild") || e.isWilderness())
        {
            if(pla.getCurrentFamiliar() != null)
            {
                if(BossPets.getByPet(pla.getCurrentFamiliar().getId()) == null)
                      pla.getCurrentFamiliar().setTransform(pla.getCurrentFamiliar().getId() + 1);
            }
        }

        if(e.getName().equals("CW_ZONE") || e.getName().equalsIgnoreCase("castle_wars"))
        {
            if(!CastleWarsEngine.getSingleton().isPlaying(pla)){
                pla.teleport(2441, 3089, 0);
            }
        }

        if (e.getName().equalsIgnoreCase("pc_boat") &&!PCGame.static_game.isInLobby(pla)) {
            pla.teleport(2657, 2640, 0);
            pla.setCurrentInstance(null);
        }

        if (pla.getRights() >= Player.ADMINISTRATOR) {
            pla.getActionSender().sendMessage("area entered: " + e.getName());
        }

        String name = e.getName();

        if ((pla.getCurrentFamiliar() != null) && e.isFamiliarsBanned()) {
            pla.resetSummon();;
            pla.getActionSender().sendMessage("Your familiar is not allowed in this area and has been dismissed.");
        }

        if(e == ClanWar.RED_PORTAL_PVP) {
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
            pla.getGameFrame().openOverlay(265);
            pla.getGameFrame().sendHideIComponent(265, 23, false);
            pla.getGameFrame().sendHideIComponent(265, 0, false);
            pla.getActionSender().sendVar(1305, ClanWarRules.SettingBit.values()[ClanWarRules.ITEMS_ON_DEATH].getFlag());
        }else if(e == ClanWar.WHITE_PORTAL_PVP){
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
            pla.getGameFrame().openOverlay(265);
            pla.getGameFrame().sendHideIComponent(265, 23, false);
            pla.setCurDeathHandler(new DeathHandler() {
                @Override
                public void handleDeath(Killable killed, Killable killedBy) {
                  if(!killed.isInArea(ClanWar.WHITE_PORTAL_PVP))
                     new DefaultDeathHandler().handleDeath(killed, killedBy);
                    else
                        killed.getPlayer().teleport(2994, 9678, 0);
                 if(killedBy != null && killedBy.getPlayer() != null)
                    killedBy.getPlayer().addAchievementProgress2(Achievement.WHITE_ENTHUSIAST, 1);
                    killed.getPlayer().setCurDeathHandler(new DefaultDeathHandler());
                }
            });
            pla.getGameFrame().sendHideIComponent(265, 0, false);
            pla.getActionSender().sendVar(1305, 0);
        }

        if (e == PCGame.PEST_CONTROL_GAME) {
            if (!PCGame.static_game.isPlaying(pla)) {
                pla.teleport(3222, 3222, 0);

                return;
            }
        }

        if (name.equals("snow")) {
            pla.getWindowManager().showOverlay(11877);
        }

        if (name.equals("barrows_tomb")) {
            pla.getGameFrame().openOverlay(24);
            pla.getActionSender().sendVar(453, BarrowsGame.slayedBrothers(pla));
        }

        if ((name.equals("wild") || e.isWilderness()) && !pla.isInZone("cw-classic")) {
            pla.getGameFrame().openOverlay(381);
            if(pla.getCurStat(4) > 113){
                pla.setCurStat(4, pla.getMaxStat(4));
                pla.getActionSender().sendMessage("Extreme ranging potions are not allowed in the wild.");
                pla.getActionSender().sendStat(4);
            }
            if(pla.getTimers().timerActive(TimingUtility.OVERLOAD_TIMER)) {
                pla.getActionSender().sendMessage(Text.RED("You have used an overload recently, so your boosted stats have been restored."));
                for(int i = 0; i < 6; i++){
                   if(i != 5)
                        pla.setCurStat(i, pla.getMaxStat(i));
                        pla.getActionSender().sendStat(i);
                }

                pla.getTimers().resetTimer(TimingUtility.OVERLOAD_TIMER);
            }
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");

            if (pla.getAccount().hasVar("wildy_warning")) {
                pla.getActionSender().sendAlert(
                    "WARNING!!!!!",
                    "Do not take items into the wild your not prepared to lose _ At any moment in time your internet may disconnect and you could end up losing your bank _ We are not responsible for refunding items if this happens.",
                    4, 4000);
                pla.getAccount().setSetting("wildy_warning", 1);
            }


            if(pla.getAccount().isWildyBanned()) {
                Dialog.printStopMessage(pla, "You are banned from PVP.");
                pla.teleport(3222, 3222, 0);
            }
            for (int i = 0; i < 14; i++) {
                if (pla.getEquipment().isEquipped(i) && bannedWild(pla.getEquipment().getId(i))) {
                    pla.getBank().insert(pla.getEquipment().getId(i), 1);
                    pla.getActionSender().sendMessage("Item banked: "
                                                      + Item.forId(pla.getEquipment().getId(i)).getName()
                                                      + " [not allowed in wild]");
                    pla.getEquipment().removeItem(i);
                    pla.getEquipment().updateEquipment(i);
                }
            }

            for (int i = 0; i < 28; i++) {
                if ((pla.getInventory().getItem(i) != null) && bannedWild(pla.getInventory().getItem(i).getIndex())) {
                    pla.getBank().insert(pla.getInventory().getItem(i).getIndex(), 1);
                    pla.getActionSender().sendMessage("Item banked: " + pla.getInventory().getItem(i).getName()
                                                      + " [not allowed in wild]");
                    pla.getInventory().deleteItemFromSlot(null, 1, i);
                }
            }
        }


        if(ClanWar.isClanWarArea(e)){
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
            pla.getGameFrame().openOverlay(265);
        }

        if (name.equalsIgnoreCase("zombie_arena")) {
            if (pla.getZombiesGame() == null) {
                pla.teleport(3222, 3222, 0);

                return;
            }
        }

        if (name.equalsIgnoreCase("pc_boat")) {
            pla.getActionSender().sendPlainObject(14314, 2660, 2639, 10, 2);
        }

        if (name.equals("agility_arena")) {
            pla.getGameFrame().openOverlay(5);
            AgilityArena.addPlayer(pla);

        }

        if (e == ClanWar.CLAN_WARS_HOME) {
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, false, "Challenge");
        }

        if (name.equals("duel_lobby")) {
            pla.getWindowManager().showOverlay(201);

            if (!pla.isInZone("duel")) {
                pla.getGameFrame().getContextMenu().sendPlayerCommand(1, false, "Challenge");
            }
        } else if (name.equalsIgnoreCase("duel") || name.equalsIgnoreCase("fight_pits")) {
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
        }

        World.getWorld().getScriptManager().trigger(pla, null, Trigger.AREA_ENTERED, e.getName());
    }

    private void areaLeft(Player pla, Zone e) {
      if(pla.getRights() >= Player.ADMINISTRATOR)
        pla.getActionSender().sendMessage("Area left: " + e.getName());

        if (e.isMulti() &&!pla.isInMultiArea()) {
            pla.getGameFrame().setMulti(false);
        }

        String name = e.getName();

        if(e.getName().equalsIgnoreCase("wild") || e.isWilderness())
        {
            if(pla.getCurrentFamiliar() != null)
            {
                pla.getCurrentFamiliar().setTransform(pla.getCurrentFamiliar().getId());
            }
        }

        if (name.equalsIgnoreCase("zombie_arena") && (pla.getZombiesGame() != null)) {
            pla.getZombiesGame().unlinkPlayer(pla);

            return;
        }

        pla.nexLeave();

        if (name.equalsIgnoreCase("nex")) {
            NexCombatScript.notifyLeave(pla);
        }



        if (name.equalsIgnoreCase("house")) {
            if (pla.getCurrentHouse() != null) {
                pla.getCurrentHouse().notifyRemove(pla);
            }
        }

        if (ClanWar.isClanWarArea(e) && (pla.getGetCurrentWar() != null)) {
            pla.getGetCurrentWar().unlinkPlayer(pla);
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "null");
        }


        if(e == ClanWar.RED_PORTAL_PVP || e == ClanWar.WHITE_PORTAL_PVP) {
            pla.getGameFrame().closeOverlay();
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "null");
        }


        if(e == ClanWar.CLAN_WARS_HOME) {
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, false, "Challenge");

        }
        if (name.equalsIgnoreCase("barrows_tomb") || name.equalsIgnoreCase("snow")) {
            pla.getGameFrame().closeOverlay();
        }

        if (name.equalsIgnoreCase("barrows")) {
            if (pla.getAccount().hasArray("barrows_kc")) {
                pla.getAccount().getArray("barrows_kc").clear();
            }
        }

        if (name.equalsIgnoreCase("agility_arena")) {
            pla.getGameFrame().closeOverlay();
            AgilityArena.removePlayer(pla);
        }

        if (name.equalsIgnoreCase("fight_pits")) {
            pla.getGameFrame().getContextMenu().sendPlayerCommand(2, true, "null");

            if (!(pla.getCurrentInstance() instanceof Lobby)) {
                pla.setCurrentInstance(null);
                pla.getWindowManager().showOverlay(-1);
            }
        }



        if ((name.equalsIgnoreCase("wild") || e.isWilderness()) && !pla.isInWilderness() && !pla.isInArea(DeathTowerGame.TOWER_ZONE)) {
            pla.getGameFrame().closeOverlay();
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "null");
        } else if (name.equalsIgnoreCase("duel")) {
            Duel d = pla.getGameFrame().getDuel();

            if (d != null) {
                d.on_leave(pla);
            }

            if (pla.isInZone("duel_lobby")) {

                pla.getGameFrame().getContextMenu().sendPlayerCommand(1, false, "Challenge");
            }
        } else if (name.equalsIgnoreCase("duel_lobby") &&!pla.isInZone("duel")) {
            pla.getWindowManager().closeOverlay();
            pla.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "null");
        }

        World.getWorld().getScriptManager().trigger(pla, null, Trigger.AREA_LEFT, e.getName());
    }
}
