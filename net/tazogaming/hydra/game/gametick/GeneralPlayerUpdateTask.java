package net.tazogaming.hydra.game.gametick;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.Referral;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.fp.FightPitsGame;
import net.tazogaming.hydra.game.minigame.pc.PCGame;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.skill.farming2.Farming;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.WildernessOverlay;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning.SummoningTab;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.net.packethandler.command.BankCommandHandler;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;
import java.util.Iterator;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 29/09/13
 * Time: 00:05
 * To change this template use File | Settings | File Templates.
 */
public class GeneralPlayerUpdateTask extends PlayerTickEvent {

    /**
     * Constructs ...
     *
     *
     * @param pla
     */
    public GeneralPlayerUpdateTask(Player pla) {
        super(pla, 1, false, true);
    }

    private boolean dontSave(Player plr) {
        if (plr.getZombiesGame() != null) {
            return true;
        }

        if(plr.getAccount().getGrandExchange().hasPendingOffers())
            return true;

        if(CastleWarsEngine.getSingleton().isPlaying(plr) || CastleWarsEngine.getSingleton().isInLobby(plr))
            return true;

        if (plr.getDungeon() != null) {
            return true;
        }


        if (plr.isPreventUnlink()) {
            return true;
        }

        if (plr.isInArea(FightPitsGame.FIGHT_PITS_ZONE)) {
            return true;
        }

        if (plr.isInArea(House.HOUSE_ZONE)) {
            return true;
        }

        if (plr.isInArea(PCGame.PEST_CONTROL_GAME)) {
            return true;
        }

        if (plr.getGetCurrentWar() != null) {
            return true;
        }

        return false;
    }

    /**
     * Method doTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param owner
     */

    @Override
    public void doTick(final Player owner) {
       if(owner.getGameFrame().getCurrentMainWindow() == 105)
        owner.getAccount().getGrandExchange().updateOffers(owner);


        if ((Core.getSecondsForTicks(owner.getAccount().getPlayTime()  / 60) /60) > 1) {
            owner.addAchievementProgress2(Achievement.PLAYER, 1);
        }

        if(owner.getBHTarget() != null && owner.getAccount().getLastBountyLocation() != owner.getBHTarget().getLocation()){
            WildernessOverlay.sendBounty(owner);
            owner.getAccount().setLastBountyLocation(owner.getBHTarget().getLocation());
        }

        if(owner.getAccount().getPlayTime() >= Core.getTicksForHours(8))
        {
            if(owner.getAccount().getReferral() != null)
            {
                final Referral ref = owner.getAccount().getReferral();
                owner.getAccount().setReferral(null);
                World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
                    @Override
                    public boolean compute() {
                        World.getWorld().getDatabaseLink().query("UPDATE `user` set user_tokens=user_tokens+10, ref_pot=ref_pot+10000000 where id="+ref.getReferredFrom());

                        return true;

                    }
                });
            }
        }

        if ((Core.getSecondsForTicks(owner.getAccount().getPlayTime() / 60) / 60) > 3) {
            owner.addAchievementProgress2(Achievement.ENTHUSIAST, 1);
        }

        for(int i = 0; i < 6; i++)
           if(owner.getMaxStat(i) == 99)
            owner.addAchievementProgress2(Achievement.COMBAT_ENTHUSIAST, 1);

        if(owner.getCombatLevel() == 126)
            owner.addAchievementProgress2(Achievement.MAXED, 1);

        for(int i = 6; i < 24; i++)
            if(owner.getMaxStat(i) == 99)
                owner.addAchievementProgress2(Achievement.PROGRESSOR, 1);


        if ((Core.getSecondsForTicks(owner.getAccount().getPlayTime() / 60) / 60) > 10) {
            owner.addAchievementProgress2(Achievement.FANATIC, 1);
        }

        if ((Core.getSecondsForTicks(owner.getAccount().getPlayTime() / 60) / 60) > 24) {
            owner.addAchievementProgress2(Achievement.ADDICT, 1);
        }

        if(owner.getRunDirection() > -1 && owner.getRunDirection() != -2999)
            owner.addAchievementProgress2(Achievement.MARATHON, 2);
        else if(owner.getWalkDir() != -1)
            owner.addAchievementProgress2(Achievement.MARATHON, 1);


        if (((Core.getSecondsForTicks(owner.getAccount().getPlayTime()) / 60) / 60) >= (24 * 25)) {
            owner.addAchievementProgress2(Achievement.HYDRASCAPE_RAVING_ADDICT, 1);
        }



        owner.getScriptedTaskScheduler().update();

        if (owner.getAccount().getGame() != null) {
            owner.getAccount().getGame().updateTargetLocation();
        }

        owner.updateDelayedScripts();

        if ((owner.getCurrentFamiliar() != null) && (Core.currentTime % 3 == 0)) {
            SummoningTab.updateTime(owner);
        }

        if (owner.isInWilderness()) {
            owner.getAccount().updatePVPTime();

            if (owner.getCombatAdapter().getFightingWith() instanceof Player) {
                owner.getAccount().updatePVPFightingTime();
            }
        }

        owner.getAccount().updateBossTimes();

        if (owner.getAccount().farmUpdateRequired) {
            Farming.bind(getPlayer());
            owner.getAccount().farmUpdateRequired = false;
        }

        if (owner.getAppearance().getRequested() != null) {
            int id = owner.getAppearance().getCurrentId();

            owner.setAppearance(owner.getAppearance().getRequested());
            owner.getAppearance().setCurrentID(id + 50);
            owner.getAppearance().setChanged(true);
            owner.getAppearance().setRequested(null);
        }

        /*
         * Check lending's
         */
        owner.getAccount().updateRented();

        /*
         * Validate movement
         */
        if ((owner.getLastSprite() != -1) &&!owner.isRunning()
                && (owner.getAccount().getSmallSetting(Account.INFINITE_RUN)
                    != 1) || ((owner.getGetCurrentWar() != null) && owner.getGetCurrentWar().isFlagHolder(owner))) {
            owner.removeEnergy();

            if ((owner.getGetCurrentWar() != null) && owner.getGetCurrentWar().isFlagHolder(owner)) {
                for (int i = 0; i < 15; i++) {
                    owner.removeEnergy();
                }
            }
        }

        if (!dontSave(getPlayer()) && (owner.getAccount().getLoggedInSince() != 0)
                && (owner.getAccount().isSaveRequested() || owner.getAccount().getLoggedInSince() % Config.AUTOSAVE_DELAY == 0)) {
            owner.getAccount().setSaveRequested(false);
            World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
                @Override
                public boolean compute() {
                    synchronized (getPlayer()) {
                        new UserAccount().build_save_game(getPlayer());

                        return true;
                    }

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
        }



        if (owner.getCurrentEnergy() < 1) {
            owner.getAccount().setB(119, 0, true);
        }

        owner.getAccount().updatePlaytime();

        if ((owner.getAccount().getPlayTime() % Config.BANK_RESTORE_RATE) == 0) {
            if (owner.getAccount().hasVar("bank_credits")
                    && (owner.getAccount().getInt32("bank_credits") < BankCommandHandler.getMaxBankCredits(owner))) {
                owner.getAccount().setSetting("bank_credits", BankCommandHandler.getMaxBankCredits(owner), true);
                owner.getActionSender().sendMessage("Your bank credit's have been restored");
                owner.getActionSender().changeLine("Open bank (" + owner.getAccount().getInt32("bank_credits")
                                                   + ") remaining", 39959);
            }
        }

        if (owner.getAccount().getPlayTime() % 100 == 0) {
            owner.getActionSender().sendPlaytime();

            // owner.getActionSender().changeLine();
        }

        if (owner.isFollowing()) {
            int distance = Point.getDistance(owner.getFollowing().getFollowing().getLocation(), owner.getLocation());

            if ((distance > 15) || owner.getFollowing().getFollowing().isDead()
                    ||!owner.withinRange(owner.getFollowing().getFollowing())) {
                getPlayer().unFollow();
                getPlayer().getFocus().unFocus();
            }
        }

        /*
         * Update scripts
         */
        getPlayer().getScriptsToProcess().addAll(getPlayer().getNewScriptQueue());
        getPlayer().getNewScriptQueue().clear();

        for (Iterator<Scope> scriptIterator =
                getPlayer().getScriptsToProcess().iterator(); scriptIterator.hasNext(); ) {
            Scope script = scriptIterator.next();

            if (script.isTerminated()) {
                scriptIterator.remove();

                continue;
            }

            if (script.isWaiting()) {
                script.validateNpcChat();

                continue;
            }

            if (script.isDelayed()) {
                if (script.hasDelayTimePassed()) {
                    script.terminate();
                    scriptIterator.remove();
                }

                continue;
            }

            if (script.canRun() &&!script.isDelayed()) {
                if (World.getWorld().getScriptManager().runScript(script)) {
                    script.notifyParentNodes();
                    script.terminate();

                    if (!script.isDelayed()) {
                        scriptIterator.remove();
                    }
                }
            }
        }

        /*
         * Update timers
         */
        getPlayer().getTimers().updateTimers(getPlayer());

        /*
         * Update other actions
         */
        if (getPlayer().getCurrentAction() != null) {
            getPlayer().getCurrentAction().tick();
        }
    }
}
