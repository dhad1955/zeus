package net.tazogaming.hydra.game.gametick;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 17:29
 */
public abstract class WorldTickEvent {
    private boolean isTerminated;
    private int     ticks;
    private Object  attachment;
    private int     startedAt;

    /**
     * Constructs ...
     *
     *
     * @param ticks
     */
    public WorldTickEvent(int ticks) {
        this.startedAt = Core.currentTime + ticks;
        this.ticks     = ticks;
    }

    /**
     * Constructs ...
     *
     *
     * @param ticks
     * @param at
     */
    public WorldTickEvent(int ticks, Object at) {
        this.attachment = at;
        this.ticks      = ticks;
        this.startedAt  = Core.currentTime + ticks;
    }

    /**
     * Method getAttachment
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object getAttachment() {
        return this.attachment;
    }

    /**
     * Method isReady
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isReady() {
        return Core.currentTime >= startedAt;
    }

    /**
     * Method timePassed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int timePassed() {
        return Core.currentTime - (startedAt - ticks);
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        tick();

        if (isReady()) {
            finished();
            terminate();
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void terminate() {
        isTerminated = true;
    }

    /**
     * Method isTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminated() {
        return isTerminated;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void tick();

    /**
     * Method finished
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void finished();
}
