package net.tazogaming.hydra.game.gametick;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 28/09/13
 * Time: 00:51
 * To change this template use File | Settings | File Templates.
 */
public abstract class PlayerTickEvent {
    public static final int TYPE_DEATH     = 0;
    private boolean         autoRemove     = true;
    private boolean         nonActionEvent = false;
    private boolean         terminated     = false;
    private int             id             = 0;
    private Player          player;
    private int             startedAt;
    private int             endsAt;
    private int             curTick;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param time
     */
    public PlayerTickEvent(Player pla, int time) {
        this(pla, time, true, false);
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param nonAction
     * @param time
     */
    public PlayerTickEvent(Player pla, boolean nonAction, int time) {
        this(pla, time, false, nonAction);
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param time
     * @param b
     * @param ba
     */
    public PlayerTickEvent(Player pla, int time, boolean b, boolean ba) {
        this(pla, time, b, ba, -1);
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param time
     * @param autoRemove
     * @param nonAction
     * @param id
     */
    public PlayerTickEvent(Player pla, int time, boolean autoRemove, boolean nonAction, int id) {
        this.nonActionEvent = nonAction;
        startedAt           = Core.currentTime;
        player              = pla;
        this.autoRemove     = autoRemove;
        this.id             = id;
        this.endsAt         = time;
    }

    /**
     * Method getID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getID() {
        return id;
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void terminate() {
        terminated = true;
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        curTick++;

        if (ready() &&!isTerminated()) {
            doTick(player);

            if (autoRemove) {
                terminated = true;
            } else {
                startedAt = Core.currentTime;
            }
        }
    }

    /**
     * Method requestTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void requestTermination() {
        if (!this.nonActionEvent) {
            terminated = true;
        }
    }

    protected void updateDelay(int delay) {
        startedAt   = Core.currentTime;
        this.endsAt = delay;
    }

    /**
     * Method isNonActionEvent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isNonActionEvent() {
        return nonActionEvent;
    }

    /**
     * Method setNonActionEvent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param nonActionEvent
     */
    public void setNonActionEvent(boolean nonActionEvent) {
        this.nonActionEvent = nonActionEvent;
    }

    /**
     * Method getCurTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurTick() {
        return curTick;
    }

    /**
     * Method getEndsAt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getEndsAt() {
        return endsAt;
    }

    /**
     * Method setEndsAt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param endsAt
     */
    public void setEndsAt(int endsAt) {
        this.endsAt = endsAt;
    }

    /**
     * Method isAutoRemove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isAutoRemove() {
        return autoRemove;
    }

    /**
     * Method setAutoRemove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param autoRemove
     */
    public void setAutoRemove(boolean autoRemove) {
        this.autoRemove = autoRemove;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method setController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Method isTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminated() {
        return terminated;
    }

    /**
     * Method ready
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean ready() {
        return Core.timeSince(startedAt) >= endsAt;
    }

    /**
     * Method updateTask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateTask() {}

    /**
     * Method doTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param owner
     */
    public abstract void doTick(Player owner);
}
