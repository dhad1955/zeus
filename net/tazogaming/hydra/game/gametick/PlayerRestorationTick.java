package net.tazogaming.hydra.game.gametick;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.PrayerBook;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 28/09/13
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
public class PlayerRestorationTick extends PlayerTickEvent {

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param time
     * @param autoRemove
     * @param nonAction
     */
    public PlayerRestorationTick(Player pla, int time, boolean autoRemove, boolean nonAction) {
        super(pla, time, autoRemove, nonAction);
    }

    /**
     * Method doTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param owner
     */
    @Override
    public void doTick(Player owner) {

        /*
         * Update pvp points
         */
        owner.getAccount().updateKills();

        /*
         * Restore run energy
         *
         */
        if (owner.getCurrentEnergy() < 100) {
            owner.setCurrentEnergy(owner.getCurrentEnergy() + 0.25);
            owner.getActionSender().updateEnergy();
        }

        /*
         * Restore health
         */
        if ((getPlayer().getCurrentHealth() < getPlayer().getMaxHealth()) && Core.timePassed(60)) {
            getPlayer().addHealth(1);
        }


        if ((getPlayer().getCurrentHealth() > getPlayer().getMaxHealth()) && Core.timePassed(30)) {
            getPlayer().setCurrentHealth(getPlayer().getCurrentHealth() - 1);
            getPlayer().getActionSender().sendStat(3);
        }

        /*
         * Restore potions & special attacks
         */
        int restorationTime = 120;


        if(getPlayer().getPrayerBook().curseActive(PrayerBook.BERSERKER))
            restorationTime *= 2;

        if (Core.timePassed(restorationTime)) {
            owner.update_curse_drain();
            owner.getPrayerBook().update_curse_boost();

            for (int i = 0; i < 25; i++) {
                if ((getPlayer().getCurStat(i) > getPlayer().getMaxStat(i)) && (!getPlayer().isTenth(15))) {
                    int stat = getPlayer().getCurStat(i);

                    getPlayer().setCurStat(i, stat - 1);
                    getPlayer().getActionSender().sendStat(i);
                } else if (getPlayer().getCurStat(i) < getPlayer().getMaxStat(i)) {
                    int stat = getPlayer().getCurStat(i);

                    getPlayer().setCurStat(i, stat + 1);
                    getPlayer().getActionSender().sendStat(i);
                }
            }
        }

        /*
         * Poisoning
         */
        if (getPlayer().isPoisoned()) {
            if ((getPlayer().updatePoisonTime() % 100) == 0) {
                getPlayer().hit(getPlayer(), getPlayer().getDamageForPoisonTime(), Damage.TYPE_POISON, 0);
            }
        }

        /*
         * Special bar
         */
        int specialTicks = Core.getTicksForSeconds(15);


        if (getPlayer().isInWilderness()) {
            specialTicks = Core.getTicksForSeconds(30);
        }

        if (Core.timePassed(specialTicks)) {
            if (getPlayer().getCurrentSpecial() < 10) {
                getPlayer().setCurrentSpecial(getPlayer().getCurrentSpecial() + 0.5);
                SpecialAttacks.updateSpecial(getPlayer());
            }

            getPlayer().increaseSumSpec();
        }

        /*
         * Summoning
         */
        if (Core.ticksPassed(100)) {
            if ((getPlayer().getCurrSummonPts() > 0) && (getPlayer().getCurrentFamiliar() != null)) {
                getPlayer().setCurrSummonPts(getPlayer().getCurrSummonPts() - 1);
                getPlayer().getActionSender().sendStat(23);

                if (getPlayer().getCurrSummonPts() == 0) {
                    getPlayer().getActionSender().sendMessage("You've ran out of summoning points.");
                }
            }
        }

        /*
         * Active prayers
         */
        if (Core.ticksPassed(2)) {
            getPlayer().getPrayerBook().updatePrayer();
        }
    }
}
