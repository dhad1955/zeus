package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueEntry;
import net.tazogaming.hydra.game.minigame.pkleague.PKTournament;
import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/11/2014
 * Time: 06:58
 */
public class Killcounts {

    /** informationList made: 14/11/19 **/
    private static Map<Integer, KillCountType> informationList = new HashMap<Integer, KillCountType>();

    /**
     * Method load
     * Created on 14/11/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load() {
        informationList.clear();

        TextFile file = TextFile.fromPath("config/killcounts.cfg");

        if (!file.exists()) {
            return;
        }

        for (Line line : file) {

            if (line.getData().startsWith("kc")) {
                try {
                    if (line.getData().startsWith("//")) {
                        continue;
                    }

                    String[] split = line.getData().split(" ");
                    int      cat   = Integer.parseInt(split[2]);
                    int      npcId = Integer.parseInt(split[3]);

                    if (!informationList.containsKey(cat)) {
                        informationList.put(npcId, new KillCountType(cat, npcId));
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();

                }
            }
        }
    }

    /**
     * Method get
     * Created on 14/11/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     *
     * @return
     */
    public static KillCountType get(int key) {
        if (informationList.containsKey(key)) {
            return informationList.get(key);
        }

        return null;
    }

    /**
     * Method display
     * Created on 14/11/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void display(Player player) {
        List[] categories = new List[3];

        player.getGameFrame().openWindow(275);
        for (int i = 0; i < categories.length; i++) {
            categories[i] = new LinkedList<KillCountRecord>();
        }

        Map<Integer, Integer> playerKc = player.getKillCounts();

        for (Integer npcIds : playerKc.keySet()) {
            KillCountType type = get(npcIds);

            if (type != null) {
                categories[type.cat].add(new KillCountRecord(npcIds, playerKc.get(npcIds)));
            }
        }

        for (int i = 0; i < categories.length; i++) {
            Collections.sort(categories[i], new Comparator<KillCountRecord>() {
                @Override
                public int compare(KillCountRecord o1, KillCountRecord o2) {
                    if (o1.kc > o2.kc) {
                        return 1;
                    } else if (o1.kc == o2.kc) {
                        return 0;
                    }

                    return -1;
                }
            });
        }

        int cur = 16;

        for (int i = 0; i < categories.length; i++) {
            if (categories[i].size() == 0) {
                continue;
            }

            player.getGameFrame().sendString((i == 0)
                                             ? "---- BOSS KILLCOUNTS ----"
                                             : "---- SLAYER KILLCOUNTS -----", 275, cur++);
            player.getGameFrame().sendString("", 275, cur++);

            List<KillCountRecord> tmp = categories[i];

            for (KillCountRecord kc : tmp) {
                player.getGameFrame().sendString(NpcDef.FOR_ID(kc.npcid).getName() + " - kills: "
                                                 + Text.BLUE("" + kc.kc), 275, cur++);
            }

            player.getGameFrame().sendString("", 275, cur++);
        }

        for (int i = cur; i < 100; i++) {
            player.getGameFrame().sendString("", 275, i);
        }
    }

    /**
     * Class description
     * Hydrascape 639 Game server
     * Copyright (C) Tazogaming 2014
     *
     *
     * @version        Enter version here..., 14/11/19
     * @author         Daniel Hadland
     */
    private static class KillCountRecord {
        int npcid;
        int kc;

        /**
         * Constructs ...
         *
         *
         * @param npcid
         * @param kc
         */
        private KillCountRecord(int npcid, int kc) {
            this.npcid = npcid;
            this.kc    = kc;
        }
    }


    /**
     * Class description
     * Hydrascape 639 Game server
     * Copyright (C) Tazogaming 2014
     *
     *
     * @version        Enter version here..., 14/11/19
     * @author         Daniel Hadland
     */
    public static class KillCountType {
        int npcID, cat;

        KillCountType(int cat, int npcID) {
            this.cat   = cat;
            this.npcID = npcID;
        }
    }
}
