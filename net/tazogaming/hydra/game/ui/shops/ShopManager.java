package net.tazogaming.hydra.game.ui.shops;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.ui.SkillGuide;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 22:04
 */
public class ShopManager implements RecurringTickEvent {
    private static HashMap<Integer, Shop> shops      = new HashMap<Integer, Shop>();
    private static Player                   loadPlayer = null;

    static {
        Core.submitTask(new ShopManager());
    }

    /**
     * Method getShop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static Shop getShop(int id) {
        return shops.get(id);
    }

    /**
     * Method reload
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void reload(Player pla) {
        shops.clear();
        loadPlayer = pla;
        load();
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load() {
        File f = new File("config/shops/");

        for (File e : f.listFiles()) {
            loadShop(e);
        }
    }

    /**
     * Method sellItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     * @param slot
     * @param pla
     * @param amount
     */
    public static void sellItem(Shop s, int slot, Player pla, int amount) {
        Item item = pla.getInventory().getItem(slot);

        if (item == null) {
            return;
        }

        if (pla.getAccount().hasVar("trade_lock")) {
            return;
        }

        if (!item.isTradeable()) {
            pla.getActionSender().sendMessage("You cant sell this item");

            return;
        }

        if (!s.isSellingAllowed()) {
            pla.getActionSender().sendMessage("You can't sell to this store.");

            return;
        }

        int price = item.getPrice() / 2;
        int max   = pla.getInventory().getItemMax(slot, amount);

        if (amount > max) {
            amount = max;
        }

        int shopSlot = s.getItemSlot(item.getIndex());

        if (item.getIndex() == 995) {
            return;
        }

        if (shopSlot == -1) {
            if (s.isGeneralStore()) {
                if (s.addNewItem(item, amount, -1) && (s.getItemSlot(item.getIndex()) != -1)) {
                    pla.getInventory().deleteItem(item, amount);
                    pla.getInventory().addItem(Item.forId(995), price * amount);
                    s.setChanged(true);
                }
            } else {
                pla.getActionSender().sendMessage("The shop can not accept this item.");
            }
        } else {
            pla.getInventory().deleteItem(item, amount);
            s.addItem(item.getIndex(), amount);
            pla.getInventory().addItem(Item.forId(995), price * amount);
            s.setChanged(true);
        }
    }

    private static void buyStaticItem(Shop s, int slot, Player player) {
        if (s.getItem(slot) == null) {
            return;
        }

        Item item = s.getItem(slot);    // .getIndex();
        int  cost = (int) (item.getPrice() * 1.20);

        if (s.getCost(slot) > -1) {
            cost = s.getCost(slot);
        }

        if (player.getInventory().getFreeSlots(item.getIndex()) == 0) {
            player.getActionSender().sendMessage("You don't have enough free space.");

            return;
        }

        if (s.getCurrencyPoints() != -1) {
            if (player.getPoints(s.getCurrencyPoints()) < cost) {
                player.getActionSender().sendMessage("You don't have enough points");

                return;
            }

            player.setPoints(s.getCurrencyPoints(), player.getPoints(s.getCurrencyPoints()) - cost);
        } else {
            int currency = 995;

            if (s.getCurrency() > 0) {
                currency = s.getCurrency();
            }

            if (!player.getInventory().hasItem(currency, cost)) {
                player.getActionSender().sendMessage("You don't have enough");

                return;
            }

            player.getInventory().deleteItem(currency, cost);
        }

        player.getInventory().addItem(item.getIndex(), s.getAmount(slot));
    }

    /**
     * Method buyItemForPoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     * @param slot
     * @param points
     * @param pla
     * @param amount
     */
    public static void buyItemForPoints(Shop s, int slot, int points, Player pla, int amount) {
        Item item = s.getItem(slot);

        if (item == null) {
            pla.getActionSender().sendDev("Item is null: " + slot);

            return;
        }

        int freeSlotsAvailable = pla.getInventory().getFreeSlots(item.getIndex());

        if (freeSlotsAvailable == 0) {
            pla.getActionSender().sendMessage("You don't have enough space.");

            return;
        }

        if ((freeSlotsAvailable < amount) &&!item.isStackable()) {
            amount = freeSlotsAvailable;
            pla.getActionSender().sendMessage("You don't have enough space.");
        }

        if (amount > 40000) {
            pla.getActionSender().sendMessage("The maximum you can buy at a time is 40,000");
            amount = 40000;
        }

        long expectedPrice = (long) ((s.getCost(slot) == -1)
                                     ? (item.getPrice() * 1.20)
                                     : (long) s.getCost(slot)) * (long) amount;

        if (expectedPrice > Integer.MAX_VALUE) {
            pla.getActionSender().sendMessage("Too many!");
            amount = s.getCost(slot) / Integer.MAX_VALUE;
        }

        int playersCurrency = pla.getPoints(points);

        if (playersCurrency < expectedPrice) {
            amount = playersCurrency / ((s.getCost(slot) == -1)
                                        ? (int) (item.getPrice() * 1.20)
                                        : s.getCost(slot));

            if (amount != 0) {
                pla.getActionSender().sendMessage("You don't have enough, you buy: " + amount);
            } else {
                pla.getActionSender().sendMessage("You don't have enough coins");
            }
        }

        if (s.getAmount(slot) < amount) {
            amount = s.getAmount(slot);
            pla.getActionSender().sendMessage("The shop has ran out of stock");
        }

        if (amount <= 0) {
            return;
        }

        pla.setPoints(points, pla.getPoints(points) - (amount * ((s.getCost(slot) == -1)
                ? (int) (item.getPrice() * 1.20)
                : s.getCost(slot))));
        pla.getInventory().addItem(s.getItem(slot), amount);
        s.deleteItem(slot, amount);
        s.setChanged(true);
    }

    /**
     * Method buyItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     * @param slot
     * @param pla
     * @param amount
     */

    private static String cashFormat(int amount){
        if (amount < 100000) {
            return String.valueOf(amount);
        }

        if (amount < 1000000) {
            return amount / 1000 + "K";
        } else {
            return amount / 1000000 + "M";
        }
    }
    public static void buyItem(Shop s, int slot, Player pla, int amount) {
        Item item = s.getItem(slot);

        if (item == null) {
            pla.getActionSender().sendDev("Item is null: " + slot);

            return;
        }




        if(s.getItem(slot).getIndex() >= Equipment.MASTER_CAPES[0]){
            for(int i = 0; i < Equipment.MASTER_CAPES.length; i++)
               if(s.getItem(slot).getIndex() == Equipment.MASTER_CAPES[i]) {
                   if (pla.getExps()[i] < 500000000) {
                       pla.getActionSender().sendMessage("You don't have enough " + SkillGuide.SKILL_NAMES[i] + " experience to buy this ("+cashFormat(500000000 - pla.getExps()[i])+" more  needed)");
                       return;
                   }
               }
        }
        if(World.getWorld().getScriptManager().directTrigger(pla, null, Trigger.ON_BUY, s.id, item.getIndex())){
            return;
        }

        if (s.isStaticMode()) {
            buyStaticItem(s, slot, pla);

            return;
        }


        boolean defaultPrice = (s.getCost(slot) == -1) && (s.getCurrencyPoints() != 12);

        if ((s.getCurrencyPoints() != -1) && (s.getCurrencyPoints() != 12)) {
            buyItemForPoints(s, slot, s.getCurrencyPoints(), pla, amount);

            return;
        }

        int freeSlotsAvailable = pla.getInventory().getFreeSlots(item.getIndex());

        if (freeSlotsAvailable == 0) {
            pla.getActionSender().sendMessage("You don't have enough space.");

            return;
        }

        if ((freeSlotsAvailable < amount) &&!item.isStackable()) {
            amount = freeSlotsAvailable;
            pla.getActionSender().sendMessage("You don't have enough space.");
        }

        if (amount > 40000) {
            pla.getActionSender().sendMessage("The maximum you can buy at a time is 40,000");
            amount = 40000;
        }

        long expectedPrice = (long) ((defaultPrice)
                                     ? (item.getPrice() * 1.20)
                                     : (long) s.getCost(slot)) * (long) amount;

        if (expectedPrice > Integer.MAX_VALUE) {
            pla.getActionSender().sendMessage("Too many!");
            amount = s.getCost(slot) / Integer.MAX_VALUE;
        }

        int currency        = (s.getCurrency() == -1)
                              ? 995
                              : s.getCurrency();
        int playersCurrency = pla.getInventory().getStackCount(currency);

        if (playersCurrency < expectedPrice) {
            amount = playersCurrency / ((defaultPrice)
                                        ? (int) (item.getPrice() * 1.20)
                                        : s.getCost(slot));

            if (amount != 0) {
                pla.getActionSender().sendMessage("You don't have enough, you buy: " + amount);
            } else {
                pla.getActionSender().sendMessage("You don't have enough coins");
            }
        }

        if (s.getAmount(slot) < amount) {
            amount = s.getAmount(slot);
            pla.getActionSender().sendMessage("The shop has ran out of stock");
        }

        if (amount <= 0) {
            return;
        }

        pla.log("bought shop item "+s.getItem(slot)+" x "+amount);

        pla.getInventory().deleteItem(currency, amount * ((defaultPrice)
                ? (int) (item.getPrice() * 1.20)
                : s.getCost(slot)));
        pla.getInventory().addItem(s.getItem(slot), amount);
        s.deleteItem(slot, amount);
        s.setChanged(true);
    }

    /**
     * Method tickShops
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void tickShops() {
        for (Shop s : shops.values()) {
            s.tick();
        }
    }

    /**
     * Method resetShops
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void resetShops() {
        for (Shop s : shops.values()) {
            s.hasChanged = false;
        }
    }

    private static void loadShop(File f) {
        String line    = null;
        int    lineNum = 0;

        try {
            BufferedReader     in            = new BufferedReader(new FileReader(f));
            ArrayList<Integer> shopItems     = new ArrayList<Integer>();
            ArrayList<Integer> shopItemsA    = new ArrayList<Integer>();
            ArrayList<Integer> shopItemsCost = new ArrayList<Integer>();
            int                shopID        = 0;
            int                currency      = 995;
            Shop               shop          = new Shop();

            while ((line = in.readLine()) != null) {
                lineNum++;

                String[] tokens = line.split(" ");

                if (tokens[0].equalsIgnoreCase("subshop")) {
                    int     id   = Integer.parseInt(tokens[2]);
                    String  name = tokens[3].replaceAll("_", " ");
                    SubShop st   = new SubShop();

                    st.id   = id;
                    st.name = name;
                    shop.shops.add(st);
                }

                if (tokens[0].equalsIgnoreCase("shopname")) {
                    String shopName = line.split(" ", 3)[2].replaceAll("_", " ");

                    shop.setName(shopName);
                }

                if(tokens[0].equalsIgnoreCase("iron_man")){
                    shop.setIronManEnabled(Boolean.parseBoolean(tokens[2]));
                }

                if (tokens[0].equalsIgnoreCase("static_mode")) {
                    shop.setStaticMode(Boolean.parseBoolean(tokens[2]));
                }

                if (tokens[0].equalsIgnoreCase("selling_disabled")) {
                    shop.setSellingAllowed(!Boolean.parseBoolean(tokens[2]));
                }

                if (tokens[0].equalsIgnoreCase("currency_points_id")) {
                    shop.setCurrencyPoints(Integer.parseInt(tokens[2]));
                }

                if (tokens[0].equalsIgnoreCase("shopid")) {
                    shopID = Integer.parseInt(tokens[2]);
                }

                if (tokens[0].equalsIgnoreCase("currency")) {
                    currency = Integer.parseInt(tokens[2]);
                }

                if (tokens[0].equalsIgnoreCase("shopitem")) {
                    if (tokens[3].equalsIgnoreCase("inf")) {
                        tokens[3] = Integer.toString(Integer.MAX_VALUE);

                        // System.out.println("Infinity");
                    }

                    int item = Integer.parseInt(tokens[2]);
                    int id   = Integer.parseInt(tokens[3]);

                    shopItems.add(item);
                    shopItemsA.add(id);

                    try {
                        int cost = Integer.parseInt(tokens[4]);

                        // Logger.log(cost);
                        shopItemsCost.add(cost);
                    } catch (Exception ee) {
                        shopItemsCost.add(-1);
                    }
                }
            }

            Object[] items  = shopItems.toArray();
            Object[] itemsA = shopItemsA.toArray();
            Object[] itemsC = shopItemsCost.toArray();
            Item[]   items2 = new Item[50];
            int[]    items3 = new int[50];
            int[]    items4 = new int[50];

            for (int i = 0; i < items.length; i++) {
                items2[i] = Item.forId(Integer.parseInt(items[i].toString()));
                items3[i] = Integer.parseInt(itemsA[i].toString());
                items4[i] = Integer.parseInt(itemsC[i].toString());
            }

            shop.setItems(items2);
            shop.id = shopID;
            shop.setCurrency(currency);
            shop.setItemAmount(items3);
            shop.setItemsCost(items4);
            shops.put(shopID, shop);
            in.close();
        } catch (Exception ee) {
            ee.printStackTrace();
            Logger.log("Error " + f.getName() + " line: " + line);

            if (loadPlayer != null) {
                loadPlayer.getActionSender().sendMessage("Error: " + f.getName() + " parse error line: " + lineNum);
                loadPlayer.getActionSender().sendMessage("@red@" + line);
            }
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        tickShops();

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
