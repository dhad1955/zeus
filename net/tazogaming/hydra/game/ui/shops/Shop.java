package net.tazogaming.hydra.game.ui.shops;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 22:04
 */
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Gander
 * @Date: 11-Aug-2008
 * @Time: 23:41:46.
 */
public class Shop {
    private ArrayList<Player> shoppers           = new ArrayList<Player>();
    public ArrayList<SubShop> shops              = new ArrayList<SubShop>();
    public int                id                 = -1;
    private int               currency           = 995;
    private boolean[]         shopItemsType      = new boolean[100];
    private int[]             nextRestock        = new int[100];
    private int               currency_points_id = -1;
    private String            var_name           = null;
    private String            shop_points_name   = null;
    private boolean           sellingAllowed     = true;
    private boolean           staticMode         = false;
    private int[]             cachedItemCount;
    public boolean            hasChanged;
    private String            shopName;
    private Item[]            shopItems;
    private int[]             shopItemsA;
    private int[]             shopItemsCost;
    private boolean[] shopInfinity;

    public boolean isIronManEnabled() {
        return isIronManEnabled;
    }

    public void setIronManEnabled(boolean isIronManEnabled) {
        this.isIronManEnabled = isIronManEnabled;
    }

    private boolean isIronManEnabled = false;



    public static final int IRON_MAN_SHOP = 4991;

    /**
     * Constructs ...
     *
     */
    public Shop() {
        this.shopItemsCost = new int[100];
        this.shopInfinity = new boolean[500];

        for (int i = 0; i <100; i++) {
            this.shopItemsType[i] = false;
            this.shopItemsCost[i] = -1;

            this.nextRestock[i]   = Core.currentTime + 100;
        }
    }


    public int randomItem() {
        List<Integer> possibleItems = new ArrayList<Integer>();
        for(int i = 0; i < shopItems.length; i++){
            if(shopItems[i] != null)
                possibleItems.add(shopItems[i].getIndex());
        }

        return possibleItems.get(GameMath.rand3(possibleItems.size()));
    }

    /**
     * Method isSellingAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSellingAllowed() {
        return sellingAllowed;
    }

    /**
     * Method priceOf
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int priceOf(int id) {
        for (int i = 0; i < shopItems.length; i++) {
            if ((shopItems[i] != null) && (shopItems[i].getIndex() == id)) {
                return shopItemsCost[i];
            }
        }

        return -1;
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Item[] getItems() {
        return shopItems;
    }

    /**
     * Method getItemsCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getItemsCount() {
        return shopItemsA;
    }

    /**
     * Method setSellingAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sellingAllowed
     */
    public void setSellingAllowed(boolean sellingAllowed) {
        this.sellingAllowed = sellingAllowed;
    }

    /**
     * Method setCurrencyPoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ind
     */
    public void setCurrencyPoints(int ind) {
        currency_points_id = ind;
    }

    /**
     * Method isStaticMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isStaticMode() {
        return staticMode;
    }

    /**
     * Method setStaticMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param staticMode
     */
    public void setStaticMode(boolean staticMode) {
        this.staticMode = staticMode;
    }

    /**
     * Method getCurrencyPoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrencyPoints() {
        return currency_points_id;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {
        for (int i = 0; i < shopItems.length; i++) {
            if (shopItems[i] != null) {
                if (shopItemsType[i] && (Core.timeUntil(nextRestock[i]) <= 0)) {
                    shopItems[i] = null;
                    deleteItem(i, shopItemsA[i]);
                    setChanged(true);
                    reOrderItems();
                } else {
                    if (Core.timeUntil(nextRestock[i]) <= 0) {
                        nextRestock[i] = Core.currentTime + 100;

                        if (cachedItemCount[i] > shopItemsA[i]) {
                            shopItemsA[i]++;
                            setChanged(true);
                        }
                    }
                }
            }
        }
    }

    /**
     * Method getSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSize() {
        int size = 0;

        for (int i = 0; i < this.shopItems.length; i++) {
            if (shopItems[i] != null) {
                size++;
            }
        }

        return size;
    }


    public int length() {
        return this.shopItems.length;
    }
    /**
     * Method isGeneralStore
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isGeneralStore() {
       if(shopName == null)
           return false;

        return this.shopName.toLowerCase().contains("general");
    }

    /**
     * Method setChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     *
     * @return
     */
    public boolean setChanged(boolean b) {
        this.hasChanged = b;

        return b;
    }

    /**
     * Method hasChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasChanged() {
        return this.hasChanged;
    }

    /**
     * Method addShopper
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void addShopper(Player pla) {
        if (!this.shoppers.contains(pla)) {
            this.shoppers.add(pla);
        }
    }

    /**
     * Method removeShopper
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void removeShopper(Player pla) {
        this.shoppers.remove(pla);
    }

    /**
     * Method getShoppers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Player> getShoppers() {
        return this.shoppers;
    }

    /**
     * Method deleteItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param amount
     */
    public void deleteItem(int slot, int amount) {
        if (amount < shopItemsA.length) {
            shopItemsA[slot] -= amount;
        } else {
            this.shopItemsA[slot] -= amount;
        }

        if (this.shopItemsA[slot] < 0) {
            this.shopItemsA[slot] = 0;
        }

        if (this.shopItemsType[slot] && (shopItemsA[slot] == 0)) {
            this.shopItems[slot] = null;
            reOrderItems();
        }
    }

    /**
     * Method isInShop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param idx
     *
     * @return
     */
    public boolean isInShop(int idx) {
        for (Item shopItem : this.shopItems) {
            if (shopItem.getIndex() == idx) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void addItem(int id) {
        for (int i = 0; i < this.shopItems.length; i++) {
            if (shopItems[i] == null) {
                continue;
            }

            if (shopItems[i].getIndex() == id) {
                shopItemsA[i] += 1;
            }
        }
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amt
     */
    public void addItem(int id, int amt) {
        for (int i = 0; i < this.shopItems.length; i++) {
            if (shopItems[i] == null) {
                continue;
            }

            if (shopItems[i].getIndex() == id) {
                shopItemsA[i] += amt;

                return;
            }
        }
    }

    /**
     * Method getItemSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getItemSlot(int id) {
        for (int i = 0; i < this.shopItems.length; i++) {
            if (shopItems[i] == null) {
                continue;
            }

            if (shopItems[i].getIndex() == id) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param slot
     * @param amount
     */
    public void addItem(Item item, int slot, int amount) {
        this.shopItems[slot]  = item;
        this.shopItemsA[slot] = amount;
    }



    /**
     * Method getTotalItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTotalItems() {
        return this.shopItems.length;
    }

    /**
     * Method getAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     *
     * @return
     */
    public int getAmount(int slot) {
        return this.shopItemsA[slot];
    }

    /**
     * Method addNewItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param amount
     *
     * @return
     */
    public boolean addNewItem(Item item, int amount, int cost) {
        for (int i = 0; i < this.shopItems.length; i++) {
            if (this.shopItems[i] == null) {
                this.shopItemsA[i]    = amount;
                this.shopItems[i]     = item;
                this.shopItemsCost[i] = cost;
                this.shopItemsType[i] = true;
                this.nextRestock[i]   = Core.currentTime + 2000;

                return true;
            }
        }

        return false;
    }

    /**
     * Method reOrderItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reOrderItems() {
        Item[] temp   = new Item[this.shopItems.length];
        int[]  temp2  = new int[this.shopItems.length];
        int[]  temp3  = new int[this.shopItems.length];
        int[]  temp4  = new int[this.shopItems.length];
        int    offset = 0;

        for (int i = 0; i < this.shopItems.length; i++) {
            if (this.shopItems[i] != null) {
                temp[offset]  = shopItems[i];
                temp2[offset] = shopItemsA[i];
                temp3[offset] = nextRestock[i];
                temp4[offset] = cachedItemCount[i];
                offset++;
            }
        }

        this.shopItems       = temp;
        this.shopItemsA      = temp2;
        this.nextRestock     = temp3;
        this.cachedItemCount = temp4;
    }

    /**
     * Method setItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param items
     */
    public void setItems(Item[] items) {
        this.shopItems = items;
    }

    /**
     * Method setItemAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param shopItems
     */
    public void setItemAmount(int[] shopItems) {
        this.shopItemsA      = shopItems;
        this.cachedItemCount = new int[shopItems.length];
        System.arraycopy(shopItems, 0, cachedItemCount, 0, shopItems.length);
    }

    /**
     * Method getItemCost
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public int getItemCost(int item) {
        for (int i = 0; i < shopItems.length; i++) {
            if ((shopItems[i] != null) && (shopItems[i].getIndex() == item)) {
                return shopItemsCost[i];
            }
        }

        return -1;
    }

    /**
     * Method setItemsCost
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cost
     */
    public void setItemsCost(int[] cost) {
        this.shopItemsCost = cost;
    }

    /**
     * Method getCost
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     *
     * @return
     */
    public int getCost(int slot) {
        return shopItemsCost[slot];
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.shopName = name;
    }

    /**
     * Method getItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     *
     * @return
     */
    public Item getItem(int slot) {
        return this.shopItems[slot];
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return this.shopName;
    }

    /**
     * Method setCurrency
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cur
     */
    public void setCurrency(int cur) {
        currency = cur;
    }

    /**
     * Method getCurrency
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrency() {
        return this.currency;
    }
}
