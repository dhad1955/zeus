package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/07/14
 * Time: 00:06
 */
public class ProduceRequest {
    public static final int[]     NAME_IDS      = {
        132, 133, 134, 135, 136, 137, 275, 316, 317, 318
    };
    public static final int[]     CONFIG_IDS    = {
        755, 756, 757, 758, 759, 760, 120, 185, 87, 90
    };
    private Item[]                products      = new Item[20];
    private String[]              productNames  = new String[20];
    private int[]                 identifiers   = new int[20];
    private int                   caret         = 0;
    private int                   produceAmount = 0;
    private int                   configID;
    private int                   maxToProduce;
    private String                text;
    private Player                player;
    private ProduceActionListener listener;

    /**
     * Constructs ...
     *
     *
     * @param configID
     * @param text
     * @param player
     */
    public ProduceRequest(int configID, String text, Player player) {
        this.configID = configID;
        this.player   = player;
        this.text     = text;
    }

    /**
     * Method setListener
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param listener
     */
    public void setListener(ProduceActionListener listener) {
        this.listener = listener;
    }

    /**
     * Method getMaxToProduce
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaxToProduce() {
        return maxToProduce;
    }

    /**
     * Method setMaxToProduce
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param maxToProduce
     */
    public void setMaxToProduce(int maxToProduce) {
        this.maxToProduce = maxToProduce;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param name
     * @param identifier
     */
    public void addItem(int itemId, String name, int identifier) {
        if (caret == products.length) {
            throw new IndexOutOfBoundsException("Error, to many products max is 20!");
        }

        this.products[caret]       = Item.forId(itemId);
        this.identifiers[caret]    = identifier;
        this.productNames[caret++] = name;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param name
     */
    public void addItem(int itemId, String name) {
        addItem(itemId, name, -1);
    }

    /**
     * Method setAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     */
    public void setAmount(int amount) {
        this.player.getActionSender().sendVar(1363, (28 << 20) | (amount << 26));
        this.produceAmount = amount;
    }

    /**
     * Method onButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void onButton(int id) {
        switch (id) {
        case 5 :
            setAmount(1);

            break;

        case 6 :
            setAmount(5);

            break;

        case 7 :
            setAmount(10);

            break;

        case 20 :
            if (produceAmount > 1) {
                setAmount(produceAmount - 1);
            }

            break;

        case 19 :
            if (produceAmount < maxToProduce) {
                setAmount(produceAmount + 1);
            }

            break;

        case 8 :
            setAmount(maxToProduce);

            break;
        }
    }

    /**
     * Method make
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param component
     */
    public void make(int component) {
        if (listener == null) {
            return;
        }

        if (component == 10) {
            component = 6;
        }

        if(component == 11)
            component = 7;


        if ((caret < component) || (component < 0)) {
            return;
        }

        this.listener.onProduce((this.identifiers[component] != -1)
                                ? this.identifiers[component]
                                : this.products[component].getIndex(), produceAmount, component, player);
    }

    /**
     * Method pack
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void pack() {
        if (this.maxToProduce == 0) {
            throw new RuntimeException("Error max to produce must be set before packing");
        }

        player.getGameFrame().sendString(this.text, 916, 1);
        player.getActionSender().sendBConfig(754, this.configID);
        player.getGameFrame().sendInterface(1, 905, 4, 916);

        for (int i = 0; i < caret; i++) {
            player.getActionSender().sendBConfig(CONFIG_IDS[i], this.products[i].getIndex());
            player.getGameFrame().sendGlobalString(NAME_IDS[i], this.productNames[i]);
        }
        for(int i = caret; i < CONFIG_IDS.length; i++){
            player.getActionSender().sendBConfig(CONFIG_IDS[i], -1);
        }

        AccessMaskBuilder bldr = new AccessMaskBuilder();

        bldr.setRightClickOptionSettings(0, true);
        player.getGameFrame().sendAMask(-1, -1, 916, 8, 0, bldr.getValue());
        player.getGameFrame().sendAMask(-1, -1, 916, 4, 0, bldr.getValue());
        player.getGameFrame().setDialog(905);
        setAmount(getMaxToProduce());
    }
}
