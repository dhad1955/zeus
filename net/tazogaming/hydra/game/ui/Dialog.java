package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 22:37
 */
public class Dialog {
    public static final int
        TYPE_NPC_TALK         = 0,
        TYPE_PLAYER_TALK      = 1,
        TYPE_INFORMATION      = 2,
        TYPE_DIALOG_WITH_ITEM = 3,
        TYPE_SELECT_OPTION_1  = 4,
        TYPE_SELECT_OPTION_2  = 5;

    /** type made: 14/08/26 */
    private int type = -1;

    /** entityId made: 14/08/26 */
    private int entityId = -1;

    /** isEnd made: 14/08/26 */
    private boolean isEnd = false;

    /**
     * Method draw
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    private boolean isHidden = false;

    /** entityEmote made: 14/08/26 */
    private int entityEmote;

    /** lines made: 14/08/26 */
    private String[] lines;

    /** entityName made: 14/08/26 */
    private String entityName;

    /** player made: 14/08/26 */
    private Player player;

    /** scriptToUnlock made: 14/08/26 */
    private Scope scriptToUnlock;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param entityId
     * @param entityEmote
     * @param entityName
     * @param script
     * @param lines
     */
    public Dialog(int type, int entityId, int entityEmote, String entityName, Scope script, String... lines) {
        this(type, entityId, entityEmote, entityName, script, false, lines);
    }

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param entityId
     * @param entityEmote
     * @param entityName
     * @param script
     * @param hidden
     * @param lines
     */
    public Dialog(int type, int entityId, int entityEmote, String entityName, Scope script, boolean hidden,
                  String... lines) {
        if (lines[lines.length - 1].equalsIgnoreCase("true")) {
            String[] lines2 = new String[lines.length - 1];

            System.arraycopy(lines, 0, lines2, 0, lines.length - 1);
            lines = lines2;
            isEnd = true;
        }

        this.type           = type;
        this.entityId       = entityId;
        this.entityEmote    = entityEmote;
        this.entityName     = entityName;
        this.lines          = lines;
        this.isHidden       = hidden;
        this.scriptToUnlock = script;
    }

    /**
     * Method printStopMessage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param lines
     */
    public static void printStopMessage(Player player, String... lines) {
        Scope  s = new Scope();
        Dialog d = new Dialog(Dialog.TYPE_INFORMATION, -1, 0, null, s, lines);

        d.isEnd = true;
        player.getWindowManager().showDialog(d);
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {
        if (this.scriptToUnlock != null) {
            this.scriptToUnlock.release(Scope.WAIT_TYPE_DIALOG);
        }
    }

    /**
     * Method getScriptToUnlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Scope getScriptToUnlock() {
        return scriptToUnlock;
    }

    private void sendString(String str, int id, int index) {
        player.getGameFrame().sendString(str, id, index);
    }

    private void drawMessageBox(Player player) {
        int leng = this.lines.length;

        if (leng > 5) {
            throw new IndexOutOfBoundsException("Error MessageBox can only contain up to 5 lines");
        }

        int interfaceId = 209 + leng;
        int index       = 1;

        for (String string : lines) {
            sendString(string, interfaceId, index);
            index++;
        }

        int typeid = -1;

            switch (leng) {
            case 1 :
                typeid = 2;

                break;

            case 2 :
                typeid = 3;

                break;

            case 3 :
                typeid = 4;

                break;

            case 4 :
                typeid = 5;

                break;

            case 5 :
                typeid = 6;

                break;
            }


        if (typeid != -1 && isHidden) {
            player.getGameFrame().sendHideIComponent(interfaceId, typeid, false);
        } else{
            player.getGameFrame().sendHideIComponent(interfaceId, typeid, true);

        }

        player.getGameFrame().setDialog(interfaceId);
    }

    /**
     * Method drawOption
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void drawOption(Player player) {
        int interfaceId = 224 + ((this.lines.length) * 2);
        int index       = 2;

       sendString(this.entityName, interfaceId, 1);
        for (String string : lines) {
            sendString(string, interfaceId, index);
            index++;
        }

        player.getGameFrame().setDialog(interfaceId);
    }

    /**
     * Method drawNpcChat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void drawNpcChat(Player player) {
        String name = "Unknown";

        if (entityName != null) {
            name = entityName;
        }

        int interfaceId = 240 + lines.length;
        int index       = 4;

        sendString(name, interfaceId, 3);

        for (String s : lines) {
            sendString(s, interfaceId, index++);
        }

        player.getGameFrame().setDialog(interfaceId);
        player.getGameFrame().sendNpcOnInterface(interfaceId, 2, entityId);
        sendInterAnimation(player, 9810, interfaceId, 2);
    }

    /**
     * Method sendInterAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param emoteId
     * @param interId
     * @param childId
     */
    public static void sendInterAnimation(Player player, int emoteId, int interId, int childId) {
        MessageBuilder bldr = new MessageBuilder(86, Packet.Size.Fixed);

        bldr.addInt1(interId << 16 | childId);
        bldr.addLEShortA(emoteId);
        player.dispatch(bldr);
    }

    /**
     * Method drawPlayerChat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void drawPlayerChat(Player player) {
        String name        = player.getUsername();
        int    interfaceId = 63 + lines.length;
        int    index       = 4;

        sendString(name, interfaceId, 3);

        for (String s : lines) {
            sendString(s, interfaceId, index++);
        }

        player.getGameFrame().sendPlayerOnInterface(interfaceId, 2);
        player.getGameFrame().setDialog(interfaceId);
        sendInterAnimation(player, 9810, interfaceId, 2);
    }

    /**
     * Method draw
     * Created on 14/08/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void draw(Player pla) {
        player = pla;

        switch (type) {
        case TYPE_NPC_TALK :
            drawNpcChat(pla);

            break;

        case TYPE_PLAYER_TALK :
            drawPlayerChat(pla);

            break;

        case TYPE_INFORMATION :
            drawMessageBox(pla);

            break;

        case TYPE_SELECT_OPTION_1 :
            drawOption(pla);

            break;
        }
    }

    /**
     * Method isEnd
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isEnd() {
        return isEnd;
    }
}
