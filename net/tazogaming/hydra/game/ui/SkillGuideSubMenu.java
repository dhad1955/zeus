package net.tazogaming.hydra.game.ui;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 18:52
 */
public class SkillGuideSubMenu {
    private int        bindedActionId = -1;
    int                offset         = 0;
    private SkillSub[] subs           = new SkillSub[40];
    private int        skillId;
    private String     name;

    /**
     * Method getBindedActionId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBindedActionId() {
        return bindedActionId;
    }

    /**
     * Method setBindedActionId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bindedActionId
     */
    public void setBindedActionId(int bindedActionId) {
        this.bindedActionId = bindedActionId;
    }

    /**
     * Method getSkillId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSkillId() {
        return skillId;
    }

    /**
     * Method setSkillId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param skillId
     */
    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method addSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     */
    public void addSub(SkillSub s) {
        subs[offset++] = s;
    }

    /**
     * Method getAmounts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getAmounts() {
        int[] amts = new int[subs.length];

        Arrays.fill(amts, 1);

        return amts;
    }

    /**
     * Method length
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        return offset;
    }

    /**
     * Method getSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public SkillSub getSub(int index) {
        return subs[index];
    }

    /**
     * Method getModels
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getModels() {
        int[] models = new int[offset];

        for (int i = 0; i < models.length; i++) {
            models[i] = subs[i].getModelIndex();
        }

        return models;
    }
}
