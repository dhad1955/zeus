package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 00:26
 */
public class HelpMenuParser {
    private static String[][] HELP_MENUS = new String[20][100];

    /**
     * Method parse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     */
    public static void parse(File f) {
        List<File> files = (List<File>) FileUtils.listFiles(f, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

        for (File menu_file : files) {
            try {
                BufferedReader     reader = new BufferedReader(new FileReader(menu_file));
                LinkedList<String> lines  = new LinkedList<String>();
                String             line   = null;
                int                menuId = 0;

                while ((line = reader.readLine()) != null) {
                    if (line.startsWith("define")) {
                        String[] tokens = line.split(" ");

                        if (tokens[1].equalsIgnoreCase("menu_id")) {
                            menuId = Integer.parseInt(tokens[3]);

                            continue;
                        }
                    }

                    if (line.length() == 0) {
                        lines.add(line);

                        continue;
                    }

                    String[] split = splitIntoLine(line, 55);

                    Collections.addAll(lines, split);

                    // builder.append(line+" \\n");
                }

                reader.close();

                String[] str = new String[lines.size()];

                HELP_MENUS[menuId] = lines.toArray(str);
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param menuId
     * @param player
     */
    public static void render(int menuId, Player player) {
        if (HELP_MENUS[menuId] == null) {
            return;
        }

        int start      = 13591;
        int end        = 13690;
        int lineOffset = 0;

        for (int i = start; i < end; i++) {
            if ((lineOffset != HELP_MENUS[menuId].length) && (HELP_MENUS[menuId][lineOffset] != null)) {
                player.getActionSender().changeLine(HELP_MENUS[menuId][lineOffset++], i);
            } else {
                player.getActionSender().changeLine("", i);
            }
        }
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param menu
     */
    public static void render(Player player, MenuBuilder menu) {
        render(menu.getLines(), player);
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lines
     * @param player
     */
    public static void render(String[] lines, Player player) {
        int start      = 16;
        int end        = 100;
        int lineOffset = 0;

        player.getGameFrame().openWindow(275);
        for (int i = start; i < end; i++) {
            if ((lineOffset != lines.length) && (lines[lineOffset] != null)) {
                player.getGameFrame().sendString(lines[lineOffset++], 275, i);
            } else {
                player.getGameFrame().sendString("", 275, i);
            }
        }
    }

    /**
     * Method splitIntoLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param input
     * @param maxCharInLine
     *
     * @return
     */
    public static String[] splitIntoLine(String input, int maxCharInLine) {
        StringTokenizer tok     = new StringTokenizer(input, " ");
        StringBuilder   output  = new StringBuilder(input.length());
        int             lineLen = 0;

        while (tok.hasMoreTokens()) {
            String word = tok.nextToken();

            while (word.length() > maxCharInLine) {
                output.append(word.substring(0, maxCharInLine - lineLen) + "\n");
                word    = word.substring(maxCharInLine - lineLen);
                lineLen = 0;
            }

            if (lineLen + word.length() > maxCharInLine) {
                output.append("\n");
                lineLen = 0;
            }

            output.append(word + " ");
            lineLen += word.length() + 1;

            if (word.contains("@")) {
                lineLen -= 5;
            }
        }

        // output.split();
        // return output.toString();
        return output.toString().split("\n");
    }
}
