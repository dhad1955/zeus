package net.tazogaming.hydra.game.ui;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/01/14
 * Time: 10:32
 */
public class ConstructableItem {
    private int[] items;
    private int[] amts;
    private int   lvl;
    private int   model_id;

    /**
     * Constructs ...
     *
     *
     * @param model
     * @param lvl
     * @param inventory
     */
    public ConstructableItem(int model, int lvl, int... inventory) {
        this.model_id = model;
        this.lvl      = lvl;
        items         = new int[inventory.length / 2];
        amts          = new int[inventory.length / 2];

        for (int i = 0; i < inventory.length; i++) {
            items[i / 2] = inventory[i];
            i++;
            amts[i / 2] = inventory[i];
        }
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getItems() {
        return items;
    }

    /**
     * Method getLvl
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLvl() {
        return lvl;
    }

    /**
     * Method getAmts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getAmts() {
        return amts;
    }

    /**
     * Method getModel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModel() {
        return model_id;
    }
}
