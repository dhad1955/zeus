package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.map.Zone;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 20:37
 */
public class BankInventoryInterface extends GameInterface implements AmountTrigger {

    /**
     * Constructs ...
     *
     */
    public BankInventoryInterface() {
        super(5063);
        setSubInterfaceIds(5064);
    }

    /**
     * Method onButtonClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buttonId
     * @param pla
     */
    @Override
    public void onButtonClick(int buttonId, Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void onOpen(Player pla) {

        /*
         *      pla.getActionSender().sendResetInventory(5064, pla.getInventory().getItems(),
         *            pla.getInventory().getItemsCount());
         */
    }

    /**
     * Method onClose
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void onClose(Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onItemClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param i2
     * @param itemSlot
     * @param clickSlot
     * @param pla
     */
    @Override
    public void onItemClick(int i, int i2, int itemSlot, int clickSlot, Player pla) {
        Item item = pla.getInventory().getItem(itemSlot);

        if (!Zone.canBank(pla)) {
            pla.getActionSender().sendMessage("You're in an area where banking is not allowed.");
            pla.getWindowManager().closeWindow();

            return;
        }

        if (item == null) {
            return;
        }

        switch (clickSlot) {
        case 1 :
            pla.getBank().bankItem(itemSlot, 1);

            break;

        case 2 :
            pla.getBank().bankItem(itemSlot, 5);

            break;

        case 3 :
            pla.getBank().bankItem(itemSlot, 10);

            break;

        case 4 :
            pla.getBank().bankItem(itemSlot, Integer.MAX_VALUE);

            break;

        case 5 :    // bank X
            pla.getWindowManager().requestAmount(itemSlot, this);

            break;
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void tick(Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method moveItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param subId
     * @param pla
     */
    @Override
    public void moveItems(int from, int to, int subId, Player pla) {
        pla.getInventory().moveItems(from, to);
    }

    /**
     * Method amountEntered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param itemSlot
     * @param amount
     */
    @Override
    public void amountEntered(Player pla, int itemSlot, int amount) {
        pla.getBank().bankItem(itemSlot, amount);
    }
}
