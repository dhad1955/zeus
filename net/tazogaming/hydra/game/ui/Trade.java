package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/10/13
 * Time: 00:57
 */
public class Trade {
    public static final int
        TRADE_SECOND_SCREEN                = 334,
        TRADE_FIRST                        = 335;
    private boolean       isTerminated     = false;
    private boolean       isInSecondScreen = false;
    private Player[]      players;
    private TradeHolder[] holders;

    /**
     * Constructs ...
     *
     *
     * @param players
     */
    public Trade(Player... players) {
        this.players = players;
        holders      = new TradeHolder[2];
        holders[0]   = new TradeHolder();
        holders[1]   = new TradeHolder();

        for (int i = 0; i < players.length; i++) {
            players[i].getGameFrame().setTrade(this);
            players[i].setTradeRequest(-1);
            players[i].log("trade opened: " + getOtherPlayer(players[i]).getUsername());
        }

        for (int i = 0; i < players.length; i++) {
            players[i].getGameFrame().openWindow(335);
            if(players[i].getAccount().getIronManMode() > 0)
            {
                declineTrade(players[i]);
                for(Player p :players)
                        p.getGameFrame().close();
                return;
            }
        }


    }

    /**
     * Method init
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void init(Player player) {
        Player otherPlayer = getOtherPlayer(player);

        if (otherPlayer == player) {
            throw new RuntimeException("what the fuck?");
        }

        Item[] initItems = new Item[28];
        int[]  initAmts  = new int[28];

        otherPlayer.getActionSender().sendItems(90, initItems, initAmts, false);
        otherPlayer.getActionSender().sendItems(90, initItems, initAmts, true);
        player.getActionSender().sendItems(90, initItems, initAmts, false);
        player.getActionSender().sendItems(90, initItems, initAmts, true);
        otherPlayer.getGameFrame().sendString("Trading with: " + player.getUsername(), 335, 15);
        player.getGameFrame().sendString(otherPlayer.getUsername(), 335, 22);
        player.getGameFrame().sendString("has " + otherPlayer.getInventory().getFreeSlots(0) + " free inventory slots",
                                         335, 21);
        otherPlayer.getGameFrame().sendString("has " + player.getInventory().getFreeSlots(0) + " free inventory slots",
                335, 21);
        player.getGameFrame().sendString("", 334, 34);
        otherPlayer.getGameFrame().sendString("", 334, 34);
        otherPlayer.getGameFrame().sendString("", 335, 37);
        player.getGameFrame().sendString("", 335, 37);
    }

    /**
     * Method canAccept
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param items
     * @param amounts
     * @param pla
     *
     * @return
     */
    public boolean canAccept(int[] items, int[] amounts, Player pla) {
        Inventory inv         = pla.getInventory();
        int       slotsNeeded = 0;
        int       count       = 0;

        for (int i = 0; i < items.length; i++) {
            if (items[i] == 995) {
                long c = (long)((long)pla.getInventory().getStackCount(995) + (long)amounts[i]);

                if (c > Integer.MAX_VALUE) {
                    return false;
                }
            }

            if (items[i] != -1) {
                count++;

                Item item = Item.forId(items[i]);

                if (!item.isStackable() || (!inv.hasItem(item.getIndex(), 1))) {
                    slotsNeeded++;
                }
            }
        }

        return pla.getInventory().getFreeSlots(0) >= slotsNeeded;
    }

    /**
     * Method accept
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void accept(Player pla) {
        if (isTerminated) {
            return;
        }

        int index = getIndex(pla);

        if (holders[index].currentStatus == 0) {
            holders[index].currentStatus = 1;
        }

        sendStatusChange(pla, holders[index].currentStatus);

        if (!isInSecondScreen && (holders[0].currentStatus == 1) && (holders[1].currentStatus == 1)) {
            isInSecondScreen = true;

            for (Player plr : players) {
                plr.getGameFrame().switchWindow(334);
                holders[getIndex(plr)].currentStatus = 0;
            }
        } else if (isInSecondScreen && (holders[0].currentStatus == 1) && (holders[1].currentStatus == 1)) {
            Player      otherPlayer   = getOtherPlayer(pla);
            int         otherIndex    = getIndex(otherPlayer);
            TradeHolder myTradedItems = holders[otherIndex];

            if (!canAccept(myTradedItems.tradedItems, myTradedItems.tradedItemsCount, pla)) {
                declineTrade(pla, false);
                pla.getGameFrame().close();
                pla.setTradeRequest(-1);
                pla.getActionSender().sendMessage("You don't have enough free spaces to complete this trade.");
                otherPlayer.getActionSender().sendMessage("Other player doesn't have enough free spaces.");

                return;
            }

            if (!canAccept(holders[index].tradedItems, holders[index].tradedItemsCount, otherPlayer)) {
                otherPlayer.getActionSender().sendMessage("You don't have enough free spaces to complete this trade.");
                pla.getActionSender().sendMessage("Other player doesnt have enough free spaces");
                declineTrade(otherPlayer, false);
                otherPlayer.getGameFrame().close();
                pla.getGameFrame().close();

                return;
            }

            isTerminated = true;

            for (Player pl2 : players) {
                pl2.getGameFrame().close();
                pl2.getGameFrame().setTrade(null);
            }

            holders[otherIndex].returnItems(pla);
            holders[index].returnItems(otherPlayer);

            // GameFrame.getInterface(pla.getWindowManager().getCurrentInventoryInterface()).onOpen(pla);
            // GameFramer.getInterface(pla.getWindowManager().getCurrentInventoryInterface()).onOpen(otherPlayer);
        }
    }

    /**
     * Method declineTrade
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void declineTrade(Player pla) {
        declineTrade(pla, true);
    }

    /**
     * Method declineTrade
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param message
     */
    public void declineTrade(Player pla, boolean message) {
        if (isTerminated) {
            return;
        }

        isTerminated = true;

        for (int i = 0; i < 2; i++) {
            holders[i].returnItems(players[i]);
            holders[i] = null;

            if (players[i] == pla) {
                if (message) {
                    pla.getActionSender().sendMessage("Trade declined.");
                    pla.log("trade declined");
                }
            } else {
                players[i].getGameFrame().close();
                players[i].getFocus().unFocus();

                if (message) {
                    players[i].getActionSender().sendMessage("Other player declined the trade.");
                }
            }

            players[i].getGameFrame().setTrade(null);
        }
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public int[] getItems(Player pla) {
        return holders[getIndex(pla)].tradedItems;
    }

    /**
     * Method getAmounts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public int[] getAmounts(Player pla) {
        return holders[getIndex(pla)].tradedItemsCount;
    }

    /**
     * Method getOtherPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public Player getOtherPlayer(Player pla) {
        int index = getIndex(pla);

        if (index == 1) {
            index = 0;
        } else if (index == 0) {
            index = 1;
        }

        return players[index];
    }

    /**
     * Method sendMinorUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemSlot
     * @param pla
     * @param holder
     */
    public void sendMinorUpdate(int itemSlot, Player pla, TradeHolder holder) {
        Player otherPlayer = getOtherPlayer(pla);

        pla.getActionSender().setItem(Item.forId(holder.tradedItems[itemSlot]), holder.tradedItemsCount[itemSlot],
                                      itemSlot, 90, false);
        otherPlayer.getActionSender().setItem(Item.forId(holder.tradedItems[itemSlot]),
                holder.tradedItemsCount[itemSlot], itemSlot, 90, true);
        otherPlayer.getGameFrame().sendString("has " + pla.getInventory().getFreeSlots(0) + " free inventory slots.",
                335, 21);
    }

    /**
     * Method sendMajorUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param holder
     */
    public void sendMajorUpdate(Player pla, TradeHolder holder) {
        Player otherPlayer = getOtherPlayer(pla);

        pla.getActionSender().sendItems(90, holder.tradedItems, holder.tradedItemsCount, false);
        otherPlayer.getActionSender().sendItems(90, holder.tradedItems, holder.tradedItemsCount, true);

        // otherPlayer.getActionSender().sendModelGrid(holder.tradedItems, holder.tradedItemsCount, 3416);
        otherPlayer.getGameFrame().sendString("has " + pla.getInventory().getFreeSlots(0) + " free inventory slots.",
                335, 21);

        /*
         *   otherPlayer.getActionSender().changeLine(pla.getUsername() + " \\n has " + pla.getInventory().getFreeSlots(0)
         *         + " \\n inventory slots.", 23505);
         */
    }

    /**
     * Method sendStatusChange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param status
     */
    public void sendStatusChange(Player pla, int status) {
        Player otherPlayer = getOtherPlayer(pla);
        int    id          = isInSecondScreen
                             ? 334
                             : 335;
        int    id2         = isInSecondScreen
                             ? 34
                             : 37;

        pla.getGameFrame().sendString((status == 1)
                                      ? "Waiting for the other player..."
                                      : "", id, id2);
        otherPlayer.getGameFrame().sendString((status == 1)
                ? "Other player has accepted"
                : "", id, id2);
    }

    private void removePlayerTradeStatus(Player pla) {
        int index = getIndex(getOtherPlayer(pla));

        holders[index].currentStatus = 0;
    }

    /**
     * Method removeItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param slot
     * @param amount
     */
    public void removeItem(Player pla, int slot, int amount) {
        if (isInSecondScreen || isTerminated) {
            return;
        }

        int         myIndex  = getIndex(pla);
        TradeHolder myHolder = holders[myIndex];

        myHolder.currentStatus = 0;
        removePlayerTradeStatus(pla);
        sendStatusChange(pla, myHolder.currentStatus);

        Item i = Item.forId(myHolder.tradedItems[slot]);

        if (i == null) {
            return;
        }

        amount = myHolder.getItemMax(i.getIndex(), amount);
        pla.log("removed interfaces[trade]: " + i.getName());

        if (i.isStackable() || (amount == 1)) {
            myHolder.removeItem(slot, amount);
            pla.getInventory().addItem(i.getIndex(), amount);
            sendMinorUpdate(slot, pla, myHolder);

            return;
        } else {
            myHolder.removeItems(i.getIndex(), amount);
            pla.getInventory().addItem(i.getIndex(), amount);
            sendMajorUpdate(pla, myHolder);
        }
    }

    /**
     * Method tradeItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param slot
     * @param amount
     */
    public void tradeItem(Player pla, int slot, int amount) {
        if (isInSecondScreen || isTerminated) {
            return;
        }

        if (amount <= 0) {
            return;
        }

        int         myIndex  = getIndex(pla);
        TradeHolder myHolder = holders[myIndex];

        myHolder.currentStatus = 0;
        removePlayerTradeStatus(pla);
        sendStatusChange(pla, myHolder.currentStatus);

        Item i = pla.getInventory().getItem(slot);

        if (i == null) {
            return;
        }

        if (!i.isTradeable(pla)) {
            pla.getActionSender().sendMessage("You can't trade this interfaces");

            return;
        }

        amount = pla.getInventory().getItemMax(slot, amount);
        pla.log("Traded interfaces :" + i.getName() + " x " + amount);

        if (i.isStackable() || (amount == 1)) {
            pla.getInventory().deleteItemFromSlot(i, amount, slot);

            int slot2 = myHolder.addItem(i.getIndex(), amount);

            sendMinorUpdate(slot2, pla, myHolder);

            return;
        }

        pla.getInventory().deleteItem(i, amount);
        myHolder.addItem(i.getIndex(), amount);
        sendMajorUpdate(pla, myHolder);
    }

    private int getIndex(Player pla) {
        for (int i = 0; i < players.length; i++) {
            if (players[i] == pla) {
                return i;
            }
        }

        throw new NullPointerException();
    }
}


/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
class TradeHolder {
    protected int[] tradedItems      = new int[28];
    protected int[] tradedItemsCount = new int[28];
    protected int   currentStatus    = 0;

    /**
     * Constructs ...
     *
     */
    public TradeHolder() {
        for (int i = 0; i < 28; i++) {
            tradedItems[i] = -1;
        }
    }

    /**
     * Method returnItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void returnItems(Player pla) {
        for (int i = 0; i < tradedItems.length; i++) {
            if (tradedItems[i] != -1) {
                pla.getInventory().addItem(tradedItems[i], tradedItemsCount[i]);
            }
        }

        Arrays.fill(tradedItems, -1);
        Arrays.fill(tradedItemsCount, -1);
    }

    /**
     * Method getItemSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     *
     * @return
     */
    public int getItemSlot(int itemid) {
        for (int i = 0; i < tradedItems.length; i++) {
            if (tradedItems[i] == itemid) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getItemMax
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param count
     *
     * @return
     */
    public int getItemMax(int id, int count) {
        int curCount = 0;

        for (int i = 0; i < tradedItems.length; i++) {
            if (tradedItems[i] == id) {
                curCount += tradedItemsCount[i];
            }
        }

        if (curCount < count) {
            count = curCount;
        }

        return count;
    }

    /**
     * Method removeItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param amount
     */
    public void removeItem(int slot, int amount) {

        if(amount < 0)
            return;

        this.tradedItemsCount[slot] -= amount;

        if (this.tradedItemsCount[slot] <= 0) {
            this.tradedItems[slot] = -1;
        }
    }

    /**
     * Method removeItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amount
     */
    public void removeItems(int id, int amount) {
        int amt = 0;

        for (int i = 0; i < tradedItems.length; i++) {
            if (amt == amount) {
                return;
            }

            if (tradedItems[i] == id) {
                amt++;
                tradedItems[i]      = -1;
                tradedItemsCount[i] = 0;
            }
        }
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amount
     *
     * @return
     */
    public int addItem(int itemid, int amount) {
        if (amount <= 0) {
            return 0;
        }

        int c    = 0;
        int slot = getItemSlot(itemid);

        if ((slot != -1) && Item.forId(itemid).isStackable()) {
            tradedItemsCount[slot] += amount;

            return slot;
        } else {
            for (int i = 0; i < tradedItems.length; i++) {
                if (tradedItems[i] == -1) {
                    tradedItems[i] = itemid;

                    if (Item.forId(itemid).isStackable() || (amount == 1)) {
                        tradedItemsCount[i] = amount;

                        return i;
                    }

                    c++;
                    tradedItemsCount[i] = 1;

                    if (c == amount) {
                        return i;
                    }
                }
            }
        }

        return -1;
    }
}
