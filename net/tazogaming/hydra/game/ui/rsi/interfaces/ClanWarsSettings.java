package net.tazogaming.hydra.game.ui.rsi.interfaces;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

//~--- JDK imports ------------------------------------------------------------


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/09/14
 * Time: 23:19
 */
public class ClanWarsSettings implements InterfaceAdapter {
    public static int       VICTORY_OPTION_BIT        = 1,
                            TIME_LIMIT_BIT            = 16,
                            STRAGGLERS_BIT            = 256,
                            ITEMS_ON_DEATH_BIT        = 512,
                            MELEE_COMBAT_DISABLED_BIT = 1024,
                            RANGE_COMBAT_DISABLED_BIT = 2048,
                            MAGE_OPTIONS_BIT          = 4096,
                            SUMMONING_DISABLED_BIT    = 16384,
                            FOOD_DISABLED_BIT         = 32768,
                            POTS_DISABLED_BIT         = 65536,
                            PRAYER_DISABLED_BIT       = 131072,
                            ARENA_CHOICE_BIT          = 67108863;
    public static final int
        VICTORY_OPTION                                = 0,
        TIME_LIMIT                                    = 1,
        STRAGGLERS                                    = 2,
        ITEMS_ON_DEATH                                = 3,
        MELEE_DISABLED                                = 4,
        RANGE_DISABLED                                = 5,
        MAGE_OPTIONS                                  = 6,
        SUMMONING_DISABLED                            = 7,
        FOOD_DISABLED                                 = 8,
        POTS_DISABLED                                 = 9,
        PRAYER_DISABLED                               = 10,
        ARENA_CHOICE                                  = 11;
    public static int[]       settings                = new int[12];
    public static final int[] KILLS_TO_WIN            = {
        -1, 25, 50, 100, 200, 400, 750, 1000, 2500, 5000, 10000
    };
    public static final int[] TIME_LIMITS             = {
        Core.getTicksForMinutes(5), Core.getTicksForMinutes(10), Core.getTicksForMinutes(30),
        Core.getTicksForMinutes(60), Core.getTicksForMinutes(90), Core.getTicksForMinutes(120),
        Core.getTicksForMinutes(180), Core.getTicksForMinutes(240), Core.getTicksForMinutes(300),
        Core.getTicksForMinutes(360), Core.getTicksForMinutes(420), Core.getTicksForMinutes(480),
        Core.getTicksForMinutes(520)
    };

    public enum SettingBit {
        VICTORY_OPTION(1), TIME_LIMIT(16), STRAGGLERS(256), ITEMS_ON_DEATH(512), MELEE_DISABLED(1024),
        RANGE_DISABLED(2048), MAGE_OPTIONS(4096), SUMMONING_DISABLED(16384), FOOD_DISABLED(32768),
        POTS_DISABLED(65536), PRAYER_DISABLED(131072), ARENA_CHOICE(67108863);

        /** flag made: 14/09/29 */
        private int flag;

        SettingBit(int flag) {
            this.flag = flag;
        }

        /**
         * Method getFlag
         * Created on 14/09/29
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getFlag() {
            return flag;
        }
    }

    /**
     * Method pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule
     * Created on 14/09/29
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param idx
     * @param settingValue
     */


    /**
     * Method pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule
     * Created on 14/09/29
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param idx
     */


    /**
     * Method getSettingBit
     * Created on 14/09/29
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static int getSettingBit() {
        int bit = 0;

        for (int i = 0; i < settings.length; i++) {
            bit += (SettingBit.values()[i].getFlag() * settings[i]);
        }

        return bit;
    }

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceOpened(Player player) {
        AccessMaskBuilder builder = new AccessMaskBuilder();

        player.getGameFrame().sendIComponentSettings(791, 141, 0, 63, 2);

    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {

        if(button == 143) {
           pla.getGameFrame().getCurrentClanWarsRequest().accept(pla);
            return;
        }
        pla.getGameFrame().getCurrentClanWarsRequest().resetAccepted();
        switch (button) {
        case 116 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule(ITEMS_ON_DEATH);

            break;


            case 141:
                    if(slot == 3){
                        pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(ARENA_CHOICE, 0);
                    }else if(slot == 7){
                        pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(ARENA_CHOICE, 1);
                    }else if(slot == 11){
                        pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(ARENA_CHOICE, 2);
                    }else if(slot == 15){
                        pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(ARENA_CHOICE, 3);

                    }else if(slot == 19){
                        pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(ARENA_CHOICE, 4);

                    }

             break;


        case 134 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule(FOOD_DISABLED);

            break;

        case 120 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule(MELEE_DISABLED);

            break;

        case 128 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule(RANGE_DISABLED);

            break;

        case 130 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule(PRAYER_DISABLED);

            break;

        case 136 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule(POTS_DISABLED);

            break;

        case 132 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().toggleRule(SUMMONING_DISABLED);

            break;

        case 122 :
            int set = pla.getGameFrame().getCurrentClanWarsRequest().getRules().getRule(MAGE_OPTIONS);
            set ++;
            set %= 4;
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(MAGE_OPTIONS, set);
            break;

        case 60 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 1);

            break;

        case 63 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 2);

            break;

        case 66 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 3);

            break;

        case 69 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 4);

            break;

        case 72 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 5);

            break;

        case 75 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 6);

            break;

        case 78 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 7);

            break;

        case 81 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 8);

            break;

        case 84 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 9);

            break;

        case 87 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 10);

            break;

        case 90 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 11);

            break;

        case 100 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 0);

            break;

        case 93 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(TIME_LIMIT, 12);

            break;

        case 20 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 0);

            break;

        case 25 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 1);

            break;

        case 28 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 2);

            break;

        case 31 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 3);

            break;

        case 34 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 4);

            break;

        case 37 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 5);

            break;

        case 40 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 6);

            break;

        case 43 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 7);

            break;

        case 46 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 8);

            break;

        case 49 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 9);

            break;

        case 52 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 10);

            break;

        case 56 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(VICTORY_OPTION, 15);

            break;

        case 106 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(STRAGGLERS, 0);

            break;

        case 109 :
            pla.getGameFrame().getCurrentClanWarsRequest().getRules().setRule(STRAGGLERS, 1);

            break;
        }

    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     *
     * @param from
     * @param to
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick(Player player) {}
}
