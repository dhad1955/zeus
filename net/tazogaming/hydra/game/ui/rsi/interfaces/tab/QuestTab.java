package net.tazogaming.hydra.game.ui.rsi.interfaces.tab;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.questing.Quest;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/11/2014
 * Time: 02:40
 */
public class QuestTab implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceOpened(Player player) {
        sendQuests(player);
    }

    private void sendQuests(Player player) {
        GameFrame gameFrame = player.getGameFrame();

        gameFrame.sendString("Quests", 1071, 3);

        int i     = 0;
        int inter = 1;

        for (Iterator<Quest> questsIterator = Quest.getQuests().values().iterator(); questsIterator.hasNext(); ) {


            Quest quest = questsIterator.next();

            if (player.quest_completed(quest.getQuestId())) {
                gameFrame.sendString("<hcol=" + Text.GREEN + ">" + quest.getQuestName(), 1071, inter);
            } else if (player.questStarted(quest.getQuestId())) {
                gameFrame.sendString("<hcol=" + Text.YEL + ">" + quest.getQuestName(), 1071, inter);
            } else {
                gameFrame.sendString(quest.getQuestName(), 1071, inter);
            }
            if (inter != 1) {
                inter++;
            } else {
                inter = 8;
            }
        }

        while (inter < 100){
            gameFrame.sendString("", 1071, inter);
            inter++;
        }
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceRefreshed(Player player) {
        sendQuests(player);
    }

    /**
     * Method itemClicked
     * Created on 14/08/18
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {

        if(button == 7){
            pla.getGameFrame().setTab(3, 1072, true);
            return;
        }
        int questId = -1;
        if(button == 1)
             questId = 0;
        else
            questId = button - 7;
        Quest quest = Quest.getQuest(questId);
        if(quest == null){
            pla.getActionSender().sendMessage("This quest is not yet available ");
        } else {
            Quest.showJournal(pla, quest);
        }

    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     *
     * @param from
     * @param to
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick(Player player) {}
}
