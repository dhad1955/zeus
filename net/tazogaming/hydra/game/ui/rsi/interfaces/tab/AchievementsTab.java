package net.tazogaming.hydra.game.ui.rsi.interfaces.tab;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/11/2014
 * Time: 23:04
 */
public class AchievementsTab implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceOpened(Player player) {
        Achievement[] achievements = Achievement.ORDERED_ACHIEVEMENTS;

        player.getGameFrame().sendString("<col=B26B24>Completed: " + player.getAchievementsCompletedTotal() + "/"
                                         + Achievement.ACHIEVEMENTS.length + " - <col=" + Text.GREEN + ">"
                                         + GameMath.getPercentFromTotal(player.getAchievementsCompletedTotal(),
                                             Achievement.ACHIEVEMENTS.length) + "%", 1072, 1);

        for (int i = 0; i < achievements.length; i++) {
            if (achievements[i] != null) {
                updateAchievementStatus(achievements[i].getId(), i,
                                        player.getAchievementProgress(achievements[i].getId()), player);
            }
        }
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (button > 7) {
            try {
                Achievement ach = Achievement.ORDERED_ACHIEVEMENTS[button - 8];

                if(ach == null)
                    return;

                Achievement.buildMenu(pla, ach.getId());
            } catch (IndexOutOfBoundsException ee) {
                pla.getActionSender().sendMessage("Coming soon.");
            }
        }
    }

    /**
     * Method updateAchievementStatus
     * Created on 14/11/05
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param achievementID
     * @param progress
     * @param player
     */
    public static void updateAchievementStatus(int achievementID, int progress, Player player) {
        Achievement achievement = Achievement.ACHIEVEMENTS[achievementID];

        player.getGameFrame().sendString("<col=B26B24>" + player.getAchievementsCompletedTotal() + "/"
                                         + Achievement.ACHIEVEMENTS.length + " ( "
                                         + GameMath.getPercentFromTotal(player.getAchievementsCompletedTotal(),
                                             Achievement.ACHIEVEMENTS.length), 1072, 7);
        player.getGameFrame().sendString("<col=B26B24>Completed: " + player.getAchievementsCompletedTotal() + "/"
                                         + Achievement.ACHIEVEMENTS.length + " - <col=" + Text.GREEN + ">"
                                         + GameMath.getPercentFromTotal(player.getAchievementsCompletedTotal(),
                                             Achievement.ACHIEVEMENTS.length) + "%", 1072, 1);

        if (achievement != null) {
            for (int currentOrderCaret = 0; currentOrderCaret < Achievement.ORDERED_ACHIEVEMENTS.length;
                    currentOrderCaret++) {
                if (Achievement.ORDERED_ACHIEVEMENTS[currentOrderCaret].getId() == achievementID) {
                    updateAchievementStatus(achievementID, currentOrderCaret, progress, player);

                    return;
                }
            }
        }
    }

    /**
     * Method updateAchievementStatus
     * Created on 14/11/05
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param achievementID
     * @param orderedId
     * @param progress
     * @param player
     */
    public static void updateAchievementStatus(int achievementID, int orderedId, int progress, Player player) {
        Achievement achievement = Achievement.ACHIEVEMENTS[achievementID];

        if (achievement != null) {
            String data = "Easy";

            if (achievement.getType() == 1) {
                data = "Medium";
            } else if (achievement.getType() == 2) {
                data = "Hard";
            }

            int componentId = 8 + orderedId;

            if (progress == 0) {
                player.getGameFrame().sendString("[" + data + "]" + achievement.getTitle(), 1072, componentId);
            } else if (progress == achievement.getProgress()) {
                player.getGameFrame().sendString("<hcol=" + Text.GREEN + ">[" + data + "]" + achievement.getTitle(),
                                                 1072, componentId);
            } else {
                player.getGameFrame().sendString("<hcol=" + Text.LIGHT_ORANGE + ">[" + data + "]"
                                                 + achievement.getTitle(), 1072, componentId);
            }
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     *
     * @param from
     * @param to
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick(Player player) {}
}
