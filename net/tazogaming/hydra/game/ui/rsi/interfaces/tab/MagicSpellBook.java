package net.tazogaming.hydra.game.ui.rsi.interfaces.tab;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/08/14
 * Time: 00:03
 */
public class MagicSpellBook implements InterfaceAdapter {



    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
       // player.getActionSender().sendVar(1376, getSortConfig(player.getGameFrame().getSpellbook(), player));

        if(player.getGameFrame().getSpellbook() == 193)
            player.getActionSender().sendVar(439, 1);
        else if(player.getGameFrame().getSpellbook() == 192)
            player.getActionSender().sendVar(439, 0);
        else if(player.getGameFrame().getSpellbook() == 430)
            player.getActionSender().sendVar(439, 2);

    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        Player player = pla;
        if(player.getGameFrame().getSpellbook() == 193)
            player.getActionSender().sendVar(439, 1);
        else if(player.getGameFrame().getSpellbook() == 192)
            player.getActionSender().sendVar(439, 0);
        else if(player.getGameFrame().getSpellbook() == 430)
            player.getActionSender().sendVar(439, 2);

        if (pla.getAutoCast() == button) {
           if(pla.getGameFrame().getSpellbook() == 193)
               pla.getActionSender().sendVar(439, 1);
                else
                    pla.getActionSender().sendVar(439, 0);

            pla.getActionSender().sendVar(108, 0);
            pla.setAutocast(-1);

            return;
        }

        if (getSpellAutoCastConfigValue(button, pla.getGameFrame().getSpellbook()) != 0) {
            if(pla.getGameFrame().getSpellbook() == 193)
                pla.getActionSender().sendVar(439, 1);
            else
                pla.getActionSender().sendVar(439, 0);
            pla.getActionSender().sendVar(108, getSpellAutoCastConfigValue(button, pla.getGameFrame().getSpellbook()));
            pla.setAutocast(button);
            pla.setRequestedSpell(button);
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}

    /**
     * Method getSpellAutoCastConfigValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param autoCastSpell
     * @param spellBook
     *
     * @return
     */

    public int getSortConfig(int spellBook, Player player){
        Account account = player.getAccount();
        if(!account.hasVar("sb_sortlvl")) {
            account.setSetting("sb_sortlvl", 0, true);
            account.setSetting("sb_sortcmb", 0, true);
            account.setSetting("sb_showtele", 1, true);
            account.setSetting("sb_showmisc", 1, true);
            account.setSetting("sb_showcmb", 1, true);
        }
        int show_tele = account.getInt32("sb_showtele");
        int show_cmb = account.getInt32("sb_showcmb");
        player.getActionSender().sendMessage("show_tele: "+show_tele+" show_cmb: "+show_cmb);
        if(spellBook == 193) {
            int sortId = 2;
            int cfg = 1 << 9 | (show_cmb == 1 ? 0 : 1 << 15)
                    | (show_tele == 1 ? 0 : 1 << 16 | 2 << 3);
            return cfg;
        }

        return 0;

    }
    public int getSpellAutoCastConfigValue(int autoCastSpell, int spellBook) {
        if (spellBook == 192) {
            switch (autoCastSpell) {
            case 25 :
                return 3;

            case 28 :
                return 5;

            case 30 :
                return 7;

            case 32 :
                return 9;

            case 34 :
                return 11;    // air bolt

            case 39 :
                return 13;    // water bolt

            case 42 :
                return 15;    // earth bolt

            case 45 :
                return 17;    // fire bolt

            case 49 :
                return 19;    // air blast

            case 52 :
                return 21;    // water blast

            case 58 :
                return 23;    // earth blast

            case 63 :
                return 25;    // fire blast

            case 66 :         // Saradomin Strike
                return 41;

            case 67 :         // Claws of Guthix
                return 39;

            case 68 :         // Flames of Zammorak
                return 43;

            case 70 :
                return 27;    // air wave

            case 73 :
                return 29;    // water wave

            case 77 :
                return 31;    // earth wave

            case 80 :
                return 33;    // fire wave

            case 84 :
                return 47;

            case 87 :
                return 49;

            case 89 :
                return 51;

            case 91 :
                return 53;

            case 99 :
                return 145;

            default :
                return 0;
            }
        } else if (spellBook == 193) {
            switch (autoCastSpell) {
            case 28 :
                return 63;

            case 32 :
                return 65;

            case 24 :
                return 67;

            case 20 :
                return 69;

            case 30 :
                return 71;

            case 34 :
                return 73;

            case 26 :
                return 75;

            case 22 :
                return 77;

            case 29 :
                return 79;

            case 33 :
                return 81;

            case 25 :
                return 83;

            case 21 :
                return 85;

            case 31 :
                return 87;

            case 35 :
                return 89;

            case 27 :
                return 91;

            case 23 :
                return 93;

            case 36 :
                return 95;

            case 37 :
                return 99;

            case 38 :
                return 97;

            case 39 :
                return 101;

            default :
                return 0;
            }
        } else {
            return 0;
        }
    }
}
