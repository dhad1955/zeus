package net.tazogaming.hydra.game.ui.rsi.interfaces.tab;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/07/14
 * Time: 11:56
 */
public class SkillsTabAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {}

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /*
     *
     */

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        int skillID  = -1,
            configID = -1;

        switch (button) {
        case 200 :
            skillID  = 0;
            configID = 1;

            break;

        case 11 :
            skillID = configID = 2;

            break;

        case 28 :
            skillID  = 1;
            configID = 5;

            break;

        case 52 :
            skillID  = 4;
            configID = 3;

            break;

        case 76 :
            skillID  = 5;
            configID = 7;

            break;

        case 93 :
            skillID  = 6;
            configID = 4;

            break;

        case 110 :
            configID = 12;
            skillID  = 20;

            break;

        case 134 :
            skillID  = 21;
            configID = 22;

            break;

        case 193 :
            configID = 6;
            skillID  = 3;

            break;

        case 19 :
            skillID  = 16;
            configID = 8;

            break;

        case 60 :
            skillID  = 17;
            configID = 10;

            break;

        case 84 :
            skillID  = 12;
            configID = 11;

            break;

        case 101 :
            skillID  = 9;
            configID = 19;

            break;

        case 118 :
            skillID  = 18;
            configID = 20;

            break;

        case 142 :
            skillID  = 22;
            configID = 23;

            break;

        case 186 :
            skillID  = 14;
            configID = 13;

            break;

        case 179 :
            skillID  = 13;
            configID = 14;

            break;

        case 44 :
            skillID  = 10;
            configID = 15;

            break;

        case 68 :
            skillID  = 7;
            configID = 16;

            break;

        case 172 :
            skillID  = 11;
            configID = 17;

            break;

        case 165 :
            skillID  = 8;
            configID = 18;

            break;

        case 126 :
            skillID  = 19;
            configID = 21;

            break;

        case 150 :
            skillID  = 23;
            configID = 24;

            break;

        case 158 :
            skillID  = 25;
            configID = 25;

            break;
        }

        if ((skillID == -1) || (configID == -1)) {
            return;
        }

        switch (contextSlot) {
        case 0 :
            pla.getAccount().set(965, configID, true);
            pla.getGameFrame().openWindow(499);

            break;
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
