package net.tazogaming.hydra.game.ui.rsi.interfaces.tab;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.runtime.GameEngine;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/07/14
 * Time: 16:47
 */
public class StatisticsInterface implements InterfaceAdapter {
    private static void statsLine(StringBuffer bldr, String info, String pts) {
        bldr.append("<col=6BB224>" + info + ":  <col=FCFCF7>" + pts);
        bldr.append("<br>");
    }

    /*
     * int rank = p.readByte();
     * int hr = p.readByte();
     * int min = p.readByte();
     * int playtimeDays = p.readShort();
     * int playtimeHours = p.readByte();
     * int pvpPoints = p.readInt();
     * int pvpkills = p.readShort();
     * int pvpDeaths = p.readShort();
     * int kdr       = p.readString();
     * String slayerName = p.readString();
     * int slayerKillsRemaining = p.readShort();
     * int slayerPoints = p.readShort();
     */
    public static final double kdr(int kills, int deaths) {
        return (double) kills / (double) deaths;
    }

    /**
     * Method rebuildStats
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void rebuildStats(Player player) {
        MessageBuilder msg = new MessageBuilder(124, Packet.Size.VariableShort);

        msg.addByte(player.getRights());

        Calendar calendar = GregorianCalendar.getInstance();       // creates a new calendar puresLeaderboard
        int      hr       = calendar.get(Calendar.HOUR_OF_DAY);    // gets hour in 24h format
        int      min      = calendar.get(Calendar.MINUTE);

        msg.addByte(hr);
        msg.addByte(min);
        msg.addInt(player.getAccount().getPlayTime());
        msg.addInt(player.getPoints(Points.PK_POINTS_2));
        int kills = player.getAccount().getKills(Account.WILDERNESS);
        int deaths = player.getAccount().getDeaths(Account.WILDERNESS);
        if(kills < 0 )
            kills  = 0;
        if(deaths < 0)
            deaths = 0;

        msg.addShort(kills);
        msg.addShort(deaths);//player.getSettings().getInt32("deaths"));

        double kdr = kdr(kills, deaths);

        if (kdr == Double.NaN) {
            kdr = 0.0;
        }
        String str = Double.toString(kdr);
        if(str.toLowerCase().equals("nan"))
            str = "0.0";

        if(player.getRights() >= Player.ADMINISTRATOR){
            str += "<br><br>Cycle time: <col=fffff0>"+ GameEngine.lastCycle +"ms<br>" ;
        }
        if(BossTournament.bossId != -1)
        str+= "<br><col=FF5C33>Boss challenge:</col> "+NpcDef.FOR_ID(BossTournament.bossId).getName();
        msg.addRS2String(str);



        String npcName = "N/a";

        if (player.hasSlayerTask()) {

            if (NpcDef.FOR_ID(player.getSlayerTask().getTaskId()) == null) {
                npcName = "Npc not defined";
            } else {
                npcName = NpcDef.FOR_ID(player.getSlayerTask().getTaskId()).getName();
            }
        }

        if(npcName == null)
            npcName = "N/a";

        if(player.getSlayerTask() != null && player.getSlayerTask().isDuoTask()){
            npcName += "<br><col=FF5C33>Partner:</col>: "+player.getSlayerTask().getPartnerName()+" <br>";
        }


        msg.addRS2String(npcName);

        if (player.hasSlayerTask()) {
            msg.addShort(player.getSlayerTask().getKillsToComplete() - player.getSlayerTask().getTotalKills());
        } else {
            msg.addShort(65535);
        }

        msg.addShort(player.getPoints(Points.SLAYER_POINTS));
        player.dispatch(msg);
    }

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        rebuildStats(player);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {
        if (player.getAccount().getInt32("p") != World.getWorld().getPlayers().size()) {
            player.getAccount().setSetting("p", World.getWorld().getPlayers().size());
            player.getGameFrame().sendString("Players online: <col=ffffff>" + (World.getWorld().getPlayers().size()+ Config.DUMMY_PLAYERS),
                                             930, 10);
        }
    }
}
