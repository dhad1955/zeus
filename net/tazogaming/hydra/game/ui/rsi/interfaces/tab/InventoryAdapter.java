package net.tazogaming.hydra.game.ui.rsi.interfaces.tab;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.Sounds;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.object.DwarfMultiCannon;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.barrows.BarrowsGame;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.castlewars.CastlewarsTeam;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRules;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.game.pets.BossPet;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.ui.WindowManager;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.hunter.HuntingController;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/07/14
 * Time: 17:21
 */
public class InventoryAdapter implements InterfaceAdapter {
    public static final long SLOT = Text.longForName("slot");

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void interfaceOpened(Player pla) {
        AccessMaskBuilder inv = new AccessMaskBuilder();

        pla.getInventory().flushInventory();
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method handleItemClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemSlot
     * @param plr
     */
    public void handleItemClick(final int itemSlot, int itemID, Player plr) {
        Item i = plr.getInventory().getItem(itemSlot);

        if (i == null) {
            return;
        }

        if (plr.isActionsDisabled()) {
            return;
        }

        if(i.getIndex() != itemID)
            return;

        if(BossPets.getByItem(i.getIndex()) != null){
            BossPet pet = BossPets.getByItem(i.getIndex());
            if(plr.getCurrentFamiliar() != null){
                plr.getCurrentFamiliar().unlink();
            }
            plr.setCurrentFamiliar(new NPC(pet.getPetId(), plr));
            World.getWorld().registerNPC(plr.getCurrentFamiliar());
            plr.getTimers().setTime(TimingUtility.SUMMON_TIMER, 720);
        }

        plr.terminateScripts();
        plr.terminateActions();

        plr.log("click item: "+i.getIndex());

        if (i.getPotionHandler() != null) {

            handlePotion(itemSlot, plr);


            return;
        }

        if(i.getIndex() == 4053){
            if(CastleWarsEngine.getSingleton().isPlaying(plr)){
                CastlewarsTeam team = CastleWarsEngine.getSingleton().getTeam(plr);
                if(team != null && team.setupBarricade(plr))
                    plr.getInventory().deleteItem(4053, 1);
            }
            return;
        }

        if(i.getIndex() == DwarfMultiCannon.ITEM_BASE) {
            DwarfMultiCannon cannon = DwarfMultiCannon.setupCannon(plr);
            if(cannon != null) {
                plr.getAccount().setObject("cannon", cannon, false);
            }
            return;

        }

        if (i.getIndex() == 952) {
            if (BarrowsGame.onDig(plr)) {
                return;
            }
        }


        if (HuntingController.onItemClick(itemSlot, plr)) {
            return;
        }

        if (i.getIndex() == 11169) {
            if (plr.getAccount().getGame() == null) {
                Dialog.printStopMessage(plr, "You don't have a target active",
                                        "Speak to the stranger in edgeville to get a new targt");
            } else {
                plr.getAccount().getGame().build_interface(plr);
            }

            return;
        }

        if (i.getIndex() == 4277) {
            if (plr.getAccount().getGame() == null) {
                Dialog.printStopMessage(plr, "You don't have a target active",
                                        "Speak to the stranger in edgeville to get a new targt");
            } else {
                plr.getAccount().getGame().trackTarget();
            }

            return;
        }

        if ((i.getIndex() == 4053) && (plr.getZombiesGame() != null)) {
            plr.getZombiesGame().setupBarricade(plr);
            plr.getInventory().deleteItem(i, 1);

            return;
        }

        if (ClueManager.showClue(plr, i)) {
            return;
        }

        plr.getAccount().putSmall(Account.LAST_USED_SLOT, itemSlot);

        if (i.getIndex() == 952) {
            plr.getAnimation().animate(830);
            plr.getTickEvents().add(new PlayerTickEvent(plr, 2, true, false) {
                @Override
                public void doTick(Player owner) {
                    owner.getAnimation().animate(65535);

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
            plr.getActionSender().sendMessage("You dig your spade into the ground.");

            if (World.getWorld().getScriptManager().directTrigger(plr, null, Trigger.DIG, plr.getX(), plr.getY())) {
                return;
            } else {
                plr.getActionSender().sendMessage("You find nothing.");
            }
        }


        if(plr.isStunned()) {
            return;
        }

        ScriptVariableInjector slotInjector = new ScriptVariableInjector() {
            @Override
            public void injectVariables(Scope scope) {
                scope.declareVar(SLOT, new ScriptVar(SLOT, itemSlot));
            }
        };

     //   plr.getActionSender().sendMessage("forcing trigger: "+ Core.currentTime);
        if (World.getWorld().getScriptManager().forceTrigger(plr, Trigger.ITEM_CLICK, slotInjector, i.getIndex())) {
            return;
        } else {
            plr.getActionSender().sendMessage("Nothing interesting happens...");
        }
    }

    private void handlePotion(int itemSlot, Player pla) {
        Item potion   = pla.getInventory().getItem(itemSlot);
        int  curDose  = potion.getPotionHandler().getRemaining(potion.getIndex());
        Duel cur_duel = pla.getGameFrame().getDuel();


        if(pla.getTimers().getTimeRemaining(TimingUtility.EATING_COOLDOWN_3) == 2)
            return;

        if(pla.getTimers().getFoodProtTime() == 2)
            return;

        if ((cur_duel != null) && (cur_duel.getStatus() == Duel.STATUS_IN_FIGHT)) {
            if (cur_duel.setting(Duel.NO_DRINKS)) {
                pla.getActionSender().sendMessage("Potion drinking has been disabled during this duel.");

                return;
            }
        }

        if (pla.getGetCurrentWar() != null) {
            if (pla.getGetCurrentWar().getGameRules().settingActive(ClanWarRules.POTS_DISABLED)) {
                pla.getActionSender().sendMessage("Potions are not allowed in this war.");

                return;
            }
        }

        if (pla.getTimers().timerActive(TimingUtility.POTION_DELAY_TIMER)) {
            return;
        }

        pla.getCombatAdapter().reset();
        pla.getRequest().close();

        pla.getTimers().setTime(TimingUtility.POTION_DELAY_TIMER, 2);
        pla.getAnimation().animate(829);

        // pla.getCombatHandler().delay(3);
        curDose--;

        Item newItem = null;

        if (curDose != -1) {
            newItem = Item.forId(potion.getPotionHandler().getDose(curDose));
        } else {
            newItem = Item.forId(229);
        }

        Sounds.playSingleSound(2401, pla);

        String name = potion.getName().substring(0, potion.getName().indexOf("("));

        pla.getActionSender().sendMessage("You drink some of your "
                                          + potion.getName().substring(0, potion.getName().indexOf("(")));

        if (curDose == -1) {
            pla.getActionSender().sendMessage("You have finished your " + name);
        } else {
            pla.getActionSender().sendMessage("You now have " + (curDose + 1) + " doses left.");
        }

        potion.getPotionHandler().getDoseHandler().drink(pla);
        pla.getInventory().replaceItem(itemSlot, newItem.getIndex());
    }

    private void handleWieldOrAction(final int slot, int itemId, Player player) {

        if (player.getInventory().getItem(slot) == null) {
            return;
        }

        if(player.getInventory().getItem(slot).getIndex() != itemId)
            return;


        if (World.getWorld().getScriptManager().directTrigger(player, new ScriptVariableInjector() {
            @Override
            public void injectVariables(Scope scope) {
                scope.declare(Text.longForName("slot"), slot);
            }
        }, Trigger.ITEM_ACTION2, player.getInventory().getItem(slot).getIndex())) {
            return;
        }
        player.log("Wield item: "+player.getInventory().getItem(slot).getIndex());
        player.getEquipment().wieldItem(slot);

    }

    private void handleDrop(int itemSlot, Player pla) {

        if (World.getWorld().getScriptManager().isBlocked(pla, Trigger.DROP_ITEM,
                pla.getInventory().getItem(itemSlot).getIndex())) {
            return;
        }

        if (ClueManager.isClue(pla.getInventory().getItem(itemSlot).getIndex())) {
            pla.getInventory().deleteItemFromSlot(Item.forId(1), Integer.MAX_VALUE, itemSlot);
            pla.getAccount().removeVar("clue_level");
            pla.getAccount().removeVar("clue_data");
            pla.getActionSender().sendMessage("Your clue scroll crumbles.");

            return;
        }

        if (pla.getInventory().getItem(itemSlot).isLend()) {
            pla.getAccount().removeRented(pla.getInventory().getItem(itemSlot).getIndex());
            pla.getInventory().deleteItemFromSlot(null, 1, itemSlot);

            return;
        }

        FloorItem fla = new FloorItem(pla.getInventory().getItem(itemSlot).getIndex(),
                                      pla.getInventory().getCount(itemSlot), pla.getX(), pla.getY(),
                                      pla.getLocation().getHeight(), pla);


        if(pla.isInWilderness()){
            if(Item.forId(fla.getItemId()).isTradeable()){
                fla.globalise();

            }
        }

        if (pla.getDungeon() != null) {
            fla.setDontRemove(true);
            pla.getDungeon().registerFloorItem(fla);
        }

        try {
            pla.log("Dropped item: " + pla.getInventory().getItem(itemSlot).getName());
        } catch (Exception er) {}

        pla.getInventory().deleteItemFromSlot(Item.forId(1), Integer.MAX_VALUE, itemSlot);
        pla.terminateActions();
        pla.terminateScripts();
    }

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, final int slot, int clickSlot, Player player) {
        Item   item = Item.forId(itemId);
        Player plr  = player;

        if(player.getInventory().getItem(slot) == null)
            return;


        player.log("Clicked item: "+item.getName()+" "+slot+" "+clickSlot);


        switch (clickSlot) {
        case 0 :
            handleItemClick(slot, itemId, player);

            break;

        case 2 :
            if (item.isDungItem()) {
                if (plr.getDungeon() != null) {
                    if (plr.getCurStat(24) < 40) {
                        plr.getActionSender().sendMessage("You need a dungeoneering level of atleast 40 to this.");

                        return;
                    }


                    int needed = player.getEquipment().getBindedItemsCount() + 1;
                    int pointsReq = needed * 1000;

                    if (plr.getPoints(Points.DUNGEONEERING_POINTS) < pointsReq) {
                        plr.getActionSender().sendMessage(
                            "You need atleast "+pointsReq+" dungeoneering points to bind this item.");

                         return;
                    }

                    int bindSlot = item.getWieldSlot();

                    if ((bindSlot == 3) && player.getEquipment().isBinded(Equipment.SHIELD)
                            && ((item.getWeaponHandler() != null) && item.getWeaponHandler().isTwoHanded())) {
                        player.getEquipment().bind(0, Equipment.SHIELD);
                    } else if ((bindSlot == Equipment.SHIELD) && player.getEquipment().isBinded(3)
                               && (Item.forId(
                                   player.getEquipment().getBindedItems()[3]).getWeaponHandler().isTwoHanded())) {
                        player.getEquipment().bind(0, 3);
                    }

                    int count = 1;

                    if (plr.getEquipment().isBinded(bindSlot)) {
                        count--;
                    }

                    if (Dungeon.checkBind(player, plr.getEquipment().getBindedItemsCount() + count,
                                          plr.getCurStat(24))) {
                        plr.getActionSender().sendMessage(
                                item.getName() + " is now binded, you will start with this every time in dungeoneering.");
                      if(pointsReq > 0)
                        plr.addPoints(Points.DUNGEONEERING_POINTS, -pointsReq);
                        plr.getEquipment().bind(item.getIndex(), item.getWieldSlot());
                    }
                }
            } else {
                World.getWorld().getScriptManager().directTrigger(player, new ScriptVariableInjector() {
                    @Override
                    public void injectVariables(Scope scope) {
                        scope.declare(Text.longForName("slot"), slot);
                    }
                }, Trigger.ITEM_ACTION2, player.getInventory().getItem(slot).getIndex());
            }

            break;

            case 4 :
                if(item.getIndex() == 11283){
                    if(player.getAccount().hasVar("dfs_charges")){
                        player.getAccount().removeVar("dfs_charges");
                        player.getActionSender().sendMessage("Dfs emptied.");
                    }else{
                        player.getActionSender().sendMessage("Your DFS is already empty.");
                    }
                }
            break;
        case 1 :
            handleWieldOrAction(slot, itemId, player);

            break;

        case 6 :
            if (Summoning.pouchClick(player, slot)) {
                return;
            }

            break;

        case 7 :
            handleDrop(slot, player);

            break;

        case 3 :
            if (item.isDungItem()) {
                if (plr.getDungeon() != null) {
                    if (plr.getCurStat(24) < 40) {
                        plr.getActionSender().sendMessage("You need a dungeoneering level of atleast 40 to this.");

                        return;
                    }

                    if (plr.getPoints(Points.DUNGEONEERING_POINTS) < 10) {
                        plr.getActionSender().sendMessage(
                            "You need atleast 10 dungeoneering points to bind this weapon.");

                        return;
                    }

                    plr.getActionSender().sendMessage(
                        item.getIndex()
                        + " is now binded, you will start with this weapon every time in dungeoneering.");
                    plr.addPoints(Points.DUNGEONEERING_POINTS, -10);
                    plr.getAccount().setSetting("dung_bind", item.getIndex(), true);
                }
            } else {
                if (World.getWorld().getScriptManager().forceTrigger(plr, Trigger.ITEM_ACTION_4, null,
                        item.getIndex())) {
                    return;
                } else {
                    plr.getActionSender().sendMessage("Nothing interesting happens...");
                }
            }

            break;
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param pla
     */
    @Override
    public void itemsSwitched(int from, int to, Player pla) {
        to -= 28;
        pla.getInventory().moveItems(from, to);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
