package net.tazogaming.hydra.game.ui.rsi.interfaces;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.potion.Potion;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Created by Dan on 03/01/2016.
 */
public class HPOrb implements InterfaceAdapter {
    @Override
    public void interfaceOpened(Player player) {

    }

    @Override
    public void interfaceClosed(Player player) {

    }

    @Override
    public void interfaceRefreshed(Player player) {

    }

    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {


    }

    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
            if(button == 2){
                int[] poisons = { 179,175,2446, 185,183,181,2448};
                for(Integer i : poisons){
                    if(pla.getInventory().hasItem(i, 1)){
                        pla.getGameFrame().itemClick(149,i,  pla.getInventory().getItemSlot(Item.forId(i)), 0, pla);
                        return;
                    }
                }
            }
    }

    @Override
    public void itemsSwitched(int from, int to, Player player) {

    }

    @Override
    public void tick(Player player) {

    }
}
