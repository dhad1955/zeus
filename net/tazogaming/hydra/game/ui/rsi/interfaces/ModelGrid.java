package net.tazogaming.hydra.game.ui.rsi.interfaces;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.CS2Call;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/07/14
 * Time: 21:51
 */
public class ModelGrid {
    private int               widthType    = 0;
    private int               heightType   = 0;
    private int               componentId  = 0;
    private int               interfaceId  = -1;
    private int               childId      = -1;
    private String[]          options      = new String[6];
    private int               optionsCaret = 0;
    private AccessMaskBuilder accessMask   = new AccessMaskBuilder();

    /**
     * Constructs ...
     *
     *
     * @param comp
     * @param inter
     * @param child
     */
    public ModelGrid(int comp, int inter, int child) {
        this(comp, inter, child, 7, 4);
    }

    // modele g

    /**
     * Constructs ...
     *
     *
     * @param componentId
     * @param interfaceId
     * @param childId
     * @param widthType
     * @param heightType
     */
    public ModelGrid(int componentId, int interfaceId, int childId, int widthType, int heightType) {
        this.widthType   = widthType;
        this.componentId = componentId;
        this.heightType  = heightType;
        this.interfaceId = interfaceId;
        this.childId     = childId;
    }

    /**
     * Method addOption
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param option
     */
    public void addOption(String option) {
        this.accessMask.setRightClickOptionSettings(optionsCaret, true);
        this.options[optionsCaret++] = option;
    }

    /**
     * Method getCS2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public CS2Call getCS2() {
        CS2Call tmp = new CS2Call(150);

        for (int i = optionsCaret - 1; i >= 0; i--) {
            tmp.addString(options[i]);
        }

        tmp.addInt(-1);
        tmp.addInt(0);    // unsure
        tmp.addInt(widthType);
        tmp.addInt(heightType);
        tmp.addInt(componentId);
        tmp.addInt(interfaceId << 16 | childId);

        return tmp;
    }
}
