package net.tazogaming.hydra.game.ui.rsi.interfaces.ge;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.PlayerGrandExchange;
import net.tazogaming.hydra.util.SystemUpdate;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/01/2015
 * Time: 11:26
 */
public class GrandExchange implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceOpened(Player player) {
        if(SystemUpdate.isActive() && SystemUpdate.getTimeRemaining() < 200){
            player.getActionSender().sendMessage("You cannot use the grand exchange during a system update.");
            player.getGameFrame().close();
            return;
        }

        player.getAccount().getGrandExchange().closeOfferScreen();
        player.getActionSender().sendVar(1112, -1);
        player.getGameFrame().sendIComponentSettings(105, 206, -1, -1, 6);
        player.getGameFrame().sendIComponentSettings(105, 208, -1, -1, 6);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceClosed(Player player) {

        // player.getGameFrame().sendCloseInterface(752, 7);
        // player.getGameFrame().sendHideIComponent(752, 7, false);
        player.getActionSender().sendClientScript(new CS2Call(571));
    }

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        PlayerGrandExchange exchange = player.getAccount().getGrandExchange();
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if(SystemUpdate.isActive() && SystemUpdate.getTimeRemaining() < 200){
            pla.getActionSender().sendMessage("You cannot use the grand exchange during a system update.");
            return;
        }

        int     ge_slot = -1;
        boolean buy     = false;

        if (button == 19) {
            ge_slot = 0;
        }

        if (button == 35) {
            ge_slot = 1;
        }

        if (button == 51) {
            ge_slot = 2;
        }

        if (button == 70) {
            ge_slot = 3;
        }

        if (button == 89) {
            ge_slot = 4;
        }

        // buy
        if (button == 108) {
            ge_slot = 5;
        } else if (button == 31) {
            ge_slot = 0;
            buy     = true;
        } else if (button == 32) {
            ge_slot = 0;
            buy     = false;
        } else if (button == 47) {
            buy     = true;
            ge_slot = 1;
        } else if (button == 48) {
            buy     = false;
            ge_slot = 1;
        } else if (button == 63) {
            buy     = true;
            ge_slot = 2;
        } else if (button == 64) {
            buy     = false;
            ge_slot = 2;
        } else if (button == 82) {
            buy     = true;
            ge_slot = 3;
        } else if (button == 83) {
            ge_slot = 3;
            buy     = false;
        } else if (button == 101) {
            ge_slot = 4;
            buy     = true;
        } else if (button == 102) {
            ge_slot = 4;
            buy     = false;
        } else if (button == 120) {
            ge_slot = 5;
            buy     = true;
        } else if (button == 121) {
            ge_slot = 5;
            buy     = false;
        }

        final PlayerGrandExchange exchange = pla.getAccount().getGrandExchange();

        if (ge_slot != -1) {
            pla.getAccount().getGrandExchange().openOfferScreen(ge_slot, buy);
        }

        if (button == 206) {
            if ((exchange.getCurOfferScreen() >= 0) && (exchange.getOffer(exchange.getCurOfferScreen()) != null)) {
                exchange.getOffer(exchange.getCurOfferScreen()).withdrawItems(pla, Integer.MAX_VALUE, contextSlot == 0);
                exchange.getOffer(exchange.getCurOfferScreen()).checkEmpty();
            }
        }

        if (button == 208) {
            if ((exchange.getCurOfferScreen() >= 0) && (exchange.getOffer(exchange.getCurOfferScreen()) != null)) {
                exchange.getOffer(exchange.getCurOfferScreen()).withdrawCash(pla);
                exchange.getOffer(exchange.getCurOfferScreen()).checkEmpty();
            }
        }

        if (button == 177) {
            pla.getGameFrame().requestAmount("Please enter an amount", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    pla.getAccount().getGrandExchange().setPPI(amount);
                }
            });
        }

        if (button == 179) {
            int ppi = (int) Math.ceil(pla.getAccount().getGrandExchange().getCurrentPPI() * 1.05);

            pla.getAccount().getGrandExchange().setPPI(ppi);
        }

        if (button == 181) {
            int ppi = (int) (Math.floor(pla.getAccount().getGrandExchange().getCurrentPPI() * 0.95));

            if (ppi >= 0) {
                pla.getAccount().getGrandExchange().setPPI(ppi);
            }
        }

        if (button == 160) {
            exchange.increaseOffer(1);
        } else if (button == 162) {
            exchange.increaseOffer(10);
        } else if (button == 164) {
            exchange.increaseOffer(100);
        } else if (button == 166) {
            if (exchange.isBuying()) {
                exchange.increaseOffer(1000);
            } else if (exchange.getCurrentGEItem() != -1) {
                exchange.setCurrOfferQuantity(pla.getInventory().getMaxItem(exchange.getCurrentGEItem()));

                int q1 = pla.getInventory().getMaxItem(exchange.getCurrentGEItem());
                int q2 = pla.getInventory().getMaxItem(exchange.getCurrentGEItem() + 1);

                if (Item.forId(exchange.getCurrentGEItem() + 1).isNote()) {
                    q1 += q2;
                }

                exchange.setCurrOfferQuantity(q1);
            }
        } else if (button == 155) {
            if (exchange.getQuantity() > 0) {
                exchange.setCurrOfferQuantity(exchange.getQuantity() - 1);
            }
        } else if (button == 157) {
            if (exchange.getQuantity() < Integer.MAX_VALUE) {
                exchange.setCurrOfferQuantity(exchange.getQuantity() + 1);
            }
        } else if (button == 168) {
            pla.getGameFrame().requestAmount("Please enter an amount", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    if ((amount > -1) && (amount < Integer.MAX_VALUE)) {
                        exchange.setCurrOfferQuantity(amount);
                    }
                }
            });
        } else if (button == 190) {
            if (exchange.isBuying()) {
                CS2Call C = new CS2Call(570);

                C.addString("Grand exchange item search");
                pla.getGameFrame().sendInterface(1, 752, 7, 389);
                pla.getActionSender().sendClientScript(C);
            } else {
                pla.getActionSender().sendMessage("Is not buying? ");
            }
        }

        if (button == 200) {
            exchange.abortOffer(exchange.getCurOfferScreen());
        }

        if (button == 128) {
            exchange.closeOfferScreen();
        }

        if (button == 186) {
            exchange.submitOffer();

            // submit
        }

        if (button == 175) {
            if (exchange.getCurrentGEItem() != -1) {
                exchange.setCurItem(exchange.getCurrentGEItem());
            }
        }

        if (button == 171) {
            exchange.increasePrice(1);
        } else if (button == 169) {
            exchange.decreasePrice(1);
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     *
     * @param from
     * @param to
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick(Player player) {}
}
