package net.tazogaming.hydra.game.ui.rsi.interfaces.ge;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ModelGrid;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/02/2015
 * Time: 15:24
 */
public class GrandExchangeItemSetsInterface implements InterfaceAdapter {
    public enum Sets {
        BRONZE_LG(11814, 3, 1155, 1117, 1075, 1189), BRONZE_SK(11816, 3, 1155, 1117, 1087, 1189),
        IRON_LG(11818, 3, 1153, 1115, 1067, 1191), IRON_SK(11820, 3, 1153, 1115, 1081, 1191),
        STEEL_LG(11822, 3, 1157, 1119, 1069, 1193), STEEL_SK(11824, 3, 1157, 1119, 1083, 1193),
        BLACK_LG(11826, 3, 1165, 1125, 1077, 1195), BLACK_SK(11828, 3, 1165, 1125, 1089, 1195),
        MITHRIL_LG(11830, 3, 1159, 1121, 1071, 1197), MITHRIL_SK(11832, 3, 1159, 1121, 1085, 1197),
        ADAMANT_LG(11834, 3, 1161, 1123, 1073, 1199), ADAMANT_SK(11836, 3, 1161, 1123, 1091, 1199),
        INITIATE_MALE(9668, 3, 5575, 5576, 5574), INITIATE_FEMALE(23125, 3, 5575, 23122, 5574),
        PROSELYTE_LG(9666, 2, 9672, 9674, 9676), PROSELYTE_SK(9670, 2, 9672, 9674, 9678),
        RUNE_LG(11838, 3, 1163, 1127, 1079, 1201), RUNE_SK(11840, 3, 1163, 1127, 1093, 1201),
        DRAG_CHAIN_LG(11842, 3, 1149, 3140, 4087, 1187), DRAG_CHAIN_SK(11844, 3, 1149, 3140, 4585, 1187),
        DRAG_PLATE_LG(14529, 3, 11335, 14479, 4087, 1187), DRAG_PLATE_SK(14531, 3, 11335, 14479, 4585, 1187),
        BLACK_H1_LG(19520, 3, 10306, 19167, 7332, 19169), BLACK_H1_SK(19530, 3, 10306, 19167, 7332, 19171),
        BLACK_H2_LG(19522, 3, 10308, 19188, 7338, 19190), BLACK_H2_SK(19532, 3, 10308, 19188, 7338, 19192),
        BLACK_H3_LG(19524, 3, 10310, 19209, 7344, 19211), BLACK_H3_SK(19534, 3, 10310, 19209, 7344, 19213),
        BLACK_H4_LG(19526, 3, 10312, 19230, 7350, 19232), BLACK_H4_SK(19536, 3, 10312, 19230, 7350, 19234),
        BLACK_H5_LG(19528, 3, 10314, 19251, 7356, 19253), BLACK_H5_SK(19538, 3, 10314, 19251, 7356, 19255),
        BLACK_TRIM_LG(11878, 3, 2587, 2583, 2585, 2589), BLACK_TRIM_SK(11880, 3, 2587, 2583, 3472, 2589),
        BLACK_GTRIM_LG(11882, 3, 2595, 2591, 2593, 2597), BLACK_GTRIM_SK(11884, 3, 2595, 2591, 3473, 2597),
        ADAMANT_H1_LG(19540, 3, 10296, 19173, 7334, 19175), ADAMANT_H1_SK(19550, 3, 10296, 19173, 7334, 19177),
        ADAMANT_H2_LG(19542, 3, 10298, 19194, 7340, 19196), ADAMANT_H2_SK(19552, 3, 10298, 19194, 7340, 19198),
        ADAMANT_H3_LG(19544, 3, 10300, 19215, 7346, 19217), ADAMANT_H3_SK(19554, 3, 10300, 19215, 7346, 19219),
        ADAMANT_H4_LG(19546, 3, 10302, 19236, 7352, 19238), ADAMANT_H4_SK(19556, 3, 10302, 19236, 7352, 19240),
        ADAMANT_H5_LG(19548, 3, 10304, 19257, 7358, 19259), ADAMANT_H5_SK(19558, 3, 10304, 19257, 7358, 19261),
        ADAMANT_TRIM_LG(11886, 3, 2605, 2599, 2601, 2603), ADAMANT_TRIM_SK(11888, 3, 2605, 2599, 3474, 2603),
        ADAMANT_GTRIM_LG(11890, 3, 2613, 2607, 2609, 2611), ADAMANT_GTRIM_SK(11892, 3, 2613, 2607, 3475, 2611),
        RUNE_H1_LG(19560, 3, 10286, 19179, 19182, 7336), RUNE_H1_SK(19570, 3, 10286, 19179, 19185, 7336),
        RUNE_H2_LG(19562, 3, 10288, 19200, 19203, 7342), RUNE_H2_SK(19572, 3, 10288, 19200, 19206, 7342),
        RUNE_H3_LG(19564, 3, 10290, 19221, 19224, 7348), RUNE_H3_SK(19574, 3, 10290, 19221, 19227, 7348),
        RUNE_H4_LG(19566, 3, 10292, 19242, 19245, 7354), RUNE_H4_SK(19576, 3, 10292, 19242, 19248, 7354),
        RUNE_H5_LG(19568, 3, 10294, 19263, 19266, 7360), RUNE_H5_SK(19578, 3, 10294, 19263, 19269, 7360),
        RUNE_TRIM_LG(11894, 3, 2627, 2623, 2625, 2629), RUNE_TRIM_SK(11896, 3, 2627, 2623, 3477, 2629),
        RUNE_GTRIM_LG(11898, 3, 2619, 2615, 2617, 2621), RUNE_GTRIM_SK(11900, 3, 2619, 2615, 3676, 2621),
        GUTHIX_LG(11926, 3, 2673, 2669, 2671, 2675), GUTHIX_SK(11932, 3, 2673, 2669, 3480, 2675),
        SARADOMIN_LG(11928, 3, 2665, 2661, 2663, 2667), SARADOMIN_SK(11934, 3, 2665, 2661, 3479, 2667),
        ZAMORAK_LG(11930, 3, 2657, 2653, 2655, 2659), ZAMORAK_SK(11936, 3, 2657, 2653, 3478, 2659),
        ARMADYL_LG(19588, 3, 19422, 19413, 19416, 19425), ARMADYL_SK(19590, 3, 19422, 19413, 19419, 19425),
        BANDOS_LG(19592, 3, 19437, 19428, 19431, 19440), BANDOS_SK(19594, 3, 19437, 19428, 19434, 19440),
        ANCIENT_LG(19596, 3, 19407, 19398, 19401, 19410), ANCIENT_SK(19598, 3, 19407, 19398, 19404, 19410),
        ROCKSHELL(11942, 4, 6128, 6129, 6130, 6151, 6145), ELITEBLACK(14527, 2, 14494, 14492, 14490),
        THIRDAGEMELEE(11858, 3, 10350, 10348, 10352, 10346), THIRDAGERANGE(11860, 3, 10334, 10330, 10332, 10336),
        THIRDAGEMAGE(11862, 3, 10342, 10334, 10338, 10340), THIRDAGEPRAY(19580, 3, 19314, 19317, 19320, 19311, 19308),
        AHRIM(11846, 3, 4708, 4710, 4712, 4714), DHAROK(11848, 3, 4716, 4718, 4720, 4722),
        GUTHAN(11850, 3, 4724, 4726, 4728, 4730), KARIL(11852, 3, 4732, 4734, 4736, 4738),
        TORAG(11854, 3, 4745, 4747, 4749, 4751), VERAC(11856, 3, 4753, 4755, 4757, 4759),
        AKRISAE(21768, 3, 21736, 21744, 21752, 21760), LEATHER_TRIM(11908, 3, 7364, 7368),
        LEATHER_GTRIM(11910, 3, 7362, 7366), GREEN_TRIM(11912, 3, 7372, 7380), GREEN_GTRIM(11914, 3, 7370, 7378),
        BLUE_TRIM(11916, 3, 7376, 7384), BLUE_GTRIM(11918, 3, 7374, 7382), GREEN_D_HIDE(11864, 3, 1065, 1099, 1135),
        BLUE_D_HIDE(11866, 3, 2487, 2493, 2499), RED_D_HIDE(11868, 3, 2489, 2495, 2501),
        BLACK_D_HIDE(11870, 3, 2491, 2497, 2503), ROYAL_D_HIDE(24386, 3, 24388, 24382, 24379, 24376),
        GREEN_BLESSED(11920, 3, 10382, 10378, 10380, 10376), BLUE_BLESSED(11922, 3, 10390, 10386, 10388, 10384),
        RED_BLESSED(11924, 3, 10374, 10370, 10372, 10368), BROWN_BLESSED(19582, 3, 19457, 19453, 19455, 19451),
        PURPLE_BLESSED(19584, 3, 19449, 19445, 19447, 19443), SILVER_BLESSED(19586, 3, 19465, 19461, 19463, 19459),
        WIZARD_TRIM(11904, 3, 7396, 7392, 7388), WIZARD_GTRIM(11906, 3, 7394, 7390, 7386),
        MYSTIC(11872, 3, 4089, 4091, 4093, 4095, 4097), LIGHT_MYSTIC(11960, 3, 4109, 4111, 4113, 4115, 4117),
        DARK_MYSTIC(11962, 3, 4099, 4101, 4103, 4105, 4107), DAGON_HAI(14525, 3, 14499, 14497, 14501),
        ENCHANTED(11902, 3, 7400, 7399, 7398), INFINITY(11874, 3, 6918, 6916, 6924, 6920, 6922),
        GILDED_LG(11938, 3, 3486, 3481, 3483, 3488), GILDED_SK(11940, 3, 3486, 3481, 3485, 3488),
        SPLITBARK(11876, 3, 3385, 3387, 3389, 3391, 3393), SPINED(11944, 3, 6131, 6133, 6135, 6143, 6149),
        SKELETAL(11946, 3, 6137, 6139, 6141, 6147, 6153), DWARF_CANNON(11967, 3, 6, 8, 10, 12);

        /** sets made: 15/02/16 **/
        private static Map<Integer, Sets> sets = new HashMap<Integer, Sets>();


        public static void loadPrices() {
            for(Sets set : Sets.values()){
              if(set.getId() <= 20027) {
                  Item setItem = Item.forId(set.getId());
                  if (setItem != null) {
                      int price = 0;
                      for (Integer i : set.getItems())
                          price += Item.forId(i).getExchangePrice();
                      setItem.setExchangePrice(price);
                  }
              }
            }
        }
        static {
            for (Sets set : Sets.values()) {
                sets.put(set.getId(), set);
            }
        }

        /** setId made: 15/02/16 **/
        private int setId;

        /** items made: 15/02/16 **/
        private int[] items;

        /** requiredSlots made: 15/02/16 **/
        private int requiredSlots;

        /**
         * Constructs ...
         *
         *
         * @param setId
         * @param requiredSlots
         * @param items
         */
        private Sets(int setId, int requiredSlots, int... items) {
            this.setId         = setId;
            this.requiredSlots = requiredSlots;
            this.items         = items;
        }

        /**
         * Method forId
         * Created on 15/02/16
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @param itemId
         *
         * @return
         */
        public static Sets forId(int itemId) {
            return sets.get(itemId);
        }

        /**
         * Method getId
         * Created on 15/02/16
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getId() {
            return setId;
        }

        /**
         * Method getItems
         * Created on 15/02/16
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int[] getItems() {
            return items;
        }

        /**
         * Method getRequiredSlots
         * Created on 15/02/16
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getRequiredSlots() {
            return requiredSlots;
        }
    }

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceOpened(Player player) {
        final CS2Call cx = new CS2Call(676);

        player.getTickEvents().add(new PlayerTickEvent(player, true, 2) {
            @Override
            public void doTick(Player owner) {
                owner.getActionSender().sendClientScript(cx);
            }
        });

        ModelGrid grid = new ModelGrid(93, 644, 0);

        grid.addOption("Exchange");
        player.getActionSender().sendClientScript(grid.getCS2());
        player.getGameFrame().sendIComponentSettings(645, 16, 0, 115, 14);
        player.getGameFrame().sendIComponentSettings(644, 0, 0, 27, 1030);
        player.getGameFrame().setInventoryInterface(644, 93);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        Sets set = Sets.forId(itemId);

        if (set != null) {
            if (clickSlot == 1) {
                for (Integer item : set.getItems()) {
                    if (!player.getInventory().hasItem(item, 1)) {
                        player.getActionSender().sendMessage("You need a " + Item.forId(item).getName()
                                + " to make this set");

                        return;
                    }
                }

                for (Integer item : set.getItems()) {
                    player.getInventory().deleteItem(item, 1);
                }

                player.getInventory().addItem(itemId, 1);
                player.getActionSender().sendMessage("You exchange for a set");
            }else{
                for(Integer item : set.getItems()){
                    player.getActionSender().sendMessage("Set contains: "+ Text.BLUE(Item.forId(item).getName()));
                }
            }
        } else {
            player.getActionSender().sendMessage("Invalid item set");
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     *
     * @param from
     * @param to
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick(Player player) {}
}
