package net.tazogaming.hydra.game.ui.rsi.interfaces;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.SkillGuide;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/08/14
 * Time: 14:02
 */
public class LevelUP implements InterfaceAdapter {
    public static final int[] FLASH_VALUES = {
        1, 4, 2, 64, 8, 16, 32, 32768, 131072, 2048, 16384, 65536, 1024, 8192, 4096, 256, 128, 512, 524288, 1048576,
        262144, 4194304, 2097152, 8388608, 16777216,
    };

    /**
     * The skill icons send on the level up chat box interface.
     */
    public static final int[] SKILL_ICON = {
        67108864, 335544320, 134217728, 402653184, 201326592, 469762048, 268435456, 1073741824, 1207959552, 1275068416,
        1006632960, 1140850688, 738197504, 939524096, 872415232, 603979776, 536870912, 671088640, 1342177280,
        1409286144, 805306368, 1543503872, 1610612736, 1476395008, 1677721600
    };
    public static final int[] CONFIG_VALUES = {
        1, 5, 2, 6, 3, 7, 4, 16, 18, 19, 15, 17, 11, 14, 13, 9, 8, 10, 20, 21, 12, 23, 22, 24, 25
    };

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        int flashingConfig = 0;
        int spriteConfig   = 0;

        for (int i = 0; i < player.getAccount().getLeveledUp().length; i++) {
            if (player.getAccount().getLeveledUp()[i]) {
                flashingConfig |= FLASH_VALUES[i];
                spriteConfig   = CONFIG_VALUES[i];
            }
        }

        player.getActionSender().sendVar(1179, spriteConfig << 26);

        for (int i = 0; i < player.getAccount().getLeveledUp().length; i++) {
            if (player.getAccount().getLeveledUp()[i]) {
                player.getActionSender().sendMessage("Congratulations you've just advanced "
                        + (SkillGuide.SKILL_NAMES[i].startsWith("a")
                           ? "an"
                           : "a") + " " + SkillGuide.SKILL_NAMES[i] + " level");
                player.getGameFrame().sendString("Congratulations, you have just advanced a "
                                                 + SkillGuide.SKILL_NAMES[i] + " level!", 740, 0);
                player.getGameFrame().sendString("You have now reached level " + player.getMaxStat(i) + "!", 740, 1);
                player.getAccount().getLeveledUp()[i] = false;
            }
        }
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
