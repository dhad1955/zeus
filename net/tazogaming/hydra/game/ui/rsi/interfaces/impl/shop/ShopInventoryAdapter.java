package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.shop;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.shops.Shop;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/07/14
 * Time: 03:50
 */
public class ShopInventoryAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        CS2Call settings = new CS2Call(149);

        settings.addString("Sell 50").addString("Sell 10").addString("Sell 5").addString("Sell 1").addString(
            "Value").addInt(-1).addInt(1).addInt(7).addInt(4).addInt(93).addInt(40697856);
        player.getActionSender().sendClientScript(settings);
        player.getGameFrame().sendAMask(0, 27, 621, 0, 36, 1086);
        player.getActionSender().sendItems(93, player.getInventory().getItems(), player.getInventory().getItemsCount(),
                                           false);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        switch (clickSlot) {
        case 0 :
            Shop s = player.getGameFrame().getShop();
            Item i = player.getInventory().getItem(slot);

            if (!s.isSellingAllowed()) {
                player.getActionSender().sendMessage("This shop can't buy this item at this time");

                return;
            }

            player.getActionSender().sendMessage(i.getName() + " shop will buy for " + (i.getPrice() / 2) + " gold ( "
                    + GameMath.formatMoney(i.getPrice() / 2) + ")");

            break;

        case 1 :
            ShopManager.sellItem(player.getGameFrame().getShop(), slot, player, 1);

            break;

        case 2 :
            ShopManager.sellItem(player.getGameFrame().getShop(), slot, player, 5);

            break;

        case 3 :
            ShopManager.sellItem(player.getGameFrame().getShop(), slot, player, 10);

            break;

        case 4 :
            ShopManager.sellItem(player.getGameFrame().getShop(), slot, player, 50);

            break;
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {
        player.getInventory().moveItems(from, to);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
