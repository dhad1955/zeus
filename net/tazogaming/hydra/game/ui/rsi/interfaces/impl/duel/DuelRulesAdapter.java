package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.duel;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/07/14
 * Time: 15:51
 */
public class DuelRulesAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        player.getGameFrame().sendAMask(1278, 631, 94, 0, 27);
        player.getGameFrame().sendAMask(1278, 628, 0, 0, 27);
        player.getGameFrame().sendString("Dragon Dagger only", 631, 33);

        CS2Call invCall = new CS2Call(150);
        String  str     = "";

        invCall.addString("").addString("").addString("").addString("");
        invCall.addString("Remove X");
        invCall.addString("Remove All");
        invCall.addString("Remove 10");
        invCall.addString("Remove 5");
        invCall.addString("Remove");
        invCall.addInts(1, 0, 2, 2, 134, 631 << 16 | 94);
        player.getActionSender().sendClientScript(invCall);
        invCall = new CS2Call(150).addString("").addString("").addString("").addString("");
        invCall.addString("Stake X").addString("Stake All").addString("Stake 10").addString("Stake 5").addString(
            "Stake");
        invCall.addInts(-1, 0, 7, 4, 93, 628 << 16);
        player.getActionSender().sendClientScript(invCall);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {
        if (player.getGameFrame().getDuel() != null) {
            player.getGameFrame().getDuel().decline(player);
        }
    }

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, final int slot, int clickSlot, final Player player) {
        switch (clickSlot) {
        case 0 :
            player.getGameFrame().getDuel().unstakeItem(slot, 1, player);

            break;

        case 1 :
            player.getGameFrame().getDuel().unstakeItem(slot, 5, player);

            break;

        case 2 :
            player.getGameFrame().getDuel().unstakeItem(slot, 10, player);

            break;

        case 3 :
            player.getGameFrame().getDuel().unstakeItem(slot, Integer.MAX_VALUE, player);

            break;

        case 4 :
            player.getGameFrame().requestAmount("Select the amount you wish to remove", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    if (pla.getGameFrame().getDuel() != null) {
                        pla.getGameFrame().getDuel().unstakeItem(slot, amount, pla);
                    }
                }
            });
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        pla.getGameFrame().getDuel().onButton(button);

        if (button == 93) {
            pla.getGameFrame().getDuel().onAccept(pla);
        } else if (button == 100) {
            pla.getGameFrame().getDuel().decline(pla);
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
