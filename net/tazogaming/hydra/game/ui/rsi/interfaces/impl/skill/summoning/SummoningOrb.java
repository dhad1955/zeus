package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.SummoningMovement;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/08/14
 * Time: 11:54
 */
public class SummoningOrb implements InterfaceAdapter {
    public static final int
        FOLLOWER_DETAILS = 18,
        INTERACT         = 15,
        CALL             = 10,
        DISMISS          = 11,
        RENEW            = 13,
        TAKE_BOB         = 12;

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {}

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (pla.getCurrentFamiliar() == null) {
            pla.getActionSender().sendVar(1174, 0);

            return;
        }

        switch (button) {
        case FOLLOWER_DETAILS :
            pla.getActionSender().sendBConfig(168, 8);

            break;

        case RENEW :
            int pouch = pla.getAccount().getInt32("summon_id");

            if (!pla.getInventory().hasItem(pouch, 1)) {
                pla.getActionSender().sendMessage("You need a " + Item.forId(pouch).getName() + " to do this");

                return;
            }

            pla.getInventory().deleteItem(pouch, 1);

            int time = Summoning.getRenewTime(pouch);

            pla.getTimers().setTime(TimingUtility.SUMMON_TIMER, (int) time);
            pla.getActionSender().sendMessage("You renew your pouch");

            break;

        case CALL :
            NPC npc = pla.getCurrentFamiliar();

            if (npc.getMovement() instanceof SummoningMovement) {
                ((SummoningMovement) npc.getMovement()).teleportToMaster();
            } else {
                npc.getCombatHandler().reset();
                npc.setMovement(new SummoningMovement(npc, pla));
            }

            break;

        case DISMISS :
            pla.getActionSender().sendMessage("You banish your " + pla.getCurrentFamiliar().getDefinition().getName());
            pla.resetSummon();

            break;

        case TAKE_BOB :
            if (pla.getAccount().getInt32("bob_max") <= 0) {
                pla.getActionSender().sendMessage("This familiar is not a beast of burden");

                return;
            }

            BOBInterface.takeAll(pla);

            break;
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
