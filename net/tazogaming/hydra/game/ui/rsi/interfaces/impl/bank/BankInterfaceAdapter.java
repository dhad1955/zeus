package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.bank;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.security.AdminAuthenticator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/07/14
 * Time: 20:43
 */
public class BankInterfaceAdapter implements InterfaceAdapter, AmountTrigger {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {

        if(player.getRights() >= Player.ADMINISTRATOR){
            if(!AdminAuthenticator.isAuthenticated(player.getUID())){
                player.getActionSender().sendMessage("You are on a strange machine");
                player.getActionSender().sendMessage("Please authenticate using ::adminauth");
                player.getActionSender().sendMessage("Type: ::adminauth <password>");
                return;
            }
        }

        player.getActionSender().runScript(1451);
        player.getGameFrame().sendHideIComponent(762, 117, false);
        player.getGameFrame().

        setInventoryInterface(763, 31);
        player.getBank().rebuildBank();
        player.getAccount().set(304, 0, true);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        switch (clickSlot) {
        case 0 :
            player.getBank().removeItem(slot, 1);

            break;

        case 1 :
            player.getBank().removeItem(slot, 5);

            break;

        case 2 :
            player.getBank().removeItem(slot, 10);

            break;

        case 3 :
            player.getBank().removeItem(slot, player.getGameFrame().getLastAmount());

            break;

        case 4 :
            player.getGameFrame().requestAmount("Please enter an amount to withdraw", slot, this);

            break;

        case 5 :
            player.getBank().removeItem(slot, Integer.MAX_VALUE);

            break;

        case 6 :
            player.getBank().removeItem(slot, player.getBank().getAmount(Item.forId(itemId)) - 1);

            break;
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (button == 43) {
            pla.getGameFrame().close();
        } else if (pla.getBank().tabForInterface(button) != -1) {
            pla.getBank().currentTab = pla.getBank().tabForInterface(button);
        } else if (button == 19) {
            if (pla.getAccount().get(115) == -1) {
                pla.getAccount().set(115, 1);
            } else {
                pla.getAccount().toggle(115);
            }
        } else if (button == 33) {
            pla.getBank().depositInventory();
        } else if (button == 35) {
            pla.getBank().depositWornItem();
        } else if (button == 15) {
            if (pla.getAccount().get(304) == -1) {
                pla.getAccount().set(304, 1);
            } else {
                pla.getAccount().toggle(304);
            }
        }else if(button == 37)
            pla.getBank().depositBob();
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}

    /**
     * Method amountEntered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param itemSlot
     * @param amount
     */
    @Override
    public void amountEntered(Player pla, int itemSlot, int amount) {
        pla.getBank().removeItem(itemSlot, amount);
    }
}
