package net.tazogaming.hydra.game.ui.rsi.interfaces.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.TextInputRequest;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 00:30
 */
public class ClanSettings implements InterfaceAdapter {
    public static final String[] RANK_INDEX = {
        "Anyone", "Any friends", "Recruit+", "Corporal+", "Sergeant+", "Lieutenant+", "Captain+", "General+", "Only me"
    };

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceOpened(Player player) {
        sendSettings(player);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     *
     * @param button
     * @param slot
     * @param player
     * @param contextSlot
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void actionButton(int button, int slot, Player player, int contextSlot) {
        final ClanChannel channel = World.getWorld().getChannelList().getByPlayer(player.getUsername());

        if (button == 22) {
            if (channel == null) {
                player.getGameFrame().setRequest(new TextInputRequest() {
                    @Override
                    public void onResponse(Player player, String response) {
                        if (response.length() > 13) {
                            player.getActionSender().sendMessage("Clan name to long");
                        } else {
                            ClanChannel channel = World.getWorld().getChannelList().addChannel(response, player);

                            if (channel != null) {
                                player.getAccount().setSetting("channel_name", response, true);
                            }

                            sendSettings(player);
                        }
                    }
                }, "Please enter a name for your clan");
            } else {
                if (contextSlot == 1) {
                    World.getWorld().getChannelList().removeChannel(channel);
                    sendSettings(player);

                    return;
                } else {
                    player.getGameFrame().setRequest(new TextInputRequest() {
                        @Override
                        public void onResponse(Player player, String response) {
                            if (response.length() > 13) {
                                player.getActionSender().sendMessage("Clan name to long");
                            } else {
                                World.getWorld().getChannelList().changeName(player, response);
                                sendSettings(player);
                            }
                        }
                    }, "Please enter a new name for your clan");
                }
            }
        }

        if (channel == null) {
            return;
        }

        if (button > 21) {
            channel.changeSetting(button, contextSlot);
            sendSettings(player);
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     *
     * @param from
     * @param to
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick(Player player) {}

    /**
     * Method sendSettings
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void sendSettings(Player player) {
        ClanChannel channel = World.getWorld().getChannelList().getByPlayer(player.getUsername());

        if (channel == null) {
            player.getGameFrame().sendString("Chat disabled", 590, 22);

            return;
        } else {
            player.getGameFrame().sendString(channel.getName(), 590, 22);
        }


        player.getGameFrame().sendString(RANK_INDEX[channel.getRankJoin()], 590, 23);
        player.getGameFrame().sendString(RANK_INDEX[channel.getRankTalk()], 590, 24);
        player.getGameFrame().sendString(RANK_INDEX[channel.getRankKick()], 590, 25);
    //    player.getGameFrame().sendString(RANK_INDEX[channel.getRankLoot()], 590, 26);
    }
}
