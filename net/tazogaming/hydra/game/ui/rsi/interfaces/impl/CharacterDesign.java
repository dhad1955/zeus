package net.tazogaming.hydra.game.ui.rsi.interfaces.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.PlayerAppearance;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/07/14
 * Time: 15:07
 */
public class CharacterDesign implements InterfaceAdapter {

    // ripped this shit from something else, rofl wow what a pain glad i didnt have to code it
    // lol this is some big ass code, ideally we should be loading from the config in the cache but im not prepared to do that atm
    public static final int[] SKIN_COLOURS  = new int[] {
        9, 8, 7, 0, 1, 2, 3, 4, 5, 6, 10, 11
    };
    public static final int[] HAIR_COLOURS  = new int[] {
        20, 19, 10, 18, 4, 5, 15, 7, 0, 6, 21, 9, 22, 17, 8, 16, 24, 11, 23, 3, 2, 1, 14, 13, 12
    };
    public static final int[] CLOTH_COLOURS = new int[] {
        32, 101, 48, 56, 165, 103, 167, 106, 54, 198, 199, 200, 225, 35, 39, 53, 42, 46, 29, 91, 57, 90, 34, 102, 104,
        105, 107, 173, 137, 201, 204, 211, 197, 108, 217, 220, 221, 226, 227, 215, 222, 166, 212, 174, 175, 169, 144,
        135, 136, 133, 123, 119, 192, 194, 117, 115, 111, 141, 45, 49, 84, 77, 118, 88, 85, 138, 51, 92, 112, 145, 179,
        143, 149, 151, 153, 44, 154, 155, 86, 89, 72, 66, 33, 206, 109, 110, 114, 116, 184, 170, 120, 113, 150, 205,
        210, 207, 209, 193, 152, 156, 183, 161, 159, 160, 73, 75, 181, 185, 208, 74, 36, 37, 43, 50, 58, 55, 139, 148,
        147, 64, 69, 70, 71, 68, 93, 94, 95, 124, 182, 96, 97, 219, 63, 228, 79, 82, 98, 99, 100, 125, 126, 127, 40,
        128, 129, 188, 130, 131, 186, 132, 164, 157, 180, 187, 31, 162, 168, 52, 163, 158, 196, 59, 60, 87, 78, 61, 76,
        80, 171, 172, 176, 177, 178, 38, 41, 47, 62, 65, 67, 81, 83, 121, 122, 134, 140, 142, 146, 189, 190, 191, 195,
        202, 203, 213, 214, 216, 218, 223, 224
    };
    public static final int[] FEET_COLOURS = new int[] {
        55, 54, 14, 120, 194, 53, 11, 154, 0, 1, 2, 3, 4, 5, 9, 78, 25, 33, 142, 80, 144, 83, 31, 175, 176, 177, 12, 16,
        30, 19, 23, 6, 68, 34, 67, 11, 79, 81, 82, 84, 150, 114, 178, 181, 188, 174, 85, 194, 197, 198, 203, 204, 192,
        199, 143, 189, 151, 152, 146, 121, 112, 113, 110, 100, 96, 169, 171, 94, 92, 88, 118, 22, 26, 61, 95, 65, 62,
        115, 28, 69, 89, 122, 156, 126, 128, 130, 21, 131, 132, 63, 66, 49, 43, 10, 183, 86, 87, 91, 93, 161, 147, 97,
        90, 127, 182, 187, 184, 186, 170, 129, 133, 160, 138, 136, 137, 50, 52, 158, 162, 185, 51, 13, 20, 27, 35, 32,
        116, 125, 124, 41, 46, 47, 48, 45, 70, 71, 72, 101, 159, 73, 74, 196, 40, 205, 56, 59, 75, 76, 77, 102, 103,
        104, 17, 105, 106, 165, 107, 108, 163, 109, 141, 134, 157, 164, 8, 139, 145, 29, 140, 135, 173, 36, 37, 64, 38,
        57, 148, 149, 153, 155, 15, 18, 24, 39, 42, 44, 58, 60, 98, 99, 117, 119, 123, 166, 167, 168, 172, 179, 180,
        190, 191, 193, 195, 200, 201
    };
    public static final int[] MALE_HAIR_LOOKS = new int[] {
        5, 6, 93, 96, 92, 268, 265, 264, 267, 315, 94, 263, 312, 313, 311, 314, 261, 310, 1, 0, 97, 95, 262, 316, 309,
        3, 91, 4
    };
    public static final int[][] MALE_TORSO_LOOKS = new int[][] {
        { 457, 588, 364 }, { 445, -1, 366 }, { 459, 591, 367 }, { 460, 592, 368 }, { 461, 593, 369 }, { 462, 594, 370 },
        { 452, -1, 371 }, { 463, 596, 372 }, { 464, 597, 373 }, { 446, -1, 374 }, { 465, 599, 375 }, { 466, 600, 376 },
        { 467, 601, 377 }, { 451, -1, 378 }, { 468, 603, 379 }, { 453, -1, 380 }, { 454, -1, 381 }, { 455, -1, 382 },
        { 469, 607, 383 }, { 470, 608, 384 }, { 450, -1, 385 }, { 458, 589, 365 }, { 447, -1, 386 }, { 448, -1, 387 },
        { 449, -1, 388 }, { 471, 613, 389 }, { 443, -1, 390 }, { 472, 615, 391 }, { 473, 616, 392 }, { 444, -1, 393 },
        { 474, 618, 394 }, { 456, -1, 9 }
    };
    public static final int[] MALE_LEG_LOOKS = new int[] {
        620, 622, 623, 624, 625, 626, 627, 628, 629, 630, 631, 632, 633, 634, 635, 636, 637, 638, 639, 640, 641, 621,
        642, 643, 644, 645, 646, 647, 648, 649, 650, 651
    };
    public static final int[] MALE_FEET_LOOKS   = new int[] {
        427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 42, 43
    };
    public static final int[] MALE_FACIAL_LOOKS = new int[] {
        14, 13, 98, 308, 305, 307, 10, 15, 16, 100, 12, 11, 102, 306, 99, 101, 104, 17
    };
    public static final int[] FEMALE_HAIR_LOOKS = new int[] {
        141, 361, 272, 273, 359, 274, 353, 277, 280, 360, 356, 269, 358, 270, 275, 357, 145, 271, 354, 355, 45, 52, 49,
        47, 48, 46, 143, 362, 144, 279, 142, 146, 278, 135
    };
    public static final int[][] FEMALE_TORSO_LOOKS = new int[][] {
        { 565, 395, 507 }, { 567, 397, 509 }, { 568, 398, 510 }, { 569, 399, 511 }, { 570, 400, 512 },
        { 571, 401, 513 }, { 561, -1, 514 }, { 572, 403, 515 }, { 573, 404, 516 }, { 574, 405, 517 }, { 575, 406, 518 },
        { 576, 407, 519 }, { 577, 408, 520 }, { 560, -1, 521 }, { 578, 410, 522 }, { 562, -1, 523 }, { 563, -1, 524 },
        { 564, -1, 525 }, { 579, 414, 526 }, { 559, -1, 527 }, { 580, 416, 528 }, { 566, 396, 508 }, { 581, 417, 529 },
        { 582, 418, 530 }, { 557, -1, 531 }, { 583, 420, 532 }, { 584, 421, 533 }, { 585, 422, 534 }, { 586, 423, 535 },
        { 556, -1, 536 }, { 587, 425, 537 }, { 558, -1, 538 }
    };
    public static final int[] FEMALE_LEG_LOOKS = new int[] {
        475, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 476,
        497, 498, 499, 500, 501, 502, 503, 504, 505, 506
    };
    public static final int[] FEMALE_FEET_LOOKS = new int[] {
        539, 540, 541, 542, 543, 544, 545, 546, 547, 548, 549, 550, 551, 552, 553, 554, 555, 79, 80
    };

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {}

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        PlayerAppearance appearance = pla.getAppearance();


        pla.getAppearance().setChanged(true);
        switch (button) {
        case 117 :
            pla.getGameFrame().changeRoot(pla.isHD()
                    ? 746
                    : 548);
            if (pla.getAccount().get(Account.TUTORIAL_STAGE) == 0) {
                pla.getAccount().set(Account.TUTORIAL_STAGE, 1000, true);
                pla.getAppearance().setChanged(true);
                World.getWorld().getScriptManager().directTrigger(pla, null, Trigger.CHAR_DESIGN_FINISHED, 0);

                // todo change
            }

            break;

        case 95 :
            pla.getAccount().setSetting("char_tab", 1);

            break;

        case 96 :
            pla.getAccount().setSetting("char_tab", 2);

            break;

        case 97 :
            pla.getAccount().setSetting("char_tab", 3);

            break;

        case 98 :
            pla.getAccount().setSetting("char_tab", 4);

            break;

        case 99 :
            pla.getAccount().setSetting("char_tab", 5);

            break;

        case 100 :
            pla.getAccount().setSetting("char_tab", 6);

            break;

        case 107 :
            switch (pla.getAccount().getInt32("char_tab")) {
            case 5 :
                if (appearance.getGender() == 0) {
                    appearance.setLook(6, MALE_FEET_LOOKS[slot]);
                } else {
                    appearance.setLook(6, FEMALE_FEET_LOOKS[slot]);
                }

                break;

            case 2 :
                if (appearance.getGender() == 0) {
                    appearance.setLook(0, MALE_HAIR_LOOKS[slot]);
                } else {
                    appearance.setLook(0, FEMALE_HAIR_LOOKS[slot]);
                }

                break;

            case 3 :
                if (appearance.getGender() == 0) {
                    appearance.setLook(2, MALE_TORSO_LOOKS[slot][0]);
                    appearance.setLook(3, MALE_TORSO_LOOKS[slot][1]);
                    appearance.setLook(4, MALE_TORSO_LOOKS[slot][2]);

 
                } else if (appearance.getGender() == 1) {
                    appearance.setLook(2, FEMALE_TORSO_LOOKS[slot][0]);
                    appearance.setLook(3, FEMALE_TORSO_LOOKS[slot][1]);
                    appearance.setLook(4, FEMALE_TORSO_LOOKS[slot][2]);
                }

                break;

            case 4 :
                if (appearance.getGender() == 0) {
                    appearance.setLook(5, MALE_LEG_LOOKS[slot]);
                } else {
                    appearance.setLook(5, FEMALE_LEG_LOOKS[slot]);
                }

                break;

            case 6 :
                if (appearance.getGender() == 0) {
                    appearance.setLook(1, MALE_FACIAL_LOOKS[slot]);
                }

                break;
            }

            break;

        case 45 :                       // skinc main screen
            appearance.setColor(4, SKIN_COLOURS[slot]);

            break;

        case 111 :                      // color list
            switch (pla.getAccount().getInt32("char_tab")) {
            case 2 :
                appearance.setColor(0, HAIR_COLOURS[slot]);

                break;

            case 5 :
                appearance.setColor(3, FEET_COLOURS[slot]);

                break;

            case 3 :
                appearance.setColor(1, CLOTH_COLOURS[slot]);

                break;

            case 4 :
                appearance.setColor(2, CLOTH_COLOURS[slot]);

                break;

            case 6 :                    // possible bug? lol
                appearance.setColor(0, HAIR_COLOURS[slot]);

                break;

            default :
                appearance.setColor(4, SKIN_COLOURS[slot]);

                break;
            }

            break;

        case 38 :
            appearance.setGender(0);
            appearance.setLook(0, 3);
            appearance.setLook(1, 14);

            break;

        case 39 :
            appearance.setGender(1);    // female
            appearance.setLook(0, 48);
            appearance.setLook(1, 1000);

            break;

        case 83 :
            if (pla.getAccount().getInt32("charStage") == 1) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 471;
                    appearance.getLook()[3]   = 613;
                    appearance.getLook()[4]   = 389;
                    appearance.getLook()[5]   = 645;
                    appearance.getLook()[6]   = 439;
                    appearance.getColour()[1] = 37;
                    appearance.getColour()[2] = 213;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 583;
                    appearance.getLook()[3]   = 420;
                    appearance.getLook()[4]   = 532;
                    appearance.getLook()[5]   = 500;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 45;
                    appearance.getColour()[2] = 221;
                    appearance.getColour()[3] = 15;
                }
            } else if (pla.getAccount().getInt32("charStage") == 2) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 453;
                    appearance.getLook()[4]   = 380;
                    appearance.getLook()[5]   = 636;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 197;
                    appearance.getColour()[2] = 202;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 562;
                    appearance.getLook()[3]   = 425;
                    appearance.getLook()[4]   = 523;
                    appearance.getLook()[5]   = 491;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 197;
                    appearance.getColour()[2] = 202;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 3) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 447;
                    appearance.getLook()[4]   = 386;
                    appearance.getLook()[5]   = 642;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 125;
                    appearance.getColour()[2] = 125;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 581;
                    appearance.getLook()[3]   = 417;
                    appearance.getLook()[4]   = 529;
                    appearance.getLook()[5]   = 497;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 125;
                    appearance.getColour()[2] = 125;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 4) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 469;
                    appearance.getLook()[3]   = 607;
                    appearance.getLook()[4]   = 383;
                    appearance.getLook()[5]   = 639;
                    appearance.getLook()[6]   = 431;
                    appearance.getColour()[1] = 149;
                    appearance.getColour()[2] = 150;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 579;
                    appearance.getLook()[3]   = 414;
                    appearance.getLook()[4]   = 526;
                    appearance.getLook()[5]   = 494;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 149;
                    appearance.getColour()[2] = 149;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 5) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 451;
                    appearance.getLook()[4]   = 378;
                    appearance.getLook()[5]   = 634;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 165;
                    appearance.getColour()[2] = 165;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 560;
                    appearance.getLook()[3]   = 416;
                    appearance.getLook()[4]   = 521;
                    appearance.getLook()[5]   = 489;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 165;
                    appearance.getColour()[2] = 166;
                    appearance.getColour()[3] = 142;
                }
            } else if (pla.getAccount().getInt32("charStage") == 6) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 452;
                    appearance.getLook()[3]   = 592;
                    appearance.getLook()[4]   = 371;
                    appearance.getLook()[5]   = 627;
                    appearance.getLook()[6]   = 434;
                    appearance.getColour()[1] = 189;
                    appearance.getColour()[2] = 189;
                    appearance.getColour()[3] = 39;
                } else {
                    appearance.getLook()[2]   = 561;
                    appearance.getLook()[3]   = 416;
                    appearance.getLook()[4]   = 514;
                    appearance.getLook()[5]   = 482;
                    appearance.getLook()[6]   = 545;
                    appearance.getColour()[1] = 189;
                    appearance.getColour()[2] = 189;
                    appearance.getColour()[3] = 39;
                }
            } else if (pla.getAccount().getInt32("charStage") == 7) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 464;
                    appearance.getLook()[3]   = 597;
                    appearance.getLook()[4]   = 373;
                    appearance.getLook()[5]   = 629;
                    appearance.getLook()[6]   = 435;
                    appearance.getColour()[1] = 29;
                    appearance.getColour()[2] = 30;
                    appearance.getColour()[3] = 0;
                } else {
                    appearance.getLook()[2]   = 573;
                    appearance.getLook()[3]   = 404;
                    appearance.getLook()[4]   = 516;
                    appearance.getLook()[5]   = 484;
                    appearance.getLook()[6]   = 547;
                    appearance.getColour()[1] = 29;
                    appearance.getColour()[2] = 30;
                    appearance.getColour()[3] = 0;
                }
            } else if (pla.getAccount().getInt32("charStage") == 8) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 468;
                    appearance.getLook()[3]   = 603;
                    appearance.getLook()[4]   = 379;
                    appearance.getLook()[5]   = 635;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 117;
                    appearance.getColour()[2] = 118;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 578;
                    appearance.getLook()[3]   = 410;
                    appearance.getLook()[4]   = 522;
                    appearance.getLook()[5]   = 490;
                    appearance.getLook()[6]   = 550;
                    appearance.getColour()[1] = 117;
                    appearance.getColour()[2] = 118;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 9) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 458;
                    appearance.getLook()[3]   = 589;
                    appearance.getLook()[4]   = 365;
                    appearance.getLook()[5]   = 621;
                    appearance.getLook()[6]   = 428;
                    appearance.getColour()[1] = 69;
                    appearance.getColour()[2] = 69;
                    appearance.getColour()[3] = 154;
                } else {
                    appearance.getLook()[2]   = 566;
                    appearance.getLook()[3]   = 396;
                    appearance.getLook()[4]   = 508;
                    appearance.getLook()[5]   = 476;
                    appearance.getLook()[6]   = 540;
                    appearance.getColour()[1] = 69;
                    appearance.getColour()[2] = 69;
                    appearance.getColour()[3] = 154;
                }
            } else if (pla.getAccount().getInt32("charStage") == 10) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 465;
                    appearance.getLook()[3]   = 599;
                    appearance.getLook()[4]   = 375;
                    appearance.getLook()[5]   = 631;
                    appearance.getLook()[6]   = 436;
                    appearance.getColour()[1] = 205;
                    appearance.getColour()[2] = 206;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 575;
                    appearance.getLook()[3]   = 406;
                    appearance.getLook()[4]   = 518;
                    appearance.getLook()[5]   = 486;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 205;
                    appearance.getColour()[2] = 206;
                    appearance.getColour()[3] = 4;
                }
                ;

                appearance.getColour()[3] = 4;
            } else if (pla.getAccount().getInt32("charStage") == 11) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 460;
                    appearance.getLook()[3]   = 592;
                    appearance.getLook()[4]   = 368;
                    appearance.getLook()[5]   = 624;
                    appearance.getLook()[6]   = 431;
                    appearance.getColour()[1] = 93;
                    appearance.getColour()[2] = 94;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 569;
                    appearance.getLook()[3]   = 399;
                    appearance.getLook()[4]   = 511;
                    appearance.getLook()[5]   = 479;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 93;
                    appearance.getColour()[2] = 94;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 12) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 456;
                    appearance.getLook()[3]   = 589;
                    appearance.getLook()[4]   = 9;
                    appearance.getLook()[5]   = 651;
                    appearance.getLook()[6]   = 442;
                    appearance.getColour()[1] = 141;
                    appearance.getColour()[2] = 141;
                    appearance.getColour()[3] = 118;
                } else {
                    appearance.getLook()[2]   = 558;
                    appearance.getLook()[3]   = 399;
                    appearance.getLook()[4]   = 538;
                    appearance.getLook()[5]   = 506;
                    appearance.getLook()[6]   = 555;
                    appearance.getColour()[1] = 141;
                    appearance.getColour()[2] = 141;
                    appearance.getColour()[3] = 118;
                }
            } else if (pla.getAccount().getInt32("charStage") == 13) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 459;
                    appearance.getLook()[3]   = 591;
                    appearance.getLook()[4]   = 367;
                    appearance.getLook()[5]   = 623;
                    appearance.getLook()[6]   = 430;
                    appearance.getColour()[1] = 133;
                    appearance.getColour()[2] = 134;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 568;
                    appearance.getLook()[3]   = 398;
                    appearance.getLook()[4]   = 510;
                    appearance.getLook()[5]   = 478;
                    appearance.getLook()[6]   = 542;
                    appearance.getColour()[1] = 133;
                    appearance.getColour()[2] = 134;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 14) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 462;
                    appearance.getLook()[3]   = 594;
                    appearance.getLook()[4]   = 370;
                    appearance.getLook()[5]   = 626;
                    appearance.getLook()[6]   = 433;
                    appearance.getColour()[1] = 85;
                    appearance.getColour()[2] = 85;
                    appearance.getColour()[3] = 69;
                } else {
                    appearance.getLook()[2]   = 571;
                    appearance.getLook()[3]   = 401;
                    appearance.getLook()[4]   = 513;
                    appearance.getLook()[5]   = 481;
                    appearance.getLook()[6]   = 544;
                    appearance.getColour()[1] = 85;
                    appearance.getColour()[2] = 86;
                    appearance.getColour()[3] = 69;
                }
            } else if (pla.getAccount().getInt32("charStage") == 15) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 467;
                    appearance.getLook()[3]   = 601;
                    appearance.getLook()[4]   = 377;
                    appearance.getLook()[5]   = 633;
                    appearance.getLook()[6]   = 438;
                    appearance.getColour()[1] = 173;
                    appearance.getColour()[2] = 174;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 577;
                    appearance.getLook()[3]   = 408;
                    appearance.getLook()[4]   = 520;
                    appearance.getLook()[5]   = 488;
                    appearance.getLook()[6]   = 549;
                    appearance.getColour()[1] = 173;
                    appearance.getColour()[2] = 174;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 16) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 461;
                    appearance.getLook()[3]   = 593;
                    appearance.getLook()[4]   = 369;
                    appearance.getLook()[5]   = 625;
                    appearance.getLook()[6]   = 432;
                    appearance.getColour()[1] = 77;
                    appearance.getColour()[2] = 78;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 570;
                    appearance.getLook()[3]   = 400;
                    appearance.getLook()[4]   = 512;
                    appearance.getLook()[5]   = 480;
                    appearance.getLook()[6]   = 543;
                    appearance.getColour()[1] = 77;
                    appearance.getColour()[2] = 78;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 17) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 445;
                    appearance.getLook()[3]   = 589;
                    appearance.getLook()[4]   = 366;
                    appearance.getLook()[5]   = 622;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 101;
                    appearance.getColour()[2] = 102;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 567;
                    appearance.getLook()[3]   = 397;
                    appearance.getLook()[4]   = 509;
                    appearance.getLook()[5]   = 477;
                    appearance.getLook()[6]   = 541;
                    appearance.getColour()[1] = 101;
                    appearance.getColour()[2] = 102;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 18) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 446;
                    appearance.getLook()[3]   = 599;
                    appearance.getLook()[4]   = 374;
                    appearance.getLook()[5]   = 630;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 109;
                    appearance.getColour()[2] = 109;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 574;
                    appearance.getLook()[3]   = 405;
                    appearance.getLook()[4]   = 517;
                    appearance.getLook()[5]   = 485;
                    appearance.getLook()[6]   = 548;
                    appearance.getColour()[1] = 109;
                    appearance.getColour()[2] = 109;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 19) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 466;
                    appearance.getLook()[3]   = 600;
                    appearance.getLook()[4]   = 376;
                    appearance.getLook()[5]   = 632;
                    appearance.getLook()[6]   = 437;
                    appearance.getColour()[1] = 181;
                    appearance.getColour()[2] = 181;
                    appearance.getColour()[3] = 56;
                } else {
                    appearance.getLook()[2]   = 576;
                    appearance.getLook()[3]   = 407;
                    appearance.getLook()[4]   = 519;
                    appearance.getLook()[5]   = 487;
                    appearance.getLook()[6]   = 553;
                    appearance.getColour()[1] = 181;
                    appearance.getColour()[2] = 181;
                    appearance.getColour()[3] = 158;
                }
            } else if (pla.getAccount().getInt32("charStage") == 20) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 463;
                    appearance.getLook()[3]   = 596;
                    appearance.getLook()[4]   = 372;
                    appearance.getLook()[5]   = 628;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 157;
                    appearance.getColour()[2] = 157;
                    appearance.getColour()[3] = 134;
                } else {
                    appearance.getLook()[2]   = 572;
                    appearance.getLook()[3]   = 403;
                    appearance.getLook()[4]   = 515;
                    appearance.getLook()[5]   = 483;
                    appearance.getLook()[6]   = 546;
                    appearance.getColour()[1] = 157;
                    appearance.getColour()[2] = 157;
                    appearance.getColour()[3] = 54;
                }
            }

            break;

        case 84 :
            if (pla.getAccount().getInt32("charStage") == 1) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 443;
                    appearance.getLook()[3]   = 614;
                    appearance.getLook()[4]   = 390;
                    appearance.getLook()[5]   = 646;
                    appearance.getLook()[6]   = 440;
                    appearance.getColour()[1] = 37;
                    appearance.getColour()[2] = 213;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 584;
                    appearance.getLook()[3]   = 421;
                    appearance.getLook()[4]   = 533;
                    appearance.getLook()[5]   = 501;
                    appearance.getLook()[6]   = 553;
                    appearance.getColour()[1] = 45;
                    appearance.getColour()[2] = 221;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 2) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 454;
                    appearance.getLook()[4]   = 381;
                    appearance.getLook()[5]   = 637;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 197;
                    appearance.getColour()[2] = 202;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 563;
                    appearance.getLook()[3]   = 425;
                    appearance.getLook()[4]   = 524;
                    appearance.getLook()[5]   = 492;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 197;
                    appearance.getColour()[2] = 202;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 3) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 448;
                    appearance.getLook()[4]   = 387;
                    appearance.getLook()[5]   = 643;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 125;
                    appearance.getColour()[2] = 125;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 582;
                    appearance.getLook()[3]   = 418;
                    appearance.getLook()[4]   = 530;
                    appearance.getLook()[5]   = 498;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 125;
                    appearance.getColour()[2] = 129;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 4) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 470;
                    appearance.getLook()[3]   = 608;
                    appearance.getLook()[4]   = 384;
                    appearance.getLook()[5]   = 640;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 149;
                    appearance.getColour()[2] = 150;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 559;
                    appearance.getLook()[3]   = 415;
                    appearance.getLook()[4]   = 527;
                    appearance.getLook()[5]   = 495;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 149;
                    appearance.getColour()[2] = 150;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 15) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 457;
                    appearance.getLook()[3]   = 588;
                    appearance.getLook()[4]   = 364;
                    appearance.getLook()[5]   = 620;
                    appearance.getLook()[6]   = 427;
                    appearance.getColour()[1] = 61;
                    appearance.getColour()[2] = 61;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 565;
                    appearance.getLook()[3]   = 395;
                    appearance.getLook()[4]   = 507;
                    appearance.getLook()[5]   = 475;
                    appearance.getLook()[6]   = 539;
                    appearance.getColour()[1] = 61;
                    appearance.getColour()[2] = 61;
                    appearance.getColour()[3] = 4;
                }
            }

            break;

        case 85 :
            if (pla.getAccount().getInt32("charStage") == 1) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 472;
                    appearance.getLook()[3]   = 615;
                    appearance.getLook()[4]   = 391;
                    appearance.getLook()[5]   = 647;
                    appearance.getLook()[6]   = 441;
                    appearance.getColour()[1] = 37;
                    appearance.getColour()[2] = 213;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 585;
                    appearance.getLook()[3]   = 422;
                    appearance.getLook()[4]   = 534;
                    appearance.getLook()[5]   = 502;
                    appearance.getLook()[6]   = 554;
                    appearance.getColour()[1] = 45;
                    appearance.getColour()[2] = 221;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 2) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 455;
                    appearance.getLook()[4]   = 382;
                    appearance.getLook()[5]   = 638;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 197;
                    appearance.getColour()[2] = 202;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 564;
                    appearance.getLook()[3]   = 425;
                    appearance.getLook()[4]   = 525;
                    appearance.getLook()[5]   = 493;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 197;
                    appearance.getColour()[2] = 202;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 3) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 449;
                    appearance.getLook()[4]   = 388;
                    appearance.getLook()[5]   = 644;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 125;
                    appearance.getColour()[2] = 125;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 557;
                    appearance.getLook()[3]   = 419;
                    appearance.getLook()[4]   = 531;
                    appearance.getLook()[5]   = 499;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 125;
                    appearance.getColour()[2] = 125;
                    appearance.getColour()[3] = 4;
                }
            } else if (pla.getAccount().getInt32("charStage") == 4) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 450;
                    appearance.getLook()[3]   = 609;
                    appearance.getLook()[4]   = 385;
                    appearance.getLook()[5]   = 641;
                    appearance.getLook()[6]   = 429;
                    appearance.getColour()[1] = 149;
                    appearance.getColour()[2] = 150;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 580;
                    appearance.getLook()[3]   = 416;
                    appearance.getLook()[4]   = 528;
                    appearance.getLook()[5]   = 496;
                    appearance.getLook()[6]   = 552;
                    appearance.getColour()[1] = 149;
                    appearance.getColour()[2] = 150;
                    appearance.getColour()[3] = 4;
                }
            }

            break;

        case 86 :
            if (pla.getAccount().getInt32("charStage") == 1) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 473;
                    appearance.getLook()[3]   = 616;
                    appearance.getLook()[4]   = 392;
                    appearance.getLook()[5]   = 648;
                    appearance.getLook()[6]   = 441;
                    appearance.getColour()[1] = 37;
                    appearance.getColour()[2] = 213;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 586;
                    appearance.getLook()[3]   = 423;
                    appearance.getLook()[4]   = 535;
                    appearance.getLook()[5]   = 503;
                    appearance.getLook()[6]   = 553;
                    appearance.getColour()[1] = 53;
                    appearance.getColour()[2] = 53;
                    appearance.getColour()[3] = 155;
                }
            }

            break;

        case 87 :
            if (pla.getAccount().getInt32("charStage") == 1) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 444;
                    appearance.getLook()[3]   = 617;
                    appearance.getLook()[4]   = 393;
                    appearance.getLook()[5]   = 649;
                    appearance.getLook()[6]   = 441;
                    appearance.getColour()[1] = 37;
                    appearance.getColour()[2] = 213;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 556;
                    appearance.getLook()[3]   = 424;
                    appearance.getLook()[4]   = 536;
                    appearance.getLook()[5]   = 504;
                    appearance.getLook()[6]   = 554;
                    appearance.getColour()[1] = 45;
                    appearance.getColour()[2] = 221;
                    appearance.getColour()[3] = 4;
                }
            }

            break;

        case 88 :
            if (pla.getAccount().getInt32("charStage") == 1) {
                if (appearance.getGender() == 0) {
                    appearance.getLook()[2]   = 474;
                    appearance.getLook()[3]   = 618;
                    appearance.getLook()[4]   = 394;
                    appearance.getLook()[5]   = 650;
                    appearance.getLook()[6]   = 441;
                    appearance.getColour()[1] = 37;
                    appearance.getColour()[2] = 213;
                    appearance.getColour()[3] = 4;
                } else {
                    appearance.getLook()[2]   = 587;
                    appearance.getLook()[3]   = 425;
                    appearance.getLook()[4]   = 537;
                    appearance.getLook()[5]   = 505;
                    appearance.getLook()[6]   = 551;
                    appearance.getColour()[1] = 53;
                    appearance.getColour()[2] = 53;
                    appearance.getColour()[3] = 4;
                }
            }

            break;

        case 49 :
            pla.getAccount().setSetting("charStage", 2);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 453;
                appearance.getLook()[3]   = 619;
                appearance.getLook()[4]   = 380;
                appearance.getLook()[5]   = 636;
                appearance.getLook()[6]   = 429;
                appearance.getColour()[1] = 197;
                appearance.getColour()[2] = 202;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 562;
                appearance.getLook()[3]   = 425;
                appearance.getLook()[4]   = 523;
                appearance.getLook()[5]   = 491;
                appearance.getLook()[6]   = 551;
                appearance.getColour()[1] = 197;
                appearance.getColour()[2] = 202;
                appearance.getColour()[3] = 4;
            }

            break;

        case 50 :
            pla.getAccount().setSetting("charStage", 3);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 447;
                appearance.getLook()[4]   = 386;
                appearance.getLook()[5]   = 642;
                appearance.getLook()[6]   = 429;
                appearance.getColour()[1] = 125;
                appearance.getColour()[2] = 125;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 581;
                appearance.getLook()[3]   = 417;
                appearance.getLook()[4]   = 529;
                appearance.getLook()[5]   = 497;
                appearance.getLook()[6]   = 551;
                appearance.getColour()[1] = 125;
                appearance.getColour()[2] = 125;
                appearance.getColour()[3] = 4;
            }

            break;

        case 51 :
            pla.getAccount().setSetting("charStage", 4);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 469;
                appearance.getLook()[3]   = 607;
                appearance.getLook()[4]   = 383;
                appearance.getLook()[5]   = 639;
                appearance.getLook()[6]   = 431;
                appearance.getColour()[1] = 149;
                appearance.getColour()[2] = 150;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 579;
                appearance.getLook()[3]   = 414;
                appearance.getLook()[4]   = 526;
                appearance.getLook()[5]   = 494;
                appearance.getLook()[6]   = 551;
                appearance.getColour()[1] = 149;
                appearance.getColour()[2] = 149;
                appearance.getColour()[3] = 4;
            }

            break;

        case 52 :
            pla.getAccount().setSetting("charStage", 5);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 451;
                appearance.getLook()[3]   = 592;
                appearance.getLook()[4]   = 378;
                appearance.getLook()[5]   = 634;
                appearance.getLook()[6]   = 429;
                appearance.getColour()[1] = 165;
                appearance.getColour()[2] = 165;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 560;
                appearance.getLook()[3]   = 416;
                appearance.getLook()[4]   = 521;
                appearance.getLook()[5]   = 489;
                appearance.getLook()[6]   = 551;
                appearance.getColour()[1] = 165;
                appearance.getColour()[2] = 166;
                appearance.getColour()[3] = 142;
            }

            break;

        case 53 :
            pla.getAccount().setSetting("charStage", 6);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 452;
                appearance.getLook()[3]   = 592;
                appearance.getLook()[4]   = 371;
                appearance.getLook()[5]   = 627;
                appearance.getLook()[6]   = 434;
                appearance.getColour()[1] = 189;
                appearance.getColour()[2] = 189;
                appearance.getColour()[3] = 39;
            } else {
                appearance.getLook()[2]   = 561;
                appearance.getLook()[3]   = 416;
                appearance.getLook()[4]   = 514;
                appearance.getLook()[5]   = 482;
                appearance.getLook()[6]   = 545;
                appearance.getColour()[1] = 189;
                appearance.getColour()[2] = 189;
                appearance.getColour()[3] = 39;
            }

            break;

        case 54 :
            pla.getAccount().setSetting("charStage", 7);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 464;
                appearance.getLook()[3]   = 597;
                appearance.getLook()[4]   = 373;
                appearance.getLook()[5]   = 629;
                appearance.getLook()[6]   = 435;
                appearance.getColour()[1] = 29;
                appearance.getColour()[2] = 30;
                appearance.getColour()[3] = 0;
            } else {
                appearance.getLook()[2]   = 573;
                appearance.getLook()[3]   = 404;
                appearance.getLook()[4]   = 516;
                appearance.getLook()[5]   = 484;
                appearance.getLook()[6]   = 547;
                appearance.getColour()[1] = 29;
                appearance.getColour()[2] = 30;
                appearance.getColour()[3] = 0;
            }

            break;

        case 55 :
            pla.getAccount().setSetting("charStage", 8);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 468;
                appearance.getLook()[3]   = 603;
                appearance.getLook()[4]   = 379;
                appearance.getLook()[5]   = 635;
                appearance.getLook()[6]   = 429;
                appearance.getColour()[1] = 117;
                appearance.getColour()[2] = 118;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 578;
                appearance.getLook()[3]   = 410;
                appearance.getLook()[4]   = 522;
                appearance.getLook()[5]   = 490;
                appearance.getLook()[6]   = 550;
                appearance.getColour()[1] = 117;
                appearance.getColour()[2] = 118;
                appearance.getColour()[3] = 4;
            }

            break;

        case 56 :
            pla.getAccount().setSetting("charStage", 9);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 458;
                appearance.getLook()[3]   = 589;
                appearance.getLook()[4]   = 365;
                appearance.getLook()[5]   = 621;
                appearance.getLook()[6]   = 428;
                appearance.getColour()[1] = 69;
                appearance.getColour()[2] = 69;
                appearance.getColour()[3] = 154;
            } else {
                appearance.getLook()[2]   = 566;
                appearance.getLook()[3]   = 396;
                appearance.getLook()[4]   = 508;
                appearance.getLook()[5]   = 476;
                appearance.getLook()[6]   = 540;
                appearance.getColour()[1] = 69;
                appearance.getColour()[2] = 69;
                appearance.getColour()[3] = 154;
            }

            break;

        case 57 :
            pla.getAccount().setSetting("charStage", 10);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 465;
                appearance.getLook()[3]   = 599;
                appearance.getLook()[4]   = 375;
                appearance.getLook()[5]   = 631;
                appearance.getLook()[6]   = 436;
                appearance.getColour()[1] = 205;
                appearance.getColour()[2] = 206;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 575;
                appearance.getLook()[3]   = 406;
                appearance.getLook()[4]   = 518;
                appearance.getLook()[5]   = 486;
                appearance.getLook()[6]   = 551;
                appearance.getColour()[1] = 205;
                appearance.getColour()[2] = 206;
                appearance.getColour()[3] = 4;
            }

            break;

        case 58 :
            pla.getAccount().setSetting("charStage", 11);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 460;
                appearance.getLook()[3]   = 592;
                appearance.getLook()[4]   = 368;
                appearance.getLook()[5]   = 624;
                appearance.getLook()[6]   = 431;
                appearance.getColour()[1] = 93;
                appearance.getColour()[2] = 94;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 569;
                appearance.getLook()[3]   = 399;
                appearance.getLook()[4]   = 511;
                appearance.getLook()[5]   = 479;
                appearance.getLook()[6]   = 551;
                appearance.getColour()[1] = 93;
                appearance.getColour()[2] = 94;
                appearance.getColour()[3] = 4;
            }

            break;

        case 59 :
            pla.getAccount().setSetting("charStage", 12);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 456;
                appearance.getLook()[3]   = 589;
                appearance.getLook()[4]   = 9;
                appearance.getLook()[5]   = 651;
                appearance.getLook()[6]   = 442;
                appearance.getColour()[1] = 141;
                appearance.getColour()[2] = 141;
                appearance.getColour()[3] = 118;
            } else {
                appearance.getLook()[2]   = 558;
                appearance.getLook()[3]   = 399;
                appearance.getLook()[4]   = 538;
                appearance.getLook()[5]   = 506;
                appearance.getLook()[6]   = 555;
                appearance.getColour()[1] = 141;
                appearance.getColour()[2] = 141;
                appearance.getColour()[3] = 118;
            }

            break;

        case 60 :
            pla.getAccount().setSetting("charStage", 13);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 459;
                appearance.getLook()[3]   = 591;
                appearance.getLook()[4]   = 367;
                appearance.getLook()[5]   = 623;
                appearance.getLook()[6]   = 430;
                appearance.getColour()[1] = 133;
                appearance.getColour()[2] = 134;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 568;
                appearance.getLook()[3]   = 398;
                appearance.getLook()[4]   = 510;
                appearance.getLook()[5]   = 478;
                appearance.getLook()[6]   = 542;
                appearance.getColour()[1] = 133;
                appearance.getColour()[2] = 134;
                appearance.getColour()[3] = 4;
            }

            break;

        case 61 :
            pla.getAccount().setSetting("charStage", 14);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 462;
                appearance.getLook()[3]   = 594;
                appearance.getLook()[4]   = 370;
                appearance.getLook()[5]   = 626;
                appearance.getLook()[6]   = 433;
                appearance.getColour()[1] = 85;
                appearance.getColour()[2] = 85;
                appearance.getColour()[3] = 69;
            } else {
                appearance.getLook()[2]   = 571;
                appearance.getLook()[3]   = 401;
                appearance.getLook()[4]   = 513;
                appearance.getLook()[5]   = 481;
                appearance.getLook()[6]   = 544;
                appearance.getColour()[1] = 85;
                appearance.getColour()[2] = 86;
                appearance.getColour()[3] = 69;
            }

            break;

        case 62 :
            pla.getAccount().setSetting("charStage", 15);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 467;
                appearance.getLook()[3]   = 601;
                appearance.getLook()[4]   = 377;
                appearance.getLook()[5]   = 633;
                appearance.getLook()[6]   = 438;
                appearance.getColour()[1] = 173;
                appearance.getColour()[2] = 174;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 577;
                appearance.getLook()[3]   = 408;
                appearance.getLook()[4]   = 520;
                appearance.getLook()[5]   = 488;
                appearance.getLook()[6]   = 549;
                appearance.getColour()[1] = 173;
                appearance.getColour()[2] = 174;
                appearance.getColour()[3] = 4;
            }

            break;

        case 63 :
            pla.getAccount().setSetting("charStage", 16);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 461;
                appearance.getLook()[3]   = 593;
                appearance.getLook()[4]   = 369;
                appearance.getLook()[5]   = 625;
                appearance.getLook()[6]   = 432;
                appearance.getColour()[1] = 77;
                appearance.getColour()[2] = 78;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 570;
                appearance.getLook()[3]   = 400;
                appearance.getLook()[4]   = 512;
                appearance.getLook()[5]   = 480;
                appearance.getLook()[6]   = 543;
                appearance.getColour()[1] = 77;
                appearance.getColour()[2] = 78;
                appearance.getColour()[3] = 4;
            }

            break;

        case 64 :
            pla.getAccount().setSetting("charStage", 17);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 445;
                appearance.getLook()[3]   = 589;
                appearance.getLook()[4]   = 366;
                appearance.getLook()[5]   = 622;
                appearance.getLook()[6]   = 429;
                appearance.getColour()[1] = 101;
                appearance.getColour()[2] = 102;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 567;
                appearance.getLook()[3]   = 397;
                appearance.getLook()[4]   = 509;
                appearance.getLook()[5]   = 477;
                appearance.getLook()[6]   = 541;
                appearance.getColour()[1] = 101;
                appearance.getColour()[2] = 102;
                appearance.getColour()[3] = 4;
            }

            break;

        case 65 :
            pla.getAccount().setSetting("charStage", 18);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 446;
                appearance.getLook()[3]   = 599;
                appearance.getLook()[4]   = 374;
                appearance.getLook()[5]   = 630;
                appearance.getLook()[6]   = 429;
                appearance.getColour()[1] = 109;
                appearance.getColour()[2] = 109;
                appearance.getColour()[3] = 4;
            } else {
                appearance.getLook()[2]   = 574;
                appearance.getLook()[3]   = 405;
                appearance.getLook()[4]   = 517;
                appearance.getLook()[5]   = 485;
                appearance.getLook()[6]   = 548;
                appearance.getColour()[1] = 109;
                appearance.getColour()[2] = 109;
                appearance.getColour()[3] = 4;
            }

            break;

        case 66 :
            pla.getAccount().setSetting("charStage", 19);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 466;
                appearance.getLook()[3]   = 600;
                appearance.getLook()[4]   = 376;
                appearance.getLook()[5]   = 632;
                appearance.getLook()[6]   = 437;
                appearance.getColour()[1] = 181;
                appearance.getColour()[2] = 181;
                appearance.getColour()[3] = 56;
            } else {
                appearance.getLook()[2]   = 576;
                appearance.getLook()[3]   = 407;
                appearance.getLook()[4]   = 519;
                appearance.getLook()[5]   = 487;
                appearance.getLook()[6]   = 553;
                appearance.getColour()[1] = 181;
                appearance.getColour()[2] = 181;
                appearance.getColour()[3] = 158;
            }

            break;

        case 67 :
            pla.getAccount().setSetting("charStage", 20);

            if (appearance.getGender() == 0) {
                appearance.getLook()[2]   = 463;
                appearance.getLook()[3]   = 596;
                appearance.getLook()[4]   = 372;
                appearance.getLook()[5]   = 628;
                appearance.getLook()[6]   = 429;
                appearance.getColour()[1] = 157;
                appearance.getColour()[2] = 157;
                appearance.getColour()[3] = 134;
            } else {
                appearance.getLook()[2]   = 572;
                appearance.getLook()[3]   = 403;
                appearance.getLook()[4]   = 515;
                appearance.getLook()[5]   = 483;
                appearance.getLook()[6]   = 546;
                appearance.getColour()[1] = 157;
                appearance.getColour()[2] = 157;
                appearance.getColour()[3] = 54;
            }

            break;



        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
