package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/07/14
 * Time: 17:50
 */
public class FurnitureInterface implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        if (player.getGameFrame().getFurnitureBuildSettings() != null) {
            player.getGameFrame().getFurnitureBuildSettings().send(player);
        }

        AccessMaskBuilder builder = new AccessMaskBuilder();

        builder.setRightClickOptionSettings(0, true);
        player.getGameFrame().sendAMask(builder.getValue(), 396, 11, 0, 7);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        player.getGameFrame().getFurnitureBuildSettings().onButton(player, slot);
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (button == 11) {
            pla.getGameFrame().getFurnitureBuildSettings().onButton(pla, slot);
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
