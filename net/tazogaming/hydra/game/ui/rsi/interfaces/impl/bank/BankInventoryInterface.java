package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.bank;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/07/14
 * Time: 23:32
 */
public class BankInventoryInterface implements InterfaceAdapter, AmountTrigger {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {}

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        switch (clickSlot) {
        case 0 :
            player.getBank().bankItem(slot, 1);

            break;

        case 1 :
            player.getBank().bankItem(slot, 5);

            break;

        case 2 :
            player.getBank().bankItem(slot, 10);

            break;

        case 3 :
            player.getBank().bankItem(slot, player.getGameFrame().getLastAmount());

            break;

        case 4 :    // bank x
            player.getGameFrame().requestAmount("Please enter an amount to deposit", slot, this);

            break;

        case 5 :
            player.getBank().bankItem(slot, Integer.MAX_VALUE);

            break;
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {
        player.getInventory().moveItems(from, to);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}

    /**
     * Method amountEntered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param itemSlot
     * @param amount
     */
    @Override
    public void amountEntered(Player pla, int itemSlot, int amount) {
        pla.getBank().bankItem(itemSlot, amount);
    }
}
