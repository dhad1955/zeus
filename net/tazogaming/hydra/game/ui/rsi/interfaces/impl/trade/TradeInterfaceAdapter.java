package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.trade;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/07/14
 * Time: 14:05
 */
public class TradeInterfaceAdapter implements InterfaceAdapter {

    /*
     *   public void openFirstTradeScreen(Player p) {
     *   ActionSender.sendTradeOptions(p);
     *   ActionSender.sendInterface(p, 335);
     *   ActionSender.sendInventoryInterface(p, 336);
     *   ActionSender.sendItems(p, 90, traderItemsOffered, false);
     *   ActionSender.sendItems(p, 90, partnerItemsOffered, true);
     *   ActionSender.sendString(p, "", 335, 37);
     *   String name = p.equals(trader) ? partner.getUsername() : trader.getUsername();
     *   ActionSender.sendString(p, "Trading with: " + Misc.formatPlayerNameForDisplay(name), 335, 15);
     *   ActionSender.sendString(p, Misc.formatPlayerNameForDisplay(name), 335, 22);
     * }
     *
     * public void openSecondTradeScreen(Player p) {
     *   currentState = TradeState.STATE_TWO;
     *   partnerDidAccept = false;
     *   traderDidAccept = false;
     *   ActionSender.sendInterface(p, 334);
     *   ActionSender.sendString(p, "<col=00FFFF>Trading with:<br><col=00FFFF>" + Misc.formatPlayerNameForDisplay(p.equals(trader) ? partner.getUsername() : trader.getUsername()), 334, 54);
     *   ActionSender.sendString(p, "Are you sure you want to make this trade?", 334, 34);
     * }
     *
     */

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        player.getGameFrame().getTrade().init(player);

        CS2Call toptions1 = new CS2Call(150);

        toptions1.addString("").addString("").addString("").addString("Remove-X").addString("Remove-All").addString(
            "Remove-10").addString("Remove-5").addString("Remove").addInt(-1).addInt(0).addInt(7).addInt(4).addInt(
            90).addInt(335 << 16 | 31);
        player.getActionSender().sendClientScript(toptions1);
        player.getGameFrame().sendAMask(1026, 335, 34, 0, 27);
        toptions1 = new CS2Call(695);

        for (int i = 0; i < 9; i++) {
            toptions1.addString("");
        }

        toptions1.addInt(-1).addInt(0).addInt(7).addInt(4).addInt(90).addInt(335 << 16 | 34);
        player.getActionSender().sendClientScript(toptions1);
        player.getGameFrame().sendAMask(1026, 335, 34, 0, 27);
        toptions1 = new CS2Call(150).addString("").addString("").addString("").addString("").addString(
            "Offer-X").addString("Offer-All").addString("Offer-10").addString("Offer-5").addString("Offer").addInt(
            -1).addInt(0).addInt(7).addInt(4).addInt(93).addInt(336 << 16);
        player.getActionSender().sendClientScript(toptions1);
        player.getGameFrame().sendAMask(1278, 336, 0, 0, 27);
        player.getGameFrame().setInventoryInterface(336, 90);
        player.getGameFrame().sendAMask(1150, 335, 31, 0, 27);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {
        if (player.getGameFrame().getTrade() != null) {
            player.getGameFrame().getTrade().declineTrade(player);
        }
    }

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, final int slot, int clickSlot, final Player player) {
        if (slot <= 28) {
            switch (clickSlot) {
            case 0 :
                player.getGameFrame().getTrade().removeItem(player, slot, 1);

                break;

            case 1 :
                player.getGameFrame().getTrade().removeItem(player, slot, 5);

                break;

            case 2 :
                player.getGameFrame().getTrade().removeItem(player, slot, 10);

                break;

            case 3 :
                player.getGameFrame().getTrade().removeItem(player, slot, Integer.MAX_VALUE);

                break;

            case 4 :
                player.getGameFrame().requestAmount("Please type how many items you want to remove",
                        new AmountTrigger() {
                    @Override
                    public void amountEntered(Player pla, int itemSlot, int amount) {
                        pla.getGameFrame().getTrade().removeItem(pla, slot, amount);
                    }
                });
            }
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (button == 16) {
            pla.getGameFrame().getTrade().accept(pla);
        } else if (button == 18) {
            pla.getGameFrame().getTrade().declineTrade(pla);
            pla.getGameFrame().close();
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
