package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.SummoningMovement;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/08/14
 * Time: 10:29
 */
public class SummoningTab implements InterfaceAdapter {
    public static final int
        RENEW    = 69,
        CALL     = 49,
        DISMISS  = 51,
        TAKE_BOB = 67;

    /**
     * Method updateTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void updateTime(Player player) {
        int time    = (player.getTimers().getAbsTime(TimingUtility.SUMMON_TIMER));
        int seconds = Core.getSecondsForTicks(time);

        player.getActionSender().sendVar(1176, 65 + (((seconds / 30) * 65)));
        player.getGameFrame().sendHideIComponent(747, 9, false);
        player.getGameFrame().sendHideIComponent(747, 8, true);
    }

    /**
     * Method updateSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void updateSpecial(Player player) {
        player.getActionSender().sendVar(1177, player.getSummonSpecPoints());
    }

    /**
     * Method setFollower
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param pouchID
     */
    public static void setFollower(Player player, int pouchID) {
        if (pouchID == -1) {
            player.getActionSender().sendVar(168, 1);
            player.getActionSender().sendVar(1160, 0);

            // remove
            return;
        }

        player.getActionSender().sendVar(1174, -1);
        player.getActionSender().sendVar(1160, 243269632);
        player.getActionSender().sendVar(448, pouchID);
        player.getActionSender().sendVar(168, 8);
        player.getGameFrame().sendGlobalString(204, Summoning.getSpecName(pouchID));
        player.getGameFrame().sendGlobalString(205, "A powerful special attack");
        updateSpecial(player);
        player.getActionSender().sendBConfig(1436, 1);
        player.getGameFrame().sendIComponentSettings(747, 18, 0, 0, 2);

        if (player.isInWilderness()) {
            player.getCurrentFamiliar().setTransform(player.getCurrentFamiliar().getId() + 1);
        }

        AccessMaskBuilder bb = new AccessMaskBuilder();

        bb.setRightClickOptionSettings(0, true);

        for (int i = 0; i < 9; i++) {
            bb.setRightClickOptionSettings(i, true);
        }

        bb.setUseOnSettings(AccessMaskBuilder.Settings.INTERFACECOMPONENT);
        player.getGameFrame().sendIComponentSettings(747, 18, 0, 0, 2);
        player.getGameFrame().sendIComponentSettings(662, 74, 0, 0, 20480);
        player.getGameFrame().sendAMask(20480, 662, 74, -1, -1);
        player.getActionSender().sendBConfig(1436, 0);
        player.getGameFrame().sendHideIComponent(747, 9, false);
        player.getGameFrame().sendHideIComponent(747, 8, true);
        player.getGameFrame().sendHideIComponent(747, 7, true);
        player.getGameFrame().sendHideIComponent(747, 2, true);
        player.getGameFrame().sendHideIComponent(747, 17, true);

        for (int i = 0; i < 6; i++) {
            player.getGameFrame().sendHideIComponent(747, i, true);
        }

        if (pouchID == 12089  || Summoning.getConfig(pouchID).isClickSpec) {
            player.getGameFrame().sendIComponentSettings(747, 17, 0, 0, 2);
            player.getGameFrame().sendIComponentSettings(662, 74, 0, 0, 2);
            player.getActionSender().sendBConfig(1436, 1);
            player.getActionSender().sendClientScript(new CS2Call(608));
        } else {
            player.getGameFrame().sendIComponentSettings(747, 17, 0, 0, 20480);
            player.getActionSender().sendBConfig(1436, 0);
        }

        player.getGameFrame().sendHideIComponent(747, 9, false);
        player.getActionSender().sendVar(1494, player.getAccount().getLeftClickSummoningOption());
        player.getActionSender().sendVar(1494, player.getAccount().getLeftClickSummoningOption());
    }

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        if (player.getCurrentFamiliar() != null) {
            setFollower(player, player.getAccount().getInt32("summon_id"));
        }

        player.getGameFrame().sendIComponentSettings(662, 74, 0, 0, 20480);
        player.getGameFrame().sendAMask(20480, 662, 74, -1, -1);

        // sets up for summoning mode
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {
        player.getGameFrame().sendHideIComponent(662, 44, true);
        player.getGameFrame().sendHideIComponent(662, 45, true);
        player.getGameFrame().sendHideIComponent(662, 46, true);
        player.getGameFrame().sendHideIComponent(662, 47, true);
        player.getGameFrame().sendHideIComponent(662, 48, true);
        player.getGameFrame().sendHideIComponent(662, 71, false);
        player.getGameFrame().sendHideIComponent(662, 72, false);

        player.getGameFrame().sendHideIComponent(747, 8, true);


        player.getGameFrame().sendHideIComponent(747, 9, false);

        AccessMaskBuilder bb = new AccessMaskBuilder();

        bb.setRightClickOptionSettings(0, true);

        for (int i = 0; i < 9; i++) {
            bb.setRightClickOptionSettings(i, true);
        }

        bb.setUseOnSettings(AccessMaskBuilder.Settings.INTERFACECOMPONENT);
        player.getGameFrame().sendAMask(bb.getValue(), 662, 74, 0, 0);    // Special move thingy.
        player.getGameFrame().sendIComponentSettings(747, 18, 0, 0, 2);
    }

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        switch (button) {
        case RENEW :
            int pouch = pla.getAccount().getInt32("summon_id");

            if (!pla.getInventory().hasItem(pouch, 1)) {
                pla.getActionSender().sendMessage("You need a " + Item.forId(pouch).getName() + " to do this");

                return;
            }

            pla.getInventory().deleteItem(pouch, 1);

            int time = Summoning.getRenewTime(pouch);

            pla.getTimers().setTime(TimingUtility.SUMMON_TIMER, (int) time);
            pla.getActionSender().sendMessage("You renew your pouch");

            break;

        case CALL :
            NPC npc = pla.getCurrentFamiliar();

            if (npc.getMovement() instanceof SummoningMovement) {
                ((SummoningMovement) npc.getMovement()).teleportToMaster();
            } else {
                npc.getCombatHandler().reset();
                npc.setMovement(new SummoningMovement(npc, pla));
            }

            break;

        case DISMISS :
            pla.getActionSender().sendMessage("You banish your " + pla.getCurrentFamiliar().getDefinition().getName());
            pla.resetSummon();

            break;

        case TAKE_BOB :
            if (pla.getAccount().getInt32("bob_max") <= 0) {
                pla.getActionSender().sendMessage("This familiar is not a beast of burden");

                return;
            }

            BOBInterface.takeAll(pla);

            break;
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
