package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.equipment;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Created by Dan on 01/01/2016.
 */

public class EquipmentInventoryScreenAdapter implements InterfaceAdapter {
    @Override
    public void interfaceOpened(Player player) {
        player.getGameFrame().sendAMask(0, 27, 670, 0, 36, 1086);
    }

    @Override
    public void interfaceClosed(Player player) {

    }

    @Override
    public void interfaceRefreshed(Player player) {

    }

    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        player.getEquipment().wieldItem(slot);

    }

    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {

    }

    @Override
    public void itemsSwitched(int from, int to, Player player) {

    }

    @Override
    public void tick(Player player) {

    }
}
