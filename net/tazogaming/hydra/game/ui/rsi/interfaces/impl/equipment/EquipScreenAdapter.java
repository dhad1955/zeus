package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.equipment;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/07/14
 * Time: 07:33
 */
public class EquipScreenAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        GameFrame frame   = player.getGameFrame();
        int[] normBonuses = player.getEquipment().getTotalBonuses();
        String[]  bonuses = formatBonuses(normBonuses);

        frame.sendString("Stab: " + bonuses[Item.BONUS_ATTACK_STAB], 667, 30);
        frame.sendString("Slash: " + bonuses[Item.BONUS_ATTACK_SLASH], 667, 31);
        frame.sendString("Crush: " + bonuses[Item.BONUS_ATTACK_CRUSH], 667, 32);
        frame.sendString("Magic: " + bonuses[Item.BONUS_ATTACK_MAGIC], 667, 33);
        frame.sendString("Ranged: " + bonuses[Item.BONUS_ATTACK_RANGE], 667, 34);
        frame.sendString("Stab: " + bonuses[Item.BONUS_DEFENCE_STAB], 667, 35);
        frame.sendString("Slash: " + bonuses[Item.BONUS_DEFENCE_SLASH], 667, 36);
        frame.sendString("Crush: " + bonuses[Item.BONUS_ATTACK_CRUSH], 667, 37);
        frame.sendString("Magic: " + bonuses[Item.BONUS_DEFENCE_MAGIC], 667, 38);
        frame.sendString("Ranged:" + bonuses[Item.BONUS_DEFENCE_RANGE], 667, 39);
        frame.sendString("Absorb Melee: "+normBonuses[Item.BONUS_SOAK_MELEE]+"%", 667, 41);
        frame.sendString("Absorb Range: "+normBonuses[Item.BONUS_SOAK_RANGE]+"%", 667, 42);
        frame.sendString("Absorb Magic: "+normBonuses[Item.BONUS_SOAK_MAGE]+"%", 667, 43);

        String[] absorb = { "Melee", "Range", "Magic" };

        frame.sendString("Summoning: +0", 667, 40);


        frame.sendString("Strength: " + bonuses[Item.BONUS_STRENGTH], 667, 44);

        Player  pla = player;
        boolean b   = (player.getEquipment().getWeapon().getType() == Weapon.TYPE_THROWN)
                      || (pla.getEquipment().getId(3) == 4124) || (pla.getEquipment().getId(3) == 25)
                      || (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_SPECIALBOW)
                      || (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_SPECIAL_XBOW);

        frame.sendString("Ranged strength: +" + GameMath.getRangeStr(b
                ? pla.getEquipment().getId(Equipment.ARROWS)
                : pla.getEquipment().getId(Equipment.WEAPON)), 667, 45);
        frame.sendString("Prayer: " + bonuses[Item.BONUS_PRAYER], 667, 46);




        frame.sendString("Magic damage: "+bonuses[Item.BONUS_MAGE_BOOST]+"%", 667, 47);

        AccessMaskBuilder builder = new AccessMaskBuilder();

        builder.setRightClickOptionSettings(0, true);
        pla.getGameFrame().sendAMask(0, 15, 667, 7, 0, builder.getValue());

        pla.getActionSender().sendBConfig(779, player.getRenderAnimation());
        pla.getGameFrame().setInventoryInterface(670, 93);
    }

    private String[] formatBonuses(int[] bonuses) {
        String[] format = new String[bonuses.length];

        for (int i = 0; i < bonuses.length; i++) {
            if (bonuses[i] > 0) {
                format[i] = "+" + bonuses[i];
            } else {
                format[i] = Integer.toString(bonuses[i]);
            }
        }

        return format;
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        if (clickSlot == 0) {
            player.getEquipment().unwieldItem(slot);
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (button == 74) {
            pla.getGameFrame().silentClose();
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
