package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ModelGrid;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/08/14
 * Time: 18:43
 */
public class BOBInterface implements InterfaceAdapter {
    public static final int BOB_INTERFACE_ID = 671;
    public static final int ITEMS_KEY        = 530;
    public static int       SET_W            = 7;

    private static boolean removeBob(Player player, int slot, int amount) {

        if(amount <= 0)
            return false;

        Item item = Item.forId(player.getBobItems()[slot]);

        if (item == null) {
            return false;
        }

        if (item.isStackable()) {
            return removeBobItem(player, slot, amount);
        } else {
            if(amount > 28)
                amount = 28;

            for (int i = 0; i < amount; i++) {
                int bobSlot = getItemSlot(player, item);

                if (bobSlot == -1) {
                    return false;
                }

                removeBobItem(player, bobSlot, 1);
            }
        }

        return true;
    }

    private static boolean removeBobItem(Player player, int slot, int amount) {
        if(amount <= 0)
            return false;

        int[] bobItems = player.getBobItems();
        int[] bobC     = player.getBobItemsC();

        if (amount > bobC[slot]) {
            amount = bobC[slot];
        }

        if (bobItems[slot] == -1) {
            return false;
        }

        int slots = player.getInventory().getFreeSlots(bobItems[slot]);

        if (slots == 0) {
            player.getActionSender().sendMessage("You don't have enough space");

            return false;
        }
        player.log("bob item remove: "+bobItems[slot]+" "+amount);
        bobC[slot] -= amount;

        int save = bobItems[slot];

        if (bobC[slot] <= 0) {
            bobItems[slot] = -1;
        }

        player.getInventory().addItem(save, amount);
        player.getActionSender().sendItems(ITEMS_KEY, player.getBobItems(), player.getBobItemsC(), false);

        return true;
    }

    /**
     * Method takeAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void takeAll(Player player) {

       int max = player.getAccount().getInt32("bob_max");
        if(max > 30 || max < 0)
            max = 0;

        player.log("bob [take all]");
        player.getAccount().setSetting("bob_max", max);

        for (int i = 0; i < max; i++) {
            if (!removeBob(player, i, Integer.MAX_VALUE)) {
                continue;
            }
        }
    }

    /**
     * Method getBobFreeSlots
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static final int getBobFreeSlots(Player player) {
        int x       = 0;
        int bob_max = player.getAccount().getInt32("bob_max");

        if(bob_max > 30 || bob_max < 0)
            bob_max = 0;


        for (int i = 0; i < bob_max; i++) {
            if (player.getBobItems()[i] == -1) {
                x++;
            }
        }

        return x;
    }

    /**
     * Method getItemSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param item
     *
     * @return
     */
    public static final int getItemSlot(Player player, Item item) {
        for (int i = 0; i < player.getBobItems().length; i++) {
            if (player.getBobItems()[i] == item.getIndex()) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method findBobSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param item
     *
     * @return
     */
    public static final int findBobSlot(Player player, Item item) {
        if (item.isStackable()) {
            for (int i = 0; i < player.getBobItems().length; i++) {
                if (player.getBobItems()[i] == item.getIndex()) {
                    return i;
                }
            }
        }

        int max = player.getAccount().getInt32("bob_max");

        if(max > 30 || max < 0)
            max = 30;

        for (int i = 0; i < max; i++) {
            if (player.getBobItems()[i] == -1) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method addBobItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param slot
     * @param amount
     */
    public static void addBobItem(Player player, int slot, int amount) {
        if(amount <= 0)
            return;

        Item i = player.getInventory().getItem(slot);

        if (i == null) {
            return;
        }

        if(i.isDungItem()){
            player.getActionSender().sendMessage("No.");
            return;
        }

        int inv_amount = player.getInventory().getCount(slot);

        if (inv_amount == 0) {
            return;
        }

        int bob_slots = getBobFreeSlots(player);

        if ((bob_slots == 0) &&!i.isStackable()) {
            return;
        }

        if (i.isStackable()) {
            int bobSlot = findBobSlot(player, i);

            if (bobSlot == -1) {
                return;
            }

            int max = player.getInventory().getItemMax(slot, amount);

            if (max < amount) {
                amount = max;
            }

            if (amount == 0) {
                return;
            }
            player.log("deposit bob: "+i.getIndex()+", "+amount);
            player.getInventory().deleteItem(i.getIndex(), amount);
            player.getBobItems()[bobSlot]  = i.getIndex();
            player.getBobItemsC()[bobSlot] += amount;
            player.getActionSender().sendItems(ITEMS_KEY, player.getBobItems(), player.getBobItemsC(), false);

            return;
        } else {
            if (amount > bob_slots) {
                amount = bob_slots;
            }

            int max = player.getInventory().getItemMax(slot, amount);

            if (max < amount) {
                amount = max;
            }

            int c = 0;

            player.getInventory().deleteItem(i, amount);
            player.log("deposit bob2: "+i.getIndex()+", "+amount);

            long start = Core.currentTimeMillis();
            while (c++ != amount) {
                int nextSlot = findBobSlot(player, i);

                if(System.currentTimeMillis() - start > 100)
                    throw new RuntimeException("BOB CRASH");

                player.getBobItems()[nextSlot]  = i.getIndex();
                player.getBobItemsC()[nextSlot] = 1;
            }

            player.getActionSender().sendItems(ITEMS_KEY, player.getBobItems(), player.getBobItemsC(), false);
        }
    }

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        ModelGrid grid = new ModelGrid(ITEMS_KEY, BOB_INTERFACE_ID, 27, 5, 6);

        grid.addOption("Remove");
        grid.addOption("Remove-5");
        grid.addOption("Remove-10");
        grid.addOption("Remove-X");
        grid.addOption("Remove-All");
        player.getActionSender().sendClientScript(grid.getCS2());

        AccessMaskBuilder br = new AccessMaskBuilder();

        br.enableAllRightClickSettings();
        player.getGameFrame().sendAMask(br.getValue(), BOB_INTERFACE_ID, 27, 0, 27);
        player.getActionSender().sendItems(ITEMS_KEY, player.getBobItems(), player.getBobItemsC(), false);
        player.getGameFrame().setInventoryInterface(665, 93);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, final int slot, int clickSlot, final Player player) {
        switch (clickSlot) {
        case 0 :
            removeBob(player, slot, 1);

            break;

        case 1 :
            removeBob(player, slot, 5);

            break;

        case 2 :
            removeBob(player, slot, 10);

            break;

        case 3 :
            player.getGameFrame().requestAmount("Please enter an amount to withdraw", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    removeBob(player, slot, amount);
                }
            });

            break;

        case 4 :
            removeBob(player, slot, Integer.MAX_VALUE);

            break;
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (button == 29) {
            takeAll(pla);
            pla.getActionSender().sendItems(ITEMS_KEY, pla.getBobItems(), pla.getBobItemsC(), false);
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
