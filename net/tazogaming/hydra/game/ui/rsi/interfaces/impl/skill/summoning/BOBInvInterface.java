package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.AccessMaskBuilder;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ModelGrid;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/08/14
 * Time: 19:03
 */
public class BOBInvInterface implements InterfaceAdapter {
    public static final int BOB_ID = 665;

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        ModelGrid grid = new ModelGrid(93, BOB_ID, 0);

        grid.addOption("Store");
        grid.addOption("Store-5");
        grid.addOption("Store-10");
        grid.addOption("Store-X");
        grid.addOption("Store-All");
        player.getActionSender().sendClientScript(grid.getCS2());

        AccessMaskBuilder br = new AccessMaskBuilder();

        br.enableAllRightClickSettings();
        player.getGameFrame().sendAMask(br.getValue(), BOB_ID, 0, 0, 27);
        player.getActionSender().sendClientScript(grid.getCS2());
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, final int slot, int clickSlot, final Player player) {
        player.log("bob inventory deposit: "+itemId+" "+clickSlot);
        switch (clickSlot) {
        case 0 :
            BOBInterface.addBobItem(player, slot, 1);

            break;

        case 1 :
            BOBInterface.addBobItem(player, slot, 5);

            break;

        case 2 :
            BOBInterface.addBobItem(player, slot, 10);

            break;

        case 3 :
            player.getGameFrame().requestAmount("Please enter the amount you wish to deposit", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    BOBInterface.addBobItem(player, slot, amount);
                }
            });

            break;

        case 4 :
            BOBInterface.addBobItem(player, slot, Integer.MAX_VALUE);

            break;
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
