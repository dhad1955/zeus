package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/07/14
 * Time: 19:11
 */
public class WeaponInterfaceAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        SpecialAttacks.updateSpecial(player);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        switch (button) {
        case 11 :
            if(pla.getAutoCast() > -1) {
                pla.setRequestedSpell(-1);
                pla.setAutocast(-1);
                pla.getCombatAdapter().reset();
                pla.getRequest().close();
                pla.unFollow();
            }
            pla.getAccount().set(43, 0, true);
            pla.setAutocast(-1);
            pla.setRequestedSpell(-1);

            break;

        case 12 :
            if(pla.getAutoCast() > -1) {
                pla.setRequestedSpell(-1);
                pla.setAutocast(-1);
                pla.getCombatAdapter().reset();
                pla.getRequest().close();
                pla.unFollow();
            }
            pla.getAccount().set(43, 1, true);
            pla.setAutocast(-1);
            pla.setRequestedSpell(-1);

            break;

        case 13 :
            if(pla.getAutoCast() > -1) {
                pla.setRequestedSpell(-1);
                pla.setAutocast(-1);
                pla.getCombatAdapter().reset();
                pla.getRequest().close();
                pla.unFollow();
            }
            pla.getAccount().set(43, 2, true);
            pla.setAutocast(-1);
            pla.setRequestedSpell(-1);

            break;

        case 14 :
            if(pla.getAutoCast() > -1) {
                pla.setRequestedSpell(-1);
                pla.setAutocast(-1);
                pla.getCombatAdapter().reset();
                pla.getRequest().close();
                pla.unFollow();
            }
            pla.getAccount().set(43, 3, true);
            pla.setRequestedSpell(-1);
            pla.setAutocast(-1);

            break;

        case 4 :
           if(pla.getAutoCast() > -1) {
               pla.setRequestedSpell(-1);
               pla.setAutocast(-1);
               pla.getCombatAdapter().reset();
               pla.getRequest().close();
               pla.unFollow();
           }

            if ((pla.getEquipment().getId(3) == 4153) && pla.getCombatAdapter().isInCombat()) {
                SpecialAttacks.doSpecial(pla, pla.getCombatAdapter().getFightingWith());
            } else {
                SpecialAttacks.changeHighlight(pla);
            }

            break;

        case 15 :
            pla.getAccount().toggle(Account.AUTO_RETALIATE);

            break;
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
