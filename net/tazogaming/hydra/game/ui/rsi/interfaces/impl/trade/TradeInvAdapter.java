package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.trade;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/07/14
 * Time: 14:30
 */
public class TradeInvAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {}

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, final int slot, int clickSlot, final Player player) {
        if(player.getGameFrame().getTrade() == null) {
            player.getActionSender().sendMessage("Error with trading, please report this to admin");
            return;
        }



        if (clickSlot == 0) {
            player.getGameFrame().getTrade().tradeItem(player, slot, 1);
        }

        if (clickSlot == 1) {
            player.getGameFrame().getTrade().tradeItem(player, slot, 5);
        }

        if (clickSlot == 2) {
            player.getGameFrame().getTrade().tradeItem(player, slot, 10);
        }

        if (clickSlot == 3) {
            player.getGameFrame().getTrade().tradeItem(player, slot, Integer.MAX_VALUE);
        } else if (clickSlot == 4) {
            player.getGameFrame().requestAmount("Please type an amount to trade", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {

                    player.getGameFrame().getTrade().tradeItem(pla, slot, amount);
                }
            });
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {}

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {
        player.getInventory().moveItems(from, to);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
