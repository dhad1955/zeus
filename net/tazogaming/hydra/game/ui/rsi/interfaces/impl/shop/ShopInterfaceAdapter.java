package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.shop;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.game.ui.shops.Shop;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/07/14
 * Time: 02:42
 */
public class ShopInterfaceAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        player.getActionSender().sendVar(118, 4);                                                // no idea
        player.getActionSender().sendVar(1496, -1);
        player.getGameFrame().sendAMask(0, 12, 620, 26, 0, 1150);
        player.getGameFrame().sendAMask(0, 240, 620, 25, 0, 1150);
        player.getActionSender().sendVar(532, player.getGameFrame().getShop().getCurrency());    // currency?
        resetShop(player, player.getGameFrame().getShop());
        player.getGameFrame().setInventoryInterface(621, 93);
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {
        int[] buyAmounts = { 1, 5, 10, 50, 500 };


        if (clickSlot == 0) {
            int  itemSlot = slot / 6;
            Shop s        = player.getGameFrame().getShop();
            int  cost     = s.getCost(itemSlot);
            Item currency = Item.forId(995);


            if(s.getItem(itemSlot) == null)
                return;

            if (cost == -1) {
                cost = s.getItem(itemSlot).getPrice();
            }

            if ((s.getCurrency() != 995) && (s.getCurrency() != -1)) {
                currency = Item.forId(s.getCurrency());
            }



            if (s.getCurrencyPoints() != -1) {
                player.getActionSender().sendMessage("Shop will sell for " + (s.getCost(itemSlot)) + " "
                        + Points.getNameForId(s.getCurrencyPoints()) + " points");

                return;
            }

            player.getActionSender().sendMessage(s.getItem(itemSlot).getName() + " shop will sell for "+GameMath.formatMoney(cost)+" "
                    + currency.getName());

            // price check
        } else {
            ShopManager.buyItem(player.getGameFrame().getShop(), slot / 6, player, buyAmounts[clickSlot - 1]);
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        itemClicked(-1, slot, contextSlot, pla);

        if (button == 18) {
            pla.getGameFrame().closeAll();
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}

    /**
     * Method resetShop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param shop
     */
    public static void resetShop(Player player, Shop shop) {
        player.getActionSender().sendItems(4, shop.getItems(), shop.getItemsCount(), false);
        for(int i = 0; i < 50; i++){
            if(shop.getCost(i) == -1)
                player.getAccount().setB(946 + i, 0);
            else
                player.getAccount().setB(946 + i , -1);
        }
    }
}
