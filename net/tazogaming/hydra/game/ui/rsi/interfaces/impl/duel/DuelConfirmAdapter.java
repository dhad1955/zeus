package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.duel;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/07/14
 * Time: 16:05
 */
public class DuelConfirmAdapter implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        Duel duel = player.getGameFrame().getDuel();

        if (duel == null) {
            player.getGameFrame().close();

            return;
        }

        ArrayList<String> beforeRules = new ArrayList<String>();
        ArrayList<String> duringRules = new ArrayList<String>();
        GameFrame         gameFrame   = player.getGameFrame();
        int               mySize      = duel.getRiskAmount(player);                         // 25
        int               thereSize   = duel.getRiskAmount(duel.getOtherPlayer(player));    // 26

        gameFrame.sendString((mySize > 0)
                             ? ""
                             : "Absolutely nothing!", 626, 25);
        gameFrame.sendString((thereSize > 0)
                             ? ""
                             : "Absolutely nothing!", 626, 26);
        beforeRules.add("Boosted stats will be restored");
        beforeRules.add("Existing prayers will be stopped");
        beforeRules.add("Summoned familiars will be banished.");

        for (Integer i : duel.getSettings()) {
            if ((i >= 65536) && (i <= 134217728)) {
                beforeRules.add("Some worn items will be taken off");
            }
        }

        if (duel.setting(Duel.NO_FOOD)) {
            duringRules.add("Food/eating will be disabled.");
        }

        if (duel.setting(Duel.FUN_WEAPONS)) {
            duringRules.add("Dragon daggers will only be allowed");
        }

        if (duel.setting(Duel.NO_RANGED)) {
            duringRules.add("All Ranged attacks will be disabled");
        }

        if (duel.setting(Duel.NO_MAGIC)) {
            duringRules.add("All magic attacks will be disabled");
        }

        if (duel.setting(Duel.NO_MELEE)) {
            duringRules.add("All Melee will be disabled");
        }

        if (duel.setting(Duel.NO_MOVEMENT)) {
            duringRules.add("You will not be able to move during the duel");
        }

        if (duel.setting(Duel.OBSTACLES)) {
            duringRules.add("There will be obstacles in the arena");
        }

        if (duel.setting(Duel.NO_PRAYER)) {
            duringRules.add("Prayer will be disabled");
        }

        if (duel.setting(Duel.NO_DRINKS)) {
            duringRules.add("Drinks will be disabled");
        }

        if (duel.setting(Duel.NO_ARROWS)) {
            duringRules.add("You won't be able to use arrows");
        }

        if (!duel.setting(Duel.NO_RINGS)) {
            duringRules.add("Rings will be allowed to be worn");
        }

        if (duel.setting(Duel.NO_SPEC)) {
            duringRules.add("Special attacks will be disabled");
        }

        int count = 0;

        for (int i = 28; i <= 31; i++) {
            if (beforeRules.size() > count) {
                gameFrame.sendString(beforeRules.get(count++), 626, i);
            } else {
                gameFrame.sendString("", 626, i);
            }
        }

        count = 0;

        for (int i = 33; i <= 44; i++) {
            if (duringRules.size() > count) {
                gameFrame.sendString(duringRules.get(count++), 626, i);
            } else {
                gameFrame.sendString("", 626, i);
            }
        }
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {
        if (player.getGameFrame().getDuel() != null) {
            player.getGameFrame().getDuel().decline(player);
        }
    }

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        switch (button) {
        case 53 :
            pla.getGameFrame().getDuel().onAccept(pla);

            break;

        case 55 :
            pla.getGameFrame().getDuel().decline(pla);

            break;
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
