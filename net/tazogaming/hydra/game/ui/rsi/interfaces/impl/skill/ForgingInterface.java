package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.actions.SmithItemAction;
import net.tazogaming.hydra.game.skill.smithing.SmithingUtils;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/07/14
 * Time: 02:18
 */
public class ForgingInterface implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {}

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        int clickedChild     = -1;
        int productionAmount = -1;

        for (int i = 3; i <= 6; i++) {
            for (int index = 0; index < SmithingUtils.CHILD_IDS.length; index++) {
                if (SmithingUtils.CHILD_IDS[index] + i == button) {
                    clickedChild     = index;
                    productionAmount = SmithingUtils.CLICK_OPTIONS[i - 3];

                    break;
                }
            }
        }

        if ((clickedChild == -1) || (productionAmount == -1)) {
            return;
        }

        if (productionAmount == 32767) {
            pla.getGameFrame().requestAmount("How many would you like to make?", clickedChild, new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    doSmith(pla, itemSlot, amount);
                }
            });

            return;
        }

        doSmith(pla, clickedChild, productionAmount);
    }

    /**
     * Method doSmith
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param child
     * @param amount
     */
    public void doSmith(Player player, int child, int amount) {
        SmithingUtils.ForgingBar bar = (SmithingUtils.ForgingBar) player.getAccount().getAttribute("forgingBar");

        if (bar == null) {
            player.getGameFrame().close();

            return;
        }

        int levelRequired = bar.getBaseLevel() + SmithingUtils.getLevelIncrement(bar, bar.getItems()[child]);
        int barsRequired  = SmithingUtils.getBarAmount(levelRequired, bar, bar.getItems()[child]);
        int exp           = (int) bar.getExperience()[(barsRequired == 5)
                ? 3
                : barsRequired - 1];

        if (levelRequired > 99) {
            levelRequired = 99;
        }

        SmithItemAction action = new SmithItemAction(player, bar.getBarId(), barsRequired, levelRequired, exp,
                                     bar.getItems()[child]);

        action.setActionAmount(amount);
        player.setCurrentAction(action);
        player.getGameFrame().sendHideIComponent(300, 65, false);
        player.getGameFrame().sendHideIComponent(300, 89, false);
        player.getGameFrame().sendHideIComponent(300, 161, false);
        player.getGameFrame().sendHideIComponent(300, 169, false);
        player.getGameFrame().sendHideIComponent(300, 209, false);
        player.getGameFrame().sendHideIComponent(300, 266, false);
    }

    /*
     *             public static void handleForgingOptions(Player player, int button) {
     *   int clickedChild = -1;
     *   int productionAmount = -1;
     *   for (int i = 3; i <= 6; i++) {
     *     for (int index = 0; index < SmithingUtils.CHILD_IDS.length; index++) {
     *         if (SmithingUtils.CHILD_IDS[index] + i == button) {
     *             clickedChild = index;
     *             productionAmount = SmithingUtils.CLICK_OPTIONS[i - 3];
     *             break;
     *         }
     *     }
     *   }
     *   if (clickedChild == -1 || productionAmount == -1) {
     *     return;
     *   }
     *   ForgingBar bar = player.getAttribute("smithingBar");
     *   if (bar != null) {
     *     if (productionAmount == 32767) {
     *         player.setAttribute("smithingIndex", clickedChild);
     *         InputHandler.requestIntegerInput(player, 7, "Enter amount:");
     *         return;
     *     }
     *     int levelRequired = bar.getBaseLevel() + SmithingUtils.getLevelIncrement(bar, bar.getItems()[clickedChild]);
     *     int barsRequired = SmithingUtils.getBarAmount(levelRequired, bar, bar.getItems()[clickedChild]);
     *     String itemName = Misc.withPrefix(ItemDefinition.forId(bar.getItems()[clickedChild]).getName().toLowerCase());
     *     if (!player.getInventory().contains(bar.getBarId(), barsRequired)) {
     *         String barName = bar.toString().toLowerCase();
     *         DialogueManager.sendDisplayBox(player, -1, "You do not have enough " + barName + " bars to smith " + itemName + ".");
     *         ActionSender.sendCloseInterface(player);
     *         return;
     *     }
     *     if (player.getSkills().getLevel(Skills.SMITHING) < levelRequired) {
     *         DialogueManager.sendDisplayBox(player, -1, "You need a Smithing level of at least " + levelRequired + " to make " + itemName + ".");
     *         ActionSender.sendCloseInterface(player);
     *         return;
     *     }
     *     player.removeAttribute("smithingBar");
     *     player.registerAction(new Forging(3, productionAmount, bar, clickedChild));
     *     ActionSender.sendCloseInterface(player);
     *   }
     *   }
     */

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
