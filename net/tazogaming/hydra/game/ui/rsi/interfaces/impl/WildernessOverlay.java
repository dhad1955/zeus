package net.tazogaming.hydra.game.ui.rsi.interfaces.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/11/2014
 * Time: 08:00
 */
public class WildernessOverlay implements InterfaceAdapter {
    /**
     * Method interfaceOpened
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceOpened(Player player) {
        sendBounty(player);

    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceClosed(Player player) {


    }

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void interfaceRefreshed(Player player) {
            sendBounty(player);
    }

    public static void sendBounty (Player player){
      if(player.getAccount().hasVar("bounty")) {
          player.getGameFrame().sendHideIComponent(381, 1, false);
          player.getGameFrame().sendHideIComponent(381, 3, true);
          player.getGameFrame().sendHideIComponent(381, 2, false);
//          player.getGameFrame().setCurrentHintIcon(null);
          player.getGameFrame().sendHideIComponent(381, 6, true);
          player.getGameFrame().sendString(player.getBHTarget() == null ? "Nobody" : player.getBHTarget().getUsername(), 381, 14);
          if(player.getBHTarget() != null) {
              int dir = Movement.getDirectionForWaypoints(player.getLocation(), player.getBHTarget().getLocation());
              String direction = "";
              switch (dir) {
                  case Movement.NORTH:
                      direction = "North";

                      break;

                  case Movement.NORTH_EAST:
                      direction = "North east";

                      break;

                  case Movement.NORTH_WEST:
                      direction = "North west";

                      break;

                  case Movement.EAST:
                      direction = "East";

                      break;

                  case Movement.SOUTH_EAST:
                      direction = "South east";

                      break;

                  case Movement.SOUTH_WEST:
                      direction = "South west";

                      break;

                  case Movement.SOUTH:
                      direction = "South";

                      break;

                  case Movement.WEST:
                      direction = "West";

                      break;
              }
              player.getGameFrame().sendString("Distance: " + Point.getDistance(player.getBHTarget().getLocation(), player.getLocation())+" steps", 381, 15);
          }else{
              player.getGameFrame().sendString("Target location: N/A", 381, 15);
          }

      }else{
          player.getGameFrame().sendHideIComponent(381, 1, true);
          player.getGameFrame().sendHideIComponent(381, 3, false);
          player.getGameFrame().sendHideIComponent(381, 6, false);
          player.getGameFrame().sendHideIComponent(381, 2, true);

      }

    }

    /**
     * Method itemClicked
     * Created on 14/08/18
     *
     * @param itemId
     * @param slot
     * @param clickSlot
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemClicked(int itemId, int slot, int clickSlot, Player player) {

    }

    /**
     * Method actionButton
     * Created on 14/08/18
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {

    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     *
     * @param from
     * @param to
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {

    }

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick(Player player) {


    }
}
