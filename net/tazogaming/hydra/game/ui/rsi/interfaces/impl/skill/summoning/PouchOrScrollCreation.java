package net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.CS2Call;
import net.tazogaming.hydra.game.ui.InterfaceAdapter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/08/14
 * Time: 12:39
 */
public class PouchOrScrollCreation implements InterfaceAdapter {

    /**
     * Method interfaceOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceOpened(Player player) {
        if (!player.getAccount().hasVar("create_mode") || (player.getAccount().getInt32("create_mode") == 0)) {
            loadPouchCreation(player);
        } else {
            loadScrollCreation(player);
        }
    }

    /**
     * Method interfaceClosed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceClosed(Player player) {}

    /**
     * Method loadPouchCreation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void loadPouchCreation(Player player) {
        CS2Call call = new CS2Call(757);

        call.addInt(78).addInt(1);
        call.addString("Infuse-X").addString("Infuse-All").addString("Infuse-10").addString("Infuse-5").addString(
            "Infuse").addInts(1, 8, 79 << 16 | 16);
        player.getActionSender().sendClientScript(call);
        player.getGameFrame().sendAMask(190, 79, 16, 0, 462);
        player.getGameFrame().sendHideIComponent(79, 14, true);
        player.getGameFrame().sendHideIComponent(79, 15, false);
    }

    /**
     * Method loadScrollCreation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void loadScrollCreation(Player player) {
        CS2Call call = new CS2Call(765);

        call.addInt(78).addInt(1);
        call.addString("Transform-X").addString("Transform-All").addString("Transform-10").addString(
            "Transform-5").addString("Transform").addInts(1, 8, 79 << 16 | 16);
        player.getActionSender().sendClientScript(call);
        player.getGameFrame().sendAMask(190, 79, 16, 0, 462);
        player.getGameFrame().sendHideIComponent(79, 14, false);
        player.getGameFrame().sendHideIComponent(79, 15, true);
    }

    /**
     * Method interfaceRefreshed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void interfaceRefreshed(Player player) {}

    private void itemClickedScrollMode(int itemId, final int itemSlot, int clickSlot, final Player player) {
        final int slot = itemSlot - 2;

        switch (clickSlot) {
        case 0 :
            Summoning.transform(player, slot, 1);

            break;

        case 1 :
            Summoning.transform(player, slot, 5);

            break;

        case 2 :
            Summoning.transform(player, slot, 10);

            break;

        case 4 :
            player.getGameFrame().requestAmount("Please enter the amount to transform", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    Summoning.transform(player, slot, amount);
                }
            });

            break;

        case 3 :
            Summoning.transform(player, slot, Integer.MAX_VALUE);

            break;
        }
    }

    /**
     * Method itemClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param itemSlot
     * @param clickSlot
     * @param player
     */
    @Override
    public void itemClicked(int itemId, final int itemSlot, int clickSlot, final Player player) {
        final int slot = itemSlot - 2;

        if (player.getAccount().hasVar("create_mode") && (player.getAccount().getInt32("create_mode") == 2)) {
            itemClickedScrollMode(itemId, itemSlot, clickSlot, player);

            return;
        }

        switch (clickSlot) {
        case 0 :
            Summoning.infuse(player, slot, 1);

            break;

        case 1 :
            Summoning.infuse(player, slot, 5);

            break;

        case 2 :
            Summoning.infuse(player, slot, 10);

            break;

        case 4 :
            player.getGameFrame().requestAmount("Please enter the amount to infuse", new AmountTrigger() {
                @Override
                public void amountEntered(Player pla, int itemSlot, int amount) {
                    Summoning.infuse(player, slot, amount);
                }
            });

            break;

        case 3 :
            Summoning.infuse(player, slot, Integer.MAX_VALUE);

            break;
        }
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     * @param slot
     * @param pla
     * @param contextSlot
     */
    @Override
    public void actionButton(int button, int slot, Player pla, int contextSlot) {
        if (button == 20) {
            pla.getAccount().setSetting("create_mode", 2);
            loadScrollCreation(pla);
        } else {
            pla.getAccount().setSetting("create_mode", 0);
            loadPouchCreation(pla);
        }
    }

    /**
     * Method itemsSwitched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param player
     */
    @Override
    public void itemsSwitched(int from, int to, Player player) {}

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    @Override
    public void tick(Player player) {}
}
