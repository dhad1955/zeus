package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.util.Logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 19:28
 */
public class SkillMenuParser {
    public static ArrayList<SkillGuide> menus = new ArrayList<SkillGuide>();
    private SkillGuide                  curMenu;

    /**
     * Constructs ...
     *
     *
     * @param path
     */
    public SkillMenuParser(File path) {
        int lineN = 0;

        try {
            BufferedReader     reader          = new BufferedReader(new FileReader(path));
            ArrayList<Integer> actionButtonIds = new ArrayList<Integer>();
            String             line            = null;

            curMenu = new SkillGuide();

            int     subId       = 0;
            boolean readingMenu = false;

            while ((line = reader.readLine()) != null) {
                lineN++;

                if ((line.length() == 0) || line.startsWith("//")) {
                    continue;
                }

                String[] split = line.split(" ");

                if (split[0].startsWith("submenu")) {

                    // int id = Integer.parseInt(split[1]);
                    String name = line.substring("submenu".length() + 1, line.lastIndexOf("{") - 1);

                    curMenu.set(new SkillGuideSubMenu());
                    curMenu.getMenu(subId).setName(name);
                    readingMenu = true;

                    continue;
                }

                if (readingMenu &&!line.equals("}")) {
                    String[] split2 = line.split(" ", 3);

                    if (split[0].equalsIgnoreCase("setbutton")) {
                        curMenu.getMenu(subId).setBindedActionId(Integer.parseInt(split[1]));

                        continue;
                    }

                    if (split[0].equalsIgnoreCase("setskillid")) {
                        curMenu.getMenu(subId).setSkillId(Integer.parseInt(split[1]));

                        continue;
                    }

                    SkillSub i = new SkillSub();

                    i.setModelIndex(Integer.parseInt(split2[0]));
                    i.setLevelRequired(Integer.parseInt(split2[1]));
                    i.setName(split2[2]);
                    curMenu.getMenu(subId).addSub(i);
                }

                if (line.equalsIgnoreCase("}")) {
                    readingMenu = false;
                    subId++;
                }
            }

            reader.close();
        } catch (Exception ee) {
            Logger.log("error with file: " + path.getPath() + " Line: " + lineN);
            ee.printStackTrace();
        }
    }

    /**
     * Method getMenu
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SkillGuide getMenu() {
        return curMenu;
    }

    /**
     * Method loadMenus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadMenus() {
        List<File> files = (List<File>) FileUtils.listFiles(new File("config/ui/skillguides"), TrueFileFilter.INSTANCE,
                               TrueFileFilter.INSTANCE);

        for (File f : files) {
            try {
                menus.add(new SkillMenuParser(f).getMenu());
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }
    }

    /**
     * Method getMenuForButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param player
     *
     * @return
     */
    public static SkillGuide getMenuForButton(int id, Player player) {
        if ((id < 0) || ((id > 8653) && (id <= 13928))) {
            for (SkillGuide p : menus) {
                for (int i = 0; i < p.offset; i++) {
                    if (p.getMenu(i).getBindedActionId() == id) {
                        player.getAccount().setSetting("skill_menu_id", i);

                        return p;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Method reload
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void reload() {
        menus.clear();
        loadMenus();
    }
}
