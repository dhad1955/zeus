package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/07/14
 * Time: 03:58
 */
public class FriendsList {
    private HashMap<String, Byte> friendStatus = new HashMap<String, Byte>();
    private ArrayList<String>     friendsList  = new ArrayList<String>();
    private ArrayList<String>     ignoreList   = new ArrayList<String>();
    private Player                player;

    /**
     * Constructs ...
     *
     *
     * @param play
     */
    public FriendsList(Player play) {
        this.player = play;
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chunk
     */
    public void save(Buffer chunk) {
        chunk.writeWord(friendsList.size());

        for (String str : friendsList) {
            chunk.writeString(str);
        }

        chunk.writeWord(ignoreList.size());

        for (String str : ignoreList) {
            chunk.writeString(str);
        }
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chunk
     */
    public void load(Buffer chunk) {
        int siz = chunk.readShort();

        for (int i = 0; i < siz; i++) {
            friendsList.add(chunk.readString())
            ;
        }

        siz = chunk.readShort();

        for (int i = 0; i < siz; i++) {
            ignoreList.add(chunk.readString());
        }
    }

    /**
     * Method addFriend
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param friend
     *
     * @return
     */
    public boolean addFriend(String friend) {
        friend = friend.toLowerCase();

        if (!friendsList.contains(friend)) {
            friendsList.add(friend.toLowerCase());

            Player player = World.getWorld().getPlayer(Text.stringToLong(friend));

            sendFriendStatus(friend, (player == null)
                                     ? 0
                                     : 1);

            return true;
        }

        return false;
    }

    public boolean isIgnored(String username) {
        return ignoreList.contains((username.toLowerCase()));
    }

    /**
     * Method isFriend
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param friend
     *
     * @return
     */
    public boolean isFriend(String friend) {
        return friendsList.contains(friend.toLowerCase());
    }

    private void sendFriendStatus(String friend, int status) {
        this.player.getActionSender().sendFriend(Text.ucFirst(friend), status, status == 1, false);
    }

    /**
     * Method resetStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetStatus() {
        friendStatus.clear();
    }


    public void clear() {
        this.friendsList.clear();
    }
    /**
     * Method addIgnore
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param asshole
     */
    public void addIgnore(String asshole) {
        if (!ignoreList.contains(asshole)) {
            ignoreList.add(asshole.toLowerCase());
            player.getActionSender().sendIgnore(asshole);
        }
    }

    /**
     * Method removeIgnore
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param friend
     */
    public void removeIgnore(String friend) {
        ignoreList.remove(friend);
    }

    /**
     * Method deleteFriend
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param friend
     */
    public void deleteFriend(String friend) {
        String friend2 = friend.toLowerCase();

        if (friendsList.contains(friend2)) {
            friendsList.remove(friend2);

            if (friendStatus.containsKey(friend2)) {
                friendStatus.remove(friend2);
            }

            player.getActionSender().sendMessage("Friend deleted");
        }
    }

    /**
     * Method sendFriends
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */

    public void updateFriend(String friend) {
        boolean online = World.getWorld().getPlayer(Text.stringToLong(friend)) != null;

        sendFriendStatus(friend, online
                ? 1
                : 0);

    }
    public void sendFriends() {
        for (String friend : friendsList) {
            boolean online = World.getWorld().getPlayer(Text.stringToLong(friend)) != null;

            sendFriendStatus(friend, online
                                     ? 1
                                     : 0);
            this.friendStatus.put(friend.toLowerCase(), online
                    ? (byte) 1
                    : (byte) 0);
        }

        for (String ignore : ignoreList) {
            player.getActionSender().sendIgnore(ignore);
        }
    }

    /**
     * Method updateFriends
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateFriends() {
        int    status = 0;
        Player pla    = null;

        for (String friend : friendsList) {
            pla = World.getWorld().getPlayer(Text.stringToLong(friend));    // == null? 0 : 1;

            if (pla == null) {
                status = 0;
            } else if (pla != null) {
                if (player.getRights() >= Player.MODERATOR) {
                    status = 1;
                } else if (pla.getPrivacySetting(1) == 2) {
                    status = 0;
                } else if (pla.getPrivacySetting(1) == 1) {
                    if (pla.getFriendsList().isFriend(pla.getUsername())) {
                        status = 1;
                    } else {
                        status = 0;
                    }
                } else {
                    status = 1;
                }
            }

            if (!friendStatus.containsKey(friend) || (byte) friendStatus.get(friend) != (byte) status) {
                sendFriendStatus(friend, status);
                friendStatus.put(friend, (byte) status);
            }
        }
    }
}
