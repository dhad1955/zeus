package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.GraveStone;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRequest;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.ui.rsi.interfaces.*;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchange;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchangeSetsInventoryInterface;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.CharacterDesign;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.ClanSettings;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchangeItemSetsInterface;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.WildernessOverlay;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.bank.*;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.bank.BankInventoryInterface;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.combat.PrayerInterfaceAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.combat.WeaponInterfaceAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.duel.DuelConfirmAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.duel.DuelInvAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.duel.DuelRulesAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.equipment.EquipScreenAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.equipment.EquipmentAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.equipment.EquipmentInventoryScreenAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.equipment.ItemsOnDeath;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.misc.SkillGuide;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.setting.OptionsTabHandler;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.shop.ShopInterfaceAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.shop.ShopInventoryAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.DungOverlay;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.ForgingInterface;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.FurnitureInterface;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning.*;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.trade.TradeConfirmAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.trade.TradeInterfaceAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.trade.TradeInvAdapter;
import net.tazogaming.hydra.game.ui.rsi.interfaces.tab.*;
import net.tazogaming.hydra.game.ui.shops.Shop;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 30/06/14
 * Time: 11:54
 * To change this template use File | Settings | File Templates.
 */
public class GameFrame extends PlayerTickEvent {
    public static final int
        SIDEBAR_ATTACK                                 = 0,
        SIDEBAR_SKILLS                                 = 1,
        SIDEBAR_QUESTS                                 = 2,
        SIDEBAR_ACHIEVEMENTS                           = 3,
        SIDEBAR_INV                                    = 4,
        SIDEBAR_EQUIPMENT                              = 5,
        SIDEBAR_PRAYER                                 = 6,
        SIDEBAR_MAGIC                                  = 7,
        SIDEBAR_OBJECTIVES                             = 8,
        SIDEBAR_FRIENDS                                = 9,
        SIDEBAR_IGNORE                                 = 10,
        SIDEBAR_CLAN                                   = 11,
        SIDEBAR_SETTINGS                               = 12,
        SIDEBAR_EMOTES                                 = 13,
        SIDEBAR_MUSIC                                  = 14,
        SIDEBAR_NOTES                                  = 15,
        SIDEBAR_LOGOUT                                 = 15;
    public static InterfaceAdapter[] interfaceAdapters = new InterfaceAdapter[1300];

    /** defaultInterface made: 14/09/30 */
    private static final InterfaceAdapter defaultInterface = new InterfaceAdapter() {
        @Override
        public void interfaceOpened(Player player) {}
        @Override
        public void interfaceClosed(Player player) {}
        @Override
        public void interfaceRefreshed(Player player) {}
        @Override
        public void itemClicked(int itemId, int slot, int clickSlot, Player player) {}
        @Override
        public void actionButton(int button, int slot, Player pla, int contextSlot) {}
        @Override
        public void itemsSwitched(int from, int to, Player player) {}
        @Override
        public void tick(Player player) {}
    };
    public static final int INVENTORY          = 149;
    public static final int DEFAULT_SIDEBARS[] = {
        884, 930, 320, 1070, 149, 387, 271, 192, 662, 550, 551, 589, 261, 464, 187, 34, 182
    };
    public static final int CHAT_DEFAULT       = 137;
    public static final int
        FIXED                                  = 548,
        RESIZABLE                              = 746;
    public static final int
        INVENTORY_CONTAINER                    = 93,
        EQUIPMENT_CONTAINER                    = 94;
    public static final int
        INTER_INVENTORY                        = 149,
        INTER_EQUIPMENT                        = 387,
        INTER_OPTIONS                          = 261,
        INTER_WEAPON                           = 884,
        INTER_PRAYER                           = 271,
        INTER_EQUIPSCREEN                      = 667,
        INTER_ITEMSONDEATH                     = 102,
        INTER_BANK                             = 762,
        INTER_SHOP                             = 620,
        INTER_STATISTICS                       = 930,
        INTER_SDIALOUGE                        = 905;

    static {
        interfaceAdapters[645]      = new GrandExchangeItemSetsInterface();
        interfaceAdapters[INTER_INVENTORY]    = new InventoryAdapter();
        interfaceAdapters[INTER_EQUIPMENT]    = new EquipmentAdapter();
        interfaceAdapters[INTER_OPTIONS]      = new OptionsTabHandler();
        interfaceAdapters[INTER_WEAPON]       = new WeaponInterfaceAdapter();
        interfaceAdapters[763]                = new BankInventoryInterface();
        interfaceAdapters[INTER_PRAYER]       = new PrayerInterfaceAdapter();
        interfaceAdapters[335]                = new TradeInterfaceAdapter();
        interfaceAdapters[644]                  = new GrandExchangeSetsInventoryInterface();
        interfaceAdapters[336]                = new TradeInvAdapter();
        interfaceAdapters[334]                = new TradeConfirmAdapter();
        interfaceAdapters[INTER_EQUIPSCREEN]  = new EquipScreenAdapter();
        interfaceAdapters[INTER_ITEMSONDEATH] = new ItemsOnDeath();
        interfaceAdapters[INTER_SHOP]         = new ShopInterfaceAdapter();
        interfaceAdapters[621]                = new ShopInventoryAdapter();
        interfaceAdapters[INTER_BANK]         = new BankInterfaceAdapter();
        interfaceAdapters[INTER_SDIALOUGE]    = new SkillDialogue();
        interfaceAdapters[1028]               = new CharacterDesign();
        interfaceAdapters[INTER_STATISTICS]   = new StatisticsInterface();
        interfaceAdapters[499]                = new SkillGuide();
        interfaceAdapters[880]                = new LeftClickSelect();
        interfaceAdapters[396]                = new FurnitureInterface();
        interfaceAdapters[320]                = new SkillsTabAdapter();
        interfaceAdapters[300]                = new ForgingInterface();
        interfaceAdapters[631]                = new DuelRulesAdapter();
        interfaceAdapters[628]                = new DuelInvAdapter();
        interfaceAdapters[626]                = new DuelConfirmAdapter();
        interfaceAdapters[662]                = new SummoningTab();
        interfaceAdapters[747]                = new SummoningOrb();
        interfaceAdapters[671]                = new BOBInterface();
        interfaceAdapters[665]                = new BOBInvInterface();
        interfaceAdapters[79]                 = new PouchOrScrollCreation();
        interfaceAdapters[740]                = new LevelUP();
        interfaceAdapters[192]                = new MagicSpellBook();
        interfaceAdapters[193]                = new MagicSpellBook();
        interfaceAdapters[494]                = new DungOverlay();
        interfaceAdapters[590]                = new ClanSettings();
        interfaceAdapters[791]                = new ClanWarsSettings();
        interfaceAdapters[1071]               = new QuestTab();
        interfaceAdapters[1072]               = new AchievementsTab();
        interfaceAdapters[381]                = new WildernessOverlay();
        interfaceAdapters[105]                = new GrandExchange();
        interfaceAdapters[107]                = new GESellInventoryInterfaceAdapter();
        interfaceAdapters[748]               = new HPOrb();
        interfaceAdapters[670]                = new EquipmentInventoryScreenAdapter();
    }

    /** tabs made: 14/09/30 */
    private int[] tabs = new int[17];

    /** currentDialog made: 14/09/30 */
    private int currentDialog = -1;

    /** root made: 14/09/30 */
    private int root = FIXED;

    /** currentMainWindow made: 14/09/30 */
    private int currentMainWindow = -1;

    /** lastAmount made: 14/09/30 */
    private int lastAmount = 50;

    /** currentInventoryContainer made: 14/09/30 */
    private int currentInventoryContainer = 93;

    /** amountReqSlot made: 14/09/30 */
    private int amountReqSlot = -1;

    /** currentInventoryInterface made: 14/09/30 */
    private int currentInventoryInterface = -1;

    /** currentOverlay made: 14/09/30 */
    private int currentOverlay = -1;

    /** staticBackgroundInterfaces made: 14/09/30 */
    private int[] staticBackgroundInterfaces = new int[10];

    /** currentHintIcon made: 14/09/30 */
    private HintIcon currentHintIcon;

    /** currentClanWarsRequest made: 14/09/30 */
    private ClanWarRequest currentClanWarsRequest;

    /** currentChatDialog made: 14/09/30 */
    private Dialog currentChatDialog;

    /** player made: 14/09/30 */
    private Player player;

    /** prodRequest made: 14/09/30 */
    private ProduceRequest prodRequest;

    /** currentShop made: 14/09/30 */
    private Shop currentShop;

    /** pendingProduceRequest made: 14/09/30 */
    private ProduceRequest pendingProduceRequest;

    /** menu made: 14/09/30 */
    private PlayerContextMenu menu;

    /** currentAmountRequest made: 14/09/30 */
    private AmountTrigger currentAmountRequest;

    /** furnitureBuildSettings made: 14/09/30 */
    private ConstructionInterfaceSettings furnitureBuildSettings;

    /** duel made: 14/09/30 */
    private Duel duel;

    /** trade made: 14/09/30 */
    private Trade trade;

    /** currentTextRequest made: 14/09/30 */
    private TextInputRequest currentTextRequest;

    /** bgPointer made: 14/09/30 */
    private byte bgPointer;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    public GameFrame(Player player) {
        super(player, true, 1);
        this.player = player;
        player.addEventIfAbsent(this);
        menu = new PlayerContextMenu(player);
        if (!player.isHD()) {
            root = FIXED;
        } else {
            root = RESIZABLE;
        }

        registerBackgroundInterface(747);    // summoning orb
    }

    /**
     * Method getCurrentHintIcon
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public HintIcon getCurrentHintIcon() {
        return currentHintIcon;
    }


    public int getSpellbookSortConfig(Player player){
        Account account = player.getAccount();
        if(!account.hasVar("sb_sortlvl")) {
            account.setSetting("sb_sortlvl", 0, true);
            account.setSetting("sb_sortcmb", 0, true);
            account.setSetting("sb_showtele", 1, true);
            account.setSetting("sb_showmisc", 1, true);
            account.setSetting("sb_showcmb", 1, true);
        }
        if(getSpellbook() == 193) {
            int sortId = 0;
            int cfg = 1 << 3 |
                    (account.getInt32("sb_showcmb") == 1 ? 0 : 1) << 16
                    | (account.getInt32("sb_showtele") == 1 ? 0 : 2) << 17
                    | sortId << 3;
            return cfg;
        }

        return 0;

    }
    /**
     * Method setCurrentHintIcon
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentHintIcon
     */
    public void setCurrentHintIcon(HintIcon currentHintIcon) {
        if (currentHintIcon != this.currentHintIcon) {
            player.getActionSender().sendHintIcon(currentHintIcon);
        }

        this.currentHintIcon = currentHintIcon;
    }

    /**
     * Method getCurrentClanWarsRequest
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ClanWarRequest getCurrentClanWarsRequest() {
        return currentClanWarsRequest;
    }

    /**
     * Method setCurrentClanWarsRequest
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentClanWarsRequest
     */
    public void setCurrentClanWarsRequest(ClanWarRequest currentClanWarsRequest) {
        this.currentClanWarsRequest = currentClanWarsRequest;
    }

    /**
     * Method setRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param request
     * @param question
     */
    public void setRequest(TextInputRequest request, String question) {
        this.currentTextRequest = request;

        CS2Call data = new CS2Call(110).addString(question);

        player.getActionSender().sendClientScript(data);
    }

    /**
     * Method getCurrentTextRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public TextInputRequest getCurrentTextRequest() {
        return currentTextRequest;
    }

    /**
     * Method setCurrentTextRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentTextRequest
     */
    public void setCurrentTextRequest(TextInputRequest currentTextRequest) {
        this.currentTextRequest = currentTextRequest;
    }

    private void registerBackgroundInterface(int g) {
        staticBackgroundInterfaces[bgPointer++] = g;
    }

    /**
     * Method getFurnitureBuildSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ConstructionInterfaceSettings getFurnitureBuildSettings() {
        return furnitureBuildSettings;
    }

    /**
     * Method setFurnitureBuildSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param furnitureBuildSettings
     */
    public void setFurnitureBuildSettings(ConstructionInterfaceSettings furnitureBuildSettings) {
        this.furnitureBuildSettings = furnitureBuildSettings;
    }

    /**
     * Method getDuel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Duel getDuel() {
        return duel;
    }

    /**
     * Method setDuel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param duel
     */
    public void setDuel(Duel duel) {
        this.duel = duel;
    }

    /**
     * Method getTrade
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Trade getTrade() {
        return trade;
    }

    /**
     * Method setTrade
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param trade
     */
    public void setTrade(Trade trade) {
        this.trade = trade;
    }

    /**
     * Method getSpellbook
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSpellbook() {
        return this.tabs[SIDEBAR_MAGIC];
    }

    /**
     * Method setMulti
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param multi
     */
    public void setMulti(boolean multi) {
        sendInterfaceConfig(player, 745, 3, false);
        sendInterfaceConfig(player, 745, 6, false);
        sendInterfaceConfig(player, 745, 1, multi);    // multi zone
    }

    private void sendOverlay(int overlay) {
        if (overlay != -1) {
            this.sendInterface(1, 7, overlay);
        } else {
            this.sendCloseInterface(getRoot(), 7);
        }
    }

    /**
     * Method openOverlay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void openOverlay(int id) {
        this.currentOverlay = id;
        sendOverlay(this.currentOverlay);
        getInterface(id).interfaceOpened(player);
    }

    /**
     * Method closeOverlay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void closeOverlay() {
        this.currentOverlay = -1;
        sendOverlay(this.currentOverlay);
    }

    /**
     * Method switchWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param window
     */
    public void switchWindow(int window) {
        this.currentMainWindow = window;
        this.getActiveInterface(currentMainWindow).interfaceOpened(player);
        sendInterface((getRoot() == RESIZABLE)
                      ? 9
                      : 18, window);
        sendOverlay(currentOverlay);
    }

    /**
     * Method prepareProductRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param request
     */
    public void prepareProductRequest(ProduceRequest request) {
        this.pendingProduceRequest = request;
    }

    /**
     * Method interfaceHash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceID
     * @param child
     *
     * @return
     */
    public static int interfaceHash(int interfaceID, int child) {
        return interfaceID << 16 | child;
    }

    /**
     * Method sendIComponentSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceId
     * @param componentId
     * @param fromSlot
     * @param toSlot
     * @param settingsHash
     */
    public void sendIComponentSettings(int interfaceId, int componentId, int fromSlot, int toSlot, int settingsHash) {
        sendAMask(settingsHash, interfaceId, componentId, fromSlot, toSlot);
    }

    /**
     * Method sendAMask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param set
     * @param interfaceId
     * @param childId
     * @param off
     * @param len
     */
    public void sendAMask(int set, int interfaceId, int childId, int off, int len) {
        player.dispatch(
            new MessageBuilder(119, Packet.Size.Fixed).addInt2(set).addShortA(len).addShortA(off).addLEInt(
                interfaceId << 16 | childId));
    }

    /**
     * Method getPendingProduceRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ProduceRequest getPendingProduceRequest() {
        return pendingProduceRequest;
    }

    /**
     * Method getContextMenu
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PlayerContextMenu getContextMenu() {
        return menu;
    }

    /**
     * Method requestProduct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param request
     */
    public void requestProduct(ProduceRequest request) {
        this.setDialog(905);
        this.prodRequest = request;
    }

    /**
     * Method setProductRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param request
     */
    public void setProductRequest(ProduceRequest request) {
        this.prodRequest = request;
    }

    /**
     * Method getCurrentProductRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ProduceRequest getCurrentProductRequest() {
        return prodRequest;
    }

    /**
     * Method openShop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param shopID
     */
    public void openShop(int shopID) {
        Shop shop = ShopManager.getShop(shopID);

        if ((shop != player.getBuyBackShop()) && (shopID != Shop.IRON_MAN_SHOP)
                && (player.getAccount().getIronManMode() >= Account.IRON_MAN_MEDIUM)
                && (shop.getCurrencyPoints() == -1)) {
            if (!shop.isIronManEnabled()) {
                player.getActionSender().sendMessage("You can't open this shop in iron man mode.");
                shop = ShopManager.getShop(Shop.IRON_MAN_SHOP);
            }
        }

        if (shop != null) {
            this.currentShop = shop;
            openWindow(INTER_SHOP);
        } else {
            player.getActionSender().sendDev("Invalid shop id: " + shopID);
        }
    }

    /**
     * Method openShop
     * Created on 14/11/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param shop
     */
    public void openShop(Shop shop) {
        if (shop != null) {
            this.currentShop = shop;
            openWindow(INTER_SHOP);
        } else {}
    }

    /**
     * Method requestAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param message
     * @param am
     */
    public void requestAmount(String message, AmountTrigger am) {
        requestAmount(message, -1, am);
    }

    /**
     * Method requestAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param message
     * @param slot
     * @param am
     */
    public void requestAmount(String message, int slot, AmountTrigger am) {
        this.currentAmountRequest = am;
        this.amountReqSlot        = slot;

        CS2Call call = new CS2Call(108).addString(message);

        player.getActionSender().sendClientScript(call);
    }

    /**
     * Method getShop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Shop getShop() {
        return this.currentShop;
    }

    /**
     * Method amountEntered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     *
     * @return
     */
    public boolean amountEntered(int amount) {
        if (currentAmountRequest != null) {
            if (this.currentMainWindow == INTER_BANK) {
                this.lastAmount = amount;
            }

            this.currentAmountRequest.amountEntered(player, amountReqSlot, amount);

            return true;
        }

        return false;
    }

    /**
     * Method getLastAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLastAmount() {
        return lastAmount;
    }

    /**
     * Method getCurrInventory
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrInventory() {
        return this.currentInventoryContainer;
    }

    /**
     * Method sendPlayerOnInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interId
     * @param childId
     */
    public void sendPlayerOnInterface(int interId, int childId) {
        MessageBuilder bldr = new MessageBuilder(54, Packet.Size.Fixed);

        bldr.addInt1(interId << 16 | childId);
        player.dispatch(bldr);
    }

    /**
     * Method sendNpcOnInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interId
     * @param childId
     * @param npcId
     */
    public void sendNpcOnInterface(int interId, int childId, int npcId) {
        MessageBuilder bldr = new MessageBuilder(71, Packet.Size.Fixed);

        bldr.addInt2(interId << 16 | childId);
        bldr.addLEShort(npcId);
        player.dispatch(bldr);
    }

    /**
     * Method sendString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param string
     * @param interfaceId
     * @param childId
     */
    public void sendString(String string, int interfaceId, int childId) {
        MessageBuilder bldr = new MessageBuilder(33, Packet.Size.VariableShort);

        bldr.addRS2String(string);
        bldr.addLEInt(interfaceId << 16 | childId);
        player.dispatch(bldr);
    }

    /**
     * Method getInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static InterfaceAdapter getInterface(int id) {
        if ((id < 0) || (interfaceAdapters[id] == null)) {
            return defaultInterface;
        }

        return interfaceAdapters[id];
    }

    /**
     * Method showDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     */
    public void showDialog(Dialog d) {
        close();
        this.currentChatDialog = d;
        d.draw(player);
    }

    /**
     * Method getActiveInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public InterfaceAdapter getActiveInterface(int id) {
        if (interfaceAdapters[id] == null) {
            return defaultInterface;
        }

        if(id == 748)
            return interfaceAdapters[748];


        if ((interfaceAdapters[id] != null) && (getRoot() == id)) {
            return interfaceAdapters[id];
        }

        for (int i = 0; i < bgPointer; i++) {
            if (staticBackgroundInterfaces[i] == id) {
                return getInterface(id);
            }
        }

        if (currentInventoryInterface == id) {
            return interfaceAdapters[id];
        }

        for (int i = 0; i < tabs.length; i++) {
            if (tabs[i] == id) {
                return getInterface(id);
            }
        }

        if (currentDialog == id) {
            return getInterface(id);
        }

        if (currentMainWindow == id) {
            return getInterface(id);
        }

        return defaultInterface;
    }

    /**
     * Method sendCloseInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param window
     * @param tab
     */
    public void sendCloseInterface(int window, int tab) {
        MessageBuilder bldr = new MessageBuilder(61, Packet.Size.Fixed);

        bldr.addLEInt(window << 16 | tab);
        player.dispatch(bldr);
    }

    /**
     * Method sendPrivacySettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sendPrivacySettings() {
        MessageBuilder mainSettings = new MessageBuilder(108, Packet.Size.Fixed);

        mainSettings.addByteA(player.getPrivacySetting(2));    // trade
        mainSettings.addByteS(player.getPrivacySetting(0));    // public
        player.dispatch(mainSettings);

        MessageBuilder bldr = new MessageBuilder(66, Packet.Size.Fixed);

        bldr.addByte(player.getPrivacySetting(1));
        player.dispatch(bldr);
        player.getActionSender().sendVar(1054, player.getPrivacySetting(3));
        player.getActionSender().sendVar(1055, player.getPrivacySetting(4));
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ignoreDialogs
     */
    public void close(boolean ignoreDialogs) {
        if ((currentDialog != -1) &&!ignoreDialogs) {
            getInterface(currentDialog).interfaceClosed(player);
            setDialog(-1);
            currentDialog     = -1;
            currentChatDialog = null;
        }

        if (currentMainWindow != -1) {
            getInterface(currentMainWindow).interfaceClosed(player);
            currentMainWindow = -1;
            sendCloseInterface(getRoot(), (getRoot() == RESIZABLE)
                                          ? 9
                                          : 18);
        }

        if (currentInventoryInterface != -1) {
            closeInvInterface();
            getInterface(this.tabs[SIDEBAR_INV]).interfaceOpened(player);
        }

        if (tabs[SIDEBAR_INV] != INTER_INVENTORY) {
            this.currentInventoryContainer = INVENTORY_CONTAINER;
            changeTab(SIDEBAR_INV, INTER_INVENTORY);
        }

        this.currentTextRequest   = null;
        this.currentAmountRequest = null;
    }

    /**
     * Method setInventoryInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param container
     */
    public void setInventoryInterface(int id, int container) {
        if (container != -1) {
            this.currentInventoryContainer = container;
        }

        this.currentInventoryInterface = id;
        sendInterface((getRoot() == FIXED)
                      ? 197
                      : 84, id);
        getInterface(id).interfaceOpened(player);
    }

    private void closeInvInterface() {
        sendCloseInterface(getRoot(), (getRoot() == FIXED)
                                      ? 197
                                      : 84);
    }

    /**
     * Method silentClose
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void silentClose() {
        if (currentMainWindow != -1) {
            getInterface(currentMainWindow).interfaceClosed(player);
            this.currentMainWindow = -1;
        }
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void close() {
        this.close(false);
    }

    /**
     * Method openWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void openWindow(int id) {
        openWindow(id, false);
    }

    private void sendOpenTrigger(int id) {
        World.getWorld().getScriptManager().directTrigger(player, null, Trigger.INTERFACE_LOADED, id);
    }

    /**
     * Method openWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param windowID
     * @param ignoreDialogs
     */
    public void openWindow(int windowID, boolean ignoreDialogs) {
        player.log("Interface opened: " + windowID);
        this.close(ignoreDialogs);

        switch (getRoot()) {
        case FIXED :
            getInterface(windowID).interfaceOpened(player);
            sendOpenTrigger(windowID);
            sendInterface(0, 18, windowID);

            break;

        case RESIZABLE :
            sendInterface(0, 9, windowID);
            getInterface(windowID).interfaceOpened(player);
            sendOpenTrigger(windowID);

            break;
        }

        currentMainWindow = windowID;
    }

    public int getCurrentMainWindow() {
        return currentMainWindow;
    }

    /**
     * Method actionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceId
     * @param button
     * @param slot
     * @param contextSlot
     * @param player
     */
    public void actionButton(int interfaceId, int button, int slot, int contextSlot, Player player) {
        getActiveInterface(interfaceId).actionButton(button, slot, player, contextSlot);
    }

    /**
     * Method isWindowOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean isWindowOpen(int id) {
        return currentMainWindow == id;
    }

    /**
     * Method itemClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceID
     * @param button
     * @param itemSlot
     * @param contextSlot
     * @param player
     */
    public void itemClick(int interfaceID, int button, int itemSlot, int contextSlot, Player player) {
        getActiveInterface(interfaceID).itemClicked(button, itemSlot, contextSlot, player);
    }

    /**
     * Method init
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void init() {
        setDefaultTabs();

        // sendWindowsPane(getRoot());
        setDialog(CHAT_DEFAULT);
    }

    /**
     * Method getRoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRoot() {
        return root;
    }

    private void loadTabs() {
        for (int i = 0; i < tabs.length; i++) {
            getInterface(tabs[i]).interfaceOpened(player);
            sendOpenTrigger(tabs[i]);
        }

        sendSidebar();
    }

    /**
     * Method changeRoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param root
     */
    public void changeRoot(int root) {
        this.root = root;
        sendWindowsPane(root);
        loadPane();
    }

    /**
     * Method sendAMask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param set1
     * @param set2
     * @param interfaceId1
     * @param childId1
     * @param interfaceId2
     * @param childId2
     */
    public void sendAMask(int set1, int set2, int interfaceId1, int childId1, int interfaceId2, int childId2) {
        MessageBuilder bldr = new MessageBuilder().setId(119);

        bldr.addInt2(interfaceId2 << 16 | childId2);
        bldr.addShortA(set2);
        bldr.addShortA(set1);
        bldr.addLEInt(interfaceId1 << 16 | childId1);
        player.getIoSession().write(bldr.toPacket());
    }

    /**
     * Method sendInterfaceConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param interfaceId
     * @param childId
     * @param hidden
     */
    public static void sendInterfaceConfig(Player player, int interfaceId, int childId, boolean hidden) {
        player.dispatch(new MessageBuilder(3, Packet.Size.Fixed).addInt2((interfaceId << 16) | childId).addByteC(hidden
                ? 0
                : 1));
    }

    /**
     * Method sendHideIComponent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param inter
     * @param child
     * @param hidden
     */
    public void sendHideIComponent(int inter, int child, boolean hidden) {
        sendInterfaceConfig(player, inter, child, hidden);
    }

    /**
     * Method sendFullScreenAMasks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sendFullScreenAMasks() {
        sendAMask(-1, -1, 746, 36, 0, 2);
        sendAMask(-1, -1, 884, 11, 0, 2);
        sendAMask(-1, -1, 884, 12, 0, 2);
        sendAMask(-1, -1, 884, 13, 0, 2);
        sendAMask(-1, -1, 884, 14, 0, 2);
        sendAMask(-1, -1, 746, 37, 0, 2);
        sendAMask(-1, -1, 746, 38, 0, 2);
        sendAMask(0, 300, 190, 18, 0, 14);
        sendAMask(0, 11, 190, 15, 0, 2);
        sendAMask(-1, -1, 746, 39, 0, 2);
        sendAMask(-1, -1, 746, 40, 0, 2);
        sendAMask(0, 27, 149, 0, 69, 32142);
        sendAMask(28, 55, 149, 0, 32, 0);
        sendAMask(-1, -1, 746, 41, 0, 2);
        sendAMask(-1, -1, 746, 42, 0, 2);
        sendAMask(0, 29, 271, 8, 0, 2);
        sendAMask(-1, -1, 746, 43, 0, 2);
        sendAMask(-1, -1, 746, 44, 0, 2);
        sendAMask(-1, -1, 746, 45, 0, 2);
        sendAMask(-1, -1, 746, 46, 0, 2);
        sendAMask(-1, -1, 746, 47, 0, 2);
        sendAMask(-1, -1, 746, 48, 0, 2);
        sendAMask(-1, -1, 746, 49, 0, 2);
        sendAMask(-1, -1, 746, 50, 0, 2);
        sendAMask(0, 1727, 187, 1, 0, 26);
        sendAMask(0, 11, 187, 9, 36, 6);
        sendAMask(12, 23, 187, 9, 0, 4);
        sendAMask(24, 24, 187, 9, 32, 0);
        sendAMask(-1, -1, 746, 51, 0, 2);
        sendAMask(0, 29, 34, 9, 40, 30);
        sendAMask(-1, -1, 746, 36, 0, 2);
        sendAMask(-1, -1, 884, 11, 0, 2);
        sendAMask(-1, -1, 884, 12, 0, 2);
        sendAMask(-1, -1, 884, 13, 0, 2);
        sendAMask(-1, -1, 884, 14, 0, 2);
        sendAMask(-1, -1, 746, 36, 0, 2);
        sendAMask(-1, -1, 884, 11, 0, 2);
        sendAMask(-1, -1, 884, 12, 0, 2);
        sendAMask(-1, -1, 884, 13, 0, 2);
        sendAMask(-1, -1, 884, 14, 0, 2);
        sendAMask(2, 662, 18, -1, -1);

    }

    /**
     * Method sendFixedAMasks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sendFixedAMasks() {
        sendAMask(-1, -1, 548, 123, 0, 2);
        sendAMask(-1, -1, 884, 11, 0, 2);
        sendAMask(-1, -1, 884, 12, 0, 2);
        sendAMask(-1, -1, 884, 13, 0, 2);
        sendAMask(-1, -1, 884, 14, 0, 2);
        sendAMask(-1, -1, 548, 124, 0, 2);
        sendAMask(-1, -1, 548, 125, 0, 2);
        sendAMask(0, 300, 190, 18, 0, 14);
        sendAMask(0, 11, 190, 15, 0, 2);
        sendAMask(-1, -1, 548, 126, 0, 2);
        sendAMask(-1, -1, 548, 127, 0, 2);
        sendAMask(-1, -1, 548, 128, 0, 2);
        sendAMask(-1, -1, 548, 129, 0, 2);
        sendAMask(0, 30, 271, 8, 0, 2);
        sendAMask(0, 27, 149, 0, 69, 32142);
        sendAMask(28, 55, 149, 0, 32, 0);
        sendAMask(111, 113, 548, 123, 0, 2);

        // sendAMask(0, 27, 271, 6, 0, 2);
        sendAMask(-1, -1, 548, 130, 0, 2);
        sendAMask(-1, -1, 548, 93, 0, 2);
        sendAMask(-1, -1, 548, 94, 0, 2);
        sendAMask(-1, -1, 548, 95, 0, 2);
        sendAMask(-1, -1, 548, 96, 0, 2);
        sendAMask(-1, -1, 548, 97, 0, 2);
        sendAMask(-1, -1, 548, 98, 0, 2);
        sendAMask(-1, -1, 548, 99, 0, 2);
        sendAMask(0, 1727, 187, 1, 0, 26);
        sendAMask(0, 11, 187, 9, 36, 6);
        sendAMask(12, 23, 187, 9, 0, 4);
        sendAMask(24, 24, 187, 9, 32, 0);
        sendAMask(-1, -1, 548, 100, 0, 2);
        sendAMask(0, 29, 34, 9, 40, 30);
        sendAMask(-1, -1, 548, 123, 0, 2);
        sendAMask(-1, -1, 884, 11, 0, 2);
        sendAMask(-1, -1, 884, 12, 0, 2);
        sendAMask(-1, -1, 884, 13, 0, 2);
        sendAMask(-1, -1, 884, 14, 0, 2);
        sendAMask(-1, -1, 548, 123, 0, 2);
        sendAMask(-1, -1, 884, 11, 0, 2);
        sendAMask(-1, -1, 884, 12, 0, 2);
        sendAMask(-1, -1, 884, 13, 0, 2);
        sendAMask(-1, -1, 884, 14, 0, 2);
        sendIComponentSettings(662, 74, 0, 0, 2);
        sendIComponentSettings(747, 18, 0, 0, 2);
        sendInterfaceConfig(player, 34, 13, false);
        sendInterfaceConfig(player, 34, 13, false);
        sendInterfaceConfig(player, 34, 3, false);
        player.getAccount().setB(768, 3, true);
        player.getAccount().setB(234, 0, true);
        player.getAccount().setB(181, 0, true);
        player.getAccount().setB(168, 4, true);


        // player.getSettings().setB(695e);
    }

    /**
     * Method resetAccessMasks
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetAccessMasks() {
        if (getRoot() == RESIZABLE) {
            sendFullScreenAMasks();
        } else {
            sendFixedAMasks();
        }
    }

    /**
     * Method loadPane
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void loadPane() {
        GraveStone.checkGravestones(player);

        if (currentOverlay > 0) {
            getInterface(currentOverlay).interfaceRefreshed(player);
        }

        switch (root) {
        case FIXED :
            sendInterface(1, 67, 751);
            sendInterface(1, 192, 752);
            sendInterface(1, 16, 754);
            sendInterface(1, 182, 748);
            sendInterface(1, 184, 749);
            sendInterface(1, 185, 750);
            sendInterface(1, 187, 747);
            sendInterface(1, 14, 745);
            sendInterface(1, 752, 9, 137);
            sendInterface(1, 220, 182);
            loadTabs();
            sendFixedAMasks();

            break;

        case RESIZABLE :
            sendInterface(1, 16, 751);
            sendInterface(1, 69, 752);
            sendInterface(1, 70, 754);
            sendInterface(1, 174, 748);
            sendInterface(1, 175, 749);
            sendInterface(1, 176, 750);
            sendInterface(1, 177, 747);
            sendInterface(1, 752, 9, 137);
            sendInterface(1, 15, 745);
            loadTabs();
            sendFullScreenAMasks();

            break;
        }

        sendOverlay(currentOverlay);
        setMulti(player.isInMultiArea());
        sendPrivacySettings();
        player.getActionSender().sendUnlockFriendList();
        player.getActionSender().sendVar(1801, player.getXPCounter() * 10);
    }

    /**
     * Method sendSidebar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sendSidebar() {
        if (player.getIoSession().getVersion() == 117) {
            return;
        }

        for (int i = 0; i < tabs.length; i++) {
            sendTab(i, tabs[i]);
        }
    }

    private void setDefaultTabs() {
        System.arraycopy(DEFAULT_SIDEBARS, 0, tabs, 0, DEFAULT_SIDEBARS.length);
    }

    /**
     * Method changeTab
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tabid
     * @param interfaceid
     */
    public void changeTab(int tabid, int interfaceid) {
        if (tabs[tabid] != -1) {
            getInterface(tabs[tabid]).interfaceClosed(player);
        }

        if (interfaceid != -1) {
            getInterface(interfaceid).interfaceOpened(player);
            sendOpenTrigger(interfaceid);
        }

        sendTab(tabid, interfaceid);
        tabs[tabid] = interfaceid;
    }

    /**
     * Method setTab
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tabID
     * @param interfaceID
     * @param dispatch
     */
    public void setTab(int tabID, int interfaceID, boolean dispatch) {
        sendTab(tabID, interfaceID);
        tabs[tabID] = interfaceID;
        sendOpenTrigger(interfaceID);
        getInterface(interfaceID).interfaceOpened(player);
    }

    /**
     * Method setTab
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tabID
     * @param interfaceID
     */
    public void setTab(int tabID, int interfaceID) {
        setTab(tabID, interfaceID, false);
    }

    private void sendTab(int tabID, int interfaceID) {
        switch (getRoot()) {
        case FIXED :
            if (interfaceID == -1) {
                sendCloseInterface(getRoot(), 202 + tabID);
            } else {
                sendInterface(202 + tabID, interfaceID);
            }

            break;

        case RESIZABLE :
            if (interfaceID == -1) {
                sendCloseInterface(getRoot(), 87 + tabID);
            } else {
                sendInterface(87 + tabID, interfaceID);
            }
        }
    }

    /**
     * Method sendAccessMask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param start
     * @param end
     * @param main
     * @param child
     * @param mask
     * @param shit
     */
    public void sendAccessMask(int start, int end, int main, int child, int mask, int shit) {
        if (true) {
            sendAMask(start, end, main, child, mask, shit);

            return;
        }

        MessageBuilder accessMaskBuilder = new MessageBuilder().setId(23);

        accessMaskBuilder.addLEShortA(start).addLEInt(main << 16 | child).addShortA(shit).addInt(mask).addShort(end);
        player.getIoSession().write(accessMaskBuilder.toPacket());
    }

    /**
     * Method setRoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param root
     */
    public void setRoot(int root) {
        this.root = root;
        loadPane();
    }

    /**
     * Method getDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Dialog getDialog() {
        return this.currentChatDialog;
    }

    /**
     * Method closeAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void closeAll() {
        close(false);
    }

    /**
     * Method closeDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void closeDialog() {
        setDialog(-1);
        this.currentChatDialog = null;
    }

    /**
     * Method setDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setDialog(int id) {
        if (currentDialog != -1) {
            getInterface(currentDialog).interfaceClosed(this.player);
        }

        if (id == -1) {
            this.sendCloseInterface(752, 13);
        }

        if (id == -1) {
            return;
        }

        getInterface(id).interfaceOpened(player);
        sendInterface(0, 752, (id != CHAT_DEFAULT)
                              ? 13
                              : 9, id);
        this.currentDialog = id;
    }

    /**
     * Method sendGlobalString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param str
     */
    public void sendGlobalString(int id, String str) {
        MessageBuilder bldr = new MessageBuilder(88, Packet.Size.VariableShort);

        bldr.addRS2String(str);
        bldr.addLEShortA(id);
        player.dispatch(bldr);
    }

    /**
     * Method sendItemOnInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param interfaceId
     * @param child
     * @param size
     * @param id
     */
    public static void sendItemOnInterface(Player player, int interfaceId, int child, int size, int id) {
        player.dispatch(new MessageBuilder(91,
                                           Packet.Size.Fixed).addLEShortA(id).addInt(size).addInt2(interfaceId << 16
                                           | child));
    }

    /**
     * Method sendVariableItemOnInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param interfaceId
     * @param child
     * @param size
     * @param id
     */
    public static void sendVariableItemOnInterface(Player player, int interfaceId, int child, int size, int id) {
        player.dispatch(new MessageBuilder(56,
                                           Packet.Size.Fixed).addLEInt(interfaceId << 16
                                           | child).addShortA(0).addShort(0).addShort(1300));
    }

    /**
     * Method sendInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param showId
     * @param windowId
     * @param interfaceId
     * @param childId
     */
    public void sendInterface(int showId, int windowId, int interfaceId, int childId) {
        MessageBuilder spb = new MessageBuilder().setId(50).addInt2(windowId * 65536
                                 + interfaceId).addLEShortA(interfaceId >> 16 | childId).addByteC(showId);

        getInterface(interfaceId).interfaceRefreshed(player);
        player.getIoSession().write(spb.toPacket());
    }

    /**
     * Method sendWindowsPane
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pane
     */
    public void sendWindowsPane(int pane) {
        MessageBuilder spb = new MessageBuilder().setId(36).addByteA((byte) 1).addShortA(pane);

        player.getIoSession().write(spb.toPacket());
    }

    /**
     * Method sendInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param status
     * @param container
     * @param child
     */
    public void sendInterface(int status, int container, int child) {
        sendInterface(status, getRoot(), container, child);
    }

    /**
     * Method sendInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     * @param c2
     */
    public void sendInterface(int c, int c2) {
        sendInterface(1, c, c2);
    }

    /**
     * Method doTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param owner
     */
    @Override
    public void doTick(Player owner) {
        if ((currentShop != null) && currentShop.hasChanged() && (currentMainWindow == INTER_SHOP)) {
            ShopInterfaceAdapter.resetShop(owner, currentShop);
        }

        for (int i = 0; i < tabs.length; i++) {
            getInterface(tabs[i]).tick(player);
        }

        getInterface(currentMainWindow).tick(player);
        getInterface(currentDialog).tick(player);
    }
}
