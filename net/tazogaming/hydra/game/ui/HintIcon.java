package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/08/14
 * Time: 01:19
 */
public class HintIcon {

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    /** location made: 14/08/23 **/
    private Point location;

    public Entity getFocusEntity() {
        return focusEntity;
    }

    /** focusEntity made: 14/08/23 **/
    private Entity focusEntity;

    public int getDrawheight() {
        return drawheight;
    }

    /** drawheight, sprite made: 14/08/23 **/
    private int drawheight;

    public int getSprite() {
        return sprite;
    }

    private int sprite;

    /**
     * Constructs ...
     *
     *
     * @param location
     * @param drawheight
     * @param sprite
     */
    public HintIcon(Point location, int drawheight, int sprite) {
        this.location   = location;
        this.drawheight = drawheight;
        this.sprite = sprite;
    }

    public HintIcon(Point location) {
        this(location, 100, 0);
    }

    public HintIcon(Entity entity){
        this.focusEntity = entity;
    }

    /**
     * Method getTargetType
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTargetType() {
        if (focusEntity != null) {
            if (focusEntity instanceof NPC) {
                return 1;
            } else if (focusEntity instanceof Player) {
                return 10;
            }
        }

        return 2;
    }
}
