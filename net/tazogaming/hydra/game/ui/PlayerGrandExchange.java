package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.ge.BuyOffer;
import net.tazogaming.hydra.game.ui.ge.Offer;
import net.tazogaming.hydra.game.ui.ge.OfferState;
import net.tazogaming.hydra.game.ui.ge.SellOffer;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.game.ui.ge.concurrent.impl.AbortBuyOfferCommand;
import net.tazogaming.hydra.game.ui.ge.concurrent.impl.AbortSellOfferCommand;
import net.tazogaming.hydra.game.ui.ge.concurrent.impl.SubmitOfferCommand;
import net.tazogaming.hydra.game.ui.ge.easyeco.EasyEcoItem;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ModelGrid;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ge.GrandExchange;
import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/2015
 * Time: 14:13
 */
public class PlayerGrandExchange {

    /** autoSells made: 15/02/17 */
    private static List<Integer> autoSells = new ArrayList<Integer>();

    /** current_ge_slot made: 15/01/24 */
    private int current_ge_slot = -1;

    /** current_offer_item_id made: 15/01/24 */
    private int current_offer_item_id = -1;

    /** current_offer_price_per_item made: 15/01/24 */
    private int current_offer_price_per_item = 0;

    /** current_offer_quantity made: 15/01/24 */
    private int current_offer_quantity = -1;

    /** isBuy made: 15/01/24 */
    private boolean isBuy = false;

    /** offers made: 15/01/23 */
    private Offer[] offers = new Offer[6];

    /** player made: 15/01/24 */
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    public PlayerGrandExchange(Player player) {
        this.player = player;
    }

    /**
     * Method loadAutoSells
     * Created on 15/02/17
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadAutoSells() {
        autoSells.clear();

        TextFile file = TextFile.fromPath("config/grandexchange/auto_sell.cfg");

        for (Line l : file) {
            if (l.getData().startsWith("sell")) {
                String[] split = l.getData().split(" = ");

                System.err.println("added auto sell: " + split[1]);
                autoSells.add(Integer.parseInt(split[1]));
            }
        }
    }

    /**
     * Method setCurItem
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!                                                          bbbbbbbbbbbyrt y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6y6b
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param current_offer_item_id
     */
    public void setCurItem(int current_offer_item_id) {
        this.current_offer_item_id = current_offer_item_id;
        this.player.getActionSender().sendVar(1109, this.current_offer_item_id);

        double mod = isBuying()
                     ? GrandExchangeProcessor.getSingleton().getTaxRate()
                     : 1.00;

        this.setPPI((int) (Item.forId(current_offer_item_id).getExchangePrice() * mod));
        player.getActionSender().sendVar(1114, (int) (Item.forId(current_offer_item_id).getExchangePrice() * mod));
        this.setCurrOfferQuantity(1);
    }

    /**
     * Method openOfferScreen
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     *
     * @return
     */
    public Offer getOffer(int slot) {
        return offers[slot];
    }

    /**
     * Method openOfferScreen
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param buy
     */
    public void openOfferScreen(int slot, boolean buy) {
        player.getActionSender().sendVar(1112, slot);

        if (buy) {
            player.getActionSender().sendVar(1113, 0);
        } else {
            player.getActionSender().sendVar(1113, 1);

            if (offers[slot] == null) {
                ModelGrid grid = new ModelGrid(93, 107, 18);

                grid.addOption("Offer");
                player.getActionSender().sendClientScript(grid.getCS2());
                player.getGameFrame().setInventoryInterface(107, 93);
                player.getGameFrame().sendIComponentSettings(107, 18, 0, 27, 1026);
            }
        }

        this.current_ge_slot              = slot;
        this.current_offer_item_id        = -1;
        this.current_offer_price_per_item = 0;
        this.current_offer_quantity       = 0;

        if ((this.current_offer_item_id == -1) && buy) {
            CS2Call C = new CS2Call(570);

            C.addString("Grand exchange item search");
            player.getGameFrame().sendInterface(0, 752, 7, 389);
            player.getActionSender().sendClientScript(C);
        }

        isBuy = buy;
        this.player.getActionSender().sendVar(1111, this.current_offer_price_per_item);
        this.player.getActionSender().sendVar(1110, this.current_offer_quantity);
        this.player.getActionSender().sendVar(1109, this.current_offer_item_id);
    }

    /**
     * Method submitOffer
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean submitOffer() {
        if (this.current_ge_slot != -1) {
            if (this.offers[current_ge_slot] == null) {
                if (this.getCurrentGEItem() < -1) {
                    this.player.getActionSender().sendMessage("Invalid item selected");

                    return false;
                }

                if (this.current_offer_price_per_item <= 0) {
                    this.player.getActionSender().sendMessage("Minimum price not met");

                    return false;
                }

                if (this.current_offer_quantity <= 0) {
                    this.player.getActionSender().sendMessage("Minimum quantity not met");

                    return false;
                }

                if (isBuying()) {
                    int totalCost = current_offer_price_per_item * current_offer_quantity;

                    if (!player.getInventory().hasItem(995, totalCost)) {
                        player.getActionSender().sendMessage("You don't have enough gold");

                        return false;
                    }

                    try {
                        submitBuyOffer(this.current_offer_item_id, this.current_offer_quantity,
                                       this.current_offer_price_per_item, this.current_ge_slot);
                        this.updateOffers(player);
                        this.closeOfferScreen();
                    } catch (Exception ee) {
                        ee.printStackTrace();
                        player.getActionSender().sendMessage("Unable to make offer.");
                    }

                    return true;
                } else {
                    submitSellOffer(this.current_offer_item_id, this.current_offer_quantity,
                                    this.current_offer_price_per_item, this.current_ge_slot);
                    closeOfferScreen();
                }
            }
        }

        return false;
    }

    /**
     * Method setPPI
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param price
     */
    public void setPPI(int price) {
        this.current_offer_price_per_item = price;
        this.player.getActionSender().sendVar(1111, current_offer_price_per_item);
    }

    /**
     * Method increasePrice
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param price
     */
    public void increasePrice(int price) {
        if (((long) this.current_offer_price_per_item + (long) price) > Integer.MAX_VALUE) {
            this.setPPI(Integer.MAX_VALUE);
        } else {
            this.setPPI(this.current_offer_price_per_item + price);
        }
    }

    /**
     * Method decreasePrice
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param price
     */
    public void decreasePrice(int price) {
        if (this.current_offer_price_per_item - price < 0) {
            this.setPPI(0);
        } else {
            this.setPPI(this.current_offer_price_per_item - price);
        }
    }

    /**
     * Method getCurOfferScreen
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurOfferScreen() {
        return current_ge_slot;
    }

    /**
     * Method getQuantity
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getQuantity() {
        return current_offer_quantity;
    }

    /**
     * Method closeOfferScreen
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void closeOfferScreen() {
        if (this.current_ge_slot != -1) {
            player.getActionSender().sendClientScript(new CS2Call(571));
            this.current_ge_slot = -1;
            player.getActionSender().sendVar(1112, -1);
        }
    }

    /**
     * Method nextBuyID
     * Created on 15/02/17
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws Exception
     */
    public static final synchronized int nextBuyID() throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(new File("characters/grand_exchange.txt")));
        int            id     = Integer.parseInt(reader.readLine()) + 1;
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("characters/grand_exchange.txt")));

        writer.write("" + id);
        writer.flush();
        writer.close();

        return id;
    }

    /**
     * Method setCurrOfferQuantity
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param q
     */
    public void setCurrOfferQuantity(int q) {
        this.current_offer_quantity = q;
        this.player.getActionSender().sendVar(1110, this.current_offer_quantity);
    }

    /**
     * Method submitSellOffer
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amount
     * @param price
     * @param slot
     */
    public void submitSellOffer(int itemid, int amount, int price, int slot) {

        if(amount <= 0)
            return;


        if (itemid <= 0) {
            player.getActionSender().sendMessage("Item not set");

            return;
        }

        boolean hasMain = (player.getInventory().hasItem(itemid, amount));
        boolean hasNote = Item.forId(itemid + 1).isNote() && player.getInventory().hasItem(itemid + 1, amount);

        if (!hasMain &&!hasNote) {
            player.getActionSender().sendMessage("You don't have enough");

            return;
        }

        if (offers[slot] == null) {
            if (price == 0) {
                player.getActionSender().sendMessage("Minimum price not met");

                return;
            }

            for (Offer offer : offers) {
                if ((offer != null) && (offer instanceof SellOffer) && (offer.getItem() == itemid)) {
                    player.getActionSender().sendMessage("You already have a sell offer for this item");

                    return;
                }
            }

            long l = (long) ((long) price * (long) amount);

            if (l > Integer.MAX_VALUE) {
                player.getActionSender().sendMessage(
                    "Maximum gold limit reached please sell an amount that will equal to less than 2.1b GP");

                return;
            }



            int id = -1;

            try {
                id = nextBuyID();
            } catch (Exception ee) {
                ee.printStackTrace();
                player.getActionSender().sendMessage("Unable to submit offer, unable to generate offer id");

                return;
            }

            player.log("[GE] Submit Sell offer: " + itemid + " x " + amount + " at " + price + " each [offer id: "
                       + id);
            this.offers[slot] = new SellOffer(player, id, itemid, amount, price);

            for(int i = 0; i < 1; i++)
            {
                if(GrandExchangeProcessor.getEasyBuys().containsKey(this.offers[slot].getItem()))
                {
                    if(price <= (Item.forId(itemid).getExchangePrice() * 1.1))
                    {
                        EasyEcoItem easyEco =  GrandExchangeProcessor.getEasyBuys().get(itemid);
                        int max = easyEco.getMaximum(player.getUID());
                        if(max == 0) {
                            break;
                        }

                        int toDel = itemid;
                        if(hasNote) {
                            amount = player.getInventory().maxItem(itemid + 1, amount);
                            toDel = itemid + 1;
                        }else
                            amount = player.getInventory().maxItem(itemid, amount);

                        if(amount <= 0) return;

                        int origAmount = amount;
                        if(amount > max)
                            amount = max;


                        max = amount;

                        SellOffer offer = (SellOffer)offers[slot];

                        easyEco.register(player.getUID(), max);
                        offer.setRemaining(offer.getAmount() - max);
                        offer.setCurrentPot((offer.getAmount() * price) - (max * price));
                        offer.setRemaining(offer.getAmount() - max);
                        player.getInventory().deleteItem(toDel, max);
                        offer.setSpentAmount(max);
                        offer.getCollectionBox().setCashAmt(max * price);

                        if(max == offer.getAmount()){
                            offer.setState(OfferState.COMPLETED);
                            return;
                        }
                    }
                }
            }

            SubmitOfferCommand command = new SubmitOfferCommand(player, this.offers[slot]);

            player.getActionSender().sendMessage("Submitting offer to server....");
            GrandExchangeProcessor.getSingleton().submitCommand(command);
            updateOffers(player);
        }
    }

    /**
     * Method submitBuyOffer
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemID
     * @param amount
     * @param pricePerItem
     * @param slot
     */
    public void submitBuyOffer(int itemID, int amount, int pricePerItem, int slot) {

        if(amount <= 0 || pricePerItem <= 0)
            return;

        long l = (long) ((long) amount * (long) pricePerItem);

        if (itemID <= 0) {
            return;
        }

        if (l > Integer.MAX_VALUE) {
            player.getActionSender().sendMessage("You can't buy that many at that price.");

            return;
        }

        if ((amount == 0) || (pricePerItem == 0)) {
            return;
        }

        if (!player.getInventory().hasItem(995, (int) l)) {
            player.getActionSender().sendMessage("You don't have enough gold for that");

            return;
        }

        for (Offer offer : offers) {
            if ((offer instanceof BuyOffer) && (offer.getItem() == itemID)) {
                player.getActionSender().sendMessage("You already have an offer for this item open");

                return;
            }
        }

        int id = 0;

        try {
            id = nextBuyID();
        } catch (Exception error) {
            error.printStackTrace();
            player.getActionSender().sendMessage("Unable to create offer buyid not set");

            return;
        }

        player.log("[GE] Submit buy offer: " + itemID + " x " + amount + " at " + pricePerItem + " each [offer id: "
                   + id);

        BuyOffer offer = new BuyOffer(id, player, itemID, amount, pricePerItem);

        offers[slot] = offer;

        if (pricePerItem >= Item.forId(itemID).getExchangePrice()) {
           for(int i = 0; i < 1; i++) {
               if (GrandExchangeProcessor.getEasySells().containsKey(itemID)) {
                   EasyEcoItem item = GrandExchangeProcessor.getEasySells().get(itemID);
                   long uid = player.getUID();
                   int max = item.getMaximum(uid);
                   if (max == 0)
                       break;
                   long lx = (long) (((long) Item.forId(i).getExchangePrice() * (long) amount));

                   if ((lx <= Integer.MAX_VALUE)
                           && player.getInventory().hasItem(995, Item.forId(i).getExchangePrice() * amount)) {

                       if (pricePerItem >= Item.forId(itemID).getExchangePrice() * 1.1) {
                           if (max > offers[slot].getAmount())
                               max = offers[slot].getAmount();

                           item.register(uid, max);

                           player.getInventory().deleteItem(995, pricePerItem * max);
                           offers[slot].setRemaining(offers[slot].getAmount() - max);
                           offers[slot].getCollectionBox().setItemAmt(max);
                           offers[slot].setSpentAmount(max);
                           offers[slot].setCurrentPot((pricePerItem * amount) - (pricePerItem * max));
                           if (max == offers[slot].getAmount()) {
                               offer.setState(OfferState.COMPLETED);
                               return;
                           }
                       }
                   }
               }
           }
            for (Integer i : autoSells) {

                if (i == itemID) {
                    long lx = (long) (((long) Item.forId(i).getExchangePrice() * (long) amount));

                    if ((lx <= Integer.MAX_VALUE)
                            && player.getInventory().hasItem(995, Item.forId(i).getExchangePrice() * amount)) {
                        player.getInventory().deleteItem(995, Item.forId(i).getExchangePrice() * amount);
                        offers[slot].setState(OfferState.COMPLETED);
                        offers[slot].setRemaining(0);
                        offers[slot].setCurrentPot(pricePerItem * amount);
                        offers[slot].getCollectionBox().setItemAmt(amount);

                        return;
                    }
                }
            }
        }

        SubmitOfferCommand command = new SubmitOfferCommand(player, offer);

        GrandExchangeProcessor.getSingleton().submitCommand(command);
        player.getActionSender().sendMessage("Submitting offer to server..");
        updateOffers(player);
    }

    /**
     * Method tick
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {
        for (Offer offer : offers) {
            if (offer != null) {
                if (offer.getState() == OfferState.FAILED) {
                    if ((getCurOfferScreen() > -1) && (offers[getCurOfferScreen()] == offer)) {
                        this.closeOfferScreen();
                    }

                    offer = null;
                }
            }
        }
    }

    /**
     * Method abortOffer
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param offerID
     *
     * @return
     */
    public boolean abortOffer(int offerID) {
        if ((offers[offerID] == null) || (offers[offerID].getState() != OfferState.ACTIVE)) {
            return false;
        }

        offers[offerID].setState(OfferState.ABORTING);

        if (offers[offerID] instanceof BuyOffer) {
            player.log("Abort buy offer: " + offers[offerID].getItem() + " [offerid: " + offerID);
            GrandExchangeProcessor.getSingleton().submitCommand(new AbortBuyOfferCommand((BuyOffer) offers[offerID]));
        } else if (offers[offerID] instanceof SellOffer) {
            player.log("Abort sell offer: " + offers[offerID].getItem() + " [offerid: " + offerID);
            GrandExchangeProcessor.getSingleton().submitCommand(new AbortSellOfferCommand((SellOffer) offers[offerID]));
        }

        return true;
    }

    /**
     * Method hasPendingOffers
     * Created on 15/02/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasPendingOffers() {
        for (Offer offer : offers) {
            if ((offer != null)
                    && ((offer.getState() == OfferState.SUBMITTING) || (offer.getState() == OfferState.ABORTING))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getOffers
     * Created on 15/02/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Offer[] getOffers() {
        return offers;
    }

    /**
     * Method increaseOffer
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     */
    public void increaseOffer(int amt) {
        if (this.current_offer_quantity + amt > 0) {
            this.current_offer_quantity += amt;
        } else {
            this.current_offer_quantity = Integer.MAX_VALUE;
        }

        this.player.getActionSender().sendVar(1110, this.current_offer_quantity);
    }

    /**
     * Method decreaseOffer
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     */
    public void decreaseOffer(int amt) {
        if (this.current_offer_quantity - amt > -1) {
            this.current_offer_quantity -= amt;
        } else {
            this.current_offer_quantity = 0;
        }

        this.player.getActionSender().sendVar(1110, this.current_offer_quantity);
    }

    /**
     * Method isBuying
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBuying() {
        return isBuy;
    }

    /**
     * Method getCurrentGEItem
     * Created on 15/01/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentGEItem() {
        return current_offer_item_id;
    }

    /**
     * Method matchOffers
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void updateOffers(Player player) {
        for (int i = 0; i < 6; i++) {
            if ((offers[i] != null) && (offers[i].getState() == OfferState.FAILED)) {
                offers[i] = null;
            }

            MessageBuilder builder = new MessageBuilder().setId(83);

            builder.addByte(i);

            if (offers[i] == null) {
                builder.addByte(0);
                builder.addShort(0);
                builder.addInt(0);
                builder.addInt(0);
                builder.addInt(0);
                builder.addInt(0);
            } else {
                if ((offers[i].getState() == OfferState.SUBMITTING) || (offers[i].getState() == OfferState.LOADING)) {
                    if (offers[i] instanceof BuyOffer) {
                        builder.addByte(1);
                    } else {
                        builder.addByte(9);
                    }
                } else if ((offers[i].getState() == OfferState.ABORTED)) {
                    if (offers[i] instanceof BuyOffer) {
                        builder.addByte(5);
                    } else {
                        builder.addByte(13);
                    }
                } else {
                    if (offers[i] instanceof BuyOffer) {
                        if (offers[i].getState() == OfferState.COMPLETED) {
                            builder.addByte(5);
                        } else {
                            builder.addByte(2);
                        }
                    } else {
                        builder.addByte((offers[i].getState() == OfferState.COMPLETED)
                                        ? 13
                                        : 12);
                    }
                }

                builder.addShort(offers[i].getItem());
                builder.addInt(offers[i].getPricePerItem());
                builder.addInt(offers[i].getAmount());
                builder.addInt(offers[i].getAmount() - offers[i].getRemaining());
                builder.addInt(offers[i].getCurrentPot());

                int slot = i;

                if (offers[slot] != null) {
                    if (offers[slot].getCollectionBox().getItemAmt() > 0) {
                        player.getActionSender().setItem(Item.forId(offers[slot].getItem()),
                                                         offers[slot].getCollectionBox().getItemAmt(), 0, 523 + slot,
                                                         false);
                    } else {
                        player.getActionSender().setItem(null, 0, 0, 523 + slot, false);
                    }

                    if (offers[slot].getCollectionBox().getCashAmt() > 0) {
                        player.getActionSender().setItem(Item.forId(995), offers[slot].getCollectionBox().getCashAmt(),
                                                         1, 523 + slot, false);
                    } else {
                        player.getActionSender().setItem(null, 0, 1, 523 + slot, false);
                    }
                }
            }

            player.getIoSession().write(builder.toPacket());
        }
    }

    /**
     * Method getCurrentPPI
     * Created on 15/02/17
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentPPI() {
        return current_offer_price_per_item;
    }

    /**
     * Method setOffer
     * Created on 15/02/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param offer
     * @param slot
     */
    public void setOffer(Offer offer, int slot) {
        offers[slot] = offer;
    }
}
