package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.ui.ge.easyeco.EasyEcoRecord;
import net.tazogaming.hydra.io.Buffer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 20:10
 */
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.security.BanList;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class Bank {
    public BankTab[]    tabs       = new BankTab[9];
    public int          tabOffset  = 0;
    public GridSettings knownGrid  = null;
    public int          currentTab = 0;

    /*
     * Rebuild the bank, and send the data to the client
     * send the grid settings and the current tab
     * Send the models, to place onto the grid
     */

    /** built made: 14/11/12 **/
    private boolean built = false;

    /** dontRebuild made: 14/11/12 **/
    private boolean dontRebuild = false;

    /** player made: 14/11/12 **/
    private Player player;

    /*
     * Constructs the bank, and presets the interface ids for the bank tabs.
     */

    /**
     * Constructs ...
     *
     *
     * @param plr
     */
    public Bank(Player plr) {
        this.player = plr;

        int k = 10334;

        for (int i = 0; i < tabs.length; i++) {
            tabs[i]             = new BankTab();
            tabs[i].interfaceId = k;
            k++;
        }
    }

    /*
     * Allows the viewer to view a certain players bank
     *
     */

    /**
     * Method viewBank
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param viewing
     */
    public static void viewBank(Player pla, Player viewing) {
        viewing.getBank().rebuildBank(pla);
        pla.getActionSender().showInterface(5292);
    }

    /*
     * Returns the interface id for the tab number
     */

    /**
     * Method getInterfaceIdForOffset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public int getInterfaceIdForOffset(int i) {
        if (i == 0) {
            return 0;
        }

        return 10334 + i;
    }

    /**
     * Method hasItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     * @param count
     *
     * @return
     */
    public boolean hasItem(int amt, int count) {
        for (BankTab t : tabs) {
            if (t == null) {
                continue;
            }

            for (int i = 0; i < t.tabItems.length; i++) {
                if ((t.tabItems[i] != null) && (t.tabItems[i].getIndex() == amt)) {
                    return true;
                }
            }
        }

        return false;
    }
    public boolean hasItemAmt(int amt, int count) {
        for (BankTab t : tabs) {
            if (t == null) {
                continue;
            }

            for (int i = 0; i < t.tabItems.length; i++) {
                if ((t.tabItems[i] != null) && (t.tabItems[i].getIndex() == amt && t.tabItemsAmount[i] >= count)) {
                    return true;
                }
            }
        }

        return false;
    }

    /*
     * Check to see if the tabs empty, if so rebuild it and move it
     * along to the left 1 place
     */

    /**
     * Method checkTabs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void checkTabs() {}

    /**
     * Method deleteItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param amt
     * @param rebuild
     */
    public void deleteItem(int itemId, int amt, boolean rebuild) {
        int slot = 0;

        for (BankTab t : tabs) {
            slot = t.getItemSlot(itemId);

            if (slot != -1) {
                t.withdrawItem(slot, amt);
            }
        }

        if (rebuild) {
            rebuildBank();
        }
    }

    /**
     * Method deleteItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param amt
     */
    public void deleteItem(int itemId, int amt) {
        deleteItem(itemId, amt, true);
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param file_chunk
     */
    public void load(Buffer file_chunk) {
        int version = file_chunk.readByte();
        int tabAmount = file_chunk.readByte();
        boolean agsRemove = false;



        this.tabOffset = tabAmount;

        for (int i = 0; i < tabAmount; i++) {
            Item[] tabItems   = new Item[450];
            int[]  itemsCount = new int[450];
            int    am         = file_chunk.readShort();

            for (int i3 = 0; i3 < am; i3++) {
                tabItems[i3]   = Item.forId(file_chunk.readShort());





                itemsCount[i3] = file_chunk.readDWord();
                if(tabItems[i3] != null && agsRemove && EasyEcoRecord.shouldWipe(player, tabItems[i3].getIndex())){

                    if(itemsCount[i3] > 3) {
                        itemsCount[i3] = 2;
                        System.err.println(player.getUsername()+" wiped "+itemsCount[i3]+" ags");
                    }

                }
            }



            tabs[i]                = new BankTab();
            tabs[i].tabItems       = tabItems;
            tabs[i].tabItemsAmount = itemsCount;
            tabs[i].caret          = am;
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param file_chunk
     */
    public void save(Buffer file_chunk) {
        checkTabs();
        file_chunk.addByte(1);
        file_chunk.addByte(tabOffset);

        for (int i = 0; i < tabOffset; i++) {
            file_chunk.writeWord(tabs[i].getAmount());

            if (tabs[i].getAmount() > 0) {
                for (int grid_index = 0; grid_index < tabs[i].getAmount(); grid_index++) {
                    file_chunk.writeWord(tabs[i].tabItems[grid_index].getIndex());
                    file_chunk.writeDWord(tabs[i].tabItemsAmount[grid_index]);
                }
            }
        }
    }

    /*
     * Send the 'Grid settings'
     * The grid settings determains the shape of the bank screen, eg where to display the tab lines
     * how many tabs there is in 1 interface, offsets and other things.
     */

    /**
     * Method sendGridSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void sendGridSettings(GridSettings t) {
        MessageBuilder spb = new MessageBuilder().setId(127).setSize(Packet.Size.VariableByte);

        if (t == null) {
            spb.addByte((byte) 0);
        } else {
            spb.addByte((byte) (t.tabAmount));

            for (int i = 0; i < t.tabAmount; i++) {
                spb.addByte((byte) t.rowsInTabs[i]);
                spb.addShort(t.firstSlots[i]);
            }
        }

        // player.getIoSession().write(spb.toPacket());
    }

    /*
     * Send the number of tabs needed, as well as the
     * models that identify them (first interfaces in slot)
     */

    /**
     * Method buildTabs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void buildTabs() {
        for (int i = 1; i < tabs.length; i++) {
            Item[] item    = { null };
            int[]  amounts = { 0 };

            if (tabs[i].getAmount() != 0) {
                item[0]    = tabs[i].getFirstItem();
                amounts[0] = 1;    // {1};
            }

            player.getActionSender().sendModelGrid(item, amounts, tabs[i].interfaceId);
        }
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        int count = 0;

        for (int i = 0; i < tabOffset; i++) {
            count += tabs[i].getAmount();
        }

        return count;
    }

    /**
     * Method tabForInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceID
     *
     * @return
     */
    public int tabForInterface(int interfaceID) {
        switch (interfaceID) {
        case 62 :
            return 0;

        case 60 :
            return 1;

        case 58 :
            return 2;

        case 56 :
            return 3;

        case 54 :
            return 4;

        case 52 :
            return 5;

        case 50 :
            return 6;

        case 48 :
            return 7;

        case 46 :
            return 8;

        case 44 :
            return 9;
        }

        return -1;
    }

    /**
     * Method dragItemToTab
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param fromIndex
     * @param tabID
     */
    public void dragItemToTab(int fromIndex, int tabID) {
        int[] tabAttributes = getTabAttributes(fromIndex);
        int   toTabID       = tabForInterface(tabID);

        if (toTabID == -1) {
            throw new RuntimeException("error invalid tab!");
        }

        if (toTabID > tabOffset) {
            toTabID = tabOffset;
        }

        if (toTabID == tabOffset) {
            tabOffset++;
        }

        if ((tabAttributes[0] == 0) && (getItemsInTab(0) == 1)) {
            return;    // invalid move
        }

        Item tmp       = tabs[tabAttributes[0]].tabItems[tabAttributes[1]];
        int  tmpAmount = tabs[tabAttributes[0]].tabItemsAmount[tabAttributes[1]];

        tabs[tabAttributes[0]].deleteItem(tabAttributes[1]);
        tabs[toTabID].insertItem(tmp.getIndex(), tmpAmount);
        shiftTabs();
        rebuildBank();
    }

    private int[] getTabAttributes(int from) {
        int leng        = size();
        int mainTabSize = getItemsInTab(0);
        int tab         = -1;
        int slot        = -1;

        if (from >= leng - mainTabSize) {
            if (from > size()) {
                throw new RuntimeException("Error, invalid slot " + from + " " + size());
            }

            tab = 0;

            int calc = leng - mainTabSize;

            slot = from - calc;
        } else {
            int count = 0;

            for (int i = 1; i < tabOffset; i++) {
                if (count + getItemsInTab(i) > from) {
                    tab  = i;
                    slot = from - count;

                    break;
                } else {
                    count += getItemsInTab(i);
                }
            }
        }

        if ((tab == -1) || (slot == -1)) {
            throw new RuntimeException("Invalid tab and slot: " + tab + " " + slot);
        }

        return new int[] { tab, slot };
    }

    /**
     * Method shiftTabs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void shiftTabs() {
        BankTab[] tabs = new BankTab[10];

        for (int i = 0; i < 10; i++) {
            tabs[i] = new BankTab();
        }

        int c = tabOffset;

        tabOffset = 0;

        for (int i = 0; i < c; i++) {
            if (getItemsInTab(i) != 0) {
                tabs[tabOffset++] = this.tabs[i];
            }
        }

        if (tabOffset == 0) {
            tabOffset = 1;
        }

        this.tabs = tabs;
    }

    /**
     * Method moveItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     */
    public void moveItems(int from, int to) {
        int[] fromAttributes = getTabAttributes(from);
        int[] toAttributes   = getTabAttributes(to);
        int   fromTabID      = fromAttributes[0];
        int   fromSlot       = fromAttributes[1];
        int   toTabID        = toAttributes[0];
        int   toSlot         = toAttributes[1];

        if ((fromTabID == 0) && (toTabID != 0) && (getItemsInTab(0) == 1)) {
            return;    // invalid move
        }

        if ((fromTabID > tabOffset) || (toTabID > tabOffset)) {
            throw new RuntimeException("error, invalid tab selected: " + fromTabID + " " + toTabID + " " + tabOffset);
        }

        Item tmp       = tabs[fromTabID].tabItems[fromSlot];
        int  tmpAmount = tabs[fromTabID].tabItemsAmount[fromSlot];

        if (tmp == null) {
            throw new RuntimeException("Error source interfaces is null, " + fromTabID + " " + fromSlot);
        }

        if (tabs[toTabID].tabItems[toSlot] == null) {
            throw new RuntimeException("Error destination interfaces is null, " + toTabID + " " + toSlot);
        }

        if (player.getAccount().get(304) == 1) {
            tabs[fromTabID].deleteItem(fromSlot);
            tabs[toTabID].insertAfter(toSlot, tmp.getIndex(), tmpAmount);
            shiftTabs();
            rebuildBank();

            return;
        }

        tabs[fromTabID].tabItems[fromSlot]       = tabs[toTabID].tabItems[toSlot];
        tabs[fromTabID].tabItemsAmount[fromSlot] = tabs[toTabID].tabItemsAmount[toSlot];
        tabs[toTabID].tabItems[toSlot]           = tmp;
        tabs[toTabID].tabItemsAmount[toSlot]     = tmpAmount;
        shiftTabs();
        rebuildBank();
    }

    /**
     * Method rebuildBank
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void rebuildBank() {
        if (dontRebuild) {
            shiftTabs();

            return;
        }

        player.getGameFrame().sendAMask(0, 516, 762, 93, 40, 1278);
        player.getGameFrame().sendAMask(0, 27, 763, 0, 37, 1150);

        int count = 0;

        shiftTabs();

        if (this.currentTab >= tabOffset) {
            this.currentTab = tabOffset - 1;
        }

        sendTabConfig();

        int leng = 0;

        for (int i = 1; i < tabOffset; i++) {
            leng += getItemsInTab(i);
        }

        // append main tab to the end
        leng += getItemsInTab(0);

        Item[] container = new Item[leng];
        int[]  amounts   = new int[leng];

        for (int i = 1; i < tabOffset; i++) {
            System.arraycopy(tabs[i].tabItems, 0, container, count, getItemsInTab(i));
            System.arraycopy(tabs[i].tabItemsAmount, 0, amounts, count, getItemsInTab(i));
            count += getItemsInTab(i);
        }

        // copy main tab data
        System.arraycopy(tabs[0].tabItems, 0, container, count, getItemsInTab(0));
        System.arraycopy(tabs[0].tabItemsAmount, 0, amounts, count, getItemsInTab(0));
        player.getGameFrame().sendString("Bank of Hydrascape", 762, 45);
        player.getGameFrame().sendString("" + size(), 762, 31);
        player.getGameFrame().sendString("" + getMaxSlots(), 762, 32);
        player.getActionSender().sendItems(95, container, amounts, false);
    }

    /**
     * Method rebuildBank
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public void rebuildBank(Player plr) {
        checkTabs();

        if (this.currentTab > tabOffset) {
            this.currentTab = tabOffset;
        }

        buildTabs();

        if (this.currentTab == 0) {
            GridSettings tt = this.getGridSettings();

            this.knownGrid = tt;
            plr.getBank().sendGridSettings(tt);
            plr.getActionSender().sendModelGrid(tt.items, tt.itemsCount, 5382);
        } else {
            sendGridSettings(null);    // set bank screen to default.

            BankTab t = tabs[currentTab];

            plr.getActionSender().sendModelGrid(t.tabItems, t.tabItemsAmount, 5382);
        }

        plr.getActionSender().changeLine(this.getUsedSlots() + "", 19995);
        plr.getActionSender().changeLine("" + getMaxSlots(), 19996);
    }

    /**
     * Method getMaxSlots
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaxSlots() {
        int min = 350;

        switch (player.getRights()) {
        case Player.PATRON :
        case Player.EXTREME_DONATOR :
        case Player.MODERATOR :
        case Player.COMMUNITY_MANAGER:
        case Player.OFFICIAL_HELPER :
        case Player.ADMINISTRATOR :
        case Player.ROOT_ADMIN :
            min = 400;
            break;

        case Player.PLATINUM :
            min = 370;

            break;

        case Player.DONATOR :
            min = 365;
        }

        if (player.getAccount().hasVar("bank_extra")) {
            min += player.getAccount().getInt32("bank_extra");
        }

        if (min > 450) {
            min = 450;
        }

        return min;
    }

    /*
     * Returns the amount of an interfaces in the bank
     */

    /**
     * Method getAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public int getAmount(Item item) {
        if (item == null) {
            return 0;
        }

        for (BankTab tt : tabs) {
            for (int i = 0; i < tt.tabItems.length; i++) {
                if ((tt.tabItems[i] != null) && (tt.tabItems[i].getIndex() == item.getIndex())) {
                    return tt.tabItemsAmount[i];
                }
            }
        }

        return 0;
    }



    public void checkDuper() {
        if(player.getRights() < Player.ADMINISTRATOR) {
            for (BankTab tab : tabs) {
                if (tab != null) {
                    for (int i = 0; i < tab.tabItems.length; i++) {
                        if (tab.tabItems[i] != null) {
                            long price = (long) ((long) tab.tabItems[i].getExchangePrice() * (long) tab.tabItemsAmount[i]);
                            long priceHax = (long) ((long)Integer.MAX_VALUE * 65L);
                            if (price >= priceHax && tab.tabItems[i].getExchangePrice() > 1000000) {
                                World.getWorld().getDatabaseLink().query("UPDATE user set banned=1 where id='" + player.getId() + "'");
                                World.getWorld().getDatabaseLink().query("INSERT Into userban (id, userid, bannedby, date, reason, proof, lifton) VALUES(null, " + player.getId() + ", '11', NOW(), 'DUPE DETECTION itemid:"+tab.tabItems[i].getIndex()+" amount: "+tab.tabItemsAmount[i]+" (PLEASE REVIEW)', 'NO PROOF', 0)");
                                player.getIoSession().close();
                            }
                        }
                    }
                }
            }
        }
    }
    public void checkWhips() {
        checkDuper();
       if(!player.getAccount().hasVar("whip_check")){
                  player.getAccount().setSetting("whip_check", 1, true);
       }else{
           return;
       }

        if(hasItemAmt(4151, 60)){
            for(BankTab t: tabs){
                if(t.itemExists(4151)){
                    int slot = t.getItemSlot(4151);
                    if(slot != -1){
                        t.tabItemsAmount[slot] = 1;
                    }
                }
            }
        }
    }

    /*
     * Returns the 'free space' in the bank.
     */

    /**
     * Method getSpace
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSpace() {
        return ((getMaxSlots() - 1) - getUsedSlots());
    }

    /*
     * Bank an interfaces,
     */

    /**
     * Method bankItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemSlot
     * @param amount
     */
    public void bankItem(int itemSlot, int amount) {
        if(amount <= 0)
            return;

        Item item = player.getInventory().getItem(itemSlot);

        if (item == null) {
            return;
        }

        int itemId = item.getIndex();

        amount = player.getInventory().getItemMax(itemSlot, amount);
        player.log("bank item: "+item.getName()+" "+amount);

        if (itemId == 995) {
            int am = getAmount(Item.forId(995));

            if (am + amount < am) {
                player.getActionSender().sendMessage("Your cash is at its maximum value, please note your cash");
                player.getActionSender().sendMessage(
                    "Immediately to stop it being deleted, the admin team wont be held liable for this.");

                return;
            }
        }

        if (getSpace() <= 0 && !item.isStackable() && !this.hasItem(itemId, 1)) {
            player.getActionSender().sendMessage("You dont have enough space.");

            return;
        }

        if ((itemId == -1) || (amount == 0) || (Item.forId(itemId) == null)) {
            return;
        }

        if (item.isStackable() || (amount == 1)) {
            player.getInventory().deleteItemFromSlot(item, amount, itemSlot);
        } else {
            player.getInventory().deleteItem(Item.forId(itemId), amount);
        }

        insert(itemId, amount);
        rebuildBank();
    }

    /*
     * Add a new tab to the bank
     */

    /**
     * Method addNewTab
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amount
     *
     * @return
     */
    public boolean addNewTab(int itemid, int amount) {
        if (tabOffset == 9) {
            return false;
        }

        tabOffset++;
        tabs[tabOffset].insertItem(itemid, amount);

        return true;
    }

    /*
     * move items around
     */

    /**
     * Method moveItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     */
    public void moveItem(int from, int to) {
        if (this.currentTab == 0) {    // main area
            if (this.knownGrid == null) {
                System.out.println("Error grid null");

                return;
            }

            int tabIdFrom = knownGrid.tabIds[from];
            int tabIdTo   = knownGrid.tabIds[to];

            if (tabIdTo == tabIdFrom) {
                if ((this.tabs[tabIdTo] == null) || (this.tabs[tabIdFrom] == null)) {
                    return;
                }

                this.tabs[tabIdTo].moveItems(this.knownGrid.tabSlots[from], this.knownGrid.tabSlots[to]);
                rebuildBank();

                return;
            }

            Item item = this.tabs[tabIdFrom].tabItems[this.knownGrid.tabSlots[from]];

            if (item == null) {
                return;
            }

            int amt = this.tabs[tabIdFrom].tabItemsAmount[this.knownGrid.tabSlots[from]];

            this.tabs[tabIdFrom].deleteItem(this.knownGrid.tabSlots[from]);
            this.tabs[tabIdTo].insertItem(item.getIndex(), amt);
            rebuildBank();

            return;
        } else {
            BankTab t = tabs[currentTab];

            if (t.getAvailableSlot() == to) {
                return;
            }

            t.moveItems(from, to);
            rebuildBank();
        }
    }

    /**
     * Method checkButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     */
    public void checkButton(int button) {
        if (button == 10324) {
            currentTab = 0;
            rebuildBank();

            return;
        }

        int   i          = -1;
        int[] tabButtons = {
            10324, 10325, 10326, 10327, 10328, 10329, 10330, 10331, 10332, 10333
        };

        for (int i2 = 0; i2 < tabButtons.length; i2++) {
            if (tabButtons[i2] == button) {
                i = i2;

                break;
            }
        }

        if (i == -1) {
            return;
        }

        BankTab t = tabs[i];

        if ((t == null) || (t.getAmount() == 0) || (i > tabOffset)) {
            return;
        }

        currentTab = i;
        rebuildBank();
    }

    /**
     * Method getItemsInTab
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public int getItemsInTab(int index) {
        if (tabs[index] == null) {
            return 0;
        }

        return tabs[index].getAmount();
    }

    /**
     * Method sendTabConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sendTabConfig() {
        int config = 0;

        config += getItemsInTab(1);
        config += getItemsInTab(2) << 10;
        config += getItemsInTab(3) << 20;
        player.getActionSender().sendVar(1246, config);
        config = 0;
        config += getItemsInTab(4);
        config += getItemsInTab(5) << 10;
        config += getItemsInTab(6) << 20;
        player.getActionSender().sendVar(1247, config);

        int tab = (currentTab + 1);

        config = -2013265920;
        config += (134217728 * ((tab == 10)
                                ? 0
                                : tab - 1));
        config += getItemsInTab(7);
        config += getItemsInTab(8) << 10;
        player.getActionSender().sendVar(1248, config);
    }

    /*
     * Returns a tab for an interface id
     */

    /**
     * Method getTabForInterfaceId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceId
     *
     * @return
     */
    public int getTabForInterfaceId(int interfaceId) {
        for (int i = 0; i < tabs.length; i++) {
            if (tabs[i].interfaceId == interfaceId) {
                return i;
            }
        }

        return -1;
    }

    /*
     * Called when a player drags an interfaces to a tab
     */

    /**
     * Method dragToBank
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param interfaceId
     */
    public void dragToBank(int slot, int interfaceId) {
        if (this.currentTab == 0) {
            Item i       = this.knownGrid.items[slot];
            int  tabId   = this.knownGrid.tabIds[slot];
            int  tabSlot = this.knownGrid.tabSlots[slot];

            if ((i == null) || (tabSlot == -1)) {
                return;
            }

            int tabIndex = getTabForInterfaceId(interfaceId);

            if (tabIndex == -1) {
                return;
            }

            BankTab detectedTab = tabs[tabIndex];

            if ((detectedTab == null) || (tabId == tabIndex)) {
                return;
            }

            if (tabIndex > tabOffset + 1) {
                boolean b = addNewTab(i.getIndex(), this.knownGrid.itemsCount[slot]);

                if (!b) {
                    player.getActionSender().sendMessage("You don't have any tabs left.");

                    return;
                }

                tabs[tabId].deleteItem(tabSlot);
                rebuildBank();

                return;
            } else {
                int slot2 = detectedTab.getAvailableSlot();

                if (slot2 == -1) {
                    player.getActionSender().sendMessage("This tab is full");

                    return;
                }

                tabs[tabId].deleteItem(tabSlot);
                detectedTab.insertItem(i.getIndex(), this.knownGrid.itemsCount[slot]);
                rebuildBank();
            }
        } else {
            BankTab t = tabs[currentTab];

            if (t.getAmount() == 0) {
                return;
            }

            Item i  = t.tabItems[slot];
            int  am = t.tabItemsAmount[slot];

            if ((am == 0) || (i == null)) {
                return;
            }

            int tabIndex = getTabForInterfaceId(interfaceId);

            if (tabIndex == -1) {
                return;
            }

            BankTab detectedTab = tabs[tabIndex];

            if ((detectedTab == null) || (currentTab == tabIndex)) {
                return;
            }

            int s = detectedTab.getAvailableSlot();

            if (s == -1) {
                player.getActionSender().sendMessage("This tab is full");

                return;
            }

            tabs[currentTab].deleteItem(slot);
            detectedTab.insertItem(i.getIndex(), am);
            rebuildBank();
        }
    }

    /**
     * Method getNotedItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public Item getNotedItem(Item i) {

        return Item.forId(i.getNotedId());
    }

    /**
     * Method getUnNotedItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public Item getUnNotedItem(Item i) {
        return Item.forId(i.getUnNotedId());
    }

    /**
     * Method removeItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param amount
     */
    public void removeItem(int slot, int amount) {

        if(amount < 0)
            return;

        int[] attributes = getTabAttributes(slot);

        if ((attributes[0] == -1) || (attributes[1] == -1)) {
            return;
        }


        int currentTab = attributes[0];

        slot = attributes[1];

        Item i  = this.tabs[currentTab].tabItems[slot];
        Item originalItem = i;
        int  am = this.tabs[currentTab].tabItemsAmount[slot];

        if ((i == null) || (am == 0)) {
            return;
        }

        if (amount > am) {
            amount = am;
        }

        if ((i.getIndex() != 0) && (player.getAccount().get(Account.BANK_WITHDRAW_MODE) == 1)) {
            i = getNotedItem(i);

            if (!i.isNote() ||!i.getName().equalsIgnoreCase(this.tabs[currentTab].tabItems[slot].getName())) {
                player.getActionSender().sendMessage("This item cant be withdrawn as a note");
                i = originalItem;
            }
        }

        int max = getMaxWithdrawal(i.getIndex(), amount);

        if (max == 0) {
            player.getActionSender().sendMessage("Your inventory is full");

            return;
        }

        if (i.getIndex() == 995) {
            long l = (long) player.getInventory().getStackCount(995) + (long) amount;

            if (l > Integer.MAX_VALUE) {
                player.getActionSender().sendMessage("Your cash stack is at max value.");

                return;
            }
        }

        if (max < amount) {
            player.getActionSender().sendMessage("You dont have enough free slots");
        }



        amount = max;
        this.tabs[currentTab].withdrawItem(slot, amount);
        player.getInventory().addItem(i, amount);
        rebuildBank();
    }

    /**
     * Method getMaxWithdrawal
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amount
     *
     * @return
     */
    public int getMaxWithdrawal(int itemid, int amount) {
        Item it = Item.forId(itemid);

        if (it.isStackable()) {
            if (player.getInventory().hasItem(it, 1)) {
                return amount;
            }

            if (player.getInventory().getNextSlot() != -1) {
                return amount;
            }
        }

        int slots = player.getInventory().getFreeSlots(itemid);

        if (amount < slots) {
            return amount;
        }

        return slots;
    }

    /**
     * Method depositInventory
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void depositInventory() {
        setRebuildDisabled(true);

        for (int i = 0; i < 28; i++) {
            try {
                if (getSpace() <= 0) {
                    player.getActionSender().sendMessage("You don't have enough space.");
                    setRebuildDisabled(false);
                    rebuildBank();

                    return;
                }

                Item item   = player.getInventory().getItem(i);
                int  amount = player.getInventory().getItemsCount()[i];

                if (item != null) {
                    if (insert(item.getIndex(), amount)) {
                        player.getInventory().deleteItemFromSlot(item, amount, i);
                    }
                }
            } catch (Exception ee) {}
        }

        setRebuildDisabled(false);
        rebuildBank();
    }

    private void setRebuildDisabled(boolean disabled) {
        this.dontRebuild = disabled;
    }

    /**
     * Method depositWornItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void depositWornItem() {
        setRebuildDisabled(true);

        for (int i = 0; i < 14; i++) {
            try {
                int id = player.getEquipment().getId(i);

                if (id != -1) {
                    if (getSpace() == 0) {
                        player.getActionSender().sendMessage("Not enough space.");
                        setRebuildDisabled(false);
                        rebuildBank();

                        return;
                    }

                    insert(id, player.getEquipment().getAmount(i));
                    player.getEquipment().removeItem(i);
                    player.getEquipment().updateEquipment(i);
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        setRebuildDisabled(false);
        player.getAppearance().setChanged(true);
        rebuildBank();
    }

    /**
     * Method insert
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param amount
     *
     * @return
     */
    public boolean insert(int itemId, int amount) {
        Item i = Item.forId(itemId);

        if (i == null) {
            return true;
        }

        if (i.isNote()) {
            itemId = getUnNotedItem(i).getIndex();
            i      = Item.forId(itemId);
        }

        if ((amount == 0) || (amount == -1)) {
            return true;
        }

        BankTab t = findByItem(itemId);

        if (t == null) {
            t = tabs[currentTab];
        }

        int  slot  = t.getItemSlot(i.getIndex());
      if(slot != -1) {
          int count = t.tabItemsAmount[slot];
          long check = (long) ((long) count + (long) amount);

          if (check > Integer.MAX_VALUE) {
              return false;
          }
      }

        t.insertItem(itemId, amount);
        return true;
    }

    /*
     * Finds the bank tab that an interfaces is currently in
     */

    /**
     * Method findByItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     *
     * @return
     */
    public BankTab findByItem(int itemid) {
        for (int i = 0; i < tabOffset; i++) {
            if (tabs[i].itemExists(itemid)) {
                return tabs[i];
            }
        }

        return null;
    }

    /**
     * Method getUsedSlots
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getUsedSlots() {
        int i = 0;

        for (BankTab t : tabs) {
            i += t.getAmount();
        }

        return i;
    }

    /**
     * Method depositBob
     * Created on 14/11/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void depositBob() {
        if (!player.getAccount().hasVar("bob_max") || (player.getCurrentFamiliar() == null)) {
            player.getActionSender().sendMessage("You don't have a BoB active.");
            return;
        }

        for (int i = 0; i < player.getBobItems().length; i++) {


            if (player.getBobItems()[i] > 0) {
                if(getSpace() == 0) {
                    player.getActionSender().sendMessage("Not enough space");
                    return;
                }
                player.getBank().insert(player.getBobItems()[i], player.getBobItemsC()[i]);
                player.getBobItems()[i] = -1;
                player.getBobItemsC()[i] = 0;
            }
        }

        rebuildBank();
    }

    /*
     * Gets the grid settings for the interface,
     * Basically a way of reshaping the entire bank interface to support the users layout.
     */

    /**
     * Method getGridSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public GridSettings getGridSettings() {
        int       rowCount      = 0;
        final int MAX_ROWS      = 35;
        final int ITEMS_PER_ROW = 10;
        int       slots         = getMaxSlots() + 100;
        Item[]    retItems      = new Item[slots];
        int[]     itemAmounts   = new int[slots];
        int[]     tabSlots      = new int[slots];
        int[]     tabIds        = new int[slots];
        int[]     rowAmount     = new int[9];
        int[]     firstSlots    = new int[9];
        int       tabAmount     = 0;
        int       offset        = 0;

        for (int i = 0; i < tabOffset; i++) {
            int siz = tabs[i].getAmount();

            if (siz == 0) {
                continue;
            }

            tabAmount++;
            rowAmount[i] = 1;

            if (i != 0) {
                firstSlots[i] = offset;
            }

            for (int k = 0; k < siz; k++) {
                retItems[offset]    = (tabs[i].tabItems[k]);
                itemAmounts[offset] = tabs[i].tabItemsAmount[k];
                tabSlots[offset]    = k;
                tabIds[offset]      = i;
                rowCount++;
                offset++;

                if ((rowCount == ITEMS_PER_ROW) && (k != siz - 1)) {
                    rowCount = 0;    // new row
                    rowAmount[i]++;
                }
            }

            if (rowCount != 0) {
                for (int i2 = rowCount; i2 < ITEMS_PER_ROW; i2++) {
                    tabIds[offset]     = i;
                    tabSlots[offset++] = i2;
                }

                rowCount = 0;
            }
        }

        GridSettings it = new GridSettings();

        it.items      = retItems;
        it.itemsCount = itemAmounts;
        it.rowsInTabs = rowAmount;
        it.tabAmount  = tabAmount;
        it.firstSlots = firstSlots;
        it.tabIds     = tabIds;
        it.tabSlots   = tabSlots;

        return it;
    }
}
