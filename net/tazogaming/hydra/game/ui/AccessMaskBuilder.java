package net.tazogaming.hydra.game.ui;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/07/14
 * Time: 16:35
 */
public class AccessMaskBuilder {
    public static final Settings[] ALL_SETTINGS = {
        Settings.GROUND_ITEMS, Settings.NPCS, Settings.PLAYERS, Settings.SELF, Settings.INTERFACECOMPONENT,
        Settings.SOMETHING
    };

    /**
     * Contains the value which should be sended in access mask packet.
     */
    private int value;

    /**
     * Sets use on settings.
     * By use on , I mean the options such as Cast in spellbook or use in inventory.
     * If nothing is allowed then 'use' option will not appear in right click menu.
     */
    public enum Settings {
        GROUND_ITEMS(0x1), NPCS(0x2), OBJECTS(0X4), PLAYERS(0x8), SELF(0X10), INTERFACECOMPONENT(0x20), SOMETHING(0x40);

        private int value;

        Settings(int value) {
            this.value = value;
        }

        /**
         * Method getValue
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        int getValue() {
            return value;
        }
    }



    /**
     * Set's standart option settings.
     * Great example of standart click option is the Continue button in dialog interface.
     * If the option is not allowed the packet won't be send to server.
     * @param allowed
     */
    public void setStandardClickOptionSettings(boolean allowed) {
        value &= ~(0x1);

        if (allowed) {
            value |= 0x1;
        }
    }

    /**
     * Set's right click option settings.
     * Great example of right click option is the Dismiss option in summoning orb.
     * If specified option is not allowed , it will not appear in right click menu and the packet will not be send
     * to server when clicked.
     *
     * @param optionID
     * @param allowed
     *
     * @return
     */
    public AccessMaskBuilder setRightClickOptionSettings(int optionID, boolean allowed) {
        if ((optionID < 0) || (optionID > 9)) {
            throw new IllegalArgumentException("optionID must be 0-9.");
        }

        value &= ~(0x1 << (optionID + 1));    // disable

        if (allowed) {
            value |= (0x1 << (optionID + 1));
        }

        return this;
    }

    /**
     * Method enableAllRightClickSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public AccessMaskBuilder enableAllRightClickSettings() {
        for (int i = 0; i < 9; i++) {
            setRightClickOptionSettings(i, true);
        }

        return this;
    }

    /**
     * Method setUseOnSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param settings
     *
     * @return
     */
    public AccessMaskBuilder setUseOnSettings(Settings... settings) {
        int useFlag = 0;

        for (Settings setting : settings) {
            useFlag |= setting.value;
        }

        value &= ~(127 << 7);    // disable
        value |= useFlag << 7;

        return this;
    }

    /**
     * Set's interface events depth.
     * For example, we have inventory interface which is opened
     * on gameframe interface (548 or 746).
     * If depth is 1 , then the clicks in inventory will also invoke click event handler scripts
     * on gameframe interface.
     *
     * @param depth
     */
    public void setInterfaceEventsDepth(int depth) {
        if ((depth < 0) || (depth > 7)) {
            throw new IllegalArgumentException("depth must be 0-7.");
        }

        value &= ~(0x7 << 18);
        value |= (depth << 18);
    }

    /**
     * Set's canUseOnFlag.
     * if it's true then other interface components can do use on this interface component.
     * Example would be using High alchemy spell on the inventory interfaces.
     * If inventory component where items are stored doesn't allow the canUseOn , it would not be possible to use High Alchemy on that
     * interfaces.
     *
     * @param allow
     *
     * @return
     */
    public AccessMaskBuilder setCanUseOn(boolean allow) {
        value &= ~(1 << 22);

        if (allow) {
            value |= (1 << 22);
            value |= (0x20 << 11);
            value |= (0x26caa3 << 21);
        }

        return this;
    }

    /**
     * Method getValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getValue() {
        return value;
    }
}
