package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRules;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.impact.LeechImpactEvent;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator Enchanta is a Runescape 2 Server emulator which
 * has been designed for educational purposes only Created by Daniel Hadland
 * Date: 30/09/13 Time: 21:35
 */
public class PrayerBook {
	public static final int CURSE_PROTECT_ITEM = 0, SAP_WARRIOR = 1, SAP_RANGER = 2, SAP_MAGE = 3, SAP_SPIRIT = 4,
			BERSERKER = 5, DEFLECT_SUMMONING = 6, DEFLECT_MAGIC = 7, DEFLECT_MISSILES = 8, DEFLECT_MELEE = 9,
			LEECH_ATTACK = 10, LEECH_RANGE = 11, LEECH_MAGIC = 12, LEECH_DEFENCE = 13, LEECH_STRENGTH = 14,
			LEECH_ENERGY = 15, LEECH_SPECIAL_ATTACK = 16, WRATH = 17, SOUL_SPLIT = 18, TURMOIL = 19;
	public static final int THICK_SKIN = 0, BURST_OF_STRENGTH = 1, CLARITY_OF_THOUGHT = 2, SHARP_EYE = 3,
			MYSTIC_WILL = 4, ROCK_SKIN = 5, SUPERHUMAN_STRENGTH = 6, IMPROVED_REFLEXES = 7, RAPID_RESTORE = 8,
			RAPID_HEAL = 9, PROTECT_ITEM = 10, HAWK_EYE = 11, MYSTIC_LORE = 12, STEEL_SKIN = 13, ULTIMATE_STRENGTH = 14,
			INCREDIBLE_REFLEXES = 15, PROTECT_FROM_SUMMONING = 16, PROTECT_FROM_MAGIC = 17, PROTECT_FROM_MISSILES = 18,
			PROTECT_FROM_MELEE = 19, EAGLE_EYE = 20, MYSTIC_MIGHT = 21, RETRIBUTION = 22, REDEMPTION = 23, SMITE = 24,
			CHIVALRY = 25, RAPID_RENEWAL = 26, PIETY = 27, RIGOUR = 28, AUGURY = 29;

	/** PRAYER_REQS made: 14/10/17 */
	private final static int[][] PRAYER_REQS = {

			// normal prayer book
			{ 1, 4, 7, 8, 9, 10, 13, 16, 19, 22, 25, 26, 27, 28, 31, 34, 35, 37, 40, 43, 44, 45, 46, 49, 52, 60, 65, 70,
					74, 77 },

			// ancient prayer book
			{ 50, 50, 52, 54, 56, 59, 62, 65, 68, 71, 74, 76, 78, 80, 82, 84, 86, 89, 92, 95 } };

	/** PRAYER_NAME made: 14/10/17 */
	private static final String[] PRAYER_NAME = { "Protect from Melee", "Protect from Magic", "Protect from range",
			"Smite", "Redemption", "Burst of strength", "Thick skin", "Clarity of thought", "Rock skin",
			"Superhuman Strength", "Improved reflexes", "Rapid restore", "Rapid Heal", "Protect items", "Steel Skin",
			"Ultimate Strength", "Incredible Reflexes", "Chivalry", "Piety", "Retribution", "Sharp eye", "Mystic will",
			"Hawk Eye", "Mystic Lore", "Eagle Eye", "Mystic Might", "Chivalry", "Piety" };
	public static final int TYPE_NORMAL = 0, TYPE_CURSES = 1;

	/** CONFIG_VALUES made: 14/10/17 */
	private final static int[] CONFIG_VALUES = { 1, 2, 4, 262144, 524288, 8, 16, 32, 64, 128, 256, 1048576, 2097152,
			512, 1024, 2048, 16777216, 4096, 8192, 16384, 4194304, 8388608, 32768, 65536, 131072, 33554432, 134217728,
			67108864, 536870912, 268435456 };

	/**
	 * STRENGTH, DEFENCE, ATTACK, RANGE, MAGIC, MISC, HEADICONS made: 14/10/17
	 */
	private static final int STRENGTH = 0, DEFENCE = 1, ATTACK = 2, RANGE = 3, MAGIC = 4, MISC = 5, HEADICONS = 6;
	public static int PRAYER_TYPES[][] = { { SUPERHUMAN_STRENGTH, ULTIMATE_STRENGTH, BURST_OF_STRENGTH },
			{ ROCK_SKIN, STEEL_SKIN, THICK_SKIN }, { INCREDIBLE_REFLEXES, IMPROVED_REFLEXES, CLARITY_OF_THOUGHT },
			{ HAWK_EYE, EAGLE_EYE, SHARP_EYE, RIGOUR }, { MYSTIC_LORE, MYSTIC_MIGHT, MYSTIC_WILL, AUGURY },
			{ CHIVALRY, PIETY }, { PROTECT_FROM_SUMMONING, PROTECT_FROM_MAGIC, PROTECT_FROM_MISSILES,
					PROTECT_FROM_MELEE, SMITE, REDEMPTION, RETRIBUTION } };
	public static final int[] QUICK_PRAYER_CURSES = { CURSE_PROTECT_ITEM, SAP_WARRIOR, SAP_RANGER, SAP_MAGE, SAP_SPIRIT,
			BERSERKER, DEFLECT_SUMMONING, DEFLECT_MAGIC, DEFLECT_MISSILES, DEFLECT_MELEE, LEECH_ATTACK, LEECH_RANGE,
			LEECH_MAGIC, LEECH_DEFENCE, LEECH_STRENGTH, LEECH_ENERGY, LEECH_SPECIAL_ATTACK, WRATH, SOUL_SPLIT,
			TURMOIL };
	public static final int QUICK_PRAYER_NORMAL_BOOK[] = { THICK_SKIN, BURST_OF_STRENGTH, CLARITY_OF_THOUGHT, ROCK_SKIN,
			SUPERHUMAN_STRENGTH, IMPROVED_REFLEXES, RAPID_RESTORE, RAPID_HEAL, PROTECT_ITEM, STEEL_SKIN,
			ULTIMATE_STRENGTH, INCREDIBLE_REFLEXES, PROTECT_FROM_MAGIC, PROTECT_FROM_MISSILES, PROTECT_FROM_MELEE,
			RETRIBUTION, REDEMPTION, SMITE, SHARP_EYE, MYSTIC_WILL, HAWK_EYE, MYSTIC_LORE, EAGLE_EYE, MYSTIC_MIGHT,
			PROTECT_FROM_SUMMONING, CHIVALRY, PIETY, RAPID_RENEWAL, AUGURY, RIGOUR, };

	/** RANGE_PRAYERS made: 14/10/17 */
	static final private int[] RANGE_PRAYERS = { RIGOUR, SHARP_EYE, HAWK_EYE, EAGLE_EYE };

	/** MAGIC_PRAYERS made: 14/10/17 */
	static final private int[] MAGIC_PRAYERS = { MYSTIC_LORE, MYSTIC_MIGHT, MYSTIC_WILL, AUGURY };

	/** MELEE_PRAYERS made: 14/10/17 */
	static final private int[] MELEE_PRAYERS = { ULTIMATE_STRENGTH, CHIVALRY, PIETY, SUPERHUMAN_STRENGTH, STEEL_SKIN,
			ROCK_SKIN, BURST_OF_STRENGTH, CLARITY_OF_THOUGHT, INCREDIBLE_REFLEXES, IMPROVED_REFLEXES, THICK_SKIN };
	public static final int[] BIT_MASK = new int[30];

	static {
		int x = 1;

		BIT_MASK[0] = 1;

		for (int i = 1; i < BIT_MASK.length; i++) {
			x <<= 1;
			BIT_MASK[i] = x;
		}
	}

	/** curse_boosts made: 14/10/17 */
	private int[] curse_boosts = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

	/** bookType made: 14/10/17 */
	private int bookType = 0;

	/** activePrayers made: 14/10/17 */
	private boolean[] activePrayers = new boolean[30];

	/** selectedQuickPrayers made: 14/10/17 */
	private boolean[] selectedQuickPrayers = new boolean[35];

	/** currentPoints made: 14/10/17 */
	private double currentPoints;

	/** pla made: 14/10/17 */
	private final Player pla;

	/**
	 * Constructs ...
	 *
	 *
	 * @param p
	 */
	public PrayerBook(Player p) {
		this.pla = p;
	}

	private int getBitMaskID(int prayer) {
		if (!isCurses()) {
			for (int i = 0; i < QUICK_PRAYER_NORMAL_BOOK.length; i++) {
				if (prayer == QUICK_PRAYER_NORMAL_BOOK[i]) {
					return i;
				}
			}
		}

		return prayer;
	}

	private int getQuickPrayerBit() {
		int bit = 0;

		for (int i = 0; i < selectedQuickPrayers.length; i++) {
			if (selectedQuickPrayers[i]) {
				bit += BIT_MASK[getBitMaskID(i)];
			}
		}

		return bit;
	}

	private int blockTimer = 0;

	public void disableProtectionPrayers(int time) {
		if (isCurses()) {
			unsetAll(SOUL_SPLIT, DEFLECT_MAGIC, DEFLECT_MELEE, DEFLECT_MISSILES, DEFLECT_SUMMONING);
		} else {
			unsetAllPrayers(PROTECT_FROM_SUMMONING, PROTECT_FROM_MAGIC, PROTECT_FROM_MELEE, PROTECT_FROM_MISSILES);
		}
		updateInterface();
		this.blockTimer = Core.currentTime + time;

	}

	private int curseQuickIdForButton(int id) {
		return QUICK_PRAYER_CURSES[id];
	}

	private int curseQuickSlotForPrayer(int id) {
		for (int i = 0; i < QUICK_PRAYER_CURSES.length; i++) {
			if (QUICK_PRAYER_CURSES[i] == id) {
				return i;
			}
		}

		return -1;
	}

	private void refreshQuickPrayer() {
		pla.getActionSender().sendVar(isCurses() ? 1587 : 1397, getQuickPrayerBit());
	}

	private static boolean isLeech(int spell) {
		return (spell == LEECH_ATTACK) || (spell == LEECH_DEFENCE) || (spell == LEECH_MAGIC)
				|| (spell == LEECH_STRENGTH) || (spell == SAP_MAGE) || (spell == SAP_RANGER) || (spell == SAP_WARRIOR)
				|| (spell == SAP_SPIRIT) || (spell == LEECH_RANGE);
	}

	/**
	 * Method activeQuicks Created on 14/10/17
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void activeQuicks() {
		if (pla.getAccount().getButtonConfig(182) == 0) {
			for (int i = 0; i < activePrayers.length; i++) {
				if (selectedQuickPrayers[i] && !prayerActive(i)) {
					onButton(i);
				}
			}

			pla.getAccount().setB(182, 1, true);
		} else {
			resetAll();
			pla.getAccount().setB(182, 0, true);
		}
	}

	/**
	 * Method quickPrayerButtonSelected Created on 14/10/17
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param slot
	 */
	public void quickPrayerButtonSelected(int slot) {
		if (selectedQuickPrayers[slot]) {
			selectedQuickPrayers[slot] = false;
			refreshQuickPrayer();

			return;
		}

		if (isCurses()) {
			if (isLeech(slot) && selectedQuickPrayers[TURMOIL]) {
				pla.getActionSender().sendMessage("You can't use turmoil with leeches.");
				refreshQuickPrayer();

				return;
			}

			if (slot == TURMOIL) {
				for (int i = 0; i < selectedQuickPrayers.length; i++) {
					if (isLeech(i) && selectedQuickPrayers[i]) {
						pla.getActionSender().sendMessage("You can't use turmoil with leeches.");
						refreshQuickPrayer();

						return;
					}
				}
			}

			if ((slot == DEFLECT_MAGIC) || (slot == DEFLECT_MELEE) || (slot == DEFLECT_MISSILES) || (slot == SOUL_SPLIT)
					|| (slot == DEFLECT_SUMMONING)) {
				for (int i = 0; i < selectedQuickPrayers.length; i++) {
					if (selectedQuickPrayers[i] && ((i == DEFLECT_MAGIC) || (i == DEFLECT_MISSILES)
							|| (i == DEFLECT_SUMMONING) || (i == DEFLECT_MELEE) || (i == SOUL_SPLIT))) {
						pla.getActionSender().sendMessage("You can't use multiple protection prayers at once");
						refreshQuickPrayer();

						return;
					}
				}
			}

			if (((slot == SAP_SPIRIT) && isQuickSelected(LEECH_SPECIAL_ATTACK))
					|| ((slot == LEECH_SPECIAL_ATTACK) && isQuickSelected(SAP_SPIRIT))
					|| (((slot == SAP_WARRIOR) && (isQuickSelected(LEECH_DEFENCE) || isQuickSelected(LEECH_ATTACK)
							|| isQuickSelected(LEECH_STRENGTH) || isQuickSelected(LEECH_DEFENCE)))
							|| (((slot == LEECH_DEFENCE) || (slot == LEECH_ATTACK) || (slot == LEECH_STRENGTH))
									&& isQuickSelected(SAP_WARRIOR)))
					|| (((slot == LEECH_MAGIC) && isQuickSelected(SAP_MAGE))
							|| (((slot == SAP_MAGE) && isQuickSelected(LEECH_MAGIC))
									|| (isQuickSelected(LEECH_RANGE) && (slot == SAP_RANGER))
									|| (isQuickSelected(SAP_RANGER) && (slot == LEECH_RANGE))))) {
				pla.getActionSender()
						.sendMessage("You can't use this prayer with the current selection you already have.");
				refreshQuickPrayer();

				return;
			}

			selectedQuickPrayers[slot] = true;
			refreshQuickPrayer();
		} else {
			if (isProtectionPrayer(slot)) {
				for (int i = 0; i < selectedQuickPrayers.length; i++) {
					if (isProtectionPrayer(i) && selectedQuickPrayers[i]) {
						pla.getActionSender().sendMessage("You can't use these prayers together");
						refreshQuickPrayer();

						return;
					}
				}
			}

			if (isMeleePrayer(slot) && ((slot == CHIVALRY) || (slot == PIETY))) {
				for (int i = 0; i < activePrayers.length; i++) {
					if (isMeleePrayer(i) && selectedQuickPrayers[i]) {
						pla.getActionSender().sendMessage("You can't use this in combo with prayers you have ");
						refreshQuickPrayer();

						return;
					}
				}
			}

			if (isMagicPrayer(slot)) { // wouldn't you just add in && ((slot ==
										// AUGURY)))
				for (int i = 0; i < activePrayers.length; i++) {
					if (isMagicPrayer(i) && selectedQuickPrayers[i]) {
						pla.getActionSender().sendMessage("You can't use this in combo with prayers you have");
						refreshQuickPrayer();

						return;
					}
				}
			}

			if (isRangePrayer(slot)) {
				for (int i = 0; i < activePrayers.length; i++) {
					if (isRangePrayer(i) && selectedQuickPrayers[i]) {
						pla.getActionSender().sendMessage("You can't use this in combo with prayers you have");
						refreshQuickPrayer();

						return;
					}
				}
			}

			selectedQuickPrayers[slot] = true;
			refreshQuickPrayer();
		}
	}

	/**
	 * Method getQuickPrayers Created on 14/11/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean[] getQuickPrayers() {
		return selectedQuickPrayers;
	}

	/**
	 * Method setQuickPrayers Created on 14/11/19
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param prayers
	 */
	public void setQuickPrayers(boolean[] prayers) {
		this.selectedQuickPrayers = prayers;
	}

	private static boolean isRangePrayer(int slot) {
		for (int i = 0; i < RANGE_PRAYERS.length; i++) {
			if (RANGE_PRAYERS[i] == slot) {
				return true;
			}
		}

		return false;
	}

	private static boolean isMagicPrayer(int slot) {
		for (int i = 0; i < MAGIC_PRAYERS.length; i++) {
			if (MAGIC_PRAYERS[i] == slot) {
				return true;
			}
		}

		return false;
	}

	private static boolean isMeleePrayer(int id) {
		for (int i = 0; i < MELEE_PRAYERS.length; i++) {
			if (MELEE_PRAYERS[i] == id) {
				return true;
			}
		}

		return false;
	}

	private static boolean isProtectionPrayer(int slot) {
		return (slot == PROTECT_FROM_MELEE) || (slot == PROTECT_FROM_MAGIC) || (slot == SMITE)
				|| (slot == PROTECT_FROM_MISSILES) || (slot == REDEMPTION) || (slot == RETRIBUTION);
	}

	/**
	 * Method getBoost Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param stat
	 *
	 * @return
	 */
	public double getBoost(int stat) {
		switch (pla.getAccount().get(1584)) {
		case TYPE_NORMAL:
			return normalBoost(stat);

		case TYPE_CURSES:
			return curseBoost(stat);
		}

		throw new RuntimeException("Invalid prayer book type: " + pla.getAccount().get(1584));
	}

	/**
	 * Method setPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pts
	 */
	public void setPoints(double pts) {
		this.currentPoints = pts;
	}

	/**
	 * Method reset_leeches Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void reset_leeches() {
		for (int i = 0; i < curse_boosts.length; i++) {
			curse_boosts[i] = 0;
		}
	}

	private double normalBoost(int stat) {
		switch (stat) {
		case 0:
			if (activePrayers[CLARITY_OF_THOUGHT]) {
				return 1.05;
			} else if (activePrayers[IMPROVED_REFLEXES]) {
				return 1.10;
			} else if (activePrayers[INCREDIBLE_REFLEXES] || activePrayers[CHIVALRY]) {
				return 1.15;
			} else if (activePrayers[PIETY]) {
				return 1.20;
			}

			break;

		case 1:
			if (activePrayers[THICK_SKIN]) {
				return 1.05;
			}

			if (activePrayers[ROCK_SKIN]) {
				return 1.10;
			}

			if (activePrayers[STEEL_SKIN]) {
				return 1.15;
			}

			if (activePrayers[CHIVALRY]) {
				return 1.20;
			}

			if (activePrayers[PIETY]) {
				return 1.25;
			}

			if (activePrayers[AUGURY]) {
				return 1.25;
			}

			if (activePrayers[RIGOUR]) {
				return 1.20;
			}

			break;

		case 2:
			if (activePrayers[BURST_OF_STRENGTH]) {
				return 1.05;
			}

			if (activePrayers[SUPERHUMAN_STRENGTH]) {
				return 1.10;
			}

			if (activePrayers[ULTIMATE_STRENGTH]) {
				return 1.15;
			}

			if (activePrayers[CHIVALRY]) {
				return 1.18;
			}

			if (activePrayers[PIETY]) {
				return 1.20;
			}
		case 4:
			if (activePrayers[SHARP_EYE]) {
				return 1.05;
			}

			if (activePrayers[HAWK_EYE]) {
				return 1.10;
			}

			if (activePrayers[EAGLE_EYE]) {
				return 1.15;
			}

			if (activePrayers[RIGOUR]) {
				return 1.20;
			}

			break;

		case 6:
			if (activePrayers[MYSTIC_WILL]) {
				return 1.05;
			}

			if (activePrayers[MYSTIC_LORE]) {
				return 1.10;
			}

			if (activePrayers[MYSTIC_MIGHT]) {
				return 1.15;
			}

			if (activePrayers[AUGURY]) {
				return 1.20;
			}
		}

		return 1.00;
	}

	private int getDisplayedBoost(int stat) {
		return (int) (getBoost(stat) - 1) * 10;
	}

	private void updateInterface() {
		int mask = 0;

		for (int i = 0; i < activePrayers.length; i++) {
			if (activePrayers[i]) {
				mask |= isCurses() ? 1 << i : CONFIG_VALUES[i];
			}
		}

		pla.getActionSender().sendVar(isCurses() ? 1582 : 1395, mask);

		int stat = 30;
		int value = stat + getDisplayedBoost(0);

		value |= (stat + getDisplayedBoost(2)) << 6;
		value |= (stat + getDisplayedBoost(1)) << 12;
		value |= (stat + getDisplayedBoost(4)) << 18;
		value |= (stat + getDisplayedBoost(6)) << 24;
		pla.getActionSender().sendVar(1583, value);
	}

	private void handleNormalButton(int index) {
		if (pla.getPrayerBook().prayerActive(index)) {
			deActivate(index, true);

			return;
		}

		if (prayerBlocked(index)) {
			return;
		}

		if (pla.getMaxStat(5) < PRAYER_REQS[0][index]) {
			Dialog.printStopMessage(pla, "You need a prayer level of atleast " + PRAYER_REQS[0][index]);

			return;
		}

		activatePrayer(index);
	}

	private void curseUnactivated(int id) {
		switch (id) {
		case SOUL_SPLIT:
		case WRATH:
		case DEFLECT_SUMMONING:

		case DEFLECT_MISSILES:
		case DEFLECT_MELEE:
		case DEFLECT_MAGIC:
			pla.setHeadIcon(0);

			break;
		}
	}

	private void prayerUnactivated(int id) {
		switch (id) {
		case REDEMPTION:
		case SMITE:
		case PROTECT_FROM_MISSILES:
		case PROTECT_FROM_MAGIC:
		case PROTECT_FROM_SUMMONING:

		case PROTECT_FROM_MELEE:
		case RETRIBUTION:
			pla.setHeadIcon(0);

			break;
		}
	}

	private void deActivate(int id, boolean send) {
		if (activePrayers[id]) {
			prayerUnactivated(id);
		}

		activePrayers[id] = false;

		if (send) {
			updateInterface();
		}
	}

	private void unsetType(int type) {
		unsetType(type, false);
	}

	private void unsetType(int type, boolean reset) {
		for (int i = 0; i < PRAYER_TYPES[type].length; i++) {
			deActivate(PRAYER_TYPES[type][i], reset);
		}
	}

	private int getType(int id) {
		for (int i = 0; i < PRAYER_TYPES.length; i++) {
			for (int k = 0; k < PRAYER_TYPES[i].length; k++) {
				if (PRAYER_TYPES[i][k] == id) {
					return i;
				}
			}
		}

		return -1;
	}

	private void unsetAll(int... types) {
		if (isCurses()) {
			unsetCurses(types);
		} else {
			for (Integer type : types) {
				unsetType(type);
			}
		}
	}

	/**
	 * Method activatePrayer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void activatePrayer(int id) {
		prayerActivated(id);
		activePrayers[id] = true;
		updateInterface();
	}

	/**
	 * Method deActivate Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void deActivate(int id) {
		prayerUnactivated(id);
		activePrayers[id] = false;
		updateInterface();
	}

	private void prayerActivated(int id) {
		int type = getType(id);
		if (type != -1) {
			unsetType(type);
			if (type == RANGE) {
				unsetType(MAGIC);
				unsetType(ATTACK);
				unsetType(STRENGTH);
			} else if (type == MAGIC) {
				unsetType(RANGE);
				unsetType(ATTACK);
				unsetType(STRENGTH);
				unsetType(5);
			} else if (type == STRENGTH) {
				unsetType(RANGE);
				unsetType(MAGIC);
			}

			if ((id == CHIVALRY) || (id == PIETY)) {
				unsetAll(DEFENCE, STRENGTH, ATTACK, RANGE, MAGIC);
			} else if (type <= ATTACK) {
				unsetAll(MISC);
			} else {
				unsetType(type);

			}
		}

		switch (id) {
		case RIGOUR:
			unsetAll(MISC);

			break;

		case PROTECT_FROM_MELEE:
			pla.setHeadIcon(1);

			break;

		case PROTECT_FROM_MISSILES:
			pla.setHeadIcon(2);

			break;

		case PROTECT_FROM_MAGIC:
			pla.setHeadIcon(3);

			break;

		case RETRIBUTION:
			pla.setHeadIcon(4);

			break;

		case SMITE:
			pla.setHeadIcon(5);

			break;

		case REDEMPTION:
			pla.setHeadIcon(6);

			break;
		case PROTECT_FROM_SUMMONING:
			pla.setHeadIcon(8);

			break;

		case DEFLECT_SUMMONING:
			pla.setHeadIcon(8);
			break;

		}
	}

	/**
	 * Method apply_leech_boost Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param skillId
	 */
	public void apply_leech_boost(int skillId) {
		curse_boosts[skillId]++;

		if (curse_boosts[skillId] > 10) {
			curse_boosts[skillId] = 10;
		}
	}

	private void curseActivated(int d) {
		switch (d) {
		case SAP_MAGE:
			unsetAll(LEECH_MAGIC);
			break;
		case SAP_RANGER:
			unsetAll(LEECH_RANGE);
			unsetAll(TURMOIL);
			break;
		case SAP_SPIRIT:
			unsetAll(LEECH_SPECIAL_ATTACK);
			break;

		case SAP_WARRIOR:
			unsetAll(LEECH_DEFENCE);
			unsetAll(LEECH_ATTACK);
			unsetAll(LEECH_STRENGTH);
			break;

		case LEECH_ATTACK:
		case LEECH_DEFENCE:
		case LEECH_STRENGTH:
			unsetAll(SAP_WARRIOR);
			break;

		case LEECH_RANGE:
			unsetAll(SAP_RANGER);
			break;
		case LEECH_MAGIC:
			unsetAll(SAP_MAGE);
			break;

		case LEECH_SPECIAL_ATTACK:
			unsetAll(SAP_SPIRIT);
			break;
		}
		switch (d) {

		case CURSE_PROTECT_ITEM:
			if (pla.getTimers().timerActive(TimingUtility.ACTION_TIMER)) {
				break;
			}

			pla.getAnimation().animate(12567, Animation.LOW_PRIORITY);

			if (!pla.getGraphic().hasChanged()) {
				pla.getGraphic().set(2213, Graphic.LOW);
			}

			break;

		case BERSERKER:
			if (pla.getTimers().timerActive(TimingUtility.ACTION_TIMER)) {
				break;
			}

			pla.getAnimation().animate(12589, Animation.LOW_PRIORITY);

			if (!pla.getGraphic().hasChanged()) {
				pla.getGraphic().set(2266, Graphic.LOW);
			}

			break;

		case LEECH_ATTACK:
		case LEECH_DEFENCE:
		case LEECH_STRENGTH:
		case SAP_WARRIOR:
		case LEECH_MAGIC:
		case LEECH_SPECIAL_ATTACK:
		case SAP_SPIRIT:
		case SAP_RANGER:
		case LEECH_RANGE:

		case SAP_MAGE:
			unsetAll(TURMOIL);

			break;

		case TURMOIL:
			if (pla.getTimers().timerActive(TimingUtility.ACTION_TIMER)) {
				break;
			}

			pla.getAnimation().animate(12565, Animation.LOW_PRIORITY);

			if (!pla.getGraphic().hasChanged()) {
				pla.getGraphic().set(2226, Graphic.LOW);
			}

			reset_leeches();
			unsetAll(LEECH_ATTACK, LEECH_DEFENCE, LEECH_STRENGTH, SAP_WARRIOR, LEECH_MAGIC, LEECH_SPECIAL_ATTACK,
					SAP_SPIRIT, SAP_RANGER, SAP_MAGE, LEECH_RANGE);

			break;

		case DEFLECT_MELEE:
			unsetAll(SOUL_SPLIT, DEFLECT_MAGIC, DEFLECT_MISSILES, WRATH, DEFLECT_SUMMONING);
			pla.setHeadIcon(13);

			break;

		case DEFLECT_MAGIC:
			unsetAll(SOUL_SPLIT, DEFLECT_MELEE, DEFLECT_MISSILES, WRATH, DEFLECT_SUMMONING);
			pla.setHeadIcon(14);

			break;

		case DEFLECT_MISSILES:
			unsetAll(SOUL_SPLIT, DEFLECT_MAGIC, DEFLECT_MELEE, WRATH, DEFLECT_SUMMONING);
			pla.setHeadIcon(15);

			break;

		case WRATH:
			unsetAll(SOUL_SPLIT, DEFLECT_MAGIC, DEFLECT_MELEE, DEFLECT_MISSILES, DEFLECT_SUMMONING);
			pla.setHeadIcon(20);

			break;

		case SOUL_SPLIT:
			unsetAll(WRATH, DEFLECT_MAGIC, DEFLECT_MELEE, DEFLECT_MISSILES, DEFLECT_SUMMONING);
			pla.setHeadIcon(21);

			break;

		case DEFLECT_SUMMONING:
			pla.setHeadIcon(16);
			unsetAll(SOUL_SPLIT, WRATH, DEFLECT_MAGIC, DEFLECT_MELEE, DEFLECT_MISSILES);
			break;

		}
	}

	private void unsetCurses(int... ids) {
		for (Integer i : ids) {
			if (activePrayers[i]) {
				unsetCurse(i);
			}
		}
	}

	private void unsetAllPrayers(int... ids) {
		for (Integer i : ids) {
			if (activePrayers[i]) {
				prayerUnactivated(i);
				activePrayers[i] = false;
				updateInterface();
			}
		}
	}

	private void unsetCurse(int id) {
		curseUnactivated(id);
		activePrayers[id] = false;
		updateInterface();
	}

	/**
	 * Method handle_curse_button Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param index
	 */
	public void handle_curse_button(int index) {
		if (pla.isStunned())
			return;

		if (curseActive(index)) {
			unsetCurse(index);

			return;
		}

		if (PRAYER_REQS[1][index] > pla.getCurStat(5)) {
			pla.getActionSender()
					.sendMessage("You need a prayer level of atleast " + PRAYER_REQS[1][index] + " to use this.");

			return;
		}

		if (prayerBlocked(index)) {
			return;
		}

		curseActivated(index);
		activePrayers[index] = true;
		updateInterface();
	}

	/**
	 * Method update_curse_boost Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void update_curse_boost() {
		if (!isCurses()) {
			return;
		}

		for (int i = 0; i < curse_boosts.length; i++) {
			if (curse_boosts[i] > 0) {
				curse_boosts[i]--;
			}
		}
	}

	/**
	 * Method onButton Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param button
	 */
	public void onButton(int button) {
		int current_prayer = -1;

		if (pla.isStunned())
			return;

		if (isCurses()) {
			handle_curse_button(button);
		} else {
			handleNormalButton(button);
		}
	}

	private boolean prayerBlocked(int index) {
		if ((pla.getGetCurrentWar() != null)
				&& pla.getGetCurrentWar().getGameRules().settingActive(ClanWarRules.PRAYER_DISABLED)) {
			return true;
		}

		if (pla.getAccount().isActionDisabled(Account.PRAYER_DISABLED)) {
			pla.getActionSender().sendMessage(pla.getAccount().getDisabledMessage(Account.PRAYER_DISABLED));

			return true;
		}

		if (isCurses() && (pla.getCurStat(1) < 30)) {
			pla.getActionSender().sendMessage("You need a defence level of atleast 30 to use these prayers.");

			return true;
		}
		if (Core.timeUntil(blockTimer) > 0) {
			if (isCurses() && (index == PROTECT_FROM_MELEE || index == PROTECT_FROM_MAGIC
					|| index == PROTECT_FROM_MISSILES || index == PROTECT_FROM_SUMMONING)) {
				pla.getActionSender().sendMessage("Your prayer has been disabled for 5 seconds");
				return true;
			} else if (!isCurses()
					&& (index == PROTECT_FROM_MELEE || index == PROTECT_FROM_MAGIC || index == PROTECT_FROM_MISSILES)) {
				pla.getActionSender().sendMessage("Your prayer has been disabled for 5 seconds");

				return true;
			}

		}

		if ((index == PIETY) && (pla.getCurStat(1) < 70)) {
			pla.getActionSender().sendMessage("You need atleast 70 defence");

			return true;
		}

		if ((index == CHIVALRY) && (pla.getCurStat(1) < 65)) {
			Dialog.printStopMessage(pla, "You need atleast 65 defence.");

			return true;
		}

		Duel d = pla.getGameFrame().getDuel();

		if (d != null) {
			if (d.setting(Duel.NO_PRAYER)) {
				pla.getActionSender().sendMessage("Prayer has been disabled during this fight.");

				return true;
			}
		}

		if (isCurses()) {
			if (pla.getCurStat(1) < 30) {
				Dialog.printStopMessage(pla, "You need a defence level of atleast 30");

				return true;
			}

			if (pla.getCurStat(5) < PRAYER_REQS[1][index]) {
				Dialog.printStopMessage(pla, "You need a prayer level of atleast " + PRAYER_REQS[1][index]);

				return true;
			}
		}

		if (pla.getPrayerBook().getPoints() == 0) {
			pla.getActionSender().sendMessage("You've ran out of prayer points.");

			return true;
		}

		return false;
	}

	/**
	 * Method wrath Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void wrath(Killable killedBy) {
		pla.getGraphic().set(2259, Graphic.HIGH);
		int wrathDmg = pla.getMaxStat(5) / 10 * 3;
		if (pla.isInMultiArea()) {
			for (Killable k : pla.getViewArea().getTargetsInRange(5)) {
				if ((k.getCurrentInstance() == pla.getCurrentInstance())
						&& ((k instanceof Player) ? Combat.pvp_check(pla, k.getPlayer()) : true)) {
					k.hit(pla, GameMath.rand(wrathDmg), Damage.TYPE_OTHER, 3);
					k.getGraphic().set(2260, Graphic.HIGH);
				}
			}
		} else {
			if ((killedBy.getCurrentInstance() == pla.getCurrentInstance())
					&& ((killedBy instanceof Player) ? Combat.pvp_check(pla, killedBy.getPlayer()) : true)) {
				killedBy.hit(pla, GameMath.rand(wrathDmg), Damage.TYPE_OTHER, 3);
				killedBy.getGraphic().set(2260, Graphic.HIGH);
			}
		}
	}

	private double curseBoost(int stat) {
		return getCurseModifier(stat);
	}

	/**
	 * Method prayerActive Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public boolean prayerActive(int id) {
		if (bookType == TYPE_CURSES) {
			return false;
		}

		return activePrayers[id];
	}

	/**
	 * Method switchPrayerBook Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void switchPrayerBook() {
		selectedQuickPrayers = new boolean[35];
		resetAll();
		reset_leeches();
		pla.getAccount().toggle(1584);
	}

	/**
	 * Method resetAll Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void resetAll() {
		if (pla.getAccount().getButtonConfig(182) == 1) {
			pla.getAccount().setB(182, 0, true);
		}

		activePrayers = new boolean[30];
		updateInterface();

		if (pla.getHeadicon() > 0) {
			pla.setHeadIcon(0);
			pla.getAppearance().setChanged(true);
		}
	}

	private boolean isQuickSelected(int id) {
		return selectedQuickPrayers[id];
	}

	/**
	 * Method getCurseModifier Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public double getCurseModifier(int id) {
		int total = getTotalCurseBoost(id);

		if (total == 0) {
			return 1;
		}

		if (total < 0) {
			return 1 - (double) Math.abs(total) / 100d;
		} else {
			return 1 + Math.abs(total) / 100d;
		}
	}

	/**
	 * Method getTotalCurseBoost Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param d
	 *
	 * @return
	 */
	public int getTotalCurseBoost(int d) {
		if (!isCurses()) {
			return pla.get_curse_drain(d);
		}

		return (getStaticCurseBoost(d) + curse_boosts[d]) - pla.get_curse_drain(d);
	}

	/**
	 * Method getStaticCurseBoost Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 *
	 * @return
	 */
	public int getStaticCurseBoost(int id) {
		if (curseActive(TURMOIL) && (id == 2)) {
			return 32;
		}

		if (curseActive(TURMOIL) && ((id == 0) || (id == 1))) {
			return 15;
		}

		if (pla.getCombatAdapter().getFightingWith() == null) {
			return 0;
		}

		if (curseActive(LEECH_ATTACK) && (id == 0)) {
			return 5;
		}

		if (curseActive(LEECH_STRENGTH) && (id == 2)) {
			return 5;
		}

		if (curseActive(LEECH_DEFENCE) && (id == 1)) {
			return 5;
		}

		if (curseActive(LEECH_RANGE) && (id == 4)) {
			return 5;
		}

		if (curseActive(LEECH_MAGIC) && (id == 6)) {
			return 5;
		}

		return 0;
	}

	private void sendLeech(int projId, int finishId, int statId, Killable enemy) {
		if (enemy == null) {
			return;
		}

		Projectile projectile = new Projectile(pla, enemy, projId, 30, 0);

		projectile.setAttribute(1, 30);
		projectile.setAttribute(2, 20);
		projectile.setOnImpactEvent(new LeechImpactEvent(statId, 15, finishId));
		pla.registerProjectile(projectile);
	}

	/**
	 * Method getTurmoilStrIncrease Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getTurmoilStrIncrease() {
		if (!curseActive(TURMOIL)) {
			return 0;
		}

		if (pla.getCombatAdapter().getFightingWith() == null) {
			return 0;
		}

		Killable fightingWith = pla.getCombatAdapter().getFightingWith();
		int strLevel = (fightingWith instanceof NPC) ? fightingWith.getNpc().getDefinition().getCombatStats()[2]
				: fightingWith.getPlayer().getMaxStat(2);

		return (int) (strLevel * 0.10);
	}

	/**
	 * Method isCurses Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean isCurses() {
		return pla.getAccount().get(1584) == 1;
	}

	/**
	 * Method curseActive Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param index
	 *
	 * @return
	 */
	public boolean curseActive(int index) {
		if (isCurses()) {
			return activePrayers[index];
		}

		return false;
	}

	/**
	 * Method updateCurses Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updateCurses() {
		Killable enemy = pla.getCombatAdapter().getFightingWith();

		if (enemy == null) {
			reset_leeches();

			return;
		}

		if (curseActive(LEECH_ATTACK)) {
			sendLeech(2231, 2233, 0, enemy);
		}

		if (curseActive(LEECH_STRENGTH)) {
			sendLeech(2248, 2249, 2, enemy);
		}

		if (curseActive(LEECH_DEFENCE)) {
			sendLeech(2244, 2245, 1, enemy);
		}

		if (curseActive(LEECH_MAGIC)) {
			sendLeech(2240, 2241, 6, enemy);
		}

		if (curseActive(LEECH_RANGE)) {
			sendLeech(2236, 2237, 4, enemy);
		}
	}

	/**
	 * Method getActivePrayers Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public boolean[] getActivePrayers() {
		return activePrayers;
	}

	/**
	 * Method activePrayer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param id
	 */
	public void activePrayer(int id) {
		activePrayers[id] = true;
	}

	/**
	 * Method getPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public int getPoints() {
		return (int) currentPoints;
	}

	/**
	 * Method addPoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pts
	 */
	public void addPoints(double pts) {
		currentPoints += pts;

		if (currentPoints > pla.getMaxStat(5)) {
			currentPoints = pla.getMaxStat(5);
		}

		pla.getActionSender().sendStat(5);
	}

	/**
	 * Method removePoints Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pts
	 */
	public void removePoints(double pts) {
		this.currentPoints -= pts;

		if (currentPoints < 0) {
			currentPoints = 0;
		}
		pla.getActionSender().sendStat(5);
	}

	private double getModifier() {
		double curDrainRate = 0.25; // XXX idk tamper with this value until you
									// find somethng you like
		double percent = .033;
		double bonus = pla.getEquipment().getTotalBonuses()[Item.BONUS_PRAYER];

		percent *= bonus;
		curDrainRate *= 1 - percent;

		if (curDrainRate < 0) {
			curDrainRate = 0;
		}

		// System.out.println(curDrainRate);

		return curDrainRate;
	}

	/**
	 * Method updatePrayer Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void updatePrayer() {
		if (pla.isDead()) {
			return;
		}

		double lastpts = currentPoints;

		for (int i = 0; i < activePrayers.length; i++) {
			if (activePrayers[i]) {
				if ((pla.getPrestige(5) == 10) && (pla.getMaxStat(5) == 99)) {
					if ((Core.currentTime % 2200 == 0) && (Combat.getWildernessLevel(pla) == -1)) {
						resetAll();

						return;
					}

					continue;
				}

				currentPoints -= getModifier();

				if (currentPoints < 1) {
					currentPoints = 0;
					resetAll();
					pla.getActionSender().sendMessage("You've used up all your prayer points.");
					pla.getActionSender().sendStat(5);

					return;
				}
			}
		}

		if (lastpts != currentPoints) {
			pla.getActionSender().sendStat(5);
		}

		if (Core.currentTime % 11 == 0) {
			updateCurses();
		}
	}

	/**
	 * Method setType Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param t
	 */
	public void setType(int t) {
		bookType = t;
	}
}
