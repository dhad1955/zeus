package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/10/13
 * Time: 07:48
 */
public class CursePrayerBook extends GameInterface {

    /**
     * Constructs ...
     *
     */
    public CursePrayerBook() {
        super(22500);
    }

    /**
     * Method onButtonClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buttonId
     * @param pla
     */
    @Override
    public void onButtonClick(int buttonId, Player pla) {
        pla.getPrayerBook().onButton(buttonId);

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void onOpen(Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onClose
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void onClose(Player pla) {
        pla.getPrayerBook().resetAll();

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onItemClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceId
     * @param itemId
     * @param itemSlot
     * @param clickSlot
     * @param pla
     */
    @Override
    public void onItemClick(int interfaceId, int itemId, int itemSlot, int clickSlot, Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void tick(Player pla) {
        PrayerBook b               = pla.getPrayerBook();
        int[]      statAdjustments = { b.getTotalCurseBoost(0), b.getTotalCurseBoost(2), b.getTotalCurseBoost(1),
                                       b.getTotalCurseBoost(4), b.getTotalCurseBoost(6) };

        for (int i = 0; i < statAdjustments.length; i++) {
            if (pla.getWindowManager().getKnownVariable(10 + i) != statAdjustments[i]) {
                pla.getWindowManager().setKnownVar(10 + i, statAdjustments[i]);
            } else {
                return;
            }

            int    attackPercent = statAdjustments[i];
            String color         = "@or1@";

            if (attackPercent < 0) {
                color = "@red@";
            } else if (attackPercent > 0) {
                color = "@gre@";
            }

            pla.getActionSender().changeLine(color + attackPercent + "%", 37822 + i);
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method moveItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param subId
     * @param pla
     */
    @Override
    public void moveItems(int from, int to, int subId, Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
