package net.tazogaming.hydra.game.ui;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/03/14
 * Time: 01:50
 */
public class MenuBuilder {
    private String[] lines   = new String[100];
    private int      pointer = 0;

    /**
     * Constructs ...
     *
     *
     * @param title
     */
    public MenuBuilder(String title) {}

    /**
     * Method writeLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param line
     */
    public void writeLine(String line) {
        lines[pointer++] = line;
    }

    /**
     * Method newLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void newLine() {
        lines[pointer++] = "";
    }

    /**
     * Method getLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getLines() {
        return lines;
    }
}
