package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 00:43
 * To change this template use File | Settings | File Templates.
 */
public abstract class GameInterface {
    private int   interfaceId     = -1;
    private int[] subInterfaceIds = null;

    /**
     * Constructs ...
     *
     *
     * @param id
     */
    public GameInterface(int id) {
        interfaceId = id;
    }

    /**
     * Method onButtonClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buttonId
     * @param pla
     */
    public abstract void onButtonClick(int buttonId, Player pla);

    /**
     * Method onOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public abstract void onOpen(Player pla);

    /**
     * Method onClose
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public abstract void onClose(Player pla);

    /**
     * Method onItemClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceId
     * @param itemId
     * @param itemSlot
     * @param clickSlot
     * @param pla
     */
    public abstract void onItemClick(int interfaceId, int itemId, int itemSlot, int clickSlot, Player pla);

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public abstract void tick(Player pla);

    /**
     * Method moveItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param subId
     * @param pla
     */
    public abstract void moveItems(int from, int to, int subId, Player pla);

    /**
     * Method setSubInterfaceIds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ids
     */
    public void setSubInterfaceIds(int... ids) {
        subInterfaceIds = ids;
    }

    /**
     * Method getSubInterfaceId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getSubInterfaceId(int id) {
        if (subInterfaceIds == null) {
            return -1;
        }

        return subInterfaceIds[id];
    }

    /**
     * Method hasSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean hasSub(int id) {
        if (subInterfaceIds == null) {
            return false;
        }

        for (int i = 0; i < subInterfaceIds.length; i++) {
            if (subInterfaceIds[i] == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getInterfaceId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getInterfaceId() {
        return interfaceId;
    }
}
