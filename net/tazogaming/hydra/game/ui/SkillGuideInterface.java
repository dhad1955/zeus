package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 21:03
 */
public class SkillGuideInterface extends GameInterface {

    /**
     * Constructs ...
     *
     */
    public SkillGuideInterface() {
        super(8714);
    }

    /**
     * Method onButtonClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buttonId
     * @param pla
     */
    @Override
    public void onButtonClick(int buttonId, Player pla) {
        if ((buttonId < 0) || ((buttonId >= 8823) && (buttonId <= 15309))) {
            for (int i = 0; i < SkillGuide.SKILL_MENU_TEXT.length; i++) {
                if (SkillGuide.SKILL_MENU_TEXT[i] == buttonId) {
                    if (pla.getAccount().getInt32("skill_menu_id") == i) {
                        return;
                    }

                    pla.getAccount().setSetting("skill_menu_id", i);
                    pla.getWindowManager().getCurSkillGuide().render(pla);

                    return;
                }
            }
        }
    }

    /**
     * Method onOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void onOpen(Player pla) {
        pla.getWindowManager().getCurSkillGuide().render(pla);
    }

    /**
     * Method onClose
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void onClose(Player pla) {
        pla.getWindowManager().setCurSkillGuide(null);

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onItemClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param i2
     * @param itemSlot
     * @param clickSlot
     * @param pla
     */
    @Override
    public void onItemClick(int i, int i2, int itemSlot, int clickSlot, Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    @Override
    public void tick(Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method moveItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param subId
     * @param pla
     */
    @Override
    public void moveItems(int from, int to, int subId, Player pla) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
