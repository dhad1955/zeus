package net.tazogaming.hydra.game.ui;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 18:54
 */
public class SkillSub {
    private String name;
    private int    modelIndex;
    private int    levelRequired;

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method getLevelRequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevelRequired() {
        return levelRequired;
    }

    /**
     * Method setLevelRequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param levelRequired
     */
    public void setLevelRequired(int levelRequired) {
        this.levelRequired = levelRequired;
    }

    /**
     * Method getModelIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModelIndex() {
        return modelIndex;
    }

    /**
     * Method setModelIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param modelIndex
     */
    public void setModelIndex(int modelIndex) {
        this.modelIndex = modelIndex;
    }
}
