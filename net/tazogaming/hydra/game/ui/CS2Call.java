package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/07/14
 * Time: 17:35
 */
public class CS2Call {
    private int[]    args       = new int[100];
    private String[] stringArgs = new String[100];
    private int      script;
    private int      pointer;

    /**
     * Constructs ...
     *
     *
     * @param scriptID
     */
    public CS2Call(int scriptID) {
        this.script = scriptID;
    }

    /**
     * Method addString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public CS2Call addString(String str) {
        stringArgs[pointer++] = str;

        return this;
    }

    /**
     * Method addInts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param vals
     *
     * @return
     */
    public CS2Call addInts(int... vals) {
        for (Integer i : vals) {
            addInt(i);
        }

        return this;
    }

    /**
     * Method sendInterSetItemsOptionsScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceId
     * @param componentId
     * @param key
     * @param negativeKey
     * @param width
     * @param height
     * @param options
     */
    public void sendInterSetItemsOptionsScript(int interfaceId, int componentId, int key, boolean negativeKey,
            int width, int height, String... options) {
        Object[] parameters = new Object[6 + options.length];
        int      index      = 0;

        for (int count = options.length - 1; count >= 0; count--) {
            parameters[index++] = options[count];
        }

        parameters[index++] = -1;    // dunno but always this
        parameters[index++] = 0;     // dunno but always this, maybe startslot?
        parameters[index++] = height;
        parameters[index++] = width;
        parameters[index++] = key;
        parameters[index++] = interfaceId << 16 | componentId;

        for (int i = 0; i < index; i++) {
            if (parameters[i] instanceof Integer) {
                addInt((Integer) parameters[i]);
            } else {
                addString((String) parameters[i]);
            }
        }
    }

    /**
     * Method addInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param val
     *
     * @return
     */
    public CS2Call addInt(int val) {
        args[pointer++] = val;

        return this;
    }

    private String genString() {
        StringBuilder builder = new StringBuilder();

        for (int current = pointer - 1; current >= 0; current--) {
            if (stringArgs[current] != null) {
                builder.append("s");
            } else {
                builder.append("i");
            }
        }

        return builder.toString();
    }

    /**
     * Method append
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param builder
     */
    public void append(MessageBuilder builder) {
        String types = genString();

        builder.addRS2String(types);

        int idx = 0;

        for (int i = types.length() - 1; i >= 0; i--) {
            if (types.charAt(i) == 's') {
                builder.addRS2String(stringArgs[idx]);
            } else {
                builder.addInt(args[idx]);
            }

            idx++;
        }

        builder.addInt(script);
    }
}
