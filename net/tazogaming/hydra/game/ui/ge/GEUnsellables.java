package net.tazogaming.hydra.game.ui.ge;

import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;

import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/02/2015
 * Time: 15:59
 */
public class GEUnsellables {
    private static List<Integer> badItems = new LinkedList<Integer>();


    public static void load() {
        badItems.clear();
        TextFile file = TextFile.fromPath("config/grandexchange/unsellables.cfg");
        if(!file.exists())
        {
            throw new RuntimeException("Sellables doesnt exist");
        }
        for(Line l : file){
           try {
               badItems.add(Integer.parseInt(l.getData()));
           }catch (Exception ee) {}
        }
    }
    public static boolean isFiltered(int itemid){
        return badItems.contains(itemid);
    }
}
