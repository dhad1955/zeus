package net.tazogaming.hydra.game.ui.ge;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/02/2015
 * Time: 14:46
 */
public class CollectionBox {

    private int itemId = -1;

    public int getCashAmt() {
        return cashAmt;
    }

    public void setCashAmt(int cashAmt) {
        this.cashAmt = cashAmt;
    }

    public int getItemId() {
        return itemId;
    }

    public int getItemAmt() {
        return itemAmt;
    }

    public void setItemAmt(int itemAmt) {
        this.itemAmt = itemAmt;
    }

    private int itemAmt = 0;
    private int cashAmt = 0;

}
