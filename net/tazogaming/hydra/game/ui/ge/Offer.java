package net.tazogaming.hydra.game.ui.ge;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/2015
 * Time: 15:54
 */
public abstract class Offer {
    public static int MYSQL_STATE_ACTIVE    = 0,
                      MYSQL_STATE_ABORTED   = 1,
                      MYSQL_STATE_COMPLETED = 2;

    /** collectionBox made: 15/02/12 */
    private CollectionBox collectionBox = new CollectionBox();

    /** item made: 15/01/23 */
    private int item;

    /** amount made: 15/01/23 */
    private int amount;

    /** pricePerItem made: 15/01/23 */
    private int pricePerItem;

    /** userID made: 15/01/23 */
    private int userID;

    /** currentPot made: 15/01/23 */
    private int currentPot;

    /** state made: 15/01/23 */
    private OfferState state;

    /** remaining made: 15/02/12 */
    private int remaining;

    /** id made: 15/02/12 */
    private int id;

    /**
     * Constructs ...
     *
     *
     * @param state
     */
    public Offer(OfferState state) {}

    /**
     * Constructs ...
     *
     *
     *
     * @param id
     * @param itemID
     * @param amount
     * @param price_per_item
     * @param user_id
     * @param state
     */
    public Offer(int id, int itemID, int amount, int price_per_item, int user_id, OfferState state) {
        this.item         = itemID;
        this.amount       = amount;
        this.pricePerItem = price_per_item;
        this.userID       = user_id;
        this.state        = state;
        this.remaining    = amount;
        this.id           = id;
    }

    private int spentAmount = 0;

    public int getSpentAmount() {
        return spentAmount;
    }

    public void setSpentAmount(int amount){
        this.spentAmount = amount;
    }

    /**
     * Method getCollectionBox
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public CollectionBox getCollectionBox() {
        return collectionBox;
    }

    /**
     * Method getRemaining
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRemaining() {
        return remaining;
    }

    /**
     * Method setRemaining
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param remaining
     */
    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }

    /**
     * Method getId
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Method setId
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method getCurrentPot
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentPot() {
        return currentPot;
    }

    /**
     * Method setCurrentPot
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentPot
     */
    public void setCurrentPot(int currentPot) {
        this.currentPot = currentPot;
    }

    /**
     * Method getState
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public OfferState getState() {
        return state;
    }

    /**
     * Method setState
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param state
     */
    public void setState(OfferState state) {
        this.state = state;
    }

    /**
     * Method getUserID
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getUserID() {
        return userID;
    }

    /**
     * Method setUserID
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userID
     */
    public void setUserID(int userID) {
        this.userID = userID;
    }

    /**
     * Method getAmount
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Method setAmount
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Method getItem
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getItem() {
        return item;
    }

    /**
     * Method setItem
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     */
    public void setItem(int item) {
        this.item = item;
    }

    /**
     * Method getPricePerItem
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPricePerItem() {
        return pricePerItem;
    }

    public void checkEmpty() {
        if(this.getState()== OfferState.ABORTED || this.getState() == OfferState.COMPLETED)
        {
            if(this.getCollectionBox().getCashAmt() == 0 && this.collectionBox.getItemAmt() ==0)
            {
                this.setState(OfferState.FAILED);
                return;
            }
        }
    }

    /**
     * Method withdrawItems
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void withdrawCash(Player player) {
        int total = collectionBox.getCashAmt();

        if (total == 0) {
            return;
        }
        if(player.getInventory().getFreeSlots(995) < 1){
           player.getActionSender().sendMessage("Not enough space");
            return;
        }

        int  invCount = player.getInventory().getStackCount(995);
        long overflow = (long) total + (long) invCount;

        if (overflow > Integer.MAX_VALUE) {
            player.getActionSender().sendMessage("Max cash exceeded");

            return;
        }

        if (player.getInventory().getFreeSlots(995) >= 0) {
            this.collectionBox.setCashAmt(0);
            player.getInventory().addItem(995, total);
            return;
        } else {
            player.getAccount().getGrandExchange().openOfferScreen(
                player.getAccount().getGrandExchange().getCurOfferScreen(), this instanceof BuyOffer);
        }
    }

    /**
     * Method withdrawItems
     * Created on 15/02/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param amount
     * @param notes
     */
    public void withdrawItems(Player player, int amount, boolean notes) {
        if (amount > collectionBox.getItemAmt()) {
            amount = collectionBox.getItemAmt();
        }

        Item toWithdraw = Item.forId(getItem());

        if (toWithdraw.isStackable()) {
            if (player.getInventory().getFreeSlots(toWithdraw.getIndex()) > 0) {
                this.collectionBox.setItemAmt(0);
                player.getInventory().addItem(toWithdraw, amount);
                return;

            }
        } else {
            if (notes && Item.forId(getItem() + 1).isNote()) {
                if (player.getInventory().getFreeSlots(getItem() + 1) > 0) {
                    player.getInventory().addItem(getItem() + 1, amount);
                    collectionBox.setItemAmt(0);
                    return;
                } else {
                    player.getActionSender().sendMessage("Not enough space");

                    return;
                }
            }

            if (amount > player.getInventory().getFreeSlots(toWithdraw.getIndex())) {
                amount = player.getInventory().getFreeSlots(toWithdraw.getIndex());
            }

            if (amount <= 0) {
                player.getActionSender().sendMessage("Not enough space");

                return;
            }

            this.collectionBox.setItemAmt(getCollectionBox().getItemAmt() - amount);
            player.getInventory().addItem(getItem(), amount);

            if (this.collectionBox.getItemAmt() <= 0) {
               return;
            }
        }
    }

    /**
     * Method setPricePerItem
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pricePerItem
     */
    public void setPricePerItem(int pricePerItem) {
        this.pricePerItem = pricePerItem;
    }

    /**
     * Method submit
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param connection
     *
     * @return
     */
    public boolean submit(MysqlConnection connection) {
        if (this.getState() == OfferState.ACTIVE) {
            return true;
        }

        if (this.getState() == OfferState.SUBMITTING) {
            if (!checkAndSubmit()) {
                this.setState(OfferState.FAILED);

                return false;
            }

            if (!doDatabaseEntry(connection)) {
                this.revertSubmission();
                this.setState(OfferState.FAILED);

                return false;
            }

            this.setState(OfferState.ACTIVE);

            return true;
        }

        return false;
    }

    /**
     * Method checkAndSubmit
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract boolean checkAndSubmit();

    protected abstract boolean revertSubmission();

    protected abstract boolean doDatabaseEntry(MysqlConnection connection);

    protected abstract boolean updateOffer(MysqlConnection connection);
}
