package net.tazogaming.hydra.game.ui.ge;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/02/2015
 * Time: 20:27
 */
public class AutoBuy {

    public AutoBuy(int itemID, double amt){
        this.itemID = itemID;
        this.amount = amt;
    }

    private int itemID;
    private double amount;

    public double getPriceModifier() {
        return amount;
    }

    public int getItemID() {
        return itemID;
    }

}
