package net.tazogaming.hydra.game.ui.ge;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/2015
 * Time: 15:58
 */
public class BuyOffer extends Offer{
    private Player player;
    public BuyOffer(int offerid, Player player, int itemid, int amt, int price){
        super(offerid, itemid, amt, price, player.getId(), OfferState.SUBMITTING);
        this.player = player;
    }


    @Override
    public boolean checkAndSubmit() {
        /* Does the player have the cash.*/

        if(!player.getInventory().hasItem(995, (this.getAmount() - getSpentAmount()) * this.getPricePerItem())){
            setState(OfferState.FAILED);
            return false;
        }
        player.getInventory().deleteItem(995, (this.getAmount() - getSpentAmount()) * this.getPricePerItem());
       if(this.getCurrentPot() == 0)
                this.setCurrentPot(this.getAmount() * this.getPricePerItem());
        return true;
    }

    @Override
    public boolean revertSubmission() {
        this.player.getInventory().addItem(995, this.getCurrentPot());
        this.setCurrentPot(0);

        return true;
    }

    @Override
    protected boolean doDatabaseEntry(MysqlConnection connection) {


        try {
            connection.query("INSERT into `ge_buy` (id, itemid, amount, userid, pot, ppi, remaining, dateline, state, ip) VALUES(" + getId() + ", " + getItem() + ", " + getAmount() + ", " + player.getId() + ", " + getCurrentPot() + ", " + getPricePerItem() + ", " + getRemaining() + ", NOW(), " + Offer.MYSQL_STATE_ACTIVE + ", '" + player.getIoSession().getUserIP() + "')");
            GrandExchangeProcessor.getSingleton().requestUpdate(getItem());
            return true;
        }catch (Exception ee){
            ee.printStackTrace();
            return false;
        }
    }

    @Override
    protected boolean updateOffer(MysqlConnection connection) {
        return false;
    }
}
