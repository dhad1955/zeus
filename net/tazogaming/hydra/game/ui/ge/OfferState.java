package net.tazogaming.hydra.game.ui.ge;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/2015
 * Time: 15:52
 */
public enum OfferState {
    SUBMITTING, LOADING, ABORTING, ABORTED, FAILED, ACTIVE, AWAITING_PROCESSING, COMPLETED
}
