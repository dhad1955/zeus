package net.tazogaming.hydra.game.ui.ge;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/02/2015
 * Time: 15:11
 */
public class SellOffer extends Offer {

    /** player made: 15/02/13 */
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param offerid
     * @param itemID
     * @param amount
     * @param ppi
     */
    public SellOffer(Player player, int offerid, int itemID, int amount, int ppi) {
        super(offerid, itemID, amount, ppi, player.getId(), OfferState.SUBMITTING);
        this.player = player;
    }

    /**
     * Method checkAndSubmit
     * Created on 15/01/23
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean checkAndSubmit() {
        if (player.getInventory().hasItem(getItem(), getAmount() - getSpentAmount())) {
            player.getInventory().deleteItem(getItem(), getAmount() - getSpentAmount());

            return true;
        }

        if (Item.forId(getItem() + 1).isNote() && player.getInventory().hasItem(getItem() + 1, getAmount() - getSpentAmount())) {
            player.getInventory().deleteItem(getItem() + 1, getAmount() - getSpentAmount());

            return true;
        }

        setState(OfferState.FAILED);

        return false;
    }

    @Override
    protected boolean revertSubmission() {
        player.getInventory().addItem(getItem(), getAmount());

        return true;
    }


    protected void doUpdate(MysqlConnection connection) throws SQLException {
        ResultSet resultSet = connection.query("SELECT * FROM ge_buy where itemid=" + getItem() + " and ppi >= " + getPricePerItem() + " order by ppi desc");
        if(!resultSet.next()){
            doDatabaseEntry(connection);
            return;
        } else {
          int buyer_remaining_to_buy = resultSet.getInt("remaining");
          int remaining_to_sell = getAmount();

          int total_to_buy = 0;

        }



    }


    @Override
    protected boolean doDatabaseEntry(MysqlConnection connection) {
        try {
            connection.query(
                    "INSERT into ge_sell (id, itemid, amount, pot, userid, dateline, ppi, remaining, state, ip) VALUES("
                            + getId() + ", " + getItem() + ", " + getAmount() + ", 0, " + player.getId() + ", NOW(), "
                            + getPricePerItem() + ", " + getRemaining() + ", " + Offer.MYSQL_STATE_ACTIVE + ", '" + player.getIoSession().getUserIP() + "')");
            GrandExchangeProcessor.getSingleton().requestUpdate(getItem());
        } catch (Exception ee) {
            ee.printStackTrace();

            return false;
        }

        return true;
    }

    @Override
    protected boolean updateOffer(MysqlConnection connection) {
        return false;
    }
}
