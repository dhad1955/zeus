package net.tazogaming.hydra.game.ui.ge.easyeco;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/05/2015
 * Time: 05:45
 */
public class EasyEcoItem {
    public static final byte
        SELL = 0,
        BUY  = 1;

    /** buyRecords made: 15/05/31 **/
    private Map<Long, EasyEcoRecord> buyRecords = new HashMap<Long, EasyEcoRecord>();

    /** itemID made: 15/05/31 **/
    private int itemID;

    /** maxAmountPerDay made: 15/05/31 **/
    private int maxAmountPerDay;

    /** type made: 15/05/31 **/
    private byte type;

    /**
     * Constructs ...
     *
     *
     * @param itemID
     */
    public EasyEcoItem(int itemID, byte type, int maxPerDay) {
        this.itemID = itemID;
        this.type = type;
        this.maxAmountPerDay = maxPerDay;
    }

    /**
     * Method getItemID
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getItemID() {
        return itemID;
    }

    /**
     * Method getMaximum
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     *
     * @return
     */
    public int getMaximum(long uid) {
        if (this.maxAmountPerDay == 0) {
            return Integer.MAX_VALUE;
        }

        if (this.buyRecords.containsKey(uid)) {
            EasyEcoRecord record    = buyRecords.get(uid);
            int       remaining = record.popCount();

            if (remaining > this.maxAmountPerDay) {
                return 0;
            }

            return this.maxAmountPerDay - remaining;
        }

        return this.maxAmountPerDay;
    }

    /**
     * Method register
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     * @param amount
     */
    public void register(long uid, int amount) {
        EasyEcoRecord rec = null;
        if(this.buyRecords.containsKey(uid))
            rec = buyRecords.get(uid);
        else
        {
            rec = new EasyEcoRecord();
            buyRecords.put(uid, rec);

        }

        rec.add(amount);
    }

    /**
     * Method setItemID
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemID
     */
    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    /**
     * Method getMaxAmountPerDay
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaxAmountPerDay() {
        return maxAmountPerDay;
    }

    /**
     * Method setMaxAmountPerDay
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param maxAmountPerDay
     */
    public void setMaxAmountPerDay(int maxAmountPerDay) {
        this.maxAmountPerDay = maxAmountPerDay;
    }

    /**
     * Method getType
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte getType() {
        return type;
    }
}
