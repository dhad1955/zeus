package net.tazogaming.hydra.game.ui.ge.easyeco;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.runtime.Core;

//~--- JDK imports ------------------------------------------------------------

import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/05/2015
 * Time: 05:46
 */
public class EasyEcoRecord {


    public static boolean shouldWipe(Player p, int itemid){
        Item ix = Item.forId(itemid);

        if(p.getRights() >= Player.PATRON) return false;

        if(ix.isTradeable() || ix.isDungItem())
            return true;
        return false;
    }

    /** buys made: 15/05/31 **/
    private List<EasyEcoTimeRecord> buys = new LinkedList<EasyEcoTimeRecord>();

    /**
     * Method popCount
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int popCount() {
        int                c        = 0;
        List<EasyEcoTimeRecord> toRemove = new LinkedList<EasyEcoTimeRecord>();

        for (EasyEcoTimeRecord b : buys) {
            if (Core.currentTimeMillis() - b.time > 86400000) {
                toRemove.add(b);
            } else {
                c+= b.amountBought;
            }
        }

        this.buys.removeAll(toRemove);

        return c;
    }

    public void add(int amount) {
        this.buys.add(new EasyEcoTimeRecord(amount));
    }
}


/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 15/05/31
 * @author         Daniel Hadland
 */
class EasyEcoTimeRecord {
    EasyEcoTimeRecord(int amountBought) {
        this.amountBought = amountBought;
        this.time = Core.currentTimeMillis();
    }
    protected long time;
    protected int  amountBought;
}
