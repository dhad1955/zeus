package net.tazogaming.hydra.game.ui.ge.concurrent;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.PlayerGrandExchange;
import net.tazogaming.hydra.game.ui.ge.*;
import net.tazogaming.hydra.game.ui.ge.concurrent.impl.GrandExchangeCommand;
import net.tazogaming.hydra.game.ui.ge.easyeco.EasyEcoItem;
import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/2015
 * Time: 16:04
 */
public class GrandExchangeProcessor implements Runnable {

    /** easyBuys made: 15/05/31 **/
    private static Map<Integer, EasyEcoItem> easyBuys = new HashMap<Integer, EasyEcoItem>();

    /** easySells made: 15/05/31 **/
    private static Map<Integer, EasyEcoItem> easySells = new HashMap<Integer, EasyEcoItem>();

    /** processor made: 15/02/12 */
    private static GrandExchangeProcessor processor = new GrandExchangeProcessor();

    /** inCommand made: 15/01/23 */
    private final List<GrandExchangeCommand> inCommand = new LinkedList<GrandExchangeCommand>();

    /** commandsToProccess made: 15/01/23 */
    private final List<GrandExchangeCommand> commandsToProccess = new LinkedList<GrandExchangeCommand>();
    int                                      currentCycle       = 0;

    /** connection made: 15/01/23 */
    private MysqlConnection connection = null;

    /** itemsToBuy made: 15/02/15 */
    private List<AutoBuy> itemsToBuy = new ArrayList<AutoBuy>();

    /** buy_pot made: 15/02/15 */
    private long buy_pot = 0;

    /** taxRate made: 15/02/16 */
    private double taxRate = 1.00;

    /** cashSinkRate made: 15/02/16 */
    private double cashSinkRate = 0.00;

    /** processSpeed made: 15/02/16 */
    private int processSpeed = 1000;

    /** itemList made: 15/05/31 **/
    private LinkedList<Integer> itemList = new LinkedList<Integer>();

    /**
     * Method doAutoBuy
     * Created on 15/02/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private boolean spillPot            = false;
    boolean         forceUpdateRequired = false;

    /** logFile made: 15/02/16 */
    private PrintStream logFile;

    /** logFilePath made: 15/02/16 */
    private String logFilePath;

    /**
     * Method getEasySells
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Map<Integer, EasyEcoItem> getEasySells() {
        return easySells;
    }

    /**
     * Method setEasySells
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param easySells
     */
    public static void setEasySells(Map<Integer, EasyEcoItem> easySells) {
        GrandExchangeProcessor.easySells = easySells;
    }

    /**
     * Method getEasyBuys
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Map<Integer, EasyEcoItem> getEasyBuys() {
        return easyBuys;
    }

    /**
     * Method setEasyBuys
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param easyBuys
     */
    public static void setEasyBuys(Map<Integer, EasyEcoItem> easyBuys) {
        GrandExchangeProcessor.easyBuys = easyBuys;
    }

    /**
     * Method requestUpdate
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemID
     */
    public void requestUpdate(int itemID) {
        itemList.add(itemID);
    }

    /**
     * Method getPot
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getPot() {
        return buy_pot;
    }

    private String pollUpdateQueue() {
        StringBuilder builder = new StringBuilder();

        for (Integer i : itemList) {
            builder.append(Integer.toString(i) + ",");
        }

        if (builder.length() > 1) {
            itemList.clear();

            return builder.toString().substring(0, builder.length() - 1);
        }

        return null;
    }

    /**
     * Method getSingleton
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    private final void log(String line) {
        try {
            logFile = new PrintStream(new FileOutputStream(logFilePath, true));

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
            Calendar   cal        = Calendar.getInstance();

            logFile.println(dateFormat.format(cal.getTime()) + " > " + line);
            logFile.flush();
            logFile.close();
        } catch (Exception ee) {}
    }

    /**
     * Method getSingleton
     * Created on 15/02/16
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static GrandExchangeProcessor getSingleton() {
        return processor;
    }

    /**
     * Method initialize
     * Created on 15/02/16
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @return
     */
    public double getTaxRate() {
        return taxRate;
    }

    /**
     * Method initialize
     * Created on 15/02/16
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @throws RuntimeException
     */
    public static void initialize() throws RuntimeException {
        try {
            Properties properties = new Properties();

            properties.load(new FileReader("config/grandexchange/grandexchange.cfg"));
            getSingleton().logFilePath  = properties.getProperty("autobuy_log_file");
            getSingleton().processSpeed = Integer.parseInt(properties.getProperty("process_time"));

            String buyTaxRate = properties.getProperty("buy_tax_rate");

            if (!buyTaxRate.endsWith("%")) {
                System.err.println("[GrandExchange] Unable to load grand exchange buytaxrate is not a percentage "
                                   + buyTaxRate);
                System.exit(-1);
            }

            int actual = -1;

            try {
                actual = Integer.parseInt(buyTaxRate.substring(0, buyTaxRate.length() - 1));
            } catch (NumberFormatException ee) {
                System.err.println("[GrandExchange] Error " + buyTaxRate.substring(buyTaxRate.length() - 1)
                                   + " is not a valid number for buy tax rate");
                System.exit(-1);
            }

            getSingleton().taxRate = (double) (1 + ((double) actual / 100d));

            String cashSinkRate = properties.getProperty("cash_sink_rate");

            if (!cashSinkRate.endsWith("%")) {
                System.err.println("[GrandExchange] cash_sink_rate is not a percentage");
                System.exit(-1);
            }

            try {
                actual = Integer.parseInt(cashSinkRate.substring(0, cashSinkRate.length() - 1));
            } catch (NumberFormatException ee) {
                System.err.println("Invalid cash sink rate");
                System.exit(-1);
            }

            getSingleton().cashSinkRate = (double) (((double) actual / 100d));

            try {
                getSingleton().connection = new MysqlConnection(properties.getProperty("db_server"),
                        properties.getProperty("db_user"), properties.getProperty("db_password"),
                        properties.getProperty("db_name")).connect();
            } catch (Exception ee) {
                ee.printStackTrace();
                System.err.println("[GrandExchange] Mysql database failed to load shutting down");
                System.exit(-1);
            }
        } catch (IOException ee) {
            System.err.println("Failed to load grand exchange");
            ee.printStackTrace();
            System.exit(-1);
        }
    }

    /**
     * Method submitCommand
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param command
     */
    public void submitCommand(GrandExchangeCommand command) {
        synchronized (inCommand) {
            inCommand.add(command);
        }
    }

    /**
     * Method loadAutoBuys
     * Created on 15/02/16
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */

    public void loadEasyEco() {
        getEasyBuys().clear();
        getEasySells().clear();
        TextFile file = TextFile.fromPath("config/grandexchange/easy_eco.cfg");
        for(Line l : file)
        {
            if(l.getData().startsWith("//") || l.getData().length() == 0)
                continue;
            if(l.getData().startsWith("eco"))
            {
                String[] split = l.getData().split(" ");
                int itemid = Integer.parseInt(split[2]);
                int maxBuys = Integer.parseInt(split[3]);
                int maxSells = Integer.parseInt(split[4]);
                if(maxBuys >= 0){
                    getEasySells().put(itemid, new EasyEcoItem(itemid, (byte)0, maxBuys));
                }
                if(maxSells >= 0)
                    getEasyBuys().put(itemid, new EasyEcoItem(itemid, (byte)0, maxSells));

            }
        }
    }
    public void loadAutoBuys() {
        loadEasyEco();
        this.itemsToBuy.clear();;

        TextFile file = TextFile.fromPath("config/grandexchange/auto_buy.cfg");

        for (Line l : file) {
            try {
                if (l.getData().startsWith("buy")) {
                    String[] split = l.getData().split(" ");
                    int      buy   = Integer.parseInt(split[2]);
                    double   price = Double.parseDouble(split[3]);

                    this.itemsToBuy.add(new AutoBuy(buy, price));
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }
    }

    /**
     * Method testBuyPot
     * Created on 15/02/16
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void testBuyPot() {
        this.buy_pot = Integer.MAX_VALUE;
    }

    /**
     * Method spillPot
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void spillPot() {
        spillPot = true;
    }

    /**
     * Method doAutoBuy
     * Created on 15/05/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void doAutoBuy() {
        if ((this.buy_pot >= Integer.MAX_VALUE) || spillPot) {
            AutoBuy itemToBuy = itemsToBuy.get(GameMath.rand3(itemsToBuy.size()));
            int     price     = (int) ((Item.forId(itemToBuy.getItemID()).getExchangePrice() * taxRate));

            if (price < 0) {
                if (spillPot) {
                    return;
                }

                price = Integer.MAX_VALUE;
            }

            spillPot = false;

            if (price == 0) {
                System.err.println("invalid price for:" + itemToBuy + "");

                return;
            }

            int toSpend    = Integer.MAX_VALUE;
            int totalToBuy = (int) (long) ((long) toSpend / (long) price);

            if (totalToBuy <= 0) {
                return;
            }

            long totalPrice = ((long) price * (long) totalToBuy);

            buy_pot -= toSpend;

            try {
                int buyid = PlayerGrandExchange.nextBuyID();

                connection.query(
                    "INSERT into ge_buy(id, itemid, amount, ppi, pot, userid, dateline, remaining, state, collection) values("
                    + buyid + ", " + itemToBuy.getItemID() + ", " + totalToBuy + ", " + price + ", " + totalPrice
                    + ", 0, now(), " + totalToBuy + ", " + Offer.MYSQL_STATE_ACTIVE + ", 0)");
                this.requestUpdate(itemToBuy.getItemID());
                log("[EcoBot] Inserted buy offer for " + Item.forId(itemToBuy.getItemID()).getName() + " x "
                    + totalToBuy + " for " + price + " each for a total of " + totalPrice);
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }
    }

    /**
     * Method getDatabaseLink
     * Created on 15/02/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public MysqlConnection getDatabaseLink() {
        return this.connection;
    }

    /**
     * Method run
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void run() {
        while (true) {
            currentCycle++;

            try {
                doAutoBuy();
            } catch (Exception ee) {
                ee.printStackTrace();
            }

            synchronized (inCommand) {
                commandsToProccess.addAll(inCommand);
                inCommand.clear();
            }

            for (GrandExchangeCommand nextCommand : commandsToProccess) {
                try {
                    nextCommand.doCommand();
                } catch (Exception ee) {
                    System.err.println("Warning GE Command failed: " + nextCommand);
                    ee.printStackTrace();
                }
            }

            commandsToProccess.clear();

            try {
                matchOffers();
            } catch (SQLException database_error) {
                database_error.printStackTrace();
            }

            List<Player> playersToProcess = new LinkedList<Player>();

            synchronized (World.getWorld().getPlayers()) {
                playersToProcess.addAll(World.getWorld().getPlayers());
            }

            try {
                PreparedStatement selectBuyStatement =
                    connection.prepareStatement(
                        "SELECT pot, collection, state, remaining, ppi, amount FROM ge_buy where id=?");
                PreparedStatement selectSellStatement =
                    connection.prepareStatement(
                        "SELECT remaining, pot, state, amount, collection,ppi FROM ge_sell where id= ?");

                for (Player player : playersToProcess) {
                    PlayerGrandExchange exch = player.getAccount().getGrandExchange();

                    for (Offer offer : exch.getOffers()) {
                        if ((offer != null) && (offer.getState() == OfferState.ACTIVE)) {
                            if (offer instanceof BuyOffer) {
                                try {
                                    selectBuyStatement.setInt(1, offer.getId());

                                    ResultSet res = selectBuyStatement.executeQuery();

                                    if (res.next()) {
                                        offer.setRemaining(res.getInt("remaining"));
                                        offer.setCurrentPot((res.getInt("amount") - res.getInt("remaining"))
                                                            * res.getInt("ppi"));

                                        if (res.getInt("collection") > 0) {
                                            int col = res.getInt("collection");

                                            connection.query("UPDATE ge_buy set collection=0 where id=" + offer.getId()
                                                             + "");
                                            offer.getCollectionBox().setItemAmt(offer.getCollectionBox().getItemAmt()
                                                    + col);
                                        }

                                        if (res.getInt("state") == Offer.MYSQL_STATE_COMPLETED) {
                                            offer.setState(OfferState.COMPLETED);
                                            player.getActionSender().sendMessage(
                                                Text.LIGHT_ORANGE("Your grand exchange offers have been bought"));
                                            connection.query("DELETE from ge_buy where id = " + offer.getId());
                                        }
                                    }

                                    res.close();
                                } catch (SQLException ee) {
                                    ee.printStackTrace();
                                }
                            } else if (offer instanceof SellOffer) {
                                selectSellStatement.setInt(1, offer.getId());

                                ResultSet res = selectSellStatement.executeQuery();

                                try {
                                    if (res.next()) {
                                        offer.setRemaining(res.getInt("remaining"));

                                        int pot = (res.getInt("amount") - res.getInt("remaining")) * res.getInt("ppi");

                                        offer.setCurrentPot(pot);

                                        int collection = res.getInt("collection");

                                        if (collection > 0) {
                                            connection.query("UPDATE ge_sell set collection = 0 where id="
                                                             + offer.getId());
                                            offer.getCollectionBox().setCashAmt(offer.getCollectionBox().getCashAmt()
                                                    + collection);
                                        }

                                        if (res.getInt("state") == Offer.MYSQL_STATE_COMPLETED) {
                                            offer.setState(OfferState.COMPLETED);
                                            player.getActionSender().sendMessage(
                                                Text.LIGHT_ORANGE("Your grand exchange offers have sold"));

                                            // clean up tables
                                            connection.query("DELETE from ge_sell WHERE id = " + offer.getId());
                                        }

                                        if (res.getInt("state") == Offer.MYSQL_STATE_ABORTED) {
                                            connection.query("DELETE from ge_sell where id = " + offer.getId());
                                        }
                                    }

                                    res.close();
                                } catch (SQLException ee) {
                                    ee.printStackTrace();
                                }
                            }
                        }
                    }
                }

                selectBuyStatement.close();
                selectSellStatement.close();
            } catch (Exception ee) {
                ee.printStackTrace();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ignored) {}
        }
    }

    /**
     * Method matchOffers
     * Created on 15/01/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @throws SQLException
     */
    public void matchOffers() throws SQLException {
        if ((itemList.size() == 0) &&!forceUpdateRequired) {
            return;    // no updates needed
        }

        String q = null;

        if (!forceUpdateRequired) {
            q = pollUpdateQueue();
        }

        ResultSet resultSet;
        ResultSet matchResultSet;

        if (forceUpdateRequired) {
            resultSet = connection.query("SELECT itemid,remaining,ppi,pot,id,ip FROM ge_sell WHERE state="
                                         + net.tazogaming.hydra.game.ui.ge.Offer.MYSQL_STATE_ACTIVE + "");
        } else {
            resultSet = connection.query("SELECT itemid,remaining,ppi,pot,id,ip FROM ge_sell WHERE state="
                                         + net.tazogaming.hydra.game.ui.ge.Offer.MYSQL_STATE_ACTIVE + " AND itemid IN("
                                         + q + ")");
        }

        int tot = 0;

        while (resultSet.next()) {
            tot++;

            int    itemID            = resultSet.getInt("itemid");
            int    remaining_to_sell = resultSet.getInt("remaining");
            int    price             = resultSet.getInt("ppi");
            int    pot               = resultSet.getInt("pot");
            int    sell_id           = resultSet.getInt("id");
            String ip                = resultSet.getString("ip");
            int    totalSold         = 0;

            try {
                Thread.sleep(1);
            } catch (Exception ee) {}

            matchResultSet = connection.query("SELECT remaining,pot,id FROM ge_buy WHERE state="
                                              + net.tazogaming.hydra.game.ui.ge.Offer.MYSQL_STATE_ACTIVE
                                              + " AND itemid=" + itemID + " and ppi >= " + price + " AND ip != '" + ip
                                              + "' order by ppi desc");

            while (matchResultSet.next()) {
                if (remaining_to_sell == 0) {
                    matchResultSet.close();

                    break;
                }

                int remaining_to_buy = matchResultSet.getInt("remaining");
                int buying_pot       = matchResultSet.getInt("pot");
                int buy_offer_id     = matchResultSet.getInt("id");
                int total_to_buy     = remaining_to_buy;

                if (remaining_to_sell < remaining_to_buy) {
                    total_to_buy = remaining_to_sell;
                }

                int sellersFee = total_to_buy * price;

                if (buying_pot < sellersFee) {
                    continue;
                }

                totalSold         += sellersFee;
                buying_pot        -= sellersFee;
                pot               += sellersFee;
                remaining_to_buy  -= total_to_buy;
                remaining_to_sell -= total_to_buy;

                int curStatus = 0;

                if (remaining_to_buy == 0) {
                    curStatus = net.tazogaming.hydra.game.ui.ge.Offer.MYSQL_STATE_COMPLETED;

                    int taken = (int) (buying_pot * (double) ((double) 1 - (double) cashSinkRate));

                    this.buy_pot += taken;
                }

                PreparedStatement stm =
                    connection.prepareStatement(
                        "UPDATE ge_buy SET remaining = ?, pot = ?, state = ?, collection = collection + "
                        + total_to_buy + " WHERE id = ?");

                stm.setInt(1, remaining_to_buy);
                stm.setInt(2, buying_pot);
                stm.setInt(3, curStatus);
                stm.setInt(4, buy_offer_id);
                stm.executeUpdate();
                stm.close();
            }

            matchResultSet.close();

            int curStatus = 0;

            if (remaining_to_sell <= 0) {
                curStatus = net.tazogaming.hydra.game.ui.ge.Offer.MYSQL_STATE_COMPLETED;
            }

            PreparedStatement stm =
                connection.prepareStatement(
                    "UPDATE ge_sell set  pot = ?, remaining = ?, state = ?, collection = collection + " + totalSold
                    + " WHERE id = ?");

            stm.setInt(1, pot);
            stm.setInt(2, remaining_to_sell);
            stm.setInt(3, curStatus);
            stm.setInt(4, sell_id);
            stm.executeUpdate();
            stm.close();
        }

        resultSet.close();
    }
}
