package net.tazogaming.hydra.game.ui.ge.concurrent.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.ui.ge.Offer;
import net.tazogaming.hydra.game.ui.ge.OfferState;
import net.tazogaming.hydra.game.ui.ge.SellOffer;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/02/2015
 * Time: 14:18
 */
public class AbortSellOfferCommand implements GrandExchangeCommand {

    /** offer made: 15/02/13 **/
    private SellOffer offer;

    /**
     * Constructs ...
     *
     *
     * @param offer
     */
    public AbortSellOfferCommand(SellOffer offer) {
        this.offer = offer;
    }

    /**
     * Method doCommand
     * Created on 15/02/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void doCommand() {
        if (this.offer.getState() == OfferState.ABORTING) {
            try {
                MysqlConnection connection = GrandExchangeProcessor.getSingleton().getDatabaseLink();
                ResultSet       res        = connection.query("SELECT * FROM ge_sell WHERE state = "
                        + Offer.MYSQL_STATE_ACTIVE + " and id = " + offer.getId());

                if (res.next()) {
                    connection.query("UPDATE ge_sell set state = " + Offer.MYSQL_STATE_ABORTED
                            + ", collection = 0 WHERE id = " + offer.getId());

                    int coinsTotal    = res.getInt("collection");
                    int itemsInBucket = res.getInt("remaining");

                    offer.getCollectionBox().setCashAmt(offer.getCollectionBox().getCashAmt() + coinsTotal);
                    offer.getCollectionBox().setItemAmt(offer.getCollectionBox().getItemAmt() + itemsInBucket);
                    offer.setState(OfferState.ABORTED);
                    res.close();
                }   else{
                    System.err.println("invalid state?? "+offer.getState());
                    offer.setState(OfferState.ACTIVE);
                }
                res.close();
            } catch (SQLException error1) {
                error1.printStackTrace();
            }
        }
    }
}
