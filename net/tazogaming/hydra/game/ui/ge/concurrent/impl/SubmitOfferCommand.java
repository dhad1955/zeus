package net.tazogaming.hydra.game.ui.ge.concurrent.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.ge.Offer;
import net.tazogaming.hydra.game.ui.ge.OfferState;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/2015
 * Time: 16:06
 */
public class SubmitOfferCommand implements GrandExchangeCommand {

    /** player made: 15/02/19 **/
    private Player player;

    /** offer made: 15/02/19 **/
    private Offer offer;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param offer
     */
    public SubmitOfferCommand(Player player, Offer offer) {
        this.player = player;
        this.offer  = offer;
    }

    /**
     * Method doCommand
     * Created on 15/02/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void doCommand() {
        if (this.offer.getState() == OfferState.SUBMITTING) {
            offer.submit(GrandExchangeProcessor.getSingleton().getDatabaseLink());
            if (offer.getState() == OfferState.ACTIVE) {
                player.getAccount().setSaveRequested(true);
            }
        }
    }
}
