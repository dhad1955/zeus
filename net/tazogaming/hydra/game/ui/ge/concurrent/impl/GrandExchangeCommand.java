package net.tazogaming.hydra.game.ui.ge.concurrent.impl;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/2015
 * Time: 16:05
 */
public interface GrandExchangeCommand {

    public void doCommand();
}
