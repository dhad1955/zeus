package net.tazogaming.hydra.game.ui.ge.concurrent.impl;

import net.tazogaming.hydra.game.ui.ge.BuyOffer;
import net.tazogaming.hydra.game.ui.ge.Offer;
import net.tazogaming.hydra.game.ui.ge.OfferState;
import net.tazogaming.hydra.game.ui.ge.concurrent.GrandExchangeProcessor;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

import java.sql.ResultSet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 05/02/2015
 * Time: 13:36
 */
public class AbortBuyOfferCommand implements GrandExchangeCommand{

    private Offer e;
    public AbortBuyOfferCommand(BuyOffer offer){
        this.e = offer;
    }

    @Override
    public void doCommand() {
        try {
            if (e.getState() == OfferState.ABORTING) {
                MysqlConnection con = GrandExchangeProcessor.getSingleton().getDatabaseLink();
                ResultSet res = con.query("SELECT * FROM ge_buy WHERE id=" + e.getId() + " AND state = " + Offer.MYSQL_STATE_ACTIVE + "");
                if (!res.next()) {
                    System.err.println("unable to find offer with that state "+e.getId());
                    e.setState(OfferState.ACTIVE);
                } else {
                    con.query("UPDATE ge_buy SET state=" + Offer.MYSQL_STATE_ABORTED + " WHERE id=" + e.getId() + "");
                    e.setState(OfferState.ABORTED);
                    int amt = res.getInt("amount");
                    int remaining = res.getInt("remaining");
                    int totalBought = amt - remaining;
                    e.getCollectionBox().setItemAmt(e.getCollectionBox().getItemAmt() + res.getInt("collection"));
                    e.getCollectionBox().setCashAmt(res.getInt("pot"));
                }
                res.close();
            }
            }catch(Exception ee){
                     ee.printStackTrace();
            }
        }
    }
