package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 20:09
 */
import net.tazogaming.hydra.entity3d.item.Item;

/**
 * Created with IntelliJ IDEA.
 * User: Danny
 * Date: 31/01/13
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 */
public class BankTab {
    public int    interfaceId    = 0;
    public Item[] tabItems       = new Item[450];
    public int[]  tabItemsAmount = new int[450];
    int           caret          = 0;

    /**
     * Method getAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAmount() {
        return caret;
    }

    /**
     * Method sort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sort() {
        Item[] items2   = new Item[450];
        int[]  itemsam2 = new int[450];
        int    off      = 0;

        this.caret = 0;

        for (int i = 0; i < tabItems.length; i++) {
            if (tabItems[i] != null) {
                items2[off]   = tabItems[i];
                itemsam2[off] = tabItemsAmount[i];
                off++;
                caret++;
            }
        }

        tabItems       = items2;
        tabItemsAmount = itemsam2;
    }

    /**
     * Method moveItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     */
    public void moveItems(int from, int to) {

        // System.out.println(from+" "+to);
        Item tempItem   = tabItems[from];
        int  tempAmount = tabItemsAmount[from];

        this.tabItems[from]       = this.tabItems[to];
        this.tabItemsAmount[from] = this.tabItemsAmount[to];
        this.tabItems[to]         = tempItem;
        this.tabItemsAmount[to]   = tempAmount;
        sort();
    }

    /**
     * Method itemExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public boolean itemExists(int item) {
        for (Item i : tabItems) {
            if ((i != null) && (i.getIndex() == item)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getFirstItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Item getFirstItem() {
        return tabItems[0];
    }

    /**
     * Method getItemSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public int getItemSlot(int item) {
        for (int i = 0; i < tabItems.length; i++) {
            if ((tabItems[i] != null) && (tabItems[i].getIndex() == item)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getAvailableSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAvailableSlot() {
        for (int i = 0; i < tabItems.length; i++) {
            if (tabItems[i] == null) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method insertItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amount
     */
    public void insertItem(int itemid, int amount) {
        if (itemExists(itemid)) {
            int slot = getItemSlot(itemid);

            if (itemid == 995) {
                long l = (long) ((long) (tabItemsAmount[slot]) + (long) amount);

                if (l > Integer.MAX_VALUE) {
                    tabItemsAmount[slot] = Integer.MAX_VALUE;

                    return;
                }
            }

            tabItemsAmount[slot] += amount;
        } else {
            int slot = getAvailableSlot();

            if (slot != -1) {
                tabItems[slot]       = Item.forId(itemid);
                tabItemsAmount[slot] = amount;
                caret++;
            }
        }
    }

    /**
     * Method insertAfter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param itemId
     * @param amount
     */
    public void insertAfter(int slot, int itemId, int amount) {
        Item[] tmp        = new Item[tabItems.length];
        int[]  tmpAmounts = new int[tabItems.length];
        int    caret      = 0;

        for (int i = 0; i < tmp.length; i++) {
            if (caret == tabItems.length) {
                break;
            }

            if (i == (slot)) {
                tmp[caret]          = Item.forId(itemId);
                tmpAmounts[caret++] = amount;
                tmp[caret]          = tabItems[i];
                tmpAmounts[caret++] = tabItemsAmount[i];
            } else {
                tmp[caret]          = tabItems[i];
                tmpAmounts[caret++] = tabItemsAmount[i];
            }
        }

        this.tabItems       = tmp;
        this.tabItemsAmount = tmpAmounts;
        sort();
    }

    /**
     * Method deleteItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     */
    public void deleteItem(int slot) {
        tabItems[slot]       = null;
        tabItemsAmount[slot] = 0;
        sort();
    }

    /**
     * Method withdrawItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param amount
     *
     * @return
     */
    public boolean withdrawItem(int slot, int amount) {
        if ((slot < 0) || (slot > tabItems.length)) {
            return false;
        }

        if (tabItems[slot] != null) {
            tabItemsAmount[slot] -= amount;

            if (tabItemsAmount[slot] <= 0) {
                tabItemsAmount[slot] = 0;

                tabItems[slot]       = null;

                sort();

                return true;
            }
        }

        return true;
    }
}
