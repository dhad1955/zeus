package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/11/13
 * Time: 00:16
 */
public class PlayerContextMenu {
    private String[]  actions      = { null, null, "Follow", "Trade with" };
    private boolean[] place_on_top = { false, false, false, false };
    private Player    player;

    /**
     * Constructs ...
     *
     *
     * @param p
     */
    public PlayerContextMenu(Player p) {
        player = p;
    }

    /**
     * Method sendPlayerCommand
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param onTop
     * @param name
     */
    public void sendPlayerCommand(int i, boolean onTop, String name) {
        actions[i]      = name;
        place_on_top[i] = onTop;
        player.getActionSender().sendPlayerCommand(name, onTop, i);
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void render() {
        for (int i = 0; i < actions.length; i++) {
            if (actions[i] != null) {
                player.getActionSender().sendPlayerCommand(actions[i], place_on_top[i], i);
            }
        }
    }
}
