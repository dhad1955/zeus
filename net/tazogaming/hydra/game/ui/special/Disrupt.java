package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/10/13
 * Time: 11:05
 */
public class Disrupt implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 6;
        
        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);
        pla.setSpecialMultiplier(2.1);

        double ran = GameMath.rand((GameMath.getMaxHit(pla) * 1.45));

        if (fightingWith instanceof Player) {
            if (fightingWith.getPlayer().getPrayerBook().curseActive(PrayerBook.DEFLECT_MAGIC)
                    || ((Player) fightingWith).getPrayerBook().curseActive(PrayerBook.PROTECT_FROM_MAGIC)) {
                ran /= 2;
            }
        }

        if(ran < (GameMath.getMaxHit(pla) * 0.30))
            ran = (GameMath.getMaxHit(pla) * 0.30);


        double dmg = dmg = fightingWith.hit(pla, ran, Damage.TYPE_MAGIC, 1, true);

        PlayerCombatAdapter.addCombatXP((int)dmg, -1, pla);
        pla.getAnimation().animate(14788, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(1729, Graphic.LOW);
        fightingWith.getGraphic().set(1730, 1);

        for (Killable k : pla.getViewArea().getTargetsInRange(6)) {
            if (k instanceof Player) {
                if (!Combat.pvp_check(pla, k.getPlayer())) {
                    continue;
                }
            }

            if (!k.isInMultiArea()) {
                continue;
            }

            if (k == fightingWith) {
                continue;
            }

            if (!k.canBeAttacked(pla)) {
                continue;
            }

            if (k == pla) {
                continue;
            }

            if ((k instanceof NPC) &&!k.getNpc().getDefinition().isAttackable()) {
                continue;
            }

            if (k.getCurrentInstance() != pla.getCurrentInstance()) {
                continue;
            }

            pla.setSpecialMultiplier(5.10);

            double test = Combat.applyMeleeDamage2(1.41, Damage.TYPE_HIT_MAGIC, pla, k);

            pla.setSpecialMultiplier(1.0);
            PlayerCombatAdapter.addCombatXP((int)test, -1, pla);
            k.getGraphic().set(1730, Graphic.LOW);
        }

        pla.setSpecialMultiplier(1.0);

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
