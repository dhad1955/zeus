package net.tazogaming.hydra.game.ui.special;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.player.PlayerFollowing;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/2014
 * Time: 08:12
 */
public class DragonSpear implements SpecialAttackHandler<Player> {
    /**
     * Method doSpecial
     * Created on 14/08/18
     *
     * @param player
     * @param fightingWith
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean doSpecial(Player player, Killable fightingWith) {


        if(player.getCurrentSpecial() < 2.5) {
            player.getActionSender().sendMessage("You don't have enough special energy");
            return false;
        }
        fightingWith.registerHit(player);
        player.getGraphic().set(253, 0);
        player.getAnimation().animate(1064, 0);
        SpecialAttacks.removeSpecial(player, 2.5);
        fightingWith.stun(6);
        fightingWith.getGraphic().set(254, 0);

        if(fightingWith instanceof Player){
            int direction = Movement.traverseDirection(fightingWith.getLocation(), player.getLocation());
            Point newLoc = Movement.getPointForDir(fightingWith.getLocation(), direction);

            if(PlayerFollowing.getMovementStatus(direction, fightingWith.getX(), fightingWith.getY(), fightingWith.getHeight(), null) != 0)
                     ((Player) fightingWith).teleport(newLoc.getX(), newLoc.getY(), newLoc.getHeight());

        }
        return false;
    }
}
