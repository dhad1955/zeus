package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/10/13
 * Time: 10:48
 */
public class EnergyDrain implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 5;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);
        pla.getAnimation().animate(11971, Animation.HIGH_PRIORITY);
        pla.getCombatAdapter().getFightingWith().getGraphic().set(2108, Graphic.HIGH);

        pla.getCombatAdapter().getFightingWith().hit(pla, GameMath.rand3(20), Damage.TYPE_MELEE_SLASH, 1);
        if (pla.getCombatAdapter().getFightingWith() instanceof Player) {
            pla.getCombatAdapter().getFightingWith().getPlayer().removeEnergy(100);
        }

        pla.setCurrentEnergy(pla.getCurrentEnergy() + 25);

        if (pla.getCurrentEnergy() > 100) {
            pla.setCurrentEnergy(100);
        }

        pla.getActionSender().updateEnergy();

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
