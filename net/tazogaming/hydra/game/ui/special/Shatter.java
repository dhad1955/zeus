package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/04/14
 * Time: 22:46
 */
public class Shatter implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, final Killable fightingWith) {
        if (pla.getCurrentSpecial() < 2.5) {
            pla.getActionSender().sendMessage("You don't have enough special attack");

            return false;
        }
        
        pla.setSpecialMultiplier(3.61);
        pla.getAnimation().animate(1060);
        pla.getGraphic().set(251, Graphic.HIGH);
        fightingWith.registerHit(pla);

        double dmg = Combat.applyMeleeDamage2(1.35, 1, pla, fightingWith);

        pla.setSpecialMultiplier(1.00);
        SpecialAttacks.removeSpecial(pla, 2.5);
        PlayerCombatAdapter.addCombatXP((int)dmg, Damage.TYPE_MELEE_CRUSH, pla);


        if (dmg > 0) {

        }

        return false;
    }
}
