package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/14
 * Time: 01:35
 */
public class GodWhipSpecial implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 4;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);
        pla.getGraphic().set(2176, Graphic.LOW);
        pla.getPrayerBook().setPoints(99);
        pla.getActionSender().sendStat(5);
        fightingWith.getGraphic().set(2169, Graphic.LOW);
        pla.getAnimation().animate(1658, 0);
        if (fightingWith instanceof Player) {
            fightingWith.getPlayer().getPrayerBook().removePoints(99);
            fightingWith.getPlayer().getActionSender().sendStat(5);
            fightingWith.getPlayer().getTimers().setTime(TimingUtility.DOUBLE_DROPS_TIMER, 120);
            fightingWith.getPlayer().getActionSender().sendMessage("Your prayer has been stopped for 60 seconds");
        } else {
            fightingWith.getNpc().getTimers().setTime(TimingUtility.DOUBLE_DROPS_TIMER, 60);
            pla.getActionSender().sendMessage(
                "The gods have granted you a gift, kill this mob within 30 seconds to get double loot");
        }

        return true;
    }
}
