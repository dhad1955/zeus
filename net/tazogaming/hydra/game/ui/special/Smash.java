package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/10/13
 * Time: 10:59
 */
public class Smash implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 3.5; //not drains 35% rather than 30%

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);
        pla.getAnimation().animate(10505, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(1840, Graphic.HIGH);

        if (pla.getCombatAdapter().getFightingWith() instanceof Player) {
            Player plr  = pla.getCombatAdapter().getFightingWith().getPlayer();
            int    calc = (int) (plr.getMaxStat(1) * 0.70);
             if (plr.getCurStat(1) > calc) {
                plr.setCurStat(1, calc);
                plr.getActionSender().sendStat(1);
            }
        }
        
        //now deals up to 25% more damage rather than 11% more
        int dmg = (int)Combat.applyMeleeDamage2(1.25, 1, pla, fightingWith);
        PlayerCombatAdapter.addCombatXP(dmg, -1, pla);
        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
