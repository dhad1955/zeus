package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/10/13
 * Time: 10:55
 */
public class QuickSmash implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(final Player pla, final Killable fightingWith) {
        if ((fightingWith == null) || fightingWith.isDead()) {
            return false;
        }

        if (!pla.canBeAttacked(fightingWith) ||!fightingWith.canBeAttacked(pla)) {
            return false;
        }

        if (pla.getAccount().gmaulTick == Core.currentTime) {
            return false;
        }

        pla.getAccount().gmaulTick = Core.currentTime;

        int amount = 5;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);
        pla.setSpecialMultiplier(1.00);

        double h = Combat.applyMeleeDamage2(1.00, 0, pla, fightingWith);

        PlayerCombatAdapter.addCombatXP((int) h, -1, pla);
        pla.getAnimation().animate(1667, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(340, Graphic.HIGH);
        pla.setSpecialMultiplier(1.00);

        if (pla.isInWilderness() && (pla.getCombatAdapter().getFightingWith() instanceof Player)) {
            pla.getTickEvents().add(new PlayerTickEvent(pla, 3) {
                @Override
                public void doTick(Player owner) {
                    if (fightingWith.isDead()) {
                        pla.addAchievementProgress2(Achievement.GMAUL_KO, 1);
                        pla.addAchievementProgress2(Achievement.GMAUL_FRENZY, 1);
                    }
                }
            });
        }

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
