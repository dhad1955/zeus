package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/03/14
 * Time: 16:16
 */
import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.RangeUtil;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/10/13
 * Time: 12:32
 */
public class GodBowSpecial implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(final Player pla, final Killable fightingWith) {
        int amount = 5;

        if (!RangeUtil.arrowCheck(pla)) {
            return false;
        }

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy left.");

            return false;
        }

        if (pla.getEquipment().getArrowsCount() < 2) {
            pla.getActionSender().sendMessage("You don't have enough arrows in your inventory");
        }

        SpecialAttacks.removeSpecial(pla, 5);
        pla.getAnimation().animate(1074, 0, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(472, Graphic.HIGH);
        RangeUtil.createRangeProjectile(472, pla, fightingWith, 15, 1, 1);
        pla.getEquipment().removeArrow();
        pla.setActionsDisabled(true);

        if (pla.getCombatAdapter().getFightingWith() instanceof NPC) {
            pla.getCombatAdapter().getFightingWith().getNpc().getTimers().setTime(TimingUtility.DOUBLE_DROPS_TIMER, 60);
        }

        pla.getTickEvents().addFirst(new PlayerTickEvent(pla, 1) {
            @Override
            public void doTick(Player owner) {
                RangeUtil.createRangeProjectile(256, pla, fightingWith, 15, 1, 1);
                pla.getGraphic().set(472, Graphic.HIGH);
                pla.setActionsDisabled(false);
                terminate();
            }
        });
        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
