package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.Sounds;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 16:17
 * To change this template use File | Settings | File Templates.
 */
public class AgsSpec implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(final Player pla, final Killable fightingWith) {
        double amount = 5;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }
        SpecialAttacks.removeSpecial(pla, amount);
        pla.getAnimation().animate(7074);
        Sounds.playAreaSound(pla.getLocation(), 3865);
        pla.setSpecialMultiplier(1.08); //now this will boost accuracy by 1.051x rather than 10.51x
        double dmg = Combat.applyMeleeDamage2(1.31, 1, pla, fightingWith); //now boosts by 25% rather than 31%
        pla.getTickEvents().add(new PlayerTickEvent(pla, true, 2) {
            @Override
            public void doTick(Player owner) {
                owner.setSpecialMultiplier(1.00);

                if (fightingWith.isDead() && fightingWith instanceof Player && pla.isInWilderness()) {
                    owner.addAchievementProgress2(Achievement.KNOCKOUT, 1);
                }

                terminate();
            }
        });
        pla.getGraphic().set(1222, Graphic.LOW);
        PlayerCombatAdapter.addCombatXP((int)dmg, -1, pla);

        return true;
    }
}
