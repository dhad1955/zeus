package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/14
 * Time: 01:24
 */
public class FrostWhipSpec implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 8;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);

        int time = 60;

        if (fightingWith instanceof Player) {
            time = 20;
        }

        if (pla.getGameFrame().getDuel() != null) {
            pla.getActionSender().sendMessage("This special cannot be used in the Duel arena.");

            return false;
        }

        boolean fail = false;

        if ((fightingWith instanceof NPC) && fightingWith.getNpc().getDefinition().isPoisonImmune()) {
            fail = true;
        }

        if (fail ||!fightingWith.setFrostWhipTime(time)) {
            pla.getActionSender().sendMessage("Your ice attack fails.");
        } else {
            pla.getActionSender().sendMessage("Your frost whip disables your enemies attack.");
        }

        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
