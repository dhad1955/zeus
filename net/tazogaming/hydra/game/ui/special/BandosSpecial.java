package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 15:36
 */
public class BandosSpecial implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 10;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);

        pla.setSpecialMultiplier(1.33);
        double   damage    = Combat.applyMeleeDamage2(1.10, 0, pla, fightingWith);
        pla.setSpecialMultiplier(1);
        int[] statOrder = { 1, 2, 0, 6, 4 };
        int   curInd    = 0;

        pla.getAnimation().animate(7073, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(1223, Graphic.LOW);
        PlayerCombatAdapter.addCombatXP((int)damage, -1, pla);

        long time = Core.currentTimeMillis();
        if ((damage > 0) && (fightingWith instanceof Player)) {
            Player plr = (Player) fightingWith;

            while (damage > 0) {
                if (curInd >= statOrder.length) {
                    break;
                }
                if(System.currentTimeMillis() - time > 100)
                    throw new RuntimeException("error slow bandos drain rate.");

                damage -= plr.drainStat(statOrder[curInd], (int)damage);

                if (damage != 0) {
                    curInd++;
                }

                if (damage < 0) {
                    throw new IndexOutOfBoundsException("error: damage is: " + damage + " index: " + curInd);
                }
            }
        }

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
