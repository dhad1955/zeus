package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
class MorrigansTickEvent extends PlayerTickEvent {
    private double     damage      = 0;
    private boolean wasInDuel   = false;
    private double     damageDealt = 0;
    private Player  sentBy;

    /**
     * Constructs ...
     *
     *
     * @param sentBy
     * @param target
     * @param damage
     * @param wasInDuel
     */
    public MorrigansTickEvent(Player sentBy, Player target, double damage, boolean wasInDuel) {
        super(target, 2, false, true, -1);
        this.damage    = damage;
        this.wasInDuel = wasInDuel;
        this.sentBy    = sentBy;
    }

    /**
     * Method doTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param owner
     */
    @Override
    public void doTick(Player owner) {

        double dmg = 5;
        if(dmg + damageDealt > damage)
        {
            dmg = damage - damageDealt;
        }
        this.damageDealt += owner.hit(sentBy, dmg, Damage.TYPE_OTHER, 0);



        if ((damageDealt >= damage) || (wasInDuel && (owner.getGameFrame().getDuel() == null)) || owner.isDead()) {
            terminate();
        }    // To change body of implemented methods use File | Settings | File Templates.
    }
}


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/03/14
 * Time: 13:35
 */
public class PhantomStrike implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 5;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        pla.getAnimation().animate(10501, 1);
        pla.getGraphic().set(1836, Graphic.HIGH);
        pla.getEquipment().removeWeapon();
        SpecialAttacks.removeSpecial(pla, amount);

        int[]      vars       = {
            50, 40, 50, 45, 65, 15, 90
        };

        fightingWith.registerHit(pla);
        Projectile projectile = new Projectile(pla, pla.getCombatAdapter().getFightingWith(), 1837, 10, 10);

        projectile.setVars(vars);
        projectile.setOnImpactEvent(new ImpactEvent() {
            @Override
            public void onImpact(Killable sender, Killable target) {
                sender.getPlayer().setSpecialMultiplier(4.00);
                double dmg = target.hit(sender, GameMath.randf(GameMath.rangeMaxHit(sender.getPlayer(), true, 13879)),
                                     Damage.TYPE_RANGE, 0);
                sender.getPlayer().setSpecialMultiplier(1.00);

                if ((dmg > 0) && (target instanceof Player)) {
                    target.getPlayer().addEventIfAbsent(new MorrigansTickEvent(sender.getPlayer(), target.getPlayer(),
                            dmg, ((Player) target).getGameFrame().getDuel() != null));
                }    // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        pla.registerProjectile(projectile);

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
