package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.game.ui.GameInterface;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 15:52
 * To change this template use File | Settings | File Templates.
 */
public class SpecialAttacks {
    private static SpecialAttackHandler[] handlers = new SpecialAttackHandler[100];

    static {
        handlers[0]  = new AgsSpec();
        handlers[1]  = new Puncture();
        handlers[2]  = new BandosSpecial();
        handlers[3]  = new HealingBlade();
        handlers[4]  = new IceCleave();
        handlers[5]  = new Cleave();
        handlers[6]  = new SaradominsLightning();
        handlers[7]  = new SliceNDice();
        handlers[8]  = new SnipeShot();
        handlers[10] = new DarkbowSpecial();
        handlers[11] = new PowerStab();
        handlers[12] = new Rarghh();
        handlers[13] = new Sweep();
        handlers[14] = new EnergyDrain();
        handlers[15] = new QuickSmash();
        handlers[16] = new Smash();
        handlers[17] = new Disrupt();
        handlers[18] = new Feint();
        handlers[19] = new GodWhipSpecial();
        handlers[20] = new FrostWhipSpec();
        handlers[21] = new PhoenixBowAttack();
        handlers[22] = new PhantomStrike();
        handlers[23] = new GodBowSpecial();
        handlers[24] = new Shatter();
        handlers[25] = new HandCannonBlast();
        handlers[26] = new DragonSpear();
    }

    /**
     * Method parseSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param spec
     *
     * @return
     */
    public static int parseSpecial(double spec) {
        return (int) (spec * 10);
    }

    /**
     * Method initializeSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param inter
     * @param pla
     */
    public static void initializeSpecial(GameInterface inter, Player pla) {
        Weapon               w       = pla.getEquipment().getWeapon();
        SpecialAttackHandler handler = getHandler(w);
        int                  barid   = getBarId(inter.getInterfaceId());

        if (barid == -1) {
            return;
        }

        if (handler == null) {
            MessageBuilder spb = new MessageBuilder().setId(171).addByte((byte) 1).addShort(barid);

            // pla.getIoSession().write(spb.toPacket());
            return;
        }

        MessageBuilder spb = new MessageBuilder().setId(171).addByte((byte) 0).addShort(barid);

        // pla.getIoSession().write(spb.toPacket());
        updateSpecial(pla);
        changeHighlight(pla);
    }

    /**
     * Method updateSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void updateSpecial(Player pla) {
        pla.getActionSender().sendVar(301, pla.isSpecialHighlighted()
                                           ? 1
                                           : 0);
        pla.getActionSender().sendVar(300, (int) pla.getCurrentSpecial() * 100);
        updateHighlight(pla);
    }

    /**
     * Method changeHighlight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void changeHighlight(Player pla) {
        pla.setHighlighted(!pla.isSpecialHighlighted());
        pla.getActionSender().sendVar(301, pla.isSpecialHighlighted()
                                           ? 1
                                           : 0);
    }

    /**
     * Method updateHighlight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void updateHighlight(Player pla) {
        if (specialAvailable(pla)) {
            int textOffset = getTextOffset(pla.getWindowManager().getSidebar(0).getInterfaceId());

            if (textOffset != -1) {
                String color = pla.isSpecialHighlighted()
                               ? "@yel@"
                               : "@bla@";

                pla.getActionSender().changeLine(color + "Special Attack (" + (parseSpecial(pla.getCurrentSpecial()))
                                                 + "%)", textOffset);
            }
        }
    }

    /**
     * Method specialAvailable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static boolean specialAvailable(Player pla) {
        return getHandler(pla.getEquipment().getWeapon()) != null;
    }

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param enemy
     *
     * @return
     */
    public static boolean doSpecial(Player pla, Killable enemy) {
        Duel d = pla.getGameFrame().getDuel();

        if ((d != null) && d.setting(Duel.NO_SPEC)) {
            pla.getActionSender().sendMessage("Special attacks have been disabled during this duel.");

            return false;
        }



        if (specialAvailable(pla)) {
            return handlers[pla.getEquipment().getWeapon().getSpecBarId()].doSpecial(pla, enemy);
        }

        return false;
    }

    /**
     * Method removeSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param amount
     */

    public static void removeSpecial(Player pla, double amount) {
        if (pla.getRights() >= Player.ADMINISTRATOR)
            return;

        if (pla.getEquipment().getId(Equipment.RING) == 20054 || pla.getEquipment().getId(Equipment.RING) == 13566) {
            if (!pla.isInWilderness()) {
                amount = amount * 0.50;
            }
            amount = amount * 0.85;
        }

        if (pla.getEquipment().wearingVigour()) {
            amount *= 0.90D; //should be good now
        }

        pla.setCurrentSpecial(pla.getCurrentSpecial() - amount);

        if (pla.getCurrentSpecial() < 0) {
            pla.setCurrentSpecial(0);
        }

        updateSpecial(pla);
    }

    /**
     * Method getHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param w
     *
     * @return
     */
    public static SpecialAttackHandler getHandler(Weapon w) {
        if (w.getSpecBarId() == -1) {
            return null;
        }

        return handlers[w.getSpecBarId()];
    }

    private static int getTextOffset(int xinterface) {
        switch (xinterface) {
        case 8460 :
            return 8505;

        case 12290 :
            return 12335;

        case 1764 :    // Shortbow
            return 7561;

        case 4446 :    // thrown
            return 7661;

        case 2423 :    // dragon scim
            return 7611;

        case 2276 :
            return 7586;

        case 7762 :
            return 7812;

        case 425 :
            return 7486;

        case 4705 :
            return 7711;

        case 1698 :
            return 7511;
        }

        return -1;
    }

    private static int getMediaOffset(int interfaceid) {
        switch (interfaceid) {
        case 12290 :
            return 12325;

        case 1764 :
            return 7551;

        case 4446 :
            return 7651;

        case 2423 :
            return 7601;

        case 2276 :
            return 7576;

        case 7762 :
            return 7802;

        case 4705 :
            return 7701;

        case 425 :
            return 7476;

        case 1698 :
            return 7501;
        }

        return -1;
    }

    private static int getBarId(int interfaceid) {
        switch (interfaceid) {
        case 12290 :    // Whip
            return 12323;

        case 4446 :
            return 7649;

        case 1764 :     // Shortbow
            return 7549;

        case 2423 :     // dragon scim
            return 7599;

        case 2276 :
            return 7574;

        case 425 :
            return 7474;

        case 7762 :
            return 7800;

        case 4705 :
            return 7699;

        case 8460 :
            return 8493;

        case 1698 :
            return 7499;
        }

        return -1;
    }
}
