package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.ui.PrayerBook;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/10/13
 * Time: 10:46
 */
public class Sweep implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amount = 5.5;

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);
        pla.getAnimation().animate(1203, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(282, 0);

        if(fightingWith instanceof Player){
            Player plr = (Player)fightingWith;
            PrayerBook book = plr.getPrayerBook();
            book.disableProtectionPrayers(10);

        }
        double h = Combat.applyMeleeDamage2(1.10, 1, pla, fightingWith);

        PlayerCombatAdapter.addCombatXP((int)h, -1, pla);

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
