package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public interface SpecialAttackHandler<T extends Killable> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    public boolean doSpecial(T t
            , Killable fightingWith);
}
