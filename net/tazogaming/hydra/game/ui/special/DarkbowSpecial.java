package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.skill.combat.RangeUtil;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/10/13
 * Time: 16:35
 */
public class DarkbowSpecial implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        if (!RangeUtil.arrowCheck(pla)) {
            return false;
        }

        if (pla.getEquipment().getArrowsCount() < 2) {
            pla.getActionSender().sendMessage("You don't have enough ammo.");

            return false;
        }

        if (pla.getCurrentSpecial() < 6) {
            pla.getActionSender().sendMessage("You dont have enough special energy");

            return false;
        }

        fightingWith.registerHit(pla);

        pla.getGraphic().set(RangeUtil.getDBowDrawbackGfx(pla.getEquipment().getId(Equipment.ARROWS)), Graphic.HIGH);
        pla.getAnimation().animate(426);
        pla.getEquipment().removeArrow();
        pla.getEquipment().removeArrow();
        pla.setDarkbowSpecial(true);
        SpecialAttacks.removeSpecial(pla, 6.5);

        if (RangeUtil.isDragonArrow(pla.getEquipment().getId(Equipment.ARROWS))) {
            doDragon(pla, fightingWith);
        } else {
            RangeUtil.createRangeProjectile(RangeUtil.getArrowGfx(pla.getEquipment().getId(Equipment.ARROWS),
                    pla.getEquipment().getId(3)), pla, fightingWith, 22, 3, 1.10, 50);
            RangeUtil.createRangeProjectile(RangeUtil.getArrowGfx(pla.getEquipment().getId(Equipment.ARROWS),
                    pla.getEquipment().getId(3)), pla, fightingWith, 22, 3, 1.05, 50);
        }
        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }

    private boolean doDragon(Player pla, Killable fightingWith) {
        RangeUtil.createRangeProjectile(1099, pla, fightingWith, 22, 3, 1.30, 2.90);
        RangeUtil.createRangeProjectile(1099, pla, fightingWith, 27, 3, 1.30, 2.90);
        return true;
    }
}
