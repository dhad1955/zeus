package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 16:18
 */
public class Cleave implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amt = 2.5;
    	
        if (pla.getCurrentSpecial() < amt) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amt);
        pla.getAnimation().animate(1058, 0, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(248, Graphic.HIGH);

        pla.setSpecialMultiplier(1.25);
        double hit = Combat.applyMeleeDamage2(1.25, 1, pla, fightingWith);

        pla.getTickEvents().add(new PlayerTickEvent(pla, true, 2) {
            @Override
            public void doTick(Player owner) {
                owner.setSpecialMultiplier(1.00);

            }
        });
        PlayerCombatAdapter.addCombatXP((int)hit, -1, pla);

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
