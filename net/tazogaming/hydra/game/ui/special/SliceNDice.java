package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.util.math.GameMath;

import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator Enchanta is a Runescape 2 Server emulator which
 * has been designed for educational purposes only Created by Daniel Hadland
 * Date: 30/09/13 Time: 16:34
 */
public class SliceNDice implements SpecialAttackHandler<Player> {

	public boolean doSpecial(final Player pla, final Killable fightingWith) {
		double amount = 5;
		if (pla.getCurrentSpecial() < amount) {
			pla.getActionSender().sendMessage("You don\'t have enough special energy.");
			return false;
		} else {
			SpecialAttacks.removeSpecial(pla, amount);

			pla.getAnimation().animate(10961, 0, 1);
			pla.getGraphic().set(1950, 0);
			if (pla.getCombatAdapter().getFightingWith() instanceof Player && pla.isInWilderness()) {
				pla.addAchievementProgress2(14, 1);
			}

			pla.setSpecialMultiplier(1.15D);
			double h1 = Combat.applyMeleeDamage2(1.0D, 1, pla, fightingWith);
			double h2 = 0.0D;
			double h3 = 0.0D;
			double h4 = 0.0D;

			if (h1 == 0.0D) {
				h2 = Combat.applyMeleeDamage2(1.0D, 1, pla, fightingWith);
				if (h2 > 0.0D) {
					h3 = fightingWith.hit(pla, h2 / 2.0D, 4, 2);
					h4 = fightingWith.hit(pla, h2 / 2.0D, 4, 2);
				} else {
					h3 = Combat.applyMeleeDamage2(0.75D, 2, pla, fightingWith);
					if (h3 > 0.0D) {
						h4 = Combat.applyMeleeDamage2(0.75D, 2, pla, fightingWith);
					} else {
						h4 = Combat.applyMeleeDamage2(1.5D, 2, pla, fightingWith);
						if (h4 == 0.0D)
							fightingWith.hit(pla, new Random().nextInt(8) / 10, 4, 3);
					}
				}
			} else {
				fightingWith.hit(pla, h1 / 2.0D, 4, 1);
				fightingWith.hit(pla, (h1 / 2.0D) * 0.55D, 4, 2);
				fightingWith.hit(pla, (h1 / 2.0D) * 0.45D, 4, 2);
			}

			PlayerCombatAdapter.addCombatXP((int) h1 + (int) h2 + (int) h3 + (int) h4, -1, pla);
			pla.getTickEvents().add(new PlayerTickEvent(pla, true, 3) {
				public void doTick(Player owner) {
					owner.setSpecialMultiplier(1.0D);
					if (fightingWith.isDead()) {
						owner.addAchievementProgress2(15, 1);
					}

					this.terminate();
				}
			});
			return true;
		}
	}
}

// flash was here i love u danny
// hank too