package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 16:27
 */
public class SaradominsLightning implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, Killable fightingWith) {
        double amt = 10;

        if (pla.getCurrentSpecial() < amt) {
            pla.getActionSender().sendMessage("You don't have enough special energy");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amt);
        pla.getAnimation().animate(7072, 0, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(2115, Graphic.HIGH);


        pla.setSpecialMultiplier(2.15);
        double dmg1 = Combat.applyMeleeDamage(pla, fightingWith, 0);

        PlayerCombatAdapter.addCombatXP((int)dmg1, -1, pla);


        double dmg2 = dmg1 >  0 ? fightingWith.hit(pla, GameMath.rand3(29), Damage.TYPE_MAGIC, 0) : 0;

        pla.setSpecialMultiplier(1.00);
        pla.addXp(6, (int)dmg2 * 2);
        fightingWith.getGraphic().set(1194, Graphic.LOW);

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
