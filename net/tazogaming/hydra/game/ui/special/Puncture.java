package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.Sounds;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/09/13
 * Time: 22:16
 */
public class Puncture implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(Player pla, final Killable fightingWith) {
    	double amount = 2.5;
    	
        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        SpecialAttacks.removeSpecial(pla, amount);
        pla.getAnimation().animate(1062);
        pla.getGraphic().set(252, Graphic.HIGH);
        pla.setSpecialMultiplier(1.25); //used to be 85%
        Sounds.playAreaSound(pla.getLocation(), 2537);
        if (pla.isInWilderness() && pla.getCombatAdapter().getFightingWith().isPlayer()) {
            pla.addAchievementProgress2(Achievement.HEART_PUNCTURE, 1);
        }
        double dmg = Combat.applyMeleeDamage2(1.15, 1, pla, fightingWith); //was 5%
        dmg += Combat.applyMeleeDamage2(1.15, 1, pla, fightingWith);
        PlayerCombatAdapter.addCombatXP((int)dmg, -1, pla);

        pla.getTickEvents().add(new PlayerTickEvent(pla, true, 2) {
            @Override
            public void doTick(Player owner) {
                owner.setSpecialMultiplier(1.00);
                terminate();
            }
        });
        return true;
    }
}
