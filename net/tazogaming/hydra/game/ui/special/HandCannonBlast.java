package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.RangeUtil;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/14
 * Time: 13:06
 */
public class HandCannonBlast implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     *
     * @param pla
     * @param fightingWith
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean doSpecial(Player pla, final Killable fightingWith) {
        double amount = 5;


        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy.");

            return false;
        }

        fightingWith.registerHit(pla);
        pla.getGraphic().set(2138, Graphic.LOW);
        pla.getAnimation().animate(12175);
        SpecialAttacks.removeSpecial(pla, amount);
        RangeUtil.createSpecialRangeProjectile(2143, pla, fightingWith, 10, 2, 0.90, 1.20, 20, 20);
        RangeUtil.createSpecialRangeProjectile(2143, pla, fightingWith, 10, 2, 0.90, 1.20, 20, 20);
        if(pla.isInWilderness() && pla.getCombatAdapter().getFightingWith().isPlayer()){
           pla.getTickEvents().add(new PlayerTickEvent(pla, true, 6) {
               @Override
               public void doTick(Player owner) {
                   if (fightingWith.isDead())
                       owner.addAchievementProgress2(Achievement.BLOWN_AWAY, 1);
                   terminate();
               }
           });

        }

        return false;
    }
}
