package net.tazogaming.hydra.game.ui.special;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Animation;
import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.Sounds;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.RangeUtil;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/10/13
 * Time: 12:32
 */
public class SnipeShot implements SpecialAttackHandler<Player> {

    /**
     * Method doSpecial
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fightingWith
     *
     * @return
     */
    @Override
    public boolean doSpecial(final Player pla, final Killable fightingWith) {
        int amount = 5;

        if (pla.getEquipment().getId(Equipment.ARROWS) == 15243) {
            return false;
        }

        if (!RangeUtil.arrowCheck(pla)) {
            return false;
        }

        if (pla.getCurrentSpecial() < amount) {
            pla.getActionSender().sendMessage("You don't have enough special energy left.");

            return false;
        }

        if (pla.getEquipment().getArrowsCount() < 2) {
            pla.getActionSender().sendMessage("You don't have enough arrows in your inventory");
        }

        fightingWith.registerHit(pla);
        SpecialAttacks.removeSpecial(pla, 5);
        pla.getAnimation().animate(1074, 0, Animation.HIGH_PRIORITY);
        pla.getGraphic().set(256, Graphic.HIGH);
        RangeUtil.createRangeProjectile(249, pla, fightingWith, 15, 1, 1.19, 2.1);
        pla.getEquipment().removeArrow();
        pla.setActionsDisabled(true);
        pla.getTimers().setTime(TimingUtility.ACHIEVEMENT_TIMER, 12);
        Sounds.playAreaSound(pla.getLocation(), 2545);
        pla.getTickEvents().addFirst(new PlayerTickEvent(pla, 1) {
            @Override
            public void doTick(Player owner) {
                RangeUtil.createRangeProjectile(249, pla, fightingWith, 0, 1, 1.15, 2.1);
                pla.setActionsDisabled(false);
                terminate();

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });

        if (pla.isInWilderness() && (pla.getCombatAdapter().getFightingWith() instanceof Player)) {
            pla.getTickEvents().add(new PlayerTickEvent(pla, 4) {
                @Override
                public void doTick(Player owner) {
                    if (fightingWith.isDead()) {
                        pla.addAchievementProgress2(Achievement.SNIPER, 1);
                    }
                }
            });
        }

//      pla.getEquipment().removeArrow();
        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
