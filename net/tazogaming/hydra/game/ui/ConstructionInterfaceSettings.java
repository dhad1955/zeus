package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ModelGrid;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/01/14
 * Time: 10:31
 */
public class ConstructionInterfaceSettings {
    private static final int    START_OFFSET_MODEL = 18863;
    private static final int    START_OFFSET_LEVEL = 18864;
    private static final int    LEVEL_DELIMITER    = 6;
    private static final int    START_OFFSET_ITEMS = 18865;
    private static final int    ITEMS_DELIMITER    = 6;
    private static final int    START_OFFSET_TITLE = 18862;
    public static final int     ITEM_ORDER[]       = {
        0, 2, 4, 6, 1, 3, 5
    };
    public static final int[]   ACTION_BUTTONS     = {
        18863, 18869, 18875, 18881, 18887, 18893, 18899, 18905
    };
    private ConstructableItem[] inventory          = new ConstructableItem[7];
    final int                   TEXT_BEGIN         = 14;
    final int                   ITEM_AMOUNT        = 4;
    final int                   LEVEL_BEGIN        = 56;
    final int                   CONFIG_BEGIN       = 1485;

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     * @param item
     */
    public void set(int index, ConstructableItem item) {
        if (index >= inventory.length) {
            return;
        }

        inventory[index] = item;
    }

    private int getSlot(int id) {
        for (int i = 0; i < ITEM_ORDER.length; i++) {
            if (ITEM_ORDER[i] == id) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method onButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     * @param i
     *
     * @return
     */
    public boolean onButton(Player plr, int i) {
        i = getSlot(i);

        if (i == -1) {
            return false;
        }

        if (inventory[i] != null) {
            World.getWorld().getScriptManager().directTrigger(plr, null, Trigger.CONSTRUCTION_MAKE, inventory[i].getModel());

            return true;
        }

        return false;
    }

    /**
     * Method renderItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param offset
     * @param player
     */
    public void renderItem(int offset, Player player) {
        if (inventory[offset] == null) {
            player.getGameFrame().sendString("", 396, TEXT_BEGIN + (offset * 5));
            player.getActionSender().sendVar(CONFIG_BEGIN + offset, 1);
            player.getGameFrame().sendString("", 396, LEVEL_BEGIN + offset);

            for (int i = 0; i < 4; i++) {
                player.getGameFrame().sendString("", 396, TEXT_BEGIN + (offset * 5) + i + 1);
            }
        } else {
            player.getGameFrame().sendString(Item.forId(inventory[offset].getModel()).getName(), 396,
                                             TEXT_BEGIN + (offset * 5));
            player.getGameFrame().sendString("Level " + inventory[offset].getLvl(), 396, LEVEL_BEGIN + offset);

            boolean show  = player.getCurStat(22) >= inventory[offset].getLvl();
            int[]   items = inventory[offset].getItems();
            int[]   amts  = inventory[offset].getAmts();

            for (int i = 0; i < 4; i++) {
                if (i < items.length) {
                    show &= player.getInventory().hasItem(items[i], amts[i]);
                    player.getGameFrame().sendString(amts[i] + " x" + Item.forId(items[i]).getName(), 396,
                                                     TEXT_BEGIN + (offset * 5) + i + 1);
                } else {
                    player.getGameFrame().sendString("", 396, TEXT_BEGIN + (offset * 5) + i + 1);
                }
            }

            if (show) {
                player.getActionSender().sendVar(CONFIG_BEGIN + offset, 1);
            } else {
                player.getActionSender().sendVar(CONFIG_BEGIN + offset, 0);
            }
        }
    }

    /**
     * Method send
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void send(Player player) {
        ModelGrid grid = new ModelGrid(8, 396, 11, 4, 2);

        grid.addOption("Build");
        player.getActionSender().sendClientScript(grid.getCS2());

        for (int i = 0; i < 7; i++) {
            renderItem(i, player);
        }

        Item[] renderItems  = new Item[this.inventory.length];
        int[]  renderItemsC = new int[this.inventory.length];

        for (int i = 0; i < this.inventory.length; i++) {
            if (this.inventory[i] != null) {
                renderItems[ITEM_ORDER[i]]  = Item.forId(this.inventory[i].getModel());
                renderItemsC[ITEM_ORDER[i]] = 1;
            }
        }

        player.getActionSender().sendItems(8, renderItems, renderItemsC, false);
    }
}
