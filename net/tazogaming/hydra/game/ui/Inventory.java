package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 10:46
 * To change this template use File | Settings | File Templates.
 */
public class Inventory {
    private Item[] inventoryItems       = new Item[28];
    private int[]  inventoryItemsAmount = new int[28];
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param pla
     */
    public Inventory(Player pla) {
        player = pla;
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Item[] getItems() {
        return inventoryItems;
    }

    /**
     * Method getItemsCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getItemsCount() {
        return inventoryItemsAmount;
    }

    /**
     * Method setItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param items
     * @param count
     */
    public void setItems(int[] items, int[] count) {
        for (int i = 0; i < items.length; i++) {
            if (items[i] != -1) {
                inventoryItems[i] = Item.forId(items[i]);
            }

            inventoryItemsAmount[i] = count[i];
        }
    }

    /**
     * Method getStackCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getStackCount(int id) {
        int slot = getItemSlot(Item.forId(id));

        if (slot == -1) {
            return 0;
        }

        return inventoryItemsAmount[slot];
    }

    /**
     * Method clear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void clear() {
        this.inventoryItems       = new Item[28];
        this.inventoryItemsAmount = new int[28];
        flushInventory();
    }

    /**
     * Method replaceItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param newId
     */
    public void replaceItem(int slot, int newId) {
        this.inventoryItems[slot]       = Item.forId(newId);
        this.inventoryItemsAmount[slot] = 1;
        player.getActionSender().setItem(Item.forId(newId), inventoryItemsAmount[slot], slot, 93);
    }
    public void replaceItem(int slot, int newId, int amount) {
        this.inventoryItems[slot]       = Item.forId(newId);
        this.inventoryItemsAmount[slot] = amount;
        player.getActionSender().setItem(Item.forId(newId), inventoryItemsAmount[slot], slot, 93);
    }
    /**
     * Method getFreeSlots
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public int getFreeSlots(int itemId) {
        int free = 0;

        for (int i = 0; i < inventoryItems.length; i++) {
            if ((inventoryItems[i] == null)
                    || ((inventoryItems[i] != null) && inventoryItems[i].isStackable()
                        && (inventoryItems[i].getIndex() == itemId))) {
                free++;
            }
        }

        return free;
    }

    /**
     * Method getMaxItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemID
     *
     * @return
     */
    public int getMaxItem(int itemID) {
        int slot = getItemSlot(Item.forId(itemID));

        if (slot == -1) {
            return 0;
        }

        return getItemMax(slot, Integer.MAX_VALUE);
    }

    /**
     * Method maxItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param amount
     *
     * @return
     */
    synchronized public int maxItem(int item, int amount) {
        return getItemMax(getItemSlot(Item.forId(item)), amount);
    }

    /**
     * Method getItemMax
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param max
     *
     * @return
     */
    synchronized public int getItemMax(int slot, int max) {
        if ((slot > this.inventoryItems.length) || (slot < 0)) {
            return -1;
        }

        Item theItem = this.inventoryItems[slot];

        if (theItem == null) {
            return -1;
        }

        if (theItem.isStackable()) {
            int total = this.getItemsCount()[slot];

            if (total > max) {
                total = max;
            }

            return total;
        } else {
            int total = 0;

            for (int i = 0; i < this.inventoryItems.length; i++) {
                if (inventoryItems[i] == theItem) {
                    total++;
                }
            }

            if (total > max) {
                total = max;
            }

            return total;
        }
    }

    /**
     * Method addItemDrop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param amount
     */
    public void addItemDrop(Item item, int amount) {
        int slot = getNextSlot();

        if (slot == -1) {
            new FloorItem(item.getIndex(), amount, player.getX(), player.getY(), player.getHeight(), player);
        } else {
            addItem(item, amount);
        }
    }

    /**
     * Method canAdd
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cashAmount
     *
     * @return
     */
    public boolean canAdd(int cashAmount) {
        for (int i = 0; i < inventoryItems.length; i++) {
            if ((inventoryItems[i] != null) && (inventoryItems[i].getIndex() == 995)) {
                long l = (long) ((long) inventoryItemsAmount[i] + (long) cashAmount);

                if (l > Integer.MAX_VALUE) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param amount
     * @param update
     *
     * @return
     */
    public boolean addItem(Item item, int amount, boolean update) {
        if (item == null) {
            return false;
        }

        if (amount == 0) {
            return false;
        }


        if (item.isStackable()) {
            int slot = getItemSlot(item);

            if (slot != -1) {
                int curAmount = inventoryItemsAmount[slot];

                if (amount + curAmount < 0) {
                    return false;    // over max value
                }

                long l = amount + curAmount;

                if (l > Integer.MAX_VALUE) {
                    return false;
                }

                inventoryItemsAmount[slot] += amount;
            } else {
                slot = getNextSlot();

                if (slot == -1) {
                    return true;
                }

                inventoryItems[slot]       = item;
                inventoryItemsAmount[slot] = amount;
            }

            player.getActionSender().setItem(item, inventoryItemsAmount[slot], slot, 93);

            if (player.getWindowManager().getCurrentInventoryInterface() != 3214) {

//              /                player.getActionSender().putItemOnGrid(interfaces.getIndex(), inventoryItemsAmount[slot], slot, 3214);
            }

            return true;
        }

        boolean noUpdate = amount > 3;

        for (int i = 0; i < amount; i++) {
            int nextSlot = getNextSlot();

            if (nextSlot == -1) {
                if (noUpdate) {
                    flushInventory();
                }

                return true;
            }

            inventoryItems[nextSlot]       = item;
            inventoryItemsAmount[nextSlot] = 1;

            if (!noUpdate) {
                player.getActionSender().setItem(item, inventoryItemsAmount[nextSlot], nextSlot, 93);
            }
        }

        if (noUpdate) {
            flushInventory();
        }

        return true;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param amt
     *
     * @return
     */
    public boolean addItem(Item i, int amt) {
        return addItem(i, amt, true);
    }

    /**
     * Method flushInventory
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void flushInventory() {
        player.getActionSender().sendItems(93, inventoryItems, inventoryItemsAmount, false);
    }

    /**
     * Method countItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int countItems(int id) {
        int c = 0;

        for (int i = 0; i < getItems().length; i++) {
            if ((getItems()[i] != null) && (getItems()[i].getIndex() == id)) {
                c += getCount(i);
            }
        }

        return c;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amount
     *
     * @return
     */
    public boolean addItem(int id, int amount) {
        return addItem(Item.forId(id), amount, true);
    }

    /**
     * Method getItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     *
     * @return
     */
    public Item getItem(int slot) {
        return inventoryItems[slot];
    }

    /**
     * Method getCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     *
     * @return
     */
    public int getCount(int slot) {
        return inventoryItemsAmount[slot];
    }

    /**
     * Method hasItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param amount
     *
     * @return
     */
    public boolean hasItem(Item i, int amount) {
        if (i == null) {
            return false;
        }

        if (i.isStackable()) {
            int slot = getItemSlot(i);

            if (slot != -1) {
                return inventoryItemsAmount[slot] >= amount;
            }
        }

        int count = 0;

        for (int k = 0; k < inventoryItems.length; k++) {
            if (i == inventoryItems[k]) {
                count++;
            }
        }

        return count >= amount;
    }

    /**
     * Method hasItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amount
     *
     * @return
     */
    public boolean hasItem(int id, int amount) {
        return hasItem(Item.forId(id), amount);
    }

    /**
     * Method deleteItemFromSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param amount
     * @param slot
     */
    public void deleteItemFromSlot(Item i, int amount, int slot) {
        inventoryItemsAmount[slot] -= amount;

        if (inventoryItemsAmount[slot] <= 0) {
            inventoryItemsAmount[slot] = 0;
            inventoryItems[slot]       = null;
        }

        player.getActionSender().setItem(inventoryItems[slot], inventoryItemsAmount[slot], slot, 93);
    }

    /**
     * Method moveItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     */
    public void moveItems(int from, int to) {
        Item tmp       = inventoryItems[from];
        int  tmpAmount = inventoryItemsAmount[from];

        inventoryItems[from]       = inventoryItems[to];
        inventoryItemsAmount[from] = inventoryItemsAmount[to];
        inventoryItems[to]         = tmp;
        inventoryItemsAmount[to]   = tmpAmount;

        player.getActionSender().setItem(inventoryItems[to], inventoryItemsAmount[to], to, 93);
        player.getActionSender().setItem(inventoryItems[from], inventoryItemsAmount[from], from, 93);

    }

    /**
     * Method deleteItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param amount
     */
    public void deleteItem(Item item, int amount) {
        if (item == null) {
            return;
        }

        if (item.isStackable()) {
            int slot = getItemSlot(item);

            if (slot != -1) {
                deleteItemFromSlot(inventoryItems[slot], amount, slot);

                return;
            }
        }


        if(amount > 28)
            amount = 28;

        for (int i = 0; i < amount; i++) {
            int slot = -1;

            if ((slot = getItemSlot(item)) != -1) {
                deleteItemFromSlot(inventoryItems[slot], amount, slot);
            }
        }
    }

    /**
     * Method deleteItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amount
     */
    public void deleteItem(int id, int amount) {
        deleteItem(Item.forId(id), amount);
    }

    /**
     * Method getItemSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public int getItemSlot(Item item) {
        for (int i = 0; i < inventoryItems.length; i++) {
            if ((inventoryItems[i] != null) && (inventoryItems[i].getIndex() == item.getIndex())) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getNextSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNextSlot() {
        for (int i = 0; i < inventoryItems.length; i++) {
            if (inventoryItems[i] == null) {
                return i;
            }
        }

        return -1;
    }
}
