package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 18:52
 */
public class SkillGuide {
    public static final int      SKILL_MENU_IDS[]  = {
        8800, 8844, 8813, 8825, 8828, 8838, 8841, 8850, 8860, 8863, 15294, 15304, 15307
    };
    public static final int      SKILL_MENU_TEXT[] = {
        8846, 8823, 8824, 8827, 8837, 8840, 8843, 8859, 8862, 8826, 8865, 15303, 15306, 15309
    };
    public static final String[] SKILL_NAMES       = {
        "Attack", "Defence", "Strength", "Hitpoints", "Range", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching",
        "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblore", "Agility", "Thieving", "Slayer",
        "Farming", "Runecrafting", "Hunter", "Construction", "Summoning", "Dungeoneering",
    };
    int                         offset   = 0;
    private SkillGuideSubMenu[] subMenus = new SkillGuideSubMenu[20];

    /**
     * Method levelLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lvl
     * @param req
     * @param name
     * @param pla
     * @param line
     */
    public static void levelLine(int lvl, int req, String name, Player pla, int line) {
        int i = 8720 + line;
        int k = 8760 + line;

        if (pla.getCurStat(lvl) < req) {
            pla.getActionSender().changeLine("@dre@" + req, i);
        } else {
            pla.getActionSender().changeLine("" + req, i);
        }

        pla.getActionSender().changeLine(name, k);
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void render(Player pla) {
        int subId = pla.getAccount().getInt32("skill_menu_id");

        if (subId >= subMenus.length) {
            subId = subMenus.length - 1;
        }

        pla.getActionSender().changeLine("", 8849);
        pla.getActionSender().changeLine(SKILL_NAMES[subMenus[subId].getSkillId()], 8716);

        for (int i = 0; i < SKILL_MENU_IDS.length; i++) {
            if (i >= offset) {
                pla.getActionSender().hideInter(SKILL_MENU_IDS[i], 1);
            } else {
                pla.getActionSender().hideInter(SKILL_MENU_IDS[i], 0);
                pla.getActionSender().changeLine(subMenus[i].getName(), SKILL_MENU_TEXT[i]);
            }
        }

        pla.getActionSender().clearInterface(8847);
        pla.getActionSender().sendModelGrid(subMenus[subId].getModels(), subMenus[subId].getAmounts(), 8847);

        for (int k = 8720; k < 8720 + 40; k++) {
            pla.getActionSender().changeLine("", k);
        }

        for (int y = 8760; y < 8760 + 40; y++) {
            pla.getActionSender().changeLine("", y);
        }

        for (int i = 0; i < subMenus[subId].length(); i++) {
            SkillSub t = subMenus[subId].getSub(i);

            levelLine(subMenus[subId].getSkillId(), t.getLevelRequired(), t.getName(), pla, i);
        }

        pla.getActionSender().showInterface(8714);
    }

    /**
     * Method getMenu
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public SkillGuideSubMenu getMenu(int id) {
        return subMenus[id];
    }

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void set(SkillGuideSubMenu e) {
        subMenus[offset++] = e;
    }
}
