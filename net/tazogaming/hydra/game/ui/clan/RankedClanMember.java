package net.tazogaming.hydra.game.ui.clan;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/11/13
 * Time: 04:50
 */

/**
 * Created with IntelliJ IDEA.
 * User: Danny
 * Date: 09/12/12
 * Time: 13:59
 * To change this template use File | Settings | File Templates.
 */
public class RankedClanMember {
    public long username = 0;
    int         ranking  = 0;

    /**
     * Constructs ...
     *
     */
    public RankedClanMember() {}

    /**
     * Constructs ...
     *
     *
     * @param user
     * @param rank
     */
    public RankedClanMember(long user, int rank) {
        this.username = user;
        this.ranking  = rank;
    }
}
