package net.tazogaming.hydra.game.ui.clan;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/11/13
 * Time: 05:00
 */
public class ChannelList {
    private HashMap<Long, ClanChannel> channel_list = new HashMap<Long, ClanChannel>();

    /**
     * Method joinChannel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param name
     */
    public void joinChannel(Player player, String name) {
        if ((player.getGetCurrentWar() != null) && (player.getChannel().getWar() != player.getGetCurrentWar())) {
            return;
        }

        ClanChannel channel = getChannel(name);

        if (channel == null) {
            player.getActionSender().sendMessage("Unable to find channel " + name);

            return;
        }

        if (!channel.canJoin(player)) {
            return;
        }

        channel.addPlayer(player);
    }

    public ClanChannel getByPlayer(String player){
        long l = Text.longForName(player);
        for(ClanChannel channel : channel_list.values()){
            if(channel.getChannelOwner() == l)
                return channel;
        }
        return null;
    }

    /**
     * Method saveChannels
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void saveChannels() {
      try {
          Buffer data = new Buffer(228000000);

          data.writeByte(1);
          data.writeWord(channel_list.size());

          for (ClanChannel c : channel_list.values()) {
              data.writeString(c.getName());
              data.writeQWord(c.getChannelOwner());
              data.writeQWord(c.isApproved() ? 1 : 0);
              data.writeByte(c.rankJoin);
              data.writeByte(c.rankKick);
              data.writeByte(c.rankLoot);
              data.writeByte(c.rankTalk);

              int members = c.getMembers().size();

              data.writeWord(c.getMembers().size());

              for (int i = 0; i < members; i++) {
                  RankedClanMember member = c.getMembers().get(i);

                  data.writeQWord(member.username);
                  data.writeByte(member.ranking);
              }
          }

          try {
              data.save("config/clanlist2.dat");
          } catch (IOException e) {
              e.printStackTrace();    // To change body of catch statement use File | Settings | File Templates.
          }
      }catch (Exception ee) {}
    }

    /**
     * Method loadChannels
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void loadChannels() {
        try {
            Buffer d       = new Buffer("config/clanlist2.dat");
            int    version = d.readByte();
            ;
            int    size    = d.readShort();

            boolean approved = false;
            for (int i = 0; i < size; i++) {
                String      name     = d.readString();
                long        owner    = d.readQWord();
                long        cash     = d.readQWord();
                int         rankJoin = d.readByte();
                int         rankKick = d.readByte();
                int         rankLoot = d.readByte();
                int         rankTalk = d.readByte();
                ClanChannel c        = new ClanChannel(owner, name);

                c.rankJoin = rankJoin;
                c.rankTalk = rankTalk;
                c.rankLoot = rankLoot;
                c.setApproved(cash == 1);
                c.rankKick = rankKick;

                int siz = d.readShort();

                for (int k = 0; k < siz; k++) {
                    c.getMembers().add(new RankedClanMember(d.readQWord(), d.readByte()));
                }

                channel_list.put(Text.stringToLong(name), c);
            }
        } catch (IOException e) {
            Logger.err("Unable to load clan list");
        }
    }

    /**
     * Method removeChannel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     */
    public void removeChannel(ClanChannel c) {
        c.bootAll();
        channel_list.remove(c);
    }

    /**
     * Method addChannel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param player
     *
     * @return
     */
    public ClanChannel addChannel(String name, Player player) {
        if (getChannel(name) != null) {
            player.getActionSender().sendMessage("Channel already exists, try another name");

            return null;
        }

        ClanChannel chan = new ClanChannel(player.getUsernameHash(), name);

        channel_list.put(Text.stringToLong(name), chan);
        player.getAccount().setSetting("channel_name", name, true);

        return chan;
    }

    /**
     * Method changeName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param to
     */
    public void changeName(Player pla, String to) {
        ClanChannel channel = getChannel(pla.getAccount().getString("channel_name"));

        if (channel == null) {
            return;
        }

        if (getChannel(to) != null) {
            pla.getActionSender().sendMessage("Channel already exists, try another name");

            return;
        }

        channel.setName(to);
        channel_list.remove(Text.stringToLong(pla.getAccount().getString("channel_name")));
        channel_list.put(Text.stringToLong(to), channel);
        pla.getAccount().setSetting("channel_name", to, true);
    }

    /**
     * Method getChannel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public ClanChannel getChannel(String name) {
        if (!channel_list.containsKey(Text.stringToLong(name))) {
            return null;
        }

        return channel_list.get(Text.stringToLong(name));
    }
}
