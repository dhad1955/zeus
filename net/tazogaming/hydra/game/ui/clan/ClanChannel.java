package net.tazogaming.hydra.game.ui.clan;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.slayer.Slayer;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/11/13
 * Time: 04:49
 */
public class ClanChannel {
    public static final String rankings[] = {
        "Anyone", "Clan friends", "Recruit+", "Corporal", "Sergant+", "Lieutennant+", "Captain+", "General+", "Owner",
        "Owner Only", "Hydrascape admin"
    };
    public static final int
        ANYONE        = 0,
        RECRUIT       = 1,
        CORPORAL      = 2,
        SERGANT       = 3,
        LIUETENNANT   = 4,
        CAPTAIN       = 5,
        GENERAL       = 6,
        OWNER         = 7,
        HYDRA_ADMIN = 127;

    // SERGANT = 3, LIEUTENNANT = 4,  CAPTAIN = 5, ADMIN = 6, ORGANISER = 7,  DOWNER =8, ONLY_ME = 9;
    public static final int
        SETTING_NAME    = 0,
        SETTING_JOIN    = 1,
        SETTING_KICK    = 2,
        SETTING_LOOT    = 3,
        SETTTING_POINTS = 4,
        SETTING_ADVERT  = 5;

    /** messageCounter made: 14/08/24 */
    private static int messageCounter = 0;

    // dont get better than that lol what was that ctrl + n try it on urs nah i have diff keybindings, mine are the same as vs o lol
    //
    public static final int BAN_LENGTH = 1000 * 60 * 10;

    // wot cos its not in src make a new package oh

    /**
     * Method sendClanList
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param clan
     */
    public static int RANK_ID = 0;

    /** clanPot made: 14/08/24 */
    private long   clanPot              = 0;
    int            rankedMembersDemoter = 0;
    public boolean rankAdvert           = false;
    int            rankJoin             = 0;
    int            rankKick             = 0;
    int            rankTalk             = 0;
    int            rankLoot             = 0;
    int            rankPoints           = 0;
    boolean        advertOn             = false;

    /** approved made: 14/10/02 */
    private boolean approved = false;

    /** clanWins made: 14/10/02 */
    private int clanWins = 0;

    /** clanLosses made: 14/10/02 */
    private int clanLosses = 0;

    /** chatters made: 14/08/24 */
    private ArrayList<Player>   chatters      = new ArrayList<Player>();
    ArrayList<RankedClanMember> rankedMembers = new ArrayList<RankedClanMember>();

    /** channelOwner made: 14/08/24 */
    private long channelOwner = 0;

    /** slayer_task_id made: 14/08/24 */
    private int slayer_task_id = -1;

    /** slayer_task_kills made: 14/08/24 */
    private int slayer_task_kills = -1;

    /** slayer_task_total made: 14/08/24 */
    private int slayer_task_total = -1;

    /** player_clan_kills made: 14/08/24 */
    private HashMap<Long, Integer> player_clan_kills = new HashMap<Long, Integer>();

    /** bannedUsers made: 14/10/02 */
    private Map<Long, Long> bannedUsers = new HashMap<Long, Long>();

    /** channelName made: 14/08/24 */
    private String channelName;

    /** currentWar made: 14/08/24 */
    private ClanWar currentWar;

    /**
     * Constructs ...
     *
     *
     * @param l
     * @param name
     */
    public ClanChannel(long l, String name) {
        this.channelOwner = l;    // pla.getUsernameHash();
        this.rankJoin     = ANYONE;
        this.rankTalk     = ANYONE;
        this.rankKick     = OWNER;
        this.rankLoot     = OWNER;
        this.rankPoints   = OWNER;
        this.advertOn     = false;
        this.channelName  = name;
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param name
     */
    public ClanChannel(Player pla, String name) {
        this.channelOwner = pla.getUsernameHash();
        this.rankJoin     = ANYONE;
        this.rankTalk     = ANYONE;
        this.rankKick     = OWNER;
        this.rankLoot     = OWNER;
        this.rankPoints   = OWNER;
        this.advertOn     = false;
        this.channelName  = name;
    }

    /**
     * Method banUser
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bannedBy
     * @param toBan
     */
    public void banUser(Player bannedBy, Player toBan) {
        if ((toBan != null) && chatters.contains(toBan)) {
            if (toBan.getUsernameHash() == getChannelOwner()) {
                return;
            }

            if ((getRankForPlayer(toBan) >= getRankForPlayer(bannedBy)) && (bannedBy.getRights() >= Player.MODERATOR)) {
                toBan.getActionSender().sendMessage("You can't ban someone who's equal or higher to you in rank");

                return;
            }

            this.chatters.remove(toBan);
            this.bannedUsers.put(toBan.getUsernameHash(), Core.currentTimeMillis() + BAN_LENGTH);
            sendClanList(toBan, null);
            toBan.getActionSender().sendMessage(Text.RED("You have been banned from " + getName()));
            message("ClanManager", HYDRA_ADMIN,
                    "User " + bannedBy.getUsername() + " has banned " + toBan.getUsername());

            for(Player player : chatters) {
                sendClanList(player, this);
            }
        }
    }

    /**
     * Method getClanWins
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getClanWins() {
        return clanWins;
    }

    /**
     * Method setClanWins
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param clanWins
     */
    public void setClanWins(int clanWins) {
        this.clanWins = clanWins;
    }

    /**
     * Method isApproved
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isApproved() {
        return approved;
    }

    /**
     * Method setApproved
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param approved
     */
    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    /**
     * Method getClanLosses
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getClanLosses() {
        return clanLosses;
    }

    /**
     * Method setClanLosses
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param clanLosses
     */
    public void setClanLosses(int clanLosses) {
        this.clanLosses = clanLosses;
    }

    /**
     * Method getRankJoin
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRankJoin() {
        return rankJoin;
    }

    /**
     * Method getRankTalk
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRankTalk() {
        return rankTalk;
    }

    /**
     * Method getRankKick
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRankKick() {
        return rankKick;
    }

    /**
     * Method getRankLoot
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRankLoot() {
        return rankLoot;
    }

    /**
     * Method getRankPoints
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRankPoints() {
        return rankPoints;
    }

    /**
     * Method deposit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     * @param pla
     */
    public void deposit(long l, Player pla) {
        clanPot += l;
        message("ClanServer", HYDRA_ADMIN,
                pla.getUsername() + " has deposited " + getCashString(l) + "GP into the pot");
        message("ClanServer", HYDRA_ADMIN,
                pla.getUsername() + " the pot is now at " + getCashString(clanPot) + " GP");
        updateClanPot();
    }

    /**
     * Method deposit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     */
    public void deposit(long l) {
        clanPot += l;
        updateClanPot();
    }

    /**
     * Method withdraw
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     */
    public void withdraw(long l) {
        clanPot -= l;
        updateClanPot();
    }

    /**
     * Method requestCash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void requestCash(Player player) {

        /*
         *   player.getWindowManager().setTextRequest(new TextInputRequest("Please enter an amount to deposit, eg (5m) or (5b)", player) {
         *     @Override
         *     public void onResponse(Player player, String response) {
         *         player.getChannel().depositPot(response, player);
         *     }
         * });
         */
    }

    /**
     * Method giveCash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param amount
     */
    public void giveCash(Player player, long amount) {
        player.getActionSender().sendMessage("@red@" + getCashString(amount)
                + " GP has been sent to your bank via your clan");

        final int hunMill = 100000000;

        if (amount < hunMill) {
            player.getBank().insert(995, (int) amount);
        } else {
            long notes     = amount / hunMill;
            long remainder = amount - (notes * hunMill);

            if (remainder != 0) {
                player.getBank().insert(995, (int) remainder);
            }

            player.getBank().insert(18652, (int) notes);
        }
    }

    /**
     * Method requestDeposit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void requestDeposit(Player player) {}

    /**
     * Method distribute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void distribute() {
        Player[] players = new Player[chatters.size()];
        int      count   = 0;

        for (Player p : chatters) {
            if (getRankForPlayer(p) >= this.rankLoot) {
                players[count++] = p;
            }
        }

        if (count == 0) {
            return;
        }

        long amount = clanPot / count;

        clanPot = 0;
        message("Info", HYDRA_ADMIN,
                "Clan pot has been distributed between " + count + " users (" + getCashString(amount) + " GP each)");

        for (int i = 0; i < count; i++) {
            giveCash(players[i], amount);
        }

        updateClanPot();
    }

    /**
     * Method getCashString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param j
     *
     * @return
     */
    public static final String getCashString(long j) {
        if ((j >= 0) && (j < 10000)) {
            return String.valueOf(j);
        }

        if ((j >= 10000) && (j < 10000000)) {
            return j / 1000 + "K";
        }

        if (j >= 1000000000) {
            return j / 1000000000 + "B";
        }

        if ((j >= 10000000) && (j < 999999999)) {
            return j / 1000000 + "M";
        }

        if (j >= 999999999) {
            return "*";
        } else {
            return "?";
        }
    }

    /**
     * Method getClanPot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getClanPot() {
        return clanPot;
    }

    /**
     * Method updateClanPot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateClanPot() {
        for (Player player : chatters) {
            player.getActionSender().changeLine("Clan pot: " + getCashString(clanPot) + " gp", 23712);
        }
    }

    /**
     * Method depositPot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param stringAmount
     * @param player
     */
    public void depositPot(String stringAmount, Player player) {
        try {
            final int hunMill = 100000000;

            if (stringAmount.toLowerCase().contains("k")) {
                stringAmount = stringAmount.replaceAll("k", "000");
            } else if (stringAmount.toLowerCase().contains("m")) {
                stringAmount = stringAmount.replaceAll("m", "000000");
            } else if (stringAmount.toLowerCase().contains("b")) {
                stringAmount = stringAmount.replaceAll("b", "000000000");
            }

            long l             = Long.parseLong(stringAmount);
            int  currentCash   = player.getInventory().getMaxItem(995);
            int  currentNotes  = player.getInventory().getMaxItem(18652);
            long countedAmount = 0;

            if (currentCash > 0) {
                countedAmount += currentCash;
            }

            if (currentNotes > 0) {
                countedAmount += (currentNotes * hunMill);
            }

            if (countedAmount < l) {
                player.getActionSender().sendMessage("Not enough cash");

                return;
            }

            if (l < hunMill) {
                if (currentCash < l) {
                    player.getActionSender().sendMessage("Not enough coins Please exchange your notes");

                    return;
                } else {
                    player.getInventory().deleteItem(995, (int) l);
                    deposit(l, player);

                    return;
                }
            } else {
                if (currentCash > l) {
                    player.getInventory().deleteItem(995, (int) l);
                    deposit(l, player);

                    return;
                }

                int notesRequired = (int) (l / hunMill);

                if (notesRequired <= currentNotes) {
                    long remainder = l - (notesRequired * hunMill);

                    if (remainder == 0) {
                        player.getInventory().deleteItem(18652, notesRequired);
                        deposit(notesRequired * hunMill, player);

                        return;
                    } else {
                        if (currentCash < remainder) {
                            player.getActionSender().sendMessage("Not enough coins remaining");

                            return;
                        }

                        player.getInventory().deleteItem(18652, notesRequired);
                        player.getInventory().deleteItem(995, (int) remainder);
                        deposit((notesRequired * hunMill) + remainder, player);
                    }
                }
            }
        } catch (Exception ee) {
            player.getActionSender().sendMessage("Error");
        }
    }

    /**
     * Method setCurrentWar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param war
     */
    public void setCurrentWar(ClanWar war) {
        this.currentWar = war;
    }

    /**
     * Method getWar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ClanWar getWar() {
        return currentWar;
    }

    /**
     * Method registerClanKill
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public void registerClanKill(Player plr) {
        slayer_task_kills++;

        int perc = (int)GameMath.getPercentFromTotal(slayer_task_kills, slayer_task_total);

        switch (perc) {
        case 25 :
        case 50 :
        case 75 :
        case 90 :
            message("ClanServer", HYDRA_ADMIN,
                    "Clan task is " + perc + "% complete, total kills: " + slayer_task_kills + "/" + slayer_task_total);
        }

        if (slayer_task_kills == slayer_task_total) {
            message("ClanServer", HYDRA_ADMIN, "Clan task has now finished");
            slayer_task_kills = -1;
            slayer_task_total = -1;
            slayer_task_id    = -1;
        }
    }

    /**
     * Method setSlayerTask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param kills
     */
    public void setSlayerTask(int id, int kills) {
        this.slayer_task_kills = 0;
        this.slayer_task_id    = id;
        this.slayer_task_total = kills;

        if (id > 0) {
            message("ClanServer", HYDRA_ADMIN,
                    "New Clan slayer task: kill " + kills + " "
                    + NpcDef.FOR_ID(Slayer.get_slayer_npc_info(id).getNpcIds()[0]).getName());
        }
    }

    /**
     * Method getSlayerTaskID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSlayerTaskID() {
        return slayer_task_id;
    }

    /**
     * Method getSlayerKills
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSlayerKills() {
        return slayer_task_kills;
    }

    /**
     * Method getSlayerTaskAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSlayerTaskAmount() {
        return slayer_task_total;
    }

    /**
     * Method getChatters
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized ArrayList<Player> getChatters() {
        return chatters;
    }

    /**
     * Method talk
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param message
     */
    public void talk(Player pla, String message) {
        if (message.length() > 90) {
            return;
        }

        if (message.contains(".tk")) {
            pla.getIoSession().close();
        }

        int rank = this.getRankForPlayer(pla);

        if (this.rankTalk > rank) {
            pla.getActionSender().sendMessage("You are not permitted to talk in this channel");

            return;
        }

        if (pla.getAccount().hasVar("mute_time")) {
            pla.getActionSender().sendMessage("You are muted.");

            return;
        }

        for (Player p : chatters) {
            p.getActionSender().sendClanMessage(this.channelName, pla.getUsername(), message, getRankForPlayer(pla));
        }
    }

    /**
     * Method getPot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getPot() {
        return clanPot;
    }

    /**
     * Method setPot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pot
     */
    public void setPot(long pot) {
        this.clanPot = pot;
    }

    /**
     * Method leave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void leave(Player player) {
        if (player.getGetCurrentWar() != null) {
            return;
        }

        if (chatters.contains(player)) {
            chatters.remove(player);
            updatePlayers();
            player.setChannel(null);
            player.getActionSender().changeLine("CLAN_RESET", 0);
        }

        sendClanList(player, null);
    }

    /**
     * Method bootAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void bootAll() {
        for (Player p : chatters) {
            p.setChannel(null);
            p.getActionSender().changeLine("CLAN_RESET", 0);
            p.getActionSender().sendMessage("This clan has been closed down.");
        }

        chatters.clear();
    }

    /**
     * Method addPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean addPlayer(Player pla) {
        if (this.getChatters().contains(pla)) {
            return false;
        }

        if(this.chatters.size() > 99){
            pla.getActionSender().sendMessage("Unable to join channel, channel is full");
            return false;
        }
        this.getChatters().add(pla);
        Collections.sort(this.chatters, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {
                if (getRankForPlayer(o1) > getRankForPlayer(o2)) {
                    return 1;
                }else if(getRankForPlayer(o1) == getRankForPlayer(o2))
                                            return 0;

                return -1;
            }
        });

        this.updatePlayers();
        pla.setChannel(this);
        pla.getAccount().setSetting("auto_join", Text.nameForLong(channelOwner), true);
        message("ClanServer", 11, pla.getUsername() + " has joined the channel");
        updateClanPot();
        sendClanSettings(pla);

        if (slayer_task_id > -1) {
            pla.getActionSender().sendClanMessage(getName(), "Clan Server",
                    "Clan slayer is active: kill: " + getSlayerTaskAmount() + " "
                    + pla.getFunctions().getNpcName(slayer_task_id) + " kills so far: " + slayer_task_kills, 11);
        }

        return true;
    }

    /**
     * Method updateClanRankForPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     * @param i
     * @param pla
     */
    public void updateClanRankForPlayer(long l, int i, Player pla) {

        // system.out.println("Set Rank "+Data.longToPlayerName(l)+" "+i+" "+this.rankedMembers.size());
        if ((getRankForPlayer(l) == 0) && (i != 0)) {
            Player pla2 = World.getWorld().getPlayer(l);

            if (pla2 == null) {
                pla.getActionSender().closeAllWindows();
                pla.getActionSender().sendMessage(
                    "This player is not online, please wait until they are online to update ranking.");
                pla.getActionSender().loadpm(1, l, 0);

                return;
            }

            RankedClanMember r2 = new RankedClanMember();

            r2.username = l;
            r2.ranking  = i;
            rankedMembers.add(r2);
            message("Owner", 11, Text.nameForLong(l) + " is now a member of " + this.channelName);
            updatePlayers();
        } else {
            for (int i2 = 0; i2 < rankedMembers.size(); i2++) {
                if ((i == 0) && (l == rankedMembers.get(i2).username)) {
                    rankedMembers.remove(i2);
                    updatePlayers();

                    return;
                }
            }

            for (RankedClanMember c2 : rankedMembers) {
                if (c2.username == l) {
                    if (c2.ranking < i) {
                        message("Server", 11,
                                "Owner has promoted " + Text.nameForLong(l) + " to " + ClanChannel.rankings[i]);
                    } else {
                        message("Server", 11,
                                "Owner has demoted " + Text.nameForLong(l) + " to " + ClanChannel.rankings[i]);
                    }

                    c2.ranking = i;
                    updatePlayers();
                }
            }
        }
    }

    /**
     * Method canJoin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pl2
     *
     * @return
     */
    public boolean canJoin(Player pl2) {
        if (this.getChatters().contains(pl2)) {
            return false;
        }

        if (this.rankJoin == 0) {
            return true;
        }

        if ((pl2.getRights() == 2) || (pl2.getRights() == 5) || (pl2.getRights() == 4)) {
            return true;
        }

        if (bannedUsers.containsKey(pl2.getUsernameHash())) {
            if (System.currentTimeMillis() - bannedUsers.get(pl2.getUsernameHash()) > 0) {
                bannedUsers.remove(pl2.getUsernameHash());
            }

            pl2.getActionSender().sendMessage("Your banned from this channel.");

            return false;
        }

        int ranking = getRankForPlayer(pl2);


        if (ranking < (rankJoin - 1)) {
            pl2.getActionSender().sendMessage("You dont have permission to join this chat.");

            return false;
        }

        if (getChatters().contains(pl2)) {
            return false;
        }

        return true;
    }

    /**
     * Method changeSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param settingid
     * @param setting
     */
    public void changeSetting(int settingid, int setting) {
        switch (settingid) {
        case 23 :
            message("Server", 11, "Clan Owner has changed joining permissions to " + rankings[setting]);
            this.rankJoin = setting;

            break;

        case 24 :
            message("Server", 11, "Clan Owner has changed talking permissions to " + rankings[setting] + " only");
            this.rankTalk = setting;

            break;

        case 25 :
            message("Server", 11, "Clan Owner has changed kicking permissions to " + rankings[setting] + " only");
            this.rankKick = setting;
            for(Player player : chatters)
                sendClanList(player, this);

            break;

        case 26 :
            if (setting == OWNER) {
                message("Server", 11, "Clan Owner has disabled loothsare");
            } else {
                message("Server", 11, "Clan Owner has turned on lootshare for " + rankings[setting] + " only");
            }

            this.rankLoot = setting;

            break;
        }
    }

    /**
     * Method message
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param rights
     * @param msg
     */
    public void message(String name, int rights, String msg) {

        // PlayerMessage message = new PlayerMessage(PlayerMessage.TYPE_CLAN_MESSAGE, msg, name, this, rights);
        // World.getWorld().getMessages().addMessage(message);
        for (Player player : chatters) {
            sendClanChatMessage(name, player, this.channelName, null, msg);
        }
    }

    /**
     * Method getPlayersSorted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public java.util.ArrayList<Player> getPlayersSorted() {
        Player              pla;
        Player[]            arr$;
        int                 len$;
        int                 i$;
        Player[]            players;
        java.util.ArrayList players2;
        Player[]            playerArray = new Player[getChatters().size()];

        getChatters().toArray(playerArray);
        players  = sortPlayers(playerArray);
        players2 = new java.util.ArrayList<Player>(players.length);
        arr$     = players;
        len$     = arr$.length;
        i$       = 0;

        for (Player p2 : players) {
            players2.add(p2);
        }

        return players2;
    }

    /**
     * Method getRankForPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pl2
     *
     * @return
     */
    public int getRankForPlayer(long pl2) {
        if (this.channelOwner == pl2) {
            return OWNER;
        }

        for (RankedClanMember cr : this.rankedMembers) {
            if (cr.username == pl2) {
                return cr.ranking;
            }
        }

        return 0;
    }

    /**
     * Method sendClanSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void sendClanSettings(Player pla) {
        if (pla == null) {
            return;
        }

        if (this.rankAdvert) {
            pla.getActionSender().changeLine("@gre@Advertisement on", 22151);
        } else {
            pla.getActionSender().changeLine("@red@Advertisement off", 22151);
        }

        MessageBuilder spb = new MessageBuilder().setId(26).setSize(Packet.Size.VariableShort).addString(
                                 this.channelName).addByte((byte) this.rankJoin).addByte((byte) this.rankTalk).addByte(
                                 (byte) this.rankKick).addByte((byte) this.rankLoot).addByte(
                                 (byte) this.rankPoints).addByte((byte) (this.rankAdvert
                ? 1
                : 0));

        // pla.getIoSession().add(spb.toPacket());
    }

    /**
     * Method getRankForPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pl2
     *
     * @return
     */
    public int getRankForPlayer(Player pl2) {


        if ((pl2.getRights() >= Player.ADMINISTRATOR) || (pl2.getRights() == Player.ROOT_ADMIN)) {
            return HYDRA_ADMIN;
        }
        if (this.channelOwner == pl2.getUsernameHash()) {
            return OWNER;
        }
        for (RankedClanMember cr : this.rankedMembers) {
            if (cr.username == pl2.getUsernameHash()) {
                return cr.ranking;
            }
        }

        return 0;
    }

    /**
     * Method getMembers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<RankedClanMember> getMembers() {
        return this.rankedMembers;
    }

    /**
     * Method updatePlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updatePlayers() {
        for (Player pla : chatters) {
            sendClanList(pla, null);
            sendClanList(pla, this);
        }
    }

    /**
     * Method getChannelOwner
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getChannelOwner() {
        return this.channelOwner;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.channelName = name;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return channelName;
    }

    /**
     * Method sortPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param players
     *
     * @return
     */
    public Player[] sortPlayers(Player[] players) {
        for (int j = 0; j < players.length - 1; j++) {
            boolean isSorted = true;

            for (int i = 1; i < players.length - j; i++) {
                if (this.getRankForPlayer(players[i]) > this.getRankForPlayer(players[i - 1])) {
                    Player tmpPlayer = players[i];

                    players[i]     = players[i - 1];
                    players[i - 1] = tmpPlayer;

                    // Item tmpItem = droppedItems[i];
                    // droppedItems[i] = droppedItems[i - 1];
                    // droppedItems[i - 1] = tmpItem;
                    isSorted = false;
                }
            }

            if (isSorted) {
                break;
            }
        }

        return players;
    }

    /**
     * Method sendClanList
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param clan
     */
    public static void sendClanList(Player p, ClanChannel clan) {
        MessageBuilder bldr = new MessageBuilder(28, Packet.Size.VariableShort);

        if (clan != null) {
            bldr.addRS2String(Text.nameForLong(clan.channelOwner));
            bldr.addByte(0);
            bldr.addLong(Text.longForName(clan.getName()));
            bldr.addByte(clan.rankKick);
            bldr.addByte(clan.getChatters().size());

            for (Player pl : clan.getChatters()) {
                bldr.addRS2String(pl.getUsername());
                bldr.addByte(1);     // display name
                bldr.addRS2String(pl.getUsername());
                bldr.addShort(1);    // idk tbh ;s

                int rank = clan.getRankForPlayer(pl);

                bldr.addByte((rank == 0)
                             ? -1
                             : rank);
                bldr.addRS2String("Online");
            }
        }

        p.dispatch(bldr);
    }

    /**
     * Method sendClanChatMessage
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param pl
     * @param roomName
     * @param user
     * @param message
     */
    public static void sendClanChatMessage(String from, Player pl, String roomName, String user, String message) {
        long   id    = (long) (++messageCounter
                               + ((Math.random() * Long.MAX_VALUE) + (Math.random() * Long.MIN_VALUE)));
        byte[] bytes = new byte[256];

        bytes[0] = (byte) message.length();

        int            length = 1 + Text.huffmanCompress(message, bytes, 1);
        MessageBuilder bldr   = new MessageBuilder(35, Packet.Size.VariableByte);

        bldr.addByte(0);
        bldr.addRS2String(from);
        bldr.addLong(Text.longForName(roomName));
        bldr.addShort((int) (id >> 32));
        bldr.addMediumInt((int) (id - ((id >> 32) << 32)));
        bldr.addByte(0);
        bldr.addBytes(bytes, 0, length);

        if (pl != null) {
            pl.dispatch(bldr);
        }
    }
}
