package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRequest;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.skill.construction.costumeroom.RoomBox;
import net.tazogaming.hydra.game.ui.shops.Shop;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.game.ui.shops.SubShop;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 00:49
 * To change this template use File | Settings | File Templates.
 */
public class WindowManager {
    private static GameInterface[] interfaceCache = new GameInterface[46492];
    public static final int
        ENTER_AMOUNT                              = 0,
        ENTER_NAME                                = 1;
    private static GameInterface blankInterface   = new GameInterface(0) {
        @Override
        public void onButtonClick(int buttonId, Player pla) {

            // To change body of implemented methods use File | Settings | File Templates.
        }
        @Override
        public void onOpen(Player pla) {

            // To change body of implemented methods use File | Settings | File Templates.
        }
        @Override
        public void onClose(Player pla) {

            // To change body of implemented methods use File | Settings | File Templates.
        }
        @Override
        public void moveItems(int i, int b, int c, Player pla) {}
        public void onItemClick(int i, int i2, int itemSlot, int clickSlot, Player pla) {

            // To change body of implemented methods use File | Settings | File Templates.
        }
        @Override
        public void tick(Player pla) {

            // To change body of implemented methods use File | Settings | File Templates.
        }
    };
    public static final int
        KNOWN_RUN_ENERGY           = 0,
        KNOWN_WEIGHT               = 1,
        KNOWN_PLAYER_COUNT         = 2,
        KNOWN_PRAYER               = 3,
        KNOWN_WILD                 = 4,
        KNOWN_SUMMON_PTS           = 5,
        KNOWN_SUMMON_TIME          = 8,
        KNOWN_SUMMON_SPEC          = 9,
        KNOWN_CURSE_BOOST_ATTACK   = 10,
        KNOWN_CURSE_BOOST_STRENGTH = 11,
        KNOWN_CURSE_BOOST_DEFENCE  = 12,
        KNOWN_CURSE_BOOST_RANGE    = 13,
        KNOWN_CURSE_BOOST_MAGIC    = 14,
        KNOWN_LOBBY_TIMER_SECS     = 15,
        KNOWN_MAX_HIT              = 16;

    static {}

    private ConstructionInterfaceSettings constSettings                 = null;
    private int[]                         knownVariables                = new int[200];
    private int                           currentWindow                 = -1;
    private int                           currentDialog                 = -1;
    private int                           currentInteractingInterface   = -1;
    private int[]                         currentSidebars               = new int[16];
    private int                           inventorySidebar              = -1;
    private int                           currentOverlay                = -1;
    private int                           currentAmountEnteredInterface = -1;
    private int                           amountSlot                    = -1;
    private ClanWarRequest                clanRequest;
    private Trade                         trade;
    private Duel                          duel;
    private Dialog                        currentChatDialog;
    private SkillGuide                    curSkillGuide;
    private Action                        requestedAction;
    private AmountTrigger                 amountWaitingInterface;
    private Shop                          currentShop;
    private Shop                          currentSubShop;
    private RoomBox                       curCostumeRoomInterface;
    private Player                        player;
    private TextInputRequest              textRequest;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    public WindowManager(Player player) {
        this.player = player;

        for (int i = 0; i < knownVariables.length; i++) {
            knownVariables[i] = -2;
        }
    }

    /**
     * Method getConstructionInterSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ConstructionInterfaceSettings getConstructionInterSettings() {
        return constSettings;
    }

    /**
     * Method setConstSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     */
    public void setConstSettings(ConstructionInterfaceSettings k) {
        constSettings = k;
    }

    /**
     * Method setClanRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param request
     */
    public void setClanRequest(ClanWarRequest request) {
        this.clanRequest = request;
    }

    /**
     * Method setCostume
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ii
     */
    public void setCostume(RoomBox ii) {
        curCostumeRoomInterface = ii;
    }

    /**
     * Method getCostumeInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RoomBox getCostumeInterface() {
        return curCostumeRoomInterface;
    }

    /**
     * Method setTextRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param request
     */
    public void setTextRequest(TextInputRequest request) {
        this.textRequest = request;
    }

    /**
     * Method getTextRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public TextInputRequest getTextRequest() {
        return this.textRequest;
    }

    /**
     * Method bobInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void bobInterface() {
        if (player.getAccount().getInt32("bob_max") > 0) {
            player.getWindowManager().inventoryAndWindow(4465, 2005);
        } else {
            player.getActionSender().sendMessage("This familiar is not a beast of burden.");
        }
    }

    /**
     * Method getDuel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Duel getDuel() {
        return duel;
    }

    /**
     * Method setDuel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param duel
     */
    public void setDuel(Duel duel) {
        this.duel = duel;
    }

    /**
     * Method getCurSkillGuide
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SkillGuide getCurSkillGuide() {
        return curSkillGuide;
    }

    /**
     * Method setCurSkillGuide
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param curSkillGuide
     */
    public void setCurSkillGuide(SkillGuide curSkillGuide) {
        this.curSkillGuide = curSkillGuide;
    }

    /**
     * Method getRequestedAction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Action getRequestedAction() {
        return requestedAction;
    }

    /**
     * Method makeItemInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param items
     */
    public void makeItemInterface(int... items) {
        this.closeWindow();
        this.currentDialog = 8880;

        for (int i = 0; i < items.length; i++) {
            int ind    = 8883 + i;
            int txtInd = 8889 + i;

            if (i == 1) {
                txtInd = 8893;
            } else if (i == 2) {
                txtInd = 8897;
            }

            if (items[i] != -1) {

                // player.getActionSender().hideInter(ind, 0);
                player.getActionSender().sendImage(ind, 200, items[i]);
                player.getActionSender().changeLine(Item.forId(items[i]).getName(), txtInd);
            } else {
                player.getActionSender().sendImage(ind, 200, -1);
                player.getActionSender().changeLine("", txtInd);
            }
        }

        getInterface(currentDialog).onOpen(player);
        player.getActionSender().sendDialog(currentDialog);
    }

    /**
     * Method setRequestedAction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param requestedAction
     */
    public void setRequestedAction(Action requestedAction) {
        this.requestedAction = requestedAction;
    }

    /**
     * Method showDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     */
    public void showDialog(Dialog d) {
        d.draw(player);
        currentChatDialog = d;
    }

    /**
     * Method getDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Dialog getDialog() {
        return currentChatDialog;
    }

    /**
     * Method getCurrentDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentDialog() {
        return currentDialog;
    }

    /**
     * Method isTrading
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTrading() {
        return trade != null;
    }

    /**
     * Method getTrade
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Trade getTrade() {
        return trade;
    }

    /**
     * Method setTrade
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void setTrade(Trade t) {
        this.trade = t;
    }

    /**
     * Method resetKnownVariables
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetKnownVariables() {
        for (int i = 0; i < knownVariables.length; i++) {
            knownVariables[i] = -2;
        }
    }

    /**
     * Method hasWindowOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasWindowOpen() {
        return (currentWindow != -1) || (currentDialog != -1);
    }

    /**
     * Method showWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param window
     */
    public void showWindow(int window) {
        if (currentWindow == window) {
            return;
        }

        closeWindow();
        this.currentWindow = window;
        getInterface(currentWindow).onOpen(player);
        player.getActionSender().showInterface(window);
        player.getRoute().resetPath();
        player.terminateActions();
        player.getActionSender().sendCloseWalkingFlag();
    }

    /**
     * Method showOverlay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param window
     */
    public void showOverlay(int window) {
        if (currentOverlay == window) {
            return;
        }

        if (getCurrentOverlay() != null) {
            getCurrentOverlay().onClose(player);
        }

        this.currentOverlay = window;
        getInterface(currentOverlay).onOpen(player);
        player.getActionSender().overlay(window);
    }

    /**
     * Method closeOverlay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void closeOverlay() {
        if (currentOverlay != -1) {
            getCurrentOverlay().onClose(player);
        }

        currentOverlay = -1;
        player.getActionSender().overlay(currentOverlay);
    }

    /**
     * Method getKnownVariable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getKnownVariable(int id) {
        return knownVariables[id];
    }

    /**
     * Method setKnownVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param var
     */
    public void setKnownVar(int id, int var) {
        knownVariables[id] = var;
    }

    /**
     * Method closeWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void closeWindow() {
        closeWindow(false);

        if (player.getGameFrame() != null) {
            player.getGameFrame().close();
        }
    }

    /**
     * Method getClanWarRequest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ClanWarRequest getClanWarRequest() {
        return clanRequest;
    }

    /**
     * Method closeWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ignoreDialogs
     */
    public void closeWindow(boolean ignoreDialogs) {
        try {
            if (this.currentWindow != -1) {
                getInterface(this.currentWindow).onClose(player);
            }

            if (!ignoreDialogs && (this.currentDialog != -1)) {
                getInterface(this.currentDialog).onClose(player);
            }

            this.currentWindow = -1;

            if (!ignoreDialogs) {
                this.currentDialog = -1;
            }

            if (this.inventorySidebar != -1) {
                getInterface(this.inventorySidebar).onClose(player);
                this.inventorySidebar = -1;
                getInterface(currentSidebars[4]).onOpen(player);
            }

            if (this.currentInteractingInterface != -1) {
                getInterface(currentInteractingInterface).onClose(player);
                this.currentInteractingInterface = -1;
            }

            amountWaitingInterface = null;
            textRequest            = null;

            if (!ignoreDialogs) {
                currentChatDialog = null;
            }

            player.getActionSender().closeAllWindows();
        } catch (Exception ee) {
            ee.printStackTrace();
            player.getIoSession().close();
        }
    }

    /**
     * Method changeWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void changeWindow(int id) {
        this.currentInteractingInterface = id;
        getInterface(currentInteractingInterface).onOpen(player);
        player.getActionSender().sendBagInterface(id, inventorySidebar);
    }

    /**
     * Method getCurrentOverlayID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentOverlayID() {
        return currentOverlay;
    }

    /**
     * Method inventoryAndWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param windowId
     * @param interfaceId
     */
    public void inventoryAndWindow(int windowId, int interfaceId) {
        closeWindow();
        inventorySidebar = interfaceId;
        getInterface(inventorySidebar).onOpen(player);
        currentInteractingInterface = windowId;
        getInterface(currentInteractingInterface).onOpen(player);
        player.getActionSender().sendBagInterface(windowId, interfaceId);
    }

    /**
     * Method updateWindows
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateWindows() {
        if (inventorySidebar != -1) {
            getInterface(inventorySidebar).tick(player);
        }

        if (currentWindow != -1) {
            getInterface(currentWindow).tick(player);
        }

        if (currentDialog != -1) {
            getInterface(currentDialog).tick(player);
        }

        if (currentInteractingInterface != -1) {
            getInterface(currentInteractingInterface).tick(player);
        }

        if (currentOverlay != -1) {
            getInterface(currentOverlay).tick(player);
        }

        for (int i = 0; i < currentSidebars.length; i++) {
            if (currentSidebars[i] != -1) {
                getInterface(currentSidebars[i]).tick(player);
            }
        }
    }

    /**
     * Method openShop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void openShop(int id) {
        Shop shop = ShopManager.getShop(id);

        if (shop == null) {
            return;
        }

        closeWindow();
        currentSubShop = null;
        currentShop    = shop;
        inventoryAndWindow(3824, 3822);
    }

    /**
     * Method getCurrentInventoryInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentInventoryInterface() {
        if (inventorySidebar != -1) {
            return getInterface(inventorySidebar).getSubInterfaceId(0);
        }

        return getInterface(currentSidebars[4]).getSubInterfaceId(0);
    }

    /**
     * Method registerActionButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     */
    public void registerActionButton(int button) {
        if (currentWindow != -1) {
            getInterface(currentWindow).onButtonClick(button, player);
        }

        if (currentDialog != -1) {
            getInterface(currentDialog).onButtonClick(button, player);
        }

        if (currentInteractingInterface != -1) {
            getInterface(currentInteractingInterface).onButtonClick(button, player);
        }

        for (int i = 0; i < currentSidebars.length; i++) {
            getInterface(currentSidebars[i]).onButtonClick(button, player);
        }
    }

    /**
     * Method getWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public GameInterface getWindow(int id) {
        for (int i = 0; i < currentSidebars.length; i++) {
            if (currentSidebars[i] == id) {
                return getInterface(currentSidebars[i]);
            }
        }

        if (currentDialog == id) {
            return getInterface(currentDialog);
        }

        if (inventorySidebar == id) {
            return getInterface(inventorySidebar);
        }

        if (currentInteractingInterface == id) {
            return getInterface(currentInteractingInterface);
        }

        return null;
    }

    /**
     * Method getCurrentOverlay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public GameInterface getCurrentOverlay() {
        if (currentOverlay == -1) {
            return null;
        }

        return getInterface(currentOverlay);
    }

    /**
     * Method getSidebar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public GameInterface getSidebar(int id) {
        return getInterface(currentSidebars[id]);
    }

    /**
     * Method getBySubInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public GameInterface getBySubInterface(int id) {
        for (int i = 0; i < currentSidebars.length; i++) {
            if (getInterface(currentSidebars[i]).hasSub(id)) {
                return getInterface(currentSidebars[i]);
            }
        }

        if (getInterface(currentInteractingInterface).hasSub(id)) {
            return getInterface(currentInteractingInterface);
        }

        if (getInterface(currentWindow).hasSub(id)) {
            return getInterface(currentWindow);
        }

        if (getInterface(currentDialog).hasSub(id)) {
            return getInterface(currentDialog);
        }

        if (getInterface(inventorySidebar).hasSub(id)) {
            return getInterface(inventorySidebar);
        }

        return null;
    }

    /**
     * Method disable_sidebar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void disable_sidebar() {
        int[][] sideBarInterfaces = {
            {
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
            }, {
                5855, 3917, 31000, 20638, 3213, 1644, 5608, 1151, 5065, 18128, 6299, 904, 147, 19940, 46491, 17011
            }
        };

        for (int i = 0; i < sideBarInterfaces[0].length; i++) {
            player.getActionSender().sendSideBarInterface(sideBarInterfaces[0][i], -1);
        }
    }

    /**
     * Method enable_sidebar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void enable_sidebar() {
        int[][] sideBarInterfaces = {
            {
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
            }, {
                5855, 3917, 31000, 638, 3213, 1644, 5608, 1151, 5065, 18128, 6299, 904, 147, 19940, 46491, 17011
            }
        };

        for (int i = 0; i < sideBarInterfaces[0].length; i++) {
            player.getActionSender().sendSideBarInterface(sideBarInterfaces[0][i], currentSidebars[i]);
        }
    }

    /**
     * Method flash_enable_sidebar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void flash_enable_sidebar(int id) {
        player.getActionSender().flashSidebar(id);
        player.getActionSender().sendSideBarInterface(id, currentSidebars[id]);
    }

    /**
     * Method loadSidebars
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void loadSidebars() {
        int[][] sideBarInterfaces = {
            {
                0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15
            }, {
                5855, 3917, 31000, 638, 3213, 1644, 5608, 1151, 5065, 18128, 6299, 904, 147, 19940, 46491, 17011
            }
        };

        if (player.getEquipment().getId(3) != -1) {
            sideBarInterfaces[1][0] = Equipment.getInterfaceIdForWeapon(player.getEquipment().getId(3),
                    Item.forId(player.getEquipment().getId(3)).getName());
        }

        if (player.getAccount().hasVar("prayer_book") && (player.getAccount().getInt32("prayer_book") == 1)) {
            sideBarInterfaces[1][6] = 22500;
        }

        if (player.getAccount().hasVar("spellbook_type") && (player.getAccount().getInt32("spellbook_type") == 1)) {
            sideBarInterfaces[1][7] = 12855;
        }

        for (int i = 0; i < sideBarInterfaces[0].length; i++) {
            player.getActionSender().sendSideBarInterface(sideBarInterfaces[0][i], sideBarInterfaces[1][i]);
            currentSidebars[sideBarInterfaces[0][i]] = sideBarInterfaces[1][i];
            getInterface(currentSidebars[sideBarInterfaces[0][i]]).onOpen(player);
        }
    }

    /**
     * Method setSidebar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ing
     * @param id
     */
    public void setSidebar(int ing, int id) {
        getInterface(currentSidebars[ing]).onClose(player);
        currentSidebars[ing] = id;
        getInterface(currentSidebars[ing]).onOpen(player);
        player.getActionSender().sendSideBarInterface(ing, id);
    }

    /**
     * Method getInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static GameInterface getInterface(int id) {
        if (id == -1) {
            return blankInterface;
        }

        if (interfaceCache[id] != null) {
            return interfaceCache[id];
        }

        return blankInterface;
    }

    /**
     * Method setSubShopIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ind
     */
    public void setSubShopIndex(int ind) {
        if (ind == -1) {
            currentSubShop = null;

            return;
        }

        SubShop s       = currentShop.shops.get(ind);    // .id;
        Shop    newShop = ShopManager.getShop(s.id);

        if (newShop != null) {
            currentSubShop = newShop;
        }
    }

    /**
     * Method requestAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemSlot
     * @param amountTrigger
     */
    public void requestAmount(int itemSlot, AmountTrigger amountTrigger) {
        this.amountSlot        = itemSlot;
        amountWaitingInterface = amountTrigger;
        player.getActionSender().requestAmount();
    }

    /**
     * Method amountEntered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     *
     * @return
     */
    public boolean amountEntered(int amount) {
        if (amountWaitingInterface == null) {
            return false;
        }

        amountWaitingInterface.amountEntered(player, amountSlot, amount);
        amountWaitingInterface = null;

        return true;
    }

    /**
     * Method sendDialog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void sendDialog(int id) {
        closeWindow();
        currentDialog = id;
        getInterface(currentDialog).onOpen(player);
        player.getActionSender().sendDialog(id);
    }

    /**
     * Method getCurrentWindow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentWindow() {
        return currentWindow;
    }
}
