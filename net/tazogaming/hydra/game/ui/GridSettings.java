package net.tazogaming.hydra.game.ui;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/10/13
 * Time: 20:11
 */
import net.tazogaming.hydra.entity3d.item.Item;

/**
 * Created with IntelliJ IDEA.
 * User: Danny
 * Date: 31/01/13
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */
public class GridSettings {
    public int    tabAmount  = 0;
    public int[]  rowsInTabs = new int[10];
    public Item[] items      = new Item[350];
    public int[]  itemsCount = new int[350];
    public int[]  tabIds     = new int[350];
    public int[]  tabSlots   = new int[350];
    public int[]  firstSlots = new int[10];
}
