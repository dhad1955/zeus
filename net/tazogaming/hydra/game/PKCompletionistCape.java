package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueEntry;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueTable;
import net.tazogaming.hydra.game.minigame.pkraffle.PKRaffle;
import net.tazogaming.hydra.game.questing.Quest;
import net.tazogaming.hydra.game.skill.construction.RoomConfig;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/12/2014
 * Time: 04:15
 */
public class PKCompletionistCape {
    public static PkRequirement[] requirements;

    static {
        requirements = new PkRequirement[]
                {
                        new PkRequirement("Have atleast 500 kills") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.getAccount().hasVar("kills") && player.getAccount().getVar("kills").getIntData() >=500;
                            }
                        },

                        new PkRequirement("Have 50 wilderness worm kills") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.getKillCounts().containsKey(3334) && player.getKillCounts().get(3334) >= 50;
                            }
                        },

                        new PkRequirement("Complete achievement \"daily winner\"") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.achievementComplete(26);
                            }
                        },

                        new PkRequirement("Complete achievement 'Failure'") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.achievementComplete(Achievement.FAILURE);
                            }

                        },

                        new PkRequirement("Complete achievement 'killing frenzy'") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.achievementComplete(Achievement.KILLING_FRENZY);
                            }
                        },
                        new PkRequirement("Complete achievement - Vengeance") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.achievementComplete(Achievement.VENGEANCE);
                            }
                        },
                        new PkRequirement("Complete achievement 'Revoloution'") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.achievementComplete(Achievement.REVOLUTION);
                            }
                        },
                        new PkRequirement("Kill 100 targets in the wild") {
                            @Override
                            boolean hasCompleted(Player player) {
                                return player.getAccount().hasVar("bh_kills") && player.getAccount().getInt32("bh_kills") >= 100;
                            }
                        }
                };
    }


    /**
     * Method getMenu
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static List<String> getMenu(Player player) {
        List<String> lines = new ArrayList<String>();

        for (PkRequirement req : requirements) {
            if (req.hasCompleted(player)) {
                lines.add("<str>" + req.getName());
            } else {
                lines.add(req.getName());
            }
        }

        return lines;
    }

    /**
     * Method showStats
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param data
     */
    public static void showStats(Player player, List<String> data) {
        player.getGameFrame().openWindow(275);
        player.getGameFrame().sendString("PK Master Cape requirements", 275, 2);

        int cur = 16;

        for (String req : data) {
            player.getGameFrame().sendString(req, 275, cur++);
        }

        for (; cur < 100; cur++) {
            player.getGameFrame().sendString("", 275, cur);
        }
    }

    /**
     * Method canWear
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean canWear(Player player) {
        for (PkRequirement req : requirements) {
            if (!req.hasCompleted(player)) {
                return false;
            }
        }

        return true;
    }


    public static int getCompletionPercent(Player player){
        int completed = getTotalCompleted(player);

        return (int)GameMath.getPercentFromTotal(completed, requirements.length);
    }

    public static int getTotalCompleted(Player player){
        int completed = 0;
        for(PkRequirement requirement : requirements) {
            if(requirement.hasCompleted(player))
                completed ++;
        }
        return completed;
    }

    private static boolean ensureBank(Player player, int... items) {
        for (Integer i : items) {
            if (!player.getBank().hasItem(i, 1)) {
                return false;
            }
        }

        return true;
    }
}


/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/12/06
 * @author         Daniel Hadland
 */
abstract class PkRequirement {

    /** name made: 14/12/06 */
    private String name;

    PkRequirement(String name) {
        this.name = name;
    }

    /**
     * Method getName
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    String getName() {
        return name;
    }

    /**
     * Method hasCompleted
     * Created on 14/12/06
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    abstract boolean hasCompleted(Player player);
}
