package net.tazogaming.hydra.game.questing;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.cfg.Definition;
import net.tazogaming.hydra.util.cfg.DefinitionConfig;
import net.tazogaming.hydra.util.cfg.DefinitionGroup;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/10/2014
 * Time: 09:51
 */
public class Quest {

    /** quests made: 14/11/01 */
    private static final Map<Integer, Quest> quests = new HashMap<Integer, Quest>();

    public String[] getRewardInformation() {
        return rewardInformation;
    }

    public void setRewardInformation(String[] rewardInformation) {
        this.rewardInformation = rewardInformation;
    }

    public QuestStage[] getStages() {
        return stages;
    }

    public void setStages(QuestStage[] stages) {
        this.stages = stages;
    }

    public String getQuestName() {
        return questName;
    }

    public void setQuestName(String questName) {
        this.questName = questName;
    }

    public int getQuestId() {
        return questId;
    }

    public void setQuestId(int questId) {
        this.questId = questId;
    }

    public String getStartPointInfo() {
        return startPointInfo;
    }

    public void setStartPointInfo(String startPointInfo) {
        this.startPointInfo = startPointInfo;
    }

    /** stages made: 14/11/01 */
    private QuestStage[] stages;

    /** questName made: 14/11/01 */
    private String questName;

    /** questId made: 14/11/01 */
    private int questId;

    /** requirementsString made: 14/11/01 */
    private String startPointInfo;

    /** rewardInformation made: 14/11/01 */
    private String[] rewardInformation;

    private boolean notInUse = false;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param name
     * @param stages
     */
    public Quest(int id, String name, QuestStage[] stages) {
        this.questId   = id;
        this.questName = name;
        this.stages    = stages;
    }

    /**
     * Method getQuest
     * Created on 14/11/01
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static Quest getQuest(int id) {
        return quests.get(id);
    }

    /**
     * Method loadQuests
     * Created on 14/11/01
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param developer
     *
     * @throws IOException
     */
    public static void loadQuests(Player developer) throws IOException {
        quests.clear();

        BufferedReader   reader            = new BufferedReader(new FileReader(new File("config/Quests.cfg")));
        String           line              = null;
        int              lineNo            = 0;
        int              curQuestID        = -1;
        String           questName         = "";
        List<QuestStage> stages            = new ArrayList<QuestStage>();
        List<String>     rewardInformation = new ArrayList<String>();
        String           startPoint        = "Unknown";

        boolean notInUse = false;
        System.err.println("Loading Quests.");
        try {
            while ((line = reader.readLine()) != null) {
                line = line.trim();
                lineNo++;

                if (line.startsWith("Quest")) {
                    if (curQuestID != -1) {
                        throw new RuntimeException("Error already parsing a quest at line " + lineNo);
                    }

                    if (quests.containsKey(curQuestID)) {
                        throw new RuntimeException("Error quest " + curQuestID + " is already defined " + lineNo);
                    }

                    curQuestID = Integer.parseInt(line.split(" ")[1]);
                }

                if (line.startsWith("stage")) {
                    String[] split   = line.split(" ", 5);
                    int      stageId = Integer.parseInt(split[2]);
                    boolean  hidden  = Boolean.parseBoolean(split[3]);
                    String   info    = split[4];

                    stages.add(new QuestStage(info, hidden));
                } else if (line.startsWith("name")) {
                    questName = line.split(" ", 3)[2];
                }

                if(line.startsWith("in_use = false"))
                    notInUse = true;

                if (line.startsWith("startpoint")) {
                    startPoint = line.substring(12);
                } else if (line.startsWith("reward")) {
                    rewardInformation.add(line.substring(8));
                }

                if (line.startsWith("}")) {
                    QuestStage[] tmp = new QuestStage[stages.size()];

                    stages.toArray(tmp);
                    stages.clear();

                    Quest quest = new Quest(curQuestID, questName, tmp);

                    if (rewardInformation.size() > 0) {
                        String[] rwrds = new String[rewardInformation.size()];

                        rewardInformation.toArray(rwrds);
                        quest.rewardInformation = rwrds;
                    }

                    quest.startPointInfo = startPoint;

                    if (quests.containsKey(curQuestID)) {
                        throw new RuntimeException("Error Duplicate quest added " + curQuestID);
                    }

                    System.err.println("[Quest Loaded]: "+questName+" idx: "+curQuestID+" "+notInUse);

                    quest.notInUse = notInUse;
                    notInUse = false;
                    quests.put(curQuestID, quest);
                    curQuestID = -1;

                }
            }
        } catch (Exception ee) {
            ee.printStackTrace();
            if (developer != null) {
                developer.getActionSender().sendMessage(Text.RED("[FATAL]: Error loading quest " + curQuestID
                        + " parse error line " + lineNo));
            }
        }
    }

    public boolean isInUse() {
        return !notInUse;
    }
    public static Map<Integer, Quest> getQuests() {
        return quests;
    }


    /**
     * Method buildMenu
     * Created on 14/11/01
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param quest
     *
     * @return
     */

    static public final void showJournal(Player player, Quest quest) {
        player.getGameFrame().openWindow(275);
        GameFrame gameFrame = player.getGameFrame();
        gameFrame.sendString(quest.getQuestName(), 275,2);

        player.getIoSession().setMaxOutput(100000000);
        String[] journalInfo = buildMenu(player, quest);
        int i = 1;
        for(; i < journalInfo.length + 1; i++) {
            gameFrame.sendString(journalInfo[ i - 1], 275, 16 + i - 1);
        }
        while (i < 100) {
            gameFrame.sendString("", 275, 16 + i - 1);
            i++;
        }

        player.getIoSession().setMaxOutput(29000);


    }
    static private final String[] buildMenu(Player player, Quest quest) {
        List<String> tab = new ArrayList<String>();

        tab.add(Text.BLUE(quest.questName));
        tab.add("");

        if (!player.questStarted(quest.questId)) {
            tab.add("You can start this quest by");
            tab.add(quest.startPointInfo);
        } else {
            if (player.quest_completed(quest.questId)) {
                tab.add(Text.GREEN("You have finished this quest."));
                tab.add("");
            } else {
                tab.add("You have started this quest");
                tab.add("");
            }

            for (int i = 0; i < quest.stages.length; i++) {
                if(player.getCurStage(quest.getQuestId()) < i)
                    continue;

                if (quest.stages[i].isHidden()) {
                    continue;
                }

                if (player.quest_stage_completed(quest.questId, i)) {
                    tab.add("<str>" + quest.stages[i].getObjective());
                } else {
                    tab.add(quest.stages[i].getObjective());
                }
            }
        }

        String[] arr = new String[tab.size()];

        tab.toArray(arr);

        return arr;
    }

    /**
     * Method length
     * Created on 14/11/01
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        return stages.length;
    }
}
