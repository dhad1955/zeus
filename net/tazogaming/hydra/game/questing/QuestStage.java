package net.tazogaming.hydra.game.questing;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/10/2014
 * Time: 09:47
 */
public class QuestStage {


    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public boolean isHidden() {
        return isHidden;
    }

    public void setHidden(boolean isHidden) {
        this.isHidden = isHidden;
    }

    private String objective;

    public QuestStage(String objective, boolean isHidden) {
        this.objective = objective;
        this.isHidden = isHidden;
    }

    private boolean isHidden;
}
