package net.tazogaming.hydra.game.minigame.fp;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.minigame.util.ilobby.Lobby;
import net.tazogaming.hydra.game.minigame.util.ilobby.LobbyListener;
import net.tazogaming.hydra.game.minigame.util.ilobby.LobbyMode;
import net.tazogaming.hydra.game.minigame.util.ilobby.LobbyUIUpdater;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/01/14
 * Time: 02:47
 */
public class FightPitsGame implements RecurringTickEvent, LobbyListener {
    private static final Point RANDOM_POINTS[] = {
        Point.location(2397, 5163, 0), Point.location(2403, 5159, 0), Point.location(2411, 5152, 0),
        Point.location(2400, 5140, 0), Point.location(2390, 5141, 0), Point.location(2383, 5150, 0)
    };
    public static final int
        GAME_STATE_WAITING         = 1,
        GAME_STATE_IN_GAME         = 2,
        GAME_STATE_GAME_STARTING   = 3;
    private static Zone LOBBY_Zone = new Zone(2393, 5169, 2405, 5175, -1, true, true,
                                         "fp_lobby").setStatic(true).setBankingAllowed(true).setTeleportAllowed(true);
    private static FightPitsGame currentGame     = new FightPitsGame();
    public static final Zone FIGHT_PITS_ZONE =
        new Zone(2380, 5129, 2423, 5167, -1, true, true,
                 "fight_pits").setBankingAllowed(false).setStatic(true).setTeleportAllowed(false);
    private int               gameState   = GAME_STATE_WAITING;
    private int               playerTotal = 0;
    private int               timer       = 40;
    private ArrayList<Player> players     = new ArrayList<Player>();
    private Player            previous    = null;
    private Lobby             lobby;

    /**
     * Constructs ...
     *
     */
    public FightPitsGame() {
        lobby = new Lobby(LOBBY_Zone);
        lobby.setMode(LobbyMode.TIMER_MODE);
        lobby.setListener(this);
        lobby.setCenterPoint(Point.location(2400, 5173, 0));
        lobby.setMinimumPlayers(2);
        lobby.setMaxPlayers(200);
        lobby.setCountdown(10);
        lobby.setUI(new LobbyUIUpdater() {
            @Override
            public void updateUI(Player pla, Lobby lob) {
                pla.getActionSender().changeLine("", 2806);

                if (gameState == GAME_STATE_IN_GAME) {
                    pla.getActionSender().changeLine("Waiting for round to finish", 2805);

                    return;
                }

                if (lob.getSecondsTillStart() <= 0) {
                    pla.getActionSender().changeLine("Need " + (lob.getMinimumPlayers() - lob.getPlayers().size())
                                                     + " more players to start", 2805);
                } else {
                    pla.getActionSender().changeLine("Game starts in: " + lob.getSecondsTillStart() + " sec", 2805);
                }
            }
            @Override
            public void playerEntered(Player pla, Lobby l) {
                pla.getWindowManager().showOverlay(2804);

                // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void playerLeft(Player pla, Lobby l) {
                if (!pla.isInArea(FIGHT_PITS_ZONE)) {
                    pla.getWindowManager().showOverlay(-1);
                }

                // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void playerConnectionLost(Player pla, Lobby l) {

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        Core.submitTask(this);
    }

    /**
     * Method enter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void enter(Player player) {
        currentGame.lobby.addPlayer(player);
    }

    /**
     * Method isPlaying
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean isPlaying(Player player) {
        if (currentGame.gameState == GAME_STATE_WAITING) {
            return false;
        }

        if (!currentGame.players.contains(player)) {
            return false;
        }

        if (currentGame.gameState == GAME_STATE_GAME_STARTING) {
            return false;
        }

        return true;
    }

    /**
     * Method checkGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void checkGame() {
        if (players.size() == 1) {
            Player winner = players.get(0);

            winner.getActionSender().sendMessage("Congratulations you have won fight pits, and survived: "
                    + playerTotal + " players.");

            int total = 500 + (winner.getAccount().getInt32("fpkc")) * (playerTotal);

            winner.getInventory().addItem(6529, total);
            winner.getActionSender().sendMessage("You have earned " + total + " tokkul");
            lobby.addPlayer(winner);
            winner.setCurDeathHandler(new DefaultDeathHandler());
            gameState = GAME_STATE_WAITING;
            lobby.setCountdown(20);
            winner.setHeadicon2(0);
            Duel.restore_stats(winner);
            players.clear();
            updateUI();
        }
    }

    /**
     * Method registerPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void registerPlayer(Player player) {
        Point p = RANDOM_POINTS[GameMath.rand3(RANDOM_POINTS.length)];

        player.teleport(p.getX(), p.getY(), 0);
        player.getAccount().setSetting("fpkc", 0);
        player.getWindowManager().showOverlay(2804);
        updateUI();
        player.setCurDeathHandler(new DeathHandler() {
            @Override
            public void handleDeath(Killable killed, Killable killedBy) {
                killedBy.getPlayer().getAccount().increaseVar("fpkc", 1);
                players.remove(killed.getPlayer());
                lobby.addPlayer(killed.getPlayer());
                killed.getPlayer().setHeadicon2(0);
                killed.getPlayer().setCurDeathHandler(new DefaultDeathHandler());
                killed.getPlayer().getActionSender().sendMessage("You survived " + playerTotal
                        + " players. and killed " + killed.getPlayer().getAccount().getInt32("fpkc") + "");

                int total = killed.getPlayer().getAccount().getInt32("fpkc") * (100 + (playerTotal * 2));

                killed.getPlayer().getInventory().addItem(6529, total);
                killed.getPlayer().getActionSender().sendMessage("You have earned " + total + " tokkul");
                Duel.restore_stats(killed.getPlayer());
                updateUI();
                checkGame();
            }
        });
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        lobby.update();

        if (timer > 0) {
            timer--;
        }

        if ((timer == 0) && (gameState == GAME_STATE_GAME_STARTING)) {
            gameState = GAME_STATE_IN_GAME;

            for (Player player : players) {
                player.getActionSender().sendMessage("Game starting");
            }
        }

        checkGame();

        if (players.size() == 0) {
            gameState = GAME_STATE_WAITING;
        }

        for (int i = 0; i < players.size(); i++) {
            Player pla = players.get(i);

            if (!pla.isInArea(FIGHT_PITS_ZONE) ||!pla.isLoggedIn()) {
                players.remove(i);
                pla.setCurrentInstance(null);
                pla.setHeadicon2(0);
                Duel.restore_stats(pla);
                pla.setCurDeathHandler(new DefaultDeathHandler());
                updateUI();
            }
        }
    }

    private Player getCurrentChampion() {
        Player highest   = null;
        int    highestKc = 0;

        for (Player player : players) {
            int kc = player.getAccount().getInt32("fpkc");

            if ((highest == null) || (kc > highestKc)) {
                highestKc = kc;
                highest   = player;
            }
        }

        return highest;
    }

    /**
     * Method updateUI
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateUI() {
        Player highest = getCurrentChampion();

        if (highest != null) {
            highest.setHeadicon2(2);
            highest.getAppearance().setChanged(true);
        }

        if (previous != null) {
            previous.setHeadicon2(0);
            previous.getAppearance().setChanged(true);
        }

        previous = highest;

        for (Player player : players) {
            if (highest != null) {
                player.getActionSender().changeLine("Leader: " + highest.getUsername(), 2805);
            }

            player.getActionSender().changeLine("Foes remaining: " + (players.size() - 1), 2806);
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onLobbyFinished
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lobby
     *
     * @return
     */
    @Override
    public boolean onLobbyFinished(Lobby lobby) {
        if (gameState != GAME_STATE_WAITING) {
            return false;    // To change body of implemented methods use File | Settings | File Templates.
        }

        if (lobby.getPlayers().size() == 0) {
            return false;
        }

        players.addAll(lobby.getPlayers());
        timer = 0;

        for (Player player : players) {
            registerPlayer(player);
        }

        playerTotal = players.size();
        gameState   = GAME_STATE_GAME_STARTING;

        return true;
    }

    /**
     * Method onLobbyEmpty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lobby
     *
     * @return
     */
    @Override
    public boolean onLobbyEmpty(Lobby lobby) {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
