package net.tazogaming.hydra.game.minigame.clanwars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;

//~--- JDK imports ------------------------------------------------------------

import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/05/14
 * Time: 17:10
 */
public enum ClanWarsMap {
    CLASSIC(3263, 3714, 3328, 3839, 0, Point.location(3291, 3830, 0), Point.location(3303, 3714, 0), "cw-classic",
            Point.location(3267, 3775, 0), Point.location(3317, 3775, 0), 28176, 7206),
    PLATAUE(2854, 5890, 2909, 5952,
                0, Point.location(2885, 5940, 0), Point.location(2884, 5902, 0), "cw-plat",
                Point.location(2856, 5921, 0), Point.location(2907, 5921, 0), 38687, 10368),
    QUARRY(2884, 5507, 2939,
                    5562, 0, Point.location(2925, 5559, 0), Point.location(2895, 5513, 0), "quarry",
                    Point.location(2885, 5531, 0), Point.location(2938, 5531, 0), 38685, 10369),
    FOREST(2884, 5636, 2944, 5695, 0, Point.location(2931, 5686, 0), Point.location(2889, 5640, 0), "forrest",
                        Point.location(2884, 5667, 0), Point.location(2939, 5667, 0), 38689, 10370),
    TURRETS(2692, 5508, 2747, 5627, 0, Point.location(2734, 5510, 0), Point.location(2706, 5623, 0), "turrets",
            Point.location(2692, 5553, 0), Point.location(2747, 5553, 0), 38691, 10371);

    private Zone zone;

    private Point[] spawnPoints;

    private int wallId, animationId;

    private Point wallFrom, wallTo;

    ClanWarsMap(int lx, int ly, int hx, int hy, int h, Point spawn1, Point spawn2, String name, Point wallFrom,
                Point wallTo, int objId, int animationID) {
        zone = new Zone(lx, ly, hx, hy, h, true, true, name);
        this.spawnPoints    = new Point[2];
        this.spawnPoints[0] = spawn1;
        this.spawnPoints[1] = spawn2;
        this.wallTo         = Point.location(wallTo.getX() + 1, wallTo.getY(), wallTo.getHeight());
        this.wallFrom       = Point.location(wallFrom.getX() - 1, wallFrom.getY(), wallFrom.getHeight());
        this.wallId         = objId;
        this.animationId    = animationID;
    }

    public int getAnimationID() {
        return animationId;
    }

    public List<GameObject> buildWall(ClanWar war) {
        Point            current = wallFrom;
        List<GameObject> obj     = new LinkedList<GameObject>();

        do {
            int dir = Movement.getDirectionForWaypoints(current, wallTo);

            current = Movement.getPointForDir(current, dir);

            GameObject object = new GameObject();

            object.setLocation(current);
            object.setId(wallId);
            object.setCurrentRotation(1);
            object.setIdentifier(wallId);
            object.setObjectType(10);

            if (ClippingDecoder.isWaterOrHill(current.getX(), current.getY(), current.getHeight())
                    || (World.getWorld().getTile(current.getX(), current.getY(), current.getHeight()).getClippingData()
                        != 0)) {
                object.setObjectType(5);    // make it invisible XD
            }

            object.setCurrentInstance(war);
            obj.add(object);
        } while ((current.getX() != wallTo.getX()) || (current.getY() != wallTo.getY()));

        return obj;
    }

    public Zone getZone() {
        return zone;
    }

    public Point getSpawnPoint(int teamID) {
        return spawnPoints[teamID];
    }
}
