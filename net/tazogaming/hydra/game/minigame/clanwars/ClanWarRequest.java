package net.tazogaming.hydra.game.minigame.clanwars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.util.RecurringTickEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/05/14
 * Time: 12:28
 */
public class ClanWarRequest implements RecurringTickEvent {
    private ClanChannel[] setChannels = new ClanChannel[2];
    private Player[]      players     = new Player[2];
    private boolean[]     accepted    = new boolean[2];
    private long[]        stake       = new long[2];
    private boolean       isClosed    = false;
    private boolean       isDeclined  = false;
    private ClanWarRules currentRules;

    /**
     * Constructs ...
     *
     *
     * @param challenging
     * @param who
     */
    public ClanWarRequest(Player challenging, Player who) {
        players[0]     = challenging;
        players[1]     = who;
        setChannels[0] = challenging.getChannel();
        setChannels[1] = who.getChannel();
        currentRules   = new ClanWarRules();
        updateInterface();

        for (final Player player : players) {
            player.getGameFrame().openWindow(791);

            player.getGameFrame().sendString("Challenging: " + getOtherPlayer(player).getUsername() + " (" + getOtherPlayer(player).getChannel().getName() + ")", 791, 14);


            player.getGameFrame().setCurrentClanWarsRequest(this);
        }


        Core.submitTask(this);
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player[] getPlayers() {
        return players;
    }

    /**
     * Method setPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param players
     */
    public void setPlayers(Player[] players) {
        this.players = players;
    }

    /**
     * Method isDeclined
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDeclined() {
        return isDeclined;
    }

    /**
     * Method setDeclined
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param isDeclined
     */
    public void setDeclined(boolean isDeclined) {
        this.isDeclined = isDeclined;
    }

    /**
     * Method getRules
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ClanWarRules getRules() {
        return currentRules;
    }

    /**
     * Method resetAccepted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetAccepted() {
        int c = 0;
        for (int i = 0; i < accepted.length; i++) {
            if(accepted[i])
                c++;
           if(c == 1)
            players[i].getActionSender().sendMessage("Accepted reset");
        }

        accepted = new boolean[2];
    }

    /**
     * Method updateInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateInterface() {
        for (Player pla : players) {

         //  pla.getActionSender().sendVar(1305, currentRules);
        }

    }


    /**
     * Method accept
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void accept(Player player) {
        if (accepted[getPlayerID(player)]) {
            return;
        }

        accepted[getPlayerID(player)] = true;
        getOtherPlayer(player).getActionSender().sendMessage("Other player accepted the rules..");
        updateInterface();
    }

    private Player getOtherPlayer(Player player) {
        return players[((getPlayerID(player) + 1) % 2)];
    }

    private int getPlayerID(Player player) {
        int i = 0;

        for (Player player1 : players) {
            if (player1 == player) {
                return i;
            }

            i++;
        }

        return -1;
    }

    /**
     * Method decline
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void decline(Player player) {
        if (isDeclined) {
            return;
        }

        getOtherPlayer(player).getActionSender().sendMessage("Other party declined clan-war rules");
        isDeclined = true;

        for (Player pla : players) {
            pla.getGameFrame().closeAll();
            pla.getGameFrame().setCurrentClanWarsRequest(null);
        }

        isClosed = true;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        int acceptedCount = 0;

       boolean update = currentRules.popSettingsUpdate();

        for (int i = 0; i < players.length; i++) {
            if (accepted[i]) {
                acceptedCount++;
            }
        if(update)
            players[i].getActionSender().sendVar(1305, currentRules.getSettingBit());



            if (players[i].getChannel() != setChannels[i]) {
                decline(players[i]);
                return;
            }
        }

        if (acceptedCount == 2) {
            isClosed = true;

            boolean badStake = false;

            for (int i = 0; i < setChannels.length; i++) {
                if (setChannels[i].getWar() != null) {
                    players[i].getActionSender().sendMessage("Your team is already at war.");
                    isClosed = true;
                    decline(players[i]);

                    return;
                }

                if (stake[i] > 0) {
                    if (setChannels[i].getClanPot() < stake[i]) {
                        players[(i + 1 % 2)].getActionSender().sendMessage(
                            "Users clan doesn't have enough funds for this stake");
                        players[i].getActionSender().sendMessage("Your clan doesn't have enough funds for this stake.");
                        decline(players[i]);

                        return;
                    }
                }
            }

            isClosed   = true;
            isDeclined = true;

            for (Player player : players) {
                player.getGameFrame().closeAll();
                player.setChallengeRequest(-1);
                player.getGameFrame().setCurrentClanWarsRequest(null);
            }

            ClanWar war = new ClanWar(players[0], players[1], ClanWarsMap.values()[currentRules.getRule(ClanWarRules.ARENA_CHOICE)], currentRules, stake[0], stake[1]);

            setChannels[0].message("Info", ClanChannel.HYDRA_ADMIN,
                                   players[0].getUsername() + " has declared war on " + setChannels[1].getName());

            if (stake[0] > 0) {
                setChannels[0].withdraw(stake[0]);
                setChannels[0].message("Info", ClanChannel.HYDRA_ADMIN,
                                       ClanChannel.getCashString(stake[0]) + " GP of the clan pot has been staked");
                setChannels[0].message("Info", ClanChannel.HYDRA_ADMIN,
                                       "Enemy is staking " + ClanChannel.getCashString(stake[1]) + " GP");
            }

            setChannels[1].message("Info", ClanChannel.HYDRA_ADMIN,
                                   "War has been declared on our clan by " + setChannels[0].getName());

            if (stake[1] > 0) {
                setChannels[1].withdraw(stake[1]);
                setChannels[1].message("Info", ClanChannel.HYDRA_ADMIN,
                                       ClanChannel.getCashString(stake[1]) + " GP of the clan pot has been staked");
                setChannels[1].message("Info", ClanChannel.HYDRA_ADMIN,
                                       "Enemy is staking " + ClanChannel.getCashString(stake[0]) + " GP");
            }

            for (int i = 0; i < 2; i++) {
                setChannels[1].message("Info", ClanChannel.HYDRA_ADMIN, "War will start in 2 minutes");
            }
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return isClosed;
    }
}
