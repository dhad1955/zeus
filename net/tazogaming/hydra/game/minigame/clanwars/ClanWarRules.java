package net.tazogaming.hydra.game.minigame.clanwars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/14
 * Time: 01:09
 */
public class ClanWarRules {
    public static final int
            VICTORY_OPTION                     = 0,
            TIME_LIMIT                         = 1,
            STRAGGLERS                         = 2,
            ITEMS_ON_DEATH                     = 3,
            MELEE_DISABLED                     = 4,
            RANGE_DISABLED                     = 5,
            MAGE_OPTIONS                       = 6,
            SUMMONING_DISABLED                 = 7,
            FOOD_DISABLED                      = 8,
            POTS_DISABLED                      = 9,
            PRAYER_DISABLED                    = 10,
            ARENA_CHOICE                       = 11;
    public static int[]       settings     = new int[12];
    public static final int[] KILLS_TO_WIN = {
            -1, 25, 50, 100, 200, 400, 750, 1000, 2500, 5000, 10000
    };
    public static final int[] TIME_LIMITS  = {
            Core.getTicksForMinutes(5), Core.getTicksForMinutes(10), Core.getTicksForMinutes(30),
            Core.getTicksForMinutes(60), Core.getTicksForMinutes(90), Core.getTicksForMinutes(120),
            Core.getTicksForMinutes(180), Core.getTicksForMinutes(240), Core.getTicksForMinutes(300),
            Core.getTicksForMinutes(360), Core.getTicksForMinutes(420), Core.getTicksForMinutes(480),
            Core.getTicksForMinutes(520)
    };

    /** rules made: 14/09/30 */
    private int[] rules = new int[12];

    public enum SettingBit {
        VICTORY_OPTION(1), TIME_LIMIT(16), STRAGGLERS(256), ITEMS_ON_DEATH(512), MELEE_DISABLED(1024),
        RANGE_DISABLED(2048), MAGE_OPTIONS(4096), SUMMONING_DISABLED(16384), FOOD_DISABLED(32768),
        POTS_DISABLED(65536), PRAYER_DISABLED(131072), ARENA_CHOICE(67108864);

        /** flag made: 14/09/29 */
        private int flag;

        SettingBit(int flag) {
            this.flag = flag;
        }

        /**
         * Method getFlag
         * Created on 14/09/29
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getFlag() {
            return flag;
        }
    }

    /**
     * Method getKillsToWin
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    int getKillsToWin() {
        if (isKnockoutMode() || (rules[VICTORY_OPTION] == 15)) {
            return -1;
        }

        return KILLS_TO_WIN[rules[VICTORY_OPTION]];
    }

    /**
     * Method isKnockoutMode
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
   public boolean isKnockoutMode() {
        return rules[VICTORY_OPTION] == 0;
    }

    int getTimeLimit() {
        return TIME_LIMITS[rules[TIME_LIMIT]];
    }


    public boolean magicAllowed() {
        return rules[MAGE_OPTIONS] < 3;
    }

    public int getRule(int setting) {
        return rules[setting];
    }

    public int getSettingBit() {
        int bit = 0;
        for (int i = 0; i < rules.length; i++) {
            bit += (SettingBit.values()[i].getFlag() * rules[i]);
        }



        return bit;
    }



    public void toggleRule(int ruleID) {


        updateInterface();
        if (rules[ruleID] == 1) {
            rules[ruleID] = 0;
        } else {
            if(ruleID == MELEE_DISABLED && rules[RANGE_DISABLED] == 1 && rules[MAGE_OPTIONS] > 1)
                return;
            else if(ruleID == RANGE_DISABLED && rules[MELEE_DISABLED] == 1 && rules[MAGE_OPTIONS] > 1)
                return;

            rules[ruleID] = 1;
        }

        ruleChanged(ruleID, rules[ruleID]);

        updateInterface();
    }

    public void setRule(int ruleID, int rule) {

        if(ruleID == MAGE_OPTIONS && rule > 1 && rules[MELEE_DISABLED] == 1 && rules[RANGE_DISABLED] == 1){
            return;
        }
        rules[ruleID] = rule;
        ruleChanged(ruleID, rule);
        updateInterface();
    }


    public boolean settingActive(int rule) {
        return rules[rule] == 1;
    }

    private void ruleChanged(int ruleID, int setTo) {

        if ((ruleID == VICTORY_OPTION) && (setTo == 15)) {
            if (rules[TIME_LIMIT] == 0) {
                rules[TIME_LIMIT] = 1;
            }
        }

        if((ruleID == TIME_LIMIT && setTo == 0)){
            if(rules[VICTORY_OPTION] == 15) {
                rules[VICTORY_OPTION] = 2;
            }
        }
    }


    private  boolean settingsUpdate = false;
    private void updateInterface() {
        settingsUpdate = true;
    }

    public boolean popSettingsUpdate() {
        if(settingsUpdate)          {
            settingsUpdate = false;
            return true;
        }
        return false;
    }
}
