package net.tazogaming.hydra.game.minigame.clanwars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;

//~--- JDK imports ------------------------------------------------------------

import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/05/14
 * Time: 12:05
 */
public class ClanWarsDeathHandler implements DeathHandler {

    /** gameRules made: 14/10/02 */
    private ClanWarRules gameRules;

    /** war made: 14/10/02 */
    private ClanWar war;

    /**
     * Constructs ...
     *
     *
     * @param rules
     * @param war
     */
    public ClanWarsDeathHandler(ClanWarRules rules, ClanWar war) {
        this.gameRules = rules;
        this.war       = war;
    }

    /**
     * Method handleDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killed
     * @param killedBy
     */
    @Override
    public void handleDeath(Killable killed, Killable killedBy) {
        if (gameRules.settingActive(ClanWarRules.ITEMS_ON_DEATH)) {
            new DefaultDeathHandler().handleDeath(killed, killedBy);
        }

        killed.getPlayer().setActionsDisabled(false);
        killedBy.getPlayer().setActionsDisabled(false);

        killed.getPlayer().setCurrentSpecial(10.00);
        SpecialAttacks.updateSpecial(killed.getPlayer());
        if (killedBy.getPlayer().getAccount().getKillCount(killed.getPlayer().getUID()) < 5) {
            if (killed.getPlayer().getUID() != killedBy.getPlayer().getUID()) {
                try {
                    if ((new Random().nextInt(100) > 40) && killedBy.getPlayer().getGetCurrentWar()
                            .getTeam(killedBy.getPlayer()).getChannel()
                            .isApproved() && (killedBy.getPlayer().getChannel()
                                .getRankForPlayer(killedBy.getPlayer()) > ClanChannel.RECRUIT)) {
                      //  killedBy.getPlayer().addPoints(Points.PK_POINTS_2, 1);
                        killedBy.getPlayer().getActionSender().sendMessage(
                            "Point earned for kill, playing for approved clan!");
                        killedBy.getPlayer().getAccount().registerKilled(killed.getPlayer());
                    }
                } catch (Exception ee) {}
            }
        }

        if (gameRules.isKnockoutMode()) {}
        else if (!war.isWar()) {
            war.getTeam(killedBy.getPlayer()).addPoint();
        }

        war.unlinkPlayer(killed.getPlayer());
        war.gameChanged();
    }
}
