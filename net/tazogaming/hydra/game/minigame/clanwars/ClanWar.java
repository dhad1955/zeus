package net.tazogaming.hydra.game.minigame.clanwars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/05/14
 * Time: 17:17
 */
public class ClanWar extends Instance implements RecurringTickEvent {
    public static final int
        MY_PLAYERS_CONFIG                   = 261,
        THERE_PLAYERS_CONFIG                = 262,
        MY_KILLS                            = 263,
        THERE_KILLS                         = 264;
    public static final Zone CLAN_WARS_HOME = new Zone(2951, 9655, 3026, 9709, 0, true, true,
                                                  "clanwars").setBankingAllowed(true).setStatic(false);
    public static final int
        PHASE_WAITING_PARTICIPANTS            = 0,
        PHASE_PRE_WAR                         = 1,
        PHASE_IN_WAR                          = 2;
    public static final Zone WHITE_PORTAL_PVP = new Zone(
                                                    2756, 5512, 2876, 5628, 0, false, true,
                                                    "white_pvp").setBankingAllowed(false).setBobAllowed(
                                                        false).setTeleportAllowed(false).setTeleportMessage(
                                                        "Nope.").setStatic(true);
    public static final Zone RED_PORTAL_PVP =
        new Zone(2948, 5512, 3070, 5629, 0, true, true,
                 "red_pvp").setBankingAllowed(false).setTeleportAllowed(false).setStatic(true);

    static {}

    /** separatorWall made: 14/10/02 **/
    private List<GameObject> separatorWall = null;

    /** teams made: 14/08/18 */
    private ClanWarTeam[] teams = new ClanWarTeam[2];

    /** gameState made: 14/08/18 */
    private int gameState = 0;

    /** isTerminated made: 14/08/18 */
    private boolean isTerminated = false;

    /** gameChanged made: 14/08/18 */
    private boolean gameChanged = false;

    /** wallRemoveTImer made: 14/10/02 **/
    private int wallRemoveTImer = 0;

    /** map made: 14/08/18 */
    private ClanWarsMap map;

    /** gameStartsIn made: 14/08/18 */
    private int gameStartsIn;

    /** gameEndsIn made: 14/08/18 */
    private int gameEndsIn;

    /** curDeathHandler made: 14/08/18 */
    private DeathHandler curDeathHandler;

    /** gameRules made: 14/08/18 */
    private ClanWarRules gameRules;

    /** warFlag made: 14/08/18 */
    private GameObject warFlag;

    /** warFlagHolder made: 14/08/18 */
    private Player warFlagHolder;

    /**
     * Constructs ...
     *
     *
     * @param challenging
     * @param fo
     * @param map
     * @param rules
     * @param stake1
     * @param stake2
     */
    public ClanWar(Player challenging, Player fo, ClanWarsMap map, ClanWarRules rules, long stake1, long stake2) {
        teams[0] = new ClanWarTeam(challenging.getChannel());
        teams[1] = new ClanWarTeam(fo.getChannel());
        teams[0].setStake(stake1);
        teams[1].setStake(stake2);
        this.map       = map;
        this.gameRules = rules;
        challenging.getChannel().setCurrentWar(this);
        fo.getChannel().setCurrentWar(this);

        int time = 0;

        gameStartsIn         = Core.currentTime + 250;
        time                 = rules.getTimeLimit();
        gameEndsIn           = gameStartsIn + time;
        this.curDeathHandler = new ClanWarsDeathHandler(rules, this);
        this.separatorWall   = map.buildWall(this);
        Core.submitTask(this);
    }

    /**
     * Method isInClanWars
     * Created on 14/10/01
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean isInClanWars(Player player) {
        for (Zone e : player.getAreas()) {
            if (isClanWarArea(e)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isClanWarArea
     * Created on 14/10/01
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     *
     * @return
     */
    public static boolean isClanWarArea(Zone e) {
        for (ClanWarsMap map : ClanWarsMap.values()) {
            if (map.getZone() == e) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method placeWarFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     */
    public void placeWarFlag(Point location) {
        warFlag = new GameObject();
        warFlag.setIdentifier(4900);
        warFlag.setObjectType(10);
        warFlag.setLocation(location);
        warFlag.setCurrentInstance(this);
        World.getWorld().getObjectManager().registerObject(warFlag);
        updatePointer();
    }

    /**
     * Method onObjectClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     * @param x
     * @param y
     *
     * @return
     */
    public boolean onObjectClick(Player player, int id, int x, int y) {
        if (warFlag != null) {
            if ((id == 4900) && (x == warFlag.getX()) && (y == warFlag.getY())) {
                pickupWarFlag(player);
            }

            return true;
        }

        return false;
    }

    /**
     * Method isWar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Deprecated
    public boolean isWar() {
        return false;
    }

    /**
     * Method pickupWarFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void pickupWarFlag(Player player) {
        if (warFlag != null) {
            if (player.getEquipment().getId(3) != -1) {
                if (!player.getEquipment().unwieldItem(3)) {
                    player.getActionSender().sendMessage("Not enough slots");

                    return;
                }
            }

            player.getEquipment().set(4037, 1, 3);
            player.getEquipment().updateEquipment(3);
            player.getEquipment().updateWeapon();
            player.getAppearance().setChanged(true);
            warFlagHolder = player;
            updateMarker(player);
            warFlag.remove();
            warFlag = null;
        }
    }

    /**
     * Method gameChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void gameChanged() {
        gameChanged = true;
    }

    /**
     * Method isTeamedWith
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param player2
     *
     * @return
     */
    public boolean isTeamedWith(Player player, Player player2) {
        return (player.getGetCurrentWar() == player2.getGetCurrentWar()) && (getTeamID(player) == getTeamID(player2));
    }

    /**
     * Method gameStarted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean gameStarted() {
        return gameState == PHASE_IN_WAR;
    }

    /**
     * Method getGameRules
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ClanWarRules getGameRules() {
        return gameRules;
    }

    /**
     * Method setGameRules
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param gameRules
     */
    public void setGameRules(ClanWarRules gameRules) {
        this.gameRules = gameRules;
    }

    /**
     * Method getTeam
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public ClanWarTeam getTeam(int index) {
        return teams[index];
    }

    private void startGame() {
        gameChanged();
        gameState = PHASE_IN_WAR;

        if (isWar()) {
            placeWarFlag(Point.location(3297, 3776, 0));
        }


       for(ClanWarTeam team : teams)
        for(Player player : team.getPlayers()){
            if(getGameRules().getRule(ClanWarRules.ITEMS_ON_DEATH) == 0){
               if(!team.getChannel().isApproved()) {
                    player.getActionSender().sendMessage(Text.BLUE("You won't receive pkp for this because your clan is not approved"));
                    player.getActionSender().sendMessage(Text.BLUE("Your leader can request approval on the forums."));
               }else{
                   player.getActionSender().sendMessage(Text.BLUE("Ranked Team members will receive PKP for this game."));
               }
            }

        }
        for (GameObject object : separatorWall) {
            for (Player player : getAllPlayers()) {
                player.getActionSender().sendAnimateObject(object, map.getAnimationID());
            }
        }

        wallRemoveTImer = 6;
    }

    /**
     * Method getOppositeTeam
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public ClanWarTeam getOppositeTeam(Player player) {
        int teamID = getTeamID(player);

        return getOppositeTeam(teamID);
    }

    private int getTeamID(ClanWarTeam t) {
        if (teams[0] == t) {
            return 0;
        }

        return 1;
    }

    private ClanWarTeam getOppositeTeam(int teamID) {
        return teams[(teamID + 1) % 2];
    }

    /**
     * Method updateMarker
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void updateMarker(Player player) {
        if (warFlagHolder == player) {
            player.getActionSender().markPlayer(-1);

            return;
        }

        if (warFlag != null) {
            player.getActionSender().createArrow(warFlag.getX(), warFlag.getY(), 100, 2);
        } else if (warFlagHolder != null) {
            if (player.getWatchedPlayers().getAllEntities().contains(warFlagHolder)) {
                player.getActionSender().markPlayer(warFlagHolder.getIndex());
            } else {
                player.getActionSender().createArrow(warFlagHolder.getX(), warFlagHolder.getY(), 100, 2);
            }
        } else {}
    }

    private ArrayList<Player> getAllPlayers() {
        ArrayList<Player> tmp = new ArrayList<Player>(teams[0].getPlayers().size() + teams[1].getPlayers().size());

        tmp.addAll(teams[0].getPlayers());
        tmp.addAll(teams[1].getPlayers());

        return tmp;
    }

    /**
     * Method updatePointer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updatePointer() {
        for (Player player : getAllPlayers()) {
            updateMarker(player);
        }
    }

    /**
     * Method updateGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void updateGame(Player player) {
        if (gameState == PHASE_WAITING_PARTICIPANTS) {
            player.getActionSender().sendClanTimer(Core.timeUntil(gameStartsIn), 1);
        } else if (gameEndsIn != -1) {
            player.getActionSender().sendClanTimer(Core.timeUntil(gameEndsIn), 0);
        } else {
            player.getActionSender().sendClanTimer(0, 0);
        }

        player.getActionSender().sendClanWar(this);
    }

    /**
     * Method endGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param winner
     */
    public void endGame(ClanWarTeam winner) {



        if (separatorWall != null) {
            for (GameObject obj : separatorWall) {
                obj.remove();
                obj.setLocation(null);
            }

            separatorWall = null;
        }

        if (winner != null) {
            winner.getChannel().setClanWins(winner.getChannel().getClanWins() + 1);
            getOppositeTeam(getTeamID(winner)).getChannel().setClanLosses(
                    getOppositeTeam(getTeamID(winner)).getChannel().getClanLosses());
            for (Player player : winner.getPlayers()) {
                if (gameRules.isKnockoutMode()) {
                    player.getActionSender().sendMessage(winner.getChannel().getName() + " won the game by knockout");
                }
            }

            if (winner.getStake() > 0) {
                winner.getChannel().deposit(winner.getStake());
            }

            if (getOppositeTeam(getTeamID(winner)).getStake() > 0) {
                winner.getChannel().message("Info", ClanChannel.HYDRA_ADMIN,
                                            "War won. "
                                            + ClanChannel.getCashString(getOppositeTeam(getTeamID(winner)).getStake())
                                            + " gp added to the clan pot ");
                winner.getChannel().deposit(getOppositeTeam(getTeamID(winner)).getStake());
            }
        }

        for (ClanWarTeam team : teams) {
            team.getChannel().setCurrentWar(null);
        }

        ArrayList<Player> tmp = new ArrayList<Player>();

        tmp.addAll(teams[0].getPlayers());
        tmp.addAll(teams[1].getPlayers());

        for (Player player : tmp) {

            unlinkPlayer(player);
        }

        if (warFlag != null) {
            warFlag.remove();
            warFlag = null;
        }

        isTerminated = true;
    }

    /**
     * Method isFlagHolder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isFlagHolder(Player player) {
        return warFlagHolder == player;
    }

    /**
     * Method updateGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private void updateConfig(Player player) {
      if(player == null || getTeam(player) == null)
          return;

        player.getActionSender().sendBConfig(MY_PLAYERS_CONFIG, getTeam(player).getPlayers().size());
        player.getActionSender().sendBConfig(THERE_PLAYERS_CONFIG, getOppositeTeam(player).getPlayers().size());
        player.getActionSender().sendBConfig(MY_KILLS, getTeam(player).getScore());
        player.getActionSender().sendBConfig(THERE_KILLS, getOppositeTeam(player).getScore());

        if (gameStarted()) {
            player.getActionSender().sendBConfig(260, 1);
            player.getActionSender().sendBConfig(270, Core.getSecondsForTicks(gameEndsIn - Core.currentTime) / 60);
        } else {
            player.getActionSender().sendBConfig(260, 0);
            player.getActionSender().sendBConfig(270, gameStartsIn - Core.currentTime);
        }
    }

    /**
     * Method updateGame
     * Created on 14/10/01
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public boolean isWallPoint(int x, int y) {
        if (separatorWall == null) {
            return false;
        }

        for (GameObject obj : separatorWall) {
            if ((obj.getX() == x) && (obj.getY() == y)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method wallClipped
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param point
     *
     * @return
     */
    public static boolean wallClipped(Player player, Point point) {
        if (player.getCurrentInstance() instanceof ClanWar) {
            return player.getGetCurrentWar().isWallPoint(point.getX(), point.getY());
        }

        return false;
    }

    /**
     * Method updateGame
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateGame() {
        ArrayList<Player> tmp = new ArrayList<Player>();

        tmp.addAll(teams[0].getPlayers());
        tmp.addAll(teams[1].getPlayers());

        if ((wallRemoveTImer == 1) && (separatorWall != null)) {
            for (GameObject obj : separatorWall) {
                obj.remove();
                obj.setLocation(null);
            }

            separatorWall = null;
        } else if (wallRemoveTImer > 1) {
            wallRemoveTImer--;
        }

        if (isWar()) {
            if (warFlagHolder != null) {
                int team = getTeamID(warFlagHolder);

                if ((team != -1) && (Core.currentTime % 2 == 0)) {
                    teams[team].addPoint();


                    gameChanged();
                }
            }
        }

        for (Player player : tmp) {
            if(tmp.size() >= 50)
                player.addAchievementProgress2(Achievement.BLOODLUST, 1);

            updateConfig(player);

            if (isWar() && isFlagHolder(player)) {
                int teamID = getTeamID(player);

                if (Point.getDistance(map.getSpawnPoint(teamID), player.getLocation()) < 25) {
                    player.getActionSender().sendMessage(
                        "You cannot stand within 25 tiles of your spawn point with the flag");
                    player.getEquipment().removeWeapon();
                    placeWarFlag(player.getLocation());
                    warFlagHolder = null;
                }
            }

            if (gameChanged) {
                updateGame(player);
            }

            if (Core.currentTime % 2 == 0) {
                if (isWar()) {
                    updateMarker(player);
                } else {}
            } else {}

            if (!player.isLoggedIn() ||!player.isInArea(map.getZone())) {
                unlinkPlayer(player);

                continue;
            }
        }

        switch (gameState) {
        case PHASE_WAITING_PARTICIPANTS :
            if (Core.timeUntil(gameStartsIn) <= 0) {
                startGame();
            }

            break;

        case PHASE_IN_WAR :
            if (tmp.size() == 0) {
                endGame(null);

                return;
            }

            if (gameRules.isKnockoutMode()) {
                for (int team = 0; team < teams.length; team++) {
                    if (teams[team].getPlayers().size() == 0) {
                        endGame(getOppositeTeam(team));

                        return;
                    }
                }
            } else {
                if ((gameEndsIn != -1) && (Core.timeSince(gameEndsIn) > 1)) {
                    if (gameRules.isKnockoutMode()) {
                        int winner_id = -1;

                        if (teams[0].getPlayers().size() > teams[1].getPlayers().size()) {
                            winner_id = 0;
                        } else {
                            winner_id = 1;
                        }

                        endGame(teams[winner_id]);

                        return;
                    } else {
                        int winner_id = -1;

                        if (teams[0].getScore() > teams[1].getScore()) {
                            winner_id = 0;
                        } else {
                            winner_id = 1;
                        }

                        endGame(teams[winner_id]);
                    }

                    return;
                }

                for (int team = 0; team < teams.length; team++) {
                    if ((gameRules.getKillsToWin() != 0) && (gameRules.getKillsToWin() <= teams[team].getScore())) {
                        endGame(getTeam(team));

                        return;
                    } else if ((teams[team].getPlayers().size() == 0) && gameRules.isKnockoutMode()) {
                        endGame(getOppositeTeam(team));
                    }
                }
            }

            break;
        }
    }

    /**
     * Method getTeamID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public int getTeamID(Player player) {
        for (int i = 0; i < teams.length; i++) {
            if (teams[i].getChannel() == player.getChannel()) {
                return i;
            }
        }

        return -1;
    }

    private int getCurrentTeamID(Player player) {
        for (int i = 0; i < teams.length; i++) {
            if (teams[i].isRegistered(player)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getTeam
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public ClanWarTeam getTeam(Player player) {
        int id = getTeamID(player);

        return (id == -1)
               ? null
               : teams[id];
    }

    /**
     * Method getMap
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ClanWarsMap getMap() {
        return map;
    }

    /**
     * Method registerPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean registerPlayer(Player player) {
        if (getCurrentTeamID(player) != -1) {
            return false;
        }

        updateConfig(player);
        player.getActionSender().sendVar(1305, getGameRules().getSettingBit());
        player.getGameFrame().openOverlay(265);

        int teamSelection = getTeamID(player);

        if (teamSelection == -1) {
            return false;
        }

        if (teams[teamSelection].registerPlayer(player)) {
            player.setCurrentInstance(this);

            Point startPoint = map.getSpawnPoint(teamSelection);

            player.teleport(startPoint.getX(), startPoint.getY(), startPoint.getHeight());
            Duel.restore_stats(player);
            player.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
            player.setCurDeathHandler(curDeathHandler);
            updateGame(player);
            gameChanged();
            player.setGetCurrentWar(this);
            updateMarker(player);
        }

        return true;
    }

    /**
     * Method unlinkPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void unlinkPlayer(Player player) {
        player.getGameFrame().closeOverlay();

        if (warFlagHolder == player) {
            player.getEquipment().removeWeapon();
            placeWarFlag(player.getLocation());
            warFlagHolder = null;
        }

        Duel.restore_stats(player);
        player.cancelDeaths();

        try {
            player.resetDamage();
        } catch (Exception ee) {}

        teams[getCurrentTeamID(player)].unRegister(player);
        player.teleport(2994, 9678, 0);
        player.getActionSender().sendClanWar(null);
        player.getActionSender().markPlayer(-1);
        player.setGetCurrentWar(null);
        player.setCurrentInstance(null);
        player.setCurDeathHandler(new DefaultDeathHandler());
        player.getGameFrame().getContextMenu().sendPlayerCommand(1, false, "Challenge");
        gameChanged();
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        updateGame();
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return isTerminated;
    }
}
