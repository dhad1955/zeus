package net.tazogaming.hydra.game.minigame.achievement;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.HelpMenuParser;
import net.tazogaming.hydra.game.ui.MenuBuilder;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/04/14
 * Time: 11:22
 */
public class Achievement {
    public static final int
        PK_NEWBIE                = 0,
        PK_NOVICE                = 1,
        PK_ENTHUSIAST            = 2,
        PK_PROFESSIONAL          = 3,
        PK_PROFESSIONAL_II       = 4,
        PK_MASTER                = 5,
        ELITE_PKER               = 6,
        KILLING_FRENZY           = 7,
        RUNNING_RIOT             = 8,
        RAMPAGE                  = 9,
        UBER_RAMPAGE             = 10,
        HEART_PUNCTURE           = 11,
        KNOCKOUT                 = 12,
        ABYSSAL_PUNISHMENT       = 13,
        SLICE_N_DICE             = 14,
        CAPITAL_SHREDDING        = 15,
        GMAUL_KO                 = 16,
        GMAUL_FRENZY             = 17,
        SNIPER                   = 18,
        BLOWN_AWAY               = 19,
        HYBRID                   = 20,
        SCIMMER                  = 21,
        VENGEANCE                = 22,
        VENGEANCE_II             = 23,
        VENGEANCE_III            = 24,
        RAFFLE_WINNER            = 25,
        DAILY_WINNER             = 26,
        WEEKLY_WINNER            = 27,
        MONTHLY_WINNER           = 28,
        TOP_3                    = 29,
        ENTRANT                  = 30,
        PK_FAILURE               = 31,
        HIGH_RISKER              = 32,
        PURE                     = 33,
        PURE_HOBBYIST            = 34,
        PURE_ADDICT              = 35,
        ZERKER                   = 36,
        ZERKER_HOBBYIST          = 37,
        ZERKER_ADDICT            = 38,
        DEF_LOVER                = 39,
        DEF_HOBBYIST             = 40,
        ITS_ALL_ABOUT_THE_DEF    = 41,
        KURADEL_SENPAI           = 42,
        KURADEL_SENPAI_II        = 43,
        KURADEL_SENPAI_III       = 44,
        SLAYER_MASTER            = 45,
        WHOS_THE_BOSS_NOW        = 46,
        CORPOREAL_PUNISHMENT     = 47,
        TIME_TO_CROSSOVER        = 48,
        REVINATION               = 49,
        REVOLUTION               = 50,
        WORM_KILLER              = 51,
        WORM_ENTHUSIAST          = 52,
        WORM_VETERAN             = 53,
        BOW_AND_THE_BEAST        = 54,
        ABYSSAL_KILLER           = 55,
        EXTERMINATOR             = 56,
        BLESS_MY_SOLES           = 57,
        STARGAZER                = 58,
        SLAYER_NEWBIE            = 59,
        IN_IT_FOR_THE_LONG_HAUL  = 60,
        BANDOS_PUNISHMENT        = 61,
        ARMADYL_PUNISHMENT       = 62,
        KBD_WRECKER              = 63,
        TOWN_MASSACCRE           = 64,
        DRAGON_SLAYER            = 65,
        DRAGON_RIDER             = 66,
        DRAGON_DOMINATOR         = 67,
        FROST_SLAYER             = 68,
        THE_TORMENTED            = 69,
        HAND_BREAKER             = 70,
        SPECTATOR                = 71,
        BLOODSHEAD               = 72,
        GARGOYLE_WRECKAGE        = 73,
        CLEANUP_CREW             = 74,
        WILDERNESS_PVMER         = 75,
        WILDERNESS_PVMER_II      = 76,
        WILDERNESS_PVMER_III     = 77,
        WILDERNESS_PVMER_IIII    = 78,
        BUG_SQUASHER             = 79,
        BUG_EXTERMINATOR         = 80,
        MINER                    = 81,
        MINER_II                 = 82,
        MINER_III                = 83,
        ADAMANT_MINER            = 84,
        RUNE_MINER               = 85,
        DOUBLE_MINER             = 86,
        LUMBERJACK               = 87,
        LUMBERJACK_II            = 88,
        LUMBERJACK_III           = 89,
        MAHOGANIST               = 90,
        MAGIC_WOOD               = 91,
        CAKE_CRUNCHER            = 92,
        SILVER_HOARDER           = 93,
        GEM_SNATCHER             = 94,
        PROFESSIONAL_THIEF       = 95,
        JUNIOR_ROGUE             = 96,
        ROGUE                    = 97,
        NOT_A_VERY_GOOD_GUARD    = 98,
        TERRIBLE_GUARDS          = 99,
        ONYX_FINDER              = 100,
        QUICK_FINGERS            = 101,
        JAIL_VISTOR              = 102,
        JAILBIRD                 = 103,
        DO_NOT_PASS_GO           = 104,
        FINGER_BREAKER           = 105,
        SLEIGHT_OF_HAND          = 106,
        CHEF                     = 107,
        PROFESSIONAL_CHEF        = 108,
        MASTER_CHEF              = 109,
        ROCKS_GRILL              = 110,
        SHARK_ON_TOAST           = 111,
        FISHERMAN                = 112,
        FISHERMAN_II             = 113,
        TUNA_SALAD               = 114,
        PRO_FISHER               = 115,
        AIR_BINDER               = 116,    //
        NAT_BINDER               = 117,    //
        PRO_RUNECRAFTER          = 118,    //
        GEM_CUTTER               = 119,    //
        UPMARKET_GEM_MAN         = 120,    //
        WEALTH_MAKER             = 121,    //
        STRENGTH_MAKER           = 122,    //
        ANTIOXIDANT              = 123,    //
        ALCHEMIST                = 124,
        PRO_ALCHEMIST            = 125,
        MEGA_ALCHEMIST           = 126,
        BOLT_ENCHANTER           = 127,    //
        DIAMOND_GEAZER           = 128,    //
        MAGE                     = 129,
        MAGE_II                  = 130,
        MAGE_III                 = 131,
        VENG_CASTER              = 132,
        VENG_ENTHUSIAST          = 133,
        VENG_MASTER              = 134,
        DRAGON_WORSHIPER         = 135,
        FIRESTARTER              = 136,
        TWISTED_FIRESTARTER      = 137,
        PYROMANIAC               = 138,
        MAGIC_BURNER             = 139,
        FORGER                   = 140,
        GOLDSMITH                = 141,
        CANNONBALL_MAKER         = 142,
        RUNE_SMITHSMAN           = 143,
        IRON_MAKER               = 144,
        GREEN_BAR                = 145,
        QUICK_MOVER              = 146,
        ATHLETE                  = 147,
        WILD_JUMPER              = 148,
        TAG_BOX_PRO              = 149,
        BIRDWATCH                = 150,
        CHINCATCHER              = 151,
        IMP_FINDER               = 152,
        IMP_ENTHUSIAST           = 153,
        IMPATHON                 = 154,
        KINGLYLIKE               = 155,
        STARDUST                 = 156,
        HOLY_GRAIL               = 157,
        BOB_THE_BUILDER          = 158,
        HOME_OWNER               = 159,
        PLAYER                   = 160,
        ENTHUSIAST               = 161,
        FANATIC                  = 162,
        ADDICT                   = 163,
        HYDRASCAPE_RAVING_ADDICT = 164,
        MARATHON                 = 165,
        BILL_BUSTER_I            = 166,
        BILL_BUSTER_II           = 167,
        INDIANA_JONES            = 168,
        HARD_TREASURE            = 169,
        X_MARKS_THE_SPOT         = 170,
        STAKER                   = 171,
        PRO_STAKER               = 172,
        SPIN_DOCTOR              = 173,
        DUNGEON_RAIDER           = 174,
        DUNGEON_MASTER           = 175,
        FAILURE                  = 176,
        PROGRESSOR               = 177,
        COMBAT_ENTHUSIAST        = 178,
        MAXED                    = 179,
        BLOODLUST                = 180,
        WHITE_ENTHUSIAST         = 181;

    /** totalAchievements made: 14/11/04 **/
    private static int          totalAchievements    = 0;
    public static Achievement[] ACHIEVEMENTS         = new Achievement[200];
    public static Achievement[] ORDERED_ACHIEVEMENTS = new Achievement[200];

    /** writer made: 14/11/04 **/
    private static BufferedWriter writer;

    /** type made: 14/11/04 **/
    private int type = 0;

    /** progress made: 14/11/04 **/
    private int progress;

    /** title made: 14/11/04 **/
    private String title;

    /** id made: 14/11/04 **/
    private int id;

    public int getPkp() {
        return pkp;
    }

    private int pkp;


    /** information made: 14/11/04 **/
    private String information;

    /** cashReward made: 14/11/04 **/
    private int cashReward;

    /** extraReward made: 14/11/04 **/
    private String extraReward;

    /** cashString made: 14/11/04 **/
    private String cashString;

    /** interfaceID made: 14/11/04 **/
    private int interfaceID;

    /**
     * Constructs ...
     *
     */
    public Achievement() {}

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param title
     * @param progress
     * @param cash
     */
    public Achievement(int id, String title, int progress, int cash) {
        this(id, title, progress, cash, null);
    }

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param title
     * @param progress
     * @param cash
     * @param extra
     */
    public Achievement(int id, String title, int progress, int cash, String extra) {
        this.extraReward = extra;
        this.id          = id;
        this.progress    = progress;
        this.cashReward  = cash;
        this.title       = title;
    }

    /**
     * Method getInterfaceID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getInterfaceID() {
        return interfaceID;
    }

    /**
     * Method getType
     * Created on 14/11/04
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return this.type;
    }

    /**
     * Method saveAchievements
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param file_chunk
     */
    public static void saveAchievements(Player player, Buffer file_chunk) {
        file_chunk.writeByte(UserAccount.OPCODE_ACHIEVEMENTS);

        for (int i = 0; i < Achievement.ACHIEVEMENTS.length; i++) {
            file_chunk.writeDWord(player.getAchievementProgress(i));
        }
    }

    /**
     * Method loadAchievements
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param file
     */
    public static void loadAchievements(Player player, Buffer file) {
        for (int i = 0; i < Achievement.ACHIEVEMENTS.length; i++) {
            player.setAchievementProgress(i, file.readDWord());
        }
    }

    private static void sortAchievementsByDifficulty(Achievement[] ach) {
        List<Achievement> achievements = Arrays.asList(ach);

        Collections.sort(achievements, new Comparator<Achievement>() {
            @Override
            public int compare(Achievement o1, Achievement o2) {
                if (o1.type < o2.type) {
                    return -1;
                }

                if(o1.type == o2.type)
                    return 0;


                return 1;
            }
        });

       ORDERED_ACHIEVEMENTS =  achievements.toArray(ORDERED_ACHIEVEMENTS);
    }

    /**
     * Method dumpConstants
     * Created on 14/11/04
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void dumpConstants() {
        for (int i = 0; i < ACHIEVEMENTS.length; i++) {
            if (ACHIEVEMENTS[i] != null) {
                StringBuffer buffer = new StringBuffer();

                for (Character c : ACHIEVEMENTS[i].title.toCharArray()) {
                    if (Character.isLetterOrDigit(c) || (c == '_') || (c == ' ')) {
                        if (c == ' ') {
                            c = '_';
                        }

                        buffer.append(c);
                    }
                }

                System.err.println(buffer.toString().toUpperCase() + " = " + i + ",");
            }
        }
    }

    /**
     * Method buildMenu
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     */
    public static void buildMenu(Player player, int id) {
        if (ACHIEVEMENTS[id] == null) {
            player.getActionSender().sendMessage("Error");

            return;
        }

        MenuBuilder builder = new MenuBuilder("Achievement");

        builder.writeLine("Viewing Achievement " + Text.DARK_RED(ACHIEVEMENTS[id].title));
        builder.newLine();
        builder.writeLine(Text.BLUE("Objective"));
        builder.writeLine(ACHIEVEMENTS[id].information);
        builder.newLine();
        builder.writeLine(Text.DARK_RED("Progress - "
                                        + (GameMath.getPercentFromTotal(player.getAchievementProgress(id),
                                            ACHIEVEMENTS[id].progress)) + "%"));
        builder.writeLine(player.getAchievementProgress(id) + " out of " + ACHIEVEMENTS[id].progress);
        builder.newLine();
        builder.writeLine(Text.BLUE("Cash Reward"));
        builder.writeLine("Secret");
        builder.newLine();
        builder.writeLine(Text.BLUE("Extra reward"));

        if(ACHIEVEMENTS[id].pkp > 0){
            builder.writeLine(ACHIEVEMENTS[id].pkp+" PVP Points");
        }
        if ((ACHIEVEMENTS[id].extraReward == null) || ACHIEVEMENTS[id].extraReward.equalsIgnoreCase("null")) {
            builder.writeLine("None");
        } else {
            builder.writeLine(ACHIEVEMENTS[id].extraReward);
        }


        player.getWindowManager().showWindow(13585);
        player.getGameFrame().sendString("Viewing achievement", 275, 2);
        HelpMenuParser.render(player, builder);
    }

    /**
     * Method setInterfaceID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interfaceID
     */
    public void setInterfaceID(int interfaceID) {
        this.interfaceID = interfaceID;
    }

    /**
     * Method giveCash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cash
     * @param player
     */
    public static void giveCash(int cash, Player player) {
        int oneHunderedMill = 1000020000;

        if (cash > oneHunderedMill) {
            player.getBank().insert(18652, cash / oneHunderedMill);
            player.getActionSender().sendMessage("" + (cash / oneHunderedMill)
                    + " bank notes have been sent to your bank");

            int amt       = cash / oneHunderedMill;
            int c2        = amt * oneHunderedMill;
            int remainder = cash - c2;

            if (remainder > 0) {
                player.getBank().insert(995, remainder);
            }

            return;
        } else {
            player.getBank().insert(995, cash);
            player.getActionSender().sendMessage("<img="+ UserAccount.CROWN_SUPPORT+">"+Text.RED("" + cash + " gp has been sent to your bank."));
        }
    }

    /**
     * Method getDifficutlty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static final int getDifficutlty(int id) {
        return ACHIEVEMENTS[id].type;
    }

    /**
     * Method getTotal
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static final int getTotal() {
        return totalAchievements;
    }

    /**
     * Method loadAchievements
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param file
     */
    public static void loadAchievements(String file) {
        String        line   = "";
        int           count  = 0;
        Achievement[] tmp    = new Achievement[200];
        int           lineNo = 0;

        try {
            BufferedReader         reader = new BufferedReader(new FileReader(file));
            Achievement            ach    = null;
            ArrayList<Achievement> easy   = new ArrayList<Achievement>();
            ArrayList<Achievement> medium = new ArrayList<Achievement>();
            ArrayList<Achievement> hard   = new ArrayList<Achievement>();

            do {
                line = reader.readLine();
                lineNo++;

                if (line == null) {
                    break;
                }

                if ((line.length() == 0) || line.startsWith("//") || line.startsWith(" ")) {
                    continue;
                }

                if (line.startsWith("ach")) {
                    ach = new Achievement();

                    String[] str = line.split(" ");

                    ach.id = Integer.parseInt(str[1]);

                    continue;
                }

                if (ach != null) {
                    ArgumentTokenizer pr = new ArgumentTokenizer(line.split(" "));

                    if (line.startsWith("level")) {
                        pr.next();
                        pr.next();

                        String dif = pr.nextString();

                        if (dif.equalsIgnoreCase("easy")) {
                            ach.type = 0;
                        } else if (dif.equalsIgnoreCase("hard")) {
                            ach.type = 2;
                        } else {
                            ach.type = 1;
                        }
                    }

                    if (line.startsWith("name")) {
                        ach.title = line.substring(7);
                    } else if (line.startsWith("interfaceid")) {
                        pr.next();
                        pr.next();
                        ach.interfaceID = pr.nextInt();
                    }

                    if (line.startsWith("progress")) {
                        pr.next();
                        pr.next();
                        ach.progress = pr.nextInt();
                    } else if (line.startsWith("cash")) {
                        pr.next();
                        pr.next();
                        ach.cashString = pr.nextString();

                        // lex the string
                        String cashAmt = "";
                        char   type    = '\n';    // = '';

                        for (int i = 0; i < ach.cashString.length(); i++) {
                            if (Character.isLetter(ach.cashString.charAt(i))) {
                                type = ach.cashString.charAt(i);
                            } else if (Character.isDigit(ach.cashString.charAt(i))) {
                                cashAmt += ach.cashString.charAt(i);
                            }
                        }

                        if (cashAmt.length() == 0) {
                            System.out.println("Error with achievement: " + ach.information);
                        }

                        int totalCash = Integer.parseInt(cashAmt);

                        switch (type) {
                        case '\n' :
                            throw new RuntimeException("ERROR PARSING CASH STRING " + line + ", IGNORING ACHIEVEMENT");

                        case 'b' :
                            totalCash *= 1000000000;

                            break;

                        case 'm' :
                            totalCash *= 1000000;

                            break;

                        case 'k' :
                            totalCash *= 1000;

                            break;
                        }


                        ach.cashReward = totalCash;


                        // ach.cashReward = pr.nextInt();
                    } else if (line.startsWith("obj")) {
                        ach.information = line.substring(6);
                    } else if (line.startsWith("reward")) {
                        ach.extraReward = line.substring(7);
                    }else if(line.startsWith("pkp")) {
                        ach.pkp = Integer.parseInt(line.substring(6));
                    }else if (line.startsWith("}")) {
                        if (tmp[ach.id] != null) {
                            throw new RuntimeException("duplicate achievement: " + ach.id);
                        }

                        tmp[ach.id] = ach;
                        totalAchievements++;

                        if (ach.type == 0) {
                            easy.add(ach);
                        } else if (ach.type == 1) {
                            medium.add(ach);
                        } else {
                            hard.add(ach);
                        }

                        ach = null;

                        continue;
                    }
                }
            } while (true);

            for (int i = 0; i < totalAchievements; i++) {
                if (tmp[i] == null) {
                    throw new RuntimeException("null achievement @ " + i);
                }
            }

            Achievement[] tmpList = new Achievement[totalAchievements];

            ACHIEVEMENTS = new Achievement[totalAchievements];
            System.arraycopy(tmp, 0, ACHIEVEMENTS, 0, totalAchievements);

            System.arraycopy(tmp, 0, tmpList, 0, totalAchievements);

            sortAchievementsByDifficulty(tmpList);

            // reader.close();
        } catch (Exception ee) {
            System.err.println("erro on line " + line + ": " + lineNo);
            ee.printStackTrace();
        }
    }

    private static void output(String str) throws IOException {
        if (writer == null) {
            writer = new BufferedWriter(new FileWriter("config/ach.txt", true));
        }

        if (str.length() > 0) {
            writer.write(str);
        }

        writer.newLine();
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ach_id
     *
     * @return
     */
    public static final String getName(int ach_id) {
        if (ACHIEVEMENTS[ach_id] == null) {
            return "NULL";
        }

        return ACHIEVEMENTS[ach_id].title;
    }

    /**
     * Method getInfo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ach_id
     *
     * @return
     */
    public static final String getInfo(int ach_id) {
        if (ACHIEVEMENTS[ach_id] == null) {
            return "NULL";
        }

        return ACHIEVEMENTS[ach_id].information;
    }

    /**
     * Method getProgress
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ach_id
     *
     * @return
     */
    public static final int getProgress(int ach_id) {
        if (ACHIEVEMENTS[ach_id] == null) {
            return 0;
        }

        return ACHIEVEMENTS[ach_id].progress;
    }

    /**
     * Method getCash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ach_id
     *
     * @return
     */
    public static final int getCash(int ach_id) {
        if (ACHIEVEMENTS[ach_id] == null) {
            return 0;
        }

        return ACHIEVEMENTS[ach_id].cashReward;
    }

    /**
     * Method getTitle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getTitle() {
        return title;
    }

    /**
     * Method setTitle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Method getExtraReward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getExtraReward() {
        return extraReward;
    }

    /**
     * Method setExtraReward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param extraReward
     */
    public void setExtraReward(String extraReward) {
        this.extraReward = extraReward;
    }

    /**
     * Method getCashReward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCashReward() {
        return cashReward;
    }

    /**
     * Method setCashReward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cashReward
     */
    public void setCashReward(int cashReward) {
        this.cashReward = cashReward;
    }

    /**
     * Method getId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Method setId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method getProgress
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getProgress() {
        return progress;
    }

    /**
     * Method setProgress
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param progress
     */
    public void setProgress(int progress) {
        this.progress = progress;
    }
}
