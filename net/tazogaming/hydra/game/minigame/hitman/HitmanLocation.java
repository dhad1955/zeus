package net.tazogaming.hydra.game.minigame.hitman;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/01/14
 * Time: 17:35
 */
public class HitmanLocation {


    private static HitmanLocation cache[] = new HitmanLocation[60];
    private static int            cur_len = 0;
    private int                   id      = 0;
    private String                name;
    private Point                 location;

    /**
     * Constructs ...
     *
     *
     * @param n
     * @param loc
     * @param id
     */
    public HitmanLocation(String n, Point loc, int id) {
        this.location = loc;
        this.name     = n;
        this.id       = id;
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static HitmanLocation get(int id) {
        return cache[id];
    }

    /**
     * Method randomLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static HitmanLocation randomLocation() {
        return cache[GameMath.rand3(cur_len)];
    }

    /**
     * Method getID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getID() {
        return id;
    }

    /**
     * Method parseLocations
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void parseLocations() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("config/world/minigame/hitman_locations.cfg"));
            String         line   = null;

            while ((line = reader.readLine()) != null) {
                if (line.length() == 0) {
                    continue;
                }

                String   read      = line.substring(0, line.length() - 1);
                String   next_line = reader.readLine();
                String[] split     = next_line.split(" ");

                cache[cur_len] = new HitmanLocation(read,
                        Point.location(Text.parseInt(split[0]), Text.parseInt(split[1]), 0), cur_len++);
            }

            reader.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        Logger.log("Loaded " + cur_len + " hitman locations");
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method getLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getLocation() {
        return location;
    }
}
