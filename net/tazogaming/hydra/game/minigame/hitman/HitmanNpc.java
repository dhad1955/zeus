package net.tazogaming.hydra.game.minigame.hitman;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.HashMap;
import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/01/14
 * Time: 16:47
 */
public class HitmanNpc {
    private static HashMap[] npc_cache = new HashMap[4];    // <Integer, HitmanNpc>();//new HitmanNpc[70];
    private static int         hm_count  = 0;

    static {
        unpack();
    }

    private int    npc_id;
    private int    base_cash_reward;
    private String info;

    /**
     * Method randomNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param diffuclty
     *
     * @return
     */
    public static HitmanNpc randomNpc(int diffuclty) {
        Random   generator   = new Random();
        Object[] values      = npc_cache[diffuclty].values().toArray();
        Object   randomValue = values[generator.nextInt(values.length)];

        return (HitmanNpc) randomValue;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return NpcDef.FOR_ID(npc_id).getName().replace("(HM)", "");
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param difficulty
     * @param id
     *
     * @return
     */
    public static HitmanNpc get(int difficulty, int id) {
        return (HitmanNpc) npc_cache[difficulty].get(id);
    }

    /**
     * Method getCash
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCash() {
        return base_cash_reward;
    }

    /**
     * Method getInfo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getInfo() {
        return info;
    }

    /**
     * Method getNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNpc() {
        return npc_id;
    }

    /**
     * Method unpack
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void unpack() {
        try {
            BufferedReader reader =
                new BufferedReader(new FileReader(new File("config/world/minigame/hitman_npcs.cfg")));
            String   line        = null;
            int      cur_npc_id  = -1;
            boolean  parsing     = false;
            String[] tokens      = null;
            String   info        = "";
            int      diffuclty   = 0;
            int      base_reward = 0;

            while ((line = reader.readLine()) != null) {
                tokens = line.split(" ");

                if (line.startsWith("target")) {
                    cur_npc_id = Text.parseInt(tokens[1]);

                    continue;
                }

                if (line.startsWith("info")) {
                    tokens = line.split(" ", 3);
                    info   = tokens[2];

                    continue;
                }

                if (line.startsWith("base_reward")) {
                    base_reward = Text.parseInt(tokens[2]);

                    continue;
                }

                if (line.startsWith("difficulty")) {
                    diffuclty = Text.parseInt(tokens[2]);

                    continue;
                }

                if (line.contains("}")) {
                    HitmanNpc ret = new HitmanNpc();

                    ret.base_cash_reward = base_reward;
                    ret.info             = info;
                    ret.npc_id           = cur_npc_id;

                    if (npc_cache[diffuclty] == null) {
                        npc_cache[diffuclty] = new HashMap<Integer, HitmanNpc>();
                    }

                    npc_cache[diffuclty].put(ret.npc_id, ret);
                }
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}
