package net.tazogaming.hydra.game.minigame.hitman;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.npc.ai.movement.RoamingMovement;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/01/14
 * Time: 17:33
 */
public class HitmanGame {
    private int    last_track   = 0;
    private int    target_id    = 0;
    private int    time_started = 0;
    private int    diff         = 0;
    private int    loc_id       = 0;
    private Point  target_location;
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param id
     * @param location_id
     * @param diff
     */
    public HitmanGame(Player pla, int id, int location_id, int diff) {
        this.player          = pla;
        this.loc_id          = location_id;
        this.target_location = Point.location(HitmanLocation.get(location_id).getLocation().getX(),
                HitmanLocation.get(location_id).getLocation().getY(), 0);
        this.time_started = Core.currentTime;
        this.target_id    = id;
        this.diff         = diff;
        scrambleTarget();

        HitmanNpc target = HitmanNpc.get(diff, target_id);

        pla.getActionSender().sendMessage("Your target is " + target.getName() + " and he was last seen at "
                                          + HitmanLocation.get(location_id).getName());
    }

    /**
     * Method getSecsRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSecsRemaining() {
        return Core.getSecondsForTicks((time_started + (100 * 10) - Core.currentTime));
    }

    /**
     * Method getTimeString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getTimeString() {
        int ticks = getSecsRemaining();

        if (ticks > 60) {
            return (ticks / 60) + " min";
        }

        return ticks + " secs";
    }

    /**
     * Method build_interface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void build_interface(Player player) {
        StringBuffer buffer = new StringBuffer();
        HitmanNpc    npcz   = HitmanNpc.get(diff, target_id);
        int          len    = 0;

        player.getActionSender().changeLine(npcz.getName(), 23907);

        String[] split = npcz.getInfo().split(" ");

        for (String str : split) {
            if ((len + str.length() + 1) >= 60) {
                buffer.append("\\n");
                len = 0;
            }

            buffer.append(str);
            buffer.append(" ");
            len += str.length() + 1;
        }

        player.getActionSender().changeLine(buffer.toString(), 23905);
        player.getActionSender().npcPic(target_id, 23902);
        player.getActionSender().changeLine("Last known location: " + HitmanLocation.get(loc_id).getName(), 23906);
        player.getActionSender().changeLine("Time remaining: " + getTimeString(), 23908);
        player.getActionSender().changeLine("Current reward: " + GameMath.formatMoney(getReward()) + " gp", 23914);
        player.getWindowManager().showWindow(23900);
    }

    /**
     * Method generate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param difficulty
     *
     * @return
     */
    public static HitmanGame generate(Player pla, int difficulty) {
        HitmanNpc      target = HitmanNpc.randomNpc(difficulty);
        HitmanLocation loc    = HitmanLocation.randomLocation();

        return new HitmanGame(pla, target.getNpc(), loc.getID(), difficulty);
    }

    /**
     * Method trackTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void trackTarget() {
        if (Core.timeSince(last_track) < 40) {
            player.getActionSender().sendMessage("You can only track once every 25 sec");

            return;
        }

        last_track = Core.currentTime;

        int    dir       = Movement.getDirectionForWaypoints(player.getLocation(), target_location);
        String direction = "";

        switch (dir) {
        case Movement.NORTH :
            direction = "North";

            break;

        case Movement.NORTH_EAST :
            direction = "North east";

            break;

        case Movement.NORTH_WEST :
            direction = "North west";

            break;

        case Movement.EAST :
            direction = "East";

            break;

        case Movement.SOUTH_EAST :
            direction = "South east";

            break;

        case Movement.SOUTH_WEST :
            direction = "South west";

            break;

        case Movement.SOUTH :
            direction = "South";

            break;

        case Movement.WEST :
            direction = "West";

            break;
        }

        if (Point.getDistance(target_location, player.getLocation()) < 13) {
            player.getActionSender().sendMessage("Your target is very close by");

            return;
        }

        player.getActionSender().sendMessage("target location: " + target_location);
        Dialog.printStopMessage(player, "Your target is: @blu@" + direction, "Walk in this direction to get closer",
                                "Remember your target is on the move constantly!");
    }

    /**
     * Method updateTargetLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateTargetLocation() {
        if ((getSecsRemaining() <= 0) || player.isDead()) {
            player.getActionSender().sendMessage("You have failed your hitman task.");
            player.getAccount().setHitmanGame(null);

            return;
        }

        if (target_location != null) {
            if (Point.getDistance(target_location, player.getLocation()) < 11) {
                for (NPC npc : player.getLinked_npcs()) {
                    if (npc.getId() == target_id) {
                        return;
                    }
                }

                NPC target = new NPC(this.target_id, target_location, 400, NpcDef.FOR_ID(target_id));

                target.setQuestController(player);
                player.add_linked_npc(target, false);
                World.getWorld().registerNPC(target);
                target.setMovement(new RoamingMovement(target));
                target.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
                    @Override
                    protected void onDeath(NPC killed, Killable killedBy) {
                        killedBy.getPlayer().getActionSender().sendMessage("Your target has been destroyed.");
                        killedBy.getPlayer().getActionSender().sendMessage(
                            "Head back to the agency to get a new hitman task.");
                        target_location = null;
                        killedBy.getPlayer().getActionSender().sendMessage("You have been awarded "
                                + GameMath.formatMoney(getReward()));
                        killedBy.getPlayer().getInventory().addItemDrop(Item.forId(995), getReward());
                        killedBy.getPlayer().getAccount().setHitmanGame(null);

                        // To change body of implemented methods use File | Settings | File Templates.
                    }
                });

                return;
            }

            int random = GameMath.rand3(7);

            for (int i = 0; i < GameMath.rand(6); i++) {
                if (IfClipped.getMovementStatus(random, target_location.getX(), target_location.getY(), 0) == 1) {
                    target_location = Movement.getPointForDir(target_location, random);
                } else {
                    random = GameMath.rand3(7);
                }
            }
        }
    }

    /**
     * Method getReward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getReward() {
        int amt = HitmanNpc.get(diff, target_id).getCash();

        amt *= 1.7;

        int secs    = getSecsRemaining();
        int percent = (int)GameMath.getPercentFromTotal(secs, 600);
        int dub     = amt;

        dub *= (percent / 100d);
        amt += dub;

        return amt;
    }

    /**
     * Method scrambleTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void scrambleTarget() {
        int random = GameMath.rand3(7);

        for (int i = 0; i < GameMath.rand(30); i++) {
            if (IfClipped.getMovementStatus(random, target_location.getX(), target_location.getY(), 0) == 1) {
                target_location = Movement.getPointForDir(target_location, random);
            } else {
                random = GameMath.rand3(7);
            }
        }
    }
}
