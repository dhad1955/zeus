package net.tazogaming.hydra.game.minigame.castlewars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.entity3d.player.actions.MiningAction;

import static net.tazogaming.hydra.entity3d.player.actions.MiningAction.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/01/2015
 * Time: 10:45
 */
public class CastlewarsMiningAction extends Action {

    /** MAX_HEALTH made: 15/06/28 */
    private static final int MAX_HEALTH = 6;
    public static final int
        MODE_COLLAPSE                   = 0,
        MODE_CLEAR                      = 1;

    /** collapsingCave made: 15/06/28 */
    private CollapsingCave collapsingCave;

    /** mode made: 15/06/28 */
    private int mode;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param cave
     * @param mode
     */
    public CastlewarsMiningAction(Player player, CollapsingCave cave, int mode) {
        super(0, player);
        this.mode           = mode;
        this.collapsingCave = cave;
    }

    @Override
    protected void tick(int time) {
        int pickaxeId = MiningAction.getPickaxeId(getPlayer());

        if (pickaxeId == -1) {
            terminate();
            getPlayer().getActionSender().sendMessage("You don't have a pickaxe.");

            return;
        }

        if ((collapsingCave.getState() == CollapsingCave.COLLAPSED) && (mode == MODE_COLLAPSE)) {
            terminate();

            return;
        } else if ((collapsingCave.getState() == CollapsingCave.ACTIVE) && (mode == MODE_CLEAR)) {
            terminate();

            return;
        }

        getPlayer().getAnimation().animate(anims[pickaxeId]);

        int delay = (20 - (getPlayer().getCurStat(14) / 10) - (int) (pickaxeBonus[pickaxeId] * 1.60));

        if ((time % delay == 0) && (time != 0)) {
            collapsingCave.removeHealth();

            if (collapsingCave.getHealth() == 0) {
                if (mode == MODE_CLEAR) {
                    collapsingCave.reset();
                } else {
                    collapsingCave.collapse();
                }
            }

            return;
        }
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {
        getPlayer().getAnimation().animate(65535);
    }
}
