package net.tazogaming.hydra.game.minigame.castlewars;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/01/2015
 * Time: 21:14
 */
public class CastleWarsRespawnRoomEvent extends PlayerTickEvent {

    int time = 0;
    CastlewarsTeam team;
    public CastleWarsRespawnRoomEvent(Player player, CastlewarsTeam team){
           super(player, 1, false, true);
            this.team = team;

    }
    /**
     * Method doTick
     * Created on 14/08/18
     *
     * @param owner
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void doTick(Player owner) {
        if(!owner.isInArea(team.getDefinition().getRespawnArea()))
        {
            terminate();
            return;
        }else{
            time++;
        }
        if(time == Core.getTicksForMinutes(2))
        {
            getPlayer().teleport(3222, 3222 ,0);
            terminate();
            return;
        }

    }
}
