package net.tazogaming.hydra.game.minigame.castlewars;

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/01/2015
 * Time: 08:29
 */
public class Barricade {

    private NPC npc;
    private int fireTimer = -1;
    private boolean isUnlinked = false;
    private GameObject clientBlocker;



    public NPC getNPC() {
        return npc;
    }

    public boolean isBurning() {
        return this.fireTimer > 0;
    }
    public Barricade(Point location){
        this.npc = new NPC(1532, location, 0);
        NpcDef def = new NpcDef(1532);
        def.setAttackable(true);
        def.setBlockAnim(-1);
        def.setDeathAnim(-1);
        def.setStartingHealth(500);
        npc.setMaxHealth(500);
        npc.addHealth(500);
        // solid clipping

        clientBlocker = new GameObject();
        clientBlocker.setId(59);
        clientBlocker.setIdentifier(59);
        clientBlocker.setObjectType(10);
        clientBlocker.setLocation(location);
        npc.setDefinition(def);


        World.getWorld().getObjectManager().registerObject(clientBlocker);
        ClippingDecoder.addClipping(location.getX(), location.getY(), location.getHeight(), 0x20000);

        npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.FAST_ACTION) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                unlink();
            }
        });
        World.getWorld().registerNPC(npc);
    }

    public void unlink() {
        if(isUnlinked)
            return;
        ClippingDecoder.removeClipping(npc.getX(), npc.getY(), npc.getHeight(), 0x20000);
        npc.unlink();
        this.clientBlocker.remove();
        isUnlinked = true;
    }

    public boolean isUnlinked() {
        return isUnlinked;
    }

    public void tick() {
        if(Core.timeUntil(fireTimer) <= 0 && fireTimer > -1)
            unlink();

    }

    public void burn() {
        this.npc.setTransform(1533);
        this.fireTimer = Core.currentTime + 100;
    }


}
