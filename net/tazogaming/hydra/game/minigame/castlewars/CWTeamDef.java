package net.tazogaming.hydra.game.minigame.castlewars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Zone;

//~--- JDK imports ------------------------------------------------------------

import java.awt.geom.Area;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/01/2015
 * Time: 03:01
 */
public enum CWTeamDef {
    RED("Zamorak", Point.location(2373, 3131, 0), Point.location(2423, 9518, 0), 4042, 4903, 4901, 4039,
        new Zone(2410, 9513, 2430, 9536, 0, true, true, "zm_waitroom"),
        new Zone(2368, 3127, 2376, 3135, 1, true, true,
                 "Zamorak respawn room").setStatic(true).setHeightSensitive(true)), BLUE("Saradomin",
                     Point.location(2426, 3076, 1), Point.location(2380, 9488, 0), 4041, 4902, 4900, 4037,
                     new Zone(2368, 9482, 2394, 9498, 0, true, true, "Sara_waitroom").setStatic(true),
                     new Zone(2423, 3072, 2431, 3080, 1, true, true,
                              "sara_waitroom").setStatic(true).setHeightSensitive(true));

    /** capeID made: 15/06/28 **/
    private int capeID;

    /** name made: 15/06/28 **/
    private String name;

    /** startingLoc made: 15/06/28 **/
    private Point startingLoc;

    /** respawnArea made: 15/06/28 **/
    private Zone respawnArea;

    /** lobbyArea made: 15/06/28 **/
    private Zone lobbyArea;

    /** flagBseObject, flagGroundObject, flagItem made: 15/06/28 **/
    private int flagBseObject, flagGroundObject, flagItem;

    /** lobbyLoc made: 15/06/28 **/
    private Point lobbyLoc;

    CWTeamDef(String name, Point loc, Point loc2, int capeID, int flagBaseObject, int flagGroundObject, int flagItem,
              Zone lobbyArea, Zone respawnArea) {
        this.name             = name;
        this.lobbyLoc         = loc2;
        this.startingLoc      = loc;
        this.capeID           = capeID;
        this.flagBseObject    = flagBaseObject;
        this.flagGroundObject = flagGroundObject;
        this.flagItem         = flagItem;
        this.lobbyArea        = lobbyArea;
        this.respawnArea      = respawnArea;
    }

    /**
     * Method getStartingLocation
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getStartingLocation() {
        return startingLoc;
    }

    /**
     * Method getLobbyArea
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Zone getLobbyArea() {
        return lobbyArea;
    }

    /**
     * Method setLobbyArea
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lobbyArea
     */
    public void setLobbyArea(Zone lobbyArea) {
        this.lobbyArea = lobbyArea;
    }

    /**
     * Method getRespawnArea
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Zone getRespawnArea() {
        return respawnArea;
    }

    /**
     * Method setRespawnArea
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param respawnArea
     */
    public void setRespawnArea(Zone respawnArea) {
        this.respawnArea = respawnArea;
    }

    /**
     * Method getName
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method getCapeID
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCapeID() {
        return capeID;
    }

    /**
     * Method setCapeID
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param capeID
     */
    public void setCapeID(int capeID) {
        this.capeID = capeID;
    }

    /**
     * Method getLobbyLocation
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getLobbyLocation() {
        return lobbyLoc;
    }

    /**
     * Method getFlagBseObject
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFlagBseObject() {
        return flagBseObject;
    }

    /**
     * Method getStartingLoc
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getStartingLoc() {
        return startingLoc;
    }

    /**
     * Method getFlagItem
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFlagItem() {
        return flagItem;
    }

    /**
     * Method getFlagGroundObject
     * Created on 15/06/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFlagGroundObject() {
        return flagGroundObject;
    }
}
