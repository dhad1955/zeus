package net.tazogaming.hydra.game.minigame.castlewars;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.object.GameObject;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/01/2015
 * Time: 04:20
 */
public class Flag {
    public static final int PICKED_UP = 1, ON_GROUND = 2, SAFE = 0;

    private GameObject homeObject;
    private CWTeamDef def;


    public Flag(GameObject homeObject, CWTeamDef def){
        this.def = def;
        this.homeObject = homeObject;
        this.state = SAFE;
    }


    public void drop() {
        if(this.state != PICKED_UP){
            throw new RuntimeException("Error flag is not in picked up state.");
        }
        this.state = ON_GROUND;
        this.holder.getEquipment().set(-1, 0, 3);
        this.holder.getEquipment().updateEquipment(3);
        this.groundObject = new GameObject();
        this.groundObject.setId(def.getFlagGroundObject());
        this.groundObject.setIdentifier(def.getFlagGroundObject());

        this.groundObject.setObjectType(10);
        this.groundObject.setLocation(this.holder.getLocation());
        this.holder = null;
        World.getWorld().getObjectManager().registerObject(groundObject);

    }
    public void pickup(Player player){
        if(this.state == ON_GROUND){
            if(player.getEquipment().isWearing(3)){
                player.getEquipment().unwieldItem(3);
            }
            if(player.getEquipment().isWearing(3))
            {
                return;
            }
            this.groundObject.remove();
            this.groundObject = null;
            this.state = PICKED_UP;
            this.holder = player;
            this.holder.getEquipment().set(this.def.getFlagItem(), 1, 3);
            this.holder.getEquipment().updateEquipment(3);
        }
    }
    public void snatch(Player player) {
        this.state = PICKED_UP;
        this.holder = player;

        this.homeObject.changeIndex(4377);
        this.homeObject.next();
        this.holder.getEquipment().set(this.def.getFlagItem(), 1, 3);
        this.holder.getEquipment().updateEquipment(3);
    }


    public void reset() {
        if(this.holder != null){
            this.holder.getEquipment().set(-1, 0, 3);
            this.holder.getEquipment().updateEquipment(3);
            this.holder = null;
        }
        if(this.state == ON_GROUND){
            this.groundObject.remove();
        }
        this.state = SAFE;
        this.homeObject.changeIndex(def.getFlagBseObject());
        this.homeObject.next();

    }

    public GameObject getGroundObject() {
        return groundObject;
    }

    public GameObject getHomeObject() {
        return homeObject;
    }

    public int getState() {
        return state;
    }

    public Player getHolder() {
        return holder;
    }

    private GameObject groundObject;

    private int state;
    private Player holder;


}
