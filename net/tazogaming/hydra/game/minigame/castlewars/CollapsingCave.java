package net.tazogaming.hydra.game.minigame.castlewars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/01/2015
 * Time: 13:00
 */
public class CollapsingCave {
    public static final int
        ACTIVE                  = 0,
        COLLAPSED               = 1;
    int       rot               = -1;
    final int MAX_MINING_HEALTH = 6;

    /** status made: 15/01/12 **/
    private int status = COLLAPSED;

    /** definition made: 15/01/12 **/
    private RockFallDefinition definition;

    /** object made: 15/01/12 **/
    private GameObject object;

    /** miningHealth made: 15/01/12 **/
    private int miningHealth;

    /**
     * Constructs ...
     *
     *
     * @param definition
     */
    public CollapsingCave(RockFallDefinition definition) {
        this.definition = definition;
        this.object     = new GameObject();
        this.object.setLocation(definition.getImpactPoint());
        this.object.setObjectType(10);
        this.object.setIdentifier(4437);
        this.rot = object.getRotation();
        this.status = ACTIVE;
        this.collapse();
    }

    /**
     * Method getState
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getState() {
        return status;
    }
    private int health = MAX_MINING_HEALTH;

    public void removeHealth() {
        if(health > 0)
            health--;
    }

    public int getHealth() {
        return health;
    }

    /**
     * Method getDefinition
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RockFallDefinition getDefinition() {
        return definition;
    }

    /**
     * Method reset
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        this.object.changeIndex(0);
        this.object.setIsMapObject(false);
        for (int x = this.definition.getLowerX(); x <= this.getDefinition().getHigherX(); x++) {
            for (int y = this.getDefinition().getLowerY(); y <= this.getDefinition().getHigherY(); y++) {
                World.getWorld().getTile(x, y, 0).setClippingData(0);
            }
        }
        this.health = MAX_MINING_HEALTH;

        this.status = ACTIVE;
    }

    public Point getLocation() {
        return object.getLocation();
    }



    /**
     * Method collapse
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void collapse() {
        if (this.status == ACTIVE) {
            this.status = COLLAPSED;
            this.object = new GameObject();
            this.object.setIdentifier(4437);
            this.object.setObjectType(10);
            this.object.setCurrentRotation(rot);
            this.object.setLocation(this.definition.getImpactPoint());

            this.health = MAX_MINING_HEALTH;
            for (int x = this.definition.getLowerX(); x <= this.getDefinition().getHigherX(); x++) {
                for (int y = this.getDefinition().getLowerY(); y <= this.getDefinition().getHigherY(); y++) {
                    ClippingDecoder.addClipping(x, y, 0, 0x20000);
                }
            }

            World.getWorld().getObjectManager().registerObject(object);
            World.getWorld().submitEvent(new WorldTickEvent(1) {
                @Override
                public void tick() {}
                @Override
                public void finished() {
                    for (Player player : World.getWorld().getPlayers()) {
                        if ((player.getX() >= definition.getLowerX()) && (player.getY() >= definition.getLowerY())
                                && (player.getY() <= definition.getHigherY())
                                && (player.getX() <= definition.getHigherX())) {
                            player.getRoute().resetPath();
                            player.setActionsDisabled(true);
                            player.hit(player, 190, Damage.TYPE_HIT, 1);
                        }
                    }
                }
            });
        }
    }
}
