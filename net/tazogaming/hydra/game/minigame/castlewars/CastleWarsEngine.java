package net.tazogaming.hydra.game.minigame.castlewars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.player.actions.MiningAction;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.game.ui.HintIcon;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * FIX AREA CHECK OR ITS ALL FUCKED
 * -- FIX POTS
 * -- CAN USE QUICK TELE
 */

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/01/2015
 * Time: 03:07
 */
public class CastleWarsEngine implements RecurringTickEvent {
    public static final int   MAX_POINTS_TO_WIN    = 3;
    public static final int   GAME_TIME_IN_MINUTES = 10;
    public static final int   TIME_UNTIL_NEXT_GAME = 3;
    public static final int[] CW_ITEMS             = { 4045, 4047, 4049, 4051, 4053 };

    /** CASTLE_WARS_ZONE, CASTLE_WARS_UNDERGROUND_ZONE made: 15/01/09 */
    public static Zone CASTLE_WARS_ZONE = new Zone(2368, 3072, 2431, 3135, -1, true, true,
                                              "castle_wars").setTeleportAllowed(false),
                       CASTLE_WARS_UNDERGROUND_ZONE = new Zone(2361, 9472, 2447, 9543, 0, true, true,
                                                          "CW_ZONE").setTeleportAllowed(false);

    /** singleton made: 15/01/09 */
    private static CastleWarsEngine singleton = new CastleWarsEngine();

    /**
     * Method atObject
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     * @param x
     * @param y
     *
     * @return
     */
    private static final int
        COLLAPSING_CAVE     = 4448,
        EXPLOSIVE_POTION    = 4045,
        ENERGY_BARRIER_RED  = 4470,
        ENERGY_BARRIER_BLUE = 4469,
        BASE_STOLEN         = 4377,
        BASE_RED_FLAG       = 4902,
        GROUND_RED_FLAG     = 4900,
        BASE_BLUE_FLAG      = 4903,
        GROUND_BLUE_FLAG    = 4901;
    public static final int
        GUTHIX_PORTAL       = 4408,
        SARA_PORTAL         = 4388,
        ZAM_PORTAL          = 4387,
        SCORE_BOARD         = 4484;

    /**
     * Method tick
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param teamID
     * @param player
     */
    public static final int MAX_DIFF     = 2;
    public static final int ROCKS_FALLEN = 4437;

    /** caves made: 15/01/09 */
    private CollapsingCave[] caves = new CollapsingCave[] { new CollapsingCave(RockFallDefinition.WEST),
            new CollapsingCave(RockFallDefinition.EAST), new CollapsingCave(RockFallDefinition.NORTH),
            new CollapsingCave(RockFallDefinition.SOUTH) };

    /** castleWarsDeathHandler made: 15/01/09 */
    private DeathHandler castleWarsDeathHandler = new DeathHandler() {
        @Override
        public void handleDeath(Killable killed, Killable killedBy) {
            CastlewarsTeam team = CastleWarsEngine.getSingleton().getTeam(killed.getPlayer());

            killed.getPlayer().setActionsDisabled(false);
            killed.getPlayer().getAccount().increaseVar("cw_deaths", 1);
            killedBy.getPlayer().getAccount().increaseVar("cw_kills", 1);
            killed.getPlayer().getTickEvents().add(new CastleWarsRespawnRoomEvent(killed.getPlayer(), team));

            for (CastlewarsTeam team_check : CastleWarsEngine.getSingleton().teams) {
                if (team_check.getFlag().getHolder() == killed.getPlayer()) {
                    team_check.getFlag().drop();
                    messageGame("Your flag has been dropped", team_check);
                    gameUpdate = true;
                }
            }

            if (team != null) {
                killed.getPlayer().teleport(team.getDefinition().getStartingLoc().getX(),
                                            team.getDefinition().getStartingLoc().getY(), 1);
            }
        }
    };

    /** castlewarsTimer made: 15/01/09 */
    private int castlewarsTimer = Core.currentTime + Core.getTicksForMinutes(TIME_UNTIL_NEXT_GAME);

    /** state made: 15/01/09 */
    private CastleWarsGameState state = CastleWarsGameState.LOBBY_WAITING;

    /** teams made: 15/01/09 */
    private CastlewarsTeam[] teams = new CastlewarsTeam[2];

    /**
     * Method tick
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private boolean gameUpdate = false;

    /**
     * Constructs ...
     *
     */
    public CastleWarsEngine() {
        GameObject redFlag  = new GameObject();
        GameObject blueFlag = new GameObject();

        redFlag.setLocation(Point.location(2370, 3133, 3));
        blueFlag.setLocation(Point.location(2429, 3074, 3));
        redFlag.setIdentifier(4903);
        redFlag.setId(4903);
        redFlag.setObjectType(10);
        blueFlag.setIdentifier(4902);
        blueFlag.setObjectType(10);
        blueFlag.setId(4902);
        teams[0] = new CastlewarsTeam(CWTeamDef.BLUE, blueFlag);
        teams[1] = new CastlewarsTeam(CWTeamDef.RED, redFlag);
        Core.submitTask(this);

        for (CollapsingCave cave : caves) {
            if (cave != null) {
                cave.reset();
            }
        }
    }

    /**
     * Method getRockStatus
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param team
     *
     * @return
     */
    public int[] getRockStatus(Player player, CastlewarsTeam team) {
        if (team.getDefinition() == CWTeamDef.BLUE) {
            return new int[] { caves[0].getState(), caves[2].getState() };
        } else {
            return new int[] { caves[1].getState(), caves[3].getState() };
        }
    }

    /**
     * Constructs ...
     *
     */
    private int traverseStatus(int status) {
        if (status > 1) {
            throw new IndexOutOfBoundsException("Status can only be 0 or 1");
        }

        if (status == 1) {
            return 0;
        }

        return 1;
    }

    /**
     * Method getBarricadeForNpc
     * Created on 15/01/11
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net       f
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public Barricade getBarricadeForNpc(NPC npc) {
        for (CastlewarsTeam team : teams) {
            for (Barricade barricade : team.getBarricades()) {
                if (barricade.getNPC() == npc) {
                    return barricade;
                }
            }
        }

        return null;
    }

    /**
     * Method isOnBattlement
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     *
     * @return
     */
    public static boolean isOnBattlement(Point p) {
        return (((p.getX() >= 2414) && (p.getY() >= 3075) && (p.getX() <= 2416) && (p.getY() <= 3091))
                || ((p.getX() >= 2412) && (p.getY() >= 3087) && (p.getX() <= 2425)
                    && (p.getY() <= 3089))) || (((p.getX() >= 2374) && (p.getY() >= 3118) && (p.getX() <= 2387)
                        && (p.getY() <= 3120)) && ((p.getX() >= 2383) && (p.getY() >= 3116) && (p.getX() <= 2385)
                            && (p.getY() <= 3132)));
    }

    private void updateGame(Player player) {
        player.getActionSender().sendVar(380, Core.getSecondsForTicks(getTimeRemaining()) / 60);
        player.getActionSender().sendVarBit(153, teams[1].getFlag().getState());
        player.getActionSender().sendVarBit(155, teams[1].getScore());
        player.getActionSender().sendVarBit(143, teams[0].getFlag().getState());
        player.getActionSender().sendVarBit(145, teams[0].getScore());

        int[] caveStatus = getRockStatus(player, getTeam(player));

        player.getActionSender().sendVarBit(148, traverseStatus(caveStatus[0]));
        player.getActionSender().sendVarBit(149, traverseStatus(caveStatus[1]));

        HintIcon h           = getHintIcon(player, getTeam(player));
        HintIcon playersIcon = player.getGameFrame().getCurrentHintIcon();

        if ((h == null) && (playersIcon != null)) {
            player.getGameFrame().setCurrentHintIcon(null);
        } else if ((h != null) && (playersIcon != null) && (playersIcon.getFocusEntity() != h.getFocusEntity())) {
            player.getGameFrame().setCurrentHintIcon(h);
        } else if ((playersIcon != null) && (playersIcon.getLocation() != null) && (h.getLocation() != null)
                   &&!Point.equals(h.getLocation(), playersIcon.getLocation())) {
            ;
        }

        player.getGameFrame().setCurrentHintIcon(h);
    }

    /**
     * Method resetCaves
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetCaves() {
        for (CollapsingCave cave : caves) {
            if (cave != null) {
                cave.reset();
            }
        }
    }

    /**
     * Method atObject
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param itemid
     * @param objId
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public boolean itemAtObject(Player player, int itemid, int objId, int x, int y, int h) {
        if (itemid == EXPLOSIVE_POTION) {
            if (objId == ROCKS_FALLEN) {
                for (CollapsingCave cave : caves) {
                    if ((cave.getLocation().getX() == x) && (cave.getLocation().getY() == y)) {
                        if (cave.getState() == CollapsingCave.COLLAPSED) {
                            player.getInventory().deleteItem(EXPLOSIVE_POTION, 1);

                            for (int i = 0; i < 3; i++) {
                                cave.removeHealth();
                            }

                            if (cave.getHealth() == 0) {
                                cave.reset();

                                return true;
                            }
                        }
                    }

                    return true;
                }

                return true;
            }

            if (objId == 4448) {
                for (CollapsingCave cave : caves) {
                    for (Point p : cave.getDefinition().getBreakPoints()) {
                        if ((p.getX() == x) && (p.getY() == y)) {
                            player.getInventory().deleteItem(EXPLOSIVE_POTION, 1);
                            cave.collapse();

                            return true;
                        }
                    }
                }

                return true;
            }
        }

        return false;
    }

    /**
     * Method atObject
     * Created on 15/01/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     * @param x
     * @param y
     *
     * @return
     */
    public boolean atObject(Player player, int id, int x, int y) {
        if ((id == ZAM_PORTAL) || (id == SARA_PORTAL) || (id == GUTHIX_PORTAL)) {
            if (id == ZAM_PORTAL) {
                registerToLobby(1, player);
            } else if (id == SARA_PORTAL) {
                registerToLobby(0, player);
            }

            if (id == GUTHIX_PORTAL) {
                registerToLobby(-1, player);
            }

            return true;
        }

        if (!isPlaying(player)) {
            return false;
        }

        if (id == ROCKS_FALLEN) {
            for (CollapsingCave cave : caves) {
                if ((cave.getLocation().getX() == x) && (cave.getLocation().getY() == y)) {
                    player.setCurrentAction(new CastlewarsMiningAction(player, cave,
                            CastlewarsMiningAction.MODE_CLEAR));

                    return true;
                }
            }

            return true;
        }

        // Collapsing cave
        if (id == COLLAPSING_CAVE) {
            for (CollapsingCave cave : caves) {
                if (cave != null) {
                    if (cave.getState() == CollapsingCave.ACTIVE) {
                        for (Point p : cave.getDefinition().getBreakPoints()) {
                            if ((p.getX() == x) && (p.getY() == y)) {
                                if (player.getAction() == null) {
                                    if (cave.getState() == CollapsingCave.ACTIVE) {
                                        player.setCurrentAction(new CastlewarsMiningAction(player, cave,
                                                CastlewarsMiningAction.MODE_COLLAPSE));

                                        return true;
                                    }
                                }

                                return true;
                            }
                        }
                    }
                }
            }
        }

        if (id == ENERGY_BARRIER_RED) {
            if (getTeam(player).getDefinition() != CWTeamDef.RED) {
                return true;
            }

            for (CastlewarsTeam team : teams) {
                if (team.getFlag().getHolder() == player) {
                    return true;
                }
            }

            if ((x == 2376) && (y == 3131)) {
                if (player.getX() > 2376) {
                    player.teleport(2376, 3130, 1);
                } else {
                    player.teleport(2377, 3131, 1);
                }
            } else if ((x == 2373) && (y == 3127)) {
                if (player.getY() <= 3126) {
                    player.teleport(2373, 3128, 1);
                } else {
                    player.teleport(2373, 3126, 1);
                }
            }

            return true;
        } else if (id == ENERGY_BARRIER_BLUE) {
            for (CastlewarsTeam team : teams) {
                if (team.getFlag().getHolder() == player) {
                    return true;
                }
            }

            if (getTeam(player).getDefinition() != CWTeamDef.BLUE) {
                return true;
            }

            if ((x == 2426) && (y == 3080)) {
                if (player.getY() <= 3080) {
                    player.teleport(2426, 3081, 1);
                } else {
                    player.teleport(2426, 3080, 1);
                }
            } else if ((x == 2423) && (y == 3076)) {
                if (player.getX() >= 2423) {
                    player.teleport(2422, 3076, 1);
                } else {
                    player.teleport(2423, 3076, 1);
                }
            }
        }

        return false;
    }

    /**
     * Method flagCheck
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     * @param x
     * @param y
     */
    public void flagCheck(Player player, int id, int x, int y) {
        if (!isPlaying(player)) {
            return;
        }

        int teamId = -1;

        if (id == BASE_STOLEN) {
            if ((getTeam(player).getFlag().getHolder() == player)
                    && (getTeam(player).getFlag().getHomeObject().getId() == id)) {
                getTeam(player).getFlag().reset();
                gameUpdate = true;
            } else {
                if ((getOtherTeam(getTeam(player)).getFlag().getHolder() == player)
                        && (getTeam(player).getFlag().getHomeObject().getId() == id)) {
                    if (getTeam(player).getDefinition().getName().equalsIgnoreCase("zamorak")) {
                        teamId = 0;
                        id     = BASE_RED_FLAG;
                    } else {
                        teamId = 1;
                        id     = BASE_BLUE_FLAG;
                    }
                } else {
                    return;
                }
            }
        }

        if ((id == BASE_RED_FLAG) || (id == GROUND_RED_FLAG)) {
            teamId = 0;
        }

        if ((id == BASE_BLUE_FLAG) || (id == GROUND_BLUE_FLAG)) {
            teamId = 1;
        }

        if (teamId == -1) {
            return;
        }

        if ((id == GROUND_BLUE_FLAG) || (id == GROUND_RED_FLAG)) {
            if (getTeam(player).getDefinition().getFlagGroundObject() == id) {
                if (getTeam(player).getFlag().getState() == Flag.ON_GROUND) {
                    getTeam(player).getFlag().pickup(player);
                    gameUpdate = true;

                    return;
                }
            } else {
                for (CastlewarsTeam team : teams) {
                    if ((team.getFlag().getState() == Flag.ON_GROUND)
                            && (team.getDefinition().getFlagGroundObject() == id)) {
                        team.getFlag().pickup(player);
                        gameUpdate = true;

                        return;
                    }
                }
            }
        }

        if (teams[teamId].getPlayers().contains(player)) {
            int check = 0;

            if (teamId == 0) {
                check = 1;
            }

            if (teams[check].getFlag().getHolder() == player) {
                teams[check].getFlag().reset();

                CastlewarsTeam curWinner = getWinner();

                getTeam(player).addPoint();
                player.getAccount().increaseVar("cw_captures", 1);

                CastlewarsTeam newWinner = getWinner();

                messageGame(getTeam(player).getDefinition().getName() + " have captured the flag", null);

                if (curWinner != newWinner) {
                    messageGame(newWinner.getDefinition().getName() + " have taken the lead", null);
                }

                gameUpdate = true;
            }

            player.getActionSender().sendMessage("You can't take your own flag");

            return;
        }

        if (teams[teamId].getFlag().getState() != Flag.SAFE) {
            return;
        }

        if (player.getEquipment().checkUnwield(player.getEquipment().getId(3), 3)) {
            if (player.getEquipment().unwieldItem(3)) {
                messageGame("Your flag has been taken", teams[teamId]);
                teams[teamId].getFlag().snatch(player);
                gameUpdate = true;
            }
        }
    }

    /**
     * Method isInLobby
     * Created on 15/01/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isInLobby(Player player) {
        for (CastlewarsTeam team : teams) {
            if (team.getLobbyPlayers().contains(player)) {
                return true;
            }
        }

        return false;
    }

    private static boolean isBlocked(int i) {
        if (i < 0) {
            return false;
        }

        if ((i > 551) && (i < 568)) {
            return false;
        }

        Item item = Item.forId(i);

        if (item != null) {
            if (item.getName().toLowerCase().contains("potion")) {
                return false;
            }

            if ((item.getWieldSlot() == 3) && (item.getWeaponHandler() == null)) {
                return true;
            }
        }

        return false;
    }

    private static final boolean isFiltered(Player p) {
        for (Item i : p.getInventory().getItems()) {
            if ((i != null) && isBlocked(i.getIndex())) {
                return true;
            }
        }

        for (int i = 0; i < 14; i++) {
            if (isBlocked(p.getEquipment().getId(i))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method resetFlags
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetFlags() {
        for (CastlewarsTeam team : this.teams) {
            team.getFlag().reset();
        }
    }

    /**
     * Method startGame
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isPlaying(Player player) {
        if (player.isInArea(CASTLE_WARS_ZONE) || player.isInArea(CASTLE_WARS_UNDERGROUND_ZONE)) {
            return true;
        }

        for (CastlewarsTeam team : teams) {
            if (team.getPlayers().contains(player) || team.getLobbyPlayers().contains(player)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method startGame
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void startGame() {
        this.castlewarsTimer = Core.currentTime + Core.getTicksForMinutes(TIME_UNTIL_NEXT_GAME)
                               + Core.getTicksForMinutes(GAME_TIME_IN_MINUTES);
        state = CastleWarsGameState.IN_PLAY;
        World.globalMessage("[<col=9900CC>CastleWars</col>]: Castle-wars has started " + getAllLobbyPlayers().size());

        for (CastlewarsTeam team : teams) {
            for (Player player : team.getLobbyPlayers()) {
                registerGamePlayer(player, team);
            }

            team.getLobbyPlayers().clear();
        }
    }

    /**
     * Method end
     * Created on 15/01/11
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void end() {
        if (state == CastleWarsGameState.IN_PLAY) {
            endGame(null);
        }
    }

    /**
     * Method registerGamePlayer
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param team
     */
    public void registerGamePlayer(Player player, CastlewarsTeam team) {
        CWTeamDef def = team.getDefinition();

        player.getEquipment().set(def.getCapeID(), 1, Equipment.CAPE);
        player.getEquipment().updateEquipment(Equipment.CAPE);
        player.getEquipment().updateEquipment(Equipment.HAT);
        player.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
        player.getTickEvents().add(new CastleWarsRespawnRoomEvent(player, team));

        // teleport to game,
        player.teleport(def.getStartingLocation().getX(), def.getStartingLocation().getY(), 1);
        team.getPlayers().add(player);
        player.getGameFrame().openOverlay(59);
        player.setCurDeathHandler(castleWarsDeathHandler);
    }

    /**
     * Method getTimeRemaining
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTimeRemaining() {
        return Core.timeUntil(castlewarsTimer) - TIME_UNTIL_NEXT_GAME;
    }

    /**
     * Method getWinner
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public CastlewarsTeam getWinner() {
        if (teams[0].getScore() > teams[1].getScore()) {
            return teams[0];
        }

        if (teams[0].getScore() < teams[1].getScore()) {
            return teams[1];
        }

        return null;
    }

    /**
     * Method getSingleton
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static CastleWarsEngine getSingleton() {
        return singleton;
    }

    /**
     * Method unlink
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void unlink(Player player) {
        player.cancelDeaths();
        player.setCurrentHealth(player.getMaxHealth());
        player.getActionSender().sendStat(3);
        player.setPoisonTime(0);
        player.clearProjectiles();

        if (getAllGamePlayers().contains(player)) {
            for (CastlewarsTeam team : teams) {
                if (team.getFlag().getHolder() == player) {
                    team.getFlag().reset();
                }
            }
        }

        for (Integer i : CW_ITEMS) {
            if (player.getInventory().hasItem(i, 1)) {
                player.getInventory().deleteItem(i, Integer.MAX_VALUE);
            }
        }

        player.teleport(2443, 3091, 0);
        player.getEquipment().set(-1, 0, Equipment.CAPE);
        player.getEquipment().set(-1, 0, Equipment.HAT);
        player.getEquipment().updateEquipment(Equipment.CAPE);
        player.getEquipment().updateEquipment(Equipment.HAT);
        player.getGameFrame().closeOverlay();
        player.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "null");
        player.setCurDeathHandler(new DefaultDeathHandler());
    }

    /**
     * Method notifyFlag
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void notifyFlag(Player player) {
        for (CastlewarsTeam team : teams) {
            if (team.getFlag().getHolder() == player) {
                team.getFlag().drop();
                messageGame("Your flag has been dropped", team);
            }
        }

        gameUpdate = true;
    }

    /**
     * Method castleWarsCheck
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param fighting
     * @param message
     *
     * @return
     */
    public boolean castleWarsCheck(Player pla, Player fighting, boolean message) {
        if (!isPlaying(pla)) {
            return false;
        }

        CastlewarsTeam team         = getTeam(pla);
        CastlewarsTeam fightingTeam = getTeam(fighting);

        if ((team == null) || (fightingTeam == null)) {
            return false;
        }

        if (team == fightingTeam) {
            if (message) {
                pla.getActionSender().sendMessage("You can't attack your own team mates.");
            }

            return false;
        }

        if (pla.isInArea(team.getDefinition().getRespawnArea())) {
            if (message) {
                pla.getActionSender().sendMessage("You can't attack someone in the respawn area.");
            }

            for (Zone e : pla.getAreas()) {
                pla.getActionSender().sendMessage(e.getName());
            }

            return false;
        }

        if (fighting.isInArea(fightingTeam.getDefinition().getRespawnArea())) {
            return false;
        }

        if (pla.isInArea(CASTLE_WARS_ZONE) && fighting.isInArea(CASTLE_WARS_ZONE)) {
            return true;
        }

        if (pla.isInArea(CASTLE_WARS_UNDERGROUND_ZONE) && fighting.isInArea(CASTLE_WARS_UNDERGROUND_ZONE)) {
            return true;
        }

        return false;
    }

    /**
     * Method getTeam
     * Created on 15/01/11
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public CastlewarsTeam getTeam(Player player) {
        for (CastlewarsTeam team : teams) {
            if (team.getLobbyPlayers().contains(player) || team.getPlayers().contains(player)) {
                return team;
            }
        }

        return null;
    }

    /**
     * Method registerToLobby
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param teamID
     * @param player
     */
    public void registerToLobby(int teamID, Player player) {
        for (Player lobbyCheck : getAllLobbyPlayers()) {
            if (lobbyCheck.getUID() == player.getUID()) {
                Dialog.printStopMessage(player, "Unable to join game");

                return;
            }
        }

        if (player.getEquipment().isWearing(Equipment.HAT) || player.getEquipment().isWearing(Equipment.CAPE)) {
            player.getActionSender().sendMessage("You can't wear hats or capes in castle-wars.");

            return;
        }

        if (isFiltered(player)) {
            player.getActionSender().sendMessage("You can't take non-combat items into the arena.");

            return;
        }

        if (teamID == -1) {
            if (state == CastleWarsGameState.IN_PLAY) {
                if (teams[0].getPlayers().size() < teams[1].getPlayers().size()) {
                    registerToLobby(0, player);

                    return;
                } else {
                    registerToLobby(1, player);

                    return;
                }
            } else {
                if (teams[0].getLobbyPlayers().size() < teams[1].getLobbyPlayers().size()) {
                    registerToLobby(0, player);

                    return;
                } else {
                    registerToLobby(1, player);

                    return;
                }
            }
        }

        int opposite = traverseStatus(teamID);

        if (teams[opposite].getLobbyPlayers().size() < teams[teamID].getLobbyPlayers().size()) {
            int difference = teams[teamID].getLobbyPlayers().size() - teams[opposite].getLobbyPlayers().size();

            if (difference >= MAX_DIFF) {
                player.getActionSender().sendMessage("The teams are unbalanced please use the other team. "
                        + difference);

                return;
            }
        }

        if (!teams[teamID].getLobbyPlayers().contains(player)) {
            teams[teamID].getLobbyPlayers().add(player);
            player.getGameFrame().openOverlay(57);

            Account acc = player.getAccount();

            if (!acc.hasVar("cw_kills")) {
                acc.setSetting("cw_kills", 0, true);
            }

            if (!acc.hasVar("cw_captures")) {
                acc.setSetting("cw_captures", 0, true);
            }

            if (!acc.hasVar("cw_deaths")) {
                acc.setSetting("cw_deaths", 0, true);
            }

            if (!acc.hasVar("cw_losses")) {
                acc.setSetting("cw_losses", 0, true);
            }

            if (!acc.hasVar("cw_wins")) {
                acc.setSetting("cw_wins", 0, true);
            }

            if (!acc.hasVar("cw_games")) {
                acc.setSetting("cw_games", 0, true);
            }

            Point startLoc = teams[teamID].getDefinition().getLobbyLocation();

            player.teleport(startLoc.getX(), startLoc.getY(), 0);

            Zone lobbyZone = teams[teamID].getDefinition().getLobbyArea();

            if (!player.getAreas().contains(lobbyZone)) {
                player.getAreas().add(lobbyZone);
            }
        }
    }

    /**
     * Method tick
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        int time = Core.timeUntil(castlewarsTimer);
        int size = getAllLobbyPlayers().size();

        if (size > 3) {
            if (time == Core.getTicksForMinutes(5)) {
                World.globalMessage("[<col=9900CC>CastleWars</col>]: Castle-wars starts in 5 minutes " + size
                                    + " players ready");
            }

            if (time == Core.getTicksForMinutes(1)) {
                World.globalMessage("[<col=9900CC>CastleWars</col>]: Castle-wars starts in 1 minute " + size
                                    + " players ready");
            }
        }

        // Update the lobbies.
        for (CastlewarsTeam team : teams) {
            for (Iterator<Player> players = team.getLobbyPlayers().iterator(); players.hasNext(); ) {
                Player player = players.next();

                if (!player.isInArea(team.getDefinition().getLobbyArea()) ||!player.isLoggedIn()) {
                    players.remove();
                    unlink(player);
                }

                if (team.getPlayers().size() < getOtherTeam(team).getPlayers().size()) {
                    registerGamePlayer(player, team);
                    players.remove();

                    continue;
                }

                if (Core.currentTime % 30 == 0) {
                    player.getGameFrame().sendString(getLobbyStatus(), 57, 0);
                }
            }

            for (Iterator<Barricade> barricadesIterator =
                    team.getBarricades().iterator(); barricadesIterator.hasNext(); ) {
                Barricade barricade = barricadesIterator.next();

                if (barricade.isUnlinked()) {
                    barricadesIterator.remove();
                } else {
                    barricade.tick();
                }
            }
        }

        // update the players in game,
        if (state == CastleWarsGameState.IN_PLAY) {
            if (getTimeRemaining() <= 0) {
                endGame(getWinner());

                return;
            }

            if (getAllGamePlayers().size() == 0) {
                endGame(null);

                return;
            }

            for (CastlewarsTeam team : teams) {

                // Has the team won ?
                if (team.getScore() >= MAX_POINTS_TO_WIN) {
                    endGame(team);

                    return;
                }

                for (Iterator<Player> players = team.getPlayers().iterator(); players.hasNext(); ) {
                    Player player = players.next();

                    if ((Core.currentTime % 30 == 0) || gameUpdate) {
                        updateGame(player);
                    }

                    if ((!player.isInArea(CASTLE_WARS_UNDERGROUND_ZONE) &&!player.isInArea(CASTLE_WARS_ZONE))
                            ||!player.isLoggedIn()) {
                        unlink(player);
                        players.remove();
                    }
                }
            }
        } else if (state == CastleWarsGameState.LOBBY_WAITING) {
            if (Core.timeUntil(castlewarsTimer) <= 0) {
                if (getAllLobbyPlayers().size() < 6) {
                    castlewarsTimer = Core.currentTime + Core.getTicksForMinutes(TIME_UNTIL_NEXT_GAME);
                } else {
                    startGame();

                    return;
                }
            }
        }

        gameUpdate = false;
    }

    private CastlewarsTeam getOtherTeam(CastlewarsTeam team) {
        if (teams[0] == team) {
            return teams[1];
        }

        return teams[0];
    }

    /**
     * Method getHintIcon
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param team
     *
     * @return
     */
    public HintIcon getHintIcon(Player player, CastlewarsTeam team) {
        if (teams[0] == team) {
            team = teams[1];
        } else {
            team = teams[0];
        }

        if (team.getFlag().getHolder() == player) {
            return null;
        }

        if ((team.getFlag().getState() == Flag.ON_GROUND)
                && (team.getFlag().getGroundObject().getHeight() == player.getHeight())) {
            return new HintIcon(team.getFlag().getGroundObject().getLocation(), 10, 1);
        } else if (team.getFlag().getState() == Flag.PICKED_UP) {
            return new HintIcon(team.getFlag().getHolder());
        } else {
            return null;
        }
    }

    private String getLobbyStatus() {
        if (getAllLobbyPlayers().size() < 6) {
            return "Need at least 6 players to start";
        }

        return "Game starts in " + (Core.getSecondsForTicks(Core.timeUntil(castlewarsTimer) / 60) + 1) + " minutes";
    }

    private List<Player> getAllGamePlayers() {
        List<Player> tmp = new ArrayList<Player>();

        tmp.addAll(teams[0].getPlayers());
        tmp.addAll(teams[1].getPlayers());

        return tmp;
    }

    /**
     * Method messageGame
     * Created on 15/01/13
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mes
     * @param filter
     */
    public void messageGame(String mes, CastlewarsTeam filter) {
        if (filter == null) {
            for (CastlewarsTeam team : teams) {
                for (Player player : team.getPlayers()) {
                    player.getActionSender().sendMessage("[<col=9900CC>CastleWars</col>]:" + mes);
                }
            }
        } else {
            for (Player player : filter.getPlayers()) {
                player.getActionSender().sendMessage("[<col=9900CC>CastleWars</col>]:" + mes);
            }
        }
    }

    private List<Player> getAllLobbyPlayers() {
        List<Player> tmp = new ArrayList<Player>();

        tmp.addAll(teams[0].getLobbyPlayers());
        tmp.addAll(teams[1].getLobbyPlayers());

        return tmp;
    }

    private void endGame(CastlewarsTeam winningTeam) {
        if (winningTeam != null) {
            World.globalMessage("[<col=9900CC>CastleWars</col>]: Castle-wars has ended");

            for (Player player : winningTeam.getPlayers()) {
                for (Integer i : CW_ITEMS) {
                    if (player.getInventory().hasItem(i, 1)) {
                        player.getInventory().deleteItem(i, Integer.MAX_VALUE);
                    }
                }

                player.getAccount().increaseVar("cw_wins", 1);
                player.getInventory().addItem(4067, 3);
            }
        } else {
            for (CastlewarsTeam team : teams) {
                for (Player p : team.getPlayers()) {
                    for (Integer i : CW_ITEMS) {
                        if (p.getInventory().hasItem(i, 1)) {
                            p.getInventory().deleteItem(i, Integer.MAX_VALUE);
                        }
                    }

                    p.getInventory().addItem(4067, 1);
                }
            }
        }

        for (CastlewarsTeam team : teams) {
            if (team != winningTeam) {
                for (Player pl : team.getPlayers()) {
                    for (Integer i : CW_ITEMS) {
                        if (pl.getInventory().hasItem(i, 1)) {
                            pl.getInventory().deleteItem(i, Integer.MAX_VALUE);
                        }
                    }

                    pl.getInventory().addItem(4067, 1);
                }
            }
        }

        for (CastlewarsTeam team : teams) {
            for (Player pla : team.getPlayers()) {
                pla.getAccount().increaseVar("cw_games", 1);
                unlink(pla);
            }

            for (Barricade barricade : team.getBarricades()) {
                barricade.unlink();
            }

            team.getBarricades().clear();
            team.getFlag().reset();
            team.getPlayers().clear();
            team.reset();
        }

        for (CollapsingCave cave : caves) {
            cave.reset();
        }

        this.castlewarsTimer = Core.currentTime + Core.getTicksForMinutes(TIME_UNTIL_NEXT_GAME);
        state                = CastleWarsGameState.LOBBY_WAITING;
    }

    /**
     * Method terminate
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;
    }
}
