package net.tazogaming.hydra.game.minigame.castlewars;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/01/2015
 * Time: 03:16
 */
public enum  CastleWarsGameState {
LOBBY_WAITING, IN_PLAY
}
