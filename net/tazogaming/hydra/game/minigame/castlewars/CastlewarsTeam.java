package net.tazogaming.hydra.game.minigame.castlewars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.game.ui.HintIcon;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/01/2015
 * Time: 03:09
 */
public class CastlewarsTeam {

    /** players made: 15/01/09 */
    private List<Player> players = new ArrayList<Player>();

    /** lobbyPlayers made: 15/01/12 **/
    private List<Player> lobbyPlayers = new ArrayList<Player>();

    /** barricades made: 15/01/12 **/
    private List<Barricade> barricades = new ArrayList<Barricade>();

    /** definition made: 15/01/12 **/
    private CWTeamDef definition;

    /** flag made: 15/01/12 **/
    private Flag flag;

    /** score made: 15/01/09 */
    private int score;

    /**
     * Constructs ...
     *
     *
     * @param definition
     * @param flag
     */
    public CastlewarsTeam(CWTeamDef definition, GameObject flag) {
        this.definition = definition;
        this.flag       = new Flag(flag, definition);
    }

    /**
     * Method getFlag
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Flag getFlag() {
        return flag;
    }

    /**
     * Method setupBarricade
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean setupBarricade(Player player) {
        if (barricades.size() == 10) {
            player.getActionSender().sendMessage("Your team already has 10 barricades set up");

            return true;
        }

        if (player.getHeight() == 1) {
            if (((player.getLocation().getX() == 2422) && (player.getLocation().getY() == 3076))
                    || ((player.getLocation().getX() == 2426) && (player.getLocation().getY() == 3081))
                    || (player.getLocation().getX() == 2373 && player.getY() == 3126)) {
                player.getActionSender().sendMessage("You can't set up a barricade here");
            }
        }

        if (World.getWorld().getTile(player.getLocation()).containsNPCChunks()) {
            player.getActionSender().sendMessage("You can't set up a barricade here.");

            return false;
        }

        if (player.isInArea(definition.getRespawnArea())) {
            return false;
        }

        this.barricades.add(new Barricade(player.getLocation()));

        return true;
    }

    /**
     * Method getLobbyPlayers
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Player> getLobbyPlayers() {
        return lobbyPlayers;
    }

    /**
     * Method getDefinition
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public CWTeamDef getDefinition() {
        return definition;
    }

    /**
     * Method getBarricades
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Barricade> getBarricades() {
        return barricades;
    }

    /**
     * Method reset
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        this.score = 0;
    }

    /**
     * Method getPlayers
     * Created on 15/01/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Player> getPlayers() {
        return players;
    }

    /**
     * Method addPoint
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void addPoint() {
        score++;
    }

    /**
     * Method getScore
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getScore() {
        return score;
    }
}
