package net.tazogaming.hydra.game.minigame.castlewars;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/01/2015
 * Time: 12:52
 */
public enum RockFallDefinition {
    WEST(2391, 9501, 2392, 9502, Point.location(2391, 9501, 0), Point.location(2390, 9503, 0),
         Point.location(2393, 9503, 0), Point.location(2390, 9500, 0), Point.location(2393, 9500, 0)), EAST(2409, 9503,
             2410, 9504, Point.location(2409, 9503, 0), Point.location(2408, 9505, 0), Point.location(2411, 9505, 0),
             Point.location(2408, 9502, 0), Point.location(2411, 9502, 0)), SOUTH(2401, 9494, 2402, 9495,
                 Point.location(2401, 9494, 0), Point.location(2403, 9496, 0), Point.location(2403, 9493, 0),
                 Point.location(2400, 9496, 0), Point.location(2400, 9493, 0)), NORTH(2400, 9512, 2401, 9513,
                     Point.location(2400, 9512, 0), Point.location(2399, 9511, 0), Point.location(2402, 9511, 0),
                     Point.location(2402, 9514, 0), Point.location(2399, 9514, 0));

    /** lowerX, lowerY, higherX, higherY made: 15/01/09 **/
    private int lowerX, lowerY, higherX, higherY;

    /** breakPoints made: 15/01/09 **/
    private Point[] breakPoints;

    /** impactPoint made: 15/01/09 **/
    private Point impactPoint;

    RockFallDefinition(int lowerX, int lowerY, int higherX, int higherY, Point impactPoint, Point... breakPoints) {
        this.lowerX      = lowerX;
        this.lowerY      = lowerY;
        this.higherX     = higherX;
        this.impactPoint = impactPoint;
        this.higherY     = higherY;
        this.breakPoints = breakPoints;
    }

    /**
     * Method getLowerX
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLowerX() {
        return lowerX;
    }

    /**
     * Method getLowerY
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLowerY() {
        return lowerY;
    }

    /**
     * Method getImpactPoint
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getImpactPoint() {
        return impactPoint;
    }

    /**
     * Method getHigherX
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHigherX() {
        return higherX;
    }

    /**
     * Method getHigherY
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHigherY() {
        return higherY;
    }

    /**
     * Method getBreakPoints
     * Created on 15/01/09
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point[] getBreakPoints() {
        return breakPoints;
    }
}
