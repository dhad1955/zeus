package net.tazogaming.hydra.game.minigame;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/03/2015
 * Time: 20:34
 */
public class SkeletalHorror implements RecurringTickEvent {

    private static SkeletalHorror singleTon = new SkeletalHorror();

    static {
        Core.submitTask(singleTon);
    }

    /**
     * locations made: 15/03/23 *
     */
    private static Point[] locations = new Point[]{Point.location(3236, 3944, 0),
            Point.location(3358, 3645, 0), Point.location(3007, 3628, 0)};

    public static void init() {}
    /**
     * locationNames made: 15/03/23 *
     */
    private static String[] locationNames = new String[]{"Scorpion pit", "South of East Dragons", "Castle near west dragons!"};
    public static final int COOLDOWN_TIME = Core.getTicksForMinutes(120);
    public static final int MAX_LIFE_TIME = Core.getTicksForMinutes(7);

    /**
     * PK_SHOP_ID made: 15/03/23 *
     */
    private static final int PK_SHOP_ID = 34;

    /**
     * PRAYER_EXPERIENCE made: 15/03/23 *
     */
    private static final int PRAYER_EXPERIENCE = 1000;

    /**
     * SLAYER_EXPERIENCE made: 15/03/23 *
     */
    private static final int SLAYER_EXPERIENCE = 1100;

    /**
     * SLAYER_LEVEL_TO_KILL made: 15/03/23 *
     */
    private static final int SLAYER_LEVEL_TO_KILL = 80;

    /**
     * CASH_AMOUNT made: 15/03/23 *
     */
    private static final int CASH_AMOUNT = 25000000;

    /**
     * skeletalNpc made: 15/03/23 *
     */
    private NPC skeletalNpc;

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private int spawnIn;

    /**
     * spawnTime made: 15/03/23 *
     */
    private int spawnTime;

    private void spawnHorror() {
        if (this.skeletalNpc != null) {
            this.skeletalNpc.unlink();
        }

        int ran = GameMath.rand3(locationNames.length);

        this.skeletalNpc = createHorror(locations[ran], this);
        World.getWorld().registerNPC(skeletalNpc);
        World.getWorld().globalMessage(Text.RED("The skeletal horror has spawned at " + locationNames[ran]
                + " must be killed within 7 minutes!"));
        spawnTime = Core.currentTime;
    }

    private static NPC createHorror(Point location, final SkeletalHorror horror) {
        NPC npc = new NPC(9176, location, 30);

        npc.setDefinition(new NpcDef(9176));
        npc.getDefinition().setAttackable(true);
        npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                killedBy.getPlayer().addXp(5, PRAYER_EXPERIENCE);
                killedBy.getPlayer().addXp(18, SLAYER_EXPERIENCE);

                if (new Random().nextInt(100) > 85) {
                    new FloorItem(995, CASH_AMOUNT, killed.getX(), killed.getY(), killed.getHeight(),
                            (Player) killedBy);
                    new FloorItem(15389, 1, killed.getX(), killed.getY(), killed.getHeight(), (Player) killedBy);
                } else {
                	int item = ShopManager.getShop(PK_SHOP_ID).randomItem();
                    int amt = ShopManager.getShop(PK_SHOP_ID).getAmount(ShopManager.getShop(PK_SHOP_ID).getItemSlot(item));

                    if(item == 20455)
                        item = 14484;

                    String name = Item.forId(item).getName();

                    World.globalMessage(Text.RED(killedBy.getPlayer().getUsername() + " has just got x" + amt + " " + name
                            + " drop from the Skeletal horror."));
                    new FloorItem(item, amt, killed.getX(), killed.getY(), killed.getHeight(), (Player) killedBy);

                    World.globalMessage(Text.RED("Next spawn in 2 hours!"));
                }

                horror.reset();
            }
        });
        return npc;
    }
    private void reset() {
       if(this.skeletalNpc != null) {
           this.skeletalNpc.unlink();
           this.skeletalNpc = null;
       }
        spawnIn          = Core.currentTime;
    }

    /**
     * Method tick
     * Created on 15/03/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        if (this.skeletalNpc != null) {
            if (Core.timeSince(spawnTime) > MAX_LIFE_TIME) {
                World.globalMessage(
                    Text.RED("The skeletal horror has taken too long to kill, next spawn in 2 hours!"));
                reset();
            }
        } else {
            if (Core.timeSince(spawnIn) > COOLDOWN_TIME) {
                spawnHorror();
            }
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
        return false;
    }
}
