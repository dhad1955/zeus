package net.tazogaming.hydra.game.minigame;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.ui.HintIcon;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.WildernessOverlay;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;

//~--- JDK imports ------------------------------------------------------------

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/11/2014
 * Time: 04:22
 */
public class Targetting {

    /** TARGET_RANGE made: 14/11/22 **/
    private static final int TARGET_RANGE = 650;



    public static void checkTargets(Player player){
            if((player.getBHTarget() != null && !isValidTarget(player, player.getBHTarget())) || (player.isInWilderness() && player.getBHTarget() == null))
                setNextTarget(player);

            updateBounty(player);
    }


   public static void toggleBounty(Player player) {
        if(player.getAccount().hasVar("bounty"))
            player.getAccount().removeVar("bounty");
        else
            player.getAccount().setSetting("bounty", 1, true);

        player.setTarget(null);
        WildernessOverlay.sendBounty(player);
    }

    private static void setNextTarget(Player player){
        Player nextTarget = getTarget(player);
        player.setTarget(nextTarget);

        WildernessOverlay.sendBounty(player);
        if(nextTarget != null)
            player.getGameFrame().setCurrentHintIcon(new HintIcon(nextTarget));
        else
            player.getGameFrame().setCurrentHintIcon(null);
    }

    public static void updateBounty(Player player){
        if(!player.getAccount().hasVar("bounty"))
                return;

            if(!player.isInWilderness() || player.getBHTarget() == null){
            player.getGameFrame().setCurrentHintIcon(null);
            return;
        }
        if(Point.getDistance(player.getBHTarget().getLocation(), player.getLocation()) < 14){
            if(Core.currentTime % 8 == 0){
                player.getGameFrame().setCurrentHintIcon(new HintIcon(player.getBHTarget()));
            }
        }else{
            if(Core.currentTime % 10 == 0){
                player.getGameFrame().setCurrentHintIcon(new HintIcon(player.getBHTarget().getLocation()));
            }
        }
    }

    private static boolean isValidTarget(Player player, Player possibleTarget) {
        if (!possibleTarget.canBeAttacked(player)) {
            return false;
        }




        /* Is the user logged in? */
        if (!possibleTarget.isVisible()) {
            return false;
        }

        if (Point.getDistance(player.getLocation(), possibleTarget.getLocation()) > TARGET_RANGE) {
            return false;
        }

        if (possibleTarget.getRights() >= Player.ADMINISTRATOR) {
            return false;
        }

        if (!Combat.canAttack(player, possibleTarget, Combat.getWildernessLevel(possibleTarget),
                Combat.getWildernessLevel(possibleTarget))) {
            return false;
        }

        return true;
    }

    /**
     * Method getTarget
     * Created on 14/11/22
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Player getTarget(final Player player) {

        List<Player> targets = getPossibleTargets(player);

        if(targets.size() == 0)
            return null;

        Collections.sort(targets, new Comparator<Player>() {
            @Override
            public int compare(Player o1, Player o2) {

                  if(player.getAccount().getKillCount(o1.getUID()) == player.getAccount().getKillCount(o2.getUID()))
                      return 0;
                  if(player.getAccount().getKillCount(o1.getUID()) < player.getAccount().getKillCount(o2.getUID()))
                      return 1;
                return -1;
            }
        });

        return targets.get(0);
    }



    private static List<Player> getPossibleTargets(Player player) {
        List<Player> tmp = new LinkedList<Player>();

        for (Player possibleTarget : World.getWorld().getPlayers()) {
            if ((possibleTarget == player) ||!possibleTarget.isInWilderness()) {
                continue;
            }

            if (!isValidTarget(player, possibleTarget)) {
                continue;
            }

            tmp.add(possibleTarget);
        }

        return tmp;
    }
}
