package net.tazogaming.hydra.game.minigame.barrows;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.game.LootTable;
import net.tazogaming.hydra.util.collections.Array;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/11/13
 * Time: 05:09
 */
public class BarrowsGame {

    /** cryps made: 14/09/04 **/
    private static ArrayList<Crypt> cryps  = new ArrayList<Crypt>();
    protected static final int      VERAC  = 0;    // 2030
    protected static final int      DHAROK = 1;    // 2026
    protected static final int      AHRIM  = 2;    // 2025
    protected static final int      GUTHAN = 3;    // 2027
    protected static final int      KARIL  = 4;    // 2028
    protected static final int      TORAG  = 5;    // 2029

    static {
        addCrypt(new Crypt(3556, 3297).setBossId(2030).setId(0).setTombCoords(3578,
                           9706).setTombId(6823).setLadderId(6707));
        addCrypt(new Crypt(3575, 3298).setBossId(2026).setId(1).setTombCoords(3556,
                           9718).setTombId(6771).setLadderId(6703));
        addCrypt(new Crypt(3564, 3289).setBossId(2025).setId(2).setTombCoords(3557,
                           9703).setTombId(6821).setLadderId(6702));
        addCrypt(new Crypt(3553, 3282).setBossId(2029).setId(3).setTombCoords(3568,
                           9683).setTombId(6772).setLadderId(6706));
        addCrypt(new Crypt(3566, 3275).setBossId(2028).setId(4).setTombCoords(3546,
                           9684).setTombId(6822).setLadderId(6705));
        addCrypt(new Crypt(3577, 3282).setBossId(2027).setId(5).setTombCoords(3534,
                           9704).setTombId(6773).setLadderId(6704));
    }

    /**
     * Method onDig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static boolean onDig(Player pla) {
        if (pla.isInZone("barrows_digsite")) {
            for (Crypt cry : cryps) {
                if (Point.getDistance(pla.getX(), pla.getY(), cry.getDigX(), cry.getDigY()) < 5) {
                    pla.getActionSender().sendMessage("You dig into a crypt");
                    pla.teleport(cry.getCryptX(), cry.getCryptY(), 3);
                    pla.getAnimation().animate(-1);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method onObjectClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param h
     * @param p
     *
     * @return
     */
    public static boolean onObjectClick(int id, int x, int y, int h, Player p) {
        if (h != 3) {
            return false;
        }

        for (Crypt c : cryps) {
            if (c.getLadderID() == id) {
                p.teleport(c.getDigX(), c.getDigY(), 0);

                return true;
            }

            if (c.getCryptId() == id) {
                if (!p.getAccount().hasArray("barrows_kc")) {
                    p.getAccount().addArray("barrows_kc", 6, false);
                }

                if (!p.getAccount().getArray("barrows_kc").contains(c.getBossId())) {
                    if (p.getLinked_npcs().size() != 0) {
                        return false;
                    }

                    NpcAutoSpawn spawn = new NpcAutoSpawn();

                    spawn.setNpcId(c.getBossId());
                    spawn.setSpawnY(p.getY());
                    spawn.setSpawnX(p.getX());
                    spawn.setSpawnHeight(p.getHeight());
                    spawn.setRoamRange(4);

                    NPC brother = new NPC(c.getBossId(), p.getLocation(), spawn);    // , false);

                    World.getWorld().registerNPC(brother);
                    brother.setQuestController(p);
                    brother.setDeathHandler(new NpcDeathHandler() {
                        @Override
                        protected void onDeath(NPC killed, Killable killedBy) {
                            setPolicy(DeathPolicy.UNLINK_ON_DEATH);
                            killedBy.getPlayer().getLinked_npcs().remove((killed.getNpc()));
                            BarrowsGame.npcKilled((NPC) killed, (killedBy instanceof Player)
                                    ? (Player) killedBy
                                    : ((NPC) killedBy).getSummoner());

                            // To change body of implemented methods use File | Settings | File Templates.
                        }
                    });
                    p.add_linked_npc(brother, false);

                    Point safeTele = Movement.getTeleLocation(p.getLinked_npcs().get(0), p);

                    if (safeTele != null) {
                        p.getLinked_npcs().get(0).teleport(safeTele.getX(), safeTele.getY(), safeTele.getHeight());
                    }

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method slayedBrothers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static int slayedBrothers(Player pla) {
        if (!pla.getAccount().hasArray("barrows_kc")) {
            pla.getAccount().addArray("barrows_kc", 6, false);
        }

        int   config = 0;
        Array ar     = pla.getAccount().getArray("barrows_kc");

        for (Iterator<Object> kcIterator = ar.iterator(); kcIterator.hasNext(); ) {
            Integer i = (Integer) kcIterator.next();

            config |= 1 << (i - 2025);
        }

        int killCount = ar.size();

        return (killCount << 17 | config);
    }

    /**
     * Method brotherForIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public static int brotherForIndex(int index) {
        switch (index) {
        case 0 :
            return 2030;

        case 1 :
            return 2026;

        case 2 :
            return 2025;

        case 3 :
            return 2027;

        case 4 :
            return 2028;

        case 5 :
            return 2029;
        }

        return 0;
    }

    /**
     * Method npcKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param player
     */
    static void npcKilled(NPC npc, Player player) {
        for (Crypt c : cryps) {
            if (c.getBossId() == npc.getId()) {
                player.getAccount().getArray("barrows_kc").add(npc.getId());

                if (player.getAccount().getArray("barrows_kc").size() == cryps.size()) {
                    player.getAccount().getArray("barrows_kc").clear();
                    player.getActionSender().sendVar(453, 0);

                    LootTable l = LootTable.getTable(555);

                    if (l != null) {
                        player.getActionSender().clearInterface(6963);

                        Loot[] loot     = l.getLoot(2, player);
                        Loot[] always   = l.getLootAlways();
                        Item[] items    = new Item[always.length + 1];
                        int[]  amts     = new int[always.length + 1];
                        int[]  intItems = new int[always.length + 1];
                        int    count    = 0;

                        for (int i = 0; i < always.length; i++) {
                            items[count]    = Item.forId(always[i].getItemID());
                            intItems[count] = always[i].getItemID();
                            amts[count++]   = always[i].getAmount();
                        }

                        Loot win = loot[0];

                        if (GameMath.rand(100) > 40) {
                            win = l.randomLoot(LootTable.LOOT_ULTRA_RARE);
                        }

                        if(win == null){
                            win = new Loot(995, 1000000);
                        }
                        items[count]    = Item.forId(win.getItemID());
                        intItems[count] = win.getItemID();
                        amts[count++]   = win.getAmount();

                        for (int i = 0; i < items.length; i++) {
                            if (items[i] != null) {
                                player.getInventory().addItemDrop(items[i], amts[i]);
                            }
                        }

                        ClueManager.rewardsInterface(intItems, amts, player);
                        player.getGameFrame().openWindow(364);
                    }
                } else {
                    player.getActionSender().sendVar(453, BarrowsGame.slayedBrothers(player));
                }
            }
        }
    }

    /**
     * Method addCrypt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     */
    static void addCrypt(Crypt c) {
        cryps.add(c);
    }
}
