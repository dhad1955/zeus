package net.tazogaming.hydra.game.minigame.barrows;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/11/13
 * Time: 03:34
 */
public class Crypt {
    private int ladderId = 0;
    private int cryptId  = -1;
    private int digSmallX, digSmallY, digBigX, digBigY;
    private int cryptX, cryptY;
    private int bossId;

    /**
     * Constructs ...
     *
     *
     * @param digSmallX
     * @param digSmallY
     */
    public Crypt(int digSmallX, int digSmallY) {
        this.digSmallX = digSmallX;
        this.digSmallY = digSmallY;
    }

    /**
     * Method setTombId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public Crypt setTombId(int id) {
        this.cryptId = id;

        return this;
    }

    /**
     * Method setLadderId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public Crypt setLadderId(int id) {
        this.ladderId = id;

        return this;
    }

    /**
     * Method setBossId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public Crypt setBossId(int id) {
        this.bossId = id;

        return this;
    }

    /**
     * Method setId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public Crypt setId(int id) {
        this.cryptId = id;

        return this;
    }

    /**
     * Method setTombCoords
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public Crypt setTombCoords(int x, int y) {
        this.cryptX = x;
        this.cryptY = y;

        return this;
    }

    /**
     * Method getDigX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDigX() {
        return digSmallX;
    }

    /**
     * Method getDigY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDigY() {
        return digSmallY;
    }

    /**
     * Method getBossId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBossId() {
        return bossId;
    }

    /**
     * Method getCryptId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCryptId() {
        return cryptId;
    }

    /**
     * Method getCryptX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCryptX() {
        return cryptX;
    }

    /**
     * Method getCryptY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCryptY() {
        return cryptY;
    }

    /**
     * Method getLadderID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLadderID() {
        return ladderId;
    }
}
