package net.tazogaming.hydra.game.minigame.deathtower;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.game.StaticLootTable;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/12/13
 * Time: 12:56
 */
public class DeathTowerGame extends Instance implements RecurringTickEvent {
    public static final Zone TOWER_ZONE =
        new Zone(3089, 3923, 3118, 3947, -1, true, false,
                 "Death_tower").setBankingAllowed(false).setStatic(true);

    // 0 = evil chicken, 1 = kolodion  2 = jungle demon, 5904, untouchable, bulwark beast, balance elemental,
    public static final int[][] WAVES = {
        { 3375, 2 }, { 911, 3 }, { 1472, 5 }, { 5904, 6 }, { 10100, 12 }, { 9465, 15 }, { 5362, 5 }, { 3200, 7 },
        { 5902, 10 }, { 8281, 15 }, { 3494, 7 }, { 5666, 10 }, { 8597, 10 }, { 8528, 30 }, { 4972, 10 }, { 1293, 17 }
    };
    public static final int[]            stats = new int[] {
        0, 0, 0, 0, 0, 0, 0, 0, 0
    };
    private static DeathTowerRewardTable rewardTable;

    static {
        int g = 0;

        for (int i = 0; i < WAVES.length; i++) {
            g += WAVES[i][1];
        }

        setupGame();

        try {
            GameObject obj = new GameObject();

            obj.setLocation(Point.location(3117, 3933, 0));
            obj.setIdentifier(2603);
            obj.setObjectType(10);
            obj.setCurrentRotation(1);
            obj.setCurrentInstance(Instance.GLOBAL_INSTANCE);
            World.getWorld().getObjectManager().registerObject(obj);
        } catch (Exception ee) {}
    }

    private ArrayList<Integer> possibleNpcs   = new ArrayList<Integer>();
    private double             currentPercent = 0;
    private int                currentWave    = 0;
    private boolean            waitingRound   = false;
    private int                waveTimer      = 0;
    private boolean            terminated     = false;
    private int                currentIndex   = 0;
    Random                     r              = new Random();
    private Player             player;
    private NPC                currentNPC;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    public DeathTowerGame(Player player) {
        int totalRead;
        int len;

        player.setCurrentInstance(this);
        this.player = player;
        player.setCurDeathHandler(new DeathHandler() {
            @Override
            public void handleDeath(Killable killed, Killable killedBy) {
                unlink(killed.getPlayer());
            }
        });
        player.teleport(3093, 3935, 0);
        prepareNextWave();
        player.getActionSender().sendMessage("Battle will commence in 10 seconds");
        Core.submitTask(this);

        for (int i = 0; i < WAVES.length; i++) {
            possibleNpcs.add(i);
        }

        init();
    }

    /**
     * Method percboost
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param perc
     */
    public void percboost(int perc) {
        currentPercent += perc;
    }

    /**
     * Method onNpcKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void onNpcKilled(NPC npc) {
        possibleNpcs.remove(new Integer(currentIndex));
        this.currentPercent += WAVES[currentIndex][1];
        currentWave++;
        currentNPC = null;
        player.getActionSender().sendMessage(
            "Well done, you have defeated the enemy. The reward chance has been increased.");
        player.getActionSender().sendMessage("Please proceed to the chest.");
        sendCurrentStatus();
        updateGamePercent();
        updateEnemyHealth();
        player.getActionSender().createArrow(3117, 3933, 0, 2);

        if (npc.getId() == 8528) {
            player.getActionSender().sendMessage("You get full health and prayer for defeating nomad.");
            player.getPrayerBook().addPoints(5000);
            player.getActionSender().sendStat(5);
            player.addHealth(9999);
        }
    }

    /**
     * Method getRound
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRound() {
        return currentWave;
    }

    /**
     * Method isNpcDead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isNpcDead() {
        if (waveTimer != 0) {
            return false;
        }

        return (currentNPC == null) || currentNPC.isDead();
    }

    /**
     * Method prepareNextWave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void prepareNextWave() {
        player.getActionSender().markNpc(-1);
        waveTimer = 20;
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void unlink(Player player) {
        player.setCurrentInstance(null);
        player.getActionSender().markNpc(-1);
        player.getActionSender().sendMessage("Dominion Tower is now over for you, you lasted " + currentWave
                + " rounds");
        player.setCurDeathHandler(new DefaultDeathHandler());
        player.teleport(2540, 4717, 0);
        player.setPoisonTime(0);
        player.restore_all_stats();
        player.setCurrentSpecial(10);
        SpecialAttacks.updateSpecial(player);
        player.getPrayerBook().addPoints(90000);
        terminated = true;
    }

    /**
     * Method startWave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param wave
     */
    public void startWave(int wave) {
        currentWave++;

        NpcAutoSpawn spwn = new NpcAutoSpawn();

        spwn.setNpcId(WAVES[wave][0]);
        spwn.setSpawnX(3109);
        spwn.setSpawnY(3933);
        spwn.setRoamRange(30);
        spwn.setSpawnHeight(0);

        NPC q_npc = new NPC(spwn.getNpcId(), Point.location(spwn.getSpawnX(), spwn.getSpawnY(), player.getHeight()),
                            spwn);

        q_npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH, DeathPolicy.DONT_DROP_LOOT) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                onNpcKilled(killed);

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        World.getWorld().registerNPC(q_npc);
        q_npc.setQuestController(player);
        q_npc.setCurrentInstance(player.getCurrentInstance());
        player.add_linked_npc(q_npc, true);
        player.getActionSender().sendMessage("Round " + (currentWave + 1) + " is starting.");
        currentNPC = q_npc;
        currentNPC.addDamageFilter("towerupdate", new DamageFilter() {
            @Override
            public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
                updateEnemyHealth();

                return damage;
            }
        });
        updateEnemyHealth();
    }

    /**
     * Method getEnemyHP
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getEnemyHP() {
        if ((currentNPC == null) || currentNPC.isDead()) {
            return "n/a";
        }

        return "" + currentNPC.getCurrentHealth();
    }

    /**
     * Method updateGamePercent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateGamePercent() {}

    /**
     * Method init
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void init() {
        player.getWindowManager().showOverlay(11908);
        player.getActionSender().changeLine("@whi@Dominion Tower", 4969);
        player.getActionSender().changeLine("@whi@Reward: ", 11930);
        player.getActionSender().changeLine("@whi@Enemy HP:", 11933);
        player.getActionSender().changeLine("Dominion Tower", 11932);
        player.getActionSender().changeLine("", 11937);
        player.getActionSender().changeLine("", 11936);
        sendCurrentStatus();
        updateEnemyHealth();
        updateGamePercent();
    }

    /**
     * Method updateEnemyHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateEnemyHealth() {
        player.getActionSender().changeLine(getEnemyHP(), 11938);
    }

    /**
     * Method sendCurrentStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sendCurrentStatus() {
        if (waveTimer > 0) {
            player.getActionSender().changeLine("@whi@Round starts in " + (waveTimer / 2) + "", 11931);
            player.getActionSender().changeLine("", 11935);
        } else if ((currentNPC == null) || currentNPC.isDead()) {
            player.getActionSender().changeLine("@fla@@gre@Proceed to chest!", 11931);
            player.getActionSender().changeLine("", 11935);
        } else {
            player.getActionSender().changeLine("Round:", 11931);
            player.getActionSender().changeLine("      @whi@" + (currentWave + 1), 11935);
        }
    }

    /**
     * Method take_reward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void take_reward() {
        unlink(player);

        Loot[] loot = DeathTowerRewardTable.getLoot(getPercent());

        player.getActionSender().clearInterface(6963);

        if (loot != null) {
            for (int i = 0; i < loot.length; i++) {
                Loot looty = loot[i];

                if (looty != null) {
                    player.getActionSender().putItemOnGrid(looty.getItemID(), looty.getAmount(), i, 6963);
                    player.getInventory().addItemDrop(Item.forId(looty.getItemID()), looty.getAmount());
                }
            }
        }

        player.getWindowManager().showWindow(6960);
    }

    /**
     * Method getLoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Loot[] getLoot() {
        if (currentPercent == 0) {
            return null;
        }

        int chances = (int) (currentPercent / 25);

        if (chances <= 0) {
            chances = 1;
        }

        int               seed       = (int) (810 + (currentPercent / 2));
        StaticLootTable[] rewards    = new StaticLootTable[chances];
        int               ultra_rare = 500 + GameMath.rand(seed / 2);
        int               rare_range = (int) currentPercent / 16;
        int               reward     = -1;

        for (int i = 0; i < chances; i++) {
            int random = r.nextInt(seed);

            if ((random >= ultra_rare) && (random <= ultra_rare + rare_range)) {
                reward = Reward.REWARD_ULTRA_RARE;
            } else if (random < 500) {
                reward = Reward.REWARD_JUNK;
            } else if ((random > 500) && (random < 700)) {
                reward = Reward.REWARD_RUNES;
            } else if ((random > 700) && (random < 760)) {
                reward = Reward.REWARD_GOOD_RUNES;
            } else if ((random > 760) && (random < 800)) {
                reward = Reward.REWARD_HERBS;
            } else if ((random > 800) && (random < 810 + (currentPercent / 2))) {
                reward = Reward.REWARD_MISC;
            }

            if (reward != -1) {
                rewards[i] = rewardTable.reward_list[reward];
            }
        }

        Loot[] returnLoot = new Loot[chances];

        for (int i = 0; i < chances; i++) {
            returnLoot[i] = rewards[i].getLoot(1)[0];
        }

        return null;
    }

    /**
     * Method getPercent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPercent() {
        return (int) currentPercent;
    }

    /**
     * Method setupGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void setupGame() {
        rewardTable = new DeathTowerRewardTable();

        // runes
        rewardTable.addReward(554, 1000, Reward.REWARD_RUNES);
        rewardTable.addReward(555, 1000, Reward.REWARD_RUNES);
        rewardTable.addReward(556, 8000, Reward.REWARD_RUNES);
        rewardTable.addReward(557, 1000, Reward.REWARD_RUNES);
        rewardTable.addReward(558, 600, Reward.REWARD_RUNES);
        rewardTable.addReward(560, 600, Reward.REWARD_GOOD_RUNES);
        rewardTable.addReward(561, 1500, Reward.REWARD_GOOD_RUNES);
        rewardTable.addReward(562, 900, Reward.REWARD_GOOD_RUNES);
        rewardTable.addReward(564, 1000, Reward.REWARD_GOOD_RUNES);
        rewardTable.addReward(565, 1500, Reward.REWARD_GOOD_RUNES);

        // herbs
        rewardTable.addReward(250, 100, Reward.REWARD_HERBS);
        rewardTable.addReward(252, 100, Reward.REWARD_HERBS);
        rewardTable.addReward(254, 100, Reward.REWARD_HERBS);
        rewardTable.addReward(256, 100, Reward.REWARD_HERBS);
        rewardTable.addReward(258, 120, Reward.REWARD_HERBS);
        rewardTable.addReward(260, 120, Reward.REWARD_HERBS);
        rewardTable.addReward(262, 120, Reward.REWARD_HERBS);
        rewardTable.addReward(264, 120, Reward.REWARD_HERBS);
        rewardTable.addReward(266, 120, Reward.REWARD_HERBS);
        rewardTable.addReward(268, 120, Reward.REWARD_HERBS);
        rewardTable.addReward(270, 120, Reward.REWARD_HERBS);

        // junk
        rewardTable.addReward(1123, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(1079, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(1163, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(1185, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(1249, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(1261, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(158, 5, Reward.REWARD_JUNK);
        rewardTable.addReward(139, 4, Reward.REWARD_JUNK);
        rewardTable.addReward(1285, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(9236, 50, Reward.REWARD_JUNK);
        rewardTable.addReward(9237, 50, Reward.REWARD_JUNK);
        rewardTable.addReward(9236, 50, Reward.REWARD_JUNK);
        rewardTable.addReward(9236, 50, Reward.REWARD_JUNK);
        rewardTable.addReward(9240, 50, Reward.REWARD_JUNK);
        rewardTable.addReward(9241, 50, Reward.REWARD_JUNK);
        rewardTable.addReward(1631, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(1620, 20, Reward.REWARD_JUNK);
        rewardTable.addReward(1264, 20, Reward.REWARD_JUNK);
        rewardTable.addReward(1632, 50, Reward.REWARD_MISC);
        rewardTable.addReward(6573, 2, Reward.REWARD_MISC);
        rewardTable.addReward(6731, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6733, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6736, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6739, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6916, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6918, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6920, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6922, 1, Reward.REWARD_MISC);
        rewardTable.addReward(6924, 1, Reward.REWARD_MISC);
        rewardTable.addReward(17273, 1, Reward.REWARD_MISC);
        rewardTable.addReward(15332, 1, Reward.REWARD_JUNK);
        rewardTable.addReward(4835, 200, Reward.REWARD_MISC);
        rewardTable.addReward(20001, 1, Reward.REWARD_MISC);
        rewardTable.addReward(25, 1, Reward.REWARD_ULTRA_RARE);
        rewardTable.addReward(20003, 1, Reward.REWARD_ULTRA_RARE);
    }

    /**
     * Method runtest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void runtest() {
        for (int i = 0; i < 1110240; i++) {
            try {
                Thread.sleep(100);
            } catch (Exception ee) {}

            try {
                rewardTable.getRewards(105);
            } catch (Exception EE) {
                EE.printStackTrace();
            }
        }

        for (int percantage = 25; percantage <= 100; percantage += 25) {
            for (int i = 0; i < 100; i++) {
                rewardTable.getRewards(percantage);
            }

            Logger.log("================================");
            Logger.log("Reward at: " + percantage + "%");
            System.out.println();
            System.out.print(" junk: " + DeathTowerRewardTable.stats[Reward.REWARD_JUNK]);
            System.out.print(" runes: " + DeathTowerRewardTable.stats[Reward.REWARD_RUNES]);
            System.out.print(" goodrunes: " + DeathTowerRewardTable.stats[Reward.REWARD_GOOD_RUNES]);
            System.out.print(" HERBS: " + DeathTowerRewardTable.stats[Reward.REWARD_HERBS]);
            System.out.print(" Rares: " + DeathTowerRewardTable.stats[Reward.REWARD_MISC]);
            System.out.print(" decent: " + DeathTowerRewardTable.stats[Reward.REWARD_RARE_ITEMS]);
            System.out.print("ultra rares: " + DeathTowerRewardTable.stats[Reward.REWARD_ULTRA_RARE]);
            System.out.println();
            DeathTowerRewardTable.stats = new int[20];
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        if (waveTimer > 0) {
            waveTimer--;

            if (waveTimer == 0) {
                currentIndex = possibleNpcs.get(GameMath.rand3(possibleNpcs.size()));
                startWave(currentIndex);
            }

            sendCurrentStatus();
        }

        if (!player.isLoggedIn() ||!player.isConnected() ||!player.isInArea(TOWER_ZONE)) {
            unlink(player);
            terminated = true;
        }

        if (player.getWindowManager().getCurrentOverlayID() != 11908) {
            player.getWindowManager().showOverlay(11908);
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return terminated;
    }
}
