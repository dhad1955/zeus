package net.tazogaming.hydra.game.minigame.deathtower;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/12/13
 * Time: 12:54
 */
public class Reward {
    public static final int
        REWARD_JUNK       = 0,
        REWARD_RUNES      = 1,
        REWARD_HERBS      = 2,
        REWARD_MISC       = 3,
        REWARD_RARE_ITEMS = 4,
        REWARD_ULTRA_RARE = 5,
        REWARD_GOOD_RUNES = 6;
}
