package net.tazogaming.hydra.game.minigame.deathtower;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.game.StaticLootTable;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.security.SecureRandom;

import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/12/13
 * Time: 12:53
 */
public class DeathTowerRewardTable {
    public static final int
        POOR_GAME                                      = 0,
        SEMI_POOR_GAME                                 = 1,
        DECENT_GAME                                    = 2,
        GOOD_GAME                                      = 3,
        ULTRA_RARE                                     = 4,
        GOOD_HERBS                                     = 5,
        BAD_HERBS                                      = 6,
        GOOD_GEMS                                      = 4,
        BAD_GEMS                                       = 5;
    private static final StaticLootTable[] LOOT_TABLES = new StaticLootTable[20];
    public static int[]                    stats       = new int[9];
    private static SecureRandom            random      = new SecureRandom();

    static {
        for (int i = 0; i < LOOT_TABLES.length; i++) {
            LOOT_TABLES[i] = new StaticLootTable();
        }

        // poor reward table
        addPoor(40, 5);
        addPoor(440, 10);
        addPoor(807, 10);
        addPoor(1067, 1);
        addPoor(1081, 1);
        addPoor(1101, 1);
        addPoor(1137, 1);
        addPoor(1420, 1);
        addPoor(1309, 1);
        addPoor(3096, 1);
        addPoor(3192, 1);
        addPoor(592, 1);
        addPoor(569, 1);
        addPoor(571, 1);
        addPoor(581, 1);
        addPoor(579, 1);
        addPoor(577, 1);
        addPoor(573, 1);
        addPoor(626, 1);
        addPoor(628, 1);
        addPoor(630, 1);
        addPoor(632, 1);
        addPoor(636, 1);
        addPoor(638, 1);
        addPoor(640, 1);
        addPoor(642, 1);
        addPoor(1381, 1);
        addPoor(1383, 1);
        addPoor(1385, 1);
        addPoor(1387, 1);
        addSemi(1071, 1);
        addSemi(1085, 1);
        addSemi(1085, 1);
        addSemi(1109, 1);
        addSemi(1121, 1);
        addSemi(1143, 1);
        addSemi(1159, 1);
        addSemi(1181, 1);
        addSemi(1181, 1);
        addSemi(1197, 1);
        addSemi(1209, 1);
        addSemi(1209, 1);
        addSemi(1285, 1);
        addSemi(1329, 1);
        addSemi(1315, 1);
        addSemi(3198, 1);
        addSemi(5657, 1);
        addSemi(5664, 100);
        addSemi(1393, 1);
        addSemi(1394, 1);
        addSemi(1396, 1);
        addSemi(1397, 1);
        addSemi(1399, 1);
        addSemi(2352, 10);
        addSemi(2349, 10);
        addSemi(2351, 10);
        addDecent2(868, 100);
        addDecent2(876, 100);
        addDecent2(1079, 1);
        addDecent2(1093, 1);
        addDecent2(1113, 1);
        addDecent2(1127, 1);
        addDecent2(1201, 1);
        addDecent2(1113, 1);
        addDecent2(1247, 1);
        addDecent2(1147, 1);
        addDecent2(1207, 1);
        addDecent2(1201, 1);
        addDecent2(1432, 1);
        addDecent2(1359, 1);
        addDecent2(1289, 1);
        addDecent2(1275, 1);
        addDecent2(1247, 1);
        addDecent2(3101, 1);
        addDecent2(1149, 1);
        addDecent2(1215, 1);
        addDecent2(1231, 1);
        addDecent2(1249, 1);
        addDecent2(1305, 1);
        addDecent2(1377, 1);
        addDecent2(1434, 1);
        addDecent2(1632, 3);
        addDecent2(1702, 1);
        addDecent2(3176, 1);
        addDecent2(3104, 1);
        addDecent2(158, 50);
        addDecent2(140, 10);
        addGood(1231, 1);
        addGood(8784, 1);
        addGood(15308, 1);
        addGood(15312, 1);
        addGood(4153, 1);
        addGood(5680, 1);
        addGood(5698, 1);
        addGood(5716, 1);
        addGood(5730, 1);
        addGood(6739, 1);
        addGood(6904, 10);
        addGood(7158, 1);
        addGood(1147, 1);
        addGood(1113, 1);
        addGood(1185, 1);
        addGood(1319, 1);
        addGood(1333, 1);
        addGood(1347, 1);
        addGood(1359, 1);
        addGood(1373, 1);
        addGood(2615, 1);
        addGood(2623, 1);
        addGood(1127, 1);
        addGood(1113, 1);
        addGood(1201, 1);
        addGood(1185, 1);
        addGood(868, 1000);
        addGood(4087, 1);
        addGood(11732, 1);
        addGood(11286, 1);
        addGood(3103, 1);
        addGood(892, 1000);
        addUltraRare(20002, 1);
        addUltraRare(20003, 1);
        addUltraRare(17, 1);
        addUltraRare(25, 1);
        addBadGem(1622, 10);
        addBadGem(1624, 10);
        addBadGem(1628, 10);
        addBadGem(1622, 5);
        addBadGem(1624, 5);
        addBadGem(1628, 5);
        addGoodGem(1618, 10);
        addGoodGem(1618, 20);
        addGoodGem(1622, 30);
        addGoodGem(1624, 30);
        addGoodGem(1632, 5);
        addGoodGem(1620, 10);
        addGoodGem(1620, 50);
        addBadHerb(200, 10);
        addBadHerb(204, 10);
        addBadHerb(206, 10);
        addBadHerb(200, 5);
        addBadHerb(204, 5);
        addGoodHerb(200, 20);
        addGoodHerb(200, 25);
        addGoodHerb(200, 30);
        addGoodHerb(204, 40);
        addGoodHerb(204, 10);
        addGoodHerb(204, 25);
        addGoodHerb(208, 20);
        addGoodHerb(214, 20);
        addGoodHerb(212, 20);
        addGoodHerb(210, 20);
        addGoodHerb(218, 20);
        addGoodHerb(215, 20);
        addGoodHerb(214, 20);
    }

    /*
     * Random seed for reward table
     */
    private SecureRandom     r           = new SecureRandom();
    public StaticLootTable[] reward_list = new StaticLootTable[9];

    /**
     * Constructs ...
     *
     */
    public DeathTowerRewardTable() {
        for (int i = 0; i < reward_list.length; i++) {
            reward_list[i] = new StaticLootTable();
        }
    }

    private static void addPoor(int id, int amt) {
        LOOT_TABLES[POOR_GAME].add(id, amt);
    }

    private static void addSemi(int id, int amt) {
        LOOT_TABLES[SEMI_POOR_GAME].add(id, amt);
    }

    private static void addDecent2(int id, int amt) {
        LOOT_TABLES[DECENT_GAME].add(id, amt);
    }

    private static void addGood(int id, int amt) {
        LOOT_TABLES[GOOD_GAME].add(id, amt);
    }

    private static void addUltraRare(int id, int amt) {
        LOOT_TABLES[ULTRA_RARE].add(id, amt);
    }

    private static void addHerbBad(int id, int amt) {
        LOOT_TABLES[BAD_HERBS].add(id, amt);
    }

    private static void addHerbGood(int id, int amt) {
        LOOT_TABLES[GOOD_HERBS].add(id, amt);
    }

    private static void addGemsBad(int id, int amt) {
        LOOT_TABLES[BAD_GEMS].add(id, amt);
    }

    private static void addGoodGems(int id, int amt) {
        LOOT_TABLES[GOOD_GEMS].add(id, amt);
    }

    private static void addBadGem(int id, int amt) {
        LOOT_TABLES[BAD_GEMS].add(id, amt);
    }

    private static void addGoodGem(int id, int amt) {
        LOOT_TABLES[GOOD_GEMS].add(id, amt);
    }

    private static void addBadHerb(int id, int amt) {
        LOOT_TABLES[BAD_HERBS].add(id, amt);
    }

    private static void addGoodHerb(int id, int amt) {
        LOOT_TABLES[GOOD_HERBS].add(id, amt);
    }

    /**
     * Method getLoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param percentage
     *
     * @return
     */
    public static final Loot[] getLoot(int percentage) {
        int    total  = 1;
        int    rating = POOR_GAME;
        Loot[] loot   = new Loot[20];

        if (percentage < 20) {
            rating = POOR_GAME;
        } else if (percentage < 40) {
            rating = SEMI_POOR_GAME;
        } else if (percentage < 79) {
            rating = DECENT_GAME;
        } else {
            rating = GOOD_GAME;
        }

        int herbTable = -1;

        if ((rating == SEMI_POOR_GAME) || (rating == DECENT_GAME)) {
            herbTable = BAD_HERBS;
        } else if (rating == GOOD_GAME) {
            herbTable = GOOD_HERBS;
        }

        int gemTable = -1;

        if ((rating == GOOD_GAME) || (rating == DECENT_GAME)) {
            gemTable = BAD_GEMS;
        } else if (percentage == 100) {
            gemTable = GOOD_GEMS;
        }

        int ran = random.nextInt(20000);

        if (ran >= 19999 - (percentage)) {
            rating = ULTRA_RARE;
        }

        int cashAmount = 1200000;

        cashAmount *= (double) (percentage / 100d);

        int offset = 0;

        loot[offset++] = new Loot(995, cashAmount);
        loot[offset++] = LOOT_TABLES[rating].getLoot(1)[0];

        if (gemTable != -1) {
            loot[offset++] = LOOT_TABLES[gemTable].getLoot(1)[0];
        } else if (herbTable != -1) {
            loot[offset++] = LOOT_TABLES[herbTable].getLoot(1)[0];
        }

        Loot[] tmp = new Loot[offset];

        System.arraycopy(loot, 0, tmp, 0, offset);

        return tmp;
    }

    /*
     * Adds a reward
     */

    /**
     * Method addReward
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param max
     * @param cat
     */
    public void addReward(int itemId, int max, int cat) {
        if (reward_list[cat] == null) {
            reward_list[cat] = new StaticLootTable();
        }

        reward_list[cat].add(itemId, max);
    }

    /**
     * Method addDecent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amt
     */
    public void addDecent(int id, int amt) {
        reward_list[Reward.REWARD_RARE_ITEMS].add(id, amt);
    }

    private static int randomReward(int currentPercent) {
        int ran = random.nextInt(1000);

        if (ran < 700) {
            stats[Reward.REWARD_RARE_ITEMS]++;

            return Reward.REWARD_JUNK;
        } else if (ran < 950) {
            stats[Reward.REWARD_MISC]++;

            return Reward.REWARD_MISC;
        } else if ((ran > 950) && (ran < 950 + (currentPercent / 6))) {
            stats[Reward.REWARD_ULTRA_RARE]++;

            return Reward.REWARD_ULTRA_RARE;
        }

        stats[Reward.REWARD_RARE_ITEMS]++;

        return Reward.REWARD_JUNK;
    }

    /**
     * Method getRewards
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentPercent
     *
     * @return
     */
    public Loot[] getRewards(int currentPercent) {
        StaticLootTable[] rewards      = new StaticLootTable[2 + ((currentPercent > 80)
                ? 1
                : 0)];
        SecureRandom      secureRandom = new SecureRandom();
        Random            random       = new Random();

        random.setSeed(System.currentTimeMillis() + System.nanoTime());

        if (random.nextInt(1000) > 200 + (currentPercent * 3)) {
            rewards[0] = reward_list[Reward.REWARD_JUNK];
            stats[Reward.REWARD_JUNK]++;
        } else {
            rewards[0] = reward_list[randomReward(currentPercent)];
        }

        int ind = 1;

        if (currentPercent > 80) {
            rewards[ind++] = reward_list[(secureRandom.nextInt(1000) > 700)
                                         ? Reward.REWARD_JUNK
                                         : randomReward(currentPercent)];
        }

        if (GameMath.rand3(200) < currentPercent) {
            rewards[ind++] = reward_list[Reward.REWARD_GOOD_RUNES];
        } else {
            rewards[ind++] = reward_list[Reward.REWARD_RUNES];
        }

        Loot[][] tmp = new Loot[rewards.length][1];

        for (int i = 0; i < rewards.length; i++) {
            tmp[i] = rewards[i].getLoot(1);

            if (tmp[i].length == 0) {
                tmp[i] = rewards[i].getLoot(1);
            }

            for (int k = 0; k < tmp[i].length; k++) {
                Loot l = tmp[i][k];

                if (Item.forId(l.getItemID()).isStackable()) {
                    l.setAmount(r.nextInt(20 + (int) (l.getAmount() * (double) (currentPercent / 100d))));
                }
            }
        }

        Loot[] returnLoot = new Loot[tmp.length];

        for (int k = 0; k < returnLoot.length; k++) {
            returnLoot[k] = tmp[k][0];
        }

        return returnLoot;
    }

    /**
     * Method addJunk
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     */
    public void addJunk(int itemid) {
        reward_list[Reward.REWARD_JUNK].add(itemid, 1);
    }
}
