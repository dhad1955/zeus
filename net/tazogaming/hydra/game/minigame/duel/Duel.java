package net.tazogaming.hydra.game.minigame.duel;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.EquipmentTypes;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DuelDeathHandler;
import net.tazogaming.hydra.game.ui.HintIcon;
import net.tazogaming.hydra.game.ui.WindowManager;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 11:26
 */
public class Duel {
    public static final short DUEL_RULES_INTERFACE = 631;

    /**
     * The second duel interface id.
     */
    public static final short DUEL_SECOND_INTERFACE = 626;
    public static final int
        NO_FOREFIT                                  = 1,
        NO_MOVEMENT                                 = 2,
        NO_RANGED                                   = 16,
        NO_MELEE                                    = 32,
        NO_MAGIC                                    = 64,
        NO_DRINKS                                   = 128,
        NO_FOOD                                     = 256,
        NO_PRAYER                                   = 512,
        OBSTACLES                                   = 1024,
        FUN_WEAPONS                                 = 4096,
        NO_SPEC                                     = 8192,
        NO_HELM                                     = 16384,
        NO_CAPE                                     = 32768,
        NO_AMULET                                   = 65536,
        NO_WEAPON                                   = 131072,
        NO_BODY                                     = 262144,
        NO_SHIELD                                   = 524288,
        NO_LEGS                                     = 2097152,
        NO_GLOVES                                   = 8388608,
        NO_BOOTS                                    = 16777216,
        NO_RINGS                                    = 67108864,
        NO_ARROWS                                   = 134217728;
    public static final int
        STATUS_FIRST_SCREEN                         = 0,
        STATUS_SECOND_SCREEN                        = 1,
        STATUS_PRE_FIGHT                            = 2,
        STATUS_IN_FIGHT                             = 3,
        STATUS_OVER                                 = 4;
    private Player[]           players              = new Player[2];
    private TradeHolder[]      holders              = new TradeHolder[2];
    private int                currentStatus        = 0;
    private boolean            isTerminated         = false;
    private ArrayList<Integer> settings             = new ArrayList<Integer>();

    public boolean rewardGiven = false;
    /**
     * Constructs ...
     *
     *
     * @param players
     */
    public Duel(Player... players) {
        this.players = players;

        for (Player p : players) {
            p.log("Duel screen loaded: " + getOtherPlayer(p));
        }

        if (players[0] == players[1]) {
            return;
        }

        for (Player pla : players) {
            pla.getGameFrame().openWindow(DUEL_RULES_INTERFACE);
            pla.getGameFrame().setInventoryInterface(628, 93);

            for (int i = 0; i < 14; i++) {
                if (pla.getEquipment().getId(i) != -1) {

                    // pla.getActionSender().putItemOnGrid(pla.getEquipment().getId(i), pla.getEquipment().getAmount(i),
                    // i, 13824);
                }
            }

            pla.getGameFrame().setDuel(this);
        }

        for (int i = 0; i < 2; i++) {
            holders[i] = new TradeHolder(9);
        }

        for (Player pla : players) {
            resetAll(pla);
        }
    }

    /**
     * Method getSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Integer> getSettings() {
        return settings;
    }

    /**
     * Method sort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void sort() {
        Collections.sort(settings, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                int v1 = o1;
                int v2 = o2;

                if (v1 == v2) {
                    return 0;
                }

                if (v1 < v2) {
                    return -1;
                }

                return 1;
            }
        });
    }

    /**
     * Method resetAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void resetAll(Player player) {
        player.getActionSender().sendVar(286, 0);

        // player.getGameFrame().sendString("", 631, 28);
        player.getGameFrame().sendString(getOtherPlayer(player).getUsername(), 631, 23);
        player.getGameFrame().sendString(Integer.toString(getOtherPlayer(player).getCombatLevel()), 631, 25);

        Item[] tmp  = new Item[28];
        int[]  tmp2 = new int[28];

        player.getActionSender().sendItems(134, tmp, tmp2, false);
        player.getActionSender().sendItems(134, tmp, tmp2, true);
    }

    /**
     * Method decline
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void decline(Player pla) {
        if (isTerminated || ((currentStatus != STATUS_FIRST_SCREEN) && (currentStatus != STATUS_SECOND_SCREEN))) {
            return;
        }

        pla.log("declined duel");
        isTerminated = true;
        holders[getIndex(pla)].returnItems(pla);
        holders[getOtherIndex(pla)].returnItems(getOtherPlayer(pla));
        getOtherPlayer(pla).getActionSender().sendMessage("Other player declined duel and stake options");

        for (Player pl2 : players) {
            pl2.getGameFrame().close();
            pl2.log("Duel declined: true");
            pl2.getGameFrame().setDuel(null);
        }

        holders[0] = new TradeHolder();
        holders[1] = new TradeHolder();    // just incase
        pla.getGameFrame().setDuel(null);
    }

    /**
     * Method onAccept
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void onAccept(Player pla) {
        if (isTerminated || ((currentStatus != STATUS_FIRST_SCREEN) && (currentStatus != STATUS_SECOND_SCREEN))) {
            return;
        }

        if (setting(NO_MELEE) && setting(NO_RANGED) && setting(NO_MAGIC)) {
            pla.getActionSender().sendMessage("You can't have all 3 combat types disabled.");
            duelChanged();

            return;
        }

        if(setting(NO_MELEE) && setting(FUN_WEAPONS)){
           pla.getActionSender().sendMessage("How are you meant to do a DDS Stake with melee turned off?");
            duelChanged();
            return;
        }

        if (setting(OBSTACLES) && setting(NO_MOVEMENT)) {
            pla.getActionSender().sendMessage("You can't have obstacles and no-movement turned on at the same time");
            duelChanged();

            return;
        }


        for(Player p : players){
            long totalCash = 0;
            if(p.getInventory().hasItem(995, 1)){
                totalCash = p.getInventory().countItems(995);
            }
                int slot = holders[getIndex(p)].getItemSlot(995);
           if(slot != -1)
                totalCash += holders[getIndex(p)].tradedItemsCount[slot];
            if(holders[getIndex(getOtherPlayer(p))].getItemSlot(995) != -1){
                    int otherSlot = holders[getIndex(getOtherPlayer(p))].getItemSlot(995);
                    totalCash += holders[getIndex(getOtherPlayer(p))].tradedItemsCount[otherSlot];
                }


                if(totalCash >= Integer.MAX_VALUE){
                    duelChanged();
                    pla.getActionSender().sendMessage("Unable to start, MAX CASH exceeded");
                    return;
                }


        }

        if (setting(FUN_WEAPONS) && ((pla.getEquipment().getId(3) != 1215) || setting(NO_WEAPON))) {
            duelChanged();
            pla.getActionSender().sendMessage("You can only wear a dragon dagger (non poisoned) in this duel.");

            return;
        }

        int index = getIndex(pla);

        if (holders[index].status != 1) {
            holders[index].status = 1;

            if (currentStatus == STATUS_FIRST_SCREEN) {
                pla.getGameFrame().sendString("Waiting for other player to accept", 631, 26);
                getOtherPlayer(pla).getGameFrame().sendString("Other player has accepted", 631, 26);

                if (!canAcceptDuel(pla)) {
                    pla.getActionSender().sendMessage("You can't accept this duel because you don't have enough space");
                    pla.getActionSender().sendMessage(
                        "in your inventory for the items you need to remove during this duel.");
                    getOtherPlayer(pla).getActionSender().sendMessage("Other player doesn't have enough free space.");
                    duelChanged();
                } else {
                    pla.log("Duel accepted, waiting other player.");
                }

                if (setting(FUN_WEAPONS) && (getOtherPlayer(pla).getEquipment().getId(3) != 1215)) {
                    duelChanged();
                    getOtherPlayer(pla).getActionSender().sendMessage(
                        "You can only wear a dragon dagger (non poisoned) in this duel");
                    pla.getActionSender().sendMessage("Other user is not wearing a dragon dagger");

                    return;
                }

                if (!canAcceptDuel(getOtherPlayer(pla))) {
                    getOtherPlayer(pla).getActionSender().sendMessage(
                        "You can't accept this duel because you don't have enough space");
                    getOtherPlayer(pla).getActionSender().sendMessage(
                        "in your inventory for the items you need to remove during this duel.");
                    pla.getActionSender().sendMessage("Other player doesn't have enough free space.");
                    duelChanged();

                    return;
                }

                if (holders[getOtherIndex(pla)].status == 1) {
                    this.currentStatus = STATUS_SECOND_SCREEN;
                    this.duelChanged();
                    displayConfirmScreen(pla);
                    duelChanged();

                    for (Player pla_2 : players) {
                        pla_2.getActionSender().showInterface(6412);
                        WindowManager.getInterface(3213).onOpen(pla_2);
                    }
                }
            } else if (currentStatus == STATUS_SECOND_SCREEN) {
                pla.getGameFrame().sendString("Waiting for the other player..", 626, 45);

                if (holders[getOtherIndex(pla)].status == 1) {
                    boolean b = DuelArena.place_users(this, pla, getOtherPlayer(pla));

                    if (!b) {
                        pla.getActionSender().sendMessage(
                            "There isn't enough space in the arena at the moment, try again in a few seconds");
                        getOtherPlayer(pla).getActionSender().sendMessage(
                            "There isn't enough space in the arena at the moment, try again in a few seconds");
                        duelChanged();

                        return;
                    }

                    currentStatus = STATUS_IN_FIGHT;
                    prepare_for_fight(pla);
                    prepare_for_fight(getOtherPlayer(pla));
                    pla.getGameFrame().setCurrentHintIcon(new HintIcon(getOtherPlayer(pla)));
                    getOtherPlayer(pla).getGameFrame().setCurrentHintIcon(new HintIcon(pla));

                    pla.setCurDeathHandler(new DuelDeathHandler(this));
                    getOtherPlayer(pla).setCurDeathHandler(new DuelDeathHandler(this));
                    pla.getWindowManager().closeWindow();
                    getOtherPlayer(pla).getWindowManager().closeWindow();
                    pla.log("Duel started");
                    getOtherPlayer(pla).log("duel started");
                    getOtherPlayer(pla).setSpecialMultiplier(1.00);
                    pla.setSpecialMultiplier(1.00);
                    pla.getPrayerBook().reset_leeches();
                    pla.getAccount().removeVar("v");
                    getOtherPlayer(pla).getAccount().removeVar("v");
                } else {
                    getOtherPlayer(pla).getGameFrame().sendString("Other player has accepted", 626, 45);
                }
            }
        }
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void unlink(Player pla) {
        if (currentStatus != STATUS_OVER) {
            return;
        }

        pla.getCombatAdapter().reset();
        pla.unFollow();
        pla.getRequest().close();
        pla.teleport(3377, 3273, 0);
        pla.getGameFrame().setCurrentHintIcon(null); 
        pla.getGameFrame().setDuel(null);
        pla.getTimers().resetTimer(TimingUtility.SKULL_TIMER);
        pla.getAppearance().setChanged(true);

        if (pla.isDead()) {
            pla.cancelDeaths();
        }

        pla.setCurDeathHandler(new DefaultDeathHandler());
    }

    /**
     * Method showSpoils
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void showSpoils(Player pla) {
        pla.getActionSender().changeLine(getOtherPlayer(pla).getUsername(), 6840);
        pla.getActionSender().changeLine("Combat Level: ", 6838);
        pla.getActionSender().changeLine(Integer.toString(getOtherPlayer(pla).getCombatLevel()), 6839);
        pla.getActionSender().changeLine("Name:", 6837);

        int[] items  = holders[getOtherIndex(pla)].tradedItems;
        int[] itemsC = holders[getOtherIndex(pla)].tradedItemsCount;

        pla.getActionSender().sendModelGrid(items, itemsC, 6822);
        pla.getWindowManager().showWindow(6733);
    }

    /**
     * Method on_leave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void on_leave(Player pla) {

        if (currentStatus == STATUS_IN_FIGHT) {
            restore_stats(pla);
            give_items_to_winner(getOtherPlayer(pla));
            currentStatus = STATUS_OVER;
            unlink(pla);
            unlink(getOtherPlayer(pla));
            showSpoils(getOtherPlayer(pla));
            pla.getActionSender().sendMessage("You have fore-fitted the duel.");
            getOtherPlayer(pla).getActionSender().sendMessage("Other play quit the duel.");
            pla.setCurDeathHandler(new DefaultDeathHandler());
        }
    }

    private void displayConfirmScreen(Player pla) {
        int             otherIndex  = getOtherIndex(pla);
        int             myIndex     = getIndex(pla);
        Player          otherPlayer = players[otherIndex];
        int[]           myItems     = holders[myIndex].tradedItems;
        int[]           myItemsC    = holders[myIndex].tradedItemsCount;
        int[]           thereItems  = holders[otherIndex].tradedItems;
        int[]           thereItemsC = holders[otherIndex].tradedItemsCount;
        String          my_string   = "";
        ArrayList<Item> used        = new ArrayList<Item>();

        for (int i = 0; i < myItems.length; i++) {
            if (myItems[i] != -1) {
                Item item = Item.forId(myItems[i]);

                if (item.isStackable()) {
                    my_string += item.getName() + " x " + myItemsC[i] + " (" + GameMath.formatMoney(myItemsC[i])
                                 + ") \\n";
                } else {
                    if (!used.contains(item)) {
                        my_string += item.getName() + " x " + holders[myIndex].getItemMax(myItems[i], 28) + " \\n";
                    }

                    used.add(item);
                }
            }
        }

        used.clear();

        String there_string = "";

        for (int i = 0; i < thereItems.length; i++) {
            if (thereItems[i] != -1) {
                Item item = Item.forId(thereItems[i]);

                if (item.isStackable()) {
                    there_string += item.getName() + " x " + thereItemsC[i] + " \\n";
                } else {
                    if (!used.contains(item)) {
                        there_string += item.getName() + " x " + holders[otherIndex].getItemMax(item.getIndex(), 28)
                                        + " \\n";
                    }

                    used.add(item);
                }
            }
        }

        if (my_string.length() == 0) {
            my_string = "Absolutely Nothing!";
        }

        if (there_string.length() == 0) {
            there_string = "Absolutely Nothing!";
        }

        pla.getActionSender().changeLine(my_string, 6516);
        pla.getActionSender().changeLine(there_string, 6517);
        otherPlayer.getActionSender().changeLine(there_string, 6516);
        otherPlayer.getActionSender().changeLine(my_string, 6517);

        ArrayList<String> rules = new ArrayList<String>();

        if (setting(NO_PRAYER)) {
            rules.add("Existing prayers will be stopped.");
        }

        if (setting(NO_DRINKS)) {
            rules.add("Boosted stats will be restored.");
        }

        if (setting(NO_FOOD)) {
            rules.add("Food will be disabled, you will not be able to eat during");
        }

        for (int i = 14; i < 27; i++) {
            if (setting(i)) {
                rules.add("Some worn items will be taken off");

                break;
            }
        }

        if (rules.size() < 4) {
            for (int i = rules.size(); i < 4; i++) {
                rules.add("");
            }
        }

        if (setting(NO_FOOD)) {
            rules.add("You cannot eat.");
        }

        if (setting(NO_SPEC)) {
            rules.add("You cant use special attacks");
        }

        if (setting(NO_MOVEMENT)) {
            rules.add("You cannot move");
        }

        if (setting(NO_MAGIC)) {
            rules.add("You cannot use Ranged attacks");
        }

        if (setting(NO_MELEE)) {
            rules.add("You cannot use melee attacks.");
        }

        if (setting(NO_MAGIC)) {
            rules.add("You cannot use magic attacks ");
        }

        if (setting(NO_PRAYER)) {
            rules.add("You cannot use prayer");
        }

        if (setting(NO_DRINKS)) {
            rules.add("You cant drink potions");
        }

        if (setting(OBSTACLES)) {
            rules.add("There will be obstacles in the arena.");
        }

        int offset2 = 8238,
            max     = 8253;

        for (int i = offset2; i <= max; i++) {
            otherPlayer.getActionSender().changeLine("", i);
            pla.getActionSender().changeLine("", i);
        }

        for (String ruleStrings : rules) {
            if (offset2 > max) {
                break;
            }

            otherPlayer.getActionSender().changeLine(ruleStrings, offset2);
            otherPlayer.getGameFrame().switchWindow(626);
            pla.getActionSender().changeLine(ruleStrings, offset2);
            pla.getGameFrame().switchWindow(626);
            offset2++;
        }
    }

    /**
     * Method getRiskAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public int getRiskAmount(Player player) {
        int index = getIndex(player);

        return holders[index].total();
    }

    private int getOtherIndex(Player pla) {
        for (int i = 0; i < holders.length; i++) {
            if (players[i] != pla) {
                return i;
            }
        }

        throw new RuntimeException();
    }

    /**
     * Method canAcceptDuel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean canAcceptDuel(Player pla) {
        int       freeSlots   = pla.getInventory().getFreeSlots(-1);
        Equipment e           = pla.getEquipment();
        int       slotsNeeded = 0;

        if (setting(NO_CAPE) && e.isEquipped(Equipment.CAPE)) {
            slotsNeeded++;
        }

        if (setting(NO_LEGS) && e.isEquipped(Equipment.LEGS)) {
            slotsNeeded++;
        }

        if (setting(NO_WEAPON) && e.isEquipped(Equipment.WEAPON)) {
            slotsNeeded++;
        }

        if (setting(NO_GLOVES) && e.isEquipped(Equipment.HANDS)) {
            slotsNeeded++;
        }

        if (setting(NO_SHIELD) && e.isEquipped(Equipment.SHIELD)) {
            slotsNeeded++;
        }

        if (setting(NO_HELM) && e.isEquipped(Equipment.HAT)) {
            slotsNeeded++;
        }

        if (setting(NO_RINGS) && e.isEquipped(Equipment.RING)) {
            slotsNeeded++;
        }

        if (setting(NO_ARROWS) && e.isEquipped(Equipment.ARROWS)) {
            slotsNeeded++;
        }

        if (setting(NO_BOOTS) && e.isEquipped(Equipment.FEET)) {
            slotsNeeded++;
        }

        if (setting(NO_BODY) && e.isEquipped(Equipment.TORSO)) {
            slotsNeeded++;
        }

        // if(!setting("equipment-allowed"))
        int[] myItems    = holders[getIndex(pla)].tradedItems;
        int[] otherItems = holders[getOtherIndex(pla)].tradedItems;

        for (int i : myItems) {
            if (i != -1) {
                slotsNeeded++;
            }
        }

        for (int p : otherItems) {
            if (p != -1) {
                slotsNeeded++;
            }
        }

        return slotsNeeded <= freeSlots;
    }

    /**
     * Method sendMinorUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemSlot
     * @param pla
     * @param holder
     */
    public void sendMinorUpdate(int itemSlot, Player pla, TradeHolder holder) {
        Player otherPlayer = getOtherPlayer(pla);

        if (otherPlayer == pla) {
            throw new NullPointerException();
        }

        pla.getActionSender().setItem(Item.forId(holder.tradedItems[itemSlot]), holder.tradedItemsCount[itemSlot],
                itemSlot, 134, false);
        otherPlayer.getActionSender().setItem(Item.forId(holder.tradedItems[itemSlot]),
                holder.tradedItemsCount[itemSlot], itemSlot, 134, true);
    }

    /**
     * Method sendMajorUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param holder
     */
    public void sendMajorUpdate(Player pla, TradeHolder holder) {
        Player otherPlayer = getOtherPlayer(pla);

        pla.getActionSender().sendItems(134, holder.tradedItems, holder.tradedItemsCount, false);
        otherPlayer.getActionSender().sendItems(134, holder.tradedItems, holder.tradedItemsCount, true);
    }

    /**
     * Method duelChanged
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void duelChanged() {
        int id  = -1;
        int id2 = -1;

        if (currentStatus == STATUS_SECOND_SCREEN) {
            id  = 626;
            id2 = 45;
        } else {
            id  = 631;
            id2 = 26;
        }

        for (int i = 0; i < players.length; i++) {
            holders[i].status = 0;
            players[i].getGameFrame().sendString("", id, id2);
        }
    }

    private int getIndex(Player pla) {
        int i = 0;

        for (i = 0; i < players.length; i++) {
            if (players[i] == pla) {
                return i;
            }
        }

        throw new RuntimeException();
    }

    /**
     * Method stakeItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param amount
     * @param player
     */
    public void stakeItem(int slot, int amount, Player player) {
        if(amount <= 0)
            return;

        if (isTerminated || ((currentStatus != STATUS_FIRST_SCREEN) && (currentStatus != STATUS_SECOND_SCREEN))) {
            return;
        }

        if (player.getAccount().hasVar("trade_lock") || player.getAccount().getIronManMode() > 0) {
            player.getActionSender().sendMessage("You are trade-locked");

            return;
        }

        if (getOtherPlayer(player).getAccount().hasVar("trade_lock") || getOtherPlayer(player).getAccount().getIronManMode() > 0) {
            player.getActionSender().sendMessage("This user is trade-locked and cannot stake.");

            return;
        }

        amount = player.getInventory().getItemMax(slot, amount);
        duelChanged();

        player.log("Staked item: "+player.getInventory().getItem(slot)+" "+amount);
        Item        stakedItem = player.getInventory().getItem(slot);
        int         index      = getIndex(player);
        TradeHolder myHolder   = holders[index];

        if (stakedItem == null) {
            return;
        }

        if (!stakedItem.isTradeable(player)) {
            player.getActionSender().sendMessage("You can't stake this item.");

            return;
        }

        if (amount == 0) {
            return;
        }

        if (myHolder.freeSlots() == 0) {
            player.getActionSender().sendMessage("You can't stake more than 9 items at once.");

            return;
        }

        if (!stakedItem.isStackable() && (myHolder.freeSlots() < amount)) {
            amount = myHolder.freeSlots();
            player.getActionSender().sendMessage("You can't stake more than 9 items at once.");
        }

        int added_slot = myHolder.addItem(stakedItem.getIndex(), amount);

        if (added_slot == -1) {
            player.getActionSender().sendMessage("You can't stake more than 9 items at once.");

            return;
        }

        if (stakedItem.isStackable() || (amount == 1)) {
            player.getInventory().deleteItemFromSlot(stakedItem, amount, slot);
            this.sendMinorUpdate(added_slot, player, myHolder);

            return;
        } else {
            player.getInventory().deleteItem(stakedItem.getIndex(), amount);
            sendMajorUpdate(player, myHolder);
        }
    }

    /**
     * Method unstakeItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param amt
     * @param player
     */
    public void unstakeItem(int slot, int amt, Player player) {
        if (isTerminated || ((currentStatus != STATUS_FIRST_SCREEN) && (currentStatus != STATUS_SECOND_SCREEN))) {
            return;
        }

        if (currentStatus == STATUS_SECOND_SCREEN) {
            return;
        }

        duelChanged();

        int         index  = getIndex(player);
        TradeHolder holder = holders[index];

        amt = holder.getItemMax(holder.tradedItems[slot], amt);

        int  itemId = holder.tradedItems[slot];
        Item asItem = Item.forId(itemId);
        if(asItem == null)
            return;


        if (asItem.isStackable() || (amt == 1)) {
            holder.removeItem(slot, amt);
            player.getInventory().addItem(asItem, amt);
            sendMinorUpdate(slot, player, holder);
        } else {
            holder.removeItems(itemId, amt);
            player.getInventory().addItem(itemId, amt);
            sendMajorUpdate(player, holder);
        }
    }

    /**
     * Method setStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param status
     */
    public void setStatus(int status) {
        currentStatus = status;
    }

    /**
     * Method getMyPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public Player getMyPlayer(Player player) {
        for (int i = 0; i < players.length; i++) {
            if (players[i] == player) {
                return player;
            }
        }

        throw new RuntimeException();
    }

    /**
     * Method getOtherPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public Player getOtherPlayer(Player player) {
        for (int i = 0; i < players.length; i++) {
            if (players[i] != player) {
                return players[i];
            }
        }

        throw new RuntimeException();
    }

    /**
     * Method resetAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetAll() {
        settings.clear();

        for (Player pla : players) {
            pla.getActionSender().clearInterface(6669);
            pla.getActionSender().clearInterface(6670);
            pla.getActionSender().changeLine("", 6684);
        }
    }

    private int getBitMask() {
        int mask = 0;

        for (Integer i : settings) {
            mask += i;
        }

        return mask;
    }

    private void updateInterface() {
        for (Player pla : players) {
            pla.getActionSender().sendVar(286, getBitMask());
        }
    }

    /**
     * Method setting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param set
     *
     * @return
     */
    public boolean setting(int set) {
        return settings.contains(set);
    }

    /**
     * Method restore_stats
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void restore_stats(Player pla) {
        pla.restore_all_stats();
        pla.setCurrentSpecial(10);

        pla.getTimers().resetTimer(TimingUtility.OVERLOAD_REPLENISH_TIMER);
        pla.getTimers().resetTimer(TimingUtility.OVERLOAD_TIMER);

        SpecialAttacks.updateSpecial(pla);
        pla.getPrayerBook().resetAll();
        pla.getPrayerBook().setPoints(pla.getMaxStat(5));
        pla.getPrayerBook().updatePrayer();
        pla.setPoisonTime(0);

        if (pla.getAccount().hasVar("v")) {
            pla.getAccount().removeVar("v");
        }


        pla.setCurrentHealth(pla.getMaxHealth());
        pla.getActionSender().sendStat(5);
        pla.getActionSender().sendStat(3);
    }

    /**
     * Method give_items_to_winner
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void give_items_to_winner(Player pla) {
        if(rewardGiven == true){
            for(Player player : World.getWorld().getPlayers()){
                if(player.getRights() >= Player.MODERATOR)
                    player.getActionSender().sendMessage(Text.RED("DUPE ATTEMPTED: " + pla));
            }
            return;

        }

        rewardGiven = false;
        if (currentStatus == STATUS_IN_FIGHT) {
            pla.getAccount().addStakeWin();
            pla.addAchievementProgress2(Achievement.STAKER, 1);
            pla.addAchievementProgress2(Achievement.PRO_STAKER, 1);
            holders[getIndex(pla)].returnItems(pla);
            holders[getOtherIndex(pla)].returnItems(pla);


        }
    }

    /**
     * Method prepare_for_fight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void prepare_for_fight(Player pla) {
        restore_stats(pla);
        pla.getCombatAdapter().delay(7);

        pla.setSpecialMultiplier(1.00);
        if (setting(NO_HELM)) {
            pla.getEquipment().unwieldItem(Equipment.HAT);
        }

        if (setting(Duel.NO_HELM)) {
            pla.getEquipment().unwieldItem(Equipment.HAT);
        }

        if (setting(Duel.NO_CAPE)) {
            pla.getEquipment().unwieldItem(Equipment.CAPE);
        }

        if (setting(Duel.NO_GLOVES)) {
            pla.getEquipment().unwieldItem(Equipment.HANDS);
        }

        if (setting(NO_WEAPON)) {
            pla.getEquipment().unwieldItem(Equipment.WEAPON);
        }

        if (setting(NO_BODY)) {
            pla.getEquipment().unwieldItem(Equipment.TORSO);
        }

        if (setting(NO_LEGS)) {
            pla.getEquipment().unwieldItem(Equipment.LEGS);
        }

        if (setting(NO_BOOTS)) {
            pla.getEquipment().unwieldItem(Equipment.FEET);
        }

        if (setting(NO_CAPE)) {
            pla.getEquipment().unwieldItem(Equipment.CAPE);
        }

        if (setting(NO_SHIELD)) {
            pla.getEquipment().unwieldItem(Equipment.SHIELD);

            if (pla.getEquipment().getWeapon().isTwoHanded()) {
                pla.getEquipment().unwieldItem(Equipment.WEAPON);
            }
        }

        if (setting(NO_AMULET)) {
            pla.getEquipment().unwieldItem(Equipment.AMULET);
        }

        if (setting(NO_ARROWS)) {
            pla.getEquipment().unwieldItem(Equipment.ARROWS);
        }

        if (setting(NO_RINGS)) {
            pla.getEquipment().unwieldItem(Equipment.RING);
        }

        pla.setCurrentHealth(pla.getMaxHealth());
        pla.getActionSender().sendStat(3);

       pla.setAutocast(-1);

        try {
            pla.getTimers().setTime(TimingUtility.DUEL_WAIT_TIMER, 8);

            SignedBlock start_block = World.getWorld().getScriptManager().get_block("duel_start");

            pla.getNewScriptQueue().add(new Scope(pla, start_block));
        } catch (Exception ee) {}

    }

    /**
     * Method getStateType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStatus() {
        return currentStatus;
    }

    /**
     * Method canWield
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public boolean canWield(int item) {
        if (setting(NO_CAPE) && (item == Equipment.CAPE)) {
            return false;
        }

        if (setting(NO_AMULET) && (item == Equipment.AMULET)) {
            return false;
        }

        if (setting(NO_SHIELD) && EquipmentTypes.isTwoHanded(item)) {
            return false;
        }

        if (setting(NO_LEGS) && (item == Equipment.LEGS)) {
            return false;
        }

        if (setting(NO_GLOVES) && (item == Equipment.HANDS)) {
            return false;
        }

        if (setting(NO_HELM) && (item == Equipment.HAT)) {
            return false;
        }

        if (setting(NO_RINGS) && (item == Equipment.RING)) {
            return false;
        }

        if (setting(NO_SHIELD) && (item == Equipment.SHIELD)) {
            return false;
        }

        if (setting(NO_BODY) && (item == Equipment.TORSO)) {
            return false;
        }

        if (setting(NO_WEAPON) && (item == Equipment.WEAPON)) {
            return false;
        }

        if (setting(NO_ARROWS) && (item == Equipment.ARROWS)) {
            return false;
        }

        if (setting(NO_BOOTS) && (item == Equipment.FEET)) {
            return false;
        }

        return true;
    }

    /**
     * Method buttonSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setting
     */
    public void buttonSetting(int setting) {
        duelChanged();

        if (settings.contains((Integer) setting)) {
            settings.remove((Integer) setting);
        } else {
            settings.add(setting);
        }

        sort();
        updateInterface();
    }

    /**
     * Method onButton
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param button
     */
    public void onButton(int button) {
        if (currentStatus == STATUS_SECOND_SCREEN) {
            return;
        }

        switch (button) {
        case 28 :
            buttonSetting(NO_RANGED);

            break;

        case 30 :
            buttonSetting(NO_MELEE);

            break;

        case 32 :
            buttonSetting(NO_MAGIC);

            break;

        case 48 :
            buttonSetting(NO_SPEC);

            break;

        case 34 :
            buttonSetting(FUN_WEAPONS);

            break;

        case 36 :
            buttonSetting(NO_FOREFIT);

            break;

        case 38 :
            buttonSetting(NO_DRINKS);

            break;

        case 40 :
            buttonSetting(NO_FOOD);

            break;

        case 42 :
            buttonSetting(NO_PRAYER);

            break;

        case 44 :
            buttonSetting(NO_MOVEMENT);

            break;

        case 46 :
            buttonSetting(OBSTACLES);

            break;

        case 54 :
            buttonSetting(NO_HELM);

            break;

        case 55 :
            buttonSetting(NO_CAPE);

            break;

        case 59 :
            buttonSetting(NO_BODY);

            break;

        case 61 :
            buttonSetting(NO_LEGS);

            break;

        case 60 :
            buttonSetting(NO_SHIELD);

            break;

        case 57 :
            buttonSetting(NO_ARROWS);

            break;

        case 56 :
            buttonSetting(NO_AMULET);

            break;

        case 58 :
            buttonSetting(NO_WEAPON);

            break;

        case 62 :
            buttonSetting(NO_RINGS);

            break;

        case 63 :
            buttonSetting(NO_BOOTS);

            break;

        case 64 :
            buttonSetting(NO_GLOVES);

            break;
        }
    }

    private static int getMask(int id) {
        int b = 1 << id;

        return b;
    }
}


/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
class TradeHolder {
    protected int[] tradedItems      = new int[28];
    protected int[] tradedItemsCount = new int[28];
    protected int   status           = 0;
    protected int   count            = 0;

    /**
     * Constructs ...
     *
     */
    public TradeHolder() {
        this(28);
    }

    /**
     * Constructs ...
     *
     *
     * @param leng
     */
    public TradeHolder(int leng) {
        this.tradedItems      = new int[leng];
        this.tradedItemsCount = new int[leng];

        for (int i = 0; i < leng; i++) {
            tradedItems[i] = -1;
        }
    }

    /**
     * Method returnItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void returnItems(Player pla) {
        for (int i = 0; i < tradedItems.length; i++) {
            if (tradedItems[i] != -1) {
                pla.getInventory().addItem(tradedItems[i], tradedItemsCount[i]);
            }
        }

        tradedItems      = new int[28];
        tradedItemsCount = new int[28];
        Arrays.fill(tradedItems, -1);
    }

    /**
     * Method total
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int total() {
        return count;
    }

    /**
     * Method getItemSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     *
     * @return
     */
    public int getItemSlot(int itemid) {
        for (int i = 0; i < tradedItems.length; i++) {
            if (tradedItems[i] == itemid) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getItemMax
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param count
     *
     * @return
     */
    public int getItemMax(int id, int count) {
        int curCount = 0;

        for (int i = 0; i < tradedItems.length; i++) {
            if (tradedItems[i] == id) {
                curCount += tradedItemsCount[i];
            }
        }

        if (curCount < count) {
            count = curCount;
        }

        return count;
    }

    /**
     * Method removeItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     * @param amount
     */
    public void removeItem(int slot, int amount) {

        if(amount <= 0)
            return;

        this.tradedItemsCount[slot] -= amount;

        if (this.tradedItemsCount[slot] <= 0) {
            this.tradedItems[slot] = -1;
        }
    }

    /**
     * Method removeItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amount
     */
    public void removeItems(int id, int amount) {

        if(amount <= 0)
            return;

        int amt = 0;

        for (int i = 0; i < tradedItems.length; i++) {
            if (amt == amount) {
                return;
            }

            if (tradedItems[i] == id) {
                amt++;
                count--;
                tradedItems[i]      = -1;
                tradedItemsCount[i] = 0;
            }
        }
    }

    /**
     * Method freeSlots
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int freeSlots() {
        return this.tradedItems.length - count;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amount
     *
     * @return
     */
    public int addItem(int itemid, int amount) {
        if(amount <= 0)
            return -1;
        int c    = 0;
        int slot = getItemSlot(itemid);

        if (amount == 0) {
            return 0;
        }

        if ((slot != -1) && Item.forId(itemid).isStackable()) {
            tradedItemsCount[slot] += amount;

            return slot;
        } else {
            for (int i = 0; i < tradedItems.length; i++) {
                if (tradedItems[i] == -1) {
                    tradedItems[i] = itemid;
                    count++;

                    if (Item.forId(itemid).isStackable() || (amount == 1)) {
                        tradedItemsCount[i] = amount;

                        return i;
                    }

                    c++;
                    tradedItemsCount[i] = 1;

                    if (c == amount) {
                        return i;
                    }
                }
            }
        }

        return -1;
    }
}
