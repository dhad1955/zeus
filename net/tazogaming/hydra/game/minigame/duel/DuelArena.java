package net.tazogaming.hydra.game.minigame.duel;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 13:36
 */
public class DuelArena extends Zone {
    public static int          TYPE_OBSTACLES   = 0,
                               TYPE_NO_MOVEMENT = 1,
                               TYPE_NORMAL      = 2;
    private static DuelArena[] arenas           = new DuelArena[3];
    private ArrayList<Point>   spawnLocations_1 = new ArrayList<Point>();
    private ArrayList<Point>   spawnLocations_2 = new ArrayList<Point>();
    private int                type;

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param type
     * @param lowerX
     * @param lowerY
     * @param higherX
     * @param higherY
     */
    public DuelArena(String name, int type, int lowerX, int lowerY, int higherX, int higherY) {
        super(lowerX, lowerY, higherX, higherY, 0, false, true, name);
        this.type = type;
        setTeleportAllowed(false);
        setBankingAllowed(false);
        setBobAllowed(false);
    }

    /**
     * Method addSpawnLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param area
     * @param x
     * @param y
     */
    public void addSpawnLocation(int area, int x, int y) {
        if (area == 0) {
            this.spawnLocations_1.add(Point.location(x, y, 0));
        } else {
            this.spawnLocations_2.add(Point.location(x, y, 0));
        }
    }

    /**
     * Method randomPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public Point randomPoint(int index) {
        if (index == 1) {
            return this.spawnLocations_1.get(GameMath.rand3(spawnLocations_1.size()));
        } else {
            return this.spawnLocations_2.get(GameMath.rand3(spawnLocations_2.size()));
        }
    }

    /**
     * Method place_users
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param duel
     * @param challenger
     * @param target
     *
     * @return
     */
    public static boolean place_users(Duel duel, Player challenger, Player target) {
        if (duel.setting(Duel.NO_MOVEMENT)) {
            DuelArena no_mov_arena = arenas[TYPE_NO_MOVEMENT];

            for (Point p : no_mov_arena.spawnLocations_1) {
                Tile t = World.getWorld().getTile(p);

                if (!t.hasPlayers()) {
                    challenger.teleport(p.getX(), p.getY(), 0, true);
                    target.teleport(p.getX() - 1, p.getY(), 0, true);

                    return true;
                } else {}
            }

            return false;
        }

        DuelArena cur_arena = null;

        if (duel.setting(Duel.OBSTACLES)) {
            cur_arena = arenas[TYPE_OBSTACLES];
        } else {
            cur_arena = arenas[TYPE_NORMAL];
        }

        if (cur_arena == null) {
            throw new NullPointerException("hm ? ");
        }

        Point chal_pt  = cur_arena.randomPoint(0);
        Point other_pt = cur_arena.randomPoint(1);

        challenger.teleport(chal_pt.getX(), chal_pt.getY(), 0, true);
        target.teleport(other_pt.getX(), other_pt.getY(), 0, true);

        return true;
    }

    /**
     * Method load_areas
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load_areas() {
        DuelArena main_arena = new DuelArena("main_arena", TYPE_NORMAL, 3364, 3225, 3390, 3240);

        main_arena.addSpawnLocation(0, 3386, 3235);
        main_arena.addSpawnLocation(0, 3386, 3230);
        main_arena.addSpawnLocation(0, 3385, 3234);
        main_arena.addSpawnLocation(1, 3367, 3229);
        main_arena.addSpawnLocation(1, 3367, 3234);
        main_arena.addSpawnLocation(1, 3371, 3232);
        arenas[2] = (main_arena);

        DuelArena obstacle_arena = new DuelArena("obstacle_1", TYPE_OBSTACLES, 3362, 3206, 3389, 3221);

        obstacle_arena.addSpawnLocation(0, 3384, 3214);
        obstacle_arena.addSpawnLocation(0, 3387, 3210);
        obstacle_arena.addSpawnLocation(1, 3366, 3211);
        obstacle_arena.addSpawnLocation(1, 3367, 3216);
        arenas[0] = (obstacle_arena);

        DuelArena no_movement = new DuelArena("no_mov_1", TYPE_NO_MOVEMENT, 3332, 3207, 3357, 3220);

        no_movement.addSpawnLocation(0, 3352, 3213);
        no_movement.addSpawnLocation(0, 3350, 3208);
        no_movement.addSpawnLocation(0, 3343, 3210);
        no_movement.addSpawnLocation(0, 3342, 3215);
        no_movement.addSpawnLocation(0, 3348, 3217);
        no_movement.addSpawnLocation(0, 3345, 3214);
        no_movement.addSpawnLocation(0, 3338, 3215);
        arenas[TYPE_NO_MOVEMENT] = no_movement;

        Zone duel_zone = new Zone("duel", no_movement, obstacle_arena, main_arena);
    }
}
