package net.tazogaming.hydra.game.minigame;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/14
 * Time: 01:09
 */
public class ClanWarRules {
    public static final int
        VICTORY_OPTION                     = 0,
        TIME_LIMIT                         = 1,
        STRAGGLERS                         = 2,
        ITEMS_ON_DEATH                     = 3,
        MELEE_DISABLED                     = 4,
        RANGE_DISABLED                     = 5,
        MAGE_OPTIONS                       = 6,
        SUMMONING_DISABLED                 = 7,
        FOOD_DISABLED                      = 8,
        POTS_DISABLED                      = 9,
        PRAYER_DISABLED                    = 10,
        ARENA_CHOICE                       = 11;
    public static int[]       settings     = new int[12];
    public static final int[] KILLS_TO_WIN = {
        -1, 25, 50, 100, 200, 400, 750, 1000, 2500, 5000, 10000
    };
    public static final int[] TIME_LIMITS  = {
        Core.getTicksForMinutes(5), Core.getTicksForMinutes(10), Core.getTicksForMinutes(30),
        Core.getTicksForMinutes(60), Core.getTicksForMinutes(90), Core.getTicksForMinutes(120),
        Core.getTicksForMinutes(180), Core.getTicksForMinutes(240), Core.getTicksForMinutes(300),
        Core.getTicksForMinutes(360), Core.getTicksForMinutes(420), Core.getTicksForMinutes(480),
        Core.getTicksForMinutes(520)
    };

    /** rules made: 14/09/30 */
    private int[] rules = new int[12];

    public enum SettingBit {
        VICTORY_OPTION(1), TIME_LIMIT(16), STRAGGLERS(256), ITEMS_ON_DEATH(512), MELEE_DISABLED(1024),
        RANGE_DISABLED(2048), MAGE_OPTIONS(4096), SUMMONING_DISABLED(16384), FOOD_DISABLED(32768),
        POTS_DISABLED(65536), PRAYER_DISABLED(131072), ARENA_CHOICE(67108863);

        /** flag made: 14/09/29 */
        private int flag;

        SettingBit(int flag) {
            this.flag = flag;
        }

        /**
         * Method getFlag
         * Created on 14/09/29
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getFlag() {
            return flag;
        }
    }

    /**
     * Method getKillsToWin
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    int getKillsToWin() {
        if (isKnockoutMode() || (settings[VICTORY_OPTION] == 15)) {
            return -1;
        }

        return KILLS_TO_WIN[settings[VICTORY_OPTION]];
    }

    /**
     * Method isKnockoutMode
     * Created on 14/09/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    boolean isKnockoutMode() {
        return settings[VICTORY_OPTION] == 0;
    }

    private void toggleRule(int ruleID) {
        if (rules[ruleID] == 1) {
            rules[ruleID] = 0;
        } else {
            rules[ruleID] = 1;
        }

        updateInterface();
    }

    public boolean magicAllowed() {
        return rules[MAGE_OPTIONS] < 3;
    }

    private void setRule(int ruleID, int rule) {
        rules[ruleID] = rule;
        updateInterface();
    }

    private void ruleChanged(int ruleID, int setTo) {
        if ((ruleID == VICTORY_OPTION) && (setTo == 10)) {
            if (rules[TIME_LIMIT] == 0) {
                rules[TIME_LIMIT] = 1;
            }
        }
    }

    private void updateInterface() {}
}
