package net.tazogaming.hydra.game.minigame.pkleague;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/14
 * Time: 00:20
 */
public interface DateComparator<T extends Object> {
    public boolean isDateReached(T t);
}
