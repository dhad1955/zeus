package net.tazogaming.hydra.game.minigame.pkleague;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.io.Buffer;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/14
 * Time: 00:57
 */
public abstract class LeagueController {
    List<PrizeEntry> unclaimedPrizeEntries = new ArrayList<PrizeEntry>();

    /**
     * Method onLeagueEntered
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     * @param place
     */
    public abstract void onLeagueEntered(int userid, int place);

    /**
     * Method onTimerUp
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param table
     */
    public abstract void onTimerUp(LeagueTable table);

    /**
     * Method onOverTakenBy
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     * @param overTakenBy
     * @param placeFrom
     * @param placeTo
     */
    public abstract void onOverTakenBy(LeagueEntry entry, LeagueEntry overTakenBy, int placeFrom, int placeTo);

    /**
     * Method onPlaceGained
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     * @param placeFrom
     * @param placeTo
     */
    public abstract void onPlaceGained(LeagueEntry entry, int placeFrom, int placeTo);

    /**
     * Method getPrizes
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract Prize[] getPrizes();

    /**
     * Method getLeaderboardHeader
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract String[] getLeaderboardHeader();

    /**
     * Method load
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buffer
     */
    public void load(Buffer buffer) {
        int siz = unclaimedPrizeEntries.size();

        for (int i = 0; i < siz; i++) {
            unclaimedPrizeEntries.add(new PrizeEntry(buffer.readShort(), buffer.readByte()));
        }
    }

    protected void doPrizeClaim(int userid, int place) {
        for (Player player : World.getWorld().getPlayers()) {
            if (player.getId() == userid) {
                getPrizes()[place].onPrizeClaim(player);

                return;
            }
        }

        unclaimedPrizeEntries.add(new PrizeEntry(userid, place));
    }

    /**
     * Method save
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buffer
     */
    public void save(Buffer buffer) {
        buffer.writeWord(unclaimedPrizeEntries.size());

        for (PrizeEntry p : unclaimedPrizeEntries) {
            buffer.writeWord(p.getUserId());
            buffer.writeByte(p.getPlace());
        }
    }
}
