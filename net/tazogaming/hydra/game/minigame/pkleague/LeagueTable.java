package net.tazogaming.hydra.game.minigame.pkleague;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.IOException;
import java.util.Calendar;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/14
 * Time: 00:19
 */
public class LeagueTable implements RecurringTickEvent {

    /** entries made: 14/10/24 */
    private LeagueEntry[] entries;

    /** nextTable made: 14/10/24 */
    private LeagueTable nextTable;

    /** entryCount made: 14/10/24 */
    private int entryCount;

    /** dateChecker made: 14/10/24 */
    private DateComparator<LeagueTable> dateChecker;

    /** controller made: 14/10/24 **/
    private LeagueController controller;

    private int lastDayRecorded = -1;

    /**
     * Constructs ...
     * @param max_size
     * @param comparator
     * @param controller
     */
    public LeagueTable(int max_size, DateComparator<LeagueTable> comparator, LeagueController controller) {
        this.entries     = new LeagueEntry[max_size];
        this.dateChecker = comparator;
        this.controller  = controller;
    }

    /**
     * Method getTop3
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LeagueEntry[] getTop3() {
        LeagueEntry[] topChampions = new LeagueEntry[3];

        System.arraycopy(entries, 0, topChampions, 0, 3);

        return topChampions;
    }

    /**
     * Method addEntrantsToNextRound
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entries
     */


    /**
     * Method updateTable
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateTable() {
        LeagueEntry[] entry = getTop3();

        for (int j = 0; j < entries.length - 1; j++) {
            boolean isSorted = true;

            for (int i = 1; i < entries.length - j; i++) {
                if ((entries[i] != null) && (entries[i].getScore() > entries[i - 1].getScore())) {
                    LeagueEntry tmpItem = entries[i];

                    entries[i]     = entries[i - 1];
                    entries[i - 1] = tmpItem;
                    isSorted       = false;
                }
            }

            if (isSorted) {
                break;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (entry[i] == null) {
                continue;
            }

            int ourRankNow = getRank(entry[i].getUserID());

            if (ourRankNow > i) {

                // dropped a place.
                this.controller.onOverTakenBy(entry[i], entries[i], i, ourRankNow);
            }

            if (ourRankNow < i) {

                // gained a place...
                this.controller.onPlaceGained(entry[i], i, ourRankNow);
            }
        }
    }

    /**
     * Method addPoint
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @return
     */
    public int getEntryCount() {
        return entryCount;
    }

    /**
     * Method getEntryByRank
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rank
     *
     * @return
     */
    public LeagueEntry getEntryByRank(int rank) {
        return entries[rank];
    }

    /**
     * Method getController
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LeagueController getController() {
        return this.controller;
    }

    /**
     * Method addPoint
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void addPoint(Player player) {
        LeagueEntry entry   = getEntryForUserId(player.getId());
        boolean     entered = false;

        if (entry == null) {
            entry = addEntry(player.getId(), player.getUsername());
            player.getActionSender().sendMessage("You have been entered into the pk league!");
            entered = true;
        }

        entry.addPoint();
        updateTable();

        if (entered) {
            controller.onLeagueEntered(player.getId(), getRank(player.getId()));
        }
    }

    /**
     * Method getRank
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     *
     * @return
     */
    public int getRank(int userid) {
        for (int i = 0; i < entryCount; i++) {
            if (entries[i].getUserID() == userid) {
                return i;
            }
        }

        return -1;
    }

    private int getIndexForUserId(int userid) {
        for (int i = 0; i < entryCount; i++) {
            if (entries[i].getUserID() == userid) {
                return i;
            }
        }

        return -1;
    }

    private LeagueEntry getEntryForUserId(int userid) {
        int i = getIndexForUserId(userid);

        return (i == -1)
               ? null
               : entries[i];
    }

    /**
     * Method entryExists
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     *
     * @return
     */
    public boolean entryExists(int userid) {
        return getEntryForUserId(userid) != null;
    }

    /**
     * Method addEntry
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     * @param username
     *
     *
     * @return
     * @throws IndexOutOfBoundsException
     */
    public LeagueEntry addEntry(int userid, String username) throws IndexOutOfBoundsException {
        if (this.entryCount >= entries.length) {
            throw new IndexOutOfBoundsException("Error max entries exceeded");
        }

        return this.entries[entryCount++] = new LeagueEntry(userid, username);
    }

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {

        if(lastDayRecorded == -1)
            lastDayRecorded = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);

        if(Core.currentTime % 50 != 0){
            if(Calendar.getInstance().get(Calendar.DAY_OF_WEEK) != lastDayRecorded){
                for(int i = 0; i < 3; i++){
                    if(i < entryCount) {
                          switch (i){
                              case 0:
                              break;
                              case 2:
                              break;
                              case 3:
                              break;
                          }
                    }
                }
            }
        }
        if (dateChecker.isDateReached(this)) {

        }
    }

    private void save(Buffer buffer) {
        buffer.writeWord(entryCount);

        for (int i = 0; i < entryCount; i++) {
            buffer.writeWord(entries[i].getUserID());
            buffer.writeWord(entries[i].getScore());
        }

        if (nextTable != null) {
            nextTable.save(buffer);
        }
    }

    /**
     * Method save
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     *
     * @throws IOException
     */
    public void save(String path) throws IOException {
        Buffer buffer = new Buffer(new byte[500000]);

        save(buffer);
        buffer.save(path);
    }

    /**
     * Method setNextTable
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param table
     */
    public void setNextTable(LeagueTable table) {
        this.nextTable = table;
    }

    /**
     * Method load
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     *
     * @throws IOException
     */
    public void load(String path) throws IOException {
        load(new Buffer(new File(path)));
    }

    private void load(Buffer buffer) {
        this.entryCount = buffer.readShort();

        for (int i = 0; i < entryCount; i++) {
            int         userid = buffer.readShort();
            int         score  = buffer.readShort();
            LeagueEntry entry  = entries[i] = new LeagueEntry(userid, "todo");

            entry.setScore(score);
        }

        updateTable();
    }

    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
        return false;
    }
}
