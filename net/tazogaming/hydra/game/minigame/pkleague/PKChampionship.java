package net.tazogaming.hydra.game.minigame.pkleague;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.minigame.pkleague.impl.championship.DailyLeagueController;
import net.tazogaming.hydra.game.minigame.pkleague.impl.championship.MonthlyLeagueController;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

import java.util.Calendar;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/14
 * Time: 00:41
 */
public class PKChampionship {
    public static final int   MAX_ENTRANTS_LEAGUE_1 = 500;
    public static final int   MAX_ENTRANTS_LEAGUE_2 = 40;
    public static LeagueTable lowerLeagueTable      = new LeagueTable(MAX_ENTRANTS_LEAGUE_1,
                                                          new DateComparator<LeagueTable>() {
        @Override
        public boolean isDateReached(LeagueTable leagueTable) {
            Calendar rightNow = Calendar.getInstance();
            int      hour     = rightNow.get(Calendar.HOUR_OF_DAY);


            return Core.currentTime > 100;

            //return hour == 0;
        }
    }, new DailyLeagueController());
    public static LeagueTable higherLeagueTable = new LeagueTable(MAX_ENTRANTS_LEAGUE_2,
                                                      new DateComparator<LeagueTable>() {
        @Override
        public boolean isDateReached(LeagueTable leagueTable) {
            Calendar rightNow = Calendar.getInstance();
            int      day      = rightNow.get(Calendar.DAY_OF_MONTH);

            return day == 29;
        }
    }, new MonthlyLeagueController());

    static {
        try {
           // lowerLeagueTable.setNextTable(higherLeagueTable);

           // higherLeagueTable.load("config/championship/lowertable.dat");
            //lowerLeagueTable.load("config/championship/highertable.dat");


        } catch (Exception ee) {
            ee.printStackTrace();
            System.err.println("Failed to load league tables");
        }
    }

    /**
     * Method sendWorldNotification
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param notif
     */
    public static void sendWorldNotification(String notif) {
        World.getWorld().globalMessage(Text.RED("[Championship]:")+" <col=9966FF> " + notif);
    }


    public static void showLeaderboard(Player player, LeagueTable table) {
        player.getGameFrame().openWindow(275);
        player.getGameFrame().sendString(table.getController().getLeaderboardHeader()[0], 275, 2);

        int cur = 16;

        for(int c = 1; c < table.getController().getLeaderboardHeader().length - 1; cur++) {
            player.getGameFrame().sendString(table.getController().getLeaderboardHeader()[c++], 275, cur);
        }

        int max = table.getEntryCount();
        if(max > 70)
            max = 70;

         for(int i = 0; i <  70; i++) {
            LeagueEntry entry = table.getEntryByRank(i);
            player.getGameFrame().sendString(Text.BLUE(Text.getOrdinal(i + 1))+".  -  "+Text.BLACK(entry.getCachedUsername())+" -   "+Text.DARK_RED("[ "+Text.DARK_RED(""+entry.getScore())+" ]"), 275, cur++);
        }

        for(; cur < 100; cur++) {
            player.getGameFrame().sendString("", 275, cur);
        }
    }
}
