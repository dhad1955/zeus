package net.tazogaming.hydra.game.minigame.pkleague.impl.championship;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueController;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueEntry;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueTable;
import net.tazogaming.hydra.game.minigame.pkleague.Prize;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/14
 * Time: 01:12
 */
public class MonthlyLeagueController extends LeagueController {

    private static final Prize LEAGUE_COMPLETED_PLACE_1 = new Prize() {
        @Override
        public void onPrizeClaim(Player player) {
            player.addPoints(Points.PK_POINTS, 3);
            player.addPoints(Points.PK_POINTS_2, 30);
        }
    };

    private static final Prize LEAGUE_COMPLETED_PLACE_2 = new Prize() {
        @Override
        public void onPrizeClaim(Player player) {
            player.addPoints(Points.PK_POINTS, 2);
            player.addPoints(Points.PK_POINTS_2, 20);
        }
    };


    private static final Prize LEAGUE_COMPLETED_PLACE_3 = new Prize() {
        @Override
        public void onPrizeClaim(Player player) {
            player.addPoints(Points.PK_POINTS, 1);
            player.addPoints(Points.PK_POINTS_2, 15);
        }
    };


    private static final Prize LEAGUE_WON_PLACE_1 = new Prize() {
        @Override
        public void onPrizeClaim(Player player) {
            player.addPoints(Points.PK_POINTS_2, 300);
        }
    };

    private static final Prize LEAGUE_WON_PLACE_2 = new Prize() {
        @Override
        public void onPrizeClaim(Player player) {
            player.addPoints(Points.PK_POINTS_2, 200);
        }

    };

    private static final Prize LEAGUE_WON_PLACE_3 = new Prize() {
        @Override
        public void onPrizeClaim(Player player) {
            player.addPoints(Points.PK_POINTS_2, 100);
        }
    };


    @Override
    public void onLeagueEntered(int userid, int place) {
        this.doPrizeClaim(userid, place);
    }

    @Override
    public void onTimerUp(LeagueTable table) {
        table.updateTable();
        for(int i = 0; i < table.getTop3().length; i++) {
            doPrizeClaim(table.getTop3()[i].getUserID(), i + 3);
        }
    }

    @Override
    public void onOverTakenBy(LeagueEntry entry, LeagueEntry overTakenBy, int placeFrom, int placeTo) {

    }

    @Override
    public void onPlaceGained(LeagueEntry entry, int placeFrom, int placeTo) {

    }

    @Override
    public Prize[] getPrizes() {
        return new Prize[] {LEAGUE_COMPLETED_PLACE_1, LEAGUE_COMPLETED_PLACE_2, LEAGUE_COMPLETED_PLACE_3, LEAGUE_WON_PLACE_1, LEAGUE_WON_PLACE_2, LEAGUE_WON_PLACE_3};
    }

    @Override
    public String[] getLeaderboardHeader() {
        return new String[0];
    }
}

