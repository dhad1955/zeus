package net.tazogaming.hydra.game.minigame.pkleague.impl.championship;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.minigame.pkleague.*;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/14
 * Time: 01:04
 */
public class DailyLeagueController extends LeagueController {

    /**
     * Method onLeagueEntered
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     * @param place
     */
    @Override
    public void onLeagueEntered(int userid, int place) {
        Player player = World.getWorld().getPlayerByID(userid);
       if(player != null) {
            player.getActionSender().sendMessage("You have been entered into the daily pk league");
            player.getActionSender().sendMessage("You are ranked "+(Text.getOrdinal(place + 1)));
       }


    }

    /**
     * Method onTimerUp
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param table
     */
    @Override
    public void onTimerUp(LeagueTable table) {
        PKChampionship.sendWorldNotification("Todays PK Tournament has finished!");
        PKChampionship.sendWorldNotification("Today's winners are:");

        LeagueEntry[] top3 = table.getTop3();

        for (int i = 0; i < 3; i++) {
            PKChampionship.sendWorldNotification(Text.getOrdinal(i + 1) + " " + top3[i].getCachedUsername());
        }
    }

    /**
     * Method onOverTakenBy
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     * @param overTakenBy
     * @param placeFrom
     * @param placeTo
     */
    @Override
    public void onOverTakenBy(LeagueEntry entry, LeagueEntry overTakenBy, int placeFrom, int placeTo) {
        PKChampionship.sendWorldNotification(entry.getCachedUsername() + " has been overtaken by "
                + overTakenBy.getCachedUsername() + " for " + Text.getOrdinal(placeFrom + 1)
                + " place.");
    }

    /**
     * Method onPlaceGained
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     * @param placeFrom
     * @param placeTo
     */
    @Override
    public void onPlaceGained(LeagueEntry entry, int placeFrom, int placeTo) {
        if(placeTo == 0) {
            PKChampionship.sendWorldNotification(entry.getCachedUsername()+" has taken first place in today's league");
        }else if(placeTo == 1) {
            PKChampionship.sendWorldNotification(entry.getCachedUsername()+" is now in 2nd in today's league");
        } else if(placeTo == 2){
            PKChampionship.sendWorldNotification(entry.getCachedUsername()+" is now in the Top-3");

        }

    }

    /**
     * Method getPrizes
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public Prize[] getPrizes() {
        return null;
    }

    @Override
    public String[] getLeaderboardHeader() {
        return new String[]
                {
                        "Champions League PK Tournament (Daily)",
                        "",
                        "This is a list of todays top Pkers (by kills)",
                        "Leaderboard resets midnight every day",
                        "Refer to the PK League guide on the forums for more info",
                        "",
                        " -------- Todays leaderboard -------"

                };

    }
}
