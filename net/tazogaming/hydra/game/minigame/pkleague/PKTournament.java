package net.tazogaming.hydra.game.minigame.pkleague;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/10/14
 * Time: 02:54
 */
public class PKTournament implements RecurringTickEvent {

    public static PKTournament puresLeaderboard = new PKTournament("Pures", "pures_pkboard.dat");
    public static PKTournament mainsLeaderboard = new PKTournament("Mains", "mains_lboard.dat");
    public static PKTournament zerkersLeaderboard = new PKTournament("Zerkers [40-46 def]", "zerkers_lboard.dat");

    public static final String[] DAYS     = {
        "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday",
    };

    /** lastDayRecorded made: 14/10/28 */
    private int lastDayRecorded = -1;
    private String themeName;
    private String fileName;



    public PKTournament(String themeName, String fileName) {
        this.themeName = themeName;
        this.fileName = fileName;

    }
    /** prizes made: 14/10/28 */
    private List<QueuedPrize> prizes = new ArrayList<QueuedPrize>();

    /** entries made: 14/10/28 */
    private LeagueEntry[] entries = new LeagueEntry[1000];

    /** entryCount made: 14/10/24 */
    private int entryCount;

    /**
     * controller made: 14/10/24
     *
     * @return
     */

    /**
     * Method getTop3
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LeagueEntry[] getTop3() {
        LeagueEntry[] topChampions = new LeagueEntry[3];

        System.arraycopy(entries, 0, topChampions, 0, 3);

        return topChampions;
    }

    /**
     * Method updateTable
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateTable() {
        LeagueEntry[] entry = getTop3();

        for (int j = 0; j < entries.length - 1; j++) {
            boolean isSorted = true;

            for (int i = 1; i < entries.length - j; i++) {
                if ((entries[i] != null) && (entries[i].getScore() > entries[i - 1].getScore())) {
                    LeagueEntry tmpItem = entries[i];

                    entries[i]     = entries[i - 1];
                    entries[i - 1] = tmpItem;
                    isSorted       = false;
                }
            }

            if (isSorted) {
                break;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (entry[i] == null) {
                continue;
            }

            int ourRankNow = getRank(entry[i].getUserID());

            if (ourRankNow > i) {

                // dropped a place.
                onOverTakenBy(entry[i], entries[i], i, ourRankNow);
            }

            if (ourRankNow < i) {

                // gained a place...
                onPlaceGained(entry[i], i, ourRankNow);
            }
        }
    }

    /**
     * Method onOverTakenBy
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     * @param overTakenBy
     * @param placeFrom
     * @param placeTo
     */
    public void onOverTakenBy(LeagueEntry entry, LeagueEntry overTakenBy, int placeFrom, int placeTo) {
        Player player = World.getWorld().getPlayerByID(entry.getUserID());

        if (player != null) {
            mes("You have been overtaken in today's "+themeName+" leaderboard by " + overTakenBy.getCachedUsername(), player);
            mes("You are now ranked " + Text.getOrdinal(placeTo + 1), player);
        }

        PKChampionship.sendWorldNotification(entry.getCachedUsername() + " has been overtaken by "
                + overTakenBy.getCachedUsername() + " for " + Text.getOrdinal(placeFrom + 1) + " place in "+themeName+"'s leaderboard.");
    }

    /**
     * Method onPlaceGained
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     * @param placeFrom
     * @param placeTo
     */
    public void onPlaceGained(LeagueEntry entry, int placeFrom, int placeTo) {
        Player player = World.getWorld().getPlayerByID(entry.getUserID());

        if (player != null) {
            mes("You have gone up a place in todays "+themeName+" leaderboard you are now ranked " + Text.getOrdinal(placeTo + 1),
                player);
        }

        if (placeTo == 0) {
            PKChampionship.sendWorldNotification(entry.getCachedUsername()
                    + " has taken first place in today's "+themeName+" league");
        } else if (placeTo == 1) {
            PKChampionship.sendWorldNotification(entry.getCachedUsername() + " is now in 2nd in today's "+themeName+" league");
        } else if (placeTo == 2) {
            PKChampionship.sendWorldNotification(entry.getCachedUsername() + " is now in the Top-3 "+themeName+" leaderboard");
        }
    }

    private static void mes(String msg, Player player) {
        player.getActionSender().sendMessage(Text.RED("[Championship]:") + " <col=9966FF> " + msg);
    }

    /**
     * Method addPoint
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void addPoint(Player player) {
        LeagueEntry entry = getEntryForUserId(player.getId());

        if (entry == null) {
            entry = addEntry(player.getId(), player.getUsername());
            player.addAchievementProgress2(Achievement.ENTRANT, 1);
            mes(("You have been entered into todays "+themeName+" leaderboard!"), player);
            mes(("You are ranked " + Text.getOrdinal(getRank(player.getId()) + 1)), player);
        }

        entry.addPoint();
        updateTable();
    }

    /**
     * Method getRank
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     *
     * @return
     */
    public int getRank(int userid) {
        for (int i = 0; i < entryCount; i++) {
            if (entries[i].getUserID() == userid) {
                return i;
            }
        }

        return -1;
    }

    private int getIndexForUserId(int userid) {
        for (int i = 0; i < entryCount; i++) {
            if (entries[i].getUserID() == userid) {
                return i;
            }
        }

        return -1;
    }

    private LeagueEntry getEntryForUserId(int userid) {
        int i = getIndexForUserId(userid);

        return (i == -1)
               ? null
               : entries[i];
    }

    /**
     * Method entryExists
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     *
     * @return
     */
    public boolean entryExists(int userid) {
        return getEntryForUserId(userid) != null;
    }

    /**
     * Method addEntry
     * Created on 14/10/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     * @param username
     *
     *
     * @return
     * @throws IndexOutOfBoundsException
     */
    public LeagueEntry addEntry(int userid, String username) throws IndexOutOfBoundsException {
        if (this.entryCount >= entries.length) {
            throw new IndexOutOfBoundsException("Error max entries exceeded");
        }

        return this.entries[entryCount++] = new LeagueEntry(userid, username);
    }

    /**
     * Method save
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @param path
     */
    public void save(String path) {
        Buffer buffer = new Buffer(8000);

        buffer.writeWord(entryCount);
        buffer.writeByte(this.lastDayRecorded);

        for (int i = 0; i < entryCount; i++) {
            buffer.writeDWord(entries[i].getUserID());
            buffer.writeWord(entries[i].getScore());
            buffer.writeString(entries[i].getCachedUsername());
        }

        buffer.writeByte(this.prizes.size());

        for (QueuedPrize prize : prizes) {
            buffer.writeDWord(prize.getPlayerId());
            buffer.writeByte(prize.getPlace());
            buffer.writeByte(prize.getDayWon());
        }

        try {
            buffer.save(path);
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     * Method load
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buffer
     */
    public void load(Buffer buffer) {
        this.entryCount      = buffer.readShort();
        this.lastDayRecorded = buffer.readByte();

        for (int i = 0; i < entryCount; i++) {
            int         userid = buffer.readDWord();
            int         score  = buffer.readShort();
            LeagueEntry entry  = entries[i] = new LeagueEntry(userid, "todo");

            entry.setCachedUsername(buffer.readString());
            entry.setScore(score);
            System.err.println("Loaded PK Entrant ["+themeName+"]: "+entry.getCachedUsername());
        }

        int siz = buffer.readByte();

        for (int i = 0; i < siz; i++) {
            prizes.add(new QueuedPrize(buffer.readDWord(), buffer.readByte(), buffer.readByte()));
        }

        updateTable();
    }

    /**
     * Method tick
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        if (lastDayRecorded == -1) {
            lastDayRecorded = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        }

        if (Core.currentTime % 50 != 0) {
            if (Calendar.getInstance().get(Calendar.DAY_OF_WEEK) != lastDayRecorded || forceClaim) {
                forceClaim = false;
                for (int i = 0; i < 3; i++) {
                    LeagueEntry entry = getTop3()[i];

                    if ((i < entryCount) && (entry != null)) {
                        Player player = World.getWorld().getPlayerByID(entry.getUserID());

                        if (player != null) {
                           try {
                               onPrizeClaim(player, i, lastDayRecorded);
                           }catch (Exception ee){}
                        } else {
                            QueuedPrize prize = new QueuedPrize(entry.getUserID(), i, lastDayRecorded);

                            prizes.add(prize);
                        }
                    }
                }

                this.lastDayRecorded = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
                this.entryCount      = 0;
                this.entries         = new LeagueEntry[entries.length];
            }
        }
    }

    /**
     * Method checkPrizeClaim
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */

    public boolean forceClaim = false;

    public void checkPrizeClaim(Player player) {
        for (Iterator<QueuedPrize> prizeIterator = prizes.iterator(); prizeIterator.hasNext(); ) {
            QueuedPrize prize = prizeIterator.next();
            try {
                if (prize.getPlayerId() == player.getId()) {
                    onPrizeClaim(player, prize.getPlace(), prize.getDayWon());
                    prizeIterator.remove();
                }
            }catch (Exception ee){}
        }
    }

    /**
     * Method onPrizeClaim
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param place
     * @param day
     */
    public void onPrizeClaim(Player player, int place, int day) {
        player.addAchievementProgress2(Achievement.TOP_3, 1);
        switch (place) {
        case 0 :
            player.addAchievementProgress2(Achievement.DAILY_WINNER, 1);
            player.addAchievementProgress2(Achievement.WEEKLY_WINNER, 1);
            player.addAchievementProgress2(Achievement.MONTHLY_WINNER, 1);
            mes(("Congratulations you have won  " + DAYS[day] + "'s " + themeName + " pk tournament"), player);
            int itemid = ShopManager.getShop(34).randomItem();
            mes("You have been awarded 180 PKP + a "+ Item.forId(itemid).getName()+"", player);
            player.getBank().insert(995, 100000000);
            player.getBank().insert(6183, 1);
            player.getInventory().addItem(itemid, 1);
            player.addPoints(Points.PK_POINTS_2, 180);

            break;

        case 1 :
            mes("Congratulations you placed 2nd in " + DAYS[day] + "'s pk tournament", player);
            mes("You have been awarded 100 PKP", player);
            player.addPoints(Points.PK_POINTS_2, 100);
            player.getBank().insert(995, 80000000);

            break;

        case 3 :
            mes("Congratulations you have been placed 3rd in " + DAYS[day]
                    + "'s "+themeName+" pk tournament", player);
            mes("You have been awarded 80 PKP", player);
            player.addPoints(Points.PK_POINTS_2, 80);
            player.getBank().insert(995, 60000000);


            break;
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
        return false;
    }

    public static void showLeaderboard(Player player, PKTournament table) {
        player.getGameFrame().openWindow(275);
        player.getGameFrame().sendString("Todays top pkers", 275, 2);

        int cur = 16;

        player.getGameFrame().sendString("The top 3 Pkers will win PKP Prizes", 275, cur++);
        player.getGameFrame().sendString("Competition lasts 24 hours, and runs every day", 275, cur++);
        player.getGameFrame().sendString("", 275, cur++);
        player.getGameFrame().sendString("1st Place - "+Text.BLUE("180 PKP + Random item from shop + Treasure box + 100m"), 275, cur++);
        player.getGameFrame().sendString("2nd Place - "+Text.BLUE("100 PKP + 60m"), 275, cur++);
        player.getGameFrame().sendString("3rd Place - "+Text.BLUE("80 PKP + 30m"), 275, cur++);
        player.getGameFrame().sendString("", 275, cur++);


        for(int i = 0; i < table.entryCount; i++) {
            LeagueEntry entry = table.entries[i];
            player.getGameFrame().sendString(Text.BLUE(Text.getOrdinal(i + 1))+".  -  "+Text.BLACK(entry.getCachedUsername())+" -   "+Text.DARK_RED("[ "+Text.DARK_RED(""+entry.getScore())+" ]"), 275, cur++);
        }

        for(; cur < 100; cur++) {

            player.getGameFrame().sendString("", 275, cur);
        }
    }
}
