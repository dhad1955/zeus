package net.tazogaming.hydra.game.minigame.pkleague;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/10/14
 * Time: 03:03
 */
public class QueuedPrize {
    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public int getDayWon() {
        return dayWon;
    }

    public void setDayWon(int dayWon) {
        this.dayWon = dayWon;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public QueuedPrize(int playerId, int place, int dayWon) {
        this.playerId = playerId;
        this.place = place;
        this.dayWon = dayWon;
    }

    private int playerId;
    private int place;
    private int dayWon;

}
