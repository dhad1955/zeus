package net.tazogaming.hydra.game.minigame.pkleague;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/14
 * Time: 00:17
 */
public class LeagueEntry {
    private int userID;

    public String getCachedUsername() {
        return cachedUsername;
    }

    public void setCachedUsername(String cachedUsername) {
        this.cachedUsername = cachedUsername;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public LeagueEntry(int userID, String cachedUsername) {
        this.userID = userID;
        this.cachedUsername = cachedUsername;
    }

    private String cachedUsername;
    private int score;

    public int getScore() {
        return score;
    }
    public void addPoint() {
        score ++ ;
    }
    public void setScore(int score) {
        this.score = score;
    }
}
