package net.tazogaming.hydra.game.minigame.util.ilobby;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 22:13
 */
public interface LobbyListener {

    /**
     * Method onLobbyFinished
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lobby
     *
     * @return
     */
    public boolean onLobbyFinished(Lobby lobby);

    /**
     * Method onLobbyEmpty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lobby
     *
     * @return
     */
    public boolean onLobbyEmpty(Lobby lobby);
}
