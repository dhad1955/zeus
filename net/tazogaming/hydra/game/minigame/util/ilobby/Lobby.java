package net.tazogaming.hydra.game.minigame.util.ilobby;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;

//~--- JDK imports ------------------------------------------------------------

import java.util.Iterator;
import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 22:14
 */
public class Lobby extends Instance {
    private int                gameStartsIn   = -1;
    private LinkedList<Player> players        = new LinkedList<Player>();
    private int                minimumPlayers = 1;
    private int                maxPlayers     = 1;
    private boolean            instanceMode   = true;
    private LobbyMode          mode;
    private LobbyEntryPolicy   entryPolicy;
    private LobbyListener      listener;
    private Zone zone;
    private Point              centerPoint;
    private LobbyUIUpdater     updater;

    /**
     * Constructs ...
     *
     *
     * @param e
     */
    public Lobby(Zone e) {
        this.zone = e;
        e.setBankingAllowed(false);
    }

    /**
     * Method setInstanceMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setInstanceMode(boolean b) {
        instanceMode = b;
    }

    /**
     * Method setCenterPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void setCenterPoint(Point p) {
        centerPoint = p;
    }

    /**
     * Method setMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param m
     */
    public void setMode(LobbyMode m) {
        this.mode = m;
    }

    /**
     * Method setCountdown
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param seconds
     */
    public void setCountdown(int seconds) {
        gameStartsIn = Core.currentTime + Core.getTicksForSeconds(seconds);
    }

    /**
     * Method onLeave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param p
     */
    public void onLeave(Player player, Iterator<Player> p) {
        if (players.contains(player)) {
            player.setCurrentInstance(null);

            if (p == null) {
                players.remove(player);
            } else {
                p.remove();
            }

            if (players.size() == 0) {
                getListener().onLobbyEmpty(this);
            }

            if (updater != null) {
                updater.playerLeft(player, this);
            }
        }
    }

    /**
     * Method setUI
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ui
     */
    public void setUI(LobbyUIUpdater ui) {
        this.updater = ui;
    }

    /**
     * Method addPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public LobbyEntryResponse addPlayer(Player player) {
        if (players.contains(player)) {
            return LobbyEntryResponse.PLAYER_ALREADY_REGISTERED;
        }

        if ((entryPolicy != null) &&!entryPolicy.canEnter(player)) {
            return LobbyEntryResponse.REQUIREMENT_NOT_MET;
        }

        if (players.size() == maxPlayers) {
            return LobbyEntryResponse.TOO_MANY_PLAYERS;
        }

        players.add(player);
        player.setCurrentInstance(this);
        player.getAreas().add(zone);

        if (centerPoint != null) {
            player.teleport(centerPoint.getX(), centerPoint.getY(), centerPoint.getHeight());
        }

        if (updater != null) {
            updater.playerEntered(player, this);
        }

        return LobbyEntryResponse.OK;
    }

    /**
     * Method getSecondsTillStart
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSecondsTillStart() {
        return Core.getSecondsForTicks(Core.timeUntil(gameStartsIn));
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean update() {
        if (players.size() == 0) {
            return false;
        }

        for (Iterator<Player> iter = players.iterator(); iter.hasNext(); ) {
            Player player = iter.next();

            if (updater != null) {
                updater.updateUI(player, this);
            }

            if (!player.getAreas().contains(zone) ||!player.isLoggedIn()) {
                onLeave(player, iter);
            }
        }

        if ((players.size() == getMinimumPlayers()) && (mode == LobbyMode.PLAYER_COUNT_MODE)) {
            if (listener.onLobbyFinished(this)) {
                for (Player player : players) {
                    if (updater != null) {
                        updater.playerLeft(player, this);
                    }
                }

                players.clear();
            }

            return true;
        }

        if ((mode == LobbyMode.TIMER_MODE) && (Core.timeSince(gameStartsIn) > 0)
                && (players.size() >= getMinimumPlayers())) {
            if (listener.onLobbyFinished(this)) {
                for (Player player : players) {
                    if (updater != null) {
                        updater.playerLeft(player, this);
                    }
                }

                players.clear();
            }

            return true;
        }

        return true;
    }

    /**
     * Method setListener
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param listener
     */
    public void setListener(LobbyListener listener) {
        this.listener = listener;
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LinkedList<Player> getPlayers() {
        return players;
    }

    /**
     * Method setPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param players
     */
    public void setPlayers(LinkedList<Player> players) {
        this.players = players;
    }

    /**
     * Method getListener
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LobbyListener getListener() {
        return listener;
    }

    /**
     * Method getMaxPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaxPlayers() {
        return maxPlayers;
    }

    /**
     * Method setMaxPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param maxPlayers
     */
    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    /**
     * Method getMinimumPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMinimumPlayers() {
        return minimumPlayers;
    }

    /**
     * Method setMinimumPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param minimumPlayers
     */
    public void setMinimumPlayers(int minimumPlayers) {
        this.minimumPlayers = minimumPlayers;
    }

    /**
     * Method setEntryPolicy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param policy
     */
    public void setEntryPolicy(LobbyEntryPolicy policy) {
        this.entryPolicy = policy;
    }

    /**
     * Method getEntryPolicy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public LobbyEntryPolicy getEntryPolicy() {
        return entryPolicy;
    }
}
