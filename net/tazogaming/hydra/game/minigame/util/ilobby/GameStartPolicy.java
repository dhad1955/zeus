package net.tazogaming.hydra.game.minigame.util.ilobby;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/02/14
 * Time: 13:53
 */
public interface GameStartPolicy {
    public static final int
        RESTART_TIMER   = 0,
        WAIT_UNTIL_OKAY = 1,
        BOOT_ALL        = 2,
        OKAY            = 3;

    /**
     * Method getGameStartResponse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGameStartResponse();
}
