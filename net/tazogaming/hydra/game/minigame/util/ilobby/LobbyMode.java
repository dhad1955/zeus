package net.tazogaming.hydra.game.minigame.util.ilobby;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 22:24
 */
public enum LobbyMode { TIMER_MODE, PLAYER_COUNT_MODE, TRIGGER_MODE }
