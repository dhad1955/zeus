package net.tazogaming.hydra.game.minigame.pkraffle;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/10/14
 * Time: 13:11
 */
public class RaffleWinner {

    /** winnerID made: 14/10/21 **/
    private int winnerID;

    /** tokensAmount made: 14/10/21 **/
    private int tokensAmount;

    /** secondaryPrizeAmount made: 14/10/21 **/
    private int secondaryPrizeAmount;

    /** place made: 14/10/21 **/
    private int place;

    /**
     * Method getWinnerID
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWinnerID() {
        return winnerID;
    }

    /**
     * Method setWinnerID
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param winnerID
     */
    public void setWinnerID(int winnerID) {
        this.winnerID = winnerID;
    }

    /**
     * Method getPlace
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPlace() {
        return place;
    }

    /**
     * Method setPlace
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param place
     */
    public void setPlace(int place) {
        this.place = place;
    }

    /**
     * Method getSecondaryPrizeAmount
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSecondaryPrizeAmount() {
        return secondaryPrizeAmount;
    }

    /**
     * Method setSecondaryPrizeAmount
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param secondaryPrizeAmount
     */
    public void setSecondaryPrizeAmount(int secondaryPrizeAmount) {
        this.secondaryPrizeAmount = secondaryPrizeAmount;
    }

    /**
     * Method getTokensAmount
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTokensAmount() {
        return tokensAmount;
    }

    /**
     * Method setTokensAmount
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tokensAmount
     */
    public void setTokensAmount(int tokensAmount) {
        this.tokensAmount = tokensAmount;
    }
}
