package net.tazogaming.hydra.game.minigame.pkraffle;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.tools.HTTPost;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/03/14
 * Time: 21:25
 */
public class PKRaffle {

    /** winners made: 14/10/21 */
    private static ArrayList<Integer> winners    = new ArrayList<Integer>();
    public static final int           ENTRY_SIZE = 3500;

    /** ENTRIES made: 14/10/21 */
    private static int[] ENTRIES = new int[ENTRY_SIZE];

    /** entryOffset made: 14/10/21 */
    private static int entryOffset = 0;

    /** disqualifys made: 14/10/21 */
    private static ArrayList<Integer> disqualifys = new ArrayList<Integer>();

    /** raffleWinners made: 14/10/21 */
    private static List<RaffleWinner> raffleWinners = new ArrayList<RaffleWinner>();

    private static void messageServer(String message) {
        for (Player player : World.getWorld().getPlayers()) {
            if(player.getGameFrame().getRoot() == 548) {
                player.getActionSender().sendMessage("[" + Text.DARK_RED("PK-JACKPOT") + ""
                        + Text.BLACK("]: " + message));
            }else{
                player.getActionSender().sendMessage("[" + Text.DARK_RED("PK-JACKPOT") + ""
                        + message);
            }
        }
    }

    /**
     * Method checkWinners
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param winner
     */
    public static void checkWinners(Player winner) {
        if (winners.contains(winner.getId())) {
            winners.remove(new Integer(winner.getId()));
        }
    }

    /**
     * Method remaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static int remaining() {
        return ENTRIES.length - entryOffset;
    }

    /**
     * Method saveRaffle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void saveRaffle() {
        Buffer d = new Buffer(9000000);

        d.writeWord(entryOffset);

        for (int i = 0; i < entryOffset; i++) {
            d.writeDWord(ENTRIES[i]);
        }

        d.writeWord(disqualifys.size());

        for (Integer i : disqualifys) {
            d.writeWord(i);
        }

        d.writeByte(raffleWinners.size());

        for (RaffleWinner winner : raffleWinners) {
            d.writeWord(winner.getWinnerID());
            d.writeByte(winner.getPlace());
            d.writeWord(winner.getTokensAmount());
            d.writeWord(winner.getSecondaryPrizeAmount());
        }

        try {
            d.save("raffle.dat");
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method loadRaffle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadRaffle() {
        try {
            Buffer d = new Buffer("raffle.dat");

            entryOffset = d.readShort();

            for (int i = 0; i < entryOffset; i++) {
                ENTRIES[i] = d.readDWord();
            }

            int siz = d.readShort();

            for (int i = 0; i < siz; i++) {
                disqualifys.add(d.readDWord());
            }

            int winnerSiz = d.readByte();

            for (int i = 0; i < winnerSiz; i++) {
                RaffleWinner winner = new RaffleWinner();

                winner.setWinnerID(d.readShort());
                winner.setPlace(d.readByte());
                winner.setTokensAmount(d.readShort());
                winner.setSecondaryPrizeAmount(d.readShort());
                raffleWinners.add(winner);
            }
        } catch (Exception ee) {
            Logger.err("unable to load raffle");
            entryOffset = 0;
            ENTRIES     = new int[ENTRY_SIZE];
        }
    }

    /**
     * Method disqualify
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     */
    public static void disqualify(int userid) {
        disqualifys.add(userid);
        saveRaffle();
    }

    /**
     * Method getNextEntry
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param userid
     *
     * @return
     */
    public static int getNextEntry(int userid) {
        if (entryOffset > ENTRIES.length) {
            checkEntries();
        }

        ENTRIES[entryOffset] = userid;

        if (entryOffset == ENTRIES.length / 4) {
            messageServer("The PK prize raffle is now 1/4 of the way full, start pking now to win big tokens");
        } else if (entryOffset == ENTRIES.length / 2) {
            messageServer("The PK prize raffle is half way full, start pking now for to win big tokens!");
        } else if (entryOffset == ENTRIES.length - 10) {
            messageServer("Only 10 more kills until the PK Jackpot is DROPPED, start pking now for a huge prize.");
        }

        saveRaffle();

        return entryOffset++;
    }

    /**
     * Method query
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void query(Player player) {
        Map<Integer, Integer> list = new HashMap<Integer, Integer>();

        for (int i = 0; i < entryOffset; i++) {
            if (!list.containsKey(ENTRIES[i])) {
                list.put(ENTRIES[i], 1);
            } else {
                int count = list.get(ENTRIES[i]);

                list.put(ENTRIES[i], count + 1);
            }
        }

        for (Integer i : list.keySet()) {
            player.getActionSender().sendMessage(getUsernameForUserid(i) + " total: " + list.get(i));
        }
    }

    private static String getUsernameForUserid(int id) {
        try {
            ResultSet r =
                World.getWorld().getDatabaseLink().query("SELECT username, id from user where id='"
                        + id + "'");

            r.next();

            String str = r.getString("username");

            r.close();

            return str;
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        return "Error: " + id;
    }

    /**
     * Method checkEntries
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void checkEntries() {
        ArrayList<Integer> possibleEntries = new ArrayList<Integer>();

        if (entryOffset < ENTRIES.length - 1) {
            return;
        }

        for (int i = 0; i < ENTRIES.length; i++) {
            if (!disqualifys.contains(ENTRIES[i])) {
                possibleEntries.add(ENTRIES[i]);
            }
        }

        int[] prizes = new int[4];

        messageServer("The PK JACKPOT HAS DROPPED!");
        messageServer("WINNERS WILL FOLLOW...");
        possibleEntries.remove((Integer) 0);

        for (int i = 0; i < prizes.length; i++) {
            if (possibleEntries.size() == 0) {
                continue;
            }

            int ran = possibleEntries.get(Combat.GLOBAL_RANDOM.nextInt(possibleEntries.size()));

            if (ran == 1) {
                return;
            }

            prizes[i] = ran;
            winners.add(ran);

            int          prize  = i;
            RaffleWinner winner = new RaffleWinner();

            winner.setPlace(i);
            winner.setWinnerID(ran);

            switch (i) {
            case 0 :
                winner.setTokensAmount(1500);
                winner.setSecondaryPrizeAmount(2791);

                break;

            case 1 :
                winner.setTokensAmount(500);
                winner.setSecondaryPrizeAmount(2795);

                break;

            case 2 :
                winner.setTokensAmount(400);
                winner.setSecondaryPrizeAmount(2795);

                break;

            case 3 :
                winner.setTokensAmount(150);

                break;

            case 4 :
                winner.setTokensAmount(50);

                break;
            }

            boolean hasFound = false;

            for (Player player : World.getWorld().getPlayers()) {
                if (player.getId() == winner.getWinnerID()) {
                    winnerPK(player, winner);
                    hasFound = true;
                }
            }

            messageServer(Text.getOrdinal((i == 0)
                                          ? 1
                                          : i) + ". " + getUsernameForUserid(ran) + " - " + winner.getTokensAmount()
                                               + " pkp");

            if (!hasFound) {
                raffleWinners.add(winner);
            }

            while (possibleEntries.contains(ran)) {
                possibleEntries.remove((Integer) ran);
            }
        }

        entryOffset = 0;
        ENTRIES     = new int[ENTRY_SIZE];
    }

    /**
     * Method checkRaffle
     * Created on 14/10/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void checkRaffle(Player player) {
        RaffleWinner winner;

        for (Iterator<RaffleWinner> iter = raffleWinners.iterator(); iter.hasNext(); ) {
            winner = iter.next();

            if (winner.getWinnerID() == player.getId()) {
                winnerPK(player, winner);
                iter.remove();
            }
        }
    }

    private static void winnerPK(Player player, RaffleWinner winner) {
        Dialog.printStopMessage(player, "You have won the PK Raffle",
                                "You came " + Text.getOrdinal(winner.getPlace()) + "",
                                "You have been awarded " + winner.getTokensAmount());
        player.addPoints(Points.PK_POINTS_2, winner.getTokensAmount());
        player.addAchievementProgress2(Achievement.RAFFLE_WINNER, 1);

        if (winner.getSecondaryPrizeAmount() == 2791) {
            player.getActionSender().sendMessage("You have won a platinum box, check your bank");
            player.getBank().insert(2791, 1);
        } else if (winner.getSecondaryPrizeAmount() == 2795) {
            player.getActionSender().sendMessage("You have won a platinum box, check your bank");
            player.getBank().insert(2791, 1);
        }
    }

    /**
     * Method sendThread
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param prizes
     */
    public static void sendThread(int[] prizes) {
        if (Config.TEST_MODE) {
            return;
        }

        String thread =
            "The PK Jackpot has dropped, all 450 kills have been reached and 4 LUCKY winners have been selected at random.\n"
            + "\n" + "\n" + "PrizeEntry winners:\n" + "\n" + "[B]PrizeEntry 1 - $40.00 Cash or 200 shop tokens[/B]\n"
            + "Congratulations " + getUsernameForUserid(prizes[0])
            + " you have won the prize of $40.00 cash through paypal or 200 shop tokens, please contact [EMAIL=\"zeus.rsps@gmail.com\"]zeus.rsps@gmail.com[/EMAIL] to claim your prize.\n"
            + "\n" + "[b]PrizeEntry 2 - 100 shop tokens[/b]\n" + "Congratulations to "
            + getUsernameForUserid(prizes[1])
            + " you have won 100 shop tokens and they have been credited to your account\n" + "\n"
            + "[B]PrizeEntry 3 - 50 Million GP[/B]\n" + "Congratulations to " + getUsernameForUserid(prizes[2])
            + " you have won 50 shop tokens, and they have been credited to your account\n" + "\n"
            + "[B]PrizeEntry 4 - Free donator rank[/B]\n" + "Congratulations to " + getUsernameForUserid(prizes[3])
            + " You have won a free donor rank, this has been credited to your account if you wern't already donor!\n"
            + "\n" + "\n"
            + "The raffle has now been reset, these prizes are up for grabs ONCE again after 450 kills we will redraw the raffle.\n"
            + "\n" + "How does the raffle work:\n" + "\n"
            + "Everytime you get a legit kill in the wilderness you will be awarded with a raffle ticket (Pures with 1 defence get 2 tickets).\n"
            + "\n"
            + "Your ticket will be put into a pot, and when 450 kills have been reached in the wilderness, we will draw out 4 unique tickets and present them with the prizes you see above.\n"
            + "\n";
        HTTPost post = new HTTPost("http://www.enchanta.net/thred.php");

        post.put("title", "Winner");
        post.put("content", thread);

        try {

            // post.submitRequest();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method getEntries
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static ArrayList<Integer> getEntries(Player player) {
        ArrayList<Integer> returnEntries = new ArrayList<Integer>();

        for (int i = 0; i < ENTRIES.length; i++) {
            if (ENTRIES[i] == player.getId()) {
                returnEntries.add(i);
            }
        }

        return returnEntries;
    }

    /**
     * Method formatEntry
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entry
     *
     * @return
     */
    public static final String formatEntry(int entry) {
        if (entry < 99) {
            if (entry < 10) {
                return "00" + entry;
            }

            return "0" + entry;
        }

        return Integer.toString(entry);
    }
}
