package net.tazogaming.hydra.game.minigame.fight_caves;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/11/13
 * Time: 15:40
 */
public class FightCavesDeathHandler implements DeathHandler {

    /**
     * Method handleDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killed
     * @param killedBy
     */
    @Override
    public void handleDeath(Killable killed, Killable killedBy) {
        Player player = (Player) killed;

        if (killedBy instanceof NPC) {
            FightCavesGame g = killedBy.getNpc().getCaves();

           if(g != null) {
               g.unlink();

           }else{
               player.setCurDeathHandler(new DefaultDeathHandler());
               player.getCurDeathHandler().handleDeath(killed, killedBy);
           }
        } else {
            if (player.getCurrentInstance() instanceof FightCavesGame) {
                FightCavesGame g = (FightCavesGame) player.getCurrentInstance();

                g.unlink();
            }    // player.teleport(3222, 3222, 0);
        }        // To change body of implemented methods use File | Settings | File Templates.
    }
}
