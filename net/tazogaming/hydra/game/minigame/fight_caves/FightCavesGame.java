package net.tazogaming.hydra.game.minigame.fight_caves;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.pets.BossPet;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.SystemUpdate;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/11/13
 * Time: 14:54
 */
public class FightCavesGame extends Instance implements RecurringTickEvent {
    private static int[][] waves      = new int[64][];
    private static int[][] waves2     = new int[34][];
    private static Zone caves_zone =
        new Zone(2368, 5057, 2442, 5119, -1, true, true,
                 "fight_caves").setStatic(true).setBankingAllowed(false).setBobAllowed(false);
    private final static int[][] COORDINATES = {
        { 2403, 5094 }, { 2390, 5096 }, { 2392, 5077 }, { 2408, 5080 }, { 2413, 5108 }, { 2381, 5106 }, { 2379, 5072 },
        { 2420, 5082 }
    };
    private static int      waveOffset  = 0;
    private static int      waveOffset2 = 0;
    public static final int
        MODE_EASY                       = 0,
        MODE_HARD                       = 1;
    public static final int
        LEVEL_22                        = 2627,
        LEVEL_45                        = 2629,
        LEVEL_90                        = 2631,
        LEVEL_180                       = 2741,
        LEVEL_360                       = 2743,
        TZTOK_JAD                       = 2745;

    static {
        wave(LEVEL_22);
        wave(LEVEL_22, LEVEL_22);
        wave(LEVEL_45);
        wave(LEVEL_45, LEVEL_22);
        wave(LEVEL_45, LEVEL_22, LEVEL_22);
        wave(LEVEL_45, LEVEL_45);
        wave(LEVEL_90);
        wave(LEVEL_90, LEVEL_22);
        wave(LEVEL_90, LEVEL_22, LEVEL_22);
        wave(LEVEL_90, LEVEL_45);
        wave(LEVEL_90, LEVEL_45, LEVEL_22);
        wave(LEVEL_90, LEVEL_45, LEVEL_22, LEVEL_22);
        wave(LEVEL_90, LEVEL_45, LEVEL_45);
        wave(LEVEL_90, LEVEL_90);
        wave(LEVEL_180);
        wave(LEVEL_180, 22);
        wave(LEVEL_180, 22, 22);
        wave(LEVEL_180, 45);
        wave(LEVEL_180, 45, 22);
        wave(LEVEL_180, 45, 22, 22);
        wave(LEVEL_180, 45, 45);
        wave(180, 90);
        wave(180, 90, 22);
        wave(180, 90, 22, 22);
        wave(180, 90, 45);
        wave(180, 90, 45, 22);
        wave(180, 90, 45, 22, 22);
        wave(180, 90, 45, 45);
        wave(180, 90, 90);
        wave(180, 180);
        wave(360);
        wave(360, 22);
        wave(360, 22, 22);
        wave(360, 45, 22);
        wave(360, 45, 22, 22);
        wave(360, 45, 45);
        wave(360, 90);
        wave(360, 90, 22);
        wave(360, 90, 22, 22);
        wave(360, 90, 45);
        wave(360, 90, 45, 22);
        wave(360, 90, 45, 22, 22);
        wave(360, 90, 90);
        wave(360, 180);
        wave(360, 180, 22);
        wave(360, 180, 22, 22);
        wave(360, 180, 45);
        wave(360, 180, 45, 22);
        wave(360, 180, 45, 22, 22);
        wave(360, 180, 45, 45);
        wave(360, 180, 90);
        wave(360, 180, 90, 22);
        wave(360, 180, 90, 22, 22);
        wave(360, 180, 90, 45);
        wave(360, 180, 90, 45, 22);
        wave(360, 180, 90, 45, 22, 22);
        wave(360, 180, 90, 45, 45);
        wave(360, 180, 90, 90);
        wave(360, 180, 180);
        wave(360, 360);
        wave(TZTOK_JAD);
        wave2(22);
        wave2(22, 22);
        wave2(45);
        wave2(45, 22);
        wave2(45, 22, 22);
        wave2(45, 45);
        wave2(45, 45, 22);
        wave2(45, 45, 22, 22);
        wave2(45, 45, 45);
        wave2(90);
        wave2(90, 22);
        wave2(90, 22, 22);
        wave2(90, 22, 22, 22);
        wave2(90, 45);
        wave2(90, 45, 22);
        wave2(180);
        wave2(180, 22);
        wave2(180, 45);
        wave2(180, 90);
        wave2(180, 180);
        wave2(180, 180, 45);
        wave2(180, 180, 90);
        wave2(180, 180, 180);
        wave2(360);
        wave2(360, 22);
        wave2(360, 45);
        wave2(360, 45, 22);
        wave2(360, 45, 45);
        wave2(360, 90);
        wave2(360, 180);
        wave2(360, 180, 90);
        wave2(360, 360, 180);
    }

    private int             mode         = MODE_HARD;
    private boolean         isTerminated = false;
    private int             currentWave  = 0;
    private LinkedList<NPC> active_npcs  = new LinkedList<NPC>();
    private Player          playing;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param mode
     */
    public FightCavesGame(Player player, int mode) {

        if(SystemUpdate.isActive()){
            player.getActionSender().sendMessage("Unable to start fight caves whilst system update is active");
            return;
        }

        player.setCurrentInstance(this);
        player.teleport(2412, 5115, 0);
        player.getActionSender().sendMessage("Get ready wave 1 is starting!");
        this.playing = player;
        playing.setCurDeathHandler(new FightCavesDeathHandler());
        Core.submitTask(this);
        this.mode = mode;
    }

    /**
     * Method wave2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npcs
     */
    public static void wave2(int... npcs) {
        for (int i = 0; i < npcs.length; i++) {
            if (npcs[i] == 180) {
                npcs[i] = LEVEL_180;
            } else if (npcs[i] == 22) {
                npcs[i] = LEVEL_22;
            } else if (npcs[i] == 45) {
                npcs[i] = LEVEL_45;
            } else if (npcs[i] == 360) {
                npcs[i] = LEVEL_360;
            } else if (npcs[i] == 90) {
                npcs[i] = LEVEL_90;
            }
        }

        waves2[waveOffset2++] = npcs;
    }

    private Point randomPoint(int ind) {
        int[] loc = COORDINATES[ind];

        return Point.location(loc[0], loc[1], 0);
    }

    /**
     * Method npcKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void npcKilled(NPC npc) {
        if (active_npcs.contains(npc)) {
            npc.unlink();
            active_npcs.remove(npc);
        }

        if (npc.getId() == 2745) {
            playing.getAccount().setSetting("fight_caves", 2, true);
            unlink();
            playing.getInventory().addItem(6959, 1);

            if(GameMath.rand(100) < 10){
                BossPet pet = BossPets.getByBoss(2745);
                BossPets.sendNotification(pet, playing);
                playing.getInventory().addItem(pet.getItemId(), 1);
            }
            playing.getActionSender().sendMessage("You've unlocked the Tokaar-hal cape well done!");
            playing.getActionSender().sendMessage(
                    " You should log out for 60 seconds to ensure your game gets saved");
            playing.getActionSender().sendMessage(
                    "Although its unlikely that your savegame will be lost it's always better to be safe than sorry");
        }
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return playing;
    }

    private void runWave() {
        int   offset = 0;
        int[] wave_mode;

        if (mode == MODE_EASY) {
            wave_mode = waves2[currentWave];
        } else {
            wave_mode = waves[currentWave];
        }

        if ((wave_mode == null) || (wave_mode[0] == 0)) {
            if (!playing.getAccount().hasVar("fight_caves")) {
                playing.getAccount().setSetting("fight_caves", 1);
                playing.getActionSender().sendMessage(
                    "Congratulations, you've completed easy mode and unlocked the firecape");
                playing.getActionSender().sendMessage(
                    "Why not try the hard mode next? and go for the mighty TokHaar-Kal");
                playing.getInventory().addItem(6570, 1);
                unlink();

                return;
            }

            if (wave_mode == null) {
                return;
            }
        }

        for (int npc_id : wave_mode) {
            Point loc = randomPoint(offset);

            offset++;

            NpcAutoSpawn p = new NpcAutoSpawn();

            p.setSpawnX(loc.getX());
            p.setSpawnY(loc.getY());
            p.setRoamRange(110);

            NPC npc = new NPC(npc_id, loc, p);

            npc.setCaves(this);
            npc.setCurrentInstance(this);
            npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
                @Override
                protected void onDeath(NPC killed, Killable killedBy) {
                    npcKilled(((NPC) killed));

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
            World.getWorld().registerNPC(npc);
            active_npcs.add(npc);
        }

        currentWave++;
        playing.getActionSender().sendMessage("Wave: " + currentWave + " is starting!");
    }

    private static void wave(int... npcs) {
        for (int i = 0; i < npcs.length; i++) {
            if (npcs[i] == 180) {
                npcs[i] = LEVEL_180;
            } else if (npcs[i] == 22) {
                npcs[i] = LEVEL_22;
            } else if (npcs[i] == 45) {
                npcs[i] = LEVEL_45;
            } else if (npcs[i] == 360) {
                npcs[i] = LEVEL_360;
            } else if (npcs[i] == 90) {
                npcs[i] = LEVEL_90;
            } else if (npcs[i] == 702) {
                npcs[i] = TZTOK_JAD;
            }
        }

        waves[waveOffset++] = npcs;
    }

    private void restore_stats() {
        playing.restore_all_stats();
        playing.setPoisonTime(0);
        playing.getPrayerBook().setPoints(playing.getMaxStat(5));
        playing.getActionSender().sendStat(5);
        playing.getPrayerBook().reset_leeches();
        playing.getPrayerBook().resetAll();
        playing.setCurrentSpecial(10);
        SpecialAttacks.updateSpecial(playing);
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unlink() {
        if (isTerminated) {
            return;
        }

        for (NPC npc : active_npcs) {
            npc.unlink();
        }

        restore_stats();
        playing.setCurrentInstance(null);
        playing.teleport(2439, 5169, 0);
        playing.setCurDeathHandler(new DefaultDeathHandler());

        int tokkul = ((currentWave > 30)
                      ? currentWave * 70
                      : ((currentWave < 10)
                         ? 0
                         : 0) * 3);

        playing.getActionSender().sendMessage("Fight caves is over for you, you lasted a total of " + (currentWave - 1)
                + " waves and earned " + tokkul + " tokkul");
        playing.getInventory().addItemDrop(Item.forId(6529), tokkul);
        isTerminated = true;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {

        if(SystemUpdate.isActive()){
            if(this.currentWave >= 25){
                playing.getActionSender().sendMessage("You have been auto-awarded Tokar-cape due to system update");
                playing.getInventory().addItem(6959, 1);
                unlink();
                return;
            }
        }
        if (!playing.isLoggedIn() ||!playing.isInArea(caves_zone)) {
            unlink();

            return;
        }

        if (active_npcs.size() == 0) {
            runWave();
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return isTerminated;

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
