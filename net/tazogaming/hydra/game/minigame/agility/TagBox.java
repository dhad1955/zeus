package net.tazogaming.hydra.game.minigame.agility;

//~--- JDK imports ------------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/11/13
 * Time: 02:36
 */
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Danny
 * Date: 16/12/12
 * Time: 21:07
 * To change this template use File | Settings | File Templates.
 */
public class TagBox {
    public int             tagBoxX         = -1;
    public int             tagBoxY         = -2;
    public boolean         isActive        = false;
    public ArrayList<Long> acceptedPlayers = new ArrayList<Long>();
}
