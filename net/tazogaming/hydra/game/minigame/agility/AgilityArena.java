package net.tazogaming.hydra.game.minigame.agility;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.ui.HintIcon;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/11/13
 * Time: 02:37
 */
public class AgilityArena implements RecurringTickEvent {

    /** currOffset made: 14/08/23 **/
    private static int currOffset = 0;

    /** current_box made: 14/08/23 **/
    private static int current_box = 0;

    /** last_change made: 14/08/23 **/
    private static int last_change = Core.currentTime;

    /** players made: 14/08/23 **/
    private static ArrayList<Player> players = new ArrayList<Player>();

    /** saw_blades made: 14/08/23 **/
    private static ArrayList<GameObject> saw_blades = new ArrayList<GameObject>();

    /** bladeTimer made: 14/08/23 **/
    private static int bladeTimer = 0;

    /** tagBoxes made: 14/08/23 **/
    private static TagBox[] tagBoxes = new TagBox[24];

    static {
        addBooth(2794, 9590);
        addBooth(2783, 9590);
        addBooth(2772, 9590);
        addBooth(2761, 9590);
        addBooth(2805, 9579);
        addBooth(2794, 9579);
        addBooth(2783, 9579);
        addBooth(2772, 9579);
        addBooth(2761, 9579);
        addBooth(2805, 9568);
        addBooth(2794, 9568);
        addBooth(2783, 9568);
        addBooth(2772, 9568);
        addBooth(2761, 9568);
        addBooth(2805, 9557);
        addBooth(2794, 9557);
        addBooth(2783, 9557);
        addBooth(2772, 9557);
        addBooth(2761, 9557);
        addBooth(2805, 9546);
        addBooth(2794, 9546);
        addBooth(2783, 9546);
        addBooth(2772, 9546);
        addBooth(2761, 9546);
        add_saw_blade(2783, 9551, 0);
        add_saw_blade(2788, 9579, 1);
        add_saw_blade(2761, 9584, 0);
        Core.submitTask(new AgilityArena());
    }

    private static void add_saw_blade(int x, int y, int rotation) {
        GameObject blade = new GameObject();

        blade.setCurrentRotation(rotation);
        blade.setIdentifier(3567);
        blade.setObjectType(10);
        blade.setLocation(Point.location(x, y, 3));
        World.getWorld().getObjectManager().registerObjectUnclipped(blade);
        saw_blades.add(blade);
    }

    /**
     * Method addPlayer
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void addPlayer(Player player) {
        if (!players.contains(player)) {
            players.add(player);
        }

        player.getGameFrame().setCurrentHintIcon(new HintIcon(getTagPoint()));
    }

    /**
     * Method removePlayer
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void removePlayer(Player player) {
        players.remove(player);
    }

    /**
     * Method onObjectClick
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public static boolean onObjectClick(Player player, int id, int x, int y, int h) {
        if (h != 3) {
            return false;
        }

        for (int i = 0; i < tagBoxes.length; i++) {
            if ((tagBoxes[i].tagBoxX == x) && (tagBoxes[i].tagBoxY == y)) {
                if (current_box == i) {
                    if (tagBoxes[i].acceptedPlayers.contains(player.getUsernameHash())) {
                        player.getActionSender().sendMessage("You've already tagged this box.");
                    } else {
                        player.addAchievementProgress2(Achievement.TAG_BOX_PRO, 1);

                        if (player.getAccount().getSmallSetting(Account.TAG_BOX_ACTIVE) < 3) {
                            player.getAccount().putSmall(Account.TAG_BOX_ACTIVE,
                                                          player.getAccount().getSmallSetting(Account.TAG_BOX_ACTIVE)
                                                          + 1);
                        }

                        tagBoxes[i].acceptedPlayers.add(player.getUsernameHash());
                        player.getGameFrame().setCurrentHintIcon(null);
                        player.getActionSender().sendVar(309, 15);

                        if (player.getAccount().getSmallSetting(Account.TAG_BOX_ACTIVE) > 1) {
                            player.getActionSender().sendMessage("You get an agility ticket.");
                            player.getInventory().addItem(2996, 1);
                            if(!player.getAccount().hasVar("agil_tcks")){
                                player.getAccount().setSetting("agil_tcks", 1);
                            }                                                else{
                                player.getAccount().increaseVar("agil_tcks", 1);
                            }
                        }
                    }
                }

                return true;
            }
        }

        return false;
    }

    /**
     * Method getTagPoint
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static Point getTagPoint() {
        return Point.location(tagBoxes[current_box].tagBoxX, tagBoxes[current_box].tagBoxY, 3);
    }

    /**
     * Method tick
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {
        tick_arena();
    }

    /**
     * Method tick_arena
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void tick_arena() {
        for (int i = 0; i < players.size(); i++) {
            if (!players.get(i).isLoggedIn()) {
                players.remove(i);
            }
        }

        if (bladeTimer > 0) {
            for (GameObject obj : saw_blades) {
                Tile[] tiles = { World.getWorld().getTile(obj.getLocation()), null };

                if (obj.getRotation() == 0) {
                    tiles[1] = World.getWorld().getTile(obj.getX(), obj.getY() + 1, 3);
                }

                if (obj.getRotation() == 1) {
                    tiles[1] = World.getWorld().getTile(obj.getX() + 1, obj.getY(), 3);
                }

                for (Tile t : tiles) {
                    if (t.hasPlayers()) {
                        for (Player p : t.getPlayers()) {
                            p.hit(p, 4, Damage.TYPE_OTHER, 0);
                        }
                    }
                }

                bladeTimer--;
            }
        }

        if (Core.currentTime % 10 == 0) {
            for (GameObject obj : saw_blades) {
                obj.next();
            }

            bladeTimer = 5;
        }

        if (Core.timeSince(last_change) > 120) {
            List<Long> tmp = new ArrayList<Long>();

            tmp.addAll(tagBoxes[current_box].acceptedPlayers);
            tagBoxes[current_box].acceptedPlayers.clear();
            current_box = GameMath.rand3(tagBoxes.length);
            last_change = Core.currentTime;

            for (Player player : players) {
                if (!tmp.contains(player.getUsernameHash())
                        && (player.getAccount().getSmallSetting(Account.TAG_BOX_ACTIVE) > 0)) {
                    player.getAccount().putSmall(Account.TAG_BOX_ACTIVE, 0);
                }

                if (player.getAccount().getSmallSetting(Account.TAG_BOX_ACTIVE) > 0) {
                    player.getActionSender().sendVar(309, 15);
                } else {
                    player.getActionSender().sendVar(309, 0);
                }

                player.getGameFrame().setCurrentHintIcon(new HintIcon(getTagPoint()));
            }
        }
    }

    /**
     * Method terminate
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method addBooth
     * Created on 14/08/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     */
    public static void addBooth(int x, int y) {
        tagBoxes[currOffset]           = new TagBox();
        tagBoxes[currOffset].tagBoxX   = x;
        tagBoxes[currOffset++].tagBoxY = y;
    }
}
