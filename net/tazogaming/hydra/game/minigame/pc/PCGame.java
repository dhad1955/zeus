package net.tazogaming.hydra.game.minigame.pc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.npc.ai.hardscript.HardWiredScript;
import net.tazogaming.hydra.entity3d.npc.ai.movement.*;
import net.tazogaming.hydra.entity3d.npc.ai.movement.FollowPlayerMovement;
import net.tazogaming.hydra.entity3d.npc.ai.movement.RoamingMovement;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.util.ilobby.*;
import net.tazogaming.hydra.game.EventManager;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.skill.dungeoneering.DynamicCombatNPC;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/12/13
 * Time: 21:43
 */
public class PCGame implements RecurringTickEvent, LobbyListener {
    private static final int
        GAME_MODE_EASY                   = 0,
        GAME_MODE_SEMI                   = 1,
        GAME_MODE_HARD                   = 2;
    public static Zone PEST_CONTROL_GAME = new Zone(2614, 2549, 2700, 2631, -1, true, false,
                                               "pc_game").setBankingAllowed(false).setBobAllowed(false).setStatic(true);
    public static final int COUNTDOWN = 150;    // 2 MINUTES
    private static PCGate[] GATES     = { new PCGate(Door.getDoor(2643, 2593, 0), Door.getDoor(2643, 2592, 0)),
            new PCGate(Door.getDoor(2657, 2585, 0), Door.getDoor(2656, 2585, 0)),
            new PCGate(Door.getDoor(2670, 2592, 0), Door.getDoor(2670, 2593, 0)) };
    private static final DamageFilter pc_damage_snagger = new DamageFilter() {
        @Override
        public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
            if (hitBy instanceof Player) {
                hitBy.getPlayer().getAccount().increaseVar("pc_damage", (int)damage);
            }

            return damage;    // To change body of implemented methods use File | Settings | File Templates.
        }
    };
    public static final int
        TYPE_BRAWLER                             = 0,
        TYPE_SHIFTER                             = 1,
        TYPE_RAVAGER                             = 2,
        TYPE_TORCHER                             = 3,
        TYPE_SPLATTER                            = 4,
        TYPE_DEFLIER                             = 5,
        TYPE_SPINNER                             = 6;
    private static final int SPAWN_SPEED         = 60;    // around 30 secs
    private static final int SPAWN_SCANRANGE_MIN = 2;
    private static final int SPAWN_SCANRANGE_MAX = 6;
    public static PCGame     static_game;
    public static Lobby      lobby;

    static {
        lobby = new Lobby(new Zone(2660, 2638, 2663, 2643, -1, false, true,
                                   "pc_boat").setBankingAllowed(false).setStatic(true));
        lobby.setMaxPlayers(COUNTDOWN);
        lobby.setCenterPoint(Point.location(2661, 2639, 0));
        lobby.setCountdown(COUNTDOWN);
        lobby.setMode(LobbyMode.TIMER_MODE);
        static_game = new PCGame();
        lobby.setListener(static_game);
        lobby.setUI(new LobbyUIUpdater() {
            private int lastSend = Core.currentTime;
            public String getMinPlayers(int amt) {
                int siz = 3 - amt;

                if (siz <= 0) {
                    return amt + " players ready.";
                }

                return "Need atleast " + siz + " to start.";
            }
            @Override
            public void updateUI(Player pla, Lobby lob) {
                if (PCGame.static_game.gameRunning) {
                    pla.getGameFrame().sendString("Waiting for game to finish", 407, 13);
                } else {
                    pla.getGameFrame().sendString("Next departure: " + (1 + (lob.getSecondsTillStart() / 60)) + " min",
                                                  407, 13);
                }

                pla.getGameFrame().sendString("Players needed: <col=ffffff>" + getMinPlayers(lob.getPlayers().size()),
                                              407, 15);
                pla.getGameFrame().sendString("Mode: " + getModeName(getDifficulty(lob.getPlayers())), 407, 3);

                if (Core.timeSince(lastSend) > Core.getTicksForMinutes(4)) {
                    lastSend = Core.currentTime;

                    if (!PCGame.static_game.gameRunning) {
                        if ((lob.getPlayers().size() > 0) && (lob.getSecondsTillStart() > 0)) {
                            for (Player player : World.getWorld().getPlayers()) {

                            }
                        }
                    }
                }
            }
            @Override
            public void playerEntered(Player pla, Lobby l) {
                pla.getGameFrame().openOverlay(407);
                pla.getGameFrame().sendString("Points: " + pla.getPoints(Points.PESTCONTROL_POINTS), 407, 16);
                pla.getActionSender().sendPlainObject(14314, 2660, 2639, 10, 2);

                // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void playerLeft(Player pla, Lobby l) {
                if (!pla.isInArea(PEST_CONTROL_GAME)) {
                    pla.getGameFrame().closeOverlay();
                }

                // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void playerConnectionLost(Player pla, Lobby l) {

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    private DynamicCombatNPC[] npcs         = new DynamicCombatNPC[10];
    private ArrayList<NPC>     stragglers   = new ArrayList<NPC>();
    private boolean            gameRunning  = false;
    private NPC[]              portals      = new NPC[4];
    private int[]              portal_times = new int[4];
    private int[]              portal_alts  = new int[4];
    private ArrayList<Player>  players      = new ArrayList<Player>();
    private NPC                void_knight;
    private int                difficulty;

    /**
     * Constructs ...
     *
     */
    public PCGame() {
        Core.submitTask(this);
    }

    private static String getModeName(int mode) {
        switch (mode) {
        case GAME_MODE_HARD :
            return "Hard";

        case GAME_MODE_EASY :
            return "Easy";

        case GAME_MODE_SEMI :
            return "Medium";
        }

        return "UNKNOWN";
    }

    /**
     * Method wreckGates
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void wreckGates() {
        for (PCGate gate : GATES) {
            gate.takeDamage(500);
        }
    }

    /**
     * Method objectClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     * @param x
     * @param y
     *
     * @return
     */
    public static boolean objectClick(Player player, int id, int x, int y) {
        if (id != 14239) {
            return false;
        }

        if (!player.getInventory().hasItem(1511, 4)) {
            player.getActionSender().sendMessage("You need atleast 4 logs to repair this gate.");

            return true;
        }

        for (PCGate gate : GATES) {
            if (gate.exists(x, y)) {
                player.getInventory().deleteItem(1511, 4);
                gate.reset();
                player.getActionSender().sendMessage("You repair the gates.");
                player.getAnimation().animate(898);

                return true;
            }
        }

        return false;
    }

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void set() {
        npcs[TYPE_TORCHER]  = new DynamicCombatNPC(3752, 3755, 3759);
        npcs[TYPE_DEFLIER]  = new DynamicCombatNPC(3762, 3765, 3768);
        npcs[TYPE_BRAWLER]  = new DynamicCombatNPC(3772, 3773, 3775);
        npcs[TYPE_SPLATTER] = new DynamicCombatNPC(3728, 3730, 3731);
        npcs[TYPE_SHIFTER]  = new DynamicCombatNPC(3732, 3734, 3736);
        npcs[TYPE_RAVAGER]  = new DynamicCombatNPC(3743, 3745, 3746);
        npcs[TYPE_SPINNER]  = new DynamicCombatNPC(3748, 3750, 3751);
    }

    /**
     * Method getVoid_knight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC getVoid_knight() {
        return void_knight;
    }

    private static NpcDef getShifterConfig(int tier, int npcId) {
        NpcDef def = new NpcDef(npcId);

        tier++;
        def.setStartingHealth(40 * tier);
        def.setAttackable(true);
        def.setBlockAnim(3902);
        def.setDeathAnim(3903);
        def.setDeathTime(4);
        def.setAttackAnim(3900);
        def.setCooldown(4);
        def.setMaxHit(5 * tier);

        int[] bonuses = {
            50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50
        };

        for (int i = 0; i < bonuses.length; i++) {
            bonuses[i] += (tier * 10);
        }

        int[] combat_stats = new int[] {
            40, 30, 40, 40, 40, 40, 40
        };

        for (int i = 0; i < combat_stats.length; i++) {
            combat_stats[i] += (tier * 7);
        }

        def.setCombatStats(combat_stats);
        def.setBonuses(bonuses);
        def.setController("void_shifter");

        return def;
    }

    private static NpcDef getBrawlerConfig(int tier, int npcId) {
        NpcDef def = new NpcDef(npcId);

        tier++;
        def.setStartingHealth(45 * tier);
        def.setAttackable(true);
        def.setBlockAnim(3895);
        def.setDeathAnim(3894);
        def.setDeathTime(3);
        def.setAttackAnim(3897);
        def.setSize(2);
        def.setMaxHit(10 * tier);
        def.setCooldown(10);

        int[] bonuses = {
            180, 50, 50, 50, 50, 80, 80, 80, 80, 80, 80, 80, 80
        };

        for (int i = 0; i < bonuses.length; i++) {
            bonuses[i] += (tier * 20);
        }

        int[] combat_stats = new int[] {
            126, 70, 70, 70, 70, 70, 40
        };

        for (int i = 0; i < combat_stats.length; i++) {
            combat_stats[i] += (tier * 7);
        }

        def.setCombatStats(combat_stats);
        def.setBonuses(bonuses);
        def.setController("brawler");

        return def;
    }

    private static NpcDef getSpinnerConfig(int tier, int npcId) {
        NpcDef def = new NpcDef(npcId);

        tier++;
        def.setStartingHealth(40 * tier);
        def.setAttackable(true);
        def.setBlockAnim(3882);
        def.setDeathAnim(3881);
        def.setDeathTime(3);
        def.setCooldown(4);
        def.setAttackAnim(3900);
        def.setMaxHit(5 * tier);
        def.setMaxHitModifier(3 * tier);

        int[] bonuses = {
            50, 50, 50, 50, 50, 50, 0, 0, 0, 0, 0, 0, 0
        };

        for (int i = 0; i < bonuses.length; i++) {
            bonuses[i] += (tier * 11);
        }

        int[] combat_stats = new int[] {
            40, 11, 40, 40, 40, 40, 40
        };

        for (int i = 0; i < combat_stats.length; i++) {
            combat_stats[i] += (tier * 7);
        }

        def.setCombatStats(combat_stats);
        def.setBonuses(bonuses);
        def.setController("spinner");

        return def;
    }

    private static NpcDef getTorcherConfig(int tier, int npcId) {
        NpcDef def = new NpcDef(npcId);

        tier++;
        def.setStartingHealth(40 * tier);
        def.setAttackable(true);
        def.setBlockAnim(3882);
        def.setDeathAnim(3881);
        def.setDeathTime(3);
        def.setAttackAnim(3900);
        def.setMaxHit(5 * tier);
        def.setMaxHitModifier(3 * tier);
        def.setCooldown(4);

        int[] bonuses = {
            50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50
        };

        for (int i = 0; i < bonuses.length; i++) {
            bonuses[i] += (tier * 20);
        }

        int[] combat_stats = new int[] {
            40, 40, 40, 40, 40, 40, 40
        };

        for (int i = 0; i < combat_stats.length; i++) {
            combat_stats[i] += (tier * 7);
        }

        def.setCombatStats(combat_stats);
        def.setBonuses(bonuses);
        def.setController("void_torcher");

        return def;
    }

    private static NpcDef getSplatterConfig(int tier, int npcId) {
        NpcDef def = new NpcDef(npcId);

        tier++;
        def.setStartingHealth(12 * tier);
        def.setAttackable(true);
        def.setBlockAnim(3882);
        def.setDeathAnim(3888);
        def.setDeathTime(3);
        def.setAttackAnim(3891);
        def.setMaxHit(2 * tier);
        def.setCooldown(4);
        def.setMaxHitModifier(3 * tier);

        int[] bonuses = {
            50, 50, 50, 50, 50, 50, 50, 50, 20, 20, 20, 20, 20
        };

        for (int i = 0; i < bonuses.length; i++) {
            bonuses[i] += (tier * 10);
        }

        int[] combat_stats = new int[] {
            90, 40, 40, 40, 40, 40, 40, 40
        };

        for (int i = 0; i < combat_stats.length; i++) {
            combat_stats[i] += (tier * 7);
        }

        def.setCombatStats(combat_stats);
        def.setBonuses(bonuses);
        def.setController("splatter");

        return def;
    }

    private static NpcDef getDefConfig(int tier, int npcId) {
        NpcDef def = new NpcDef(npcId);

        tier++;
        def.setStartingHealth(40 * tier);
        def.setAttackable(true);
        def.setBlockAnim(3921);
        def.setDeathAnim(3922);
        def.setDeathTime(3);
        def.setAttackAnim(3900);
        def.setMaxHit(5 * tier);
        def.setMaxHitModifier(3 * tier);

        int[] bonuses = {
            50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50, 50
        };

        for (int i = 0; i < bonuses.length; i++) {
            bonuses[i] += (tier * 20);
        }

        int[] combat_stats = new int[] {
            80, 40, 40, 40, 40, 40, 40
        };

        for (int i = 0; i < combat_stats.length; i++) {
            combat_stats[i] += (tier * 7);
        }

        def.setCombatStats(combat_stats);
        def.setBonuses(bonuses);
        def.setController("void_deflier");

        return def;
    }

    private NPC getNearestPortal(Point p) {
        NPC closest = null;
        int dist    = 256;

        for (NPC n : portals) {
            if (n != null) {
                if (Point.getDistance(p, n.getLocation()) < dist) {
                    dist    = Point.getDistance(p, n.getLocation());
                    closest = n;
                }
            }
        }

        return closest;
    }

    /**
     * Method newMonster
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param tier
     * @param x
     * @param y
     */
    public void newMonster(int type, int tier, int x, int y) {
        if (npcs[type] == null) {
            set();
        }

        NPC npc = null;

        switch (type) {
        case TYPE_SHIFTER :
            npc = new NPC(npcs[type].get(tier), Point.location(x, y, 0), 100,
                          getShifterConfig(tier, npcs[type].get(tier)));

            break;

        case TYPE_BRAWLER :
            npc = new NPC(npcs[type].get(tier), Point.location(x, y, 0), 100,
                          getBrawlerConfig(tier, npcs[type].get(tier)));

            break;

        case TYPE_TORCHER :
            npc = new NPC(npcs[type].get(tier), Point.location(x, y, 0), 100,
                          getTorcherConfig(tier, npcs[type].get(tier)));

            break;

        case TYPE_DEFLIER :
            npc = new NPC(npcs[type].get(tier), Point.location(x, y, 0), 100, getDefConfig(tier, npcs[type].get(tier)));

            break;

        case TYPE_SPLATTER :
            npc = new NPC(npcs[type].get(tier), Point.location(x, y, 0), 100,
                          getSplatterConfig(tier, npcs[type].get(tier)));

            break;

        case TYPE_SPINNER :
            npc = new NPC(npcs[type].get(tier), Point.location(x, y, 0), 100,
                          getSpinnerConfig(tier, npcs[type].get(tier)));

            final NPC portal = getNearestPortal(npc.getLocation());

            npc.setCombatScript(new HardWiredScript(npc) {
                private NPC myPortal = portal;
                @Override
                public boolean update_combat() {
                    return false;    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public void on_spawn() {

                    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public boolean on_attacked_by(Killable k) {
                    if (k instanceof Player) {
                        getMob().setMovement(new FollowPlayerMovement(getMob(), k.getPlayer(), true));
                    }

                    return true;    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public boolean update_movement() {
                    if (getMob().getMovement() instanceof RoamingMovement) {
                        getMob().setMovement(new SummonAttackNPCMovement(getMob(), portal));
                    }

                    return false;    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public double filter_damage(Killable hit_by, Damage d) {
                    return d.getDamage();    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public boolean on_area_clear() {
                    return false;    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public void tick() {
                    if (myPortal.isDead() ||!myPortal.isVisible()) {
                        return;
                    }

                    if ((Point.getDistance(myPortal.getLocation(), getMob().getLocation()) < 2)
                            && (getMob().getMovement() instanceof SummonAttackNPCMovement)) {
                        if (Core.currentTime % 10 == 0) {
                            myPortal.addHealth(10);
                        }
                    }

                    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public boolean on_npc_dead(Player killedBy) {
                    return false;    // To change body of implemented methods use File | Settings | File Templates.
                }
            });

            break;

        default :
            npc = new NPC(type, Point.location(x, y, 0), 100, null);

            break;
        }

        if (npc == null) {
            return;
        }

        stragglers.add(npc);
        npc.setPCGame(this);
        World.getWorld().registerNPC(npc);
        npc.addDamageFilter("pc_dmg", pc_damage_snagger);
        npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {

                stragglers.remove(killed);

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    /**
     * Method staticV
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void staticV() {}

    /**
     * Method isInLobby
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isInLobby(Player player) {
        return lobby.getPlayers().contains(player);
    }

    private static int getDifficulty(List<Player> players) {
        if (players.size() < 6) {
            return GAME_MODE_EASY;
        }

        if (players.size() < 12) {
            return GAME_MODE_SEMI;
        }

        return GAME_MODE_HARD;
    }

    /**
     * Method getDifficulty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDifficulty() {
        if (players.size() < 6) {
            return GAME_MODE_EASY;
        }

        if (players.size() < 12) {
            return GAME_MODE_SEMI;
        }

        return GAME_MODE_HARD;
    }

    /**
     * Method getMaxNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaxNpcs() {
        switch (difficulty) {
        case GAME_MODE_EASY :
            return 20;

        case GAME_MODE_SEMI :
            return 30;

        case GAME_MODE_HARD :
            return 40;
        }

        return 1;
    }


    private static Point getSpawnPoint(int x, int y) {
        for (int offsetX = x + SPAWN_SCANRANGE_MIN; offsetX < x + SPAWN_SCANRANGE_MIN + SPAWN_SCANRANGE_MAX;
                offsetX++) {
            for (int offsetY = y + SPAWN_SCANRANGE_MIN; offsetY < y + SPAWN_SCANRANGE_MAX; offsetY++) {
                Point cur = Point.location(offsetX, offsetY, 0);

                if (World.getWorld().getTile(cur).blocked(null)) {
                    continue;
                }

                return cur;
            }
        }

        return null;
    }

    private int getRandomNpc() {
        int[]              possibleNpcs = {
            TYPE_BRAWLER, TYPE_TORCHER, TYPE_SHIFTER, TYPE_SPINNER, TYPE_SPLATTER, TYPE_DEFLIER
        };
        ArrayList<Integer> common       = new ArrayList<Integer>();
        ArrayList<Integer> uncommon     = new ArrayList<Integer>();

        common.add(TYPE_TORCHER);
        common.add(TYPE_BRAWLER);
        common.add(TYPE_SPLATTER);
        common.add(TYPE_SPLATTER);
        uncommon.add(TYPE_SHIFTER);
        uncommon.add(TYPE_SPINNER);

        int ran = Combat.GLOBAL_RANDOM.nextInt(1000);

        if (ran < 800) {
            return common.get(GameMath.rand3(common.size()));
        }

        if (ran < 1000) {
            return uncommon.get(GameMath.rand3(uncommon.size()));
        }

        return TYPE_TORCHER;
    }

    /**
     * Method spawnNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void spawnNpcs() {
        Point p;

        for (NPC portal : portals) {
            if ((portal != null) &&!portal.isDead()) {
                if (stragglers.size() == getMaxNpcs()) {
                    return;
                }

                int max = 1;

                if (difficulty == GAME_MODE_SEMI) {
                    max = 2;
                } else if (difficulty == GAME_MODE_HARD) {
                    max = 3;
                }

                for (int i = 0; i < max; i++) {
                    p = getSpawnPoint(portal.getX(), portal.getY());

                    if (p == null) {
                        break;
                    }

                    newMonster(getRandomNpc(), difficulty, p.getX(), p.getY());
                }
            }
        }
    }

    /**
     * Method startGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void startGame() {
        difficulty = getDifficulty();
        addPortal(0, 6146, 2628, 2591, 120);
        addPortal(1, 6147, 2680, 2588, 90);
        addPortal(2, 6148, 2669, 2570, 60);
        addPortal(3, 6149, 2645, 2569, 30);
        void_knight = new NPC(3784, Point.location(2656, 2592, 0));
        void_knight.setBlock_interactions(true);
        void_knight.setHealth(200);
        void_knight.setMaxHealth(200);
        void_knight.getDefinition().setDeathTime(4);
        World.getWorld().registerNPC(void_knight);
        void_knight.setDeathHandler(new NpcDeathHandler(DeathPolicy.FAST_ACTION, DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                endGame("The void knight was killed", false);
            }
        });
        gameRunning = true;
        spawnNpcs();

        for (PCGate g : GATES) {
            g.reset();
        }
    }

    /**
     * Method endGame
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param message
     * @param win
     */
    public void endGame(String message, boolean win) {
        if (!gameRunning) {
            return;
        }

        lobby.setCountdown(COUNTDOWN);
        messageCrew(message);

        for (Player player : players) {
            unlink(player);

            if (win) {
                int dmg = player.getAccount().getInt32("pc_damage");

                if (dmg > 120) {
                    int pts = 1;

                    player.getActionSender().sendMessage("Congratulations, you have dealt enough damage.");

                    int reward = 250000;

                    switch (difficulty) {
                    case 1 :
                        pts    = 2;
                        reward *= 1.30;

                        break;

                    case 2 :
                        pts    = 4;
                        reward *= 1.50;

                        break;
                    }

                    if (EventManager.currentDay() == Calendar.WEDNESDAY) {
                        pts *= 2;
                    }

                    player.addPoints(Points.PESTCONTROL_POINTS, pts);
                    player.getActionSender().sendMessage("You have earned " + pts + " commendations");
                    player.getActionSender().sendMessage("You have been awarded " + reward + " gp");
                    player.getInventory().addItemDrop(Item.forId(995), reward);
                }
            }
        }

        players.clear();

        for (NPC npc : stragglers) {
            npc.unlink();
        }

        stragglers.clear();
        gameRunning = false;
        void_knight.unlink();
        void_knight = null;

        for (NPC p : portals) {
            p.unlink();
        }
    }

    private void addPortal(int index, int portalID, int absX, int absY, int secs) {
        NPC npc = new NPC(portalID, Point.location(absX, absY, 0), 0, null);

        npc.setMovement(new Movement(npc) {
            @Override
            public void tick(int currentTime) {

                // To change body of implemented methods use File | Settings | File Templates.
            }

            @Override
            public void mobMoved(Point newLocation, int direction) {
            }

            @Override
            public void exceptionCaught(Exception ee) {
            }
        });
        npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.FAST_ACTION, DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        portals[index] = npc;
        npc.setMaxHealth(1000);
        npc.addHealth(1000);
        World.getWorld().registerNPC(npc);
        portal_times[index] = Core.getTicksForSeconds(secs);
        npc.addDamageFilter("dmg", pc_damage_snagger);
    }

    private void messageCrew(String msg) {
        for (Player player : players) {
            player.getActionSender().sendMessage(msg);
        }
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void unlink(Player player) {
        player.cancelDeaths();
        player.teleport(2657, 2639, 0);
        player.setCurDeathHandler(new DefaultDeathHandler());
        player.getGameFrame().closeOverlay();
        player.restore_all_stats();
        player.setCurrentHealth(player.getMaxHealth());
        player.getPrayerBook().resetAll();
        player.getPrayerBook().addPoints(99);
        player.getActionSender().sendStat(5);
        player.getActionSender().sendStat(3);
        player.setPoisonTime(0);
        player.setCurrentSpecial(10);
        SpecialAttacks.updateSpecial(player);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        lobby.update();

        if (!gameRunning) {
            return;
        }

        if (players.size() == 0) {
            endGame("No players left", false);

            return;
        }

        for (int i = 0; i < portal_times.length; i++) {
            if (portal_times[i] > 0) {
                portal_times[i]--;

                if (portal_times[i] == 0) {
                    messageCrew("The void knight has destroyed a shield on a portal");
                    portals[i].setTransform(portals[i].getId() - 4);

                    NpcDef newDEF = new NpcDef(portals[i].getDefinition());

                    newDEF.setAttackable(true);

                    int[] bonuses = new int[20];

                    bonuses[Item.BONUS_DEFENCE_CRUSH] = 250;
                    bonuses[Item.BONUS_DEFENCE_MAGIC] = 250;
                    bonuses[Item.BONUS_DEFENCE_RANGE] = 250;
                    bonuses[Item.BONUS_DEFENCE_STAB]  = 250;
                    bonuses[Item.BONUS_DEFENCE_SLASH] = 250;

                    int[] combatStats = {
                        0, 75, 0, 0, 0, 0
                    };

                    // newDEF.setBonuses(bonuses);
                    // newDEF.setCombatStats(combatStats);
                    portals[i].setDefinition(newDEF);
                    portals[i].setMaxHealth(1000 + (difficulty * 200));
                    portals[i].addHealth(1000 + (difficulty * 200));
                }
            }
        }

        int c = 0;

        for (NPC p : portals) {
            if ((p.getCurrentHealth() == 0) || p.isRemoved()) {
                c++;
            }
        }

        if (c == 4) {
            endGame("All portals were destroyed", true);

            return;
        }

        if ((Core.currentTime % SPAWN_SPEED == 0) && gameRunning) {
            spawnNpcs();
        }

        for (Iterator<Player> player_list = players.iterator(); player_list.hasNext(); ) {
            Player player = player_list.next();

            if (!player.isLoggedIn() ||!player.isInArea(PEST_CONTROL_GAME)) {
                unlink(player);
                player_list.remove();

                continue;
            }

            if (Core.currentTime % 3 == 0) {
                player.getGameFrame().sendString(formatHealth((int)void_knight.getCurrentHealth()), 408, 1);
                player.getGameFrame().sendString(formatHealth((int)portals[0].getCurrentHealth()), 408, 13);
                player.getGameFrame().sendString(formatHealth((int)portals[1].getCurrentHealth()), 408, 14);
                player.getGameFrame().sendString(formatHealth((int)portals[2].getCurrentHealth()), 408, 15);
                player.getGameFrame().sendString(formatHealth((int)portals[3].getCurrentHealth()), 408, 16);
                player.getGameFrame().sendString(Integer.toString(player.getAccount().getInt32("pc_damage")), 408, 11);
                player.getGameFrame().sendString("No Time Limit", 408, 0);
            }
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method isPlaying
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean isPlaying(Player pla) {
        return players.contains(pla);
    }

    /**
     * Method formatHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param health
     *
     * @return
     */
    public static String formatHealth(int health) {
        if (health >= 170) {
            return Text.GREEN("" + health);
        }

        if (health >= 140) {
            return Text.LIGHT_ORANGE("" + health);
        }

        if (health >= 70) {
            return Text.DARK_ORANGE("" + health);
        }

        if (health == 0) {
            return Text.RED("DEAD");
        }

        return Text.RED("" + health);
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onLobbyFinished
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lobby
     *
     * @return
     */
    @Override
    public boolean onLobbyFinished(Lobby lobby) {
        if (lobby.getPlayers().size() < 3) {
            lobby.setCountdown(COUNTDOWN);

            for (Player p : lobby.getPlayers()) {
                p.getActionSender().sendMessage("Not enough players to start.");
            }

//          return false;
        }

        if (gameRunning) {
            lobby.setCountdown(COUNTDOWN);

            return false;
        }

        for (Player player : lobby.getPlayers()) {
            player.teleport(2657, 2611, 0);
            player.getGameFrame().openOverlay(408);
            player.getAccount().setSetting("pc_damage", 0);
            players.add(player);
            player.setCurrentInstance(null);
            player.setCurDeathHandler(new DeathHandler() {
                @Override
                public void handleDeath(Killable killed, Killable killedBy) {

                    // To change body of implemented methods use File | Settings | File Templates.
                    killed.getPlayer().teleport(2657, 2611, 0);
                }
            });
        }

        startGame();

        return true;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method onLobbyEmpty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lobby
     *
     * @return
     */
    @Override
    public boolean onLobbyEmpty(Lobby lobby) {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getStartupResponse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStartupResponse() {
        if (gameRunning) {
            return GameStartPolicy.WAIT_UNTIL_OKAY;
        }

        if (!gameRunning) {
            return GameStartPolicy.OKAY;
        }

        return 0;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
