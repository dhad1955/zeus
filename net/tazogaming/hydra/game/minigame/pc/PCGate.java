package net.tazogaming.hydra.game.minigame.pc;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.object.Door;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 05/01/14
 * Time: 06:00
 */
public class PCGate {
    private int    health = 100;
    private Door[] doors;

    /**
     * Constructs ...
     *
     *
     * @param doors
     */
    public PCGate(Door... doors) {

        // this.object.setIdentifier();
        this.doors = doors;
    }

    private void setIds(int... id) {
        for (Door d : doors) {
            if (d != null) {
                d.setNewID(id[0], id[1]);
            }
        }
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        if (health > 0) {
            setIds(14236, 14235);
        } else if (health == 0) {
            setIds(14239, 14238);
        }
    }

    /**
     * Method takeDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dmg
     */
    public void takeDamage(int dmg) {
        health -= dmg;

        if (health <= 0) {
            health = 0;

            for (Door d : doors) {
                d.open();
            }

            update();
        }

        update();
    }

    /**
     * Method exists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public boolean exists(int x, int y) {
        for (Door d : doors) {
            if ((d.getX() == x) && (d.getY() == y)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        health = 100;
        update();

        for (Door d : doors) {
            d.close();
            d.setNewID(-1, -1);
        }
    }
}
