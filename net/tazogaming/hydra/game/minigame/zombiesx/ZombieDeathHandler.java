package net.tazogaming.hydra.game.minigame.zombiesx;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 18:27
 */
public class ZombieDeathHandler implements DeathHandler {

    /**
     * Method handleDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killed
     * @param killedBy
     */
    @Override
    public void handleDeath(Killable killed, Killable killedBy) {
        Player player = (Player) killed;

        player.getActionSender().sendMessage("Oh dear, zombies is now over for you. you scored "
                + player.getAccount().getZombieScore() + " and killed a total of "
                + player.getAccount().getZombieKills());
        player.getZombiesGame().unlinkPlayer(player);

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
