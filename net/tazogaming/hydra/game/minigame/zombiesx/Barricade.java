package net.tazogaming.hydra.game.minigame.zombiesx;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 18:35
 */
public class Barricade {
    private NPC     npc;
    private Zombies currGame;

    /**
     * Constructs ...
     *
     *
     * @param x
     * @param y
     * @param g
     */
    public Barricade(int x, int y, Zombies g) {
        NpcAutoSpawn p = new NpcAutoSpawn();

        p.setRoamRange(0);
        p.setSpawnY(0);
        p.setSpawnX(0);
        p.setSpawnHeight(0);

        NPC barricade = new NPC(1532, Point.location(x, y, 0), p);

        World.getWorld().registerNPC(barricade);
        barricade.setHealth(90);
        barricade.setZombieGame(g);
        barricade.setDeathHandler(NPC.UNLINK_ON_DEATH);
    }
}
