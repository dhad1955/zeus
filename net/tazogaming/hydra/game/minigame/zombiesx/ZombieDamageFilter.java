package net.tazogaming.hydra.game.minigame.zombiesx;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/11/13
 * Time: 03:42
 */
public class ZombieDamageFilter extends DamageFilter {

    /**
     * Method filterDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damage
     * @param myMob
     * @param hitBy
     * @param hitType
     *
     * @return
     */
    @Override
    public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
        if ((hitBy instanceof Player) && (Zombies.getCurrentGame() != null)
                && Zombies.getCurrentGame().timerActive(Zombies.DOUBLE_STRENGTH)) {
             damage *= 2;
        }


        if(hitBy instanceof Player && Zombies.getCurrentGame() != null)
        {

            if(Zombies.getCurrentGame().timerActive(Zombies.MAX_FOOD))
                return 0;
            if(Zombies.getCurrentGame().timerActive(Zombies.MULTI_TARGET) && hitType != 8){
                for(NPC npc : Zombies.getCurrentGame().getZombies()){
                  if(Point.getDistance(npc.getLocation(), myMob.getLocation()) < 5)
                    npc.hit(hitBy, damage, 8, 0);
                }
            }
        }

        return damage;
    }
}
