package net.tazogaming.hydra.game.minigame.zombiesx;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/11/13
 * Time: 23:21
 */
public class ZombieGroundItem extends FloorItem {
    private int     pointsRequired = 0;
    private Zombies zombieGame;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param x
     * @param y
     * @param amt
     * @param points
     * @param zombie
     */
    public ZombieGroundItem(int id, int x, int y, int amt, int points, Zombies zombie) {
        super(id, amt, x, y, 0, null);
        pointsRequired = points;
        isGlobal       = true;
        dontRemove     = true;
        zombieGame     = zombie;
    }

    /**
     * Method onPickup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void onPickup(Player player) {
        if (player.getAccount().getZombieScore() < pointsRequired) {
            player.getActionSender().sendMessage("You need atleast " + pointsRequired + " points to pick this up");

            return;
        }

        player.getAccount().setZombieScore(player.getAccount().getZombieScore() - pointsRequired);

        if (getItemId() == 4053) {
            player.getInventory().addItem(4053, 1);

            return;
        }

        player.getActionSender().sendZombiePoints(player.getAccount().getZombieScore());

        int wieldSlot = Item.forId(getItemId()).getWieldSlot();

        player.getEquipment().set(getItemId(), getAmount(), wieldSlot);
        player.getEquipment().updateEquipment(wieldSlot);
        player.getAppearance().setChanged(true);
        player.getTimers().setTime(TimingUtility.ZOMBIE_PICKUP_TIMER, Core.getTicksForMinutes(5));

    }
}
