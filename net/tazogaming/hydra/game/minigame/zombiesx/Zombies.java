package net.tazogaming.hydra.game.minigame.zombiesx;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.npc.ai.movement.PathFinding;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.entity3d.object.Door;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.util.ilobby.*;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.ui.WindowManager;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/11/13
 * Time: 02:57
 */
public class Zombies implements LobbyListener, RecurringTickEvent {
    public static final int
        ZOMBIE_ROUND      = 1,
        ZOMBIE_KILLS      = 0,
        ZOMBIES_REMAINING = 2,
        ZOMBIE_RESET      = 3;

    /** playedPlayers made: 15/04/15 */
    private static ArrayList<Player> playedPlayers = new ArrayList<Player>();
    public static final int
        NEXT_GAME_IN                               = 0,
        NEXT_ROUND_IN                              = 1,
        DOUBLE_STRENGTH                            = 2,
        DOUBLE_POINTS                              = 3,
        COMBAT_SPEED                               = 4,
        ZOMBA_FREEZE                               = 5,
        NUKE                                       = 6,
        MAX_FOOD                                   = 7,
        MULTI_TARGET                               = 8;

    /** currentGame made: 15/04/15 */
    private static Zombies currentGame = null;

    /** doors made: 15/04/15 */
    private static ArrayList<Door> doors             = new ArrayList<Door>();
    public static int[][]          ZOMBIE_CHEST_LOOT = {
        { 4151, 1 }, { 11694, 1 }, { 11696, 1 }, { 1231, 1 }, { 9185, 1, 9144, 100 }, { 19780, 1 }, { 19780, 1 },
        { 19780, 1 }, { 19780, 1 }, { 1305, 1 }, { 1205, 1 }, { 1205, 1 }, { 1205, 1 }, { 1205, 1 }, { 1205, 1 },
        { 1217, 1 }, { 1217, 1 }, {
            4675, 1, 565, 100, 560, 100, 556, 200, 554, 200, 562, 200, 555, 5000, 557, 500
        }, { 11235, 1, 892, 1000 }, { 18349, 1 }, { 11734, 1 }, { 10034, 1000 }, { 10034, 100 }, { 10034, 15 },
        { 7158, 1 }, { 9185, 1, 9242, 5000 }, { 13899, 1 }, { 13902, 1 }, { 1333, 1 }, { 1333, 1 }, { 1333, 1 },
        { 868, 1000 }
    };

    /** ZOMBIE_SPAWN_PTS made: 15/04/15 */
    private static Point[] ZOMBIE_SPAWN_PTS = new Point[] { Point.location(3699, 3497, 0),
            Point.location(3660, 3506, 0), Point.location(3653, 3486, 0) };

    /** active_games made: 15/04/15 */
    private static LinkedList<Zombies> active_games = new LinkedList<Zombies>();

    /** lobby_zone made: 15/04/15 */
    private static Zone lobby_zone = new Zone(3713, 3491, 3715, 3504, 0, true, true, "zombie-lobby").setBankingAllowed(
                                         false).setTeleportAllowed(false).setBobAllowed(false).setStatic(true);

    /** zombie_arena made: 15/04/15 */
    private static Zone zombie_arena = new Zone(
                                           3650, 3459, 3709, 3531, 0, true, true, "zombie_arena").setBankingAllowed(
                                           false).setTeleportAllowed(false).setBobAllowed(false).setStatic(true);

    // 3650 3459 3869 3531 -1 true true zombie_arena
    static {}

    /** barricades made: 15/04/15 */
    private LinkedList<Barricade> barricades = new LinkedList<Barricade>();

    /** activeTimers made: 15/04/15 */
    private long[] activeTimers = new long[12];

    /** timerSeconds made: 15/04/15 */
    private int[] timerSeconds = new int[12];

    /** game_players made: 15/04/15 */
    private LinkedList<Player> game_players = new LinkedList<Player>();

    /** active_zombies made: 15/04/15 */
    private LinkedList<NPC> active_zombies = new LinkedList<NPC>();

    /** currentWave made: 15/04/15 */
    private int currentWave = 0;

    /** npcs_to_spawn made: 15/04/15 */
    private int npcs_to_spawn = 0;

    /** cur_wave_intensity made: 15/04/15 */
    private int cur_wave_intensity = 10;

    /** wave_delay made: 15/04/15 */
    private int wave_delay = 10;

    /** isTerminated made: 15/04/15 */
    private boolean isTerminated = false;

    /** MAX_ZOMBIES_PER_WAVE made: 15/04/15 */
    private int MAX_ZOMBIES_PER_WAVE = 68;

    /** lobbyMode made: 15/04/15 */
    private boolean lobbyMode = true;

    /** lobby made: 15/04/15 */
    private Lobby lobby = null;

    // private static Area zombie_area = new Area(3652, 3)

    /** ground_items made: 15/04/15 */
    private ArrayList<ZombieGroundItem> ground_items = new ArrayList<ZombieGroundItem>();

    /** random made: 15/04/15 */
    private Random random = new Random();

    /** bossSpawned made: 15/04/23 **/
    private boolean bossSpawned = false;

    /** leader made: 15/04/15 */
    private Player leader;

    /**
     * Constructs ...
     *
     *
     * @param leader
     */
    public Zombies(Player leader) {
        zombie_arena.setBankingAllowed(false);
        this.leader = leader;
        lobby       = new Lobby(lobby_zone);
        lobby.setCountdown(2);
        lobby.setMaxPlayers(10);
        lobby.setMinimumPlayers(1);
        lobby.setMode(LobbyMode.TIMER_MODE);
        lobby.setCenterPoint(Point.location(3714, 3503, 0));
        lobby.setListener(this);
        lobby.setUI(new LobbyUIUpdater() {
            @Override
            public void updateUI(Player pla, Lobby lob) {
                if (pla.getWindowManager().getCurrentOverlay() == null) {
                    pla.getWindowManager().showOverlay(11479);
                }

                if (pla.getWindowManager().getKnownVariable(WindowManager.KNOWN_LOBBY_TIMER_SECS)
                        != lob.getSecondsTillStart()) {
                    pla.getActionSender().changeLine("Game starts in: " + ((lob.getSecondsTillStart() > 60)
                            ? (lob.getSecondsTillStart() / 60) + 1 + " mins "
                            : lob.getSecondsTillStart() + " seconds"), 11480);
                    pla.getWindowManager().setKnownVar(WindowManager.KNOWN_LOBBY_TIMER_SECS, lob.getSecondsTillStart());
                }    // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void playerEntered(Player pla, Lobby l) {
                pla.getWindowManager().showOverlay(11479);

                // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void playerLeft(Player pla, Lobby l) {
                pla.getWindowManager().closeOverlay();

                // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void playerConnectionLost(Player pla, Lobby l) {
                if (pla.getWindowManager().getCurrentOverlay() != null) {
                    pla.getWindowManager().showOverlay(-1);
                }

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        lobby.setEntryPolicy(new LobbyEntryPolicy() {
            @Override
            public boolean canEnter(Player player) {
                if (player.getEquipment().isEquipped(3)) {
                    player.getActionSender().sendMessage("You cant take any weapons into zombies.");

                    return false;
                }

                if (playedPlayers.contains(player)) {
                    player.getActionSender().sendMessage("You've already played in this game");

                    return false;
                }

                if (player.getInventory().getFreeSlots(-1) != 28) {
                    player.getActionSender().sendMessage("You cant take any inventory items into zombies.");

                    return false;
                }

                return true;    // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        lobby.addPlayer(leader);
        Core.submitTask(this);
    }

    /**
     * Method getZombies
     * Created on 15/04/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<NPC> getZombies() {
        return active_zombies;
    }

    /**
     * Method pullPowerupChest
     * Created on 15/04/23
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void pullPowerupChest(Player player) {
        if (player.getAccount().getZombieScore() < 2000) {
            player.getActionSender().sendMessage("You need atleast 2000 points to open this");

            return;
        }

        player.getAccount().setZombieScore(player.getAccount().getZombieScore() - 2000);

        Random r = new Random();

        switch (r.nextInt(12)) {
        case 1 :
            if (random.nextBoolean()) {
                for (NPC npc : active_zombies) {
                    double dmg = npc.hit(player, 50, 8, 2);

                    player.getAccount().addZombieScore((int)dmg);
                }

                break;
            }
        case 2 :
            if (!timerActive(COMBAT_SPEED)) {
                activateTimer(COMBAT_SPEED, 120);

                for (Player px : game_players) {
                    px.getActionSender().sendMessage("DOUBLE COMBAT SPEED HAS BEEN ACTIVATED!");
                }
            } else {
                player.hit(player, 30, 8, 2);
            }

            break;

        case 3 :
            player.getEquipment().set(-1, 0, 3);
            player.getEquipment().updateEquipment(3);
            player.getEquipment().updateWeapon();

            break;

        case 4 :
            player.getActionSender().sendMessage("The chest drains your prayer");
            player.getPrayerBook().resetAll();
            player.getPrayerBook().removePoints(100);

            break;

        case 5 :
            player.getTimers().setTime(TimingUtility.MIASMIC_TIMER, 60);
            player.getActionSender().sendMessage("The chest puts a miasmic curse on you");

            break;

        case 6 :
            player.poison(400);
            player.getActionSender().sendMessage("The chest poisons you");

            break;

        case 7 :
            player.getTimers().setTime(TimingUtility.ZOMBIE_PICKUP_TIMER, 1200);
            player.getActionSender().sendMessage("The chest gives you extended weapon time");

            break;

        case 8 :
            player.getPrayerBook().addPoints(100);
            player.getActionSender().sendMessage("The chest restores your prayer");

            break;

        case 9 :
            for (Player player1 : game_players) {
                player1.getActionSender().sendMessage(player.getUsername()
                        + " has activated zombie curse, for 2 minutes");
            }

            activateTimer(MAX_FOOD, 120);

            break;

        case 10 :
            activateTimer(MULTI_TARGET, 60);

            for (Player player1 : game_players) {
                player1.getActionSender().sendMessage(player.getUsername()
                        + " has activated multi target hits for 2 minutes");
            }

            break;
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param player
     *
     * @return
     */
    public boolean setupBarricade(Player player) {
        Tile t = World.getWorld().getTile(player.getLocation());

        if (t.containsNPCChunks()) {
            player.getActionSender().sendMessage("You can't setup a barricade here.");

            return false;
        }

        if (barricades.size() == 5) {
            player.getActionSender().sendMessage("There are too many barricades set up");

            return false;
        }

        barricades.add(new Barricade(player.getX(), player.getY(), this));

        return true;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @return
     */
    public Player getEasiestPlayer() {
        if (game_players.size() == 1) {
            return game_players.getFirst();
        }

        int    l = 0;
        Player p = null;

        for (Player player : game_players) {
            int count = getZombiesAttacking(player);

            if ((p == null) || (count < l)) {
                p = player;
                l = count;
            }
        }

        return p;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param player
     *
     * @return
     */
    public int getZombiesAttacking(Player player) {
        int c = 0;

        for (NPC n : active_zombies) {
            if (n.getMovement() instanceof PathFinding) {
                if (n.getMovement().getInteracting() == player) {
                    c++;
                }
            }
        }

        return c;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param id
     *
     * @return
     */
    public boolean timerActive(int id) {
        return getSecondsOnTimer(id) > 0;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public void tickTimers() {
        for (int i = 0; i < activeTimers.length; i++) {
            if (activeTimers[i] != 0) {
                if (System.currentTimeMillis() - activeTimers[i] > (timerSeconds[i] * 1000)) {
                    activeTimers[i] = 0;
                    timerFinished(i);
                }
            }
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param time1
     *
     * @return
     */
    public int getSecondsOnTimer(int time1) {
        long now     = System.currentTimeMillis();
        long elapsed = now - activeTimers[time1];
        int  secs    = (int) (elapsed / 1000);

        return timerSeconds[time1] - secs;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param id
     */
    public void timerFinished(int id) {}

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param timerId
     * @param seconds
     */
    public void activateTimer(int timerId, int seconds) {
        activeTimers[timerId] = System.currentTimeMillis();
        timerSeconds[timerId] = seconds;

        for (Player player : game_players) {
            player.getActionSender().sendTimer(timerId, getSecondsOnTimer(timerId));
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param player
     */
    public void fromChest(Player player) {
        if (!game_players.contains(player)) {
            return;
        }

        if (player.getTimers().timerActive(TimingUtility.ZOMBIE_POWERUP_TIMER)) {
            player.getActionSender().sendMessage("You can't choose a new weapon yet!");

            return;
        }

        if (player.getAccount().getZombieScore() < 950) {
            player.getActionSender().sendMessage("You need atleast 950 points to take from this chest.");

            return;
        }

        int[] loot = ZOMBIE_CHEST_LOOT[GameMath.rand3(ZOMBIE_CHEST_LOOT.length)];

        for (int i = 0; i < loot.length; i++) {
            int id  = loot[i++];
            int amt = loot[i];

            if ((id > 552) && (id <= 565)) {
                player.getInventory().addItem(id, amt);
            } else {
                int wieldSlot = Item.forId(id).getWieldSlot();

                player.getEquipment().set(id, amt, wieldSlot);
                player.getEquipment().updateEquipment(wieldSlot);
                player.getAppearance().setChanged(true);
            }
        }

        player.getTimers().setTime(TimingUtility.ZOMBIE_POWERUP_TIMER, Core.getTicksForMinutes(2));
        player.getTimers().setTime(TimingUtility.ZOMBIE_PICKUP_TIMER, Core.getTicksForMinutes(5));
        player.getAccount().setZombieScore(player.getAccount().getZombieScore() - 950);
        player.getActionSender().sendZombiePoints(player.getAccount().getZombieScore());
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @return
     */
    public static Zombies getCurrentGame() {
        return currentGame;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param player
     *
     * @return
     */
    public boolean isRegistered(Player player) {
        return game_players.contains(player);
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param pla
     *
     * @return
     */
    public boolean registerToLobby(Player pla) {
        if (lobbyMode) {
            return lobby.addPlayer(pla) == LobbyEntryResponse.OK;
        }

        return false;
    }

    private void newGroundItem(int id, int x, int y, int amt, int pts) {
        ground_items.add(new ZombieGroundItem(id, x, y, amt, pts, this));
    }

    private Point randomPoint() {
        return ZOMBIE_SPAWN_PTS[GameMath.rand3(ZOMBIE_SPAWN_PTS.length)];
    }

    private void nextWave() {
        this.currentWave++;
        this.npcs_to_spawn = currentWave * 5;

        for (Player player : game_players) {
            updateData(player);
            player.setCurrentSpecial(10);
            player.getAccount().increaseVar("zombie_wave", 1);
            SpecialAttacks.updateSpecial(player);
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @return
     */
    public int getZombiesRemaining() {
        return active_zombies.size() + npcs_to_spawn;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param pla
     */
    public void updateData(Player pla) {
        pla.getActionSender().sendZombieData(ZOMBIE_ROUND, currentWave + 1);
        pla.getActionSender().sendZombieData(ZOMBIE_KILLS, pla.getAccount().getZombieKills());
        pla.getActionSender().sendZombieData(ZOMBIES_REMAINING, getZombiesRemaining());
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public void start_game() {
        npcs_to_spawn = 5;

        for (Player player : game_players) {
            player.getAccount().setZombieKills(0);
            updateData(player);
        }

        // bronze scimitar
        newGroundItem(1321, 3671, 3499, 1, 0);
        newGroundItem(1333, 3681, 3490, 1, 4000);

        // maple short
        newGroundItem(853, 3678, 3481, 1, 2000);

        // bronze arrows
        newGroundItem(882, 3675, 3481, 100, 100);

        // msb
        newGroundItem(861, 3689, 3504, 1, 4000);

        // ddp
        newGroundItem(1231, 3671, 3468, 1, 2200);
        newGroundItem(892, 3681, 3471, 400, 2200);
        newGroundItem(4053, 3699, 3461, 1, 950);
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param id
     * @param x
     * @param y
     * @param player
     *
     * @return
     */
    public boolean onObjectClick(int id, int x, int y, Player player) {
        if (id == 10639) {
            if (player.getAccount().getZombieScore() > 2700) {
                player.getActionSender().sendMessage("You restore your prayer.");
                player.getPrayerBook().addPoints(56000);
                player.getAccount().setZombieScore(player.getAccount().getZombieScore() - 2700);
                player.getActionSender().sendZombiePoints(player.getAccount().getZombieScore());

                return true;
            } else {
                player.getActionSender().sendMessage("You need atleast 2700 points to restore your prayer");

                return true;
            }
        }

        return false;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param player
     */
    public void updateHUD(Player player) {
        player.getGameFrame().sendString("Zombie SWAT", 601, 2);
        player.getGameFrame().sendString("Round:", 601, 3);
        player.getGameFrame().sendString(Integer.toString(currentWave), 601, 8);
        player.getGameFrame().sendString("Remaining:", 601, 4);
        player.getGameFrame().sendString(getZombiesRemaining() + "", 601, 9);
        player.getGameFrame().sendString("Score:", 601, 5);
        player.getGameFrame().sendString(player.getAccount().getZombieScore() + "", 601, 10);
        player.getGameFrame().sendString("Bonus:", 601, 6);
        player.getGameFrame().sendString(getBonus(), 601, 11);
    }

    /**
     * Method getBonus
     * Created on 15/04/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getBonus() {
        List<String> timers = new ArrayList<String>(2);

        if (timerActive(DOUBLE_POINTS)) {
            timers.add("x2 Points");
        }

        if (timerActive(DOUBLE_STRENGTH)) {
            timers.add("x2 Strength");
        }

        if (timerActive(MAX_FOOD)) {
            timers.add(Text.RED("ZOMBIE SHIELD"));
        }

        if (timerActive(MULTI_TARGET)) {
            timers.add("Multi-Target");
        }

        if (timerActive(COMBAT_SPEED)) {
            timers.add("Rapid Hits");
        }

        if (timers.size() == 1) {
            return timers.get(0);
        }

        if (timers.size() == 0) {
            return "None";
        }

        StringBuilder builder = new StringBuilder();

        builder.append(timers.get(0));

        for (int i = 1; i < timers.size(); i++) {
            builder.append("+ " + timers.get(i));
        }

        return builder.toString();
    }

    /**
     * Method readyUnlink
     * Created on 15/04/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void readyUnlink(Player player) {
        player.teleport(2804, 3419, 0);
        player.setCurDeathHandler(new DefaultDeathHandler());
        player.setCurrentInstance(null);
        player.setZombiesGame(null);
        player.getEquipment().removeWeapon();
        player.getEquipment().set(-1, 0, 3);
        player.getEquipment().removeItem(3);
        player.getEquipment().updateEquipment(3);
        player.getGameFrame().closeOverlay();
        player.getEquipment().removeItem(Equipment.ARROWS);
        player.getEquipment().updateEquipment(Equipment.ARROWS);
        player.getInventory().clear();
        player.getActionSender().closeTimers();
        player.getActionSender().sendZombieData(ZOMBIE_RESET, 0);
        player.getInventory().clear();

        int amount = 0;
        int wave   = player.getAccount().getInt32("zombie_wave");

        for (int i = 0; i < wave; i++) {
            amount += (currentWave * 2);
        }

        if (!player.getAccount().hasVar("zmround")
                || (player.getAccount().hasVar("zmround")
                    && (player.getAccount().getInt32("zmround") < player.getAccount().getInt32("zombie_wave")))) {
            player.getActionSender().sendMessage("New highest round: " + player.getAccount().getInt32("zombie_wave"));
            player.getAccount().setSetting("zmround", player.getAccount().getInt32("zombie_wave"), true);
        }

        amount += player.getAccount().getZombieKills() * 80;
        player.getAccount().setSetting("zombie_wave", 0);
        player.getAccount().setZombieScore(0);
        player.getAccount().setZombieKills(0);
        player.addPoints(Points.ZOMBIE_POINTS, amount);
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param player
     */
    public void unlinkPlayer(Player player) {
        game_players.remove(player);
        readyUnlink(player);
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param player
     */
    public void registerPlayer(Player player) {
        game_players.add(player);
        playedPlayers.add(player);
        player.setCurrentInstance(null);
        player.setZombiesGame(this);
        player.teleport(3668, 3486, 0);
        player.getAccount().setSetting("zombie_wave", 0);
        player.getActionSender().sendZombieData(ZOMBIE_ROUND, currentWave);
        lobbyMode = false;
        player.setCurDeathHandler(new ZombieDeathHandler());
        player.getGameFrame().openOverlay(601);

        for (int i = 0; i < 7; i++) {
            if (getSecondsOnTimer(i) > 0) {
                player.getActionSender().sendTimer(i, getSecondsOnTimer(i));
            }
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public void endGame() {
        for (NPC npc : active_zombies) {
            npc.unlink();
        }

        active_zombies.clear();
        playedPlayers.clear();

        for (Player player : game_players) {
            readyUnlink(player);
            player.setCurrentInstance(null);
        }

        game_players.clear();
        isTerminated = true;

        for (ZombieGroundItem e : ground_items) {
            e.remove();
        }

        currentWave = 0;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public void tick() {
        if (isTerminated) {
            return;
        }

        if (lobbyMode) {
            lobby.update();

            return;
        }

        tickTimers();

        if ((game_players.size() == 0) || (currentWave == 60)) {
            endGame();

            return;
        }

        updateWave();
        updatePlayers();

        if ((npcs_to_spawn == 0) && (active_zombies.size() == 0)) {
            nextWave();
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return isTerminated;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public void newZombie() {
        Point        p  = randomPoint();
        NpcAutoSpawn sp = new NpcAutoSpawn();

        sp.setRoamRange(100);
        sp.setSpawnX(p.getX());
        sp.setSpawnY(p.getY());
        sp.setSpawnHeight(p.getHeight());

        int id   = 75;
        int next = random.nextInt(100);

        if ((currentWave > 10) && (random.nextInt(100) < 10)) {
            id = 3611;
        } else if ((currentWave > 3) && (next > 90)) {
            id = 77;
        } else if (next > 50) {
            id = 76;
        }

        NPC zombie = new NPC(id, p, sp);

        World.getWorld().registerNPC(zombie);
        zombie.addDamageFilter("zomba", new ZombieDamageFilter());
        zombie.setZombieGame(this);
        zombie.setDefinition(cloneDef(zombie.getDefinition()));
        zombie.setHealth(10 + (int) (currentWave * 1.3d));
        zombie.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                onZombieDead(killedBy.getPlayer(), killed.getNpc());

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        active_zombies.add(zombie);
    }

    private NpcDef cloneDef(NpcDef def) {
        NpcDef newDef  = new NpcDef(def);
        int[]  bonuses = newDef.getBonuses();

        for (int i = 0; i < 8; i++) {
            bonuses[i] += currentWave * 15;

            if (bonuses[i] > 220) {
                bonuses[i] = 220;
            }
        }

        newDef.setSize(def.getSize());

        int[] stats = newDef.getCombatStats();

        for (int i = 0; i < 5; i++) {
            stats[i] = (int) (currentWave * 1.2);
        }

        newDef.setCombatStats(stats);
        newDef.setMaxHit(1 + (currentWave * 2));

        return newDef;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public void updatePlayers() {
        Iterator<Player> iter = game_players.iterator();

        while (iter.hasNext()) {
            Player p = iter.next();

            if ((p.getCurrentHealth() < p.getMaxHealth()) && (Core.timeSince(p.getLastHit()) > 10)
                    && Core.timePassed(3)) {
                p.addHealth(4);
            }

            if (Core.currentTime % 25 == 0) {
                if (p.getCurrentSpecial() < 10) {
                    p.setCurrentSpecial(p.getCurrentSpecial() + 1);
                }

                SpecialAttacks.updateSpecial(p);
            }

            if (!p.getTimers().timerActive(TimingUtility.ZOMBIE_PICKUP_TIMER) && p.getEquipment().isEquipped(3)) {
                p.getEquipment().set(-1, 0, 3);
                p.getEquipment().updateEquipment(3);
                p.getEquipment().updateWeapon();
                p.getAppearance().setChanged(true);
                p.getActionSender().sendMessage("Your weapon has degraded");
            }

            updateHUD(p);

            if (!p.isLoggedIn()) {
                readyUnlink(p);
                iter.remove();

                continue;
            }

            if (!p.isInArea(zombie_arena)) {
                readyUnlink(p);
                iter.remove();
            }
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public void updateWave() {
        if (Core.timeSince(wave_delay) > 10) {
            for (int i = 0; i < npcs_to_spawn; i++) {
                if (active_zombies.size() == MAX_ZOMBIES_PER_WAVE) {
                    break;
                }

                if (i == cur_wave_intensity) {
                    break;
                }

                newZombie();
                npcs_to_spawn--;
            }
        }

        for (NPC npc : active_zombies) {
            if (!npc.isInArea(zombie_arena)) {
                npc.teleport(3668, 3486, 0);
            }
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @return
     */
    public LinkedList<Player> getPlayers() {
        return game_players;
    }

    private void updateKills() {
        for (Player p : game_players) {
            p.getActionSender().sendZombieData(ZOMBIES_REMAINING, getZombiesRemaining());
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param killedBy
     * @param npc
     */
    public void onZombieDead(Player killedBy, NPC npc) {
        active_zombies.remove(npc);
        killedBy.getAccount().setZombieKills(killedBy.getAccount().getZombieKills() + 1);
        killedBy.getActionSender().sendZombieData(ZOMBIE_KILLS, killedBy.getAccount().getZombieKills());
        updateKills();

        if (GameMath.rand(200) < 4) {
            activateTimer(DOUBLE_POINTS, 120);
        } else if (npc.getRandom().nextInt(300) < 4) {
            activateTimer(DOUBLE_STRENGTH, 120);
        }
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     */
    public static void updateGames() {}

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param pla
     *
     * @return
     */
    public static Zombies newGame(Player pla) {

        // active_games.add(zom);
        playedPlayers.clear();

        if ((currentGame != null) && currentGame.isRegistered(pla)) {
            return null;
        }

        if ((currentGame != null) &&!currentGame.isTerminated) {
            if (!currentGame.lobby.getEntryPolicy().canEnter(pla)) {
                return null;
            }

            if (!currentGame.isRegistered(pla) &&!currentGame.lobbyMode) {
                currentGame.registerPlayer(pla);

                return currentGame;
            } else if (currentGame.lobbyMode) {
                if (currentGame.registerToLobby(pla)) {
                    return currentGame;
                }
            }
        }

        currentGame = new Zombies(pla);

        return currentGame;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param lobby
     *
     * @return
     */
    @Override
    public boolean onLobbyFinished(Lobby lobby) {
        for (Player pla : lobby.getPlayers()) {
            registerPlayer(pla);
        }

        start_game();

        return true;
    }

    /**
     * Method $methodName$
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     *
     *
     * @param lobby
     *
     * @return
     */
    public boolean onLobbyEmpty(Lobby lobby) {
        isTerminated = true;

        return true;
    }
}
