package net.tazogaming.hydra.game.minigame.treasure_trails;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/10/13
 * Time: 21:56
 */
public class Clue {
    private int      level;
    private int      id;
    private String[] lines;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param level
     * @param lines
     */
    public Clue(int id, int level, String... lines) {
        setLevel(level);
        setId(id);
        setLines(lines);
    }

    /**
     * Method getLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getLines() {
        return lines;
    }

    /**
     * Method setLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lines
     */
    public void setLines(String[] lines) {
        this.lines = lines;
    }

    /**
     * Method getId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Method setId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Method getLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevel() {
        return level;
    }

    /**
     * Method setLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param level
     */
    public void setLevel(int level) {
        this.level = level;
    }
}
