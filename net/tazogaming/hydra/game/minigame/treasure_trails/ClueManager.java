package net.tazogaming.hydra.game.minigame.treasure_trails;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.ui.rsi.interfaces.ModelGrid;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.game.LootTable;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/10/13
 * Time: 21:58
 */
public class ClueManager {
    public static int[] CLUE_BINDED_IDS = { 2677, 2801, 2796, 19043 };

    /** reward_tables made: 14/08/28 **/
    private static int[] reward_tables = { 533, 534, 535, 536 };

    private static int[][] reward_ranges  = {{100000, 1000000}, {500000, 2000000}, {10000000, 3000000}, {30000000, 100000000}};

    /** clues made: 14/08/28 **/
    private static Clue[][] clues = new Clue[4][100];

    /** clueIndicies made: 14/08/28 **/
    private static ArrayList[] clueIndicies = new ArrayList[4];    // <Integer>();

    /**
     * Method getClueLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     *
     * @return
     */
    public static int getClueLevel(int itemid) {
        for (int i = 0; i < CLUE_BINDED_IDS.length; i++) {
            if (CLUE_BINDED_IDS[i] == itemid) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method isClue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isClue(int id) {
        for (Integer i : CLUE_BINDED_IDS) {
            if (i == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method load_clues
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public static void load_clues(String str) {
        load_clues(new File(str));
    }

    /**
     * Method load_clues
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     */
    public static void load_clues(File f) {
        for (int i = 0; i < clueIndicies.length; i++) {
            clueIndicies[i] = new ArrayList<Integer>();
        }

        try {
            BufferedReader br   = new BufferedReader(new FileReader(f));
            String         line = null;

            while ((line = br.readLine()) != null) {
                String[] tokens = line.split(" ");

                if (tokens[0].equalsIgnoreCase("clue")) {
                    int      id  = Integer.parseInt(tokens[2]);
                    int      lvl = Integer.parseInt(tokens[3]);
                    String[] str = new String[tokens.length - 4];


                    System.arraycopy(tokens, 4, str, 0, tokens.length - 4);
                    clues[lvl][id] = new Clue(id, lvl, str);
                    clueIndicies[lvl].add(id);
                }
            }

            br.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    private static Clue randomClue(int lvl, ArrayList<Clue> exclusions, Random random) {
        Clue[]     clue_list     = clues[lvl];
        List<Clue> possibleClues = new ArrayList<Clue>();

        for (Integer i : (ArrayList<Integer>) clueIndicies[lvl]) {
            Clue clue = clues[lvl][i];

            possibleClues.add(clue);
        }

        possibleClues.removeAll(exclusions);

        if (possibleClues.size() == 0) {
            return null;
        }

        return possibleClues.get(random.nextInt(possibleClues.size()));
    }

    /**
     * Method advanceClue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void advanceClue(Player player) {
        String   clueData = player.getAccount().getString("clue_data");    // .split(" ");
        String[] split    = clueData.split(" ");

        clueData = clueData.substring(split[0].length() + 1);

        if (clueData.length() == 0) {
            handleClueFinished(player, player.getAccount().getInt32("clue_level"));
            player.getAccount().removeVar("clue_level");
            player.getAccount().removeVar("clue_data");
        } else {
            player.getAccount().setSetting("clue_data", clueData, true);
        }
    }

    /**
     * Method rewardsInterface
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param models
     * @param modelsAmt
     * @param player
     */
    public static final void rewardsInterface(int[] models, int[] modelsAmt, Player player) {
        ModelGrid grid = new ModelGrid(90, 364, 4);

        player.getActionSender().sendClientScript(grid.getCS2());
        player.getActionSender().sendItems(90, models, modelsAmt, false);
    }

    /**
     * Method handleClueFinished
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param lvl
     */
    public static void handleClueFinished(Player pla, int lvl) {
        int randomAmount = GameMath.rand(4);

        if (randomAmount == 0) {
            randomAmount = 1;
        }

        Loot[] loot         = LootTable.getTable(reward_tables[lvl]).getLoot(randomAmount, pla);
        int[]  items        = new int[loot.length + 1];
        int[]  itemsAmounts = new int[loot.length + 1];

        items[0] = 995;
        itemsAmounts[0] =  reward_ranges[lvl][0] + GameMath.rand(reward_ranges[lvl][1] - reward_ranges[lvl][0]);


        pla.getInventory().addItemDrop(Item.forId(995), itemsAmounts[0]);

        pla.getInventory().deleteItem(CLUE_BINDED_IDS[lvl], 1);

        for (int i = 0; i < loot.length; i++) {
            if (loot[i] == null) {
                return;
            }

            pla.getInventory().addItemDrop(Item.forId(loot[i].getItemID()), loot[i].getAmount());
            items[i + 1]        = loot[i].getItemID();
            itemsAmounts[i + 1] = loot[i].getAmount();
        }
        pla.addAchievementProgress2(Achievement.INDIANA_JONES, 1);
        pla.addAchievementProgress2(Achievement.X_MARKS_THE_SPOT, 1);
        if(lvl == 2)
            pla.addAchievementProgress2(Achievement.HARD_TREASURE, 1);


        if(pla.getAccount().hasVar("cluecount"))
            pla.getAccount().increaseVar("cluecount", 1);
        else
        pla.getAccount().setSetting("cluecount", 1, true);


            rewardsInterface(items, itemsAmounts, pla);
        pla.getGameFrame().openWindow(364);
    }

    /**
     * Method handle_scroll_pickup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param scrollId
     *
     * @return
     */
    public static boolean handle_scroll_pickup(Player player, int scrollId) {
        int lvl = getClueLevel(scrollId);

        if (lvl == -1) {
            return true;
        }

        if (player.getAccount().hasVar("clue_level")) {
            player.getActionSender().sendMessage("You cant have more than one clue scroll at once.");
            player.getActionSender().sendMessage(
                "If you don't have a clue scroll and are getting this message, please contact support.");

            return false;
        }

        player.getAccount().setSetting("clue_level", lvl, true);
        setClueData(player, lvl);
        player.getActionSender().sendMessage("You've just found a clue scroll.");

        return true;
    }

    /**
     * Method setClueData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param clueLevel
     */
    public static void setClueData(Player player, int clueLevel) {
        int min    = 3;
        int max    = 12;
        int amount = player.getRandom().nextInt(max) + min;

        if (amount > max) {
            amount = max;
        }

        if (amount > clues[clueLevel].length) {
            amount = clues[clueLevel].length;
        }

        ArrayList<Clue> save = new ArrayList<Clue>();
        String          str  = "";

        for (int i = 0; i < amount; i++) {
            Clue rand = randomClue(clueLevel, save, player.getRandom());

            if (rand == null) {

                break;
            }

            save.add(rand);
            str += rand.getId();
            str += " ";
        }

        player.getAccount().setSetting("clue_data", str, true);
    }

    /**
     * Method showClue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param clickedItem
     *
     * @return
     */
    public static boolean showClue(Player pla, Item clickedItem) {
        int lvl = getClueLevel(clickedItem.getIndex());


        if (lvl == -1) {
            return false;
        }

        if (!pla.getAccount().hasVar("clue_data") ||!pla.getAccount().hasVar("clue_level")) {
            pla.getInventory().deleteItem(clickedItem.getIndex(), 1);
            pla.getAccount().removeVar("clue_level");
            pla.getAccount().removeVar("clue_data");
            pla.getActionSender().sendMessage("The clue scroll crumbles.");

            return false;
        }




        int  clue_id = Integer.parseInt(pla.getAccount().getString("clue_data").split(" ")[0]);
        Clue clue    = clues[lvl][clue_id];

        if (clue == null) {
            return false;
        }

        for (int i = 0; i < clue.getLines().length; i++) {
            pla.getGameFrame().sendString(clue.getLines()[i].replace("_", " "), 345, i + 1);
        }

        for (int i = clue.getLines().length + 1; i < 9; i++) {
            pla.getGameFrame().sendString("", 345, i);
        }

        pla.getGameFrame().openWindow(345);

        return true;
    }
}
