package net.tazogaming.hydra.game.cutscene;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.Scope;

//~--- JDK imports ------------------------------------------------------------

import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/10/13
 * Time: 08:20
 */
public class CutSceneStage {
    private Player player;

    /**
     * Method updateScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    boolean updateScript() {
        getPlayer().getScriptsToProcess().addAll(getPlayer().getNewScriptQueue());
        getPlayer().getNewScriptQueue().clear();

        for (Iterator<Scope> scriptIterator =
                getPlayer().getScriptsToProcess().iterator(); scriptIterator.hasNext(); ) {
            Scope script = scriptIterator.next();

            if (script.isTerminated()) {
                scriptIterator.remove();

                continue;
            }

            if (script.isWaiting()) {
                script.validateNpcChat();

                continue;
            }

            if (script.isDelayed()) {
                if (script.hasDelayTimePassed()) {
                    script.terminate();
                    scriptIterator.remove();
                }

                continue;
            }

            if (script.canRun()) {
                if (World.getWorld().getScriptManager().evaluate(script)) {
                    script.terminate();
                    scriptIterator.remove();
                }
            }
        }

        return getPlayer().getScriptsToProcess().size() == 0;
    }

    private Player getPlayer() {
        return player;
    }
}
