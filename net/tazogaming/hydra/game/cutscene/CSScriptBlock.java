package net.tazogaming.hydra.game.cutscene;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.js.JPlugin;
import net.tazogaming.hydra.script.runtime.runnable.ScriptRunnable;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/10/13
 * Time: 09:41
 */
public class CSScriptBlock extends ScriptRunnable {
    public static final int
        TYPE_NPC_UPDATE = 0,
        TYPE_TRIGGER    = 1,
        TYPE_SUB        = 2;
    private int                  npcUpdateId;
    private int                  type;
    private RuntimeInstruction[] lines;

    /**
     * Method getFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getFileName() {
        return "lol";
    }

    /**
     * Method getPlugin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public JPlugin getPlugin() {
        return null;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getLines() {
        return new String[0];    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getCompiledLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RuntimeInstruction[] getCompiledLines() {
        return lines;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method setNpcID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setNpcID(int id) {
        this.npcUpdateId = id;
    }

    /**
     * Method getNpcID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNpcID() {
        return npcUpdateId;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Method setCalls
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param runtimeInstructions
     */
    public void setCalls(RuntimeInstruction[] runtimeInstructions) {
        lines = runtimeInstructions;
        super.setOperationList(runtimeInstructions);
    }
}
