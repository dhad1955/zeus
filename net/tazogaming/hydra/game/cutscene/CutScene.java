package net.tazogaming.hydra.game.cutscene;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;

//~--- JDK imports ------------------------------------------------------------

import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/10/13
 * Time: 08:14
 */
public class CutScene extends Instance {
    private NPC[]          npcs            = new NPC[30];
    private int            curr_dialog_npc = -1;
    private String         dialog_name     = null;
    private Point          lastLocation    = null;
    private Player         player;
    private int            currentOffset;
    private CutSceneScript cs_script;
    private Scope          trigger;

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param script
     * @param trigger
     */
    public CutScene(Player pla, CutSceneScript script, Scope trigger) {
        this.player       = pla;
        this.cs_script    = script;
        this.lastLocation = pla.getLocation();
        player.getWindowManager().disable_sidebar();
        player.getActionSender().sendMinimapState(2);
        player.getNewScriptQueue().add(cs_script.construct(pla));
        this.trigger = trigger;
    }

    /**
     * Method getLastLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getLastLocation() {
        return lastLocation;
    }

    /**
     * Method set_curr_dialog_npc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param name
     */
    public void set_curr_dialog_npc(int id, String name) {
        this.curr_dialog_npc = id;
        dialog_name          = name;
    }

    /**
     * Method nullTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void nullTrigger() {
        trigger = null;
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void terminate() {
        for (int i = 0; i < npcs.length; i++) {
            if (npcs[i] != null) {
                npcs[i].unlink();
            }
        }

        player.setCutScene(null);
        player.setCurrentInstance(null);
        player.getNewScriptQueue().clear();
        player.getActionSender().sendMinimapState(0);
        player.getWindowManager().enable_sidebar();
        player.terminateScripts();
        player.getActionSender().resetCamera();
        ;

        if (trigger != null) {
            trigger.setTerminated(false);

            if (!World.getWorld().getScriptManager().evaluate(trigger)) {
                player.getNewScriptQueue().add(trigger);
            }
        }
    }

    /**
     * Method rewind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void rewind() {
        this.player.getWindowManager().disable_sidebar();
        this.player.getScriptsToProcess().clear();
        this.player.getNewScriptQueue().clear();
        this.player.getWindowManager().disable_sidebar();
        this.player.getActionSender().sendMinimapState(2);

        for (NPC npc : npcs) {
            if (npc != null) {
                npc.unlink();
            }
        }

        npcs = new NPC[npcs.length];
        player.getNewScriptQueue().add(cs_script.construct(player));
    }

    /**
     * Method get_npc_dialog_name
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String get_npc_dialog_name() {
        return dialog_name;
    }

    /**
     * Method get_curr_dialog_npc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int get_curr_dialog_npc() {
        return curr_dialog_npc;
    }

    /**
     * Method get_cs_script
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public CutSceneScript get_cs_script() {
        return cs_script;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    private boolean pre_process(NPC npc) {
        return true;
    }

    /**
     * Method pre_process
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean pre_process(Player player) {
        if (!player.getRoute().isIdle()) {
            player.getRoute().resetPath();
            player.getActionSender().sendCloseWalkingFlag();
        }

        return true;
    }

    /**
     * Method process
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean process() {
        return updateScript();
    }

    private int getNpcId(NPC npc) {
        for (int i = 0; i < npcs.length; i++) {
            if ((npcs[i] != null) && (npcs[i] == npc)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method process
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public boolean process(NPC npc) {
        npc.updateMovement();

        CSScriptBlock npcUpdateblock;

        npcUpdateblock = cs_script.get_npc_update(getNpcId(npc));

        if (npcUpdateblock != null) {
            Scope script = new Scope();

            script.setController(player);
            script.setRunning(npcUpdateblock);
            World.getWorld().getScriptManager().evaluate(script);
        }

        return true;
    }

    /**
     * Method addNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param npc_id
     * @param x
     * @param y
     */
    public void addNPC(int id, int npc_id, int x, int y) {
        NPC npc = new NPC(npc_id, x, y, player.getHeight(), this);

        npc.setDeathHandler(NPC.UNLINK_ON_DEATH);
        World.getWorld().registerNPC(npc);
        this.npcs[id] = npc;
    }

    /**
     * Method getNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public NPC getNpc(int id) {
        return npcs[id];
    }

    /**
     * Method updateScript
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    boolean updateScript() {
        getPlayer().getScriptsToProcess().addAll(getPlayer().getNewScriptQueue());
        getPlayer().getNewScriptQueue().clear();

        if (player.getScriptsToProcess().size() > 0) {
            for (Iterator<Scope> scriptIterator = getPlayer().getScriptsToProcess().iterator();
                    scriptIterator.hasNext(); ) {
                Scope script = scriptIterator.next();

                if (script.isTerminated()) {
                    scriptIterator.remove();

                    continue;
                }

                if (script.isWaiting()) {
                    script.validateNpcChat();

                    continue;
                }

                if (script.isDelayed()) {
                    if (script.hasDelayTimePassed()) {
                        script.terminate();
                        scriptIterator.remove();
                    }

                    continue;
                }

                if (script.canRun()) {
                    if (World.getWorld().getScriptManager().evaluate(script)) {
                        script.terminate();
                        scriptIterator.remove();
                    }
                }
            }
        }

        return getPlayer().getScriptsToProcess().size() != 0;
    }
}
