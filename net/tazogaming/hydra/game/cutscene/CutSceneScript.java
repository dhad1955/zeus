package net.tazogaming.hydra.game.cutscene;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/10/13
 * Time: 10:22
 */
public class CutSceneScript {
    private CSScriptBlock[] npc_updates = new CSScriptBlock[40];
    private CSScriptBlock[] subs        = new CSScriptBlock[40];
    private int             cutSceneId  = -1;
    private CSScriptBlock   enter_script;

    /**
     * Method construct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public Scope construct(Player player) {
        Scope script = new Scope();

        script.setController(player);
        script.setRunning(enter_script);

        return script;
    }

    /**
     * Method get_npc_update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public CSScriptBlock get_npc_update(int id) {
        return npc_updates[id];
    }

    /**
     * Method getSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public CSScriptBlock getSub(int id) {
        return subs[id];
    }

    /**
     * Method addNPCUpdate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param e
     */
    public void addNPCUpdate(int id, CSScriptBlock e) {
        this.npc_updates[id] = e;
    }

    /**
     * Method addSub
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param block
     */
    public void addSub(int id, CSScriptBlock block) {
        subs[id] = block;
    }

    /**
     * Method getCutSceneId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCutSceneId() {
        return cutSceneId;
    }

    /**
     * Method setCutSceneId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cutSceneId
     */
    public void setCutSceneId(int cutSceneId) {
        this.cutSceneId = cutSceneId;
    }

    /**
     * Method setLoad
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void setLoad(CSScriptBlock e) {
        this.enter_script = e;
    }
}
