package net.tazogaming.hydra.game.cutscene;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.script.Line;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.util.ArgumentTokenizer;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/10/13
 * Time: 09:45
 */
public class CSScriptParser {
    private static CutSceneScript[]  cutScenes = new CutSceneScript[100];
    private static Player            p_loader;
    private HashMap<String, Integer> finals = new HashMap<String, Integer>();

    /**
     * Constructs ...
     *
     *
     * @param file
     * @param loader
     */
    public CSScriptParser(String file, Player loader) {
        File       file2 = new File(file);
        List<File> files = (List<File>) FileUtils.listFiles(file2, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

        p_loader = loader;

        for (File f : files) {
            createCutscene(f);
        }
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static CutSceneScript get(int id) {
        return cutScenes[id];
    }

    /**
     * Method log_error
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void log_error(String str) {
        if (p_loader != null) {
            if (p_loader.getRights() >= Player.ADMINISTRATOR) {
                p_loader.getActionSender().sendMessage(str);
            }
        }
    }

    private void getFinals(File f) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(f));

        while (true) {
            String line = reader.readLine();

            if (line == null) {
                break;
            }

            while ((line.startsWith("\t") || line.startsWith(" ")) && (line.length() > 0)) {
                line = line.substring(1);
            }

            if ((line.length() == 0) || line.startsWith("##")) {
                continue;
            }

            if (line.startsWith("const")) {
                ArgumentTokenizer pr = new ArgumentTokenizer(line.split(" "));

                pr.next();

                String name = pr.nextString();

                pr.next();
                finals.put(name, pr.nextInt());

                continue;
            }

            if (line.toLowerCase().contains("define") && line.contains("final")) {
                ArgumentTokenizer p      = new ArgumentTokenizer(line.split(" "));
                String            curTok = p.nextString();

                if (curTok.equalsIgnoreCase("define")) {
                    String type = p.nextString();

                    if (type.equalsIgnoreCase("final") || type.equalsIgnoreCase("const")) {
                        String finalName = p.nextString();

                        p.next();

                        int id = p.nextInt();

                        finals.put(finalName, id);
                    }
                }
            }
        }

        reader.close();
    }

    private String replace_finals(String str) {
        for (String name : finals.keySet()) {
            if (str.contains(name)) {
                str = str.replace(name, "" + finals.get(name));
            }
        }

        return str;
    }

    /**
     * Method createCutscene
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     *
     * @return
     */
    public CutSceneScript createCutscene(File f) {
        int            cur_line_index = 0;
        BufferedReader cs_file_reader = null;

        try {
            getFinals(f);
            cs_file_reader = new BufferedReader(new FileReader(f));

            String                         cur_line                    = null;
            CSScriptBlock                  enter_trigger               = null;
            CSScriptBlock                  current_block               = null;
            boolean                        trigger_made                = false;
            CutSceneScript                 current_cutscene            = new CutSceneScript();
            LinkedList<RuntimeInstruction> current_runtimeInstructions = new LinkedList<RuntimeInstruction>();

            while ((cur_line = cs_file_reader.readLine()) != null) {
                cur_line_index++;

                while ((cur_line.startsWith(" ") || cur_line.startsWith("\t")) && (cur_line.length() > 0)) {
                    cur_line = cur_line.substring(1);
                }

                if (cur_line.startsWith("##") || cur_line.startsWith("//") || (cur_line.length() == 0)) {
                    continue;
                }

                ArgumentTokenizer parser        = new ArgumentTokenizer(cur_line.split(" "));
                String            current_token = parser.nextString();

                if (current_token.toLowerCase().equalsIgnoreCase("define")) {
                    String var = parser.nextString();

                    parser.next();    // equals sign

                    if (var.equalsIgnoreCase("cutscene_id")) {
                        current_cutscene.setCutSceneId(parser.nextInt());
                    }

                    continue;
                } else {
                    cur_line = replace_finals(cur_line);
                }

                if (current_token.equalsIgnoreCase("update_npc")) {
                    if (!cur_line.contains("{")) {
                        log_error("Parse error, no opening bracket on line: " + cur_line + " " + f.getName());
                        cs_file_reader.close();

                        return null;
                    }

                    current_block = new CSScriptBlock();
                    current_block.setType(CSScriptBlock.TYPE_NPC_UPDATE);
                    current_block.setNpcID(parser.nextInt());

                    continue;
                } else if (current_token.equalsIgnoreCase("sub")) {
                    if (!cur_line.contains("{")) {
                        log_error("Parse error, no opening bracket on line: " + cur_line + " " + f.getName());
                        cs_file_reader.close();

                        return null;
                    }

                    current_block = new CSScriptBlock();
                    current_block.setType(CSScriptBlock.TYPE_SUB);
                    current_block.setNpcID(parser.nextInt());

                    continue;
                } else if (current_token.equalsIgnoreCase("directTrigger")) {
                    if (!cur_line.contains("{")) {
                        log_error("Parse error, no opening bracket on line: " + cur_line + " " + f.getName());
                        cs_file_reader.close();

                        return null;
                    }

                    String trigger_name = parser.nextString();

                    current_block = new CSScriptBlock();
                    current_block.setType(CSScriptBlock.TYPE_TRIGGER);

//                  current_block.setNpcID(parser.nextInt());
                    if (trigger_name.equalsIgnoreCase("construct")) {
                        enter_trigger = current_block;
                        trigger_made  = true;
                    }

                    continue;
                }

                if (current_block != null) {
                    Line line = new Line(cur_line, cur_line_index);
                }

                if (current_token.equalsIgnoreCase("}")) {
                    if (current_block == null) {
                        log_error("Parse error, line: " + cur_line_index + " " + f.getName());
                        cs_file_reader.close();

                        return null;
                    }

                    RuntimeInstruction[] runtimeInstructions =
                        new RuntimeInstruction[current_runtimeInstructions.size()];

                    current_runtimeInstructions.toArray(runtimeInstructions);
                    current_runtimeInstructions.clear();
                    current_block.setCalls(runtimeInstructions);

                    if (current_block.getType() == CSScriptBlock.TYPE_NPC_UPDATE) {
                        current_cutscene.addNPCUpdate(current_block.getNpcID(), current_block);
                    } else if (current_block.getType() == CSScriptBlock.TYPE_SUB) {
                        current_cutscene.addSub(CSScriptBlock.TYPE_SUB, current_block);
                    } else if (current_block == enter_trigger) {
                        current_cutscene.setLoad(current_block);
                        enter_trigger = null;
                    }

                    current_block = null;
                }
            }

            if (!trigger_made) {
                log_error("Error no constructor made for cutscene.");
                cs_file_reader.close();

                return null;
            }

            cs_file_reader.close();
            cutScenes[current_cutscene.getCutSceneId()] = current_cutscene;

            return current_cutscene;
        } catch (Exception ee) {
            log_error("Parse error: " + f.getName() + " " + cur_line_index);
            ee.printStackTrace();
        }

        return null;
    }
}
