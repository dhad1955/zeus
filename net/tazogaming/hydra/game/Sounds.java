package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Region;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/09/14
 * Time: 06:19
 */
public class Sounds {

    /**
     * Method playSingleSound
     * Created on 14/09/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sfx
     * @param vol
     * @param player
     */
    public static void playSingleSound(int sfx, int vol, Player player) {
        player.getActionSender().sendSound(player, sfx, vol, 255, false);
    }

    /**
     * Method playSingleSound
     * Created on 14/09/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sfx
     * @param player
     */
    public static void playSingleSound(int sfx, Player player) {
        playSingleSound(sfx, 255, player);
    }

    /**
     * Method playSingleVoice
     * Created on 14/09/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sfx
     * @param player
     */
    public static void playSingleVoice(int sfx, Player player) {
        player.getActionSender().sendSound(player, sfx, 255, 255, true);
    }

    /**
     * Method playAreaSound
     * Created on 14/09/14
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     * @param sfx
     */
    public static void playAreaSound(Point location, int sfx) {
        for (Player player : World.getWorld().getPlayers()) {
            if (Point.getDistance(player.getLocation(), location) < 8) {
                playSingleSound(sfx, 255 - (Point.getDistance(player.getLocation(), location) * 16), player);
            }
        }
    }
}
