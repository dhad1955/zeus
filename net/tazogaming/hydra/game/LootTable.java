package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.net.packethandler.Command;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------


import java.security.SecureRandom;

import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/10/13
 * Time: 11:03
 */
public class LootTable {
    public static final int
        LOOT_ALWAYS                      = 0,
        LOOT_VERY_COMMON                 = 1,
        LOOT_COMMON                      = 2,
        LOOT_UNCOMMON                    = 3,
        LOOT_RARE                        = 4,
        LOOT_ULTRA_RARE                  = 5;
    public static LootTable LOOT_CACHE[] = new LootTable[1000];

    /** dropRandom made: 14/08/25 */
    private static SecureRandom dropRandom = new SecureRandom();

    /** loot_types made: 14/08/25 */
    private Loot[][] loot_types = new Loot[6][];

    /** sharedTables made: 14/08/25 */
    private ArrayList<Integer> sharedTables = new ArrayList<Integer>();


    public List<Loot> getAll() {
        List<Loot> tmp = new LinkedList<Loot>();
        for(Loot[] looot : loot_types){
          if(looot != null)
            tmp.addAll(Arrays.asList(looot));

        }
        return tmp;

    }
    /**
     * Constructs ...
     *
     *
     * @param id
     * @param r
     *
     * @throws SQLException
     */
    public LootTable(int id, ResultSet r) throws SQLException {
        ArrayList<Loot>[] loot_pre_list = new ArrayList[6];

        while (r.next()) {
            int rarity = r.getInt("rarity");
            int itemid = r.getInt("itemid");
            int amount = r.getInt("amount");

            if (loot_pre_list[rarity] == null) {
                loot_pre_list[rarity] = new ArrayList<Loot>();
            }

            loot_pre_list[rarity].add(new Loot(itemid, amount, rarity));
        }

        for (int i = 0; i < loot_pre_list.length; i++) {
            if (loot_pre_list[i] != null) {
                loot_types[i] = new Loot[loot_pre_list[i].size()];
                loot_pre_list[i].toArray(loot_types[i]);
            }
        }

        LOOT_CACHE[id] = this;
    }

    /**
     * Method injectTable
     * Created on 14/08/25
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param table
     */
    public void injectTable(LootTable table) {
        ArrayList<Loot> preList = new ArrayList<Loot>();

        for (int i = 0; i < table.loot_types.length; i++) {
            if ((table.loot_types[i] != null) && (table.loot_types[i].length > 0)) {
                preList = new ArrayList<Loot>();
                preList.addAll(Arrays.asList(table.loot_types[i]));

                if (loot_types[i] == null) {
                    loot_types[i] = new Loot[preList.size()];
                    preList.toArray(loot_types[i]);
                } else {
                    preList.addAll(Arrays.asList(loot_types[i]));
                    loot_types[i] = new Loot[preList.size()];
                    preList.toArray(loot_types[i]);
                }
            }
        }
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load() {
        MysqlConnection con  = World.getWorld().getDatabaseLink();
        ResultSet       r    = con.query("SELECT * FROM loottable");
        long            time = System.currentTimeMillis();

        try {
            while (r.next()) {
                new LootTable(r.getInt("id"),
                              con.query("SELECT * from npcdrop where tableindex='" + r.getInt("id") + "'"));
            }
            r.close();

        } catch (Exception ee) {
            ee.printStackTrace();
        }


        r = con.query("SELECT * from tablelink");

        try {
            while (r.next()) {
                try {
                    getTable(r.getInt("toind")).injectTable(getTable(r.getInt("fromind")));
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
            }
            r.close();
        } catch (SQLException ee) {
            ee.printStackTrace();
        }

        Logger.log("[Loot]Loaded loot tables, time: " + (System.currentTimeMillis() - time) + "ms");
    }

    /**
     * Method getTable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static LootTable getTable(int id) {
        return LOOT_CACHE[id];
    }

    // /

    /**
     * Method runtest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static final void runtest() {
        int[] statistics = new int[6];

        for (int i = 0; i < 100; i++) {
            try {
                Thread.sleep(5);
            } catch (Exception ee) {}

            statistics[rarity(5)]++;
        }

        for (int k = 0; k < statistics.length; k++) {
            System.out.println("table[" + k + "] = " + statistics[k]);
        }

        System.out.println("~~~");
    }

    private static int rarity(int bonus) {
        int ran = dropRandom.nextInt(265);

        if (ran < 100) {
            return LOOT_VERY_COMMON;
        }

        if (ran < 150) {
            return LOOT_COMMON;
        }

        if (ran < 185) {
            return LOOT_UNCOMMON;
        }

        if (ran < 192 + bonus) {
            return LOOT_RARE;
        }

        int threshHold = 1;

        threshHold += (bonus / 2);

        if (ran >= 265 - threshHold) {
            return LOOT_ULTRA_RARE;
        }

        return LOOT_VERY_COMMON;
    }
    private int getRarityPkMode(int bonus) {
        int random = GameMath.rand(1000);

        if ((random > 0) && (random < 700)) {
            return LOOT_VERY_COMMON;
        }

        if ((random > 700) && (random < 850)) {
            return LOOT_COMMON;
        }

        if ((random > 850) && (random < 900)) {
            return LOOT_UNCOMMON;
        }


        if ((random > 900) && (random < 917)) {
            return LOOT_RARE;
        }


        if ((random > 918) && (random < 917)) {
            return LOOT_ULTRA_RARE;
        }

        return GameMath.rand(3);
    }
    private int getRarity(int bonus) {
        int random = GameMath.rand(1000);
        if(Command.MAD_DROPS)
            bonus += 10;

        if ((random > 0) && (random < 500)) {
            return LOOT_VERY_COMMON;
        }

        if ((random > 500) && (random < 850)) {
            return LOOT_COMMON;
        }

        if ((random > 850) && (random < 950)) {
            return LOOT_UNCOMMON;
        }

        if ((random > 960) && (random < 970 + bonus)) {
            return LOOT_RARE;
        }

        if ((random > 974 + bonus) && (random < 978 + bonus + bonus)) {
            return LOOT_ULTRA_RARE;
        }

        return GameMath.rand(3);
    }

    /**
     * Method getLootAlways
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Loot[] getLootAlways() {
        return loot_types[0];
    }

    /**
     * Method randomLoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public Loot randomLoot(int index) {
        if (loot_types[index] == null) {
            return null;
        }

        if ((sharedTables.size() > 0) && dropRandom.nextBoolean()) {
            return getTable(sharedTables.get(GameMath.rand3(sharedTables.size()))).randomLoot(index);
        }

        return loot_types[index][GameMath.rand3(loot_types[index].length)];
    }

    /**
     * Method getLoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     * @param player
     *
     * @return
     */
    public Loot[] getLoot(int amount, Player player) {
        Loot[] returnAmt = new Loot[amount];
        int    bonus     = 0;


        if(player.getEquipment().getId(Equipment.RING) == 15014 || player.getEquipment().getId(Equipment.RING)== 13566){
            bonus = 7;
        }
        if (player.getEquipment().getId(Equipment.RING) == 2572) {
            bonus = 3;

            if (!player.getAccount().hasVar("row_uses")) {
                player.getAccount().setSetting("row_uses", 70);
            }

            player.getAccount().decreaseVar("row_uses", 1);

            if (player.getAccount().getInt32("row_uses") == 0) {
                player.getEquipment().removeItem(Equipment.RING);
                player.getEquipment().updateEquipment(Equipment.RING);
                player.getActionSender().sendMessage("Your ring of wealth smashes.");
                player.getAccount().removeVar("row_uses");
            }
        }



        for (int i = 0; i < amount; i++) {
            returnAmt[i] = randomLoot(getRarity(bonus));
        }

        return returnAmt;
    }
}
