package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/03/14
 * Time: 02:03
 */
public class PKHighscores implements RecurringTickEvent {
    private static String[] pkers = new String[30];

    static {
        //Core.submitTask(new PKHighscores());
    }

    private int last_update = 0;

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        if ((Core.timeSince(last_update) < Core.getTicksForMinutes(5)) && (last_update != 0)) {
            return;
        }

        last_update = Core.currentTime;
        World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
            @Override
            public boolean compute() {
                ResultSet r =
                    World.getWorld().getDatabaseLink().query(
                            "SELECT kills, deaths, username from `user` where usergroupid !='6' and usergroupid !='8'  order by kills desc LIMIT 30");
                int index    = 0;
                int u_length = 12;

                try {
                    while (r.next()) {
                        StringBuilder builder = new StringBuilder();

                        pkers[index++] = r.getString("username") + " - @whi@" + r.getInt("kills") + " kills";
                    }

                    r.close();
                } catch (Exception ee) {
                    ee.printStackTrace();
                }
                return true;
            }
        });
    }

    /**
     * Method isWithinTop10
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean isWithinTop10(Player player) {
        try {
            for (int i = 0; i < 10; i++) {
                if ((pkers[i] != null)
                        && pkers[i].substring(0, pkers[i].indexOf("-") - 1).equalsIgnoreCase(player.getUsername())) {
                    return true;
                }
            }

            return false;
        } catch (Exception ee) {
            return false;
        }
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void render(Player player) {
        player.getActionSender().changeLine("Top 30 PKer's", 6399);

        for (int i = 0; i < 50; i++) {
            if ((i < 10) && (pkers[i] != null)) {
                player.getActionSender().changeLine(pkers[i], 6402 + i);
            } else if ((i > 10) && (i < 30) && (pkers[i] != null)) {
                player.getActionSender().changeLine(pkers[i], 8578 + (i - 10));
            } else {
                player.getActionSender().changeLine("", 8578 + (i - 10));
            }
        }

        player.getWindowManager().showWindow(6308);
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;
    }
}
