package net.tazogaming.hydra.game.leaderboard;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/10/14
 * Time: 01:10
 */
public class BoardBackgroundTask extends BackgroundServiceRequest {

    // 5 minutes
    public static final long UPDATE_PERIOD = 1000 * 60 * 5;

    /** connection made: 14/10/02 **/
    private MysqlConnection connection;

    /**
     * Method compute
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private long lastChange;

    /**
     * Constructs ...
     *
     */
    public BoardBackgroundTask() {
        connection = World.getWorld().createDatabaseLink();
    }

    /**
     * Method compute
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean compute() {
        if ((System.currentTimeMillis() - lastChange > UPDATE_PERIOD) || (lastChange == 0)) {
            lastChange = System.currentTimeMillis();

            try {
                for (AbstractLeaderBoardPopulator board
                        : AbstractLeaderBoardPopulator.LEADERBOARDS) {
                    board.rebuild(connection);
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        }

        return false;
    }
}
