package net.tazogaming.hydra.game.leaderboard;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.sql.ResultSet;

import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/10/14
 * Time: 01:06
 */
public class WhitePortalLeaderboard extends AbstractLeaderBoardPopulator {

    /**
     * Method rebuild
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param connection
     */
    @Override
    public void rebuild(MysqlConnection connection) {
        ResultSet result =
                connection.query(
                        "SELECT whitekills, whitedeaths, whitekills / whitedeaths as kdr, name FROM members ORDER by whitekills DESC LIMIT 50");
        String[] data = new String[50];

        try {
            int dataCaret = 0;

            while (result.next()) {
                data[dataCaret++] = Text.DARK_RED(result.getString("name")) + Text.BLACK("  - kills: ") + Text.BLUE("" + result.getInt("whitekills"))
                        + Text.BLACK("  deaths: ")
                        + Text.BLUE(Integer.toString(result.getInt("whitedeaths")) + Text.BLACK("  kdr:")
                        + Text.BLUE(""+result.getDouble("kdr")));
            }

            result.close();
            setString(data);

            List<String> dataList = new LinkedList<String>();

            dataList.add("White portal Leaderboard");

            dataList.add("");
            dataList.add("--- Leaderboards update every 5 minutes ------");
            dataList.add("");

            for(int i = 0; i < data.length; i++){
                if(data[i] != null && data[i].length() > 0) {
                    dataList.add(data[i]);
                    dataList.add("<str>                                                                           ");
                }
            }

            World.getWorld().getScriptManager().updateVariable("whitelboard", dataList);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}
