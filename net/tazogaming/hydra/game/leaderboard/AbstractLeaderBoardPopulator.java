package net.tazogaming.hydra.game.leaderboard;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.mysql.MysqlConnection;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/10/14
 * Time: 00:54
 */
public abstract class AbstractLeaderBoardPopulator {

    /** leaderBoardString made: 14/10/02 **/
    private String[] leaderBoardString = new String[100];

    protected void reset() {
        leaderBoardString = new String[100];
    }

    protected void setString(String[] data) {
        leaderBoardString = data;
    }

    /**
     * Method getLeaderBoardString
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getLeaderBoardString() {
        return leaderBoardString;
    }


    /**
     * Method rebuild
     * Created on 14/10/02
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param connection
     */
    public abstract void rebuild(MysqlConnection connection);

public static AbstractLeaderBoardPopulator[] LEADERBOARDS = {new WildernessKillsLeaderboard(), new RedPortalLeaderboard(), new WhitePortalLeaderboard()};
}


