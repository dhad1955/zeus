package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueEntry;
import net.tazogaming.hydra.game.minigame.pkleague.LeagueTable;
import net.tazogaming.hydra.game.questing.Quest;
import net.tazogaming.hydra.game.skill.construction.RoomConfig;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/12/2014
 * Time: 04:15
 */
public class CompletionistCape {
    public static CompletionistCapeRequirement[] requirements;

    static {
        CompletionistCapeRequirement max_skills = new CompletionistCapeRequirement("Reach 99 in all skills") {
            @Override
            boolean hasCompleted(Player player) {
                for (int i = 0; i < 24; i++) {
                    if (player.getMaxStat(i) < 99) {
                        return false;
                    }
                }

                return true;
            }
        };


        CompletionistCapeRequirement dung = new CompletionistCapeRequirement("Reach 120 dungeoneering") {
            @Override
            boolean hasCompleted(Player player) {
                return player.getMaxStat(24) == 120;
            }
        };
        CompletionistCapeRequirement achievement_percent =
            new CompletionistCapeRequirement("Complete 70% of achievements") {
            @Override
            boolean hasCompleted(Player player) {
                int perc = (int)GameMath.getPercentFromTotal(player.getAchievementsCompletedTotal(),
                               Achievement.ACHIEVEMENTS.length);

                return perc >= 70;
            }
        };
        CompletionistCapeRequirement quest_percent = new CompletionistCapeRequirement("Complete all possible quests") {
            @Override
            boolean hasCompleted(Player player) {
                int amt            = 0;


                for(Quest c : Quest.getQuests().values()){
                    if(c.isInUse())
                    {
                        amt++;

                    }
                }

                int totalCompleted = 0;

                for (int i = 0; i < 25; i++) {
                    if (player.quest_completed(i) && Quest.getQuests().containsKey(i)) {
                        totalCompleted++;
                    }
                }
                    return GameMath.getPercentFromTotal(totalCompleted, amt) > 90;
            }
        };
        final CompletionistCapeRequirement play_time =
            new CompletionistCapeRequirement("Have at least 35 days playtime") {
            @Override
            boolean hasCompleted(Player player) {
                int time  = player.getAccount().getPlayTime();
                int mins  = (Core.getSecondsForTicks(time)) / 60;
                int hours = mins / 60;
                int days  = hours / 24;

                return days > 40;
            }
        };
        CompletionistCapeRequirement max_xp =
            new CompletionistCapeRequirement("Get at least 500m xp in any non-combat skill") {
            @Override
            boolean hasCompleted(Player player) {
                for (int i = 6; i < 24; i++) {
                    if (player.getExps()[i] >= 500000000) {
                        return true;
                    }
                }

                return false;
            }
        };
        CompletionistCapeRequirement house_kalphite =
            new CompletionistCapeRequirement("Have a kalphite queen head in your house") {
            @Override
            boolean hasCompleted(Player player) {

                // 13490
                if (player.getAccount().getHouseLayout() != null) {
                    for (int i = 0; i < player.getAccount().getHouseLayout().length; i++) {
                        for (int k = 0; k < player.getAccount().getHouseLayout()[i].length; k++) {
                            for (int z = 0; z < player.getAccount().getHouseLayout()[i][k].length; z++) {
                                if (player.getAccount().getHouseLayout()[i][k][z] != null) {
                                    RoomConfig config = player.getAccount().getHouseLayout()[i][k][z];

                                    if (config.containsNode(13487)) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }

                return false;
            }
        };
        CompletionistCapeRequirement house = new CompletionistCapeRequirement("Have a big-shark in your house") {
            @Override
            boolean hasCompleted(Player player) {

                // 13490
                if (player.getAccount().getHouseLayout() != null) {
                    for (int i = 0; i < player.getAccount().getHouseLayout().length; i++) {
                        for (int k = 0; k < player.getAccount().getHouseLayout()[i].length; k++) {
                            for (int z = 0; z < player.getAccount().getHouseLayout()[i][k].length; z++) {
                                if (player.getAccount().getHouseLayout()[i][k][z] != null) {
                                    RoomConfig config = player.getAccount().getHouseLayout()[i][k][z];

                                    if (config.containsNode(13490)) {
                                        return true;
                                    }
                                }
                            }
                        }
                    }
                }

                return false;
            }
        };

        CompletionistCapeRequirement void_set =
            new CompletionistCapeRequirement("Have a all void pieces in your bank") {
            @Override
            boolean hasCompleted(Player player) {
                return ensureBank(player, 8839, 8840, 8841, 8842, 11663, 11664, 11665);
            }
        };
        CompletionistCapeRequirement slayer_tasks = new CompletionistCapeRequirement("Complete 200 slayer tasks") {
            @Override
            boolean hasCompleted(Player player) {
                return player.achievementComplete(Achievement.SLAYER_MASTER);
            }
        };

        CompletionistCapeRequirement chaotics = new CompletionistCapeRequirement("Own every chaotic") {
            @Override
            boolean hasCompleted(Player player) {
                return ensureBank(player, 18349, 18353, 18351, 18357, 18359);

            }
        };

        CompletionistCapeRequirement lunar = new CompletionistCapeRequirement("Own a full lunar set") {
            @Override
            boolean hasCompleted(Player player) {
                return ensureBank(player, 9096, 9097, 9098, 9100);

            }
        };
        CompletionistCapeRequirement f_caves = new CompletionistCapeRequirement("Complete hard fight-caves") {
            @Override
            boolean hasCompleted(Player player) {
                return player.getAccount().hasVar("fight_caves") && player.getAccount().getInt32("fight_caves") == 2;

            }
        };
        CompletionistCapeRequirement clues = new CompletionistCapeRequirement("Complete 80 clue scrolls") {
            @Override
            boolean hasCompleted(Player player) {
                return player.getAccount().hasVar("cluecount") && player.getAccount().getInt32("cluecount") >= 80;

            }
        };


        CompletionistCapeRequirement cw_games = new CompletionistCapeRequirement("Participate in 10 castle wars games") {
            @Override
            boolean hasCompleted(Player player) {
                return player.getAccount().hasVar("cw_games") && player.getAccount().getInt32("cw_games") >= 10;

            }
        };

        CompletionistCapeRequirement cw_captures = new CompletionistCapeRequirement("Capture a castlewar flag") {
            @Override
            boolean hasCompleted(Player player) {
                return player.getAccount().hasVar("cw_captures") && player.getAccount().getInt32("cw_captures") >= 1;
            }
        };


        CompletionistCapeRequirement c_key = new CompletionistCapeRequirement("Open 50 crystal key chests") {
            @Override
            boolean hasCompleted(Player player) {
                return player.getAccount().hasVar("c_keys") && player.getAccount().getInt32("c_keys") >= 50;
            }
        };


        CompletionistCapeRequirement t_demons = new CompletionistCapeRequirement("Kill 100 tormented demons at mage bank") {
            @Override
            boolean hasCompleted(Player player) {
                return player.getAccount().hasVar("t_dklz") && player.getAccount().getInt32("t_dklz") >= 100;
            }
        };

        final CompletionistCapeRequirement gw_bosses = new CompletionistCapeRequirement("Kill 50 godwars bosses (Mix + match)") {
            @Override
            boolean hasCompleted(Player player) {
                int c = 0;
                int[] gw_bosses = new int[] {6222, 6260, 6247, 6203};
                for(Integer i : player.getKillCounts().keySet()){
                   for(int k = 0; k < gw_bosses.length; k++)
                       if(gw_bosses[k] == i)
                           c+= player.getKillCounts().get(i);
                }
                return c  >= 50;
            }
        }        ;




        CompletionistCapeRequirement[] REQUIREMENTS = {
            max_skills, max_xp, achievement_percent, quest_percent,  house, house_kalphite, slayer_tasks,
            void_set,  chaotics, f_caves, lunar,dung, clues,cw_games, cw_captures, t_demons, c_key,  gw_bosses
        };



        CompletionistCape.requirements = REQUIREMENTS;
    }

    /**
     * Method getMenu
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static List<String> getMenu(Player player) {
        List<String> lines = new ArrayList<String>();

        for (CompletionistCapeRequirement req : requirements) {
            if (req.hasCompleted(player)) {
                lines.add("<str>" + req.getName());
            } else {
                lines.add(req.getName());
            }
        }

        return lines;
    }

    /**
     * Method showStats
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param data
     */
    public static void showStats(Player player, List<String> data) {
        player.getGameFrame().openWindow(275);
        player.getGameFrame().sendString("Completionist cape requirements", 275, 2);

        int cur = 16;

        for (String req : data) {
            player.getGameFrame().sendString(req, 275, cur++);
        }

        for (; cur < 100; cur++) {
            player.getGameFrame().sendString("", 275, cur);
        }
    }

    /**
     * Method canWear
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean canWear(Player player) {
        for (CompletionistCapeRequirement req : requirements) {
            if (!req.hasCompleted(player)) {
                return false;
            }
        }

        return true;
    }


    public static int getCompletionPercent(Player player){
        int completed = getTotalCompleted(player);

        return (int)GameMath.getPercentFromTotal(completed, requirements.length);
    }

    public static int getTotalCompleted(Player player){
        int completed = 0;
        for(CompletionistCapeRequirement requirement : requirements) {
            if(requirement.hasCompleted(player))
                completed ++;
        }
        return completed;
    }

    private static boolean ensureBank(Player player, int... items) {
        for (Integer i : items) {
            if (!player.getBank().hasItem(i, 1)) {
                return false;
            }
        }

        return true;
    }
}


/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/12/06
 * @author         Daniel Hadland
 */
abstract class CompletionistCapeRequirement {

    /** name made: 14/12/06 */
    private String name;

    CompletionistCapeRequirement(String name) {
        this.name = name;
    }

    /**
     * Method getName
     * Created on 14/12/15
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    String getName() {
        return name;
    }

    /**
     * Method hasCompleted
     * Created on 14/12/06
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    abstract boolean hasCompleted(Player player);
}
