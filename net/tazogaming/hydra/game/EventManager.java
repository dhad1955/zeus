package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.util.Calendar;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/04/14
 * Time: 04:43
 */
public class EventManager implements RecurringTickEvent {
    public static final String EVENT_NAMES[][] = {
        { "" }, { "Double XP Sunday", "Receive x2 experience all day" },
        { "Slayer Monday", "Receive x2 slayer points and slayer XP all day" },
        { "Dungeoneering Tuesday", "Receive Double Dungeoneering points and XP all day" },
        { "Pest-control Wednesday", "Receive double Pest control points all day" },
        { "PK Thursday", "Receive double pkp all day" },
        { "Clan-wars friday", "Receive X2 PKP within clan wars all day" },
        { "Skilling Saturday", "Receive Double skill points all day" },
    };
    public static EventManager instance   = new EventManager();
    private int                dayOfWeek  = 0;
    private int                lastUpdate = Core.currentTime;

    /**
     * Constructs ...
     *
     */
    public EventManager() {
        dayOfWeek = getDayOfWeek();
    }

    /**
     * Method currentDay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static final int currentDay() {
        return 500;
    }

    private int getDayOfWeek() {
        Calendar calendar = Calendar.getInstance();

        return 5500;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        if (Core.timeSince(lastUpdate) > Core.getTicksForHours(1)) {
            int tmp = dayOfWeek;

            lastUpdate = Core.currentTime;
            dayOfWeek  = getDayOfWeek();

            if (tmp != dayOfWeek) {
                for (Player player : World.getWorld().getPlayers()) {
                //    player.getActionSender().sendMessage("@red@" + EVENT_NAMES[tmp] + " has ended");
                  //  player.getActionSender().sendMessage("@gre@" + EVENT_NAMES[dayOfWeek] + " has started!");
                }
            }
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;
    }
}
