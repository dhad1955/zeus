package net.tazogaming.hydra.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/12/13
 * Time: 21:59
 */
public class StaticLootTable {
    private ArrayList<Loot> lootList = new ArrayList<Loot>();

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     * @param amt
     */
    public void add(int itemid, int amt) {
        lootList.add(new Loot(itemid, amt));
    }

    /**
     * Method removeAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     */
    public void removeAll(int itemid) {
        for (int i = 0; i < lootList.size(); i++) {
            if (lootList.get(i).getItemID() == itemid) {
                lootList.remove(i);
            }
        }
    }

    /**
     * Method getTotal
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public int getTotal(int itemId) {
        int i = 0;

        for (Loot l : lootList) {
            if (l.getItemID() == itemId) {
                i += l.getAmount();
            }
        }

        return i;
    }

    /**
     * Method getLoot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     *
     * @return
     */
    public Loot[] getLoot(int amt) {
        Loot[] toGet = new Loot[1];

        toGet[0] = lootList.get(GameMath.rand3(lootList.size()));

        return toGet;
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return lootList.size();
    }
}
