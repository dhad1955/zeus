package net.tazogaming.hydra.game.pets;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Created by Danny on 10/09/2015.
 */
public class BossPet {
    public BossPet(int npcId, int itemId, int petId) {
        this.npcId = npcId;
        this.itemId = itemId;
        this.petId = petId;
    }

    public static final int RARE = 0, ULTRA_RARE = 1;

    private int npcId;
    private int rarity = 0;

    public int getRarity() {
        return rarity;
    }

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public int getKillsToUnlock() {
        return killsToUnlock;
    }

    public void setKillsToUnlock(int killsToUnlock) {
        this.killsToUnlock = killsToUnlock;
    }

    private int killsToUnlock = 0;


    public boolean isUnlocked(Player player) {
        if(!player.getKillCounts().containsKey(this.npcId))
            return false;

        if(player.getKillCounts().get(this.npcId) > this.killsToUnlock){
            if(rarity == RARE){
                return GameMath.rand3(200) == 25;
            }else if(rarity == ULTRA_RARE)
                return GameMath.rand3(350) == 24;

        }
        return false;
    }


    public int getNpcId() {
        return npcId;
    }

    public void setNpcId(int npcId) {
        this.npcId = npcId;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public int getPetId() {
        return petId;
    }

    public void setPetId(int petId) {
        this.petId = petId;
    }

    private int itemId;
    private int petId;

}
