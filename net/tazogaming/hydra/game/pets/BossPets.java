package net.tazogaming.hydra.game.pets;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.game.minigame.bosstournament.BossTournament;
import net.tazogaming.hydra.io.Line;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.script.runtime.expr.IntegerNode;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Danny on 10/09/2015.
 */
public class BossPets {

    private static Map<Integer, BossPet> pets = new HashMap<Integer, BossPet>();


    public static void sendNotification(BossPet pet, Player player){
        for(Player p: World.getWorld().getPlayers()){

            if(p.getGameFrame().getRoot() == 548){
                p.getActionSender().sendMessage("_________________________________________________________________________________");
                p.getActionSender().sendMessage("<img=5>"+player.getUsername()+" has just received a</col> <col=0033CC>"+ NpcDef.FOR_ID(pet.getNpcId()).getName()+" boss</col>pet drop!");
                p.getActionSender().sendMessage("_________________________________________________________________________________");

            }else{
                p.getActionSender().sendMessage("_________________________________________________________________________________");
                p.getActionSender().sendMessage("<img=5>"+player.getUsername()+" has just received a <col=FFFF00>"+ NpcDef.FOR_ID(pet.getNpcId()).getName()+" boss</col> <col=99FF66>pet drop!");
                p.getActionSender().sendMessage("_________________________________________________________________________________");


            }
        }
    }

    public static final void load() {
        String path = "config/world/npc/bosspets.cfg";
        pets.clear();
        TextFile file = TextFile.fromPath(path);
        for(Line line : file) {
            if(line.getData().startsWith("pet")){
                String[] split = line.getData().split(" ");
                int itemId = Integer.parseInt(split[2]);
                int npcId = Integer.parseInt(split[3]);
                int bossNpc = Integer.parseInt(split[4]);
                int killsToUnlock = Integer.parseInt(split[5]);
                int rarity = split[6].equalsIgnoreCase("rare") ? BossPet.RARE : BossPet.ULTRA_RARE;
                BossPet pet = new BossPet(bossNpc, itemId, npcId);
                pet.setRarity(rarity);
                pet.setKillsToUnlock(killsToUnlock);
                pets.put(itemId, pet);
            }
        }
    }

    public static BossPet getByItem(int itemID){
        if(!pets.containsKey(itemID))
            return null;
        return pets.get(itemID);
    }

    public static BossPet getByBoss(int bossId){
        for(BossPet pet : pets.values())
            if(pet.getNpcId() == bossId)
                return pet;
        return null;
    }
    public static BossPet getByPet(int petId){
        for(BossPet pet : pets.values())
            if(pet.getPetId() == petId)
                return pet;
        return null;
    }

}
