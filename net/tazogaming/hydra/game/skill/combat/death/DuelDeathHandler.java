package net.tazogaming.hydra.game.skill.combat.death;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.minigame.duel.Duel;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 23:44
 */
public class DuelDeathHandler implements DeathHandler {
    private Duel thisDuel;

    /**
     * Constructs ...
     *
     *
     * @param d
     */
    public DuelDeathHandler(Duel d) {
        thisDuel = d;
    }

    /**
     * Method handleDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killed
     * @param killedBy
     */
    @Override
    public void handleDeath(Killable killed, Killable killedBy) {
        if ((killed instanceof NPC) || (killedBy instanceof NPC)) {    // just incase
            new DefaultDeathHandler().handleDeath(killed, killedBy);

            return;
        }

        Player victim = killed.getPlayer();
        Player target = thisDuel.getOtherPlayer(killed.getPlayer());

        victim.getCombatAdapter().reset();
        target.getCombatAdapter().reset();
        victim.getProjectiles().clear();
        target.getProjectiles().clear();
        target.clearProjectiles();
        victim.clearProjectiles();
        victim.invalidateDamage();
        target.invalidateDamage();
        thisDuel.restore_stats(victim);
        thisDuel.restore_stats(target);
        thisDuel.give_items_to_winner(target);
        thisDuel.setStatus(Duel.STATUS_OVER);
        this.thisDuel.unlink(target);
        this.thisDuel.unlink(victim);
        target.getActionSender().sendMessage("You are victorious!");
        victim.getActionSender().sendMessage("You have been defeated.");
        this.thisDuel.showSpoils(target);
    }
}
