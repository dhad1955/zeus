package net.tazogaming.hydra.game.skill.combat.death;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.DegradeableItem;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.player.GraveStone;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.pkleague.PKTournament;
import net.tazogaming.hydra.game.minigame.pkraffle.PKRaffle;
import net.tazogaming.hydra.game.minigame.treasure_trails.ClueManager;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.game.ui.shops.ShopManager;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 28/09/13
 * Time: 01:19
 * To change this template use File | Settings | File Templates.
 */
public class DefaultDeathHandler implements DeathHandler {

           public static final int BUYBACK_CHAOTICS[] = {
            18349, 18351, 18353, 18355, 18357, 18359, 19669, 18335, 18346, 18363, 18361, 18347, 18333, 18359, 18365, 18367, 18369, 18371, 18373, 18341, 18342, 18338, 18339
    };



    public static final int[] POINT_PENALTY_ITEMS = {
          20163,20167, 20165, 20169,20165, 20155, 20157, 20151, 20153, 20149, 20147, 20136, 20139, 20141, 20142, 20143, 20144, 20145, 20146, 20147, 13740};
    /**
     * Method handle_pvp_kill
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killedBy
     * @param killed
     */
    private static void handle_pvp_kill(Player killedBy, Player killed) {


        String killedName = killed.getUsername();

        String[] randomTaunt = {killedName+" has won a free ticket to lumbridge",
                                "Who did "+killedName+" think they were challenging you?",
                                killedName+" was no match for you",
                                "You have defeated "+killedName};
        String killedByName = killedBy.getUsername();

        String[] randomTauntKilledBy = {killedByName+" has defeated you", "You were no match for "+killedByName, killedByName+" has sent you to lumbridge, GF."};

        killed.getActionSender().sendMessage(randomTauntKilledBy[GameMath.rand3(randomTauntKilledBy.length)]);
        killedBy.getActionSender().sendMessage(randomTaunt[GameMath.rand3(randomTaunt.length)]);

        if ((killedBy.getUID() == killed.getUID()) && (killedBy.getRights() < Player.ADMINISTRATOR)) {
            killedBy.getActionSender().sendMessage("Kill blocked, cheat detected. :" + killedBy.getUsername() + " "
                    + killed.getUsername());

            return;
        }

        if (killed.getEquipment().getTotalWearing() < 4) {
            return;
        }

        int count = killedBy.getAccount().getKillCount(killed.getUID());


        int     MAX_KILLS    = 5;
        boolean isWithinEdge = (killedBy.getX() >= 3025) && (killedBy.getX() <= 3127)
                               && (Combat.getWildernessLevel(killedBy) <= 15);

        if (!isWithinEdge) {
            MAX_KILLS = 3;
        }

        if (killedBy.getRights() == Player.DONATOR) {
            MAX_KILLS++;
        }

        if (killedBy.getRights() == Player.PLATINUM) {
            MAX_KILLS += 2;
        }

        if (killedBy.getRights() == Player.PATRON) {
            MAX_KILLS += 3;
        }

        if (count > MAX_KILLS) {
            killedBy.getActionSender().sendMessage(
                "Pkp blocked, you've killed this person to many times in this time space.");

            if (!isWithinEdge) {
                killedBy.getActionSender().sendMessage(
                    "Move to a more populated area like edgeville to increase this limit");
            }

            return;
        }

        int points = 3;

        if (!killedBy.getAccount().hasVar("pk_streak")) {
            killedBy.getAccount().setSetting("pk_streak", 1, true);
        } else {
            boolean rag = false;
            for(int i = 0; i < POINT_PENALTY_ITEMS.length; i++)
                if(killedBy.getEquipment().contains(POINT_PENALTY_ITEMS[i]))
                    rag = true;

            if(!rag) {
                killedBy.getAccount().increaseVar("pk_streak", 1);
            } else {
                killedBy.getActionSender().sendMessage("Your PK Streak wont be affected as you are wearing gear that is classed");
                killedBy.getActionSender().sendMessage("as overpowered.");
            }
                points += Math.floor(killedBy.getAccount().getInt32("pk_streak") / 3);

            int streak = killedBy.getAccount().getInt32("pk_streak");

            if (streak > 1) {
                killedBy.getActionSender().sendMessage("You are now on a " + streak + " kill streak");
            }

            if (streak == 10) {
                killedBy.addAchievementProgress2(Achievement.KILLING_FRENZY, 1);
                World.globalMessage(
                    Text.BLUE(
                        killedBy.getUsername()
                        + " is on a killing frenzy in the wilderness! By reaching a 10 kill streak!!"));
                killedBy.getActionSender().sendMessage("Extra point awarded!");
            } else if (streak == 15) {
                killedBy.addAchievementProgress2(Achievement.RUNNING_RIOT, 1);
                World.globalMessage(
                    Text.BLUE(
                        killedBy.getUsername()
                        + " is on a running riot in the wilderness! by reaching a 15 KillStreak!"));
                killedBy.getActionSender().sendMessage("Extra 2 points rewarded!");
                points += 2;
            } else if (streak == 20) {
                killedBy.addAchievementProgress2(Achievement.RAMPAGE, 1);
                killedBy.getActionSender().sendMessage("Extra 5 points awarded for streak!");
                World.globalMessage(Text.BLUE(killedBy.getUsername()
                                              + " is on a massacre in the wilderness by reaching a 20 Killstreak"));
            } else if (streak == 50) {
                killedBy.addAchievementProgress2(Achievement.UBER_RAMPAGE, 1);
            }

            if (streak > killedBy.getAccount().getKillStreak()) {
                killedBy.getActionSender().sendMessage("You have reached a new highest kill streak! well done!");
                killed.getAccount().setKillStreak((byte) streak);
            }
        }

        if (killed.getAccount().hasVar("pk_streak") && (killed.getAccount().getInt32("pk_streak") > 0)) {
            int streak = killed.getAccount().getInt32("pk_streak");

            if (streak > 5) {
                killedBy.getActionSender().sendMessage(
                    Text.DARK_ORANGE("You have ended your opponents streak and have been awarded bonus points"));
                points += (streak / 2);
            }

            if (streak > 10) {
                World.globalMessage(Text.LIGHT_ORANGE(killedBy.getUsername() + " has just ended "
                        + killed.getUsername() + " " + streak + " pk streak "));
            }

            killed.getActionSender().sendMessage("You've lost your pk streak");
            killed.getAccount().setSetting("pk_streak", 0, true);
        }

        killedBy.getAccount().registerKilled(killed);

        boolean noBonus = false;

        if (Combat.getWildernessLevel(killed) > 15) {
            points += 2;
            killedBy.getActionSender().sendMessage("Deep wild bonus extra 2 points!");
        }


        if(killedBy.isInMultiArea()){
            points *= 1.60;
        }
        if ((killedBy.getRights() == Player.PLATINUM) || (killedBy.getRights() == Player.DONATOR)) {
            points += 1;
        }

        if (killedBy.getBHTarget() == killed) {

            int bh_points = 1;
            for(Integer penalizedItem : POINT_PENALTY_ITEMS) {
                if (killedBy.getEquipment().contains(penalizedItem) || killedBy.getInventory().hasItem(penalizedItem, 1)) {
                    bh_points = 0;
                }
            }

            if(bh_points != 0) {
                killedBy.getActionSender().sendMessage(Text.BLUE("You have killed your bounty hunter target"));
                killedBy.getActionSender().sendMessage("You have been awarded 1m gp for killing your target");
                killedBy.addPoints(Points.BH_POINTS, 1);
                killedBy.getActionSender().sendMessage("You now have " + killedBy.getPoints(Points.BH_POINTS) + " Bounty hunter points");
                killedBy.getBank().insert(995, 1000000);
                if (!killedBy.getAccount().hasVar("bh_kills"))
                    killedBy.getAccount().setSetting("bh_kills", 0, true);

                killedBy.getAccount().increaseVar("bh_kills", 1);
            } else {
                killedBy.getActionSender().sendMessage("Bounty hunter kill blocked [Rag gear detected]");
            }
        }

        if (killedBy.getRights() == Player.PATRON) {
            points += 2;
        }

        if (killedBy.isInArea(ClanWar.RED_PORTAL_PVP)) {
            points = 0;
        }

        if (killedBy.getPlayer().getEquipment().isHybrid() && (killedBy.getMaxStat(5) > 80) && Combat.getWildernessLevel(killedBy) >= 20) {
            points *= 2;
            killedBy.addAchievementProgress2(182, 1);
            killedBy.getActionSender().sendMessage(Text.RED("HYBRID BONUS X2 PKP"));
        }

        if(Combat.getWildernessLevel(killedBy) >= 40) {
            World.globalMessage(killedBy.getUsername()+" has just earned x2 pkp for pking in deep wild");
            points *= 2;
        }



        if ((points > 0)) {
            if (Config.doublePk) {
                points *= 2;
            }else if(killedBy.getTimers().timerActive(TimingUtility.DOUBLE_PK_TIMER))
                points *= 2;


            killedBy.addAchievementProgress2(Achievement.PK_NEWBIE, 1);
            killedBy.addAchievementProgress2(Achievement.PK_NOVICE, 1);
            killedBy.addAchievementProgress2(Achievement.PK_ENTHUSIAST, 1);
            killedBy.addAchievementProgress2(Achievement.PK_PROFESSIONAL, 1);
            killedBy.addAchievementProgress2(Achievement.PK_PROFESSIONAL_II, 1);
            killedBy.addAchievementProgress2(Achievement.PK_MASTER, 1);
            killedBy.addAchievementProgress2(Achievement.ELITE_PKER, 1);

            if (killedBy.getCurStat(1) == 1) {
                killedBy.addAchievementProgress2(Achievement.PURE, 1);
                killedBy.addAchievementProgress2(Achievement.PURE_HOBBYIST, 1);
                killedBy.addAchievementProgress2(Achievement.PURE_ADDICT, 1);
            } else if ((killedBy.getCurStat(1) >= 40) && (killedBy.getCurStat(1) < 46)) {
                killedBy.addAchievementProgress2(Achievement.ZERKER, 1);
                killedBy.addAchievementProgress2(Achievement.ZERKER_ADDICT, 1);
                killedBy.addAchievementProgress2(Achievement.ZERKER_HOBBYIST, 1);
            } else if (killedBy.getCurStat(1) >= 70) {
                killedBy.addAchievementProgress2(Achievement.DEF_HOBBYIST, 1);
                killedBy.addAchievementProgress2(Achievement.DEF_LOVER, 1);
                killedBy.addAchievementProgress2(Achievement.ITS_ALL_ABOUT_THE_DEF, 1);
            }
            if(killedBy.getPlayer().getMaxStat(1) == 1 && killedBy.getPlayer().getCombatLevel() <= 90) {
                points *= 2;
                killedBy.getActionSender().sendMessage("X2 PKP For being pure!");
            }
            if (points > 20) {
                points = 20;
            }



            if(killedBy.getEquipment().contains(13730) && !killedBy.isInMultiArea()) {
                killedBy.getActionSender().sendMessage(Text.RED("Your points have been penalized for using divine spirit shield."));

                points = 0;
            }

            for(Integer penalizedItem : POINT_PENALTY_ITEMS)
            {
                if(killedBy.getEquipment().contains(penalizedItem) || killedBy.getInventory().hasItem(penalizedItem, 1)){
                    points = 0;
                    killedBy.getActionSender().sendMessage(Text.RED("Your points have been penalized because you are wearing: "+Item.forId(penalizedItem).getName()));
                    break;
                }
            }
            killedBy.addPoints(Points.PK_POINTS_2, points);
            killed.addAchievementProgress2(Achievement.PK_FAILURE, 1);

            if (killedBy.getMaxStat(1) <= 1) {
                PKTournament.puresLeaderboard.addPoint(killedBy.getPlayer());
            } else if(killedBy.getMaxStat(1) >= 40 && killedBy.getMaxStat(1) <= 46) {
                PKTournament.zerkersLeaderboard.addPoint(killedBy.getPlayer());
            }else {
                PKTournament.mainsLeaderboard.addPoint(killedBy.getPlayer());
            }
        }

        if (!killedBy.getAccount().hasVar("kills")) {
            killedBy.getAccount().setSetting("kills", 0, true);
        }

        if (!killed.getAccount().hasVar("deaths")) {
            killed.getAccount().setSetting("deaths", 0, true);
        }

        killedBy.getAccount().increaseVar("kills", 1);
        killed.getAccount().increaseVar("deaths", 1);

        int setting = killedBy.isInArea(ClanWar.RED_PORTAL_PVP)
                      ? Account.RED_PORTAL
                      : Account.WILDERNESS;

        killedBy.getAccount().registerKillCount(setting);
        killed.getAccount().registerDeathCount(setting);

        int raffleCount = (killedBy.getCurStat(1) < 55)
                          ? 2
                          : 1;

        if ((raffleCount == 1) && (Combat.getWildernessLevel(killedBy.getPlayer()) > 15)) {
            raffleCount += 1;
        }

          raffleCount += 1;

        if (killedBy.getPlayer().getEquipment().isHybrid()) {
            raffleCount += 2;
        }

        int killCount = killedBy.getAccount().getKillCount(killed.getUID());

        if (killCount < 3) {
            for (int i = 0; i < raffleCount; i++) {
                int ticket = PKRaffle.getNextEntry(killedBy.getId());


                PKRaffle.checkEntries();
            }
        }

        killedBy.getActionSender().sendMessage("You received "+raffleCount+" raffle tickets");

        if (killedBy.getAccount().hasVar("pk_boost")) {
            killedBy.getBank().insert(995, 1000000);
            killedBy.getActionSender().sendMessage("[Starter boost]: 1m has been sent to your bank");
            killedBy.getAccount().decreaseVar("pk_boost", 1);

            if (killedBy.getAccount().getInt32("pk_boost") == 0) {
                killedBy.getAccount().removeVar("pk_boost");
            }
        }

        int threshHold = 93;

        if (killedBy.getPlayer().getEquipment().isHybrid()) {
            threshHold = 70;
        }
        if(killedBy.getPlayer().isInMultiArea())
            threshHold -= 10;


        if (Combat.getWildernessLevel(killedBy.getPlayer()) > 50) {
            threshHold *= 0.95;
        }

        if ((Combat.GLOBAL_RANDOM.nextInt(100) > threshHold) &&!killed.isInArea(ClanWar.RED_PORTAL_PVP)) {
            int[] randomArtefacts = {
                14876, 14877, 14878, 14879, 14880, 14881, 14882
            };

            new FloorItem(randomArtefacts[GameMath.rand3(randomArtefacts.length)], 1, killed.getX(), killed.getY(),
                          killed.getHeight(), killedBy);
            killedBy.getActionSender().sendMessage(
                "There is an artifact on the floor, use this artefact on the altar at edgeville to receive gp");

            for (Player pla : World.getWorld().getPlayers()) {
                pla.getActionSender().sendMessage(killedBy.getUsername() + " has just got an artifact drop from PVP!");
            }
        }



        if (!killed.getPlayer().getEquipment().hasLoadout()) {
            killed.getPlayer().getActionSender().sendMessage(
                Text.RED("You do not have a loadout set, use ::setloadout to pre-set one"));
            killed.getPlayer().getActionSender().sendMessage(
                Text.RED("Loadouts will allow you to get your gear back quickly"));
        }
    }

    private static boolean isChaotic(int id) {
        Item item = Item.forId(id);

        if (item != null) {
            return item.getName().toLowerCase().contains("chaotic");
        }

        return false;
    }

    /**
     * Method getChaoticPrice
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getChaoticPrice(Integer id) {
        return ShopManager.getShop(57).priceOf(id);
    }

    /**
     * Method handleDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killed
     * @param killedBy
     */
    @Override
    public void handleDeath(Killable killed, Killable killedBy) {
        Player     pla        = (Player) killed;
        int        keepAmount = 3;
        GraveStone graveStone = null;
        if(killedBy instanceof NPC && ((NPC) killedBy).isSummoned())
            killedBy = killedBy.getNpc().getSummoner();

        if (killedBy instanceof NPC | ((killedBy instanceof Player) && (killedBy == killed))) {
            graveStone = new GraveStone(pla);
        }

        if (pla.getTimers().timerActive(TimingUtility.SKULL_TIMER)) {
            keepAmount = 0;
        }

        if (pla.getPrayerBook().prayerActive(PrayerBook.PROTECT_ITEM)
                || pla.getPrayerBook().curseActive(PrayerBook.CURSE_PROTECT_ITEM)) {
            keepAmount += 1;
        }

        if (pla.getRank() == Player.RANK_ELITE) {
            keepAmount += 1;
        }

        Player droppedTo = (killedBy instanceof Player)
                           ? (Player) killedBy
                           : pla;

        pla.setActionsDisabled(false);
        pla.setCurrentSpecial(10);
        SpecialAttacks.updateSpecial(pla);
        pla.addAchievementProgress2(Achievement.FAILURE, 1);

        if (killedBy instanceof Player) {
            handle_pvp_kill((Player) killedBy, (Player) killed);
        }

        Item[]    allItems        = new Item[28 + 14];
        int[]     allItemAmounts  = new int[28 + 14];
        boolean[] tradeable_flags = new boolean[28 + 14];
        int       index           = 0;

        pla.getTimers().resetTimer(TimingUtility.SKULL_TIMER);
        pla.getAppearance().setChanged(true);
        pla.setHeadIcon(0);

        List<Item> untradeables = new ArrayList<Item>();

        for (int i = 0; i < pla.getInventory().getItems().length; i++) {
            if (pla.getInventory().getItem(i) != null) {
                if (pla.getInventory().getItem(i).isLend()) {
                    pla.getAccount().removeRented(pla.getInventory().getItem(i).getIndex());

                    continue;
                }

                if (!isChaotic(pla.getInventory().getItem(i).getIndex())
                        &&!pla.getInventory().getItem(i).isTradeable()) {
                    untradeables.add(pla.getInventory().getItem(i));

                    continue;
                }

                if (ClueManager.isClue(pla.getInventory().getItem(i).getIndex())) {
                    try {
                        pla.getAccount().removeVar("clue_level");
                    } catch (Exception ee) {}

                    continue;
                }

                allItemAmounts[index] = pla.getInventory().getCount(i);
                allItems[index++]     = pla.getInventory().getItem(i);
            }
        }

        for (int i = 0; i < 14; i++) {
            if (pla.getEquipment().getId(i) != -1) {
                if (Item.forId(pla.getEquipment().getId(i)).isLend()) {
                    pla.getAccount().removeRented(pla.getEquipment().getId(i));

                    continue;
                }



                if (!isChaotic(pla.getEquipment().getId(i)) &&!Item.forId(pla.getEquipment().getId(i)).isTradeable()) {
                    untradeables.add(Item.forId(pla.getEquipment().getId(i)));

                    continue;
                }

                allItemAmounts[index] = pla.getEquipment().getAmount(i);
                allItems[index++]     = Item.forId(pla.getEquipment().getId(i));
            }
        }

        getBestItems(allItems, allItemAmounts);

        ArrayList<Item> returnItems = new ArrayList<Item>();

        for (int i = 0; i < keepAmount; i++) {
            if (allItems[i] != null && !isChaotic(allItems[i].getIndex())) {
                allItemAmounts[i]--;
                returnItems.add(allItems[i]);
                allItems[i] = null;
            }
        }

        for (Item i : untradeables) {
            returnItems.add(i);
        }

        int[] phats = {
            1038, 1040, 1042, 1044, 1046, 1048
        };

        if (pla.isInWilderness()) {
            for (int phat : phats) {
                if (pla.getEquipment().contains(phat)) {
                    pla.addAchievementProgress2(Achievement.HIGH_RISKER, 1);
                }
            }
        }

        for (int i = 0; i < allItems.length; i++) {
            boolean b = false;

            if (allItems[i] != null) {
                if (allItems[i].isDegradeable() && (allItems[i].getDegradeable().getMain() != allItems[i].getIndex())
                        && (allItems[i].getDegradeable().getPolicy() == DegradeableItem.POLICY_DEGRADE)) {
                    try {
                        pla.getAccount().removeDegrading(allItems[i].getIndex());
                        allItems[i]        = Item.forId(allItems[i].getDegradeable().getLast());
                        tradeable_flags[i] = true;
                        b                  = true;
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }
                }

                if (untradeables.contains(allItems[i])) {
                    continue;
                }

                boolean breakLoop = false;

                    for (int k = 0; k < BUYBACK_CHAOTICS.length; k++) {
                        if (allItems[i].getIndex() == BUYBACK_CHAOTICS[k]) {
                            if (pla.getBuyBackShop().getItemCost(BUYBACK_CHAOTICS[k]) != -1) {
                                pla.getBuyBackShop().addItem(BUYBACK_CHAOTICS[k], 1);
                            } else {
                                pla.getBuyBackShop().addNewItem(allItems[i], 1, 40000000);
                            }

                            pla.getActionSender().sendMessage(
                                Text.RED("YOU CAN BUY YOUR CHAOTICS BUY FROM THE DUNGEON MASTER FOR 40M"));
                            breakLoop = true;
                        }
                    }


                if (breakLoop) {
                    continue;
                }
                
                Player victim = (Player) killed;

                // if killed wasn't iron man
                if (victim.getAccount().getIronManMode() == 0) {
                	
                	// if killer is a player
                    if ((killedBy != null) && (killedBy instanceof Player)) {
                    	
                    	// if killer is iron man, give items to victim
                        if (killedBy.getPlayer().getAccount().getIronManMode() > Account.NO_IRONMAN) {
                            droppedTo = victim;
                        }
                    }

                   if(allItems[i] != null)
                    if(isChaotic(allItems[i].getIndex())) continue;;

                    if (allItems[i].isTradeable() || tradeable_flags[i]) {
                        if ((droppedTo == pla) &&!ClanWar.isInClanWars(pla) && (graveStone != null)) {
                            graveStone.pushItem(allItems[i].getIndex(), allItemAmounts[i]);
                        } else {
                            new FloorItem(allItems[i].getIndex(), allItemAmounts[i], killed.getX(), killed.getY(),
                                          pla.getLocation().getHeight(), droppedTo);
                        }
                    } else {
                        if (!ClanWar.isInClanWars(pla) && (graveStone != null)) {
                            graveStone.pushItem(allItems[i].getIndex(), allItemAmounts[i]);
                        } else {
                            if (allItems[i].isTradeable()) {
                                new FloorItem(allItems[i].getIndex(), allItemAmounts[i], killed.getX(), killed.getY(),
                                              pla.getLocation().getHeight(), droppedTo);
                            } else {
                                new FloorItem(allItems[i].getIndex(), allItemAmounts[i], killed.getX(), killed.getY(),
                                              pla.getLocation().getHeight(), pla);
                            }
                        }

                        if (allItems[i].getIndex() == 11235) {
                            pla.getAccount().setSetting("dfs_charges", 0);
                        }
                    }
                }
            }
        }

        pla.getInventory().clear();

        int[] bindedItems      = pla.getEquipment().getBindedItems();
        int   bindedItemsCount = pla.getEquipment().getBindedItemsCount();
        int[] load             = pla.getEquipment().getLoadout();
        int[] loadA            = pla.getEquipment().getLoadoutAmt();

        pla.setEquipment(new Equipment(pla));
        pla.getEquipment().setDungBindedItems(bindedItems, 0);
        pla.getEquipment().setLoadout(load);
        pla.getEquipment().setLoadoutAmt(loadA);

        for (Item i : returnItems) {
            pla.getInventory().addItem(i, 1);
        }

        for (int i = 0; i < 14; i++) {
            pla.getEquipment().updateEquipment(i);
        }

        if (pla.getAccount().hasVar("home_x")) {
            int absX   = pla.getAccount().getVar("home_x").getIntData();
            int absY   = pla.getAccount().getVar("home_y").getIntData();
            int height = pla.getAccount().getVar("home_h").getIntData();

            if (Point.getDistance(pla.getX(), pla.getY(), absX, absY) < 40) {
                pla.teleport(absX, absY, height);
            } else {
                pla.teleport(3222, 3222, 0);
            }
        } else {
            pla.teleport(3222, 3222, 0);
        }

        pla.getPrayerBook().resetAll();
        pla.getAppearance().setChanged(true);
    }

    /**
     * Method getItemsOnDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static Item[] getItemsOnDeath(Player pla) {
        Item[] allItems       = new Item[28 + 14];
        int[]  allItemAmounts = new int[28 + 14];
        int    index          = 0;
        int    keepAmount     = 3;

        if (pla.getTimers().timerActive(TimingUtility.SKULL_TIMER)) {
            keepAmount = 0;
        }

        if (pla.getPrayerBook().prayerActive(PrayerBook.PROTECT_ITEM)
                || pla.getPrayerBook().curseActive(PrayerBook.CURSE_PROTECT_ITEM)) {
            keepAmount++;
        }

        for (int i = 0; i < pla.getInventory().getItems().length; i++) {
            if (pla.getInventory().getItem(i) != null) {
                allItemAmounts[index] = pla.getInventory().getCount(i);
                allItems[index++]     = pla.getInventory().getItem(i);
            }
        }

        for (int i = 0; i < 14; i++) {
            if (pla.getEquipment().getId(i) != -1) {
                allItemAmounts[index] = pla.getEquipment().getAmount(i);
                allItems[index++]     = Item.forId(pla.getEquipment().getId(i));
            }
        }

        getBestItems(allItems, allItemAmounts);

        Item[] returnItems  = new Item[keepAmount];
        int[]  returnItemsC = new int[keepAmount];

        for (int i = 0; i < keepAmount; i++) {
            if (allItems[i] != null) {
                allItemAmounts[i]--;
                returnItems[i]  = allItems[i];
                returnItemsC[i] = 1;
                allItems[i]     = null;
            }
        }

        // remove n ull values
        Item[] newItems  = new Item[allItems.length];
        int[]  newItemsC = new int[allItemAmounts.length];
        int    ind       = 0;

        for (int i = 0; i < allItems.length; i++) {
            if (allItems[i] != null) {
                newItemsC[ind]  = allItemAmounts[i];
                newItems[ind++] = allItems[i];
            }
        }

        return returnItems;
    }

    /**
     * Method getBestItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param droppedItems
     * @param amounts
     *
     * @return
     */
    public static Item[] getBestItems(Item[] droppedItems, int[] amounts) {
        for (int j = 0; j < droppedItems.length - 1; j++) {
            boolean isSorted = true;

            for (int i = 1; i < droppedItems.length - j; i++) {
                if ((droppedItems[i] != null) && (droppedItems[i].getExchangePrice() > droppedItems[i - 1].getExchangePrice())) {
                    Item tmpItem   = droppedItems[i];
                    int  tmpAmount = amounts[i];

                    droppedItems[i]     = droppedItems[i - 1];
                    amounts[i]          = amounts[i - 1];
                    droppedItems[i - 1] = tmpItem;
                    amounts[i - 1]      = tmpAmount;
                    isSorted            = false;
                }
            }

            if (isSorted) {
                break;
            }
        }

        return droppedItems;
    }
}
