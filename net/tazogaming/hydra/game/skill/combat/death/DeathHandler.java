package net.tazogaming.hydra.game.skill.combat.death;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.combat.Killable;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 28/09/13
 * Time: 01:17
 * To change this template use File | Settings | File Templates.
 */
public interface DeathHandler {

    /**
     * Method handleDeath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killed
     * @param killedBy
     */
    public void handleDeath(Killable killed, Killable killedBy);
}
