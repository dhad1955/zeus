package net.tazogaming.hydra.game.skill.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.player.GraveStone;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.minigame.SkeletalHorror;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWarRules;
import net.tazogaming.hydra.game.minigame.deathtower.DeathTowerGame;
import net.tazogaming.hydra.game.minigame.duel.Duel;
import net.tazogaming.hydra.game.minigame.fp.FightPitsGame;
import net.tazogaming.hydra.game.minigame.pc.PCGame;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Class description Hydrascape 639 Game server Copyright (C) Tazogaming 2014
 *
 *
 * @version Enter version here..., 14/08/18
 * @author Daniel Hadland
 */
public class PlayerCombatAdapter extends CombatAdapter<Player> {

	/** ourPlayer made: 14/09/30 */
	private Player ourPlayer;

	/**
	 * Constructs ...
	 *
	 *
	 * @param pla
	 */
	public PlayerCombatAdapter(Player pla) {
		super(pla);
		ourPlayer = pla;
	}

	/**
	 * Method onTargetAcquired Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	@Override
	public void onTargetAcquired() {
		if (getFightingWith() instanceof NPC) {
			if (ourPlayer.getCurrentFamiliar() != null) {
				ourPlayer.getCurrentFamiliar().getControllerScript().do_trigger(Block.ON_MASTER_ATTACKED_BY,
						getFightingWith());
			}
		}

		if (getFightingWith() instanceof Player) {
			if (ourPlayer.getLastAttacked() != getFightingWith().getPlayer()) {
				ourPlayer.setLastAttacked(getFightingWith().getPlayer());
			}
		}
	}

	/**
	 * Method isWithinBoundry Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	@Override
	public boolean isWithinBoundry() {
		if (!ourPlayer.isVisible())
			return false;

		if (getFightingWith().isDead() || ourPlayer.isDead()) {
			ourPlayer.unFollow();

			return false;
		}

		if (!getFightingWith().isVisible()) {
			closeRequest();

			return false;
		}

		if ((getFightingWith() instanceof Player) && !((Player) getFightingWith()).isLoggedIn()) {
			closeRequest();

			return false;
		}

		if ((getFightingWith() instanceof Player) && getMob().isInWilderness()
				&& (Combat.getWildernessLevel(getMob()) < 10)) {
			if (((Player) getFightingWith()).getAccount().isRestrained(getMob())
					|| getMob().getAccount().isRestrained(getFightingWith().getPlayer())) {

				getMob().getActionSender().sendMessage(
						Text.RED("You and this player can no longer fight in edgeville (restraining order)"));
				closeRequest();

				return false;
			}
		}

		if ((ourPlayer.getGetCurrentWar() != null) && ClanWar.isInClanWars(ourPlayer)) {
			if (!ourPlayer.getGetCurrentWar().gameStarted()) {
				ourPlayer.getActionSender().sendMessage("Please wait until the game starts.");
				closeRequest();

				return false;
			}

			if (ourPlayer.getGetCurrentWar().isTeamedWith(getFightingWith().getPlayer(), ourPlayer)) {
				closeRequest();
				ourPlayer.getActionSender().sendMessage("Friendly fire is not tolerated!");

				return false;
			}

			return true;
		}

		if (ourPlayer.getAccount().hasVar("trade_lock") && (getFightingWith() instanceof Player)) {
			if (ourPlayer.isInWilderness()) {
				if ((ourPlayer.getGetCurrentWar() == null) || ((ourPlayer.getGetCurrentWar() != null)
						&& ourPlayer.getGetCurrentWar().getGameRules().settingActive(ClanWarRules.ITEMS_ON_DEATH))) {
					ourPlayer.getActionSender()
							.sendMessage("You are trade locked, and therefore cannot engage in PVP Combat");
					closeRequest();

					return false;
				}
			}
		} else if ((getFightingWith() instanceof Player)
				&& ((Player) getFightingWith()).getAccount().hasVar("trade_lock")) {
			Player ourPlayer = getFightingWith().getPlayer();

			if (ourPlayer.isInWilderness()) {
				if ((ourPlayer.getGetCurrentWar() == null) || ((ourPlayer.getGetCurrentWar() != null)
						&& ourPlayer.getGetCurrentWar().getGameRules().settingActive(ClanWarRules.ITEMS_ON_DEATH))) {
					this.ourPlayer.getActionSender().sendMessage("This person is trade-locked");
					closeRequest();

					return false;
				}
			}
		}

		if ((ourPlayer.getEquipment().getId(3) != -1) && Item.forId(ourPlayer.getEquipment().getId(3)).isLend()) {
			if ((Combat.getWildernessLevel(ourPlayer) != -1) && !ourPlayer.isInArea(DeathTowerGame.TOWER_ZONE)) {
				ourPlayer.getActionSender().sendMessage("You cannot use rented weapons in the wilderness.");
				closeRequest();

				return false;
			}
		}

		if (getFightingWith() == ourPlayer) {
			closeRequest();

			return false;
		}

		if (getFightingWith().getCurrentInstance() != ourPlayer.getCurrentInstance()) {
			closeRequest();

			return false;
		}

		if (getFightingWith() instanceof NPC) {
			if (getFightingWith().getNpc().blockInteractions()) {
				return false;
			}

			if (((NPC) getFightingWith()).getQuest_controller() != null
					&& (getFightingWith().getNpc().getQuest_controller() != ourPlayer)) {
				closeRequest();

				return false;
			}

			if (!getFightingWith().getNpc().getDefinition().isAttackable()
					&& !Summoning.PVPCheck(getFightingWith().getNpc(), ourPlayer)) {
				ourPlayer.getActionSender().sendMessage("You can't attack this npc");

				return false;
			}

			if (!ourPlayer.canBeAttacked(getFightingWith())) {
				ourPlayer.getActionSender().sendMessage("You are already under attack.");
				closeRequest();

				return false;
			}

			if (!getFightingWith().getNpc().canBeAttacked(ourPlayer) || !ourPlayer.canBeAttacked(getFightingWith())) {
				ourPlayer.getActionSender().sendMessage("This npc already under attack");

				return false;
			}

			if (getFightingWith() instanceof GraveStone) {
				return false;
			}

			if ((getFightingWith().getNpc().getId() == 9176) && (ourPlayer.getCurStat(18) < 80)) {
				ourPlayer.getActionSender().sendMessage("You need a slayer level of atleast 80 to kill this boss");

				return false;
			}

			if (getFightingWith().getNpc().isSlayerNPC()
					&& ((NPC) getFightingWith()).getSlayerInfo().getLevelRequired() > ourPlayer.getCurStat(18)
					&& !getFightingWith().getNpc().getSlayerInfo().isSilent()) {
				ourPlayer.getActionSender().sendMessage("You need a slayer level of atleast "
						+ getFightingWith().getNpc().getSlayerInfo().getLevelRequired() + " to kill this monster.");

				return false;
			}
		}

		if (getFightingWith() instanceof Player) {
			if (ourPlayer.getGameFrame().getDuel() != null) {
				return duelCheck(ourPlayer, getFightingWith().getPlayer());
			}

			if (CastleWarsEngine.getSingleton().castleWarsCheck(ourPlayer, getFightingWith().getPlayer(), true)) {
				return true;
			}

			if (getFightingWith().getPlayer().isActionsDisabled()) {
				closeRequest();

				return false;
			}

			if (!getFightingWith().isVisible()) {
				closeRequest();

				return false;
			}

			if (ourPlayer.getCurrentHouse() != null) {
				if (ourPlayer.getCurrentHouse().isInRing(ourPlayer) && (getFightingWith() instanceof Player)
						&& ((Player) getFightingWith()).getCurrentHouse() == ourPlayer.getCurrentHouse()
						&& getFightingWith().getPlayer().getCurrentHouse().isInRing(getFightingWith().getPlayer())) {
					return true;
				}
			}

			if ((ourPlayer.isInArea(ClanWar.RED_PORTAL_PVP) || ourPlayer.isInArea(ClanWar.WHITE_PORTAL_PVP))
					&& (getFightingWith().getPlayer().isInArea(ClanWar.RED_PORTAL_PVP)
							|| getFightingWith().isInArea(ClanWar.WHITE_PORTAL_PVP))) {
				return true;
			}

			if (FightPitsGame.isPlaying(ourPlayer) && FightPitsGame.isPlaying(getFightingWith().getPlayer())) {
				return true;
			}

			if (!getFightingWith().isInWilderness()) {
				ourPlayer.getActionSender().sendMessage("This player is not in the wilderness.");
				closeRequest();

				return false;
			}

			if (!ourPlayer.isInWilderness()) {
				closeRequest();
				ourPlayer.getActionSender().sendMessage("You're not in the wilderness.");

				return false;
			}

			if (!Combat.canAttack(ourPlayer, getFightingWith().getPlayer())
					|| !Combat.canAttack(getFightingWith().getPlayer(), ourPlayer)) {
				closeRequest();
				ourPlayer.getActionSender().sendMessage("Please move deeper into the wilderness.");

				return false;
			}

			if (!ourPlayer.canBeAttacked(getFightingWith()) && !ourPlayer.isInMultiArea()) {
				closeRequest();
				ourPlayer.getActionSender().sendMessage("You're already under attack");

				return false;
			}
		}

		return true; // To change body of implemented methods use File |
						// Settings | File Templates.
	}

	/**
	 * Method duel_block Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 * @param fightingWith
	 *
	 * @return
	 */
	public static boolean duel_block(Player pla, Player fightingWith) {
		if (pla.getGameFrame().getDuel() == null) {
			return false;
		}

		if (pla.getGameFrame().getDuel().getOtherPlayer(pla) != fightingWith) {
			return true;
		}

		return false;
	}

	private boolean duelCheck(Player pla, Player fightingWith) {
		Duel d = pla.getGameFrame().getDuel();

		if (d == null) {
			return false;
		}

		if (pla.getTimers().timerActive(TimingUtility.DUEL_WAIT_TIMER)) {
			pla.getActionSender().sendMessage("Please wait until the duel starts.");

			return false;
		}

		if (pla.isInZone("duel")) {
			if (d.getOtherPlayer(pla) != fightingWith) {
				pla.getActionSender().sendMessage("You can't attack this person, kill your target instead.");

				return false;
			}

			return true;
		}

		return false;
	}

	/**
	 * Method isWithinDistanceToTarget Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	@Override
	public boolean isWithinDistanceToTarget() {
		if (!Combat.isWithinDistance(ourPlayer, getFightingWith()) || !ourPlayer.withinRange(getFightingWith())) {
			return false; // To change body of implemented methods use File |
							// Settings | File Templates.
		}

		return true;
	}

	/**
	 * Method terminate Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	@Override
	public void terminate() {
	}

	/**
	 * Method closeRequest Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void closeRequest() {
		reset();
		ourPlayer.getFocus().unFocus();
		ourPlayer.unFollow();
		ourPlayer.getRequest().close();
	}

	/**
	 * Method executeCombatTick Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	@Override
	public void executeCombatTick() {
		if (ourPlayer.getAccount().isActionDisabled(Account.COMBAT_DISABLED)) {
			ourPlayer.getActionSender().sendMessage(ourPlayer.getAccount().getDisabledMessage(Account.COMBAT_DISABLED));
			closeRequest();

			return;
		}

		if (!ourPlayer.getFocus().isFocused(getFightingWith())) {
			ourPlayer.getFocus().focus(getFightingWith());
		}

		if (!getFightingWith().canBeAttacked(ourPlayer)) {
			ourPlayer.getActionSender().sendMessage("This person is already under attack.");
			closeRequest();

			return;
		}

		if (getFightingWith() instanceof Player) {
			if (ourPlayer.isInWilderness()) {
				if (!ourPlayer.wasAttackedBy(getFightingWith().getPlayer())
						&& !getFightingWith().getPlayer().didAttack(ourPlayer)) {
					ourPlayer.getTimers().setTime(TimingUtility.SKULL_TIMER, Core.getTicksForMinutes(20));
					ourPlayer.getAppearance().setChanged(true);
					ourPlayer.addDidAttack(getFightingWith().getPlayer());
					((Player) getFightingWith()).addAttackedBy(ourPlayer);
				}
			}
		}

		Duel d = ourPlayer.getGameFrame().getDuel();

		if (Combat.isMaging(ourPlayer)) {
			if (ourPlayer.getAccount().isActionDisabled(Account.SPELLS_DISABLED)) {
				ourPlayer.getActionSender()
						.sendMessage(ourPlayer.getAccount().getDisabledMessage(Account.SPELLS_DISABLED));

				return;
			}

			if (ourPlayer.getGetCurrentWar() != null) {
				if (!ourPlayer.getGetCurrentWar().getGameRules().magicAllowed()) {
					closeRequest();
					ourPlayer.getActionSender().sendMessage("Magic spells are not allowed in this war.");

					return;
				}

				if ((ourPlayer.getGameFrame().getSpellbook() != 192)
						&& (ourPlayer.getGetCurrentWar().getGameRules().getRule(ClanWarRules.MAGE_OPTIONS) > 0)) {
					ourPlayer.getActionSender().sendMessage("Ancient magicks/lunar are not allowed in this war");
					closeRequest();

					return;
				}
			}

			if ((d != null) && d.setting(Duel.NO_MAGIC)) {
				ourPlayer.getActionSender().sendMessage("Magic attacks have been disabled during this fight.");
				closeRequest();

				return;
			}

			attackMage();
		} else if (Combat.isRanging(ourPlayer)) {
			if (ourPlayer.getGetCurrentWar() != null) {
				if (ourPlayer.getGetCurrentWar().getGameRules().settingActive(ClanWarRules.RANGE_DISABLED)) {
					closeRequest();
					ourPlayer.getActionSender().sendMessage("Ranged is not allowed in this war.");

					return;
				}
			}

			if ((d != null) && d.setting(Duel.NO_RANGED)) {
				ourPlayer.getActionSender().sendMessage("Ranged attacks have been disabled during this fight.");
				closeRequest();

				return;
			}

			doRangeCombatTick();
		} else {
			if (ourPlayer.getGetCurrentWar() != null) {
				if (ourPlayer.getGetCurrentWar().getGameRules().settingActive(ClanWarRules.MELEE_DISABLED)) {
					closeRequest();
					ourPlayer.getActionSender().sendMessage("Melee is not allowed in this war.");

					return;
				}
			}

			if ((d != null) && d.setting(Duel.NO_MELEE)) {
				ourPlayer.getActionSender().sendMessage("Melee attacks have been disabled during this fight.");
				closeRequest();

				return;
			}

			doMeleeCombatTick();
		}

		// To change body of implemented methods use File | Settings | File
		// Templates.
	}

	/**
	 * Method attackMage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	public void attackMage() {
		ScriptVariableInjector injector = new ScriptVariableInjector() {
			@Override
			public void injectVariables(Scope scope) {
				scope.setAttachement(getFightingWith());
			}
		};

		World.getWorld().getScriptManager().directTrigger(ourPlayer, injector, Trigger.MAGE_NPC,
				ourPlayer.getGameFrame().getSpellbook(), ourPlayer.getRequestedSpell());

		if (ourPlayer.getAutoCast() == -1) {
			Killable fightingWith = getFightingWith();

			this.closeRequest();
			ourPlayer.getFocus().focus(fightingWith);
		}

		ourPlayer.setRequestedSpell(ourPlayer.getAutoCast());
	}

	/**
	 * Method doRangeCombatTick Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 */
	private void doRangeCombatTick() {
		Weapon r = ourPlayer.getEquipment().getWeapon();

		if (!ourPlayer.isSpecialHighlighted()) {
			switch (r.getType()) {

			// here we see what type of weapon the players holding
			// then we calculate an algorithim for it
			case Weapon.TYPE_SPECIALBOW:
				if (ourPlayer.getEquipment().getId(Equipment.ARROWS) != -1) {
					ourPlayer.getActionSender().sendMessage("Please remove your arrows before using this bow");
					closeRequest();

					return;
				}

				int drawBackGFX = 250;
				int arrow_proj = 249;

				ourPlayer.getAnimation().animate(426, Animation.HIGH_PRIORITY);
				ourPlayer.getGraphic().set(drawBackGFX, Graphic.HIGH);
				getFightingWith().registerHit(ourPlayer);
				RangeUtil.createRangeProjectile(arrow_proj, ourPlayer, getFightingWith(), 10, 3, 1.00);

				// as u can see, specialbow is handled in this block, with the
				// gfx's being 250 249 (msb spec bs)
				break;

			case Weapon.TYPE_CANNON:
				if (ourPlayer.getEquipment().getId(Equipment.ARROWS) != 15243) {
					ourPlayer.getActionSender().sendMessage("You don't have any hand cannon shots.");
					closeRequest();
					reset();

					return;
				}

				if (ourPlayer.getCurStat(11) < 60) {
					ourPlayer.getActionSender().sendMessage("You need atleast 60 firemaking to fire this.");
					closeRequest();
					reset();

					return;
				}

				ourPlayer.getAnimation().animate(12174);
				ourPlayer.getEquipment().removeArrow();
				RangeUtil.createSpecialRangeProjectile(2143, ourPlayer, getFightingWith(), 10, 2, 1.00, 1.00, 20, 20);
				ourPlayer.getGraphic().set(2138, Graphic.LOW);

				break;

			case Weapon.TYPE_SPECIAL_XBOW:
				int wepID = ourPlayer.getEquipment().getId(3);

				if (wepID == 16877) {
					ourPlayer.getAnimation().animate(426, Animation.HIGH_PRIORITY);
				} else {
					ourPlayer.getAnimation().animate(4230, Animation.HIGH_PRIORITY);
				}

				getFightingWith().registerHit(ourPlayer);
				RangeUtil.createRangeProjectile(955, ourPlayer, getFightingWith(), 15, 3, 1.40);

				if (wepID == 16877) {
					RangeUtil.createRangeProjectile(955, ourPlayer, getFightingWith(), 15, 5, 1.40);
				}

				// as u can see, specialbow is handled in this block, with the
				// gfx's being 250 249 (msb spec bs)
				break;

			case Weapon.TYPE_BOW:
				if (ourPlayer.getEquipment().getArrowsCount() < 1) {
					ourPlayer.getActionSender().sendMessage("You don't have enough arrows in your quiver.");
					closeRequest();

					return;
				}

				// check the arrow types
				if (!RangeUtil.arrowCheck(ourPlayer)) {
					closeRequest();

					return;
				}

				// check if it's d-bow, cause they have special gfx etc...
				if (!RangeUtil.isDarkBow(ourPlayer.getEquipment().getId(3))) {
					int drawbackGfx = RangeUtil.getArrowGfx(ourPlayer);

					ourPlayer.getGraphic().set(drawbackGfx, Graphic.HIGH);
					ourPlayer.getAnimation().animate(r.getAttackStyle(ourPlayer.getAccount().get(43)).getAnimation());

					int arrowGfx = RangeUtil.getArrowGfx(ourPlayer.getEquipment().getId(Equipment.ARROWS),
							ourPlayer.getEquipment().getId(3));

					if ((arrowGfx != -1) && (drawbackGfx != -1)) {
						RangeUtil.createRangeProjectile(arrowGfx, ourPlayer, getFightingWith(), 10, 2, 1.00);
						ourPlayer.getEquipment().removeArrow();
						getFightingWith().registerHit(ourPlayer);
					}
				} else {

					// handle the normal bs you should check the defence BS
					// again, its bss that kids in rune and a whip/cmaul can K0
					// kids in full torva lmao
					// i checked it its fine, look
					int drawBackGfx = RangeUtil.getDBowDrawbackGfx(ourPlayer.getEquipment().getId(Equipment.ARROWS));

					if (drawBackGfx != -1) {
						ourPlayer.getGraphic().set(drawBackGfx, Graphic.HIGH);
						ourPlayer.getAnimation()
								.animate(r.getAttackStyle(ourPlayer.getAccount().get(43)).getAnimation());

						int arrowGfx = RangeUtil.getArrowGfx(ourPlayer.getEquipment().getId(Equipment.ARROWS),
								ourPlayer.getEquipment().getId(3));

						if ((arrowGfx != -1)) {
							RangeUtil.createRangeProjectile(arrowGfx, ourPlayer, getFightingWith(), 15, 3, 1.00);
							RangeUtil.createRangeProjectile(arrowGfx, ourPlayer, getFightingWith(), 15, 3, 1.00);
							ourPlayer.getEquipment().removeArrow();
							ourPlayer.getEquipment().removeArrow();
							getFightingWith().registerHit(ourPlayer);
						}
					}
				}

				break;

			case Weapon.TYPE_CROSSBOW:
				if (ourPlayer.getEquipment().getArrowsCount() < 1) {
					ourPlayer.getActionSender().sendMessage("You don't have any ammunition.");
					closeRequest();

					return;
				}

				Item ammo = Item.forId(ourPlayer.getEquipment().getId(Equipment.ARROWS));
				int ammoType = RangeUtil.getArrowType(ourPlayer.getEquipment().getId(Equipment.ARROWS));

				if (!RangeUtil.arrowCheck(ourPlayer)) {
					closeRequest();

					return;
				}

				Ammunition am = ammo.getAmmoData();

				if (am.getStartGraphics() != -1) {
					ourPlayer.getGraphic().set(am.getStartGraphics(), Graphic.HIGH);
				}

				RangeUtil.createRangeProjectile(am.getProjectileId(), ourPlayer, getFightingWith(), 16, 2, 1.00);
				ourPlayer.getEquipment().removeArrow();
				ourPlayer.getAnimation().animate(ourPlayer.getEquipment().getWeapon()
						.getAttackStyle(ourPlayer.getAccount().get(43)).getAnimation());
				getFightingWith().registerHit(ourPlayer);

				break;

			case Weapon.TYPE_THROWN:
				ammoType = RangeUtil.getArrowType(ourPlayer.getEquipment().getId(3));

				if (ammoType == -1) {
					closeRequest();
					Logger.log("bad type");

					return;
				}

				int drawBack = RangeUtil.getArrowGfx(ourPlayer);

				if ((drawBack == -1) && (ourPlayer.getEquipment().getId(3) != 13879)
						&& (ourPlayer.getEquipment().getId(3) != 10034)) {
					Logger.log("bad type 2: " + ammoType);
					closeRequest();

					return;
				}

				int projectile = RangeUtil.getArrowGfx(ourPlayer.getEquipment().getId(3),
						ourPlayer.getEquipment().getId(3));

				if (projectile == -1) {
					closeRequest();

					return;
				}

				ourPlayer.getAnimation().animate(ourPlayer.getEquipment().getWeapon()
						.getAttackStyle(ourPlayer.getAccount().get(43)).getAnimation());

				if (drawBack != -1) {
					ourPlayer.getGraphic().set(drawBack, Graphic.HIGH);
				}

				int[] vars = { 50, 40, 50, 45, 65, 15, 90 };

				RangeUtil.createRangeProjectile(projectile, ourPlayer, getFightingWith(), 16, 2, 1.00, 1.00, vars);
				ourPlayer.getEquipment().removeWeapon();
				getFightingWith().registerHit(ourPlayer);

				break;
			}
		} else {
			SpecialAttacks.doSpecial(ourPlayer, getFightingWith());
			SpecialAttacks.changeHighlight(ourPlayer);
		}
	}

	/**
	 * Method getDharokMultiplier Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @return
	 */
	public double getDharokMultiplier() {
		double h = ourPlayer.getCurrentHealth();

		if (h > ourPlayer.getMaxStat(3)) {
			h = ourPlayer.getMaxStat(3);
		}

		double calculation = ourPlayer.getMaxStat(3) - h;

		if ((ourPlayer.getCurrentHealth() > ourPlayer.getMaxStat(3) / 2) && (GameMath.rand(100) < 50)) {
			calculation = 0.00;
		}

		if (calculation <= 0) {
			return 1.00;
		}

		return 1.00 + (calculation / 100d);
	}

	private void doMeleeCombatTick() {
		Weapon r = ourPlayer.getEquipment().getWeapon();
		AttackStyle currentStyle = r.getAttackStyle(ourPlayer.getAccount().get(Account.SETTING_FIGHT_STYLE));

		if (!ourPlayer.isSpecialHighlighted()) {
			if (currentStyle != null) {
				ourPlayer.getAnimation().animate(currentStyle.getAnimation());

				double multiplier = 1.00;
				boolean dh = false;

				if (ourPlayer.getEquipment().fullDharok()) {
					multiplier = getDharokMultiplier();
					dh = true;
				}

				if (ourPlayer.getEquipment().getId(Equipment.AMULET) == 11128) {
					switch (ourPlayer.getEquipment().getId(3)) {
					case 6522:
					case 6523:
					case 6524:
					case 6526:
					case 6527:
					case 6528:
						multiplier = 1.25;
					}
				}

				double damageTotal = Combat.applyMeleeDamage2(multiplier, 1, ourPlayer, getFightingWith());

				if (ourPlayer.getEquipment().getId(3) == 4151) {
					ourPlayer.addAchievementProgress2(Achievement.ABYSSAL_PUNISHMENT, (int) damageTotal);
				}

				if ((ourPlayer.getEquipment().getId(3) == 4587) && (getFightingWith() instanceof Player)
						&& ourPlayer.isInWilderness()) {
					final Player fightin = (Player) getFightingWith();

					ourPlayer.getTickEvents().add(new PlayerTickEvent(ourPlayer, 2) {
						@Override
						public void doTick(Player owner) {
							if (fightin.isDead()) {
								ourPlayer.addAchievementProgress2(Achievement.SCIMMER, 1);
							}
							terminate();
						}
					});
				}

				if ((damageTotal > 0) && ourPlayer.getEquipment().fullGuthans()) {
					if (ourPlayer.getRandom().nextBoolean()) {
						ourPlayer.addHealth(damageTotal);
					}

					getFightingWith().getGraphic().set(398, 0);
				}

				getFightingWith().registerHit(ourPlayer);

				int multi = 1;

				if (ourPlayer.getEquipment().getId(Equipment.SHIELD) == 20001) {
					ourPlayer.addHealth((int) (damageTotal * 0.30));
				}

				addCombatXP((int) (damageTotal * multi), -1, ourPlayer);
			}
		} else {
			boolean dmg = SpecialAttacks.doSpecial(ourPlayer, getFightingWith());

			if (dmg) {
				getFightingWith().registerHit(ourPlayer);
			}

			SpecialAttacks.changeHighlight(ourPlayer);
		}
	}

	/**
	 * Method addCombatXP Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param hitFor
	 * @param type
	 * @param ourPlayer
	 */
	public static void addCombatXP(int hitFor, int type, Player ourPlayer) {
		int fightStyle = ourPlayer.getEquipment().getCurrentFightStyle();

		if (ourPlayer.isInArea(PCGame.PEST_CONTROL_GAME)) {
			hitFor *= 2;
		}

		if (type == AttackStyle.TYPE_RANGE) {
			int exp = 4;

			if (fightStyle != AttackStyle.MODE_LONGRANGE) {
				ourPlayer.addXp(4, (int) (hitFor * exp));
				ourPlayer.addXp(3, (int) ((hitFor * exp)));
			} else {
				ourPlayer.addXp(4, (int) (hitFor * exp));
				ourPlayer.addXp(3, (int) ((hitFor * exp)));
				ourPlayer.addXp(1, (int) (((hitFor / 4) * exp)));
			}

			return;
		}

		if (type == AttackStyle.TYPE_MAGIC) {
			ourPlayer.addXp(6, (int) (hitFor * 4));
			ourPlayer.addXp(3, (int) ((hitFor * 1.33)));

			return;
		}

		switch (fightStyle) {
		case AttackStyle.MODE_ACCURATE:
			ourPlayer.addXp(0, (hitFor * 4));
			ourPlayer.addXp(3, (int) (hitFor * 1.33));

			break;

		case AttackStyle.MODE_AGGRESSIVE:
			ourPlayer.addXp(2, (hitFor * 4));
			ourPlayer.addXp(3, (int) (hitFor * 1.33));

			break;

		case AttackStyle.MODE_DEFENSIVE:
			ourPlayer.addXp(1, hitFor * 4);
			ourPlayer.addXp(3, (int) (hitFor * 1.33));

			break;

		case AttackStyle.MODE_CONTROLLED:
			int xp = hitFor * 4;

			ourPlayer.addXp(1, (int) (xp / 3));
			ourPlayer.addXp(2, (int) (xp / 3));
			ourPlayer.addXp(0, (int) (xp / 3));
			ourPlayer.addXp(3, (int) (hitFor * 1.33));

			break;
		}
	}
}
