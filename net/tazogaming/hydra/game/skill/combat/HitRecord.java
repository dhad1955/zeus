package net.tazogaming.hydra.game.skill.combat;

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/01/2015
 * Time: 12:10
 */
public class HitRecord {

    public HitRecord(Mob damagedBy, int dmg){
        this.damagedBy = damagedBy;
        this.damage = dmg;
        this.damageAt = Core.currentTime;
    }
    private Mob damagedBy;

    public int getDamage() {
        return damage;
    }

    public void increaseDamage(int dmg){
        this.damage += dmg;
        this.damageAt = Core.currentTime;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getDamageAt() {
        return damageAt;
    }

    public void setDamageAt(int damageAt) {
        this.damageAt = damageAt;
    }

    public Mob getDamagedBy() {
        return damagedBy;
    }

    public void setDamagedBy(Mob damagedBy) {
        this.damagedBy = damagedBy;
    }

    private int damage;
    private int damageAt;

}
