package net.tazogaming.hydra.game.skill.combat.potion;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/10/13
 * Time: 21:54
 */
public abstract class DoseHandler {

    /**
     * Method drink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public abstract void drink(Player pla);

    protected void increasePrayer(int amt, int percent, Player pla) {
        int prayer = pla.getPrayerBook().getPoints();

        prayer += amt + (pla.getMaxStat(5) / percent);

        if (prayer > pla.getMaxStat(5)) {
            prayer = pla.getMaxStat(5);
        }

        pla.getPrayerBook().setPoints(prayer);
        pla.getActionSender().sendStat(5);
    }

    protected void increaseStat(int stat, int min, int percentage, Player player) {
        int    curStat = player.getMaxStat(stat);
        int    stat2   = player.getCurStat(stat);
        double percent = 1 + (percentage / 100d);

        if ((stat2) * percent > curStat * percent) {
            stat2 = min + (int) (curStat * percent);
        } else {
            stat2 = min + (int) (stat2 * percent);
        }

        player.setCurStat(stat, stat2);
        player.getActionSender().sendStat(stat);
    }

    protected void restoreStats(int min, double percentage, Player pla) {
        for (int i = 0; i < 24; i++) {
            if (pla.getCurStat(i) < pla.getMaxStat(i)) {
                pla.setCurStat(i, (int) (pla.getCurStat(i) * percentage) + min);

                if (pla.getCurStat(i) > pla.getMaxStat(i)) {
                    pla.setCurStat(i, pla.getMaxStat(i));
                }

                pla.getActionSender().sendStat(i);
            }
        }
    }

    protected void decreaseStat(int stat, int percentage, Player player) {
        int    curStat = player.getCurStat(stat);
        double percent = 1.00 - (double) (percentage / 100d);
        int    newStat = (int) (curStat * percent);

        if (newStat < 0) {
            newStat = 0;
        }

        player.setCurStat(stat, newStat);
        player.getActionSender().sendStat(stat);
    }
}
