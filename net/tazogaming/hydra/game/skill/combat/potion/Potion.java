package net.tazogaming.hydra.game.skill.combat.potion;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/10/13
 * Time: 21:53
 */
public class Potion {

    public static void doOverload(Player pla){
        increaseStat2(4, 3, 25, pla);
        increaseStat2(1, 3, 25, pla);
        increaseStat2(2, 3, 25, pla);
        increaseStat2(0, 3, 25, pla);
        increaseStat2(6, 3, 25, pla);
    }

    protected static void increaseStat2(int stat, int min, int percentage, Player player) {
        int    curStat = player.getMaxStat(stat);
        int    stat2   = player.getCurStat(stat);
        double percent = 1 + (percentage / 100d);

        if ((stat2) * percent > curStat * percent) {
            stat2 = min + (int) (curStat * percent);
        } else {
            stat2 = min + (int) (stat2 * percent);
        }

        player.setCurStat(stat, stat2);
        player.getActionSender().sendStat(stat);
    }
    static {

        /*
         *  Begin setting up potions
         *
         */

        // executeCombatTick potion
        addPotion(new Potion(125, 123, 121, 2428).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(0, 1, 8, pla);
            }
        }));

        // defence potion
        addPotion(new Potion(137, 135, 133, 2432).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(1, 1, 8, pla);
            }
        }));

        // strength potion
        addPotion(new Potion(119, 117, 115, 113).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(2, 1, 8, pla);
            }
        }));

        // ranging potion
        addPotion(new Potion(173, 171, 169, 2444).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(4, 5, 8, pla);
            }
        }));

        // mage potion
        addPotion(new Potion(3046, 3044, 3042, 3040).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(6, 1, 8, pla);
            }
        }));

        // energy potion
        addPotion(new Potion(3014, 3012, 3010, 3008).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                pla.setCurrentEnergy(pla.getCurrentEnergy() + 15);

                if (pla.getCurrentEnergy() > 100) {
                    pla.setCurrentEnergy(100);
                }

                pla.getActionSender().updateEnergy();
            }
        }));

        // super energy potion
        addPotion(new Potion(3022, 3020, 3018, 3016).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                pla.setCurrentEnergy(pla.getCurrentEnergy() + 27);

                if (pla.getCurrentEnergy() > 100) {
                    pla.setCurrentEnergy(100);
                }

                pla.getActionSender().updateEnergy();
            }
        }));

        // agility potion
        addPotion(new Potion(3038, 3036, 3034, 3032).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(16, 1, 8, pla);
            }
        }));

        // fishing potion
        addPotion(new Potion(155, 153, 151).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(10, 1, 8, pla);
            }
        }));

        // anti poison
        addPotion(new Potion(179, 177, 175, 2446).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                if (pla.isPoisoned()) {
                    pla.getActionSender().sendMessage("Your poison is now cured.");
                }
                pla.getActionSender().sendStat(3);
                pla.setPoisonTime(0);
            }
        }));

        // suepr anti poison
        addPotion(new Potion(185, 183, 181, 2448).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                if (pla.isPoisoned()) {
                    pla.getActionSender().sendMessage("Your poison is now cured.");
                }

                pla.setPoisonTime(0);
                pla.getActionSender().sendStat(3);

                pla.getTimers().setTime(TimingUtility.POISON_IMMUNE_TIME, 300000 / 600);
            }
        }));

        // prayer potion
        addPotion(new Potion(143, 141, 139, 2434).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increasePrayer(7, 4, pla);
            }
        }));

        // super prayer
        addPotion(new Potion(15331, 15330, 15329, 15328).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increasePrayer(1119, 1114, pla);
            }
        }));

        // super executeCombatTick
        addPotion(new Potion(149, 147, 145, 2436).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(0, 5, 15, pla);
            }
        }));

        // super strength
        addPotion(new Potion(161, 159, 157, 2440).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(2, 5, 15, pla);
            }
        }));

        // super defence
        addPotion(new Potion(167, 165, 163, 2442).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(1, 5, 15, pla);
            }
        }));

        // extreme executeCombatTick
        addPotion(new Potion(15311, 15310, 15309, 15308).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(0, 3, 18, pla);
            }
        }));

        // extreme defence
        addPotion(new Potion(15319, 15318, 15317, 15316).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                increaseStat(1, 3, 18, pla);
            }
        }));

        // extreme strength
        addPotion(new Potion(15315, 15314, 15313, 15312).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
               if(!pla.isInWilderness())
                increaseStat(2, 3, 18, pla);
                else
                   pla.getActionSender().sendMessage("Extremes not allowed in wild");
            }
        }));

        // extreme magic
        addPotion(new Potion(15323, 15322, 15321, 15320).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
               if(!pla.isInWilderness())
                increaseStat(6, 3, 18, pla);
                else
                   pla.getActionSender().sendMessage("Extreme magic not allowed in wilderness.");

            }
        }));

        // extreme ranging
        addPotion(new Potion(15327, 15326, 15325, 15324).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
               if(!pla.isInWilderness())
                    increaseStat(4, 3, 18, pla);
                else
                   pla.getActionSender().sendMessage("Extreme ranging not allowed in wilderness.");
            }
        }));

        // overload
        addPotion(new Potion(15335, 15334, 15333, 15332).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
               if(!pla.isInWilderness()) {
                    increaseStat(4, 3, 25, pla);
                    increaseStat(1, 3, 25, pla);
                    increaseStat(2, 3, 25, pla);
                    increaseStat(0, 3, 25, pla);
                    increaseStat(6, 3, 25, pla);
                   if(pla.getGameFrame().getDuel() == null)
                         pla.hit(pla, 25, Damage.TYPE_OTHER, 0);

                    pla.getTimers().setTime(TimingUtility.OVERLOAD_TIMER, Core.getTicksForMinutes(6));
                    pla.getTimers().setTime(TimingUtility.OVERLOAD_REPLENISH_TIMER, Core.getTicksForSeconds(15));

               }else{
                   pla.getActionSender().sendMessage("Overloads are not allowed in the wilderness.");
               }
            }
        }));

        // saradomin brew
        addPotion(new Potion(6691, 6689, 6687, 6685).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                int boost = (int) (pla.getMaxHealth() * 0.15);
                int max   = (int) (pla.getMaxHealth() * 1.15);

                pla.setCurrentHealth(pla.getCurrentHealth() + boost);

                if (pla.getCurrentHealth() > max) {
                    pla.setCurrentHealth(max);
                }

                pla.getActionSender().sendStat(3);
               if(!pla.getTimers().timerActive(TimingUtility.OVERLOAD_TIMER)) {
                    decreaseStat(0, 10, pla);
                    decreaseStat(4, 10, pla);
                    decreaseStat(6, 10, pla);
                    decreaseStat(2, 10, pla);
               }
                increaseStat(1, 3, 20, pla);
            }
        }));

        // super restore
        addPotion(new Potion(3030, 3028, 3026, 3024).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                restoreStats(8, 1.25, pla);
                increasePrayer(8, 4, pla);
                int pts = pla.getCurrSummonPts();
                if(pts  < pla.getMaxStat(23)){
                    int calc = pts + 20;
                    if(calc > pla.getMaxStat(23))
                    {
                        pla.setCurrSummonPts(pla.getMaxStat(23));
                        pla.getActionSender().sendStat(23);
                    }else{
                        pla.setCurrSummonPts(calc);
                    }
                    pla.getActionSender().sendStat(23);

                    pla.getActionSender().sendStat(3);

                }
            }
        }));

        // restore potion
        addPotion(new Potion(131, 129, 127).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                restoreStats(6, 1.16, pla);
            }
        }));

        // 2452 Antifire_(4) 330 false false 0.0 true
        // 2453 Antifire_(4) 330 true true 0.0 true
        addPotion(new Potion(2458, 2456, 2454, 2452).setDoseHandler(new DoseHandler() {
            @Override
            public void drink(Player pla) {
                pla.getActionSender().sendMessage("You are now protected from dragon-fire");
                pla.getTimers().setTime(TimingUtility.DRAGON_FIRE_IMMUNE_TIME, 400, true);

                // To change body of implemented methods use File | Settings | File Templates.
            }
        }));
    }

    private int[]       doses = new int[4];
    private DoseHandler doseHandler;

    /**
     * Constructs ...
     *
     *
     * @param doses
     */
    public Potion(int... doses) {
        this.doses = doses;

        for (int i = 0; i < doses.length; i++) {
            Item.forId(getDose(i)).setPotionHandler(this);
        }
    }

    /**
     * Method setDoseHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param d
     *
     * @return
     */
    public Potion setDoseHandler(DoseHandler d) {
        this.doseHandler = d;

        return this;
    }

    /**
     * Method getDose
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dose
     *
     * @return
     */
    public int getDose(int dose) {
        if (dose == -1) {
            return 229;
        }

        return doses[dose];
    }

    /**
     * Method getRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public int getRemaining(int item) {
        for (int i = 0; i < doses.length; i++) {
            if (getDose(i) == item) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Method getDoseHandler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public DoseHandler getDoseHandler() {
        return doseHandler;
    }

    private static void addPotion(Potion p) {}

    /**
     * Method init
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void init() {}
}
