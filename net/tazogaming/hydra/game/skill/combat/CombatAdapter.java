package net.tazogaming.hydra.game.skill.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.minigame.zombiesx.Zombies;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 22:02
 * To change this template use File | Settings | File Templates.
 */
public abstract class CombatAdapter<T extends Killable> {
    private int      combatDelay = 0;
    private T myMob;
    private Killable fightingWith;

    /**
     * Constructs ...
     *
     *
     * @param mob
     */
    public CombatAdapter(T mob) {
        myMob = mob;
    }

    /**
     * Method attackOn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     */
    public void attackOn(Killable k) {
        fightingWith = k;
   //     update();
        onTargetAcquired();
    }

    /**
     * Method switchWeapons
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param oldSpeed
     * @param newSPeed
     */
    public void switchWeapons(int oldSpeed, int newSPeed) {

    }

    /**
     * Method isInCombat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isInCombat() {
        return fightingWith != null;
    }

    /**
     * Method delay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param seconds
     */
    public void delay(int seconds) {
        combatDelay += (seconds);
    }

    /**
     * Method updateDelayTimer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateDelayTimer() {
         if (combatDelay > 0) {
            combatDelay--;
        }
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        this.fightingWith = null;
        terminate();
    }

    /**
     * Method getFightingWith
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Killable getFightingWith() {
        return fightingWith;
    }

    /**
     * Method getMob
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public T getMob() {
        return myMob;
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */

    private int nextSmackIn = 0;


    public void addDelayToNextHit(int delay) {
        nextSmackIn += delay;
    }


    public void update() {
        if ((fightingWith != null) && (!isWithinBoundry() ||!isWithinDistanceToTarget())) {
            reset();
            terminate();
            return;
        }


        int speed = Combat.getCombatSpeed(myMob);

        if (myMob.getTimers().timerActive(TimingUtility.MIASMIC_TIMER)) {
            speed *= 0.80;
        }

        if(myMob instanceof Player && myMob.getPlayer().getZombiesGame() != null && myMob.getPlayer().getZombiesGame().timerActive(Zombies.COMBAT_SPEED))
            speed /= 2;



        if(this.nextSmackIn > 0)
            this.nextSmackIn --;
        

        if (combatDelay > 0) {
            return;
        }

        if(this.myMob.isStunned())
            return;

        if(getFightingWith() == null)
            return;

        if (nextSmackIn == 0) {
            executeCombatTick();

            nextSmackIn = speed;
        }
    }
      
    /**
     * Method isWithinBoundry
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract boolean isWithinBoundry();

    /**
     * Method isWithinDistanceToTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract boolean isWithinDistanceToTarget();

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void terminate();

    /**
     * Method executeCombatTick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void executeCombatTick();

    /**
     * Method onTargetAcquired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void onTargetAcquired();
}
