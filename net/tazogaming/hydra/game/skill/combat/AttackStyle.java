package net.tazogaming.hydra.game.skill.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.combat.Damage;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 24/09/13
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
public class AttackStyle {
    public static final int
        TYPE_CRUSH      = 0,
        TYPE_SLASH      = 1,
        TYPE_RANGE      = 2,
        TYPE_STAB       = 3,
        TYPE_MAGIC      = 4,
        MODE_AGGRESSIVE = 1,
        MODE_CONTROLLED = 2,
        MODE_ACCURATE   = 3,
        MODE_LONGRANGE  = 4,
        MODE_RAPID      = 5,
        MODE_DEFENSIVE  = 6;
    private int mode    = 3;
    private int animation;
    private int type;

    /**
     * Constructs ...
     *
     *
     * @param style
     * @param d
     * @param anim
     */
    public AttackStyle(int style, int d, int anim) {
        this.animation = anim;
        this.type      = style;
        this.mode      = d;
    }

    /**
     * Method getMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMode() {
        return mode;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getDamageType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDamageType() {
        return getTypeForStyle(type);
    }

    /**
     * Method getNameForMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param style
     *
     * @return
     */
    public static String getNameForMode(int style) {
        switch (style) {
        case MODE_LONGRANGE :
            return "Longrange";

        case MODE_RAPID :
            return "Rapid";

        case MODE_ACCURATE :
            return "Accurate";

        case MODE_DEFENSIVE :
            return "defencive";

        case MODE_CONTROLLED :
            return "Controlled";

        case MODE_AGGRESSIVE :
            return "Aggressive";
        }

        return "error: " + style;
    }

    /**
     * Method getNameForStyle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param style
     *
     * @return
     */
    public static String getNameForStyle(int style) {
        switch (style) {
        case TYPE_CRUSH :
            return "crush/smash";

        case TYPE_SLASH :
            return "slash";

        case TYPE_STAB :
            return "stab";

        case TYPE_MAGIC :
            return "magic";

        case TYPE_RANGE :
            return "ranged";
        }

        return "error";
    }

    /**
     * Method getTypeForStyle
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param style
     *
     * @return
     */
    public static int getTypeForStyle(int style) {
        switch (style) {
        case TYPE_CRUSH :
            return Damage.TYPE_MELEE_CRUSH;

        case TYPE_STAB :
            return Damage.TYPE_MELEE_STAB;

        case TYPE_SLASH :
            return Damage.TYPE_MELEE_SLASH;

        case TYPE_RANGE :
            return Damage.TYPE_RANGE;

        case TYPE_MAGIC :
            return Damage.TYPE_MAGIC;
        }

        return Damage.TYPE_OTHER;
    }

    /**
     * Method getAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAnimation() {
        return animation;
    }
}
