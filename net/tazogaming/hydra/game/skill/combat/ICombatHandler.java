package net.tazogaming.hydra.game.skill.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 22:12
 * To change this template use File | Settings | File Templates.
 */
public abstract class ICombatHandler {

    /**
     * Method checkDistance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param m
     */
    public abstract void checkDistance(Mob m);

    /**
     * Method doAttack
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param m
     */
    public abstract void doAttack(Mob m);
}
