package net.tazogaming.hydra.game.skill.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Ammunition;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.skill.combat.impact.RangeProjectileHitEvent;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
public class RangeUtil {
    public static final int
        TYPE_ARROW      = 0,
        TYPE_CANNONBALL = 2,
        TYPE_BOLT       = 3,
        TYPE_KARIL_BOLT = 4,
        TYPE_JAVELIN    = 5,
        TYPE_DART       = 6,
        TYPE_THROWNAXE  = 7,
        TYPE_KNIFE      = 8;

    /**
     * Method isDarkBow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isDarkBow(int id) {
        return Item.forId(id).getName().startsWith("Dark");
    }

    /**
     * Method isDragonArrow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isDragonArrow(int id) {
        switch (id) {
        case 11212 :
            return true;

        case 11217 :
            return true;

        case 11222 :
            return true;

        case 11227 :
            return true;

        case 11228 :
            return true;

        case 11229 :
            return true;
        }

        return false;
    }

    /**
     * Method isNoAmmoBow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isNoAmmoBow(int id) {
        return (id == 25) || ((id >= 4214) && (id <= 4223));
    }

    /**
     * Method canUseArrowsWithBow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static boolean canUseArrowsWithBow(Player pla) {
        return canUseArrowsWithBow(pla.getEquipment().getId(3), pla.getEquipment().getId(Equipment.ARROWS));
    }

    /**
     * Method arrowCheck
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static boolean arrowCheck(Player pla) {
        if (!canUseArrowsWithBow(pla)) {
            pla.getActionSender().sendMessage("You cant use this ammo with this weapon.");

            return false;
        }

        return true;
    }

    /**
     * Method canUseArrowsWithBow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bow
     * @param arrow
     *
     * @return
     */
    public static boolean canUseArrowsWithBow(int bow, int arrow) {
        if ((bow == -1) || (arrow == -1)) {
            return false;
        }

        if (isKarilBow(bow)) {
            return arrow == 4740;
        }

        Item i = Item.forId(bow);

        if ((i != null) && (i.getWeaponHandler() != null)) {
            Weapon w    = i.getWeaponHandler();
            int    type = getArrowType(arrow);

            if ((w.getType() == Weapon.TYPE_CROSSBOW) && (type != TYPE_BOLT)) {
                return false;
            }

            if ((w.getType() == Weapon.TYPE_BOW) && (type != TYPE_ARROW)) {
                return false;
            }

            if (w.getType() == Weapon.TYPE_BOW) {
                if (isDragonArrow(arrow) &&!isDarkBow(bow)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Method isKarilBow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public static boolean isKarilBow(int itemId) {
        switch (itemId) {
        case 4934 :
        case 4935 :
        case 4936 :
        case 4937 :
        case 4938 :
        case 4939 :
        case 4734 :
            return true;
        }

        return false;
    }

    /**
     * Method createRangeProjectile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param sentBy
     * @param sentTo
     * @param speed
     * @param waitTicks
     * @param multiplier
     * @param ac
     * @param vars
     */
    public static void createRangeProjectile(int id, Killable sentBy, Killable sentTo, int speed, int waitTicks,
            double multiplier, double ac, int[] vars) {

        // if(vars == null)
        Projectile projectile = new Projectile(sentBy, sentTo, id, speed, Projectile.getWaitspeedForTick(waitTicks));

        if (vars != null) {
            projectile.setVars(vars);
        }

        if (sentBy instanceof Player) {
            Player pla       = (Player) sentBy;
            int    arrowType = pla.getEquipment().getId(Equipment.ARROWS);

            if (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_THROWN) {
                arrowType = pla.getEquipment().getId(Equipment.WEAPON);
            }

            if (RangeUtil.isNoAmmoBow(pla.getEquipment().getId(3))) {
                arrowType = -1;
            }

            RangeProjectileHitEvent e = (new RangeProjectileHitEvent(sentBy, sentTo, arrowType,
                    pla.getEquipment().getId(Equipment.WEAPON), multiplier, ac));
            e.setDelay(projectile.getImpactTick());
            e.onImpact(sentBy, sentTo);
            projectile.setOnImpactEvent(new ImpactEvent() {
                @Override
                public void onImpact(Killable sender, Killable target) {

                }
            });

        }

        sentBy.registerProjectile(projectile);
    }
    public static int preCalcRangeProjectile(int id, Killable sentBy, Killable sentTo, int speed, int waitTicks,
                                             double multiplier, double ac, int[] vars) {

        // if(vars == null)
        Projectile projectile = new Projectile(sentBy, sentTo, id, speed, Projectile.getWaitspeedForTick(waitTicks));

        if (vars != null) {
            projectile.setVars(vars);
        }

        if (sentBy instanceof Player) {
            Player pla       = (Player) sentBy;
            int    arrowType = pla.getEquipment().getId(Equipment.ARROWS);

            if (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_THROWN) {
                arrowType = pla.getEquipment().getId(Equipment.WEAPON);
            }

            if (RangeUtil.isNoAmmoBow(pla.getEquipment().getId(3))) {
                arrowType = -1;
            }


            projectile.setOnImpactEvent(new RangeProjectileHitEvent(sentBy, sentTo, arrowType,
                    pla.getEquipment().getId(Equipment.WEAPON), multiplier, ac));
        }

        sentBy.registerProjectile(projectile);
        return 0;
    }


    public static void createSpecialRangeProjectile(int id, Killable sentBy, Killable sentTo, int speed, int waitTicks,
                                             double multiplier, double ac, int startHeight, int endHeight) {

        // if(vars == null)
        Projectile projectile = new Projectile(sentBy, sentTo, id, speed, Projectile.getWaitspeedForTick(waitTicks));
        projectile.setStartHeight(startHeight);
        projectile.setEndHeight(endHeight);



        if (sentBy instanceof Player) {
            Player pla       = (Player) sentBy;
            int    arrowType = pla.getEquipment().getId(Equipment.ARROWS);

            if (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_THROWN) {
                arrowType = pla.getEquipment().getId(Equipment.WEAPON);
            }

            if (RangeUtil.isNoAmmoBow(pla.getEquipment().getId(3))) {
                arrowType = -1;
            }

            projectile.setOnImpactEvent(new RangeProjectileHitEvent(sentBy, sentTo, arrowType,
                    pla.getEquipment().getId(Equipment.WEAPON), multiplier, ac));
        }

        sentBy.registerProjectile(projectile);
    }

    /**
     * Method createRangeProjectile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param sentBy
     * @param sentTo
     * @param speed
     * @param waitTicks
     * @param multiplier
     * @param ac
    */
    public static void createRangeProjectile(int id, Killable sentBy, Killable sentTo, int speed, int waitTicks,
            double multiplier, double ac) {
        createRangeProjectile(id, sentBy, sentTo, speed, waitTicks, multiplier, ac, null);
    }

    public static int preCalcRangeProjectile(int id, Killable sentBy, Killable sentTo, int speed, int waitTicks,
                                             double multiplier, double ac) {
        return preCalcRangeProjectile(id, sentBy, sentTo, speed, waitTicks, multiplier, ac, null);
    }

    /**
     * Method createRangeProjectile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param sentBy
     * @param sentTo
     * @param speed
     * @param waitTicks
     * @param multiplier
     */
    public static void createRangeProjectile(int id, Killable sentBy, Killable sentTo, int speed, int waitTicks,
            double multiplier) {
        createRangeProjectile(id, sentBy, sentTo, speed, waitTicks, multiplier, 1.00);
    }


    public static int preCalcRangeProjectile(int id, Killable sentBy, Killable sentTo, int speed, int waitTicks,
                                             double multiplier) {
       return preCalcRangeProjectile(id, sentBy, sentTo, speed, waitTicks, multiplier, 1.00);
    }


    /**
     * Method getDBowDrawbackGfx
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static int getDBowDrawbackGfx(int id) {
        if ((id == -1) || (id > 21000)) {
            return 19;
        }

        Item i = Item.forId(id);

        if (i == null) {
            return 19;
        }

        Ammunition e = i.getAmmoData();

        if (e != null) {
            return e.getDarkBowGraphics();
        }

        return 19;
    }

    /**
     * Method getKnifeMoveAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param knifeid
     *
     * @return
     */
    public static int getKnifeMoveAnim(int knifeid) {
        switch (knifeid) {
        case 6522 :
            return 442;

        case 868 :
            return 218;

        case 867 :
            return 217;

        case 866 :
            return 216;

        case 865 :
            return 214;

        case 864 :
            return 213;

        case 863 :
            return 212;

        case 13879 :
            return 1837;
        }

        return 212;
    }

    /**
     * Method getKnifeStartAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param knifeid
     *
     * @return
     */
    public static int getKnifeStartAnim(int knifeid) {
        switch (knifeid) {
        case 6522 :
            return 442;

        case 864 :
            return 219;

        case 865 :
        case 863 :
            return 220;

        case 867 :
            return 224;

        case 868 :
            return 225;

        case 866 :
            return 223;

        case 13879 :
            return 1855;
        }

        return -1;
    }

    /**
     * Method getArrowGfx
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param arrow
     * @param bow
     *
     * @return
     */
    public static int getArrowGfx(int arrow, int bow) {
        if (bow == 4212) {
            return 249;
        }

        if (bow == 6724) {
            return 471;
        }

        if (bow == 15241) {
            return 2143;
        }

        Ammunition e = Item.forId(bow).getAmmoData();

        if (e == null) {
            if (arrow != -1) {
                e = Item.forId(arrow).getAmmoData();
            }
        }

        if (e == null) {
            return -1;
        }

        return e.getProjectileId();
    }

    /**
     * Method getArrowType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param arrow
     *
     * @return
     */
    public static int getArrowType(int arrow) {
        Ammunition m;
        Item       i = Item.forId(arrow);

        if (i == null) {
            return -1;
        }

        m = i.getAmmoData();

        if (m == null) {
            return -1;
        }

        return m.getType();
    }

    /**
     * Method getArrowGfx
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     *
     * @return
     */
    public static int getArrowGfx(Player plr) {
        int bow = plr.getEquipment().getId(Equipment.WEAPON);

        if (bow == -1) {
            return -1;
        }

        if (bow == 4212) {
            return 250;
        }

        if (bow == 6724) {
            return 472;
        }

        if (bow == 15241) {
            return 2143;
        }

        Ammunition e      = Item.forId(bow).getAmmoData();
        int        arrows = plr.getEquipment().getId(Equipment.ARROWS);

        if (e == null) {
            if (arrows != -1) {
                e = Item.forId(arrows).getAmmoData();
            }
        }

        if (e == null) {
            return -1;
        }

        return e.getStartGraphics();
    }
}
