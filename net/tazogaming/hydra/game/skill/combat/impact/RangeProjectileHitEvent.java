package net.tazogaming.hydra.game.skill.combat.impact;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

//~--- non-JDK imports --------------------------------------------------------
import net.tazogaming.hydra.entity3d.Ammunition;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.game.minigame.deathtower.DeathTowerGame;
import net.tazogaming.hydra.game.skill.combat.AttackStyle;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;
import net.tazogaming.hydra.game.skill.combat.PlayerCombatAdapter;
import net.tazogaming.hydra.game.skill.combat.RangeUtil;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 22:52
 * To change this template use File | Settings | File Templates.
 */
public class RangeProjectileHitEvent extends ImpactEvent {

    /** sentArrowId made: 14/10/18 **/
    private int sentArrowId = -1;

    /** bowId made: 14/10/18 **/
    private int bowId = -1;

    /** accuracyMultiplier made: 14/10/18 **/
    private double accuracyMultiplier = 1;
    public boolean byPass             = false;

    /** multiplier made: 14/10/18 **/
    private double multiplier;
    private int delay = 0;

    public void setDelay(int delay){
        this.delay = delay;
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param to
     * @param arrowId
     * @param bowId
     */
    public RangeProjectileHitEvent(Player pla, Killable to, int arrowId, int bowId) {
        this(pla, to, arrowId, bowId, 1.00);
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param to
     * @param arrowId
     * @param bowId
     * @param multiplier
     */
    public RangeProjectileHitEvent(Killable pla, Killable to, int arrowId, int bowId, double multiplier) {
        this(pla, to, arrowId, bowId, multiplier, 1);
    }

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param to
     * @param arrowId
     * @param bowId
     * @param multiplier
     * @param accuracyMultiplier
     */
    public RangeProjectileHitEvent(Killable pla, Killable to, int arrowId, int bowId, double multiplier,
                                   double accuracyMultiplier) {
        super(pla, to);
        this.sentArrowId = arrowId;
        this.bowId       = bowId;
        this.multiplier  = multiplier;
        this.accuracyMultiplier = accuracyMultiplier;
    }
    
    private final int[] DRAGON_IDS = { 50 }; // these might be wrong
    
    /**
     * Check if the npc is a dragon type npc
     * @param npc
     * @return
     */
    private boolean isDragon(NPC npc) {
        for (int id : DRAGON_IDS) {
            if (npc.getId() == id)
                return true;
        }
        return false;
    }

    private boolean applyBoltEffect(Player player, Killable target, int bolt_id) {
        switch (bolt_id) {
        case 9244 :
            if (Combat.GLOBAL_RANDOM.nextInt(100) < 5) {
                int max_dmg = 60;
                int defLvl;

                if (target instanceof NPC) {
                    defLvl = target.getNpc().getDefinition().getCombatStats()[1];
                } else {
                    defLvl = target.getPlayer().getMaxStat(1);

                    if (target.getPlayer().getTimers().timerActive(TimingUtility.DRAGON_FIRE_IMMUNE_TIME) || (target instanceof NPC && isDragon(target.getNpc()))) {
                        max_dmg = 0;
                    }

                    switch (target.getPlayer().getEquipment().getId(Equipment.SHIELD)) {
                    case 1540 :
                        max_dmg *= 0.3;
                    case 11283 :
                    case 11824 :
                        max_dmg *= 0.2;
                    }
                }

                double reduce = (defLvl * 0.2) / 100d;

                max_dmg *= (1.00 - reduce);
                
                if (target.getPlayer().getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_MAGIC)) { // deflect from magic??
                	max_dmg = (int) (max_dmg * 0.60); // 60%
                }
                target.hit(player, GameMath.rand3(max_dmg), 8, 1 + delay, false, max_dmg);
                target.getGraphic().set(446, 0);
                return true;
            }

            break;

        case 9236 :
            if (player.getRandom().nextInt(100) > 60) {
                target.hit(player, player.getRandom().nextInt(10), Damage.TYPE_MAGIC, 1 + delay);
            }

            break;

        case 9237 :
            if (player.getRandom().nextInt(100) > 60) {
                target.getGraphic().set(755, 1);
                target.hit(player, player.getRandom().nextInt(10), Damage.TYPE_HIT_RANGE, 1 + delay);
            }

            break;

        case 9238 :
            if (player.getRandom().nextInt(100) > 60) {
                target.getGraphic().set(750, 1);
                target.hit(player, player.getRandom().nextInt(15), Damage.TYPE_HIT_RANGE, 1 + delay);
            }

            break;

        case 9240 :
            if (player.getRandom().nextInt(100) > 60) {
                if (target instanceof Player) {
                    target.getPlayer().getPrayerBook().removePoints(20);
                    player.getPrayerBook().addPoints(20);
                    target.getGraphic().set(751, 0);
                }
            }

            break;

        case 9241 :
            if (player.getRandom().nextInt(100) > 60) {
                if (target instanceof NPC) {
                    if (!target.isPoisoned() &&!target.getNpc().getDefinition().isPoisonImmune()) {
                        target.poison(Config.MEGA_POISON_TIME);
                    } else if (!target.isPoisoned()) {
                        target.poison(Config.MEGA_POISON_TIME);
                    }
                }
            }

            break;

        case 9242 :
            if ((player.getRandom().nextInt(100) > 97) &&!player.getAreas().contains(DeathTowerGame.TOWER_ZONE)) {
                double drain = player.getCurrentHealth() / 10;

                if (player.getCurrentHealth() - drain > 0) {
                    target.hit(player, target.getCurrentHealth() / 5, Damage.TYPE_HIT_RANGE, delay);
                    target.getGraphic().set(754, 1);
                    player.hit(player, drain, Damage.TYPE_HIT_RANGE, delay);
                }
            }

            break;

        case 9243 :
            if (player.getRandom().nextInt(100) > 70) {
                accuracyMultiplier = 3.60;
                target.getGraphic().set(758, 0);
            }

            break;

            case 9245:
                if(player.getRandom().nextInt(100) > 89) {
                    target.getGraphic().set(753, -1);
                    target.hit(player, GameMath.randomMaxHit(GameMath.rangeMaxHit(player, false, sentArrowId)), Damage.TYPE_RANGE, 1);
                    player.addHealth(target.getCurrentHealth() * 0.25);
                    return true;
                }
            break;

        }
        return false;
    }

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    @Override
    public void onImpact(Killable sender, Killable target) {
        if (sender instanceof Player) {
            final Player pla = (Player) sender;

            if (accuracyMultiplier > 0) {
                pla.setSpecialMultiplier(accuracyMultiplier);
            }

            if (pla.isDead()) {
                return;
            }

            boolean thrown    = false;
            boolean dropArrow = false;

            if ((bowId > 0) && (Item.forId(bowId).getWeaponHandler() != null)) {
                if (Item.forId(bowId).getWeaponHandler().getType() == Weapon.TYPE_THROWN) {
                    thrown    = true;
                    dropArrow = true;
                } else if ((Item.forId(bowId).getWeaponHandler().getType() == Weapon.TYPE_CROSSBOW)
                           || (Item.forId(bowId).getWeaponHandler().getType() == Weapon.TYPE_BOW)) {
                    dropArrow = true;
                }
            }

            if (thrown) {
                sentArrowId = bowId;
            }

            if (sentArrowId != 10034) {
                if (pla.getEquipment().getId(Equipment.CAPE) == 10499 || pla.getEquipment().getId(Equipment.CAPE) == 10498) {
                    if (pla.getRandom().nextInt(100) < 50) {
                        dropArrow = false;

                        if (pla.getEquipment().getId(Equipment.ARROWS) == sentArrowId) {
                            pla.getEquipment().set(sentArrowId, pla.getEquipment().getAmount(Equipment.ARROWS) + 1,
                                                   Equipment.ARROWS);
                            pla.getEquipment().updateEquipment(Equipment.ARROWS);
                        } else {
                            pla.getInventory().addItem(sentArrowId, 1);
                        }
                    }
                } else if (pla.getEquipment().getId(Equipment.CAPE) == 20068) {
                    if (pla.getRandom().nextInt(100) < 80) {
                        dropArrow = false;

                        if (pla.getEquipment().getId(Equipment.ARROWS) == sentArrowId) {
                            pla.getEquipment().set(sentArrowId, pla.getEquipment().getAmount(Equipment.ARROWS) + 1,
                                                   Equipment.ARROWS);
                            pla.getEquipment().updateEquipment(Equipment.ARROWS);
                        } else {
                            pla.getInventory().addItem(sentArrowId, 1);
                        }
                    }
                }

                if (dropArrow && (GameMath.rand(100) < 50)) {
                    new FloorItem(sentArrowId, 1, target.getX(), target.getY(), target.getLocation().getHeight(), pla);
                }
            } else {
                dropArrow = false;
            }

            // chinchompas
            if (target.isInMultiArea() && (sentArrowId == 10034)) {
                if (target instanceof NPC) {
                    NPC                  npc        = (NPC) target;
                    Point                location   = npc.getCenterPoint();
                    final List<NPC> alreadyHit = new ArrayList<NPC>();

                    for (int dx = (location.getX() - (npc.getSize() / 2) - 3);
                            dx < (location.getX() + (npc.getSize() / 2) + 3); dx++) {
                        for (int dy = (location.getY() - (npc.getSize() / 2) - 3);
                                dy < (location.getY() + (npc.getSize() / 2) + 3); dy++) {
                            for (NPC multiTarget : pla.getWatchedNPCs().getAllEntities()) {
                                for (Tile t : NPC.getTiles(multiTarget, multiTarget.getLocation())) {
                                    if ((t.getX() == dx) && (t.getY() == dy) &&!alreadyHit.contains(multiTarget)) {
                                        alreadyHit.add(multiTarget);

                                        if (multiTarget.getSummoner() != null) {
                                            continue;
                                        }

                                        if (multiTarget.canBeAttacked(pla)) {
                                            double dmg = multiTarget.hit(pla,
                                                                      GameMath.randf(GameMath.rangeMaxHit(pla, true, sentArrowId)),
                                                                      Damage.TYPE_RANGE, delay);

                                            PlayerCombatAdapter.addCombatXP((int) (dmg * 1.3d), AttackStyle.TYPE_RANGE,
                                                    pla);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    World.getWorld().submitEvent(new WorldTickEvent(3) {
                        @Override
                        public void tick() {}
                        @Override
                        public void finished() {
                            int c = 0;

                            for (NPC npc : alreadyHit) {
                                if ((npc.getCurrentHealth() == 0) || npc.isDead()) {
                                    c++;
                                }
                            }


                        }
                    });
                }else if(target instanceof Player){
                    for(Player player : target.getPlayer().getWatchedPlayers().getAllEntities()){
                        if(Point.getDistance(pla.getLocation(), target.getLocation()) < 4 && player != sender){
                            if(player.isInMultiArea() && player.isInWilderness()){
                                double dmg = player.hit(pla,
                                        GameMath.randf(GameMath.rangeMaxHit(pla, true, sentArrowId)),
                                        Damage.TYPE_RANGE, delay);
                                player.addXp(4, (int)dmg);
                            }
                        }
                    }
                }
            }

            if ((pla.getEquipment().getWeapon() != null)
                    && (pla.getEquipment().getWeapon().getType() == Weapon.TYPE_CROSSBOW)) {
                if(applyBoltEffect(pla, target, sentArrowId))
                    return;
            }

            if(pla instanceof Player)
                pla.setSpecialMultiplier(accuracyMultiplier);
            
            double damage = target.hit(pla, GameMath.randf(1 + GameMath.rangeMaxHit(pla, multiplier, thrown, sentArrowId)),
                    Damage.TYPE_RANGE, delay, false, (int)GameMath.rangeMaxHit(pla, 1.00, thrown, sentArrowId));
            
            if(pla instanceof Player) {
            	if(RangeUtil.isDragonArrow(sentArrowId) && pla.isDarkbowSpecial()) {
                    if(damage < 8) damage = 8;
                    pla.setDarkbowSpecial(false);
            	}
            }
            
            if(pla instanceof Player)
            pla.setSpecialMultiplier(1);

            if (sender.isPlayer()) {
                if (sender.getPlayer().getEquipment().getId(Equipment.SHIELD) == 20001) {
                    sender.getPlayer().addHealth((int) (damage * 0.30));
                }
            }

            if (target.getCurrentHealth() == 0) {
                if ((sender instanceof Player)
                        && sender.getPlayer().getTimers().timerActive(TimingUtility.ACHIEVEMENT_TIMER)) {

                }
            }

            if ((damage > 0) && (sentArrowId != -1)) {
                if (thrown) {
                    sentArrowId = bowId;
                }

                Ammunition am = Item.forId(sentArrowId).getAmmoData();

                if (am != null) {
                    boolean canPoison = am.getPoisonDamage() > 0;

                    if (target instanceof Player) {
                        canPoison = !(((Player) target)).getTimers().timerActive(TimingUtility.POISON_IMMUNE_TIME);
                    }

                    if (canPoison) {
                        int poisonTicks = (GameMath.roundTen(am.getPoisonDamage()) / 10) * 400;

                        target.poison(poisonTicks);
                    }
                }
            }

            pla.setSpecialMultiplier(1.00);

            if (damage > 0) {
                PlayerCombatAdapter.addCombatXP((int) damage, AttackStyle.TYPE_RANGE, pla);
            }
        }
    }
}
