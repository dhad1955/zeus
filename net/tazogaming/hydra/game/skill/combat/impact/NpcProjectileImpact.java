package net.tazogaming.hydra.game.skill.combat.impact;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 10:48
 */
public class NpcProjectileImpact extends ImpactEvent {
    public static final int
        TYPE_RANGE        = 0,
        TYPE_MAGIC        = 1,
        TYPE_DRAGONBREATH = 2;
    private int   type    = -1;
    private int   maxHit  = 0;
    private NPC   sender;
    private Block nImpact;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param maximumHit
     * @param sent
     * @param impactBlock
     */
    public NpcProjectileImpact(int type, int maximumHit, NPC sent, Block impactBlock) {
        this.type   = type;
        this.sender = sent;
        this.maxHit = maximumHit;
        nImpact     = impactBlock;
    }

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    @Override
    public void onImpact(Killable sender, Killable target) {
        if (nImpact != null) {
            NpcScriptScope scrpt = new NpcScriptScope(this.sender, nImpact);

            scrpt.setInteractingPlayer((Player) target);
            NPCScriptEngine.evaluate(this.sender, scrpt);
        }

        if (maxHit == 0) {
            return;
        }

        if (type == TYPE_RANGE) {
            type = Damage.TYPE_RANGE;
        } else if (type == TYPE_MAGIC) {
            type = Damage.TYPE_MAGIC;
        } else if (type == TYPE_DRAGONBREATH) {
            type = Damage.TYPE_OTHER;
        }

        double hit = target.hit(sender, sender.getRandom().nextInt(maxHit), type, 0);
    }
}
