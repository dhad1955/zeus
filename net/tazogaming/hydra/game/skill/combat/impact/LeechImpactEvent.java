package net.tazogaming.hydra.game.skill.combat.impact;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/10/13
 * Time: 14:20
 */
public class LeechImpactEvent extends ImpactEvent {
    private int     leechId     = -1;
    private int     max         = -1;
    private int     energyDrain = -1;
    private boolean special     = false;
    private int     finishId;

    /**
     * Constructs ...
     *
     *
     * @param finish
     */
    public LeechImpactEvent(int finish) {
        this.finishId = finish;
        special       = true;
    }

    /**
     * Constructs ...
     *
     *
     * @param energy
     * @param finish
     */
    public LeechImpactEvent(int energy, int finish) {
        this.energyDrain = energy;
        this.finishId    = finish;
    }

    /**
     * Constructs ...
     *
     *
     * @param leechId
     * @param max
     * @param id
     */
    public LeechImpactEvent(int leechId, int max, int id) {
        this.leechId  = leechId;
        this.max      = max;
        this.finishId = id;
    }

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    @Override
    public void onImpact(Killable sender, Killable target) {
        target.getGraphic().set(finishId, Graphic.HIGH);

        if (special) {
            Player player = (Player) target;
            double spec   = player.getCurrentSpecial();

            if (spec > 0) {
                SpecialAttacks.removeSpecial(player, 1);

                if (sender.getPlayer().getCurrentSpecial() < 10) {
                    sender.getPlayer().setCurrentSpecial(sender.getPlayer().getCurrentSpecial() + 1);
                    SpecialAttacks.updateSpecial(sender.getPlayer());
                }
            }

            return;
        }

        if (energyDrain > 0) {
            int curEnergy = (int) target.getPlayer().getCurrentEnergy();
            int calc      = curEnergy - 10;

            if (calc < 0) {
                calc = curEnergy;
            }

            target.getPlayer().removeEnergy(calc);
            sender.getPlayer().setCurrentEnergy(sender.getPlayer().getCurrentEnergy() + calc);

            if (sender.getPlayer().getCurrentEnergy() > 100) {
                sender.getPlayer().setCurrentEnergy(100);
            }

            target.getPlayer().getActionSender().updateEnergy();
            sender.getPlayer().getActionSender().updateEnergy();

            return;
        }

        target.apply_leech(sender.getPlayer(), leechId, max);
        sender.getPlayer().getPrayerBook().apply_leech_boost(leechId);

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
