package net.tazogaming.hydra.game.skill.combat.impact;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/10/13
 * Time: 09:08
 */
public class SoulsplitImpactEvent extends ImpactEvent {
    private int damage = 0;

    /**
     * Constructs ...
     *
     *
     * @param dmg
     */
    public SoulsplitImpactEvent(int dmg) {
        this.damage = dmg;
    }

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    @Override
    public void onImpact(Killable sender, Killable target) {
        target.getGraphic().set(2264, Graphic.LOW);

        Projectile soul_split_projectile = new Projectile(target, sender, 2263, 30, 0);

        soul_split_projectile.setAttribute(1, 21);
        soul_split_projectile.setAttribute(2, 21);
        target.registerProjectile(soul_split_projectile);
        sender.getPlayer().addHealth((int) (damage * 0.20));

        if (target instanceof Player) {
            target.getPlayer().getPrayerBook().removePoints((damage / 8));
            ((Player) target).getPrayerBook().updatePrayer();
        }
    }
}
