package net.tazogaming.hydra.game.skill.combat.impact;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/10/13
 * Time: 11:57
 */
public class SummonProjectileImpact extends ImpactEvent {
    private NPC     sender;
    private Killable     target;
    private Player  player;
    private int     type;
    private Graphic endGraphic;
    private int     hitDiff;

    /**
     * Constructs ...
     *
     *
     * @param sender
     * @param target
     * @param master
     * @param type
     * @param hitDiff
     */
    public SummonProjectileImpact(NPC sender, Killable target, Player master, int type, int hitDiff) {
        this.sender  = sender;
        this.target  = target;
        this.player  = master;
        this.type    = type;
        this.hitDiff = hitDiff;
    }

    /**
     * Method setEndGFX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param wait
     */
    public void setEndGFX(int id, int wait) {
        endGraphic = new Graphic();
        endGraphic.set(id, wait);
    }

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    @Override
    public void onImpact(Killable sender, Killable target) {
        int damage = hitDiff;

        if (type == NpcProjectileImpact.TYPE_MAGIC) {
            target.hit(sender.getNpc(), damage, Damage.TYPE_MAGIC, 0, true);
        } else if (type == NpcProjectileImpact.TYPE_RANGE) {
            target.hit(sender.getNpc(), damage, Damage.TYPE_RANGE, 0, true);
        } else {
            target.hit(sender.getNpc(), damage, Damage.TYPE_OTHER, 0, true);
        }

        if (endGraphic != null) {
            target.getGraphic().set(endGraphic.getIndex(), endGraphic.getGraphicWait());
        }
    }
}
