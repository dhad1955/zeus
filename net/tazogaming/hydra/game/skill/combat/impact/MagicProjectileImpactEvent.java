package net.tazogaming.hydra.game.skill.combat.impact;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Graphic;
import net.tazogaming.hydra.game.Sounds;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/10/13
 * Time: 17:32
 */
public class MagicProjectileImpactEvent extends ImpactEvent {
    private int endGfxId     = -1;
    private int endGfxHeight = -1;
    private double damage       = -1;

    /**
     * Constructs ...
     *
     *
     * @param dmg
     * @param endGraphicId
     * @param endGraphicHeight
     */
    public MagicProjectileImpactEvent(double dmg, int endGraphicId, int endGraphicHeight) {
        super();
        this.damage       = dmg;
        this.endGfxId     = endGraphicId;
        this.endGfxHeight = endGraphicHeight;
    }

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    @Override
    public void onImpact(Killable sender, Killable target) {
        double dmg = -1;

        if (damage > 0) {
            dmg = target.hit(sender, damage, Damage.TYPE_MAGIC, 0, true);
            if(endGfxId == 369){
                Sounds.playAreaSound(target.getLocation(), 168);
            }else if(endGfxId == 1677){
                Sounds.playAreaSound(target.getLocation(), 170);
            }

        }

        if (sender.isPlayer()) {
            if (sender.getPlayer().getEquipment().getId(Equipment.SHIELD) == 20001) {
                sender.getPlayer().addHealth((int) (dmg * 0.30));
            }
        }


        if (damage == 0) {
            target.getGraphic().set(85, Graphic.HIGH);
            Sounds.playAreaSound(target.getLocation(), 227);
        } else if (endGfxId != -1) {
            target.getGraphic().set(endGfxId, endGfxHeight);

            if (sender.isPlayer() && (damage > 0)) {
             //   sender.getPlayer().addXp(6, damage * 4);
            }
        }
    }
}
