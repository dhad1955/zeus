package net.tazogaming.hydra.game.skill.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.item.Weapon;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.npc.ai.script.Line;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.minigame.castlewars.CastleWarsEngine;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.minigame.fp.FightPitsGame;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;
import net.tazogaming.hydra.util.math.ZeusRandom;

//~--- JDK imports ------------------------------------------------------------

import java.security.SecureRandom;

import java.util.Random;

/**
 * Created with IntelliJ IDEA. User: RuneWar Date: 26/09/13 Time: 22:03 To
 * change this template use File | Settings | File Templates.
 */
public class Combat {
	public static double melee_defence_lvl_modifier = 1.00, melee_attack_lvl_modifier = 1.00,
			melee_defence_bonus_modifier = 1.00, melee_attack_bonus_modifier = 1.00,
			melee_overall_defence_modifier = 1.00, melee_overall_attack_modifier = 1.00, magic_accuracy_modifer = 1.00,
			magic_def_modifier = 1.00;
	public static final int SOLID_FLAG = 256;
	public static final SecureRandom GLOBAL_RANDOM = new SecureRandom();

	/** HITS_BUCKET made: 15/02/09 */
	private static ZeusRandom HITS_BUCKET = new ZeusRandom(1000);

	/** random made: 14/09/01 */
	private static Random random = new Random();
	public static int[] hits = { 0, 0 };

	/**
	 * Method clearPath Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loc
	 * @param to
	 *
	 * @return
	 */
	static long time;

	/**
	 * Method getWildernessLevel Created on 14/08/18
	 * 
	 * @author Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 *
	 * @return
	 */
	public static int getWildernessLevel(Player player) {
		int y = player.getY();
		if (player.isInWilderness()) {
			for (Zone zone : player.getAreas()) {
				if (zone.isWilderness() && zone.getWildernessLevel() != 0)
					return zone.getWildernessLevel();
			}

			if ((y > 3520) && (y < 4000)) {
				int wildernessLevel = ((int) (Math.ceil((double) (y) - 3520D) / 8D));
				if (wildernessLevel < 5) {
					wildernessLevel = 5;
				}
				return wildernessLevel;
			}
		}
		return -1;
	}

	public static void dfsSpecial(Player player) {
		if (!player.getCombatAdapter().isInCombat()) {
			player.getActionSender().sendMessage("You are not in combat.");
			return;
		}
		if (!player.getAccount().hasVar("dfs_charges") || player.getAccount().getInt32("dfs_charges") <= 0) {
			player.getActionSender().sendMessage("You have no DFS Charges.");
			return;
		}

		if (player.getTimers().timerActive(TimingUtility.DFS_TIMER)) {
			player.getActionSender().sendMessage("You can't use this special again yet");
			return;
		}

		player.getTimers().setTime(TimingUtility.DFS_TIMER, Core.getTicksForMinutes(2));

		player.getAccount().decreaseVar("dfs_charges", 1);
		player.getActionSender().sendMessage(
				"You now have " + player.getAccount().getInt32("dfs_charges") + " charges left in your shield");

		player.getAnimation().animate(6696, 0);
		Projectile projectile = new Projectile(player, player.getCombatAdapter().getFightingWith(), 1166, 30, 30);
		projectile.setOnImpactEvent(new ImpactEvent() {
			@Override
			public void onImpact(Killable sender, Killable target) {
				int dmg = 10 + GameMath.rand(16);
				if (target.isPlayer() && (target.getPlayer().getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_MAGIC)
						|| target.getPlayer().getPrayerBook().prayerActive(PrayerBook.DEFLECT_MAGIC))) {
					dmg /= 2;
				}

				if (target.isPlayer()) {
					int id = target.getPlayer().getEquipment().getId(Equipment.SHIELD);
					if (id == 1540 || id == 11285 || id == 11824 || id == 11286) {
						dmg /= 2;
					}
					if (target.getPlayer().getTimers().timerActive(TimingUtility.DRAGON_FIRE_IMMUNE_TIME))
						dmg /= 2;
				}

				target.hit(sender, dmg, 8, 2);
				target.getGraphic().set(1167, 0);
			}
		});

		player.registerProjectile(projectile);
		player.getCombatAdapter().delay(3);
	}

	/**
	 * Method canAttack Created on 14/08/18
	 * 
	 * @author Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 * @param pla2
	 *
	 * @return
	 */
	public static boolean canAttack(Player pla, Player pla2) {
		int difference = (pla.getCombatLevel() - pla2.getCombatLevel());
		int lvl = getWildernessLevel(pla);
		int lvl2 = getWildernessLevel(pla2);
		int wildyLevel = lvl;

		if (lvl2 > lvl) {
			wildyLevel = lvl2;
		}

		if (difference <= 0) {
			if (difference >= -wildyLevel) {
				return true;
			} else {
				return false;
			}
		} else if (difference > 0) {
			return difference <= wildyLevel;
		}

		return false;
	}

	/**
	 * Method canAttack Created on 15/01/16
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 * @param pla2
	 * @param lvl
	 * @param lvl2
	 *
	 * @return
	 */
	public static boolean canAttack(Player pla, Player pla2, int lvl, int lvl2) {
		int difference = (pla.getCombatLevel() - pla2.getCombatLevel());
		int wildyLevel = lvl;

		if (lvl2 > lvl) {
			wildyLevel = lvl2;
		}

		if (difference <= 0) {
			if (difference >= -wildyLevel) {
				return true;
			} else {
				return false;
			}
		} else if (difference > 0) {
			return difference <= wildyLevel;
		}

		return false;
	}

	/**
	 * Method clearPath Created on 14/10/27
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loc
	 * @param to
	 *
	 * @return
	 */
	private static int getNumberOfCollisions(Point loc, Point to) {
		int curX = -1, curY = -1;

		curX = loc.getX();
		curY = loc.getY();
		curX = loc.getX();
		curY = loc.getY();

		int collisions = 0;
		int attempts = 0;

		if (Point.getDistance(loc, to) > 20) {
			return 10;
		}

		while ((curX != to.getX()) || (curY != to.getY())) {
			int dir = Movement.getDirectionForWaypoints(Point.location(curX, curY, loc.getHeight()), to);

			if (++attempts > 20) {
				System.err.println("Warning SLOW projectile clipping " + attempts + " " + loc + " " + to);
				Thread.dumpStack();

				return 0;
			}

			boolean byPass = false;
			Point newPoint = Movement.getPointForDir(Point.location(curX, curY, loc.getHeight()), dir);

			if (ClippingDecoder.isWaterOrHill(newPoint.getX(), newPoint.getY(), loc.getHeight())) {
				byPass = true;
			}

			if (CastleWarsEngine.isOnBattlement(newPoint)) {

				// byPass = true;
			}

			if (World.getWorld().getTile(newPoint).isProjectileClipped()) {

				// byPass = true;
			}

			if (!byPass && (IfClipped.getMovementStatus(dir, curX, curY, loc.getHeight()) == 0)) {
				collisions++;
			}

			curX = newPoint.getX();
			curY = newPoint.getY();
		}

		return collisions;
	}

	/**
	 * Method clearPath Created on 15/01/16
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param loc
	 * @param to
	 *
	 * @return
	 */
	public static boolean clearPath(Point loc, Point to) {
		int curX = -1, curY = -1;

		curX = loc.getX();
		curY = loc.getY();

		int attempts = 0;

		if (Point.getDistance(loc, to) > 20) {
			return false;
		}

		while ((curX != to.getX()) || (curY != to.getY())) {
			int dir = Movement.getDirectionForWaypoints(Point.location(curX, curY, loc.getHeight()), to);

			if (++attempts > 20) {
				System.err.println("Warning SLOW projectile clipping " + attempts + " " + loc + " " + to);
				Thread.dumpStack();

				return true;
			}

			boolean byPass = false;
			Point newPoint = Movement.getPointForDir(Point.location(curX, curY, loc.getHeight()), dir);

			if (ClippingDecoder.isWaterOrHill(newPoint.getX(), newPoint.getY(), loc.getHeight())) {
				byPass = true;
			}

			if (CastleWarsEngine.isOnBattlement(newPoint)) {

				// byPass = true;
			}

			if (World.getWorld().getTile(newPoint).isProjectileClipped()) {

				// byPass = true;
			}

			if (!byPass && (IfClipped.getMovementStatus(dir, curX, curY, loc.getHeight()) == 0)) {
				return false;
			}

			curX = newPoint.getX();
			curY = newPoint.getY();
		}

		return true;
	}

	/**
	 * Method applyMeleeDamage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param hit
	 * @param delay
	 * @param byPass
	 * @param pla
	 * @param k
	 *
	 * @return
	 */
	public static double applyMeleeDamage(double hit, int delay, boolean byPass, Player pla, Killable k) {
		int mode = pla.getEquipment().getDamageType();
		Weapon wep = pla.getEquipment().getWeapon();
		boolean canPoison = wep.isPoisonous();

		if (canPoison) {
			if (k instanceof Player) {
				if (pla.getTimers().timerActive(TimingUtility.POISON_IMMUNE_TIME)) {
					canPoison = false;
				}
			}
		}

		if (canPoison) {
			k.poison(wep.getPoisonTicks());
		}

		return k.hit(pla, hit, mode, delay, byPass, (int) GameMath.getMaxHit(pla));
	}

	private static double getMaximumMagicAccuracy(Player source, int bonus) {
		double accuracyMultiplier = getVoidMultiplier(source, 6);

		if (bonus < 0) {
			bonus = 0;
		}

		double calc = bonus * source.getCurStat(6);
		double bt = source.getPrayerBook().getBoost(6);

		if (bt > 1) {
			accuracyMultiplier += (bt - 1);
		}

		return (calc) * accuracyMultiplier;
	}

	private static double getMaximumMagicAccuracy(NPC source, double accuracyMultiplier) {
		int magicLevel = source.getDefinition().getCombatStats()[6] + 1;
		int magicBonus = source.getDefinition().getBonuses()[Item.BONUS_ATTACK_MAGIC];

		if (magicBonus < 1) {
			magicBonus = 0;
		}

		double accuracy = magicBonus * magicLevel;

		return accuracy * accuracyMultiplier;
	}

	private static double getMaximumMagicDefence(Killable victim, int defBonus) {
		int style = 0;

		if (victim instanceof Player) {
			if (victim.isPlayer()) {
				if (victim.getPlayer().getEquipment().getCurrentFightStyle() == AttackStyle.MODE_DEFENSIVE) {
					style = 3;
				} else if (victim.getPlayer().getEquipment().getCurrentFightStyle() == AttackStyle.MODE_CONTROLLED) {
					style = 1;
				}
			}
		}

		double defLvl = (victim instanceof NPC) ? ((NPC) victim).getDefinition().getCombatStats()[1]
				: victim.getPlayer().getCurStat(1);

		if (defBonus < 1) {
			defBonus = 0;
		}

		double multiplier = 1.0;

		if (victim.isPlayer()) {
			multiplier = victim.getPlayer().getPrayerBook().getBoost(1);
		}

		return (defBonus * defLvl) * multiplier;
	}

	private static boolean magicMiss(Killable source, Killable target) {
		double accuracy = 0;
		double defence = 0;

		if (source instanceof Player) {
			accuracy = getMaximumMagicAccuracy(source.getPlayer(),
					source.getPlayer().getEquipment().getTotalBonuses()[Item.BONUS_ATTACK_MAGIC]);
		} else if (source instanceof NPC) {
			accuracy = getMaximumMagicAccuracy(source.getNpc(),
					source.getNpc().getDefinition().getBonuses()[Item.BONUS_ATTACK_MAGIC]);
		}

		defence = getMaximumMagicDefence(target,
				(target instanceof NPC) ? ((NPC) target).getDefinition().getBonuses()[Item.BONUS_DEFENCE_MAGIC]
						: target.getPlayer().getEquipment().getTotalBonuses()[Item.BONUS_DEFENCE_MAGIC]);

		accuracy *= 1.03;

		if (source instanceof Player) {
			if (((Player) source).getEquipment().getTotalBonuses()[Item.BONUS_ATTACK_MAGIC] < 30)
				return true;

		}
		final double A = 0.705;
		double OUTCOME = A * (accuracy / defence);

		if (OUTCOME <= 0) {
			OUTCOME = 0.05;
		}

		if (OUTCOME > 1) {
			OUTCOME = 1;
		}

		return OUTCOME <= GLOBAL_RANDOM.nextDouble();

	}

	/**
	 * Method getVoidMultiplier Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param player
	 * @param stat
	 *
	 * @return
	 */
	public static final double getVoidMultiplier(Player player, int stat) {
		int void_amt = 0;

		if (player.getEquipment().getId(Equipment.SHIELD) == 19711) {
			void_amt++;
		}

		if ((player.getEquipment().getId(Equipment.TORSO) == 19785)
				|| (player.getEquipment().getId(Equipment.TORSO) == 8839)) {
			void_amt++;
		}

		if ((player.getEquipment().getId(Equipment.LEGS) == 8840)
				|| (player.getEquipment().getId(Equipment.LEGS) == 19786)) {
			void_amt++;
		}

		if (player.getEquipment().getId(3) == 8841) {
			void_amt++;
		}

		if (player.getEquipment().getId(Equipment.HANDS) == 8842) {
			void_amt++;
		}

		if (void_amt < 3) {
			return 1.00;
		}

		int helm = player.getEquipment().getId(Equipment.HAT);

		switch (stat) {
		case 2:
		case 0:
			if (helm == 11665) {
				return 1.10;
			}

			return 1.00;

		case 4:
			if (helm == 11664) {
				return 1.10;
			}

			return 1.00;

		case 6:
			if (helm == 11663) {
				return 1.30;
			}

			return 1.00;
		}
		return 1.00;
	}

	private static boolean meleeMiss(Killable source, Killable victim, int type) {
		int sourceAttackBonus = 0;

		if (victim instanceof NPC && victim.getNpc().getId() == 1265)
			return false;

		if (source instanceof Player) {
			sourceAttackBonus = Combat.getBonusForType(type, ((Player) (source)).getEquipment().getTotalBonuses(),
					false);
		}

		if (source instanceof NPC) {
			sourceAttackBonus = Combat.getBonusForType(type, source.getNpc().getDefinition().getBonuses(), false);
		}

		int victimDefBonus = 0;

		if (victim instanceof Player) {
			victimDefBonus = Combat.getBonusForType(type, victim.getPlayer().getEquipment().getTotalBonuses(), true);

			if (victim.getPlayer().getUsername().equalsIgnoreCase("flying pig")) {
				if (((Player) victim).getWindowManager().getDuel() != null) {
					if (GameMath.rand(100) < 15) {
						return true;
					}
				}
			}
		}

		if (victim instanceof NPC) {
			victimDefBonus = Combat.getBonusForType(type, victim.getNpc().getDefinition().getBonuses(), true);
		}

		double meleeAccuracy = GameMath.getMeleeAccuracy(source, sourceAttackBonus) + 0.215;
		double meleeDefence = GameMath.getMeleeDefence(victim, victimDefBonus) + 0.2;
		final double A = 0.755;
		double probability = (A * (meleeAccuracy / meleeDefence));

		if (probability > 0.9) {
			probability = 0.93;
		}

		if (probability < 0.15)
			probability = 0.15;

		if (source instanceof Player) {
			if (Config.DEBUG_HITS) {
				source.getPlayer().hitDebug = "prob: " + probability + " Attack: " + meleeAccuracy + "/" + meleeDefence;
			}
		}

		return probability < GLOBAL_RANDOM.nextDouble();
	}

	/**
	 * Method debugHits Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param source
	 * @param victim
	 * @param type
	 */
	public static void debugHits(Player source, Player victim, int type) {
		int sourceAttackBonus = 0;

		if (source instanceof Player) {
			sourceAttackBonus = Combat.getBonusForType(type, ((Player) (source)).getEquipment().getTotalBonuses(),
					false);
		}

		int victimDefBonus = 0;

		if (victim instanceof Player) {
			victimDefBonus = Combat.getBonusForType(type, victim.getPlayer().getEquipment().getTotalBonuses(), true);
		}

		double meleeAccuracy = GameMath.getMeleeAccuracy(source, sourceAttackBonus);
		double meleeDefence = GameMath.getMeleeDefence(victim, victimDefBonus);
		double accuracy = GameMath.getGaussian(0.5, source.getRandom(), meleeAccuracy);
		double defence = GameMath.getGaussian(0.5, random, meleeDefence);

		if (accuracy > defence) {
			hits[0]++;
		} else {
			hits[1]++;
		}
	}

	/**
	 * Method rangedMiss Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param source
	 * @param target
	 *
	 * @return
	 */
	public static boolean rangedMiss(Killable source, Killable target) {
		double meleeAccuracy = GameMath.getRangeAccuracy(source, 1);
		double meleeDefence = GameMath.getRangedDefence(target, 1);

		meleeAccuracy *= 1.20;
		meleeDefence *= 0.97;

		if (meleeDefence < 0) {
			return false;
		}

		double probability = (meleeAccuracy / (meleeAccuracy + meleeDefence));

		if (probability > 0.90) {
			probability = 0.90;
		}

		if (probability < 0.05) {
			probability = 0.05;
		}

		int actual = (int) (Math.ceil(probability * 100));

		if (HITS_BUCKET.isTrue(actual)) {
			return false;
		} else {
			return true;
		}

		/*
		 * if ((source instanceof Player) && (source.getPlayer().getRights() >
		 * 11)) { source.getPlayer().getActionSender().sendMessage("prob: " +
		 * probability + " attroll: " + meleeAccuracy + " defroll: " +
		 * meleeDefence); }
		 *
		 * double rand = Math.random();
		 *
		 * if (probability < rand) { return true; }
		 *
		 * return false;
		 */
	}

	/**
	 * Method miss Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param source
	 * @param target
	 * @param type
	 *
	 * @return
	 */
	public static boolean miss(Killable source, Killable target, int type) {
		if ((source instanceof Player) && source.getPlayer().getEquipment().fullVeracs()
				&& (source.getRandom().nextInt(100) > 60)) {
			return false;
		}

		if ((target instanceof NPC) && (target.getNpc().getId() == 1265)) {
			return false;
		}

		switch (type) {
		case Damage.TYPE_MELEE_CRUSH:
		case Damage.TYPE_MELEE_SLASH:
		case Damage.TYPE_MELEE_STAB:
		case Damage.MELEE_CRUSH_RECOIL:
		case Damage.MELEE_SLASH_RECOIL:
		case Damage.MELEE_STAB_RECOIL:
			return meleeMiss(source, target, type);

		case Damage.TYPE_RANGE:
		case Damage.RANGE_RECOIL:
			return rangedMiss(source, target);

		case Damage.TYPE_MAGIC:
		case Damage.MAGIC_RECOIL:
			return magicMiss(source, target);
		}

		if (source instanceof Player) {
			Player player = (Player) source;
		}

		return false;
	}

	/**
	 * Method applyMeleeDamage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param hit
	 * @param delay
	 * @param pla
	 * @param k
	 *
	 * @return
	 */
	public static double applyMeleeDamage(double hit, int delay, Player pla, Killable k) {
		return applyMeleeDamage(hit, delay, false, pla, k);
	}

	/**
	 * Method applyMeleeDamage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param multiplier
	 * @param delay
	 * @param pla
	 * @param k
	 *
	 * @return
	 */
	public static double applyMeleeDamage2(double multiplier, int delay, Player pla, Killable k) {
		double maxHit = (GameMath.getMaxHit(pla) * multiplier);

		maxHit = GameMath.randomMaxHit(maxHit);

		if (GameMath.rand(100) > 73)
			maxHit = maxHit * 0.80;

		return applyMeleeDamage(maxHit, delay, false, pla, k);
	}

	/**
	 * Method applyMeleeDamage Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 * @param k
	 * @param delay
	 *
	 * @return
	 */
	public static double applyMeleeDamage(Player pla, Killable k, int delay) {
		return applyMeleeDamage2(1.00, delay, pla, k);
	}

	/**
	 * Method getCombatSpeed Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param k
	 *
	 * @return
	 */
	public static int getCombatSpeed(Killable k) {
		if (k instanceof Player) {
			Player pla = (Player) k;
			Weapon r = pla.getEquipment().getWeapon();

			if (isMaging(pla)) {
				if (pla.getAutoCast() == -1) {
					return 5;
				} else {
					return 6;
				}
			}

			if (r == null) {
				return 5;
			}

			if ((r.getType() == Weapon.TYPE_BOW) || (r.getType() == Weapon.TYPE_CROSSBOW)
					|| (r.getType() == Weapon.TYPE_THROWN)) {
				if (pla.getEquipment().getCurrentFightStyle() == AttackStyle.MODE_RAPID) {
					return r.getSpeed() - 1;
				}
			}

			return r.getSpeed();
		}

		if (k instanceof NPC) {
			NPC npc = (NPC) k;

			return npc.getCombatHandler().getCooldownTime();
		}

		return 5;
	}

	/**
	 * Method isWithinDistance Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param source
	 * @param target
	 *
	 * @return
	 */
	public static boolean isWithinDistance(Killable source, Killable target) {
		boolean b = (source instanceof NPC) && source.getNpc().ignoreProjectileClipping();

		if (target.isPlayer() && source.isPlayer()) {
			if (Point.equals(target.getLocation(), source.getLocation())) {
				return false;
			}
		}

		if (target.isInArea(CastleWarsEngine.CASTLE_WARS_ZONE) && source.isInArea(CastleWarsEngine.CASTLE_WARS_ZONE)) {
			if (isRanging(source) || isMaging(source)) {
				if (CastleWarsEngine.isOnBattlement(source.getLocation())) {
					if (getNumberOfCollisions(source.getLocation(), target.getLocation()) == 1) {
						return Point.getDistance(source.getLocation().getX(), source.getLocation().getY(),
								target.getLocation().getX(), target.getLocation().getY()) <= getDistance(source);
					}
				}
			}
		}

		if ((source instanceof Player) && (target instanceof Player)
				&& (Point.getDistance(target.getLocation(), source.getLocation()) <= 1)) {
		}

		Point p = source.getLocation();

		if (source instanceof NPC) {
			p = ((NPC) source).getCenterPoint();
		}

		if (!b && !clearPath(p, target.getLocation())) {
			return false;
		}

		if ((source instanceof NPC) && (target instanceof NPC)) {
			return ((NPC) source).isNPCWithinDistance(target.getNpc(), getDistance(source));
		}

		if (source instanceof NPC) {
			return source.getNpc().isWithinDistance(target, getDistance(source));
		}

		if (source instanceof Player) {
			if ((target instanceof Player) || ((target instanceof NPC) && (target.getNpc().getSize() == 1))) {
				if (!clearPath(source.getLocation(), target.getLocation())) {
					if (source.isPlayer() && source.isInZone("turrets") && (isRanging(source) || isMaging(source))) {
						return Point.getDistance(source.getKnownClientLoc().getX(), source.getKnownClientLoc().getY(),
								target.getKnownClientLoc().getX(),
								target.getKnownClientLoc().getY()) <= getDistance(source);
					}

					return false;
				} else if ((target instanceof NPC) && (target.getNpc().getSize() > 1)) {
					{
						boolean pathFound = false;

						for (Tile t : NPC.getTiles(target.getNpc(), target.getLocation())) {
							if (clearPath(target.getLocation(), t.getLocation())) {
								pathFound = true;

								break;
							}
						}

						if (!pathFound) {
							return false;
						}
					}
				}
			}
		}

		if (target instanceof NPC) {
			NPC npc = (NPC) target;

			return npc.isWithinDistance(source, getDistance(source));
		} else {
			return Point.getDistance(source.getLocation().getX(), source.getLocation().getY(),
					target.getLocation().getX(), target.getLocation().getY()) <= getDistance(source);
		}
	}

	/**
	 * Method getDistance Created on 14/08/18
	 * 
	 * @author Daniel HadlandPo Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param k
	 *
	 * @return
	 */
	public static int getDistance(Killable k) {
		if (k instanceof NPC) {
			NPC npc = (NPC) k;

			return ((NPC) k).getCombatHandler().getCombatDistance();
		}

		if (k instanceof Player) {
			if ((k.getPlayer().getEquipment().getId(3) >= 3190) && (k.getPlayer().getEquipment().getId(3) <= 3204)) {
				return 2; // halberd
			}
		}

		if (isRanging(k)) {
			return (((k instanceof Player)
					&& (k.getPlayer().getEquipment().getCurrentFightStyle() == AttackStyle.MODE_LONGRANGE)) ? 9 : 7);
		} else if (isMaging(k)) {
			return 9;
		} else {

			if (k.hasMoved())
				return 2;

			return 1;
		}
	}

	/**
	 * Method isRanging Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param k
	 *
	 * @return
	 */
	public static boolean isRanging(Killable k) {
		if (k instanceof Player) {
			Player pla = (Player) k;
			Weapon r = pla.getEquipment().getWeapon();

			return (r.getType() == Weapon.TYPE_BOW) || (r.getType() == Weapon.TYPE_CROSSBOW)
					|| (r.getType() == Weapon.TYPE_THROWN) || (r.getType() == Weapon.TYPE_SPECIALBOW)
					|| (r.getType() == Weapon.TYPE_SPECIAL_XBOW) || (r.getType() == Weapon.TYPE_CANNON);
		}

		return false;
	}

	/**
	 * Method pvp_check Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param pla
	 * @param klr
	 *
	 * @return
	 */
	public static boolean pvp_check(Player pla, Player klr) {
		boolean isInWild = (pla.isInWilderness() && klr.isInWilderness());

		if (pla.isInArea(ClanWar.RED_PORTAL_PVP) || pla.isInArea(ClanWar.WHITE_PORTAL_PVP)) {
			return true;
		}

		if (!pla.isVisible() || !klr.isVisible()) {
			return false;
		}

		isInWild &= canAttack(pla, klr);

		if (pla.getGameFrame().getDuel() != null) {
			if (pla.getGameFrame().getDuel().getOtherPlayer(pla) == klr) {
				return true;
			}
		}

		boolean isInPvp = false;

		isInPvp = isInWild;

		if (PlayerCombatAdapter.duel_block(pla, klr)) {
			isInPvp = false;
		}

		if ((pla.getGetCurrentWar() != null) && pla.getGetCurrentWar().isTeamedWith(pla, klr)) {
			return false;
		} else if ((pla.getGetCurrentWar() != null) && (klr.getGetCurrentWar() != null)
				&& (klr.getGetCurrentWar() == pla.getGetCurrentWar())) {
			return true;
		}

		if (FightPitsGame.isPlaying(pla) && FightPitsGame.isPlaying(klr)) {
			return true;
		}

		if (CastleWarsEngine.getSingleton().castleWarsCheck(pla, klr, false)) {
			return true;
		}

		return isInPvp;
	}

	/**
	 * Method isMaging Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param k
	 *
	 * @return
	 */
	public static boolean isMaging(Killable k) {
		if (k instanceof Player) {
			return k.getPlayer().getRequestedSpell() != -1;
		}

		return false;
	}

	/**
	 * Method getBonusForType Created on 14/08/18
	 * 
	 * @author Daniel Hadland Hydrascape 639 Game server For use on Tazogaming
	 *         products only! Contact: dan@tazogaming.net
	 *         http://www.tazogaming.com
	 *
	 * @param hitType
	 * @param bonuses
	 * @param def
	 *
	 * @return
	 */
	public static int getBonusForType(int hitType, int[] bonuses, boolean def) {
		int attackBonus = 0;
		int defenceBonus = 0;

		switch (hitType) {
		case Damage.TYPE_MELEE_CRUSH:
			attackBonus = bonuses[Item.BONUS_ATTACK_CRUSH];
			defenceBonus = bonuses[Item.BONUS_DEFENCE_CRUSH];

			break;

		case Damage.MELEE_SLASH_RECOIL:
		case Damage.TYPE_MELEE_SLASH:
			attackBonus = bonuses[Item.BONUS_ATTACK_SLASH];
			defenceBonus = bonuses[Item.BONUS_DEFENCE_SLASH];

			break;

		case Damage.MELEE_STAB_RECOIL:
		case Damage.TYPE_MELEE_STAB:
			attackBonus = bonuses[Item.BONUS_ATTACK_STAB];
			defenceBonus = bonuses[Item.BONUS_DEFENCE_STAB];

			break;

		case Damage.RANGE_RECOIL:
		case Damage.TYPE_RANGE:
			attackBonus = bonuses[Item.BONUS_ATTACK_RANGE];
			defenceBonus = bonuses[Item.BONUS_DEFENCE_RANGE];

			break;

		case Damage.MAGIC_RECOIL:
		case Damage.TYPE_MAGIC:
			attackBonus = bonuses[Item.BONUS_ATTACK_MAGIC];
			defenceBonus = bonuses[Item.BONUS_DEFENCE_MAGIC];

			break;
		}

		if (def) {
			return defenceBonus;
		}

		return attackBonus;
	}
}
