package net.tazogaming.hydra.game.skill.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.combat.Killable;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 27/09/13
 * Time: 22:26
 * To change this template use File | Settings | File Templates.
 */
public abstract class ImpactEvent {
    private Killable sender;
    private Killable target;

    /**
     * Constructs ...
     *
     */
    public ImpactEvent() {}

    /**
     * Constructs ...
     *
     *
     * @param sender
     * @param target
     */
    public ImpactEvent(Killable sender, Killable target) {}

    /**
     * Method onImpact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     * @param target
     */
    public abstract void onImpact(Killable sender, Killable target);
}
