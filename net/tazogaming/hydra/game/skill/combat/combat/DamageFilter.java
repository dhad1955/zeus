package net.tazogaming.hydra.game.skill.combat.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.filter.PlayerDefenceDamageFilter;
import net.tazogaming.hydra.game.skill.combat.filter.PrayerDamageFilter;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 21:47
 * To change this template use File | Settings | File Templates.
 */
public abstract class DamageFilter {
    public static final DamageFilter PRAYER_DAMAGE_FILTER         = new PrayerDamageFilter();
    public static final DamageFilter PLAYER_DEFENCE_DAMAGE_FILTER = new PlayerDefenceDamageFilter();

    /**
     * Method filterDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damage
     * @param myMob
     * @param hitBy
     * @param hitType
     *
     * @return
     */
    public abstract double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType);
}
