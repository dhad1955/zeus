package net.tazogaming.hydra.game.skill.combat.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/11/13
 * Time: 06:21
 */
public class PVPKill {
    private long killed;
    private int  killedAt;

    /**
     * Constructs ...
     *
     *
     * @param p
     */
    public PVPKill(Player p) {
        killed   = p.getUID();
        killedAt = Core.currentTime + Core.getTicksForMinutes(60);
    }

    /**
     * Constructs ...
     *
     *
     * @param l
     * @param time
     */
    public PVPKill(long l, int time) {
        killed   = l;
        killedAt = Core.currentTime + time;
    }

    /**
     * Method getKilledAt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getKilledAt() {
        return killedAt;
    }

    /**
     * Method remaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int remaining() {
        return killedAt - Core.currentTime;
    }

    /**
     * Method setKilledAt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killedAt
     */
    public void setKilledAt(int killedAt) {
        this.killedAt = killedAt;
    }

    /**
     * Method getKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getKilled() {
        return killed;
    }

    /**
     * Method setKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killed
     */
    public void setKilled(long killed) {
        this.killed = killed;
    }
}
