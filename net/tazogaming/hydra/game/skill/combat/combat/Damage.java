package net.tazogaming.hydra.game.skill.combat.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.ImpactEvent;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */
public class Damage {
    public static final int
        TYPE_MISS                        = 0,
        TYPE_HIT                         = 1,
        TYPE_POISON                      = 2,
        TYPE_MELEE_CRUSH                 = 3,
        TYPE_MELEE_SLASH                 = 4,
        TYPE_MELEE_STAB                  = 5,
        TYPE_RANGE                       = 6,
        TYPE_MAGIC                       = 7,
        TYPE_OTHER                       = 8,
        RANGE_RECOIL                     = 9,
        MELEE_STAB_RECOIL                = 10,
        MELEE_SLASH_RECOIL               = 11,
        MELEE_CRUSH_RECOIL               = 12,
        MAGIC_RECOIL                     = 13,
        TYPE_HIT_RANGE = 14, TYPE_HIT_MAGIC = 15;

    private double         damage           = 0;
    private int         type             = 0;
    private int         damageTookAt     = 0;
    private int         damageDelay      = 0;
    private double      damageMultiplier = 1.0;
    private int         damageType       = -1;
    private int         realType         = 0;
    private Mob         damageDoneBy;
    private boolean     fakeDamage;

    public double getOriginalDamage() {
        return originalDamage;
    }

    public void setOriginalDamage(double originalDamage) {
        this.originalDamage = originalDamage;
    }

    private double originalDamage = 0;
    private ImpactEvent impactEvent;

    /**
     * Constructs ...
     *
     *
     * @param dmg
     * @param m
     * @param type
     */
    public Damage(double dmg, Mob m, int type) {
        this(dmg, type, m, 0);
    }

    /**
     * Constructs ...
     *
     *
     * @param dmg
     * @param type
     * @param m
     * @param delay
     */
    public Damage(double dmg, int type, Mob m, int delay) {
        this.damage       = dmg;
        this.type         = type;
        damageTookAt      = Core.currentTime;
        damageDelay       = delay;
        this.damageDoneBy = m;
    }

    /**
     * Method setRealType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setRealType(int type) {
        this.realType = type;
    }

    /**
     * Method getClientType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public final int getClientType() {
        switch (getOrigType()) {
        case MELEE_STAB_RECOIL :
        case MAGIC_RECOIL :
        case RANGE_RECOIL :
        case MELEE_SLASH_RECOIL :
        case MELEE_CRUSH_RECOIL :
            return 4;    // deflect

        case TYPE_MELEE_CRUSH :
        case TYPE_MELEE_SLASH :
        case TYPE_MELEE_STAB :
            return 0;    // melee

        case TYPE_RANGE :
        case TYPE_HIT_RANGE:
            return 1;

        case TYPE_MAGIC :
        case TYPE_HIT_MAGIC:

            return 2;

        case TYPE_POISON :
            return 6;

        default :
            return 3;
        }
    }

    /**
     * Method getDamageMultiplier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public double getDamageMultiplier() {
        return damageMultiplier;
    }

    /**
     * Method setDamageMultiplier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damageMultiplier
     */
    public void setDamageMultiplier(double damageMultiplier) {
        this.damageMultiplier = damageMultiplier;
    }

    /**
     * Method getOrigType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOrigType() {
        return damageType;
    }

    /**
     * Method setOrigType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setOrigType(int type) {
        damageType = type;
    }

    private double soakedAmount = -1;
    public double getSoakedAmount() {
        return soakedAmount;
    }
    public void setSoaked(double soaked){
        this.soakedAmount = soaked;
    }

    private int crit = 0;

    public double getSoakPerc() {
        return soakPerc;
    }

    public void setSoakPerc(double soakPerc) {
        this.soakPerc = soakPerc;
    }

    private double soakPerc;
    public boolean isCrit() {
        return crit > 0 &&  damage >= crit;
    }
    public void setCrit(int crit){
        this.crit = crit;
    }
    /**
     * Method getImpactEvent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ImpactEvent getImpactEvent() {
        return impactEvent;
    }

    /**
     * Method setImpactEvent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param impactEvent
     */
    public void setImpactEvent(ImpactEvent impactEvent) {
        this.impactEvent = impactEvent;
    }

    /**
     * Method isFakeDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isFakeDamage() {
        return fakeDamage;
    }

    /**
     * Method setFakeDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param fakeDamage
     */
    public void setFakeDamage(boolean fakeDamage) {
        this.fakeDamage = fakeDamage;
    }

    /**
     * Method setDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dmg
     */
    public void setDamage(double dmg) {
        this.damage = dmg;

        if ((dmg == 0) && (this.type == TYPE_HIT)) {
            type = TYPE_MISS;
        }
    }

    /**
     * Method ready
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean ready() {
        return Core.timeSince(damageTookAt) >= damageDelay;
    }

    /**
     * Method getDamager
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Mob getDamager() {
        return damageDoneBy;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public double getDamage() {
        return Math.round(10 * damage) / 10d;
    }


    /**
     * Method getRecoilForType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dmg
     *
     * @return
     */
    public static int getRecoilForType(int dmg) {
        if (dmg == TYPE_MELEE_CRUSH) {
            return MELEE_CRUSH_RECOIL;
        }

        if (dmg == TYPE_MELEE_SLASH) {
            return MELEE_SLASH_RECOIL;
        }

        if (dmg == TYPE_MELEE_STAB) {
            return MELEE_STAB_RECOIL;
        }

        if (dmg == TYPE_RANGE) {
            return RANGE_RECOIL;
        }

        if (dmg == TYPE_MAGIC) {
            return MAGIC_RECOIL;
        }

        return dmg;
    }

    /**
     * Method isNormalDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isNormalDamage() {
        return (getOrigType() >= 3) && (getOrigType() <= 7);
    }
}
