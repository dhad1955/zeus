package net.tazogaming.hydra.game.skill.combat.combat;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.masks.FacingLocation;
import net.tazogaming.hydra.entity3d.player.masks.Interacting;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.HitRecord;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.security.SecureRandom;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 26/09/13
 * Time: 21:36
 * To change this template use File | Settings | File Templates.
 */
public abstract class Killable extends Mob {

    /** currentHealth made: 14/10/13 */
    private double currentHealth = 0;

    /** maxHealth made: 14/10/13 */
    private int maxHealth = 0;

    /** isDead made: 14/10/13 */
    private boolean isDead = false;

    /** damage1 made: 14/10/13 */
    private Damage damage1 = null;

    /** damage2 made: 14/10/13 */
    private Damage damage2 = null;

    /** damageFilterMap made: 14/10/13 */
    private Map<String, DamageFilter> damageFilterMap = new LinkedHashMap<String, DamageFilter>();

    /** damageQueue made: 14/10/13 */
    private ArrayList<Damage> damageQueue = new ArrayList<Damage>();

    /** projectiles made: 14/10/13 */
    private ArrayList<Projectile> projectiles = new ArrayList<Projectile>();

    /** projectilesToUpdate made: 14/10/13 */
    private ArrayList<Projectile> projectilesToUpdate = new ArrayList<Projectile>();

    /** timers made: 14/10/13 */
    private TimingUtility timers = new TimingUtility();

    /** curAnim made: 14/10/13 */
    private Animation curAnim = new Animation();

    /** curGraphic made: 14/10/13 */
    private Graphic curGraphic = new Graphic();

    /** focus made: 14/10/13 */
    private Interacting focus = new Interacting();

    /** facingLocation made: 14/10/13 */
    private FacingLocation facingLocation = new FacingLocation();

    /** damage_list made: 14/10/13 */
    private HashMap<Mob, Integer> damage_list = new HashMap<Mob, Integer>();

    /** invalidateDamage made: 14/10/13 */
    private boolean invalidateDamage = false;

    /** curse_modifier made: 14/10/13 */
    private int[] curse_modifier = new int[] {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    /** curse_boost made: 14/10/13 */
    private int[] curse_boost = new int[] {
        0, 0, 0, 0, 0, 0, 0, 0
    };

    /** cursedBy made: 14/10/13 */

    /** lastDrain made: 14/10/13 */
    private int lastDrain = Core.currentTime;

    /** random made: 14/10/13 */
    private SecureRandom random = new SecureRandom();

    /** poisonTime made: 14/10/13 */
    private int poisonTime = 0;

    /** hit_records made: 15/02/07 **/
    private Map<Killable, HitRecord> hit_records = new HashMap<Killable, HitRecord>();

    /** stunTime made: 15/02/07 **/
    private int stunTime;

    /** cachedPlayer made: 14/10/13 */
    private Player cachedPlayer;

    /** cachedNpc made: 14/10/13 */
    private NPC cachedNpc;

    /** lastHitBy made: 14/10/13 */
    private Killable lastHitBy;

    /** lastHitAt made: 14/10/13 */
    private int lastHitAt;

    /**
     * Method recordDamage
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     * @param damage
     */
    public void recordDamage(Killable hitBy, int damage) {
        if ((hitBy instanceof NPC) && this.isInWilderness()) {
            return;
        }

        if (!this.hit_records.containsKey(hitBy)) {
            this.hit_records.put(hitBy, new HitRecord(hitBy, damage));
        } else {
            this.hit_records.get(hitBy).increaseDamage(damage);
        }
    }

    /**
     * Method pollHighestHitter
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Mob pollHighestHitter() {
        HitRecord highestHitter = null;

        for (HitRecord hitByRecords : hit_records.values()) {
            if (highestHitter != null) {
                if ((highestHitter.getDamagedBy() instanceof Player)
                        &&!((Player) highestHitter.getDamagedBy()).isLoggedIn()) {
                    continue;
                }
            }

            if ((highestHitter == null) || (highestHitter.getDamage() < hitByRecords.getDamage())) {
                highestHitter = hitByRecords;
            }
        }

        hit_records.clear();

        if (highestHitter == null) {
            return this;
        }

        if ((highestHitter.getDamagedBy() instanceof NPC)
                && ((NPC) highestHitter.getDamagedBy()).getNpc().isSummoned()) {
            return ((NPC) highestHitter.getDamagedBy()).getSummoner();
        }

        return highestHitter.getDamagedBy();
    }

    /**
     * Method apply_leech
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param skillId
     * @param max
     */
    public void apply_leech(Player player, int skillId, int max) {
        curse_modifier[skillId]++;

        if (curse_modifier[skillId] > max) {
            curse_modifier[skillId] = max;
        }
    }

    /**
     * Method isStunned
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isStunned() {
        return Core.timeUntil(stunTime) > 0;
    }

    /**
     * Method stun
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     */
    public void stun(int time) {
        this.stunTime = Core.currentTime + time;
    }

    /**
     * Method reset_curse_drain
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset_curse_drain() {
        for (int i = 0; i < curse_modifier.length; i++) {
            curse_modifier[i] = 0;
        }
    }

    /**
     * Method getLastHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLastHit() {
        return lastHitAt;
    }

    /**
     * Method update_curse_drain
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update_curse_drain() {
        if (isBeingLeeched()) {
            return;
        }

        for (int i = 0; i < curse_modifier.length; i++) {
            if (curse_modifier[i] > 1) {
                curse_modifier[i]--;
            }
        }
    }

    /**
     * Method isBeingLeeched
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBeingLeeched() {
        return (lastHitBy != null) && (lastHitBy.getPlayer() != null)
               && lastHitBy.getPlayer().getPrayerBook().isCurses()
               && (lastHitBy.getPlayer().getCombatAdapter().getFightingWith() == this);
    }

    /**
     * Method getCurseModifier
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param skill
     *
     * @return
     */
    public double getCurseModifier(int skill) {
        int    drain = get_curse_drain(skill);
        double d     = 1;

        if (drain > 0) {
            d -= (drain / 100d);
        }

        int boost = curse_boost[skill];

        if (boost > 0) {
            d += (curse_boost[skill] / 100d);
        }

        return d;
    }

    /**
     * Method get_curse_total
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int get_curse_total(int id) {
        return curse_modifier[id] - get_curse_drain(id);
    }

    /**
     * Method get_curse_drain
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int get_curse_drain(int id) {
        return get_static_drain(id) + curse_modifier[id];
    }

    /**
     * Method get_static_drain
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int get_static_drain(int id) {
        Killable k = lastHitBy;

        if (k instanceof NPC) {
            return 0;
        }

        if (lastHitBy == null) {
            return 0;
        }

        Player player = lastHitBy.getPlayer();

        if (!player.isLoggedIn() || (player.getCombatAdapter().getFightingWith() != this)) {
            return 0;
        }

        PrayerBook book = player.getPrayerBook();

        switch (id) {
        case 0 :
            if (book.curseActive(PrayerBook.LEECH_ATTACK)) {
                return 10;
            }

            return 0;

        case 1 :
            if (book.curseActive(PrayerBook.LEECH_DEFENCE)) {
                return 10;
            }

            return 0;

        case 2 :
            if (book.curseActive(PrayerBook.LEECH_STRENGTH)) {
                return 10;
            }

            return 0;

        case 4 :
            if (book.curseActive(PrayerBook.LEECH_RANGE)) {
                return 10;
            }

            return 0;

        case 6 :
            if (book.curseActive(PrayerBook.LEECH_MAGIC)) {
                return 10;
            }

            break;
        }

        return 0;
    }

    /**
     * Method invalidateDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void invalidateDamage() {
        if (damageQueue.size() > 0) {
            invalidateDamage = true;
        }
    }

    /**
     * Method timerUp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param timer
     */
    public abstract void timerUp(int timer);

    /**
     * Method getFacingLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public FacingLocation getFacingLocation() {
        return facingLocation;
    }

    /**
     * Method getCombatLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCombatLevel() {
        if (isNPC()) {
            return 126;
        }

        if (isPlayer()) {
            return (int) GameMath.combatLevel(cachedPlayer.getMaxStat(0), cachedPlayer.getMaxStat(1),
                                              cachedPlayer.getMaxStat(2), cachedPlayer.getMaxStat(3),
                                              cachedPlayer.getMaxStat(5), cachedPlayer.getMaxStat(6),
                                              cachedPlayer.getMaxStat(4), cachedPlayer.getMaxStat(23));
        }

        return 0;
    }

    /**
     * Method isNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isNPC() {
        if (this instanceof NPC) {
            cachedNpc = (NPC) this;
        }

        if (cachedNpc != null) {
            return true;
        }

        return false;
    }

    /**
     * Method isPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isPlayer() {
        if (this instanceof Player) {
            cachedPlayer = (Player) this;
        }

        if (cachedPlayer != null) {
            return true;
        }

        return false;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        if (isPlayer()) {
            return cachedPlayer;
        }

        return null;
    }

    /**
     * Method getNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC getNpc() {
        if (isNPC()) {
            return cachedNpc;
        }

        return null;
    }

    /**
     * Method getPlayersThatHitAbove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     *
     * @return
     */
    public ArrayList<Player> getPlayersThatHitAbove(int amt) {
        Mob               highest    = null;
        int               highestHit = -1;
        ArrayList<Player> returnList = new ArrayList<Player>();

        for (Mob pla : damage_list.keySet()) {
            if ((pla instanceof NPC)
                    || (((pla instanceof Player) && ((Player) pla).getPlayer().isLoggedIn())
                        && (Point.getDistance(pla.getLocation(), getLocation()) < 15))) {
                int damage = damage_list.get(pla);

                if ((highestHit == -1) || (damage >= amt)) {
                    highestHit = damage;

                    if ((pla instanceof NPC) && ((NPC) pla).getNpc().isSummoned()) {
                        pla = ((NPC) pla).getNpc().getSummoner();
                    }

                    if ((pla != null) && (pla instanceof Player)) {
                        returnList.add((Player) pla);
                    }
                }
            }
        }

        return returnList;
    }

    /**
     * Method getHighestHitter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */

    /**
     * Method getRandom
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Random getRandom() {
        return Combat.GLOBAL_RANDOM;
    }

    /**
     * Method setRandom
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param random
     */
    public void setRandom(SecureRandom random) {
        this.random = random;
    }

    /**
     * Method getFocus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Interacting getFocus() {
        return focus;
    }

    /**
     * Method getGraphic
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Graphic getGraphic() {
        return curGraphic;
    }

    /**
     * Method getAnimation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Animation getAnimation() {
        return curAnim;
    }

    /**
     * Method setPoisonTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     */
    public void setPoisonTime(int time) {
        this.poisonTime = time;
    }

    /**
     * Method getTimers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public TimingUtility getTimers() {
        return timers;
    }

    /**
     * Method getDamageForPoisonTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDamageForPoisonTime() {
        return (poisonTime / 400) + 1;
    }

    /**
     * Method updatePoisonTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int updatePoisonTime() {
        return poisonTime--;
    }

    /**
     * Method isPoisoned
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isPoisoned() {
        return poisonTime > 0;
    }

    /**
     * Method poison
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     */
    public void poison(int time) {
        if (isPoisoned() || (time == 0)) {
            return;
        }

        if (getTimers().timerActive(TimingUtility.POISON_IMMUNE_TIME)) {
            return;
        }

        if ((this instanceof NPC) && getNpc().getDefinition().isPoisonImmune()) {
            return;
        }

        this.poisonTime = time;
        this.poisonTime--;

        if (this instanceof Player) {
            ((Player) this).getActionSender().sendMessage("You have been poisoned ");
            ((Player)this).getActionSender().sendStat(3);
        }
    }

    /**
     * Method freeze
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     *
     * @return
     */
    public boolean freeze(int ticks) {
        if (timers.timerActive(TimingUtility.FREEZE_IMMUNE_TIME) || timers.timerActive(TimingUtility.FROZEN_TIME)) {
            return false;
        }

        timers.setTime(TimingUtility.FROZEN_TIME, ticks);
        timers.setTime(TimingUtility.FREEZE_IMMUNE_TIME, ticks + 5);

        if (this instanceof Player) {
            ((Player) this).getActionSender().sendMessage("You have been frozen");
            getPlayer().getRoute().resetPath();
        }

        return true;
    }

    /**
     * Method registerHit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     */
    public void registerHit(Killable k) {
        lastHitAt = Core.currentTime;
        lastHitBy = k;
    }

    /**
     * Method canBeAttacked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @return
     */
    public boolean canLogout() {
        return Core.timeSince(lastHitAt) >= Core.getTicksForSeconds(16);
    }

    /**
     * Method canBeAttacked
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param k
     *
     * @return
     */
    public boolean canBeAttacked(Killable k) {
        if ((lastHitBy != null) && (lastHitBy instanceof NPC) && (k instanceof Player) && k.isInWilderness()) {
            return true;
        }

        return (Core.timeSince(lastHitAt) >= Config.COMBAT_PJ_DELAY) || (lastHitBy == k)
               || ((k instanceof NPC) && k.getNpc().isSummoned() && (lastHitBy == ((NPC) k).getSummoner()))
               || ((lastHitBy != null) && lastHitBy.isDead() && (Core.timeSince(lastHitAt) >= 3))
               || (isInMultiArea() && k.isInMultiArea())
               || ((k instanceof Player) && (lastHitBy != null) && (lastHitBy == k.getPlayer().getCurrentFamiliar()));
    }

    /**
     * Method registerProjectile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param proj
     */
    public void registerProjectile(Projectile proj) {
        projectiles.add(proj);
        projectilesToUpdate.add(proj);
    }

    /**
     * Method getProjectiles
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Projectile> getProjectiles() {
        return projectiles;
    }

    /**
     * Method clearProjectiles
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void clearProjectiles() {
        projectiles.clear();
    }

    /**
     * Method updateProjectiles
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateProjectiles() {
        for (int i = 0; i < projectilesToUpdate.size(); i++) {
            Projectile p = projectilesToUpdate.get(i);

            if (p.isReady()) {
                if (p.getOnImpactEvent() != null) {
                    p.getOnImpactEvent().onImpact(p.getSender(), p.getTarget());
                }

                projectilesToUpdate.remove(i);
            }
        }
    }

    /**
     * Method addDamageFilter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param f
     */
    public void addDamageFilter(String name, DamageFilter f) {
        damageFilterMap.put(name, f);
    }

    /**
     * Method respawn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void respawn() {
        this.isDead          = false;
        this.currentHealth   = getMaxHealth();
        frost_whip_activated = false;
        damage_list.clear();
    }

    /**
     * Method hasDamageFilter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public boolean hasDamageFilter(String name) {
        return damageFilterMap.containsKey(name);
    }

    /**
     * Method removeDamageFilter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void removeDamageFilter(String name) {
        damageFilterMap.remove(name);
    }

    /**
     * Method isDead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDead() {
        return isDead;
    }

    /**
     * Method resetDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetDamage() {
        damage1 = null;
        damage2 = null;
    }

    /**
     * Method getDamage1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Damage getDamage1() {
        return damage1;
    }

    /**
     * Method getDamage2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Damage getDamage2() {
        return damage2;
    }

    /**
     * Method hit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     * @param damage
     * @param hitType
     * @param delay
     *
     * @return
     */
    public double hit(Killable hitBy, double damage, int hitType, int delay) {
        return hit(hitBy, damage, hitType, delay, false);
    }

    /**
     * Method preCalcDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     * @param damage
     * @param hitType
     *
     * @return
     */
    public double preCalcDamage(Killable hitBy, double damage, int hitType) {
        for (DamageFilter filter : damageFilterMap.values()) {
            damage = filter.filterDamage(damage, this, hitBy, hitType);

            if (damage == 0) {
                break;
            }
        }

        if (getCurrentHealth() - damage < 0) {
            damage = getCurrentHealth();
        }

        return damage;
    }

    /**
     * Method hit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     * @param damage
     * @param hitType
     * @param delay
     * @param byPass
     *
     * @return
     */

    public double hit(Killable hitBy, double damage, int hitType, int delay, boolean byPass){
        return hit(hitBy, damage, hitType, delay, byPass, -1);
    }
    public double hit(Killable hitBy, double damage, int hitType, int delay, boolean byPass, int crit) {
        if (isDead()) {
            return 0;
        }

        if(!isVisible())
            return 0;

        if(this instanceof Player && (getPlayer().getRights() > Player.ADMINISTRATOR))
            return 0;

        if (timers.timerActive(TimingUtility.COMBAT_BLOCK_TIMER)) {
            return 0;
        }

        damage = (damage * 10d) / 10d;

        double soak = 0;
        int origType = hitType;

        if (!byPass) {
            for (DamageFilter filter : damageFilterMap.values()) {
                damage = filter.filterDamage(damage, this, hitBy, hitType);

                if (damage == 0) {
                    break;
                }
            }

            if(this instanceof Player){
                Player me = this.getPlayer();
                if(hitType == Damage.TYPE_RANGE)
                    soak = me.getEquipment().getTotalBonuses()[Item.BONUS_SOAK_RANGE];
                if(hitType == Damage.TYPE_MELEE_CRUSH || hitType == Damage.TYPE_MELEE_STAB || hitType == Damage.TYPE_MELEE_SLASH)
                    soak = me.getEquipment().getTotalBonuses()[Item.BONUS_SOAK_MELEE];
                else if(hitType == Damage.TYPE_MAGIC)
                    soak = me.getEquipment().getTotalBonuses()[Item.BONUS_SOAK_MAGE];
            }


        }

        if (hitType != Damage.TYPE_POISON) {
            hitType = (damage > 0)
                      ? Damage.TYPE_HIT
                      : Damage.TYPE_MISS;
        }

        double calc = getCurrentHealth() - damage;
        double orig = 0;

        if (calc <= 0) {
            orig = damage;
            calc = 0;
        }

                   //812

        Damage d = new Damage(damage, hitType, hitBy, delay);
        d.setCrit(crit);
        d.setSoakPerc(damage > 20 ? soak : 0);
        d.setOriginalDamage(orig);
        d.setOrigType(origType);
        damageQueue.add(d);
        recordDamage(hitBy, (int)d.getDamage());

        return damage;
    }

    /**
     * Method updateHitRecords
     * Created on 15/02/07
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateHitRecords() {
        for (Iterator<HitRecord> records = hit_records.values().iterator(); records.hasNext(); ) {
            HitRecord record = records.next();

            if (Core.timeSince(record.getDamageAt()) > Core.getTicksForMinutes(1)) {
                records.remove();
            }
        }
    }

    /**
     * Method updateDamageQueue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateDamageQueue() {
        try {
            if (isDead) {
                damageQueue.clear();

                return;
            }

            //
            for (Iterator<Damage> iter = damageQueue.iterator(); iter.hasNext(); ) {
                Damage d = iter.next();

                if (invalidateDamage) {
                    damageQueue.clear();
                    invalidateDamage = false;

                    return;
                }

                if (d.ready()) {
                    try {
                        if(d.getSoakPerc() >= 0){
                            double dmg = d.getDamage();
                              double perc = d.getSoakPerc() / 100d;

                              double reduce = dmg - 20;
                              double soaked = (reduce * perc);
                              reduce *= (double)(double)(1 - perc);
                              dmg = reduce + 20;

                              d.setSoaked(soaked);
                              d.setDamage(dmg);

                        }
                        double calc = getCurrentHealth() - d.getDamage();

                        if (calc <= 0.0) {
                            d.setDamage(getCurrentHealth());
                        }


                        removeHealth(d.getDamage());
                        onHitBy(d.getDamager(), d);

                        if (damage1 == null) {
                            damage1 = d;
                        } else if (damage2 == null) {
                            damage2 = d;
                        }


                        if (Math.ceil(getCurrentHealth()) <= 0.0) {

                            isDead = true;

                            if (this instanceof Player) {
                                getPlayer().log("Damage: " + d.getDamage() + " " + d.getType() + " "
                                                + d.getClientType());
                            }

                            entityKilled(d.getDamager());
                            damageQueue.clear();

                            return;
                        }
                    } catch (Exception ee) {
                        ee.printStackTrace();
                    }

                    iter.remove();
                }
            }
        } catch (Exception ee) {}
    }

    /**
     * Method getCurrentHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract double getCurrentHealth();

    /**
     * Method getMaxHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public abstract double getMaxHealth();

    /**
     * Method addHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param health
     */
    public abstract void addHealth(double health);

    /**
     * Method removeHealth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentHealth
     */
    public abstract void removeHealth(double currentHealth);

    /**
     * Method onHitBy
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hitBy
     * @param d
     */
    public abstract void onHitBy(Mob hitBy, Damage d);

    /**
     * Method entityKilled
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killedBy
     */
    public abstract void entityKilled(Mob killedBy);
}
