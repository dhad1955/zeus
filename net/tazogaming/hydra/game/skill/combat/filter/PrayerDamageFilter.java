package net.tazogaming.hydra.game.skill.combat.filter;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 29/09/13
 * Time: 05:10
 * To change this template use File | Settings | File Templates.
 */
public class PrayerDamageFilter extends DamageFilter {

    /**
     * Method filterDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damage
     * @param myMob
     * @param hitBy
     * @param hitType
     *
     * @return
     */
    @Override
    public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
        Player  pla    = (Player) myMob;
        boolean active = false;

        switch (hitType) {
        case Damage.TYPE_MELEE_CRUSH :
        case Damage.TYPE_MELEE_SLASH :
        case Damage.TYPE_MELEE_STAB :
            active = pla.getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_MELEE);

            break;

        case Damage.TYPE_RANGE :
            active = pla.getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_MISSILES);

            break;

        case Damage.TYPE_MAGIC :
        case Damage.TYPE_HIT_MAGIC:

            active = pla.getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_MAGIC);
        }
        
        //TODO: Test to verify this works
        if (hitBy instanceof NPC) {
        	NPC npc = (NPC) hitBy;
        	if (npc.isSummoned()) {
        		active = pla.getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_SUMMONING) || pla.getPrayerBook().curseActive(PrayerBook.DEFLECT_SUMMONING);
        	}
        }

        if (pla.getPrayerBook().isCurses()) {
            switch (hitType) {
            case Damage.TYPE_MELEE_CRUSH :
            case Damage.TYPE_MELEE_SLASH :
            case Damage.TYPE_MELEE_STAB :
                active = pla.getPrayerBook().curseActive(PrayerBook.DEFLECT_MELEE);

                if (active && (GameMath.rand(100) > 60)) {
                    pla.getAnimation().animate(12573, Animation.LOW_PRIORITY);
                    pla.getGraphic().set(2230, Graphic.LOW);
                    hitBy.hit(pla, damage / 10, Damage.getRecoilForType(hitType), 0, false);
                }

                break;

            case Damage.TYPE_RANGE :
            case Damage.TYPE_HIT_RANGE:

                active = pla.getPrayerBook().curseActive(PrayerBook.DEFLECT_MISSILES);

                if (active && (GameMath.rand(100) > 60)) {
                    pla.getAnimation().animate(12573, Animation.LOW_PRIORITY);
                    pla.getGraphic().set(2229, Graphic.LOW);
                    hitBy.hit(pla, damage / 10, Damage.getRecoilForType(hitType), 0, false);
                }

                break;

            case Damage.TYPE_MAGIC :
            case Damage.TYPE_HIT_MAGIC:

                active = pla.getPrayerBook().curseActive(PrayerBook.DEFLECT_MAGIC);

                if (active && (GameMath.rand(100) > 60)) {
                    pla.getAnimation().animate(12573, Animation.LOW_PRIORITY);
                    pla.getGraphic().set(2228, Graphic.LOW);
                    hitBy.hit(pla, damage / 10, Damage.getRecoilForType(hitType), 0, false);
                }

                break;
            }
        }

        if (!active) {
            return damage;
        }

        if(hitType == Damage.TYPE_HIT_MAGIC)
            return damage > 18 ? 18 : damage /2;

        return (hitBy instanceof Player)
               ? damage / 2
               : 0;
    }
}
