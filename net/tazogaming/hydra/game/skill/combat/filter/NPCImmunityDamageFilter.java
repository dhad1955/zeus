package net.tazogaming.hydra.game.skill.combat.filter;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 17/10/13
 * Time: 11:32
 */
public class NPCImmunityDamageFilter extends DamageFilter {
    public static final int
        TYPE_RANGE = 1,
        TYPE_MAGE  = 2,
        TYPE_MELEE = 4;

    /**
     * Method filterDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damage
     * @param myMob
     * @param hitBy
     * @param hitType
     *
     * @return
     */
    @Override
    public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
        NPC e    = (NPC) myMob;
        int flag = e.getDamageImmunity();

        switch (hitType) {
        case Damage.TYPE_MELEE_CRUSH :
        case Damage.TYPE_MELEE_SLASH :
        case Damage.TYPE_MELEE_STAB :
            if ((flag & TYPE_MELEE) != 0) {
                return 0;
            }

            break;

        case Damage.TYPE_RANGE :
            if ((flag & TYPE_RANGE) != 0) {
                return 0;
            }

            break;

        case Damage.TYPE_MAGIC :
            if ((flag & TYPE_MAGE) != 0) {
                return 0;
            }
        }

        return damage;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
