package net.tazogaming.hydra.game.skill.combat.filter;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.game.pets.BossPet;
import net.tazogaming.hydra.game.pets.BossPets;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.Random;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 29/09/13
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public class PlayerDefenceDamageFilter extends DamageFilter {

    /**
     * SLAYER_HELMS made: 14/11/04
     **/
    static final private int[] SLAYER_HELMS = new int[]{
            13263, 14637, 14636, 15492, 15496, 15497
    };

    /**
     * random made: 14/11/04
     **/
    private Random random = new Random();

    /**
     * Method filterDamage
     * Created on 14/08/18
     *
     * @param damage
     * @param myMob
     * @param hitBy
     * @param hitType
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {


        if (myMob instanceof NPC && BossPets.getByPet(myMob.getNpc().getId()) != null)
            return 0;

        if(myMob.isInZone("duel"))
            damage = (int)damage;



        if (myMob instanceof Player && myMob.getPlayer().getRights() >= Player.ADMINISTRATOR)
            return 0;

        if ((hitType == Damage.TYPE_OTHER) || (hitType == Damage.TYPE_POISON)) {
            return damage;
        }


        if (hitBy instanceof NPC && hitBy.getNpc().isSummoned() && myMob instanceof Player) {
            if (myMob.getPlayer().getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_SUMMONING) ||
                    myMob.getPlayer().getPrayerBook().curseActive(PrayerBook.DEFLECT_SUMMONING)) {
                damage /= 2;
            }
        }


        if (hitBy instanceof NPC && myMob instanceof Player) {
            BossPet p = BossPets.getByBoss(hitBy.getNpc().getId());
            if (p != null) {
                if (myMob.getPlayer().getCurrentFamiliar() != null) {
                    if (((Player) myMob).getCurrentFamiliar().getId() == p.getPetId()) {
                        if (GameMath.rand(100) < 20) {
                            ((Player) myMob).getCurrentFamiliar().getGraphic().set(2015, 0);
                            ((Player) myMob).getActionSender().sendMessage("Your pet protects you from the bosses attack.");
                            return 0;
                        }

                    }
                }
            }
        }

        if (Combat.miss(hitBy, myMob, hitType)) {
            return 0;
        }

        if (myMob instanceof Player) {
            Player plr = (Player) myMob;

            if (plr.getRights() >= Player.ADMINISTRATOR) {
                return 0;
            }

            switch (plr.getEquipment().getId(Equipment.SHIELD)) {
                case 13740:
                case 13742:
                    int pts = (int) (damage * 0.06);

                    if ((plr.getPrayerBook().getPoints() - pts) > 0) {
                        plr.getPrayerBook().removePoints(pts);
                        plr.getActionSender().sendStat(5);
                        if(plr.getEquipment().getId(Equipment.SHIELD) == 13742)
                            damage *= 0.75;
                        else
                           damage *= 0.70;
                    }
            }
        }
        if ((hitBy instanceof Player) && (myMob instanceof NPC) && (myMob.getNpc().getSlayerInfo() != null)) {
            if (!((NPC) myMob).getSlayerInfo().isSilent() && (hitBy.getPlayer().getSlayerTask() != null && hitBy.getPlayer().getSlayerTask().getTaskId() == myMob.getNpc().getId())) {
                for (Integer helmetId : SLAYER_HELMS) {
                    if (hitBy.getPlayer().getEquipment().getId(Equipment.HAT) == helmetId) {
                        damage *= 1.40d;
                    }
                }
            }
        }

        if ((hitBy instanceof Player && myMob instanceof NPC)) {
            if (myMob.getNpc().getId() >= 13469 && myMob.getNpc().getId() <= 13474) {
                if (hitBy.getPlayer().getEquipment().contains(10588))
                    damage *= 1.30d;
            }
        }
        return damage;
    }
}
