package net.tazogaming.hydra.game.skill.combat.filter;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.ai.script.NPCScriptEngine;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.Block;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 06:10
 */
public class NPCScriptDamageFilter extends DamageFilter {

    /**
     * Method filterDamage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param damage
     * @param myMob
     * @param hitBy
     * @param hitType
     *
     * @return
     */
    @Override
    public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
        NPC npc = myMob.getNpc();

        if (npc.getControllerScript().getController() != null) {
            if (npc.getControllerScript().getController().getDamageFilter() != null) {
                Block            damageF = npc.getControllerScript().getController().getDamageFilter();
                NpcScriptScope scr     = new NpcScriptScope(npc, damageF, hitBy.getPlayer());

                scr.setCurrentDamage(new Damage(damage, hitType, null, 0));
                NPCScriptEngine.evaluate(myMob.getNpc(), scr);

                return scr.getReturnDamage();
            }
        }

        return damage;
    }
}
