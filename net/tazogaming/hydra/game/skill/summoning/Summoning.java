package net.tazogaming.hydra.game.skill.summoning;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.game.ui.rsi.interfaces.impl.skill.summoning.SummoningTab;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.cfg.Definition;
import net.tazogaming.hydra.util.cfg.DefinitionConfig;
import net.tazogaming.hydra.util.cfg.DefinitionGroup;
import net.tazogaming.hydra.util.cfg.DuplicateEntryException;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/08/14
 * Time: 19:23
 */
public class Summoning {
    private static Map<Integer, Summonable> mainConfig              = new HashMap<Integer, Summonable>();
    private static Map<Integer, Summonable> pouchMappings           = new HashMap<Integer, Summonable>();
    public static final int                 SHARD                   = 12183;
    public static final int                 POUCH                   = 12155;
    public static NpcDeathHandler           SUMMONING_DEATH_HANDLER = new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
        @Override
        protected void onDeath(NPC killed, Killable killedBy) {
            NPC npc = killed.getNpc();

            npc.getSummoner().resetSummon();

            // To change body of implemented methods use File | Settings | File Templates.
        }
        ;
    };


    public static boolean PVPCheck(NPC npc, Player player){
        if(player.getCurrentFamiliar() == npc)
            return false;
        if(npc.isInWilderness() && npc.isInMultiArea() && Combat.pvp_check(npc.getSummoner(), player))
            return true;


        return false;
    }

    /**
     * Method objectClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param slot
     * @param player
     *
     * @return
     */
    public static boolean objectClick(int id, int slot, Player player) {
        if (id == 28716) {
            if (slot == 2) {
                player.setCurrSummonPts(player.getMaxStat(23));
                player.resetSumSpec();
                player.getActionSender().sendStat(3);
                player.getActionSender().sendMessage("You renew your summoning points");
                player.getAnimation().animate(1651, 0);

                return true;
            } else if (slot == 1) {
                player.getGameFrame().openWindow(79);

                return true;
            }
        }

        return false;
    }


    public static void summonSpecialClick(final NPC familiar, Player player){
        if(familiar.getId() == 6869){
           if(!player.getInventory().hasItem(12437, 1)){
               player.getActionSender().sendMessage("You don't have a "+Item.forId(12437).getName());
               return;
           }
           if(player.getCurStat(6) < player.getMaxStat(6) + 7){
               player.setCurStat(6, player.getCurStat(6) + 7);
               player.getActionSender().sendStat(6);
               player.getInventory().deleteItem(12437, 1);
               familiar.getAnimation().animate(8267);
               player.getGraphic().set(1306, 0);
               player.getAnimation().animate(7660);
               familiar.getGraphic().set(1464, -1);

           }
        }else{
            World.getWorld().getScriptManager().forceTrigger(player, Trigger.SUMMONING_SPEC, new ScriptVariableInjector() {
                @Override
                public void injectVariables(Scope scope) {
                    scope.declare(Text.longForName("npc"), familiar);
                }
            },  familiar.getId());

            //familiar.
        }
    }

    /**
     * Method is_in_no_familiar_area
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static boolean is_in_no_familiar_area(Player pla) {
        for (Zone e : pla.getAreas()) {
            if (e.isFamiliarsBanned()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getScrollID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static final int getScrollID(Player player) {
        return pouchMappings.get(player.getAccount().getInt32("summon_id")).getScrollID();
    }
    public static final int getPouchID(Player player) {
        return pouchMappings.get(player.getAccount().getInt32("summon_id")).getScrollID();
    }

    public static Summonable getConfig(int pouchID){
        return pouchMappings.get(pouchID);
    }
    /**
     * Method pouchClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param slot
     *
     * @return
     */
    public static boolean pouchClick(Player player, int slot) {
        Item pouch = player.getInventory().getItem(slot);


        if (!pouchMappings.containsKey(pouch.getIndex())) {
            return false;
        }

        Summonable summonable = pouchMappings.get(pouch.getIndex());

        if (summonable.getLevelRequired() > player.getCurStat(23)) {
            Dialog.printStopMessage(player, "You need a summoning level of atleast " + summonable.getLevelRequired());

            return true;
        }

        NPC npc = player.getCurrentFamiliar();

        if (npc != null) {
            if (npc.getId() == summonable.getNonWildNPC()) {
                player.getTimers().setTime(TimingUtility.SUMMON_TIMER, summonable.getSpawnTicks());
                player.getActionSender().sendMessage("You renew your summoning tab");
                player.getInventory().deleteItemFromSlot(null, 1, slot);

                return true;
            }
        }

        if (is_in_no_familiar_area(player)) {
            Dialog.printStopMessage(player, "You can't summon that here");

            return true;
        }

        if (player.getCurrSummonPts() < (summonable.getLevelRequired() / 3)) {
            player.getActionSender().sendMessage("You need atleast " + summonable.getLevelRequired()
                    + " summoning points");

            return true;
        }

        player.setCurrSummonPts(player.getCurrSummonPts() - (summonable.getLevelRequired() / 3));
        player.getActionSender().sendStat(23);
        player.getInventory().deleteItemFromSlot(null, 1, slot);
        player.addXp(23, summonable.getLevelRequired() / 2);

        NPC the_npc = new NPC(summonable.getNonWildNPC(), player);
        the_npc.setCurrentInstance(player.getCurrentInstance());
        player.setCurrentFamiliar(the_npc);
        player.getAccount().setSetting("bob_max", summonable.getBobMax());
        player.getTimers().setTime(TimingUtility.SUMMON_TIMER, (int) summonable.getSpawnTicks());
        SummoningTab.setFollower(player, summonable.getPouchID());
        player.getAccount().setSetting("summon_id", summonable.getPouchID(), true);
        World.getWorld().registerNPC(the_npc);
        the_npc.setDeathHandler(SUMMONING_DEATH_HANDLER);

        return true;
    }

    /**
     * Method openBOB
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void openBOB(Player player) {
        if (player.getCurrentFamiliar() == null) {
            Dialog.printStopMessage(player, "You don't have a familiar active");

            return;
        }

        if (!player.getAccount().hasVar("bob_max") || (player.getAccount().getInt32("bob_max") <= 0)) {
            Dialog.printStopMessage(player, "This familiar is not a beast of burden");

            return;
        }

        if (!player.getCurrentFamiliar().isWithinDistance(player, 1)) {
            Dialog.printStopMessage(player, "Please get near your bob to open its inventory");

            return;
        }

        if (player.getCurrentFamiliar().getCombatHandler().isInCombat()) {
            Dialog.printStopMessage(player, "Your BOB is in combat");

            return;
        }

        player.getGameFrame().openWindow(671);
    }

    /**
     * Method getRenewTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pouchID
     *
     * @return
     */
    public static int getRenewTime(int pouchID) {
        return pouchMappings.get(pouchID).getSpawnTicks();
    }



    /**
     * Method getSpecName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pouchID
     *
     * @return
     */
    public static final String getSpecName(int pouchID) {
        return Item.forId(pouchMappings.get(pouchID).getScrollID()).getName().replace("scroll", "");
    }

    /**
     * Method transform
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param configID
     * @param amount
     */
    public static void transform(final Player player, int configID, int amount) {
        final Summonable config = mainConfig.get(configID);

        if (config == null) {
            player.getActionSender().sendMessage("This is not available at the moment: " + configID);

            return;
        }

        amount = player.getInventory().maxItem(config.getPouchID(), amount);

        if (amount < 1) {
            player.getActionSender().sendMessage("You don't have enough");

            return;
        }

        if (player.getCurStat(23) < config.getLevelRequired()) {
            player.getActionSender().sendMessage("You need a level of atleast " + config.getLevelRequired()
                    + " to transform this");

            return;
        }

        player.getInventory().deleteItem(config.getPouchID(), amount);
        player.getInventory().addItem(config.getScrollID(), (amount * 10));
        player.addXp(23, (config.getLevelRequired() * 2) * amount);

        final int setAmount = amount;

        player.getTickEvents().add(new PlayerTickEvent(player, true, 3) {
            @Override
            public void doTick(Player owner) {
                if (setAmount == 1) {
                    player.getActionSender().sendMessage("You transform the pouch into a scroll");
                } else {
                    player.getActionSender().sendMessage("You transform " + setAmount + " pouches into a scroll");
                }

                owner.setActionsDisabled(false);
                owner.getGameFrame().openWindow(79);
                terminate();
            }
        });
    }

    /**
     * Method infuse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param configID
     * @param amount
     */
    public static void infuse(Player player, int configID, int amount) {
        final Summonable config = mainConfig.get(configID);

        if (config == null) {
            player.getActionSender().sendMessage("This is not available at the moment: " + configID);

            return;
        }

        int cachedAmount = amount;

        amount = player.getInventory().maxItem(SHARD, config.getShardAmount() * amount);

        if ((amount <= 0) || ((amount < cachedAmount) && (cachedAmount != Integer.MAX_VALUE))) {
            player.getActionSender().sendMessage("You need " + (config.getShardAmount() * ((amount == -1)
                    ? 1
                    : amount)) + " shards");

            if (amount <= 0) {
                return;
            }
        }

        amount = player.getInventory().maxItem(POUCH, config.getShardAmount() * amount);

        if ((amount <= 0) || ((amount < cachedAmount) && (cachedAmount != Integer.MAX_VALUE))) {
            if (amount <= 0) {
                player.getActionSender().sendMessage("You need a pouch to infuse this");

                return;
            }

            player.getActionSender().sendMessage("You don't have enough pouches");
        }

        amount = player.getInventory().maxItem(config.getCharmID(), amount);

        if ((amount <= 0) || ((amount < cachedAmount) && (cachedAmount != Integer.MAX_VALUE))) {
            player.getActionSender().sendMessage("You don't have enough " + Item.forId(config.getCharmID()) + "'s");

            if (amount <= 0) {
                return;
            }
        }

        amount = player.getInventory().maxItem(config.getMainIngr(), amount);

        if (amount <= 0) {
            player.getActionSender().sendMessage("You need a " + Item.forId(config.getMainIngr()).getName()
                    + " to make this");

            return;
        }

        if (player.getCurStat(23) < config.getLevelRequired()) {
            player.getActionSender().sendMessage("You need a summoning level of atleast " + config.getLevelRequired()
                    + " to make this");

            return;
        }

        player.getInventory().deleteItem(SHARD, amount * config.getShardAmount());
        player.getInventory().deleteItem(config.getMainIngr(), amount);
        player.getInventory().deleteItem(config.getCharmID(), amount);
        player.getGameFrame().close();
        player.setActionsDisabled(true);
        player.getAnimation().animate(725, 0);
        player.getGraphic().set(1207, 0);
        player.getInventory().deleteItem(POUCH, amount);

        final int setAmount = amount;

        player.getTickEvents().add(new PlayerTickEvent(player, true, 3) {
            @Override
            public void doTick(Player owner) {
                owner.getInventory().addItem(config.getPouchID(), setAmount);
                owner.addXp(23, (config.getLevelRequired() * 5) * setAmount);
                owner.getGameFrame().openWindow(79);

                if (setAmount == 1) {
                    owner.getActionSender().sendMessage("You infuse the " + Item.forId(config.getMainIngr())
                            + " and make a " + Item.forId(config.getPouchID()) + "");
                } else {
                    owner.getActionSender().sendMessage("You infuse " + setAmount + " pouches");
                }

                owner.setActionsDisabled(false);
                terminate();
            }
        });
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load() {
        try {
            DefinitionConfig config = new DefinitionConfig("config/iskill/summoning/summoning_struct.cfg");
            DefinitionGroup  group  = config.getGroup("summon");

            Logger.log("Total summoning definitions: " + group.size());

            for (Definition definition : group.asList()) {
                int summonID = definition.getValue();

                if (mainConfig.containsKey(summonID)) {
                    throw new DuplicateEntryException("Duplicate entry for summoning at " + summonID);
                }

                Summonable summonable = new Summonable();

                summonable.setCharmID(definition.getInt("charm"));
                summonable.setBobMax(definition.getInt("bobcount"));
                summonable.setScrollID(definition.getInt("scroll"));
                summonable.setPouchID(definition.getInt("pouch"));
                summonable.setWildNPC(definition.getInt("wildNpc"));
                summonable.setLevelRequired(definition.getInt("level"));
                summonable.setMainIngr(definition.getInt("ingredient"));
                summonable.setShardAmount(definition.getInt("shards"));
                summonable.setExperience(definition.getInt("experience"));
                summonable.setSpawnTicks(definition.getInt("spawntime"));
                summonable.setNonWildNPC(definition.getInt("nonWildNpc"));
                if(definition.hasProperty("clickspec"))
                    summonable.setClickSpec(true);

                mainConfig.put(summonID, summonable);
                pouchMappings.put(summonable.getPouchID(), summonable);
            }
        } catch (Exception ee) {
            ee.printStackTrace();
            System.exit(-1);
        }
    }
}
