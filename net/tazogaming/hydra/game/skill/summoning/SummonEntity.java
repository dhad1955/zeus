package net.tazogaming.hydra.game.skill.summoning;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/10/13
 * Time: 09:57
 */

/**
 * Created with IntelliJ IDEA.
 * User: Danny
 * Date: 11/03/13
 * Time: 22:45
 * To change this template use File | Settings | File Templates.
 */
public class SummonEntity {
    public int    bobCount            = 0;
    public int    scrollId            = -0;
    public int    pouchItemId         = -1;
    public int    levelRequirement    = 1;
    public int    exp                 = -1;
    public int    shardAmount         = -1;
    public int    charmid             = -1;
    public int    secondaryIngredient = -1;
    public String specName            = "Special Attack";
    public int    nonWildNpc;
    public int    wildNpcId;
    public String name;
    public int    maxHit;
    public int    pouchReq;
    public long   time;
    public int    summonPts;
    public int    pouchId;

    // {pouchId, nonWildNpcId, wildNpcId, SummonName, SummonTime(60*2=1min), maxhit, executeCombatTick, defence, pouchReq, bobSlotCount, summonPoints}

    /**
     * Method hashCode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public int hashCode() {
        return pouchId;
    }
}
