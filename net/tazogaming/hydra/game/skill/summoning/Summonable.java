package net.tazogaming.hydra.game.skill.summoning;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/08/14
 * Time: 19:23
 */
public class Summonable {
    private int wildNPC;
    private int nonWildNPC;
    private int pouchID;
    private int levelRequired;
    private int experience;
    private int shardAmount;
    private int charmID;
    private int mainIngr;
    private int secondIngr;
    private int spawnTicks;
    private int scrollID;
    private int bobMax;
    public boolean isClickSpec = false;

    public boolean isClickSpec() {
        return this.isClickSpec;
    }

    public void setClickSpec(boolean b){
        this.isClickSpec = b;
    }

    /**
     * Method getPouchID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPouchID() {
        return pouchID;
    }

    /**
     * Method setPouchID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pouchID
     */
    public void setPouchID(int pouchID) {
        this.pouchID = pouchID;
    }

    /**
     * Method getWildNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWildNPC() {
        return wildNPC;
    }

    /**
     * Method setWildNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param wildNPC
     */
    public void setWildNPC(int wildNPC) {
        this.wildNPC = wildNPC;
    }

    /**
     * Method getNonWildNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNonWildNPC() {
        return nonWildNPC;
    }

    /**
     * Method setNonWildNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param nonWildNPC
     */
    public void setNonWildNPC(int nonWildNPC) {
        this.nonWildNPC = nonWildNPC;
    }

    /**
     * Method getLevelRequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevelRequired() {
        return levelRequired;
    }

    /**
     * Method setLevelRequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param levelRequired
     */
    public void setLevelRequired(int levelRequired) {
        this.levelRequired = levelRequired;
    }

    /**
     * Method getExperience
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getExperience() {
        return experience;
    }

    /**
     * Method setExperience
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param experience
     */
    public void setExperience(int experience) {
        this.experience = experience;
    }

    /**
     * Method getShardAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getShardAmount() {
        return shardAmount;
    }

    /**
     * Method setShardAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param shardAmount
     */
    public void setShardAmount(int shardAmount) {
        this.shardAmount = shardAmount;
    }

    /**
     * Method getCharmID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCharmID() {
        return charmID;
    }

    /**
     * Method setCharmID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param charmID
     */
    public void setCharmID(int charmID) {
        this.charmID = charmID;
    }

    /**
     * Method getMainIngr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMainIngr() {
        return mainIngr;
    }

    /**
     * Method setMainIngr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mainIngr
     */
    public void setMainIngr(int mainIngr) {
        this.mainIngr = mainIngr;
    }

    /**
     * Method getSecondIngr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSecondIngr() {
        return secondIngr;
    }

    /**
     * Method setSecondIngr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param secondIngr
     */
    public void setSecondIngr(int secondIngr) {
        this.secondIngr = secondIngr;
    }

    /**
     * Method getSpawnTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSpawnTicks() {
        return spawnTicks;
    }

    /**
     * Method setSpawnTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param spawnTicks
     */
    public void setSpawnTicks(int spawnTicks) {
        this.spawnTicks = spawnTicks;
    }

    /**
     * Method getScrollID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getScrollID() {
        return scrollID;
    }

    /**
     * Method setScrollID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scrollID
     */
    public void setScrollID(int scrollID) {
        this.scrollID = scrollID;
    }

    /**
     * Method getBobMax
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBobMax() {
        return bobMax;
    }

    /**
     * Method setBobMax
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bobMax
     */
    public void setBobMax(int bobMax) {
        this.bobMax = bobMax;
    }
}
