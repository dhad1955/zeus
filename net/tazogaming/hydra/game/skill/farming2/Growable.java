package net.tazogaming.hydra.game.skill.farming2;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.cfg.Definition;
import net.tazogaming.hydra.util.cfg.DefinitionConfig;
import net.tazogaming.hydra.util.cfg.DefinitionGroup;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 16:37
 */
public class Growable {
    private static final HashMap<Integer, Growable> growingList = new HashMap<Integer, Growable>();
    public static final int
        NORMAL                                                  = 0,
        WATERED                                                 = 1,
        DISEASED                                                = 2,
        DEAD                                                    = 3;
    private int     seedID                                      = -1;
    private int     levelRequired                               = -1;
    private int     xp                                          = -1;
    private int     type                                        = -1;
    private int     length                                      = -1;
    private int[][] configs                                     = new int[4][];
    private int     harvest;
    private int     harvestAmt;

    /**
     * Constructs ...
     *
     *
     * @param definition
     */
    public Growable(Definition definition) {
        this.seedID            = definition.getValue();
        this.levelRequired     = definition.getInt("level");
        this.xp                = definition.getInt("xp");
        this.type              = PatchDefinition.typeForName(definition.getString("type"));
        this.configs[NORMAL]   = definition.getIntArray("seeded_ids");
        this.configs[WATERED]  = definition.getIntArray("watered_ids");
        this.configs[DEAD]     = definition.getIntArray("dead_ids");
        this.configs[DISEASED] = definition.getIntArray("diseased_ids");
        this.length            = definition.getInt("phaseAmount");

        if (!definition.hasProperty("harvest")) {
            throw new RuntimeException("Error[Farming], harvested id not set!: " + seedID);
        }

        this.harvest    = definition.getInt("harvest");
        this.harvestAmt = definition.hasProperty("harvest_amt")
                          ? definition.getInt("harvest_amt")
                          : 0;

        System.err.println("Loaded farming shit: "+this.seedID+" "+definition.getString("type")+" "+type);
    }

    /**
     * Method getHarvest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHarvest() {
        return harvest;
    }

    /**
     * Method getHarvestAmt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHarvestAmt() {
        return harvestAmt;
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public static final Growable get(int i) {
        if (growingList.containsKey(i)) {
            return growingList.get(i);
        }

        return null;
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     *
     * @throws Exception
     */
    public static void load(String path) throws Exception {
        DefinitionConfig config = new DefinitionConfig(path);
        DefinitionGroup  group  = config.getGroup("growable");

        for (Definition d : group.asList()) {
            Growable growable = new Growable(d);

            growingList.put(growable.getSeedID(), growable);
        }

        Logger.log("Loaded " + growingList.size());
    }

    private int transformPhase(int phase, int state) {

        if ((phase == DEAD) || (phase == DISEASED)) {
            return state - 1;
        }

        return state;
    }

    /**
     * Method getVarbit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param phase
     * @param state
     *
     * @return
     */
    public int getVarbit(int phase, int state) {
        return configs[phase][transformPhase(phase, state)];
    }

    /**
     * Method getMainVarbit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getMainVarbit() {
        return this.configs[0];
    }

    /**
     * Method getSeedID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSeedID() {
        return seedID;
    }

    /**
     * Method getLevelRequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevelRequired() {
        return levelRequired;
    }

    /**
     * Method getXp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getXp() {
        return xp;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getLength
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLength() {
        return length;
    }
}
