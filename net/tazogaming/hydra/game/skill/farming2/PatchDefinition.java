package net.tazogaming.hydra.game.skill.farming2;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.cfg.Definition;
import net.tazogaming.hydra.util.cfg.DefinitionConfig;
import net.tazogaming.hydra.util.cfg.DefinitionGroup;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 15:48
 */
public class PatchDefinition {
    private static final HashMap<Integer, PatchDefinition> patch_list = new HashMap<Integer, PatchDefinition>();
    public static final int
        TREE                                                          = 0,
        ALLOTMENT                                                     = 1,
        HERBS                                                         = 2,
        FLOWERS                                                       = 3;
    private int     patchID                                           = -1;
    private boolean isDonor                                           = false;
    private int     type;
    private int     y, x;
    private int     subPatch;

    /**
     * Constructs ...
     *
     *
     * @param definition
     */
    public PatchDefinition(Definition definition) {
        this.patchID = definition.getValue();

        String type = definition.getString("type");

        this.type = typeForName(type);

        if (this.type == -1) {
            throw new RuntimeException("invalid farming type: " + type);
        }

        if (definition.hasProperty("isdonor")) {
            this.isDonor = Boolean.parseBoolean(definition.getString("isdonor"));
        }

        if ((this.type == ALLOTMENT) &&!definition.hasProperty("subpatch")) {
            throw new RuntimeException("Error no sub patch specified.");
        } else if (this.type == ALLOTMENT) {
            this.subPatch = definition.getInt("subpatch");
        } else {
            this.x = definition.getInt("x");
            this.y = definition.getInt("y");
        }

        patch_list.put(this.patchID, this);
    }

    /**
     * Method getQuest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static PatchDefinition forID(int id) {
        return patch_list.get(id);
    }

    /**
     * Method isDonor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDonor() {
        return isDonor;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getX() {
        return x;
    }

    /**
     * Method getSubPatch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSubPatch() {
        return subPatch;
    }

    /**
     * Method getY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getY() {
        return y;
    }

    /**
     * Method typeForName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     *
     * @return
     */
    public static final int typeForName(String type) {
        if (type.equalsIgnoreCase("tree")) {
            return TREE;
        } else if (type.equalsIgnoreCase("allotment") || type.equalsIgnoreCase("allotments")) {
            return ALLOTMENT;
        } else if (type.equalsIgnoreCase("herb") || type.equalsIgnoreCase("herbs")) {
            return HERBS;
        } else if (type.equalsIgnoreCase("flowers") || type.equalsIgnoreCase("flower")) {
            return FLOWERS;
        }

        return -1;
    }

    /**
     * Method loadPatchDefinitions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param dir
     * @param task
     *
     * @throws Exception
     */
    public static void loadPatchDefinitions(String dir, BootTask task) throws Exception {
        DefinitionConfig group = new DefinitionConfig(dir);

        task.log("Loading patch definitions");

        DefinitionGroup patches = group.getGroup("patch");

        for (Definition d : patches.asList()) {
            PatchDefinition def = new PatchDefinition(d);

            if (def.type != PatchDefinition.ALLOTMENT) {
                World.getWorld().getTile(def.x, def.y, 0).setHasFarmingPatch(def.patchID);
            } else {
                ArrayList<Point> subPatches = SubPatches.sub_patches[def.subPatch];

                if (subPatches == null) {
                    throw new IndexOutOfBoundsException("cant find: " + def.subPatch);
                }

                for (Point p : subPatches) {
                    World.getWorld().getTile(p).setHasFarmingPatch(def.patchID);
                }
            }

            patch_list.put(def.patchID, def);
        }

        task.log("Loaded " + patch_list.size() + " patch definitions");
    }

    public int getPatchId() {
        return this.patchID;
    }
}
