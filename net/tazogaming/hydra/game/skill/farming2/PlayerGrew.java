package net.tazogaming.hydra.game.skill.farming2;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.actions.WoodcuttingAction;
import net.tazogaming.hydra.game.skill.farming.patch.PatchEntity;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.ui.Dialog;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/07/14
 * Time: 19:22
 */

//class to hold player grew plants/patches to remove them from processing.
public class PlayerGrew {
    private boolean         isUnlinked = false;
    private byte            logsRemaining;
    private PatchDefinition patchDefinition;

    // patch ID
    private int patchID;

    // status of it, for trees
    private byte status;

    // amount of items in harvest before it dissapears
    private int harvestAmount;

    // the id of the planted product
    private int grew;

    /**
     * Constructs ...
     *
     *
     * @param patchID
     * @param grew
     * @param harvestAmount
     */
    public PlayerGrew(int patchID, int grew, int harvestAmount) {
        this.patchDefinition = PatchDefinition.forID(patchID);
        this.grew            = grew;
        this.logsRemaining   = 27;
        this.patchID         = patchID;
        this.harvestAmount   = harvestAmount;
    }

    /**
     * Constructs ...
     *
     *
     * @param patchID
     * @param grew
     * @param harvestAmount
     * @param logsRemaining
     */
    public PlayerGrew(int patchID, int grew, int harvestAmount, int logsRemaining) {
        this(patchID, grew, harvestAmount);
        this.logsRemaining = (byte) logsRemaining;
    }

    /**
     * Method getHarvestAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHarvestAmount() {
        return harvestAmount;
    }

    /**
     * Method logsRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int logsRemaining() {
        return this.logsRemaining;
    }

    /**
     * Method chopLog
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean chopLog(Player player) {
        this.logsRemaining--;

        if (this.logsRemaining <= 0) {
            this.logsRemaining = 0;
            this.status        = 2;
            player.getActionSender().sendVarBit(patchID, getVarbit());
            player.getTickEvents().add(new PlayerTickEvent(player, true, 100) {
                @Override
                public void doTick(Player owner) {
                    if (isUnlinked) {
                        terminate();

                        return;
                    }

                    status = 1;
                    respawn(owner);
                    terminate();
                }
            });

            return true;
        }

        return false;
    }

    /**
     * Method getGrew
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Growable getGrew() {
        return Growable.get(this.grew);
    }

    /**
     * Method getGrewIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGrewIndex() {
        return this.grew;
    }

    /**
     * Method getPatch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PatchDefinition getPatch() {
        return patchDefinition;
    }

    /**
     * Method respawn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void respawn(Player player) {
        this.status        = 1;
        this.logsRemaining = 27;
        player.getActionSender().sendVarBit(patchID, getVarbit());
    }

    /**
     * Method itemAtObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objID
     * @param player
     *
     * @return
     */
    public boolean itemAtObject(int objID, Player player) {
        if (objID == 952) {
            unlink(player);
            player.getActionSender().sendMessage("Patch cleared");

            return true;
        }

        return false;
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void unlink(Player player) {
        isUnlinked = true;
        player.getAccount().setGrew(patchID, null);
        Farming.resetPatch(player, patchID);
    }

    /**
     * Method interact
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param slot
     */
    public void interact(Player player, int slot) {
        Growable grew = getGrew();

        if(grew == null)
            throw new RuntimeException("error: "+this.grew);
        if (slot == 1) {
            if (grew.getType() == PatchDefinition.TREE) {
                if (status == 0) {
                    Dialog.printStopMessage(player, "The tree is in good health");
                    player.addXp(19, grew.getXp());
                    status = 1;
                    player.getActionSender().sendVarBit(this.patchID, getVarbit());

                    return;
                } else if (status == 1) {
                    player.terminateActions();
                    player.terminateScripts();
                    player.setCurrentAction(new WoodcuttingAction(player, this));
                } else if (status == 2) {
                    unlink(player);
                }
            } else {
                int slots = player.getInventory().getFreeSlots(grew.getHarvest());

                if (slots == 0) {
                    Dialog.printStopMessage(player, "You don't have any space to harvest this");

                    return;
                }

                if (slots > harvestAmount) {
                    slots = harvestAmount;
                }

                if (slots < harvestAmount) {
                    Dialog.printStopMessage(player, "You pick " + slots + " crops from the harvest",
                                            "You still have " + (harvestAmount - slots) + " left to pick");
                }

                harvestAmount -= slots;
                player.getInventory().addItem(grew.getHarvest(), slots);
                player.getActionSender().sendMessage("You pick the crops");
                player.getAnimation().animate(2286, 0);

                player.addXp(19, grew.getXp() * slots);

                if (harvestAmount == 0) {
                    unlink(player);
                }
            }
        } else if (slot == 2) {
            if (grew.getType() == PatchDefinition.TREE) {
                Dialog.printStopMessage(player, "There are " + logsRemaining + " logs to chop off this tree.");

                return;
            } else {
                Dialog.printStopMessage(player, "There are " + harvestAmount + " crops to pick from this");
            }
        }
    }

    /**
     * Method getStateType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     * Method getVarbit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getVarbit() {
        Growable g          = Growable.get(this.grew);
        int      grewVarbit = g.getMainVarbit()[g.getMainVarbit().length - 1];

        if (g.getType() == PatchDefinition.TREE) {
            return grewVarbit + status;
        } else {
            return grewVarbit;
        }
    }
}
