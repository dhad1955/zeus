package net.tazogaming.hydra.game.skill.farming2;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 16:56
 */
public class Farming implements RecurringTickEvent {

    /** SEED_DIBBER, RAKE, SECATEURS, SPADE, PLANT_CURE, COMPOST, SUPER_COMPOST made: 14/08/30 */
    private static final int
        SEED_DIBBER   = 5343,
        RAKE          = 5341,
        SECATEURS     = 5329,
        SPADE         = 952,
        PLANT_CURE    = 6036,
        COMPOST       = 6032,
        SUPER_COMPOST = 6034;

    /** puresLeaderboard made: 14/08/30 */
    private static final Farming instance = new Farming();

    static {
        Core.submitTask(instance);
    }

    /** growingList made: 14/08/30 */
    private HashMap<Integer, ArrayList<Growing>> growingList = new HashMap<Integer, ArrayList<Growing>>();

    /**
     * Method isWateringCan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isWateringCan(int id) {
        return (id >= 5332) && (id <= 5340);
    }

    /**
     * Method decantWateringCan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static int decantWateringCan(int id) {
        return id - 1;
    }

    /**
     * Method objectClicked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param patchX
     * @param patchY
     * @param slot
     * @param player
     *
     * @return
     */
    public static boolean objectClicked(int patchX, int patchY, int slot, Player player) {
        int patchID = World.getWorld().getTile(patchX, patchY, 0).getPatchID();

        if (patchID == -1) {
            return false;
        }

        PlayerGrew grew = player.getAccount().getGrewPatch(patchID);

        if (grew != null) {
            grew.interact(player, slot);

            return true;
        }

        Growing g = instance.getForPlayer(player, patchID);

        switch (slot) {
        case 1 :
            if (g == null) {
                if (!player.getInventory().hasItem(RAKE, 1)) {
                    Dialog.printStopMessage(player, "You don't have a rake");
                } else {
                    g = instance.createWeeds(player, patchID);
                    instance.rakePatch(g, player);

                    return true;
                }
            }

            if ((g != null) && (g.getStateType() == Growing.DEAD)) {
                if (!player.getInventory().hasItem(952, 1)) {
                    Dialog.printStopMessage(player, "You need a spade");

                    return true;
                }

                g.reset();
                Dialog.printStopMessage(player, "You dig up the patch");

                return true;
            }

            if ((g != null) && (g.getStateType() == Growing.WEEDS)) {
                if (g.getPhase() == 3) {
                    Dialog.printStopMessage(player, "This patch is ready for planting");

                    return true;
                }

                if (!player.getInventory().hasItem(RAKE, 1)) {
                    player.getActionSender().sendMessage("You don't have a rake");
                } else {
                    instance.rakePatch(g, player);

                    return true;
                }

                return false;
            }

            break;

        case 2 :
            if (g != null) {
                if (g.getStateType() == Growing.WEEDS) {
                    if (g.getPhase() == 3) {
                        String[] data = { "The inspection reveals that this patch is ready for planting", "" };

                        if (g.getCompostType() == 0) {
                            data[1] = "You could add compost or supercompost to reduce risk of disease.";
                        } else if (g.getCompostType() == 1) {
                            data[1] = "This patch has compost on it, supercompost to reduce risk of disease.";
                        } else {
                            data[1] = "This patch has supercompost on it";
                        }

                        Dialog.printStopMessage(player, data);

                        return true;
                    }
                } else if (g.getStateType() == Growing.DISEASED) {
                    Dialog.printStopMessage(player, "Your inspection reveals that this patch is diseased",
                                            "You need to find a way to cure it before it dies.");

                    return true;
                } else if (g.getStateType() == Growing.DEAD) {
                    Dialog.printStopMessage(player, "Your inspection reveals that this patch is dead",
                                            "Best thing to do now is dig it up and start again");

                    return true;
                } else {
                    Dialog.printStopMessage(player,
                                            "Your inspection reveals that this will be ready in " + g.timeRemaining());
                }

                return true;
            }

            break;
        }

        return false;
    }

    /**
     * Method itemAtObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemID
     * @param patchX
     * @param patchY
     * @param player
     *
     * @return
     */
    public static boolean itemAtObject(int itemID, int patchX, int patchY, final Player player) {
        int patchID = World.getWorld().getTile(patchX, patchY, 0).getPatchID();

        if (patchID == -1) {
            return false;
        }

        PlayerGrew grew = player.getAccount().getGrewPatch(patchID);

        if (grew != null) {
            grew.itemAtObject(itemID, player);

            return true;
        }

        final Growing g = instance.getForPlayer(player, patchID);

        if (g != null) {
            switch (g.getState()) {
            case Growing.GROWING :
                Growable current = g.getCurrentlyGrowing();

                if (isWateringCan(itemID)) {
                    if ((current.getType() != PatchDefinition.ALLOTMENT)
                            && (current.getType() != PatchDefinition.FLOWERS)) {
                        Dialog.printStopMessage(player, "You can't water this type of crop");

                        return true;
                    }

                    if (g.isWatered()) {
                        Dialog.printStopMessage(player, "You've already watered this.",
                                                "Any more water will choke the plant to death.");

                        return true;
                    }

                    player.getInventory().deleteItem(itemID, 1);
                    player.getInventory().addItem(decantWateringCan(itemID), 1);
                    player.getAnimation().animate(2293);
                    player.getActionSender().sendMessage("You water the crops");
                    g.water();

                    return true;
                }

                switch (itemID) {
                case SPADE :
                    player.getActionSender().sendMessage("You dig up the patch");
                    g.reset();

                    return true;

                case SUPER_COMPOST :
                case COMPOST :
                    if (g.getCompostType() == 2) {
                        Dialog.printStopMessage(player, (itemID == COMPOST)
                                                        ? "This already has super compost on it"
                                                        : "You've already got supercompost on this");

                        return true;
                    }

                    int compostType = (itemID == SUPER_COMPOST)
                                      ? 2
                                      : 1;

                    g.setCompostType(compostType);
                    player.getAnimation().animate(2283, 1);
                    player.getInventory().deleteItem(itemID, 1);
                    player.getActionSender().sendMessage("You add compost to the crops");

                    return true;

                case SECATEURS :
                    if (g.getCurrentlyGrowing().getType() != PatchDefinition.TREE) {
                        Dialog.printStopMessage(player, "You can't use these on this type of crop");

                        return true;
                    }

                    Dialog.printStopMessage(player, "This tree is not diseased.");

                    return true;

                case PLANT_CURE :
                    Dialog.printStopMessage(player, "This is not diseased");

                    return true;

                case RAKE :
                    Dialog.printStopMessage(player, "This patch is already raked.");

                    return true;

                default :
                    return false;
                }
            case Growing.DISEASED :
                switch (itemID) {
                case SECATEURS :
                    if (g.getCurrentlyGrowing().getType() == PatchDefinition.TREE) {
                        player.getAnimation().animate(2276, 0);
                        player.setActionsDisabled(true);
                        player.getTickEvents().add(new PlayerTickEvent(player, false, 2) {
                            @Override
                            public void doTick(Player owner) {
                                if (g.getState() == Growing.DEAD) {
                                    terminate();

                                    return;
                                }

                                g.changeState(Growing.GROWING);
                                player.setActionsDisabled(false);
                                player.getActionSender().sendMessage("You trim the mould off the tree.");
                                player.getAnimation().animate(65535, 0);
                                terminate();
                            }
                        });

                        return true;
                    } else {
                        Dialog.printStopMessage(player, "You can only use these on tree's or bushes.");

                        return true;
                    }
                case PLANT_CURE :
                    if (g.getCurrentlyGrowing().getType() == PatchDefinition.TREE) {
                        Dialog.printStopMessage(player, "You can't use plant cure on trees");

                        return true;
                    } else {
                        g.cure();    // (Growing.GROWING);
                        player.getAnimation().animate(2288, 0);
                        player.getActionSender().sendMessage("You cure the crops");
                        player.getInventory().deleteItem(itemID, 1);

                        return true;
                    }
                }

                break;

            case Growing.DEAD :
                if (itemID == SPADE) {
                    g.reset();
                    Dialog.printStopMessage(player, "You dig up the patch");

                    return true;
                }

                Dialog.printStopMessage(player, "The only thing you can do now is dig it up",
                                        "using a spade would seem suitable.");

                return true;

            case Growing.WEEDS :
                if (itemID == RAKE) {
                    if (g.getPhase() < 3) {
                        instance.rakePatch(g, player);

                        return true;
                    } else {
                        Dialog.printStopMessage(player, "This patch is fully raked",
                                                "The next step is to plant a seed on it");

                        return true;
                    }
                }

                if (g.getPhase() == 3) {
                    Growable growable = Growable.get(itemID);

                    if (growable == null) {
                        return false;
                    }

                    if (growable.getLevelRequired() > player.getCurStat(19)) {
                        Dialog.printStopMessage(player,
                                                "You need a farming level of atleast " + growable.getLevelRequired()
                                                + "");

                        return false;
                    }

                    if (growable.getType() != PatchDefinition.TREE) {
                        if (!player.getInventory().hasItem(SEED_DIBBER, 1)) {
                            Dialog.printStopMessage(player, "You need a seed dibber for this");

                            return true;
                        }
                    }

                    if (growable.getType() != PatchDefinition.forID(patchID).getType()) {
                        Dialog.printStopMessage(player, "You cant plant that here.");

                        return true;
                    }

                    player.getInventory().deleteItem(itemID, 1);
                    g.bind(patchID, itemID);
                    player.getAnimation().animate(2286, 0);
                    player.getActionSender().sendMessage("You plant the crops");
                }
            }

            return true;
        } else {
            final Growing growing = instance.createWeeds(player, patchID);

            instance.rakePatch(growing, player);
        }

        return false;
    }

    /**
     * Method resetPatch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param patchID
     */
    public static void resetPatch(Player player, int patchID) {
        if (instance.getForPlayer(player, patchID) != null) {
            throw new RuntimeException("patch exists?");
        }

        Growing g = instance.createWeeds(player, patchID);

        for (int i = 0; i < 3; i++) {
            g.next();
        }

        g.updatePlayer();
    }

    private void rakePatch(final Growing growing, final Player player) {
        player.getAnimation().animate(2273);
        player.getTickEvents().add(new PlayerTickEvent(player, 3, false, false) {
            @Override
            public void doTick(Player owner) {
                player.getAnimation().animate(2273);

                if (growing.getPhase() < 3) {
                    growing.next();
                } else {
                    player.getAnimation().animate(65535);
                    player.getActionSender().sendMessage("You fully rake the patch");
                    terminate();
                }
            }
        });
    }

    private Growing createWeeds(Player player, int patchID) {
        Growing growing = new Growing(player, patchID);

        if (!instance.growingList.containsKey(player.getId())) {
            growingList.put(player.getId(), new ArrayList<Growing>());
        }

        growingList.get(player.getId()).add(growing);

        return growing;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        ArrayList<Growing> growing;

        for (Iterator<ArrayList<Growing>> activePatches = growingList.values().iterator(); activePatches.hasNext(); ) {
            growing = activePatches.next();

            for (Iterator<Growing> playerPatches = growing.iterator(); playerPatches.hasNext(); ) {
                Growing growingSub = playerPatches.next();

                growingSub.update();

                if (growingSub.unlinked()) {
                    playerPatches.remove();
                }
            }

            if (growing.size() == 0) {
                activePatches.remove();
            }
        }
    }

    /**
     * Method getForPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param patch
     *
     * @return
     */
    public Growing getForPlayer(Player player, int patch) {
        if (!growingList.containsKey(player.getId())) {
            return null;
        }

        for (Growing growing : growingList.get(player.getId())) {
            if (growing.getPatchID() == patch) {
                return growing;
            }
        }

        return null;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void next() {
        for (ArrayList<Growing> g : instance.growingList.values()) {
            for (Growing grow : g) {
                grow.next();
            }
        }
    }

    /**
     * Method bind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void bind(Player player) {
        Farming manager = instance;

        if (player.getAccount().getFarmPatches().size() > 0) {
            for (PlayerGrew grew : player.getAccount().getFarmPatches().values()) {
                player.getActionSender().sendVarBit(grew.getPatch().getPatchId(), grew.getVarbit());
            }
        }

        if (!manager.growingList.containsKey(player.getId())) {
            return;
        }

        for (Iterator<Growing> patchIterator = manager.growingList.get(player.getId()).iterator();
                patchIterator.hasNext(); ) {
            Growing playerPatch = patchIterator.next();

            playerPatch.bindPlayer(player);
            playerPatch.updatePlayer();

            if (playerPatch.getState() == Growing.FINISHED) {
                player.getAccount().setGrew(playerPatch.getPatchID(),
                                            new PlayerGrew(playerPatch.getPatchID(), playerPatch.getGrowingID(),
                                                playerPatch.getHarvestAmount()));
                patchIterator.remove();

                if (manager.growingList.get(player.getId()).size() == 0) {
                    manager.growingList.remove(player.getId());
                }
            }
        }
    }

    /**
     * Method addGrowing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param growing
     */
    public void addGrowing(Player player, Growing growing) {
        ArrayList<Growing> playerGrowing = null;

        if (!growingList.containsKey(player.getId())) {
            playerGrowing = new ArrayList<Growing>();
            growingList.put(player.getId(), playerGrowing);
        } else {
            playerGrowing = growingList.get(player.getId());
        }

        growing.bindPlayer(player);
        playerGrowing.add(growing);
        growing.updatePlayer();
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;
    }
}
