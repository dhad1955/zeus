package net.tazogaming.hydra.game.skill.farming2;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 17:01
 */
public class Growing {
    public static final int  GROW_DELAY_TREES = Core.getTicksForMinutes(6);
    public static final int  GROW_DELAY_OTHER = Core.getTicksForMinutes(2);
    public static final byte
        GROWING                               = 0,
        DISEASED                              = 1,
        DEAD                                  = 2,
        WEEDS                                 = 5,
        FINISHED                              = 3;

    /** phase made: 14/09/04 **/
    private int phase = 0;

    /** removed made: 14/09/04 **/
    private boolean removed = false;

    /** compostType made: 14/09/04 **/
    private byte compostType = 0;

    /** lastInteraction made: 14/09/04 **/
    private long lastInteraction;

    /** patchID made: 14/09/04 **/
    private int patchID;

    /** growingID made: 14/09/04 **/
    private int growingID;

    /** isWatered made: 14/09/04 **/
    private boolean isWatered;

    /** cachedGrowing made: 14/09/04 **/
    private Growable cachedGrowing;

    /** stateType made: 14/09/04 **/
    private byte stateType;

    /** lastChange made: 14/09/04 **/
    private int lastChange;

    /** player made: 14/09/04 **/
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param patch
     */
    public Growing(Player player, int patch) {
        this.player     = player;
        this.stateType = WEEDS;
        this.patchID    = patch;
        this.lastChange = Core.currentTime;
        updatePlayer();
    }

    /**
     * Method getCompostType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCompostType() {
        return this.compostType;
    }

    /**
     * Method setCompostType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param compost
     */
    public void setCompostType(int compost) {
        this.compostType = (byte) compost;
    }

    /**
     * Method reset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reset() {
        this.phase  = 3;
        this.stateType = WEEDS;
        updatePlayer();
    }

    /**
     * Method getCurrentlyGrowing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Growable getCurrentlyGrowing() {
        return this.cachedGrowing;
    }

    /**
     * Method water
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void water() {
        this.isWatered = true;
        updatePlayer();
    }

    /**
     * Method randomDiseaseChance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean randomDiseaseChance() {


        int min = 700;

        if (isWatered) {
            min -= 200;
        }

        if (compostType == 1) {
            min -= 200;
        }

        if (compostType == 2) {
            min -= 500;
        }

        if (min < 0) {
            return false;
        }

        return GameMath.rand(860) > min;
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unlink() {
        this.removed = true;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method getLastChange
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLastChange() {
        return lastChange;
    }

    /**
     * Method isWatered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isWatered() {
        return isWatered;
    }

    /**
     * Method getGrowingID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGrowingID() {
        return growingID;
    }

    /**
     * Method getPatchID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPatchID() {
        return patchID;
    }

    /**
     * Method getUserID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */

    /**
     * Method getStateType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte getStateType() {
        return stateType;
    }

    /**
     * Method getLastInteraction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getLastInteraction() {
        return lastInteraction;
    }

    /**
     * Method bind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param patch
     * @param growing
     */
    public void bind(int patch, int growing) {
        this.phase         = 0;
        this.stateType = GROWING;
        this.patchID       = patch;
        this.growingID     = growing;
        lastChange         = Core.currentTime;
        this.cachedGrowing = Growable.get(growing);
        updateInteraction();
        updatePlayer();
    }

    /**
     * Method bindPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void bindPlayer(Player player) {
        this.player = player;
    }

    /**
     * Method updateInteraction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateInteraction() {
        this.lastInteraction = System.currentTimeMillis();
    }

    /**
     * Method changeState
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param state
     */
    public void changeState(byte state) {
        if ((this.stateType == DISEASED) && (state == GROWING)) {
            phase++;
        }

        this.stateType = state;
        this.lastChange = Core.currentTime;
        this.isWatered  = false;


        updateInteraction();
        updatePlayer();
    }


    public void cure() {
        changeState(GROWING);
        if(phase == cachedGrowing.getLength() - 1){
            growFinished();
            changeState(FINISHED);
        }
    }

    private int getPhaseType() {
        switch (stateType) {
        case DISEASED :
            return Growable.DISEASED;

        case DEAD :
            return Growable.DEAD;

        case GROWING :
            return isWatered
                   ? Growable.WATERED
                   : Growable.NORMAL;

        case FINISHED :
            return Growable.NORMAL;

        default :
            throw new RuntimeException();
        }
    }

    /**
     * Method updatePlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updatePlayer() {
        try {
            if ((this.player != null) && this.player.getIoSession().isConnected()) {
                if (stateType == WEEDS) {
                    this.player.getActionSender().sendVarBit(patchID, phase);
                } else {
                    this.player.getActionSender().sendVarBit(patchID,
                            Growable.get(growingID).getVarbit(getPhaseType(), phase));
                }
            }
        } catch (Exception ee) {
            System.err.println("Error with farming: growable id: " + this.growingID + " " + this.phase + " "
                               + this.patchID+", "+getPhaseType());
            ee.printStackTrace();
        }
    }

    /**
     * Method unlinked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean unlinked() {
        return this.removed;
    }

    /**
     * Method timeRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String timeRemaining() {
        int cycles = ((cachedGrowing.getLength() - phase) * getDelay()) + (getDelay() - Core.timeSince(lastChange));
        int mins   = (1 + (Core.getSecondsForTicks(cycles) / 60));

        if (mins <= 0) {

        }

        return mins + " minutes";
    }

    /**
     * Method getDelay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDelay() {
        if (this.stateType == WEEDS) {
            return Core.getTicksForSeconds(20);
        }
        int id = GROW_DELAY_OTHER;
        if (this.cachedGrowing.getType() == PatchDefinition.TREE) {
           id = GROW_DELAY_TREES;
        }

        if(stateType == DISEASED)
            id *= 2;


        return id;
    }

    /**
     * Method getPhase
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPhase() {
        return phase;
    }

    /**
     * Method getState
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte getState() {
        return stateType;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {





        if (this.stateType == GROWING) {
            this.isWatered = false;

            if (this.phase < cachedGrowing.getLength()) {
                if (randomDiseaseChance() && (phase != 0)) {
                    this.changeState(DISEASED);
                } else {
                    this.phase++;
                }

                updatePlayer();

                if (this.phase == cachedGrowing.getLength() - 1) {
                    growFinished();
                    this.changeState(FINISHED);
                }
            }
        } else if (this.stateType == WEEDS) {
            if (this.phase < 3) {
                this.phase++;
            }

            updatePlayer();
        }

        lastChange = Core.currentTime;
    }

    private void growFinished() {
        if (player.getIoSession().isConnected()) {
            player.getAccount().farmUpdateRequired = true;
        }
    }

    /**
     * Method getHarvestAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHarvestAmount() {
        return cachedGrowing.getHarvestAmt();
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        if (stateType == DEAD) {
            return;
        }

        if (Core.timeSince(lastChange) >= getDelay()) {
            if (this.stateType == DISEASED) {
                changeState(DEAD);

                return;
            } else if (this.stateType == WEEDS) {
                if (this.phase > 0) {
                    this.phase--;
                    lastChange = Core.currentTime;
                    updatePlayer();
                }

                if (this.phase == 0) {
                    unlink();    // remove this
                }
            } else if (this.stateType == GROWING) {
                if (this.phase < this.cachedGrowing.getLength()) {
                    next();
                }
            }
        }
    }
}
