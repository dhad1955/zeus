package net.tazogaming.hydra.game.skill.farming2;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/07/14
 * Time: 16:00
 */
public class SubPatches {
    public static final ArrayList<Point> sub_patches[] = new ArrayList[20];

    /**
     * Method loadMultiPatches
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     */
    public static void loadMultiPatches(String path) {
        File f = new File(path);

        if (!f.exists()) {
            return;
        }

        Logger.log("[Farming]: loading list from " + path);

        HashMap<Integer, ArrayList<Point>> subPatches = new HashMap<Integer, ArrayList<Point>>();
        ArrayList<Point>                     patches    = new ArrayList<Point>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String         line;

            while ((line = br.readLine()) != null) {
                if ((line.length() == 0) || line.startsWith("//")) {
                    continue;
                }

                String[] split = line.split(" ");

                if (split[0].equals("subpatch")) {
                    int              id = Integer.parseInt(split[2]);
                    int              x  = Integer.parseInt(split[3]);
                    int              y  = Integer.parseInt(split[4]);
                    ArrayList<Point> pt = subPatches.get(id);

                    if (pt == null) {
                        pt = new ArrayList<Point>();
                        subPatches.put(id, pt);
                    }

                    pt.add(Point.location(x, y, 0));
                }
            }

            br.close();

            for (Integer key : subPatches.keySet()) {
                sub_patches[key] = subPatches.get(key);
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}
