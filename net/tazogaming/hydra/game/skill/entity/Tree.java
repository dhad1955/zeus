package net.tazogaming.hydra.game.skill.entity;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 01:03
 */
public class Tree {
    private int stumpObjectId       = -1;
    private int levelRequirement    = -1;
    private int givenLogs           = -1;
    public int  respawnTicks        = 0;
    public int  prestigeRequirement = 0;
    private int exp;
    private int actualObjectIds[];

    /**
     * Method getActualObjectId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getActualObjectId() {
        return actualObjectIds;
    }

    /**
     * Method getStump
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStump() {
        return stumpObjectId;
    }

    /**
     * Method getRequirement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRequirement() {
        return levelRequirement;
    }

    /**
     * Method getLogs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLogs() {
        return givenLogs;
    }

    /**
     * Method setLogs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     */
    public void setLogs(int x) {
        this.givenLogs = x;
    }

    /**
     * Method setRequirement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setRequirement(int id) {
        this.levelRequirement = id;
    }

    /**
     * Method setStumpId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setStumpId(int id) {
        this.stumpObjectId = id;
    }

    /**
     * Method getExp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getExp() {
        return this.exp;
    }

    /**
     * Method setExp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setExp(int id) {
        this.exp = id;
    }

    /**
     * Method setActualObjectId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setActualObjectId(int... id) {
        this.actualObjectIds = id;
    }

    /**
     * Method getRespawnTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRespawnTicks() {
        return respawnTicks;
    }

    /**
     * Method setRespawnTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param respawnTicks
     */
    public void setRespawnTicks(int respawnTicks) {
        this.respawnTicks = respawnTicks;
    }
}
