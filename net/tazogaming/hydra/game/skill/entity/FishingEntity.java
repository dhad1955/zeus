package net.tazogaming.hydra.game.skill.entity;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 05:04
 */
public class FishingEntity {
    public int prestigeRequirement = 0;
    public int groupMinimum        = 0;
    public int fishId;
    public int exp;
    public int requirement;
    public int animation;
}
