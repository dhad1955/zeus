package net.tazogaming.hydra.game.skill.entity;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 03:03
 */
public class RockEntity {
    public int   respawnTicks;
    public int   exp;
    public int   ore;
    public int   req;
    public int[] rockIds;

    /**
     * Constructs ...
     *
     *
     * @param exps
     * @param requirement
     * @param ore
     * @param respawnTicks
     * @param binding
     */
    public RockEntity(int exps, int requirement, int ore, int respawnTicks, int... binding) {
        this.ore          = ore;
        this.req          = requirement;
        rockIds           = binding;
        exp               = exps;
        this.respawnTicks = respawnTicks;
    }
}
