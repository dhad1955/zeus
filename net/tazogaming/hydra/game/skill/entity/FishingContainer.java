package net.tazogaming.hydra.game.skill.entity;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 05:04
 */
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Jackie
 * Date: 07-Mar-2009
 * Time: 00:25:51
 * To change this template use File | Settings | File Templates.
 */
public class FishingContainer {
    private static ArrayList<FishingContainer> fishingContainers = new ArrayList<FishingContainer>();

    static {
        FishingEntity turtle = new FishingEntity();

        turtle.requirement = 998;
        turtle.exp         = 1800;
        turtle.fishId      = 395;

        // manta ray
        FishingEntity mantaray = new FishingEntity();

        mantaray.requirement         = 93;
        mantaray.exp                 = 800;
        mantaray.fishId              = 389;
        mantaray.prestigeRequirement = 1;
        mantaray.groupMinimum        = Player.DONATOR;

        // shark
        FishingEntity shark = new FishingEntity();

        shark.requirement = 76;
        shark.exp         = 400;
        shark.fishId      = 383;

        // tuna
        FishingEntity tuna = new FishingEntity();

        tuna.requirement = 35;
        tuna.exp         = 160;
        tuna.fishId      = 359;

        FishingEntity rainbowFish = new FishingEntity();

        rainbowFish.fishId      = 10136;
        rainbowFish.requirement = 99;
        rainbowFish.exp         = 1;

        ArrayList<FishingEntity> harpoonFish = new ArrayList<FishingEntity>();

        harpoonFish.add(shark);
        harpoonFish.add(turtle);
        harpoonFish.add(mantaray);
        harpoonFish.add(tuna);
        harpoonFish.add(rainbowFish);

        ArrayList<FishingEntity> sharks = new ArrayList<FishingEntity>();

        sharks.add(shark);

        FishingContainer harpoon_donor = new FishingContainer(5470, 1, 311, 618, sharks);

        harpoon_donor.isDonor = true;

        FishingContainer harpooning = new FishingContainer(324, 1, 311, 618, harpoonFish);

        fishingContainers.add(harpooning);

        FishingEntity rockTail = new FishingEntity();

        rockTail.requirement         = 99;
        rockTail.exp                 = 2700;
        rockTail.fishId              = 15270;
        rockTail.prestigeRequirement = 6;
        rockTail.groupMinimum        = Player.DONATOR;

        ArrayList<FishingEntity> harpoonFishDonor = new ArrayList<FishingEntity>();

        harpoonFishDonor.add(rockTail);
        harpoonFishDonor.add(turtle);
        harpoonFishDonor.add(mantaray);

        FishingContainer harpooning2 = new FishingContainer(334, 1, 311, 618, harpoonFishDonor);

        fishingContainers.add(harpooning2);

        FishingEntity lobster = new FishingEntity();

        lobster.requirement = 40;
        lobster.exp         = 200;
        lobster.fishId      = 377;

        ArrayList<FishingEntity> lobsters = new ArrayList<FishingEntity>();

        lobsters.add(lobster);

        FishingContainer lobsterPot = new FishingContainer(324, 0, 301, 619, lobsters);

        fishingContainers.add(lobsterPot);

        FishingEntity    shrimp        = new FishingEntity();
        FishingContainer lobster_donor = new FishingContainer(324, 0, 301, 619, lobsters);

        shrimp.requirement = 1;
        shrimp.exp         = 20;
        shrimp.fishId      = 317;

        FishingEntity anchovies = new FishingEntity();

        anchovies.requirement = 15;
        anchovies.exp         = 80;
        anchovies.fishId      = 321;

        ArrayList<FishingEntity> lowLevelNetFishing = new ArrayList<FishingEntity>();

        lowLevelNetFishing.add(anchovies);
        lowLevelNetFishing.add(shrimp);

        FishingContainer netFishing = new FishingContainer(320, 0, 303, 621, lowLevelNetFishing);

        fishingContainers.add(netFishing);

        FishingEntity mackeral = new FishingEntity();

        mackeral.fishId      = 353;
        mackeral.requirement = 16;
        mackeral.exp         = 40;

        FishingEntity seaweed = new FishingEntity();

        seaweed.requirement = 16;
        seaweed.exp         = 2;
        seaweed.fishId      = 401;

        FishingEntity cod = new FishingEntity();

        cod.fishId      = 341;
        cod.requirement = 23;
        cod.exp         = 90;

        FishingEntity bass = new FishingEntity();

        bass.requirement = 46;
        bass.exp         = 200;
        bass.fishId      = 363;

        ArrayList<FishingEntity> bigNetFish = new ArrayList<FishingEntity>();

        bigNetFish.add(bass);
        bigNetFish.add(cod);
        bigNetFish.add(seaweed);
        bigNetFish.add(mackeral);

        FishingContainer bigNetFishing  = new FishingContainer(323, 0, 305, 620, bigNetFish);
        FishingContainer bigNetFishing2 = new FishingContainer(334, 0, 305, 620, bigNetFish);

        fishingContainers.add(bigNetFishing);
        fishingContainers.add(bigNetFishing2);

        FishingEntity sardine = new FishingEntity();

        sardine.requirement = 5;
        sardine.exp         = 40;
        sardine.fishId      = 327;

        FishingEntity herring = new FishingEntity();

        herring.requirement = 10;
        herring.exp         = 60;
        herring.fishId      = 345;

        FishingEntity trout = new FishingEntity();

        trout.requirement = 20;
        trout.exp         = 100;
        trout.fishId      = 335;

        FishingEntity pike = new FishingEntity();

        pike.requirement = 25;
        pike.exp         = 120;
        pike.fishId      = 349;

        FishingEntity salmon = new FishingEntity();

        salmon.requirement = 30;
        salmon.exp         = 140;
        salmon.fishId      = 331;

        ArrayList<FishingEntity> rodFishes = new ArrayList<FishingEntity>();

        rodFishes.add(salmon);
        rodFishes.add(pike);
        rodFishes.add(herring);
        rodFishes.add(trout);
        rodFishes.add(sardine);

        FishingContainer rodFishing = new FishingContainer(320, 1, 307, 622, rodFishes);

        fishingContainers.add(rodFishing);
        fishingContainers.add(harpoon_donor);
        fishingContainers.add(lobster_donor);
    }

    private ArrayList<FishingEntity> fishes  = new ArrayList<FishingEntity>();
    public boolean                   isDonor = false;
    private int                      npcId;
    private int                      npcSlot;
    private int                      objectId;
    private int                      animationId;

    /**
     * Constructs ...
     *
     *
     * @param npcId
     * @param npcSlot
     * @param objectId
     * @param animId
     * @param entites
     */
    public FishingContainer(int npcId, int npcSlot, int objectId, int animId, ArrayList<FishingEntity> entites) {
        fishes           = entites;
        this.npcId       = npcId;
        this.objectId    = objectId;
        this.npcSlot     = npcSlot;
        this.animationId = animId;
    }

    /**
     * Method getTaskGroup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */


    /**
     * Method getAnimationId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAnimationId() {
        return animationId;
    }

    /**
     * Method getObjectId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getObjectId() {
        return objectId;
    }

    /**
     * Method getLowestFishingEntity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public FishingEntity getLowestFishingEntity() {
        FishingEntity highest = null;

        for (FishingEntity entite : fishes) {
            if (highest == null) {
                highest = entite;
            } else {
                if (entite.requirement < highest.requirement) {
                    highest = entite;
                }
            }
        }

        return highest;
    }

    /**
     * Method canFish
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean canFish(Player pla) {
        for (FishingEntity entity : fishes) {
            if (entity.requirement <= pla.getCurStat(10)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getNpcId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNpcId() {
        return npcId;
    }

    /**
     * Method getNpcSlot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getNpcSlot() {
        return npcSlot;
    }

    /**
     * Method randomFish
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public FishingEntity randomFish(Player pla) {
        ArrayList<FishingEntity> possibleFish = new ArrayList<FishingEntity>();

        for (FishingEntity i : fishes) {
            if ((i.requirement <= pla.getCurStat(10)) && (i.groupMinimum <= pla.getRights())
                    && (pla.getPrestige(10) >= i.prestigeRequirement)) {
                possibleFish.add(i);
            }
        }

        if (possibleFish.size() == 0) {
            return null;
        }

        return possibleFish.get(GameMath.rand(possibleFish.size() - 1));
    }

    /**
     * Method getFishies
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<FishingEntity> getFishies() {
        return fishes;
    }

    /**
     * Method forNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npcId
     * @param npcSlot
     *
     * @return
     */
    public static FishingContainer forNPC(int npcId, int npcSlot) {
        for (FishingContainer i : fishingContainers) {
            if ((i.getNpcId() == npcId) && (i.getNpcSlot() == npcSlot)) {
                return i;
            }
        }

        return null;
    }
}
