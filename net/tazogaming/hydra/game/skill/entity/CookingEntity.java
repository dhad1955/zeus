package net.tazogaming.hydra.game.skill.entity;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.actions.CookingAction;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland         s
 * Date: 14/10/13
 * Time: 22:12
 */
public class CookingEntity {
    static final int[]              STOVE_IDS   = {
        114, 2472, 13542, 13539, 13536, 13533, 13531, 13529, 13528
    };
    static ArrayList<CookingEntity> listedMeats = new ArrayList<CookingEntity>();
    public int                      rawid, cookedid, burnedid, levelrequired, expGiven;

    /**
     * Method getBurnedID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBurnedID() {
        return burnedid;
    }

    /**
     * Method setBurnedID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param burnedid
     */
    public void setBurnedID(int burnedid) {
        this.burnedid = burnedid;
    }

    /**
     * Method getExpGiven
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getExpGiven() {
        return expGiven;
    }

    /**
     * Method setExpGiven
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param expGiven
     */
    public void setExpGiven(int expGiven) {
        this.expGiven = expGiven;
    }

    /**
     * Method getLevelrequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevelrequired() {
        return levelrequired;
    }

    /**
     * Method setLevelrequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param levelrequired
     */
    public void setLevelrequired(int levelrequired) {
        this.levelrequired = levelrequired;
    }

    /**
     * Method getRawID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRawID() {
        return rawid;
    }

    /**
     * Method setRawID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rawid
     */
    public void setRawID(int rawid) {
        this.rawid = rawid;
    }

    /**
     * Method getCookedID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCookedID() {
        return cookedid;
    }

    /**
     * Method setCookedID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cookedid
     */
    public void setCookedID(int cookedid) {
        this.cookedid = cookedid;
    }

    /**
     * Method getByRawMeat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static CookingEntity getByRawMeat(int id) {
        for (CookingEntity c : listedMeats) {
            if (c.rawid == id) {
                return c;
            }
        }

        return null;
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     *
     * @throws IOException
     */
    public static void load(String path) throws IOException {
        File           f  = new File(path);
        BufferedReader br = new BufferedReader(new FileReader(f));
        String         line;

        do {
            line = br.readLine();

            try {
                if (line == null) {
                    break;
                }

                if (line.startsWith("//") || (line.length() == 0) || line.equalsIgnoreCase("")) {
                    continue;
                }

                String[]          split  = line.split(" ");
                ArgumentTokenizer pr     = new ArgumentTokenizer(split);
                int               raw    = pr.nextInt();
                int               cooked = pr.nextInt();
                int               burnt  = pr.nextInt();
                int               level  = pr.nextInt();
                int               exp    = pr.nextInt();
                CookingEntity     cock   = new CookingEntity();

                cock.rawid         = raw;
                cock.cookedid      = cooked;
                cock.burnedid      = burnt;
                cock.levelrequired = level;
                cock.expGiven      = exp;
                listedMeats.add(cock);
            } catch (Exception ee) {
                Logger.log("Error with line: " + line);
            }
        } while (true);

        br.close();
    }

    /**
     * Method itemAtObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param pla
     * @param obj
     * @param id
     *
     * @return
     */
    public static boolean itemAtObject(Item item, Player pla, GameObject obj, int id) {
        boolean found = false;

        for (int i = 0; i < STOVE_IDS.length; i++) {
            if ((STOVE_IDS[i] == id) || (STOVE_IDS[i] == id + 256)) {
                found = true;
            }
        }

        if (!found &&!((obj != null) && (obj.getId() == 2732))) {
            return false;
        }

        CookingEntity e = getByRawMeat(item.getIndex());

        if (e == null) {
            return false;
        }

        CookingAction action = new CookingAction(pla, e, obj);

        pla.getWindowManager().setRequestedAction(action);
        pla.getActionSender().sendImage(13716, 250, e.cookedid);
        pla.getWindowManager().sendDialog(1743);

        return false;
    }
}
