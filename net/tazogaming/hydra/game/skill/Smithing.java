package net.tazogaming.hydra.game.skill;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/10/13
 * Time: 04:15
 */
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.net.ActionSender;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.game.skill.entity.SmithingItem;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;

/**
 * Rune War Game Server
 * http://hydra.tazogaming.org
 * Builder {$build}
 * net.tazo_.server
 *
 * @author Gander
 *         Date: 09-Oct-2008 {21:16:37}
 */
public class Smithing {
    private static ArrayList<SmithingHolder> holders;

    static {
        holders = new ArrayList<SmithingHolder>();
        load();

        // Create bronze smithing
    }

    /**
     * Method addItemToSmith
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param i
     * @param x
     * @param o
     * @param pla
     */
    public static void addItemToSmith(int id, int i, int x, int o, Player pla) {
        MessageBuilder spb = new MessageBuilder().setId(34).setSize(Packet.Size.VariableShort);

        spb.addShort(x).addByte((byte) 4).addInt(i).addShort(id + 1).addByte((byte) o);

        // pla.getIoSession().write(spb.toPacket());
    }

    /**
     * Method initiateSmithing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param holder
     */
    public static void initiateSmithing(Player pla, SmithingHolder holder) {
        if ((holder == null) || (holder.barId == null)) {
            return;
        }

        ActionSender as = pla.getActionSender();

        as.changeLine("", 1096);
        as.changeLine("", 11459);
        as.changeLine("", 11461);
        as.changeLine("", 1134);
        as.changeLine("", 1135);
        as.changeLine("", 13358);
        as.changeLine("", 8429);
        as.changeLine("", 8428);

        int smithLevel = pla.getCurStat(13);

        if (holder == null) {
            pla.getActionSender().sendMessage("Something went wrong, holder is null");

            return;
        }

        if (holder.sqShield.requirement > smithLevel) {
            as.replaceColor("Square shield", "@bla@");
        } else {
            as.replaceColor("Square shield", "@whi@");
        }

        if (holder.fullHelm.requirement > smithLevel) {
            as.replaceColor("Full helm", "@bla@");
        } else {
            as.replaceColor("Full helm", "@whi@");
        }

        if (holder.chainBody.requirement > smithLevel) {
            as.replaceColor("Chain body", "@bla@");
        } else {
            as.replaceColor("Chain body", "@whi@");
        }

        // lol
        if (holder.medHelm.requirement > smithLevel) {
            as.replaceColor("Medium helm", "@bla@");
        } else {
            as.replaceColor("Medium helm", "@whi@");
        }

        // lol
        if (holder.plateBody.requirement > smithLevel) {
            as.replaceColor("Plate body", "@bla@");
        } else {
            as.replaceColor("Plate body", "@whi@");
        }

        if (holder.plateLegs.requirement > smithLevel) {
            as.replaceColor("Plate legs", "@bla@");
        } else {
            as.replaceColor("Plate legs", "@whi@");
        }

        if (holder.plateSkirt.requirement > smithLevel) {
            as.replaceColor("Plate skirt", "@bla@");
        } else {
            as.replaceColor("Plate skirt", "@whi@");
        }

        if (holder.mace.requirement > smithLevel) {
            as.replaceColor("Mace", "@bla@");
        } else {
            as.replaceColor("Mace", "@whi@");
        }

        if (holder.sword != null) {
            if (holder.sword.requirement > smithLevel) {
                as.replaceColor("Sword", "@bla@");
            } else {
                as.replaceColor("Sword", "@whi@");
            }
        }

        if (holder.warHammer.requirement > smithLevel) {
            as.replaceColor("Warhammer", "@bla@");
        } else {
            as.replaceColor("Warhammer", "@whi@");
        }

        if (holder.twoHander.requirement > smithLevel) {
            as.replaceColor("2 hand sword", "@bla@");
        } else {
            as.replaceColor("2 hand sword", "@whi@");
        }

        if (holder.dartTips.requirement > smithLevel) {
            as.replaceColor("Dart tips", "@bla@");
        } else {
            as.replaceColor("Dart tips", "@whi@");
        }

        if (holder.knives.requirement > smithLevel) {
            as.replaceColor("Throwing knives", "@bla@");
        } else {
            as.replaceColor("Throwing knives", "@whi@");
        }

        if (holder.longSword.requirement > smithLevel) {
            as.replaceColor("Long sword", "@bla@");
        } else {
            as.replaceColor("Long sword", "@whi@");
        }

        if (holder.arrowTips.requirement > smithLevel) {
            as.replaceColor("Arrowtips", "@bla@");
        } else {
            as.replaceColor("Arrowtips", "@whi@");
        }

        if (holder.battleAxe.requirement > smithLevel) {
            as.replaceColor("Battle axe", "@bla@");
        } else {
            as.replaceColor("Battle axe", "@whi@");
        }

        if (holder.kiteShield.requirement > smithLevel) {
            as.replaceColor("Kite shield", "@bla@");
        } else {
            as.replaceColor("Kite shield", "@whi@");
        }

        if (holder.dagger.requirement > smithLevel) {
            as.replaceColor("Dagger", "@bla@");
        } else {
            as.replaceColor("Dagger", "@whi@");
        }

        if (holder.scimmy.requirement > smithLevel) {
            as.replaceColor("Scimitar", "@bla@");
        } else {
            as.replaceColor("Scimitar", "@whi@");
        }

        if (holder.axe.requirement > smithLevel) {
            as.replaceColor("Axe", "@bla@");
        } else {
            as.replaceColor("Axe", "@whi@");
        }

        if (pla.getInventory().hasItem(holder.barId, 3)) {
            as.replaceColor("3bars", "@gre@");
        } else {
            as.replaceColor("3bars", "@red@");
        }

        if (pla.getInventory().hasItem(holder.barId, 1)) {
            as.replaceColor("1bar", "@gre@");
        } else {
            as.replaceColor("1bar", "@red@");
        }

        if (pla.getInventory().hasItem(holder.barId, 2)) {
            as.replaceColor("2bars", "@gre@");
        } else {
            as.replaceColor("2bars", "@red@");
        }

        if (pla.getInventory().hasItem(holder.barId, 5)) {
            as.replaceColor("5bars", "@gre@");
        } else {
            as.replaceColor("5bars", "@red@");
        }

        // pla.getIoSession().write(new MessageBuilder().setId(97).addShort(994).toPacket());
        addItemToSmith(holder.dagger.id, 0, 1119, 1, pla);
        addItemToSmith(holder.axe.id, 0, 1120, 1, pla);
        addItemToSmith(holder.chainBody.id, 0, 1121, 1, pla);
        addItemToSmith(holder.medHelm.id, 0, 1122, 1, pla);
        addItemToSmith(holder.dartTips.id, 0, 1123, 1, pla);
        addItemToSmith(holder.sword.id, 1, 1119, 1, pla);
        addItemToSmith(holder.mace.id, 1, 1120, 1, pla);
        addItemToSmith(holder.plateLegs.id, 1, 1121, 1, pla);
        addItemToSmith(holder.fullHelm.id, 1, 1122, 1, pla);
        addItemToSmith(holder.arrowTips.id, 1, 1123, 15, pla);
        addItemToSmith(holder.scimmy.id, 2, 1119, 1, pla);
        addItemToSmith(holder.warHammer.id, 2, 1120, 1, pla);
        addItemToSmith(holder.plateSkirt.id, 2, 1121, 1, pla);
        addItemToSmith(holder.sqShield.id, 2, 1122, 1, pla);
        addItemToSmith(holder.knives.id, 2, 1123, 5, pla);
        addItemToSmith(holder.longSword.id, 3, 1119, 1, pla);
        addItemToSmith(holder.battleAxe.id, 3, 1120, 1, pla);
        addItemToSmith(holder.plateBody.id, 3, 1121, 1, pla);
        addItemToSmith(holder.kiteShield.id, 3, 1122, 1, pla);
        addItemToSmith(holder.twoHander.id, 4, 1119, 1, pla);
        pla.getWindowManager().showWindow(994);
    }

    /**
     * Method findItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param holder
     *
     * @return
     */
    public static SmithingItem findItem(int id, SmithingHolder holder) {
        if (holder == null) {
            return null;
        }

        if (holder.plateBody.id == id) {
            return holder.plateBody;
        }

        if (holder.battleAxe.id == id) {
            return holder.battleAxe;
        }

        if (holder.axe.id == id) {
            return holder.axe;
        }

        if (holder.dartTips.id == id) {
            return holder.dartTips;
        }

        if (holder.arrowTips.id == id) {
            return holder.arrowTips;
        }

        if (holder.fullHelm.id == id) {
            return holder.fullHelm;
        }

        if (holder.medHelm.id == id) {
            return holder.medHelm;
        }

        if (holder.dagger.id == id) {
            return holder.dagger;
        }

        if (holder.twoHander.id == id) {
            return holder.twoHander;
        }

        if (holder.chainBody.id == id) {
            return holder.chainBody;
        }

        if (holder.plateLegs.id == id) {
            return holder.plateLegs;
        }

        if (holder.plateSkirt.id == id) {
            return holder.plateSkirt;
        }

        if (holder.kiteShield.id == id) {
            return holder.kiteShield;
        }

        if (holder.sqShield.id == id) {
            return holder.sqShield;
        }

        if (holder.scimmy.id == id) {
            return holder.scimmy;
        }

        if (holder.longSword.id == id) {
            return holder.longSword;
        }

        if (holder.sword.id == id) {
            return holder.sword;
        }

        if (holder.mace.id == id) {
            return holder.mace;
        }

        if (holder.knives.id == id) {
            return holder.knives;
        }

        return null;
    }

    /**
     * Method getById
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static SmithingHolder getById(Item id) {
        for (SmithingHolder h : holders) {
            if (h.barId.getIndex() == id.getIndex()) {
                return h;
            }
        }

        return null;
    }

    /**
     * Method createItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amount
     * @param exp
     * @param requirement
     *
     * @return
     */
    public static SmithingItem createItem(int id, int amount, int exp, int requirement) {
        SmithingItem item = new SmithingItem();

        item.id          = id;
        item.amount      = amount;
        item.exp         = exp;
        item.requirement = requirement;

        return item;
    }

    /**
     * Method insert
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     * @param holder
     */
    public static void insert(SmithingItem item, SmithingHolder holder) {
        Item   theItem = item.toItem();
        String name    = theItem.getName().toLowerCase();

        if (name.contains("(t)") || name.contains("(h") || name.contains("(g)") || name.contains("(p)")
                || name.contains("(s)") || name.contains("thrown") || name.contains("pick") || name.contains("head")
                || name.contains("(+)")) {
            throw new RuntimeException();
        }

        if (name.contains("dagger")) {
            holder.dagger = item;

            return;
        }

        if (name.contains("long") && name.contains("sword")) {
            holder.longSword = item;

            return;
        } else if (name.contains("2h") && name.contains("sword")) {
            holder.twoHander = item;

            return;
        } else if (name.contains("sword")) {
            holder.sword = item;

            return;
        }

        if (name.contains("scim")) {
            holder.scimmy = item;

            return;
        }

        if (name.contains("axe") && name.contains("battle")) {
            holder.battleAxe = item;

            return;
        } else if (name.contains("axe") || name.contains("hatchet")) {
            holder.axe = item;

            return;
        }

        if (name.contains("mace")) {
            holder.mace = item;

            return;
        }

        if (name.contains("war") && name.contains("hammer")) {
            holder.warHammer = item;

            return;
        }

        if (name.contains("arrow") && name.contains("tips")) {
            holder.arrowTips = item;

            return;
        }

        if (name.contains("plate") && name.contains("body")) {
            holder.plateBody = item;

            return;
        }

        if (name.contains("chain") && name.contains("body")) {
            holder.chainBody = item;

            return;
        }

        if (name.contains("legs")) {
            holder.plateLegs = item;

            return;
        }

        if (name.contains("kite") && name.contains("shield")) {
            holder.kiteShield = item;

            return;
        } else if (name.contains("sq") && name.contains("shield")) {
            holder.sqShield = item;

            return;
        }

        if (name.contains("helm") && name.contains("full")) {
            holder.fullHelm = item;

            return;
        }

        if (name.contains("helm") && name.contains("med")) {
            holder.medHelm = item;

            return;
        }

        if (name.contains("plate") && name.contains("skirt")) {
            holder.plateSkirt = item;

            return;
        }

        if (name.contains("dart")) {
            holder.dartTips = item;

            return;
        }

        if (name.contains("knife") || name.contains("knives")) {
            holder.knives = item;

            return;
        }

        // throw new RuntimeException("Item not found: "+name);
    }

    /**
     * Method test
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void test() {}

    /**
     * Method first
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static SmithingHolder first() {
        return holders.get(5);
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load() {
        File file = new File("config/iskill/smithing/smithing.cfg");

        if (!file.exists()) {

            // Logger.err("Unable to find drop file for npc: "+id);
            return;
        }

        // Logger.log("Smithing loaded");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String         line;
            boolean        reading = false;
            SmithingHolder holder  = null;

            do {
                line = br.readLine();

                if (line == null) {
                    break;
                }

                if (line.startsWith("//") || (line.length() == 0) || line.equalsIgnoreCase("")) {
                    continue;    //
                }

                if (line.startsWith("SmithingGroup")) {
                    int      index = line.indexOf("{");
                    String[] split = line.split(" ");
                    int      barId = Integer.parseInt(split[1]);

                    holder       = new SmithingHolder();
                    holder.barId = Item.forId(barId);

                    // Logger.log("BarId:"+barId);
                    if (index == -1) {
                        throw new IOException("Smithing: failed to read config file.");
                    }

                    reading = true;

                    continue;
                }

                if (line.startsWith("}")) {

                    // Logger.log("Added: "+holder.barId.getIndex());
                    holders.add(holder);

                    continue;
                }

                if (reading) {
                    String function = line.substring(0, line.indexOf("=") - 1);
                    String value    = line.substring(line.indexOf("=") + 1);

                    if (function.equalsIgnoreCase("smithingItem")) {
                        String[]     split = value.split(" ");
                        SmithingItem item  = new SmithingItem();

                        item.id          = Integer.parseInt(split[1]);
                        item.amount      = Integer.parseInt(split[2]);
                        item.exp         = (int) Double.parseDouble(split[4]);
                        item.requirement = Integer.parseInt(split[3]);

                        try {
                            insert(item, holder);
                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }
                    }
                }
            } while (true);

            return;
        } catch (Exception ioe) {
            ioe.printStackTrace();
        }

        return;
    }
}
