package net.tazogaming.hydra.game.skill.hunter;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/11/13
 * Time: 22:08
 */
public class HuntingController implements RecurringTickEvent {
    private static LinkedList<Trap>         setup_traps       = new LinkedList<Trap>();
    private static ArrayList<HuntingEntity> trappableMonsters = new ArrayList<HuntingEntity>();
    public static final int
        WOODLANDS                                             = 0,
        DESERT                                                = 1,
        POLAR                                                 = 2,
        JUNGLE                                                = 3;

    static {
        setConfig();
        Core.submitTask(new HuntingController());
    }

    private static void tick_hunt() {
        for (Iterator<Trap> traps = setup_traps.iterator(); traps.hasNext(); ) {
            Trap t = traps.next();

            if (t.isRemoved()) {
                traps.remove();

                continue;
            }

            if ((t.getPlayer() != null)
                    && (!t.getPlayer().isLoggedIn()
                        || (Point.getDistance(t.getLocation(), t.getPlayer().getLocation()) > 15))) {
                if (t.getType() != Trap.DEAD_FALL) {
                    t.unlink();
                } else {
                    t.resetDeadfall();
                }

                continue;
            }

            if (Core.currentTime % 100 == 0) {
                if ((Core.timeSince(t.getSetupAt()) > 110) && (t.getCurrentStatus() == TrapStatus.STATUS_SETUP)) {
                    t.fall();
                } else if (((t.getCurrentStatus() == TrapStatus.STATUS_CAUGHT)
                            || (t.getCurrentStatus() == TrapStatus.STATUS_FALLEN)) && (Core.timeSince(t.getSetTime())
                               > 500)) {
                    if (t.getType() != Trap.DEAD_FALL) {
                        t.unlink();
                    } else {
                        t.resetDeadfall();
                    }
                }

                // t.fall();
            }
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {
        tick_hunt();
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getButterflyFailBareHanded
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param lvl
     *
     * @return
     */
    public static boolean getButterflyFailBareHanded(Player pla, int lvl) {
        double failChance = lvl;    // +lvl;

        failChance -= pla.getCurStat(16) / 1.5;
        failChance -= pla.getCurStat(21) / 9;

        return (GameMath.rand(100) < failChance);
    }

    /**
     * Method getButterflyFail
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param lvl
     *
     * @return
     */
    public static boolean getButterflyFail(Player pla, int lvl) {
        double failChance = 60;    // +lvl;
        int    hunterLvl  = pla.getCurStat(21);
        int    agilLvl    = pla.getCurStat(16);

        failChance -= ((hunterLvl - lvl) / 3);
        failChance -= (agilLvl * 0.4);

        return GameMath.rand(100) < failChance;
    }

    /**
     * Method catchButterfly
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param player
     *
     * @return
     */
    public static boolean catchButterfly(final NPC npc, Player player) {
        if (player.getTimers().timerActive(5) ||!npc.isVisible() || (npc.getHuntEntity() == null)
                || (npc.getHuntEntity().getType() != Trap.BUTTERFLY)) {
            return false;
        }

        boolean       hasNet = player.getEquipment().getId(3) == 10010;
        HuntingEntity entity = npc.getHuntEntity();

        if (!hasNet && (player.getCurStat(21) < entity.getLevel2())) {
            player.getActionSender().sendMessage("You need a hunting level of atleast " + entity.getLevel2()
                    + " to catch this butterfly.");

            return true;
        }

        if (hasNet && (player.getCurStat(21) < entity.getLevel())) {
            player.getActionSender().sendMessage("You need a hunting level of atleast " + entity.getLevel()
                    + " to catch this butterfly.");

            return true;
        }

        if (!player.getInventory().hasItem(10012, 1)) {
            player.getActionSender().sendMessage("You don't have a butterfly jar.");

            return true;
        }

        player.getTimers().setTime(5, 4);

        if (!hasNet) {
            player.getAnimation().animate(5215);

            if (getButterflyFailBareHanded(player, entity.getLevel2())) {
                player.getActionSender().sendMessage("The butterfly was to quick.");

                return true;
            }

            if (player.getInventory().getFreeSlots(-1) == 0) {
                player.getActionSender().sendMessage("You don't have enough free space to catch this butterfly");

                return true;
            }

            npc.setVisible(false);
            World.getWorld().submitEvent(new WorldTickEvent(20) {
                @Override
                public void tick() {

                    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public void finished() {
                    npc.setVisible(true);

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
            player.getInventory().addItem(entity.getHarvestId(), 1);

            if (entity.getHarvestId() == 10034) {
                player.getInventory().addItem(entity.getHarvestId(), 14);
            }

            if(entity.getHarvestId() == 12539)
                player.getInventory().addItem(entity.getHarvestId(), 14);

            player.addXp(21, entity.getExp() * 2);

            return true;
        } else {
            player.getAnimation().animate(5209);

            if (getButterflyFail(player, entity.getLevel())) {
                player.getActionSender().sendMessage("The butterfly was too quick.");

                return true;
            }

            if (player.getInventory().getFreeSlots(-1) == 0) {
                player.getActionSender().sendMessage("You don't have enough free space to catch this butterfly");

                return true;
            }

            player.getInventory().addItem(entity.getHarvestId(), 1);
            player.addXp(21, entity.getExp());

            if (entity.getHarvestId() == 10034) {
                player.getInventory().addItem(entity.getHarvestId(), 14);
            }

            if(entity.getHarvestId() == 12539)
                player.getInventory().addItem(entity.getHarvestId(), 14);

            npc.setVisible(false);
            World.getWorld().submitEvent(new WorldTickEvent(20) {
                @Override
                public void tick() {

                    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public void finished() {
                    npc.setVisible(true);

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });

            return true;
        }
    }

    /**
     * Method onObjectClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param la_player
     *
     * @return
     */
    public static boolean onObjectClick(int id, int x, int y, Player la_player) {
        if (id < 19000) {
            return false;
        }

        if (id == 19205) {
            Trap t = getDeadfall(x, y);

            if (t == null) {
                t = new Trap(Trap.DEAD_FALL, x, y);
                setup_traps.add(t);
            }

            if (t.getCurrentStatus() == TrapStatus.STATUS_FALLEN) {
                if (!la_player.getInventory().hasItem(946, 1)) {
                    la_player.getActionSender().sendMessage("You need a knife.");

                    return true;
                }

                if (!la_player.getInventory().hasItem(1511, 1)) {
                    la_player.getActionSender().sendMessage("You need a log");

                    return true;
                }

                t.setupDeadFall(la_player);
                la_player.getInventory().deleteItem(1511, 1);

                return true;
            } else if (t.getPlayer() != la_player) {
                la_player.getActionSender().sendMessage("This isn't your trap.");

                return true;
            } else {
                if (t.getCurrentStatus() == TrapStatus.STATUS_CAUGHT) {
                    if (la_player.getInventory().getFreeSlots(-1) == 0) {
                        la_player.getActionSender().sendMessage("You don't have enough free slots");

                        return true;
                    }

                    la_player.getInventory().addItem(t.getCaught().getHuntEntity().getHarvestId(), 1);

                    if (t.getCaught().getHuntEntity().getHarvestId() == 10034) {
                        la_player.getInventory().addItem(t.getCaught().getHuntEntity().getHarvestId(), 14);
                    }

                    if (la_player.isTenth(21)) {
                        la_player.getInventory().addItem(t.getCaught().getHuntEntity().getHarvestId(), 14);
                    }

                    if(t.getCaught().getHuntEntity().getHarvestId() == 15239){
                        la_player.getInventory().addItem(15239, GameMath.rand(20));
                    }

                    la_player.addXp(21, t.getCaught().getHuntEntity().getExp());
                    la_player.addPoints(Points.SKILLING_POINTS, t.getCaught().getHuntEntity().getLevel() * 3);
                    t.resetDeadfall();

                    return true;
                }
            }
        }

        for (Trap t : setup_traps) {
            if ((t.getLocation().getX() == x) && (t.getLocation().getY() == y)) {
                if (t.getPlayer() != la_player) {
                    la_player.getActionSender().sendMessage("This isn't your trap, leave it alone!");

                    return true;
                }

                if (t.getCurrentStatus() == TrapStatus.STATUS_REMOVED) {
                    return true;
                }

                if ((t.getCurrentStatus() == TrapStatus.STATUS_FALLING)
                        || (t.getCurrentStatus() == TrapStatus.STATUS_LANDING)) {
                    return true;
                }

                if ((t.getCurrentStatus() == TrapStatus.STATUS_SETUP)
                        || (t.getCurrentStatus() == TrapStatus.STATUS_FALLEN)) {
                    la_player.getActionSender().sendMessage("You dismantle your trap.");
                    t.unlink();
                    la_player.getInventory().addItemDrop(Item.forId((t.getType() == Trap.BIRD_SNARE)
                            ? 10006
                            : 10008), 1);

                    return true;
                }

                if (t.getCurrentStatus() == TrapStatus.STATUS_CAUGHT) {
                    if (la_player.getInventory().getFreeSlots(0) < 1) {
                        la_player.getActionSender().sendMessage("You don't have enough free slots.");

                        return true;
                    }

                    t.unlink();
                    la_player.getInventory().addItem(Item.forId(t.getCaught().getHuntEntity().getHarvestId()), 1);

                    if (t.getCaught().getHuntEntity().getHarvestId() == 10034) {
                        la_player.getInventory().addItem(Item.forId(t.getCaught().getHuntEntity().getHarvestId()), 15);
                    }

                    if(t.getType() == Trap.BIRD_SNARE)
                        la_player.addAchievementProgress2(Achievement.BIRDWATCH, 1);

                    if(t.getCaught().getHuntEntity().getHarvestId() == 15239){
                        la_player.getInventory().addItem(15239, GameMath.rand(20));
                    }

                    if(t.getCaught().getHuntEntity().getHarvestId() == 10034)
                        la_player.addAchievementProgress2(Achievement.CHINCATCHER, 1);

                    la_player.addXp(21, t.getCaught().getHuntEntity().getExp());
                    la_player.addPoints(Points.SKILLING_POINTS, t.getCaught().getHuntEntity().getLevel() * 3);
                    la_player.getInventory().addItemDrop(Item.forId((t.getType() == Trap.BIRD_SNARE)
                            ? 10006
                            : 10008), 1);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method getTrapsInView
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public static ArrayList<Trap> getTrapsInView(NPC npc) {
        if (setup_traps.size() == 0) {
            return null;
        }

        ArrayList<Trap> returnTraps = null;

        for (Trap trap : setup_traps) {
            if ((trap.getCurrentStatus() != TrapStatus.STATUS_SETUP) || trap.isActive()) {
                continue;
            }

            if (trap.getType() != npc.getHuntEntity().getType()) {
                continue;
            }

            if (trap.getPlayer().getCurStat(21) < npc.getHuntEntity().getLevel()) {
                continue;
            }

            if (Point.getDistance(npc.getLocation(), trap.getLocation()) < 6) {
                if (returnTraps == null) {
                    returnTraps = new ArrayList<Trap>();
                }

                returnTraps.add(trap);
            }
        }

        return returnTraps;
    }

    /**
     * Method onItemClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemSlot
     * @param pla
     *
     * @return
     */
    public static boolean onItemClick(int itemSlot, Player pla) {
        Item item = pla.getInventory().getItem(itemSlot);

        if (item == null) {
            return true;
        }

        if ((item.getIndex() != 10006) && (item.getIndex() != 10008)) {
            return false;
        }

        if (pla.getHeight() != 0) {
            pla.getActionSender().sendMessage("You can't setup a trap here.");

            return true;
        }

        Tile t = World.getWorld().getTile(pla.getX(), pla.getY(), pla.getHeight());

        if (t.hasObjects() || ClippingDecoder.blockedEast(pla.getX(), pla.getY(), pla.getHeight())) {
            pla.getActionSender().sendMessage("You can't setup a trap here.");

            return true;
        }

        if (checkTraps(pla)) {
            return true;
        }

        if (item.getIndex() == 10008) {
            pla.getAnimation().animate(5212);
            pla.getInventory().deleteItemFromSlot(null, 1, itemSlot);

            Trap box_trap = new Trap(Trap.BOX_TRAP, pla);

            do_walk(pla);
            setup_traps.add(box_trap);
            return true;
        }

        if (item.getIndex() == 10006) {
            pla.getAnimation().animate(5208);
            pla.getInventory().deleteItemFromSlot(null, 1, itemSlot);

            Trap bird_snare = new Trap(Trap.BIRD_SNARE, pla);

            setup_traps.add(bird_snare);
            do_walk(pla);

            return true;
        }

        return false;
    }

    /**
     * Method getMaxTraps
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static int getMaxTraps(Player pla) {
        int lvl = pla.getCurStat(21);

        if (lvl < 20) {
            return 1;
        }

        if (lvl < 40) {
            return 2;
        }

        if (lvl < 60) {
            return 3;
        }

        if (lvl < 80) {
            return 4;
        }

        return 5;
    }

    /**
     * Method getLevelForTraps
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param trap
     *
     * @return
     */
    public static int getLevelForTraps(int trap) {
        if (trap == 2) {
            return 20;
        }

        if (trap == 3) {
            return 40;
        }

        if (trap == 4) {
            return 60;
        }

        if (trap == 5) {
            return 80;
        }

        return -1;
    }

    /**
     * Method getTrapCountForPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static final int getTrapCountForPlayer(Player pla) {
        int c = 0;

        for (Trap t : setup_traps) {
            if (t.getPlayer() == pla) {
                c++;
            }
        }

        return c;
    }

    /**
     * Method checkTraps
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static boolean checkTraps(Player pla) {
        int maxTraps    = getMaxTraps(pla);
        int trapsActive = getTrapCountForPlayer(pla);

        if ((trapsActive + 1 > maxTraps) && (trapsActive != 0)) {
            int c = getLevelForTraps(trapsActive + 1);

            if (c == -1) {
                pla.getActionSender().sendMessage("You cant set up more than 5 traps");
            } else {
                pla.getActionSender().sendMessage("You need a hunter level of atleast " + c
                                                  + " to set up this many traps.");
            }

            return true;
        }

        return false;
    }

    private static Trap getDeadfall(int x, int y) {
        for (Trap t : setup_traps) {
            if (t.getType() == Trap.DEAD_FALL) {
                if ((t.getLocation().getX() == x) && (t.getLocation().getY() == y)) {
                    return t;
                }
            }
        }

        return null;
    }

    private static void do_walk(final Player pla) {
        pla.setActionsDisabled(true);
        pla.getRoute().resetPath();
        pla.getActionSender().sendCloseWalkingFlag();
        pla.getTickEvents().add(new PlayerTickEvent(pla, 4, false, true) {
            @Override
            public void doTick(Player owner) {
                if (getCurTick() == 4) {
                    pla.getRoute().addPoint(Point.location(pla.getX() + 1, pla.getY(), pla.getHeight()));
                } else {
                    pla.setActionsDisabled(false);
                    terminate();
                }

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    /**
     * Method getEntity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public static HuntingEntity getEntity(NPC npc) {
        if ((npc.getId() >= 5073) && (npc.getId() <= 8000)) {
            for (HuntingEntity e : trappableMonsters) {
                if (e.getID() == npc.getId()) {
                    return e;
                }
            }
        }

        return null;
    }

    /**
     * Method setConfig
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void setConfig() {

        // crimson swift
        trappableMonsters.add(new HuntingEntity(5073, "Crimson swift",
                Trap.BIRD_SNARE).setCaughtObjectId(19180).setLandingObject(19179,
                    3000).setAreaType(JUNGLE).setHarvestExp(34).setHarvestId(10088));

        // golden warbler
        trappableMonsters.add(new HuntingEntity(5075, "Golden Warbler",
                Trap.BIRD_SNARE).setCaughtObjectId(19184).setLandingObject(19183,
                    3000).setAreaType(WOODLANDS).setHarvestExp(800).setLevel(5).setHarvestId(10090));

        // copper longtail
        trappableMonsters.add(new HuntingEntity(5076, "Copper longtail",
                Trap.BIRD_SNARE).setCaughtObjectId(19186).setLandingObject(19185,
                    3000).setAreaType(WOODLANDS).setHarvestExp(66).setLevel(9).setHarvestId(10091));

        // twitch cerulian
        trappableMonsters.add(new HuntingEntity(5074, "twich cerulian",
                Trap.BIRD_SNARE).setCaughtObjectId(19182).setLandingObject(19181,
                    3000).setAreaType(POLAR).setHarvestExp(64).setLevel(11).setHarvestId(10089));

        // tropical cocktail or w.e
        trappableMonsters.add(new HuntingEntity(5072, "Tropical wagtail",
                Trap.BIRD_SNARE).setCaughtObjectId(19178).setLandingObject(19177,
                    3000).setAreaType(JUNGLE).setHarvestExp(95).setLevel(19).setHarvestId(10087));

        // / box trap
        trappableMonsters.add(new HuntingEntity(5081, "Ferret",
                Trap.BOX_TRAP).setCaughtObjectId(19189).setLandingObject(19195,
                    1000).setAreaType(WOODLANDS).setLevel(27).setHarvestExp(116).setHarvestId(10092));
        trappableMonsters.add(new HuntingEntity(5079, "Chinchompa",
                Trap.BOX_TRAP).setCaughtObjectId(19189).setLandingObject(19196,
                    1000).setAreaType(WOODLANDS).setLevel(53).setHarvestExp(198).setHarvestId(10033));
        trappableMonsters.add(new HuntingEntity(5080, "Canavarious Chinchompa",
                Trap.BOX_TRAP).setCaughtObjectId(19189).setLandingObject(19198,
                    1000).setAreaType(WOODLANDS).setLevel(65).setHarvestExp(265).setHarvestId(10034));

        trappableMonsters.add(new HuntingEntity(7010, "Grenwall", Trap.BOX_TRAP).setLandingObject(19191, 100).
                setCaughtObjectId(19192).setHarvestExp(800) .setLevel(77).setHarvestId(12539));

        // DEAD FALL
        trappableMonsters.add(new HuntingEntity(5089, "Wild Kebbit",
                Trap.DEAD_FALL).setCaughtObjectId(19215).setLandingObject(19213,
                    1000).setAreaType(WOODLANDS).setLevel(23).setHarvestExp(128).setHarvestId(10113));
        trappableMonsters.add(new HuntingEntity(5088, "harpoon Kebbit",
                Trap.DEAD_FALL).setCaughtObjectId(19215).setLandingObject(19212,
                    1000).setAreaType(JUNGLE).setLevel(33).setHarvestExp(168).setHarvestId(10129));
        trappableMonsters.add(new HuntingEntity(5086, "Prickly Kebbit",
                Trap.DEAD_FALL).setCaughtObjectId(19215).setLandingObject(19207,
                    1000).setAreaType(WOODLANDS).setLevel(37).setHarvestExp(204).setHarvestId(10105));
        trappableMonsters.add(new HuntingEntity(5087, "Prickly Kebbit",
                Trap.DEAD_FALL).setCaughtObjectId(19215).setLandingObject(19209,
                    1000).setAreaType(POLAR).setLevel(75).setHarvestExp(200).setHarvestId(10109));
        trappableMonsters.add(
            new HuntingEntity(5085, "Ruby Harvest", Trap.BUTTERFLY).setLevel(15).setOtherLevel(80).setHarvestExp(
                24).setAreaType(WOODLANDS).setHarvestId(10020));
        trappableMonsters.add(
            new HuntingEntity(5084, "Sapphire gladice", Trap.BUTTERFLY).setLevel(25).setOtherLevel(85).setHarvestExp(
                34).setAreaType(POLAR).setHarvestId(10018));
        trappableMonsters.add(
            new HuntingEntity(5083, "Snowy knight", Trap.BUTTERFLY).setLevel(35).setOtherLevel(90).setHarvestExp(
                44).setAreaType(POLAR).setHarvestId(10016));
        trappableMonsters.add(
            new HuntingEntity(5082, "Black Warlock", Trap.BUTTERFLY).setLevel(45).setOtherLevel(95).setHarvestExp(
                54).setAreaType(JUNGLE).setHarvestId(10014));
    }
}
