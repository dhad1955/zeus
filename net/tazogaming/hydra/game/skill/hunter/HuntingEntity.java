package net.tazogaming.hydra.game.skill.hunter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/11/13
 * Time: 22:08
 */
public class HuntingEntity {
    private int areaType      = 0;
    private int levelRequired = 0;
    private int npcId         = 0;
    private int type          = 0;
    private int harvestId     = 0;
    private int harvestExp    = 0;
    private int otherLvl      = 0;
    private int landingObjectId;
    private int landingObjectTime;
    private int caughtOjectId;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param name
     * @param type
     */
    public HuntingEntity(int id, String name, int type) {
        this.npcId = id;
        this.type  = type;
    }

    /**
     * Method getLandingObjectId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLandingObjectId() {
        return landingObjectId;
    }

    /**
     * Method getLevel2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevel2() {
        return otherLvl;
    }

    /**
     * Method setOtherLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lvl
     *
     * @return
     */
    public HuntingEntity setOtherLevel(int lvl) {
        otherLvl = lvl;

        return this;
    }

    /**
     * Method getLandingObjectTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLandingObjectTime() {
        return landingObjectTime;
    }

    /**
     * Method getCaughtId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCaughtId() {
        return caughtOjectId;
    }

    /**
     * Method setLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lvl
     *
     * @return
     */
    public HuntingEntity setLevel(int lvl) {
        this.levelRequired = lvl;

        return this;
    }

    /**
     * Method setLandingObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param objId
     * @param time
     *
     * @return
     */
    public HuntingEntity setLandingObject(int objId, int time) {
        this.landingObjectId   = objId;
        this.landingObjectTime = time;

        return this;
    }

    /**
     * Method getID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getID() {
        return npcId;
    }

    /**
     * Method setCaughtObjectId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public HuntingEntity setCaughtObjectId(int id) {
        this.caughtOjectId = id;

        return this;
    }

    /**
     * Method setHarvestExp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param xp
     *
     * @return
     */
    public HuntingEntity setHarvestExp(int xp) {
        xp /= 4;

        this.harvestExp = xp;

        return this;
    }

    /**
     * Method setHarvestId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public HuntingEntity setHarvestId(int id) {
        this.harvestId = id;

        return this;
    }

    /**
     * Method getHarvestId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHarvestId() {
        return harvestId;
    }

    /**
     * Method getExp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getExp() {
        return harvestExp;
    }

    /**
     * Method getLevel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevel() {
        return levelRequired;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return this.type;
    }

    /**
     * Method setAreaType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     *
     * @return
     */
    public HuntingEntity setAreaType(int t) {
        areaType = t;

        return this;
    }

    /**
     * Method getAreaType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAreaType() {
        return areaType;
    }
}
