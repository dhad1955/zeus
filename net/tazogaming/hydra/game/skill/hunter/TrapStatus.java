package net.tazogaming.hydra.game.skill.hunter;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/11/13
 * Time: 22:10
 */
public enum TrapStatus {
    STATUS_SETUP, STATUS_LANDING, STATUS_CAUGHT, STATUS_FALLING, STATUS_FALLEN, STATUS_REMOVED
}
