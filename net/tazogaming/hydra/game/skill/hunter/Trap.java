package net.tazogaming.hydra.game.skill.hunter;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.gametick.WorldTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.security.SecureRandom;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/11/13
 * Time: 22:09
 */
public class Trap {
    public static final int
        BIRD_SNARE                              = 0,
        BOX_TRAP                                = 1,
        DEAD_FALL                               = 2,
        BUTTERFLY                               = 6;
    private NPC        currently_moving_towards = null;
    private boolean    isRemoved                = false;
    private int        type;
    private Player     player;
    private GameObject currentObject;
    private TrapStatus currentStatus;
    private int        setupAt;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param player
     */
    public Trap(int type, Player player) {
        this.type          = type;
        this.player        = player;
        this.currentObject = new GameObject();
        this.currentObject.setObjectType(10);
        this.currentObject.setCurrentRotation(0);
        this.currentObject.setIdentifier(get_obj_for_type(type));
        this.currentObject.setCurrentInstance(player.getCurrentInstance());
        this.currentObject.setLocation(player.getLocation());
        World.getWorld().getObjectManager().registerObject(currentObject);
        this.setupAt = Core.currentTime;

        if (type != DEAD_FALL) {
            this.currentStatus = TrapStatus.STATUS_SETUP;
        } else {
            currentStatus = TrapStatus.STATUS_FALLEN;
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param x
     * @param y
     */
    public Trap(int type, int x, int y) {
        this.type          = type;
        this.currentObject = new GameObject();
        this.currentObject.setObjectType(10);
        this.currentObject.setCurrentRotation(0);
        this.currentObject.setIdentifier(get_obj_for_type(type));
        this.currentObject.setLocation(Point.location(x, y, 0));
        currentStatus = TrapStatus.STATUS_FALLEN;
    }

    /**
     * Method setupDeadFall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void setupDeadFall(Player player) {
        this.player = player;
        this.currentObject.setIdentifier(19206);
        this.currentObject.next();
        currentStatus = TrapStatus.STATUS_SETUP;
        setupAt       = Core.currentTime;
    }

    /**
     * Method resetDeadfall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void resetDeadfall() {
        player        = null;
        currentStatus = TrapStatus.STATUS_FALLEN;
        currentObject.setIdentifier(19205);
        currentObject.next();
    }

    /**
     * Method getSetTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSetTime() {
        return setupAt;
    }

    /**
     * Method snag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void snag() {
        if (currentStatus != TrapStatus.STATUS_SETUP) {
            return;
        }

        final HuntingEntity land_entity = currently_moving_towards.getHuntEntity();

        this.currentObject.setIdentifier(land_entity.getLandingObjectId());
        this.currentObject.next();
        this.currentStatus = TrapStatus.STATUS_LANDING;
        this.currently_moving_towards.setVisible(false);
        World.getWorld().submitEvent(new WorldTickEvent(70) {
            @Override
            public void tick() {

                // To change body of implemented methods use File | Settings | File Templates.
            }
            @Override
            public void finished() {
                currently_moving_towards.setVisible(true);
            }
        });
        World.getWorld().submitEvent(new WorldTickEvent(land_entity.getLandingObjectTime() / 600) {
            @Override
            public void tick() {}
            @Override
            public void finished() {
                currentObject.setIdentifier(land_entity.getCaughtId());
                currentObject.next();
                currentStatus = TrapStatus.STATUS_CAUGHT;

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    /**
     * Method getCaught
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC getCaught() {
        return currently_moving_towards;
    }

    /**
     * Method fall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void fall() {
        if (currentStatus == TrapStatus.STATUS_SETUP) {
            this.currentObject.setIdentifier(get_obj_for_type(type) + 1);
            currentStatus = TrapStatus.STATUS_FALLING;
            this.currentObject.next();
            World.getWorld().submitEvent(new WorldTickEvent(2) {
                @Override
                public void tick() {

                    // To change body of implemented methods use File | Settings | File Templates.
                }
                @Override
                public void finished() {
                    if (type == DEAD_FALL) {
                        resetDeadfall();
                        terminate();

                        return;
                    }

                    currentStatus = TrapStatus.STATUS_FALLEN;
                    currentObject.setIdentifier(getFallenID(type));
                    currentObject.next();
                    terminate();
                    player.getActionSender().sendMessage("Your trap has fallen.");

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
        }
    }

    /**
     * Method getHunterBonus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param area
     *
     * @return
     */
    public static double getHunterBonus(Player pla, int area) {
        int    top   = pla.getEquipment().getId(Equipment.TORSO);
        int    legs  = pla.getEquipment().getId(Equipment.LEGS);
        int    hat   = pla.getEquipment().getId(Equipment.HAT);
        double bonus = 0.0;

        switch (area) {
        case HuntingController.WOODLANDS :
            if (top == 10043) {
                bonus += 7.0;
            }

            if (legs == 10041) {
                bonus += 7.0;
            }

            if (hat == 10045) {
                bonus += 7.0;
            }

            if (top == 10053) {
                bonus += 5.0;
            }

            if (top == 10055) {
                bonus += 5.0;
            }

            break;

        case HuntingController.JUNGLE :
            if (top == 10043) {
                bonus += 7.0;
            }

            if (legs == 10041) {
                bonus += 7.0;
            }

            if (hat == 10045) {
                bonus += 7.0;
            }

            if (legs == 10059) {
                bonus += 5.0;
            }

            if (top == 10057) {
                bonus += 5.0;
            }

            break;

        case HuntingController.DESERT :
            if (legs == 10047) {
                bonus += 7.0;
            }

            if (top == 10049) {
                bonus += 7.0;
            }

            if (hat == 10049) {
                bonus += 7.0;
            }

            if (legs == 10063) {
                bonus += 5.0;
            }

            if (top == 10061) {
                bonus += 5.0;
            }

            break;

        case HuntingController.POLAR :
            if (legs == 10035) {
                bonus += 7.0;
            }

            if (top == 10037) {
                bonus += 7.0;
            }

            if (hat == 10039) {
                bonus += 7.0;
            }

            if (legs == 10067) {
                bonus += 5.0;
            }

            if (top == 10065) {
                bonus += 5.0;
            }

            break;
        }

        return bonus;
    }

    /**
     * Method getHunterFail
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lvl
     * @param type
     * @param pla
     *
     * @return
     */
    public static boolean getHunterFail(double lvl, int type, Player pla) {
        double       plrLvl        = pla.getCurStat(21);
        SecureRandom cookingRandom = new SecureRandom();
        double       burn_chance   = (55.0 - getHunterBonus(pla, type));
        double       cook_level    = (double) plrLvl;    // [7];
        double       lev_needed    = (double) lvl;
        double       burn_stop     = (double) lvl + 40;
        double       multi_a       = (burn_stop - lev_needed);
        double       burn_dec      = (burn_chance / multi_a);
        double       multi_b       = (cook_level - lev_needed);

        burn_chance -= (multi_b * burn_dec);

        double randNum = cookingRandom.nextDouble() * 100.0;

        // "With a " + burn_chance + "% chance of
        // burning.."
        return burn_chance > randNum;
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unlink() {
        isRemoved = true;
        this.currentObject.remove();
        currentStatus = TrapStatus.STATUS_REMOVED;
    }

    /**
     * Method isRemoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isRemoved() {
        return isRemoved;
    }


    private static int getFallenID(int type) {
        switch (type) {
            case BIRD_SNARE:
                return 19175;
            case BOX_TRAP:
                return 19192;
            case DEAD_FALL:
                return 19205;
        }
        return 19175;
    }
    private static int get_obj_for_type(int type) {
        switch (type) {
        case BIRD_SNARE :
            return 19175;

        case BOX_TRAP :
            return 19187;

        case DEAD_FALL :
            return 19205;
        }

        throw new RuntimeException("Invalid type: " + type);
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Method getSetupAt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getSetupAt() {
        return setupAt;
    }

    /**
     * Method setSetupAt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param setupAt
     */
    public void setSetupAt(int setupAt) {
        this.setupAt = setupAt;
    }

    /**
     * Method getCurrentStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public TrapStatus getCurrentStatus() {
        return currentStatus;
    }

    /**
     * Method setCurrentStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentStatus
     */
    public void setCurrentStatus(TrapStatus currentStatus) {
        this.currentStatus = currentStatus;
    }

    /**
     * Method getCurrentObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public GameObject getCurrentObject() {
        return currentObject;
    }

    /**
     * Method setCurrentObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentObject
     */
    public void setCurrentObject(GameObject currentObject) {
        this.currentObject = currentObject;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method getLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getLocation() {
        return currentObject.getLocation();
    }

    /**
     * Method setController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * Method setTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void setTarget(NPC npc) {
        this.currently_moving_towards = npc;
    }

    /**
     * Method isActive
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isActive() {
        return currently_moving_towards != null;
    }
}
