package net.tazogaming.hydra.game.skill.construction.costumeroom;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.ArgumentTokenizer;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/02/14
 * Time: 02:23
 */
public class CostumeRuleTable {
    private static CostumeRuleTable[]   cache           = new CostumeRuleTable[20];
    private HashMap<Integer, Boolean> allowedIndicies = new HashMap<Integer, Boolean>();
    private ArrayList<Integer>          inherits;
    private int                         object_id;

    /**
     * Constructs ...
     *
     *
     * @param idx
     * @param objectId
     */
    public CostumeRuleTable(int idx, int objectId) {
        this.object_id = objectId;
        cache[idx]     = this;
        inherits       = new ArrayList<Integer>();
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static CostumeRuleTable get(int id) {
        return cache[id];
    }

    /**
     * Method putRule
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemid
     */
    public void putRule(int itemid) {
        allowedIndicies.put(itemid, true);
    }

    /**
     * Method addInheritent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     */
    public void addInheritent(int index) {
        inherits.add(index);
    }

    /**
     * Method isAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public boolean isAllowed(int itemId) {
        if (allowedIndicies.containsKey(itemId)) {
            return true;
        }

        for (Integer index : inherits) {
            if (cache[index].allowedIndicies.containsKey(itemId)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method loadList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param filePath
     */
    public static final void loadList(File filePath) {
        try {
            BufferedReader   reader  = new BufferedReader(new FileReader(filePath));
            String           line    = null;
            CostumeRuleTable current = null;

            while ((line = reader.readLine()) != null) {
                ArgumentTokenizer pr = new ArgumentTokenizer(line.split(" "));

                if (line.startsWith("box")) {
                    pr.next();

                    int id = pr.nextInt();

                    current = new CostumeRuleTable(id, 0);
                }

                if (line.startsWith("alloweditem")) {
                    pr.next();
                    pr.next();
                    current.putRule(pr.nextInt());
                } else if (line.startsWith("inherit")) {
                    pr.next();
                    pr.next();
                    current.addInheritent(pr.nextInt());
                }
            }

            reader.close();
        } catch (Exception ee) {}
    }
}
