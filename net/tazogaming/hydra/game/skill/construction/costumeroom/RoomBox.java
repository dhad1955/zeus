package net.tazogaming.hydra.game.skill.construction.costumeroom;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/02/14
 * Time: 02:34
 */
public class RoomBox {
    private int   pointer = 0;
    private int   box_id  = 0;
    private int[] items;
    private int[] amts;

    /**
     * Constructs ...
     *
     *
     * @param amt
     * @param id
     */
    public RoomBox(int amt, int id) {
        box_id = id;
        amt    = 40;
        amts   = new int[amt];
        items  = new int[amt];

        for (int i = 0; i < items.length; i++) {
            items[i] = -1;
        }
    }

    /**
     * Method getItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getItems() {
        return items;
    }

    /**
     * Method setItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param items
     */
    public void setItems(int[] items) {
        this.items = items;
    }

    /**
     * Method getAmts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getAmts() {
        return amts;
    }

    /**
     * Method setAmts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amts
     */
    public void setAmts(int[] amts) {
        this.amts = amts;
    }

    /**
     * Method getBox_id
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBox_id() {
        return box_id;
    }

    /**
     * Method setBox_id
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param box_id
     */
    public void setBox_id(int box_id) {
        this.box_id = box_id;
    }

    /**
     * Method setItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ind
     * @param id
     * @param amt
     */
    public void setItem(int ind, int id, int amt) {
        items[ind] = id;
        amts[ind]  = amt;
    }

    /**
     * Method addItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amt
     *
     * @return
     */
    public boolean addItem(int id, int amt) {
        for (int i = 0; i < items.length; i++) {
            if ((items[i] == -1) || (items[i] == 0)) {
                items[i] = id;
                amts[i]  = amt;

                return true;
            } else if (items[i] == id) {
                amts[i] += amt;

                return true;
            }
        }

        return false;
    }

    /**
     * Method getItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param item
     *
     * @return
     */
    public int getItem(int item) {
        return items[item];
    }

    /**
     * Method withdrawItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slot
     */
    public void withdrawItem(int slot) {
        amts[slot]--;

        if (amts[slot] == 0) {
            items[slot] = -1;
        }

        reOrder();
    }

    /**
     * Method getCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCount() {
        int slot = 0;
        int c    = 0;

        while (items[slot++] != -1) {
            c++;
        }

        return c;
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void render(Player player) {
        player.getActionSender().sendModelGrid(items, amts, 4640);
    }

    /**
     * Method reOrder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reOrder() {
        int[] tmp1    = new int[items.length];
        int[] tmp2    = new int[items.length];
        int   pointer = 0;

        for (int i = 0; i < items.length; i++) {
            if (items[i] > 0) {
                tmp1[pointer]   = items[i];
                tmp2[pointer++] = amts[i];
            } else {
                tmp1[i] = -1;
            }
        }

        items = tmp1;
        amts  = tmp2;
    }
}
