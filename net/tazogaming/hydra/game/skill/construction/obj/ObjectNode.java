package net.tazogaming.hydra.game.skill.construction.obj;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.object.GameObject;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/14
 * Time: 16:37
 */
public class ObjectNode {
    public static final int[] CHAIR_IDS = {
        13581, 13582, 13583, 13584, 13585, 13586, 13587, 13300, 13301, 13302, 13303, 13304, 13305, 13306, 13665, 13666,
        13667, 13668, 13669, 13670, 13671
    };
    public static final int[] CHAIR_ANIMS = {
        4073, 4075, 4079, 4081, 4083, 4085, 4087, 4090, 4091, 4093, 4095, 4095, 4099, 4101, 4111, 4112, 4113, 4114,
        4115, 4116, 4117, 4118, 4119,
    };
    public static final int      RUG_MAPPINGS_OUTSKIRTS[] = { 13589, 13592, 13595 };
    public static final int      RUG_MAPPINGS_CENTER[]    = { 13590, 13593, 13596 };
    public static final int      RUG_MAPPINGS_CORNER[]    = { 13588, 13591, 13594 };
    public static final int      MAT_MAPPINGS_OUTSKIRTS[] = { 13128, 13134, 13139 };
    public static final int      MAT_MAPPINGS_CORNER[]    = { 13126, 13135, 13138 };
    public static final int      MAT_MAPPINGS_CENTER[]    = { 13127, 13136, 13140 };
    public static final int      FLOOR_TYPES_CORNER[]     = { 13334, 13333, 13337 };
    public static final int      FLOOR_TYPES_OUTSKIRT[]   = { 13335, 13332, 13337 };
    public static final int      FLOOR_TYPES_CENTER[]     = { 13334, 13331, 13337 };
    public static final int      PRISON_WALL_TYPES[]      = { 13313, 13316, 13319, 13322, 13325 };
    public static final int      HEDGING_TYPES[]          = {
        13456, 13459, 13462, 13465, 13468, 13471, 13474
    };
    public static final int      WALL_TYPES[]             = {
        13449, 13450, 13451, 13452, 13453, 13454, 13455
    };
    public static final int[]    PRAYER_ALTAR_IDS         = {
        13180, 13183, 13816, 13189, 13192, 13195, 13198
    };
    public static final double[] PRAYER_ALTAR_XP_BONUS    = {
        1.00, 1.10, 1.25, 1.50, 2.00, 2.50, 3.50
    };
    public static final int[][]  PRAYER_XP                = {
        { 526, 5 }, { 2859, 5 }, { 528, 1200 }, { 530, 5 }, { 532, 1 }, { 3125, 15 }, { 4812, 15 }, { 3181, 18 },
        { 3123, 25 }, { 6812, 50 }, { 534, 72 }, { 536, 72 }, { 4830, 84 }, { 4832, 96 }, { 6729, 125 }, { 4834, 310 },
        { 18830, 500 },  { 18832, 500 },
    };
    public static final int[] INCENSE_BURNERS = { 8073, 8074, 8075 };
    public static final int
        TYPE_DOOR                             = 0,
        TYPE_WINDOW                           = 1,
        TYPE_NORMAL                           = 2,
        TYPE_STAIRS                           = 3,
        TYPE_TRAP                             = 5,
        TYPE_MONSTER                          = 6;
    private int        type                   = 0;
    private int        regionX, regionY;
    private int        id;
    private GameObject object;




    /**
     * Constructs ...
     *
     *
     * @param id
     * @param obj
     * @param x
     * @param y
     * @param type
     */
    public ObjectNode(int id, GameObject obj, int x, int y, int type) {
        this.id      = id;
        this.object  = obj;
        this.regionX = x;
        this.regionY = y;
        this.type    = type;
    }

    /**
     * Method getCostumeBoxID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj
     *
     * @return
     */
    public static int getCostumeBoxID(int obj) {
        switch (obj) {
        case 18804 :
            return 1;

        case 18806 :
            return 2;

        case 18808 :
            return 3;

        case 18771 :
        case 18770 :
        case 18769 :
        case 18768 :
        case 18767 :
        case 18766 :
            return 5;

        case 18772 :
        case 18774 :
        case 18776 :
            return 0;

        case 18798 :
        case 18800 :
        case 18802 :
            return 7;

        case 18786 :
        case 18788 :
        case 18790 :
        case 18792 :
        case 18794 :
        case 18796 :
            return 9;

        case 18778 :
        case 18780 :
        case 18782 :
            return 10;
        }

        return -1;
    }

    /**
     * Method getMaxStorage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static final int getMaxStorage(int id) {
        switch (id) {
        case 1 :
        case 2 :
        case 3 :
            return 30;

        case 5 :
            return 20;

        case 0 :
            return 20;

        case 7 :
            return 20;

        case 10 :
            return 20;
        }

        return -1;
    }

    /**
     * Method isHedge
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj_id
     *
     * @return
     */
    public static final boolean isHedge(int obj_id) {
        if ((obj_id == 15370) || (obj_id == 15371) || (obj_id == 15372)) {
            return true;
        }

        for (int i = 0; i < HEDGING_TYPES.length; i++) {
            if ((HEDGING_TYPES[i] == obj_id) || (HEDGING_TYPES[i] + 2 == obj_id) || (HEDGING_TYPES[i] + 1 == obj_id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isFence
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj_id
     *
     * @return
     */
    public static final boolean isFence(int obj_id) {
        if (obj_id == 15369) {
            return true;
        }

        for (int i = 0; i < WALL_TYPES.length; i++) {
            if (WALL_TYPES[i] == obj_id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method npcFromObj
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param obj
     *
     * @return
     */
    public static int npcFromObj(int obj) {
        switch (obj) {

        // Skeleton
        case 13366 :
            return 3581;

        // Guard Dog
        case 13367 :
            return 3582;

        // Hobgoblin
        case 13368 :
            return 3583;

        // Baby red dragon
        case 13372 :
            return 3588;

        // Huge spider
        case 13370 :
            return 3585;

        // Troll guard
        case 13369 :
            return 3584;

        // Hellhound
        case 13371 :
            return 3586;

            case 13378:
                return 82;
            case 13374:
                return 1154;

        }

        return -1;
    }

    /**
     * Method getAnim
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static int getAnim(int id) {
        for (int i = 0; i < CHAIR_IDS.length; i++) {
            if (CHAIR_IDS[i] == id) {
                return CHAIR_ANIMS[i];
            }
        }

        throw new IndexOutOfBoundsException("not found: " + id);
    }

    /**
     * Method isChair
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isChair(int id) {
        for (int i = 0; i < CHAIR_IDS.length; i++) {
            if (CHAIR_IDS[i] == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isRug
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isRug(int id) {
        for (int i = 0; i < RUG_MAPPINGS_CENTER.length; i++) {
            if (RUG_MAPPINGS_CENTER[i] == id) {
                return true;
            }
        }

        for (int i = 0; i < RUG_MAPPINGS_CORNER.length; i++) {
            if (RUG_MAPPINGS_CORNER[i] == id) {
                return true;
            }
        }

        for (int i = 0; i < RUG_MAPPINGS_OUTSKIRTS.length; i++) {
            if (RUG_MAPPINGS_OUTSKIRTS[i] == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getRugType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static final int getRugType(int id) {
        switch (id) {
        case 15414 :
        case 15378 :
        case 15273 :
        case 15388 :
        case 15265 :
            return 0;

        case 15274 :
        case 15415 :
        case 15379 :
        case 15389 :
        case 15266 :
            return 2;    // corner

        case 15387 :
        case 15377 :
        case 15264 :
        case 15413 :     // center
            return 1;
        }

        return -1;
    }

    /**
     * Method getRegionX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRegionX() {
        return regionX;
    }

    /**
     * Method getRegionY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRegionY() {
        return regionY;
    }

    /**
     * Method getId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Method getObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public GameObject getObject() {
        return object;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }
}
