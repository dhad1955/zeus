package net.tazogaming.hydra.game.skill.construction;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Tile;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/01/14
 * Time: 23:49
 */
public class ConstructionDoor {

//  Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
//  Jad home page: http://www.kpdus.com/jad.html
//  Decompiler options: packimports(3)
//  Source File Name:   Door.java
    private int        cachedRotation   = 0;
    private byte       customOpenFace   = -1;
    private byte       customClosedFace = -1;
    private int        x                = 0,
                       y                = 0;
    private GameObject door;
    private int        originalX, originalY;
    private int        transformX;
    private int        transformY;
    private int        oldRotation;
    private int        oldX;
    private int        oldY;
    private int        currentX;
    private int        currentY;
    private int        height;
    private int        currentOffset;
    private boolean    isBusy;
    private int        openId;
    private int        closedId;
    private int        currentRotation;
    private boolean    isOpen;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param x
     * @param y
     * @param dir
     * @param isOpen
     * @param door
     */
    public ConstructionDoor(int id, int x, int y, int dir, boolean isOpen, GameObject door) {
        transformX    = 0;
        transformY    = 0;
        oldX          = -1;
        oldY          = -1;
        currentOffset = 0;
        isBusy        = false;

        if (isOpen) {
            openId   = id;
            closedId = findNextDoor(id);

            if (closedId == 0) {
                closedId = openId;
            }
        } else {
            closedId = id;
            openId   = findNextDoor(id);

            if (openId == 0) {
                openId = closedId;
            }
        }

        currentRotation = dir;
        this.isOpen     = isOpen;
        originalX       = x;
        originalY       = y;
        cachedRotation  = dir;
        this.x          = x;
        this.y          = y;
        this.door       = door;
    }

    /**
     * Method render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param room
     */
    public void render(Player player, RoomNode room) {}

    /**
     * Method getObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public GameObject getObject() {
        return door;
    }

    /**
     * Method getX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getX() {
        return x;
    }

    /**
     * Method getY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getY() {
        return y;
    }

    /**
     * Method setCustomFace
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param face
     * @param face2
     */
    public void setCustomFace(int face, int face2) {
        this.customClosedFace = (byte) face;
        this.customOpenFace   = (byte) face2;
    }

    /**
     * Method isOpen
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isOpen() {
        return isOpen;
    }

    /**
     * Method getCurrentId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentId() {
        if (isOpen) {
            return openId;
        } else {
            return closedId;
        }
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {
        currentOffset++;
    }

    /**
     * Method getCurrentOffset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentOffset() {
        return currentOffset;
    }

    /**
     * Method open
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean open() {
        if (isOpen) {
            return false;
        }

        oldRotation     = currentRotation;
        currentRotation = getNextFace();

        int transformX = 0;
        int transformY = 0;

        if ((oldRotation == 0) && (cachedRotation == 0)) {
            transformX--;
        } else if ((oldRotation == 1) && (cachedRotation == 1)) {
            transformY++;
        } else if ((oldRotation == 3) && (cachedRotation == 3)) {
            transformY--;
        } else if ((oldRotation == 2) && (cachedRotation == 2)) {
            transformX++;
        }

        currentRotation %= 4;
        oldX            = getX();
        oldY            = getY();

        int currentX = getX();
        int currentY = getY();

        if ((currentX == originalX) && (currentY == originalY)) {
            currentX += transformX;
            currentY += transformY;
        } else {
            currentX = originalX;
            currentY = originalY;
        }

        next();
        isOpen = true;

        // CacheObjectDefinition d = CacheObjectDefinition.getQuest(closedId);
        // ClippingDecoder.removeClippingForVariableObject(oldX, oldY, height, 0, oldRotation, d.aBoolean757, d.solid());
        // ClippingDecoder.addWall(currentX, currentY, height, 0, currentRotation, d.aBoolean757, d.solid());
        setLocation(currentX, currentY);

        return true;
    }

    /**
     * Method setLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     */
    public void setLocation(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Method modDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param face1
     * @param face2
     */
    public static void modDoor(int x, int y, int face1, int face2) {
        Tile t = World.getWorld().getTile(x, y, 0);

        if (t.getDoor() != null) {
            t.getDoor().setCustomFace(face1, face2);
        }

        // /      else
//      throw new NullPointerException();
    }

    private int getNextFace() {
        int f = cachedRotation;

        if (true) {
            if (!isOpen()) {
                if (customOpenFace != -1) {
                    return customOpenFace;
                }

                if ((cachedRotation == 0) && (currentRotation == 0)) {
                    f = 1;
                } else if ((cachedRotation == 1) && (currentRotation == 1)) {
                    f = 2;
                } else if ((cachedRotation == 2) && (currentRotation == 2)) {
                    f = 3;
                } else if ((cachedRotation == 3) && (currentRotation == 3)) {
                    f = 0;
                } else if (cachedRotation != currentRotation) {
                    f = cachedRotation;
                }
            } else if (isOpen()) {
                if (customClosedFace != -1) {
                    return customClosedFace;
                }

                if ((cachedRotation == 0) && (currentRotation == 0)) {
                    f = 3;
                } else if ((cachedRotation == 1) && (currentRotation == 1)) {
                    f = 0;
                } else if ((cachedRotation == 2) && (currentRotation == 2)) {
                    f = 1;
                } else if ((cachedRotation == 3) && (currentRotation == 3)) {
                    f = 2;
                } else if (cachedRotation != currentRotation) {
                    f = cachedRotation;
                }
            }
        } else if (false) {
            if (!isOpen()) {
                if ((cachedRotation == 0) && (currentRotation == 0)) {
                    f = 3;
                } else if ((cachedRotation == 1) && (currentRotation == 1)) {
                    f = 2;
                } else if ((cachedRotation == 2) && (currentRotation == 2)) {
                    f = 1;
                } else if ((cachedRotation == 3) && (currentRotation == 3)) {
                    f = 0;
                } else if (cachedRotation != currentRotation) {
                    f = cachedRotation;
                }
            } else if (isOpen()) {
                if ((cachedRotation == 0) && (currentRotation == 0)) {
                    f = 3;
                } else if ((cachedRotation == 1) && (currentRotation == 1)) {
                    f = 0;
                } else if ((cachedRotation == 2) && (currentRotation == 2)) {
                    f = 1;
                } else if ((cachedRotation == 3) && (currentRotation == 3)) {
                    f = 2;
                } else if (cachedRotation != currentRotation) {
                    f = cachedRotation;
                }
            }
        }

        return f;
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean close() {
        if (!isOpen) {
            return false;
        }

        oldRotation     = currentRotation;
        currentRotation = getNextFace();

        int transformX = 0;
        int transformY = 0;

        if ((oldRotation == 0) && (cachedRotation == 0)) {
            transformY++;
        } else if ((oldRotation == 1) && (cachedRotation == 1)) {
            transformX++;
        } else if ((oldRotation == 2) && (cachedRotation == 2)) {
            transformY--;
        } else if ((oldRotation == 3) && (cachedRotation == 3)) {
            transformX--;
        }

        oldX = getX();
        oldY = getY();

        int currentX = getX();
        int currentY = getY();

        if ((currentX == originalX) && (currentY == originalY)) {
            currentX += transformX;
            currentY += transformY;
        } else {
            currentX = originalX;
            currentY = originalY;
        }

        next();
        isOpen = false;

        CacheObjectDefinition d = CacheObjectDefinition.forId(closedId);

        ClippingDecoder.removeClippingForVariableObject(oldX, oldY, height, 0, oldRotation, d.isProjectileClipped(),
                !d.isClippingFlag());
        ClippingDecoder.addWall(currentX, currentY, height, 0, currentRotation, d.isProjectileClipped(),
                                !d.isClippingFlag());

        // setLocation(Point.location(currentX, currentY, getHeight()));
        setLocation(x, y);

        return true;
    }

    private static int findNextDoor(int id) {
        CacheObjectDefinition doorDef = CacheObjectDefinition.forId(id);
        String                option  = doorDef.options[0];

        if (option.equalsIgnoreCase("open")) {
            for (int i = 0; i < 4; i++) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("close")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }

            for (int i = 0; i > -4; i--) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("close")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }
        } else if (option.equalsIgnoreCase("close")) {
            for (int i = 0; i > -4; i--) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("open")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }

            for (int i = 0; i < 4; i++) {
                CacheObjectDefinition def = CacheObjectDefinition.forId(id + i);

                if ((def.hasActions()) && (def.options[0] != null) && def.options[0].equalsIgnoreCase("open")
                        && def.name.equalsIgnoreCase(doorDef.name)) {
                    return id + i;
                }
            }
        }

        return 0;
    }
}
