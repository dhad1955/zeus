package net.tazogaming.hydra.game.skill.construction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/01/14
 * Time: 18:32
 */
public class RoomConfig {
    public static final int
        RING_TYPE_BOXING               = 0,
        RING_TYPE_FENCING              = 1,
        RING_TYPE_COMBAT               = 2;
    public static final int
        STAIRS_UPSTAIRS                = 0,
        STAIRS_DOWNSTAIRS              = 1;
    private int   ringType             = -1;
    private int   rotation             = 0;
    private int   room_id              = 0;
    private int   stairs_id            = -1;
    private int[] object_settings      = new int[80];
    private int[] object_change_ids    = new int[80];
    private int[] object_change_timers = new int[80];
    private int   rug_id               = -1;
    private int   floor_type           = -1;
    private int   prison_type          = -1;
    private int   hedge_type           = -1;
    private int   fence_type           = -1;

    /**
     * Constructs ...
     *
     *
     * @param room_id
     * @param rotation
     */
    public RoomConfig(int room_id, int rotation) {
        this.room_id  = room_id;
        this.rotation = rotation;
    }

    /**
     * Method getHedge
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHedge() {
        return hedge_type;
    }

    /**
     * Method getFence
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFence() {
        return fence_type;
    }

    /**
     * Method setHedge
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param h
     */
    public void setHedge(int h) {
        this.hedge_type = h;
    }

    /**
     * Method setFence
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param f
     */
    public void setFence(int f) {
        this.fence_type = f;
    }

    /**
     * Method getPrisonType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPrisonType() {
        return prison_type;
    }

    /**
     * Method getRingType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRingType() {
        return ringType;
    }

    /**
     * Method changeJail
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param j
     */
    public void changeJail(int j) {
        this.prison_type = j;
    }

    /**
     * Method changeFloorType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param j
     */
    public void changeFloorType(int j) {
        this.floor_type = j;
    }

    /**
     * Method setRingType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setRingType(int type) {
        ringType = type;
    }

    /**
     * Method setRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rot
     */
    public void setRotation(int rot) {
        this.rotation = rot;
    }

    /**
     * Method changeId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void changeId(int id) {
        this.room_id = id;
    }

    /**
     * Method getCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getCount(int id) {
        int c = 0;

        for (int i = 0; i < object_settings.length; i++) {
            if (object_settings[i] == id) {
                c++;
            }
        }

        return c;
    }

    /**
     * Method hasBuilt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean hasBuilt(int id) {
        return getBuilt(id) != -1;
    }

    /**
     * Method getRoom
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRoom() {
        return room_id;
    }

    /**
     * Method getRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRotation() {
        return rotation;
    }

    /**
     * Method getRoomNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RoomNode getRoomNode() {
        return RoomNode.getNode(room_id, rotation);
    }

    /**
     * Method setSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param settings
     */
    public void setSettings(int[] settings) {
        this.object_settings = settings;
    }

    /**
     * Method is_door_open
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean is_door_open(int id) {
        return object_settings[id] == 1;
    }

    /**
     * Method set_door_open
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void set_door_open(int id) {
        this.object_settings[id] = 1;
    }

    /**
     * Method set_door_closed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void set_door_closed(int id) {
        this.object_settings[id] = 0;
    }

    /**
     * Method setRugID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rug_id
     */
    public void setRugID(int rug_id) {
        this.rug_id = rug_id;
    }

    /**
     * Method getRugID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRugID() {
        return rug_id;
    }

    /**
     * Method set_node
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param object_id
     */
    public void set_node(int id, int object_id) {
        object_settings[id] = object_id;
    }

    /**
     * Method setStairs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void setStairs(int id) {
        stairs_id = id;
    }

    /**
     * Method getStairsType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStairsType() {
        return stairs_id;
    }

    /**
     * Method containsNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean containsNode(int id) {
        for (int i = 0; i < object_settings.length; i++) {
            if ((object_settings[i] == id)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getFloorType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFloorType() {
        return floor_type;
    }

    /**
     * Method getCurrent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param node
     *
     * @return
     */
    public int getCurrent(int node) {
        if (object_change_timers[node] > 0) {
            return object_change_ids[node];
        }

        return getBuilt(node);
    }

    /**
     * Method changeObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param nodeId
     * @param newId
     * @param ticks
     */
    public void changeObject(int nodeId, int newId, int ticks) {
        object_change_timers[nodeId] = ticks;
        object_change_ids[nodeId]    = newId;
    }

    /**
     * Method getBuilt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getBuilt(int id) {
        if (object_settings[id] == 0) {
            return -1;
        }

        return object_settings[id];
    }
}
