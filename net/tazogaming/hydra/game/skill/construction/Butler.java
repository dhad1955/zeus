package net.tazogaming.hydra.game.skill.construction;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.movement.RoamingMovement;
import net.tazogaming.hydra.entity3d.npc.ai.movement.SummoningMovement;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/02/14
 * Time: 03:07
 */
public class Butler {
    private static final int
        MODE_FOLLOW                         = 0,
        MODE_SERVE_TEA                      = 1,
        MODE_MEET_AND_GREET                 = 2,
        MODE_ROAM                           = 3;
    private ArrayList<Player> playersServed = new ArrayList<Player>();
    private int               mode;
    private NPC               npc;
    private House             house;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param house
     */
    public Butler(int type, House house) {
        RoomConfig conf;

        this.house = house;

        for (int x = 0; x < 13; x++) {
            for (int y = 0; y < 13; y++) {
                conf = house.getLayout()[1][x][y];

                if (conf != null) {
                    Point p = house.translate(x, y, 1, 0, 0);

                    npc = new NPC(type, p, 40);
                    npc.setCurrentInstance(house);
                    World.getWorld().registerNPC(npc);
                    follow(house.getPlayer());

                    return;
                }
            }
        }

        follow(house.getPlayer());
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unlink() {
        npc.unlink();
    }

    /**
     * Method getButlerMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getButlerMode() {
        return mode;
    }

    /**
     * Method changeMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param mode
     */
    public void changeMode(int mode) {
        playersServed.clear();
        this.mode = mode;

        switch (mode) {
        case MODE_FOLLOW :
            follow(house.getPlayer());

            break;

        case MODE_ROAM :
            npc.getFocus().unFocus();
            npc.setMovement(new RoamingMovement(npc));
            npc.getSpawnData().setRoamRange(40);

            break;

        case MODE_SERVE_TEA :
            break;
        }
    }

    /**
     * Method goTo
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param blockname
     */
    public void goTo(Player player, String blockname) {
        if (player.getCurrentHouse() != null) {
            npc.teleport(player.getX(), player.getY(), player.getHeight());
            forceChat(player, blockname);
        }
    }

    /**
     * Method forceChat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param blockname
     */
    public void forceChat(Player player, String blockname) {
        npc.setCurrentController(player);

        Scope scr = new Scope();

        scr.setController(player);
        scr.setInteractingNpc(npc);
        scr.setInteractingNPC(npc, 0);
        scr.setRunning(World.getWorld().getScriptManager().get_block(blockname));
        player.getNewScriptQueue().add(scr);
        player.getFocus().focus(npc);
        npc.getFocus().focus(player);
    }

    /**
     * Method follow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void follow(Player player) {
        npc.setMovement(new SummoningMovement(npc, player));
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {
        switch (mode) {
        case MODE_SERVE_TEA :
            if (npc.isTalking()) {
                return;
            }

            ArrayList<Player> plrs = house.getPlayers();

            if (plrs.size() == playersServed.size()) {
                playersServed.clear();
                changeMode(MODE_ROAM);

                return;
            }

            for (Player player : plrs) {
                if (!playersServed.contains(player)) {
                    playersServed.add(player);

                    if (house.isInRing(player) || (player.getHeight() > 1)) {
                        continue;
                    }

                    goTo(player, "care_for_tea_sir");

                    return;
                }
            }

            break;
        }
    }
}
