package net.tazogaming.hydra.game.skill.construction;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.map.*;
import net.tazogaming.hydra.game.skill.construction.obj.ObjectNode;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/01/14
 * Time: 19:54
 */
public class RoomNode {
    public static final RoomNode[][] ROOM_NODE_MAP        = new RoomNode[30][4];
    public static final int          THEME_WALL_IDS[]     = {
        13098, 1902, 1415, 13111, 13011, 13116
    };
    public static final int          DOOR_HOTSPOT_IDS[][] = {
        { 15313, 15314 }, { 15307, 15308 }, { 15309, 15310 }, { 15311, 15312 }, { 15305, 15306 }, { 15315, 15316 }
    };
    public static final int          DOOR_CLOSED_IDS[][]  = {
        { 13100, 13101 }, { 13015, 13016 }, { 13006, 13007 }, { 13109, 13107 }, { 13118, 13119 }, { 13118, 13119 }
    };
    public static final int          WINDOW_IDS[]         = {
        13099, 1902, 13005, 13112, 10816, 13117, 10816
    };
    public static final int
        TYPE_ROOM                                         = 0,
        TYPE_GARDEN                                       = 1,
        TYPE_DUNGEON                                      = 2;
    public static BufferedWriter writer;
    private List<GameObject>[][] objects         = new ArrayList[8][8];
    private int                  rotation        = 0;
    private int                  build_cost      = 0;
    int                          cur_id          = 0;
    private ObjectNode[]         localObjectList = new ObjectNode[160];
    private Landscape            landscape;
    private int                  type;
    private int                  paletteX;
    private int                  paletteY;
    private int                  experience;
    private int                  height;
    private int                  level_requirement;
    private String               label;
    private GameObject[][][]     objectMap;

    /**
     * Constructs ...
     *
     *
     * @param px
     * @param py
     * @param lvl_req
     * @param rotation
     * @param build_cost
     * @param name
     * @param objs
     */
    public RoomNode(int px, int py, int lvl_req, int rotation, int build_cost, String name, ObjectNode[] objs) {
        this.label             = name;
        this.paletteX          = px;
        this.paletteY          = py;
        this.build_cost        = build_cost;
        this.rotation          = rotation;
        this.level_requirement = lvl_req;
        this.localObjectList   = Arrays.copyOf(objs, objs.length);
        this.landscape         = MapFetcher.get(paletteX / 64, paletteY / 64, rotation);
        rotateRegion(rotation);

//      objectMap = getObjectMap();
    }

    /**
     * Method getAbs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     * @param searchType
     *
     * @return
     */
    public ObjectNode getAbs(int x, int y, int searchType) {
        for (ObjectNode a : localObjectList) {
            if (a != null) {
                if ((a.getRegionX() == x) && (a.getRegionY() == y) && (a.getObject().getType() == searchType)) {
                    return a;
                }
            } else {
                return null;
            }
        }

        return null;
    }

    /**
     * Method getObjectNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param theme
     * @param config
     *
     * @return
     */
    public ObjectNode getObjectNode(int id, int x, int y, int theme, RoomConfig config) {
        boolean hotSpot = isHotspot(id, theme);

        for (ObjectNode a : localObjectList) {
            if ((id == 15292) || (id == 15295) || (id == 15291)) {
                if (a.getObject().getId() == 15277) {
                    return a;
                }
            }

            if (a != null) {
                if ((a.getRegionX() == x) && (a.getRegionY() == y)) {
                    if ((id == 13129) || (id == 13137) || (id == 13133)) {
                        return a;
                    }

                    if (ObjectNode.isRug(id) && (ObjectNode.getRugType(a.getObject().getId()) != -1)) {
                        return a;
                    }

                    if (isFloorType(a.getObject().getId()) && isFloorType(id)) {
                        return a;
                    }

                    if ((config.getPrisonType() >= 0)
                            && (id == ObjectNode.PRISON_WALL_TYPES[config.getPrisonType()] + 1)
                            && (a.getObject().getType() != 22)) {
                        return a;
                    }

                    if (ObjectNode.isFence(a.getObject().getId()) && ObjectNode.isFence(id)) {
                        return a;
                    }

                    if (ObjectNode.isHedge(a.getObject().getId()) && ObjectNode.isHedge(id)) {
                        return a;
                    }

                    if (isPrison(a.getObject().getId()) && isPrison(id)) {
                        return a;
                    }

                    if (isHotspot(a.getObject().getId()) && isHotspot(id, theme)) {
                        return a;
                    }

                    if (a.getObject().getId() == id) {
                        return a;
                    }

                    if (config.getBuilt(a.getId()) == id) {
                        return a;
                    }

                    if ((config.getStairsType() == id) && (a.getType() == ObjectNode.TYPE_STAIRS)) {
                        return a;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Method getNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param rotation
     *
     * @return
     */
    public static final RoomNode getNode(int id, int rotation) {
        return ROOM_NODE_MAP[id][rotation];
    }

    /**
     * Method generate_room_config
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param rot
     *
     * @return
     */
    public RoomConfig generate_room_config(int id, int rot) {
        RoomConfig conf = new RoomConfig(id, rot);

        conf.setSettings(new int[localObjectList.length]);

        return conf;
    }

    /**
     * Method load_room_config
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void load_room_config() {
        File file = new File("config/iskill/construction/room_config.cfg");

        if (!file.exists()) {
            Logger.err("Fatal error, unable to load room config for construction skill FILE NOT FOUND");
            System.exit(-1);
        }

        try {
            writer = new BufferedWriter(new FileWriter("config/iskill/construction/new_config.cfg", true));

            BufferedReader reader   = new BufferedReader(new FileReader(file));
            String         cur_line = null;
            boolean        parsing  = false;
            String[]       tokens;
            String         cur_name         = "";
            int            cur_id           = -1;
            int            tower_location_x = 0;
            int            cur_room_type    = TYPE_ROOM;
            int            tower_location_y = 0;
            int            building_cost    = 0;
            ObjectNode[]   obj_             = new ObjectNode[80];
            int            level_required   = 0;

            while ((cur_line = reader.readLine()) != null) {
                if (cur_line.startsWith("##") || cur_line.startsWith("//") || (cur_line.length() == 0)) {
                    continue;
                }

                if (!parsing && cur_line.startsWith("room")) {
                    obj_          = new ObjectNode[70];
                    tokens        = Text.tokenize(cur_line, ' ');
                    cur_id        = Integer.parseInt(tokens[1]);
                    cur_room_type = TYPE_ROOM;
                    cur_name      = tokens[2];
                    parsing       = true;

                    if (cur_name.toLowerCase().contains("garden")) {
                        cur_room_type = TYPE_GARDEN;
                    } else if ((cur_id >= 15) && (cur_id <= 19)) {
                        cur_room_type = TYPE_DUNGEON;
                    }
                } else if (parsing) {
                    tokens = cur_line.split(" ");

                    if (tokens[0].equalsIgnoreCase("tower-location-x")) {
                        tower_location_x = Integer.parseInt(tokens[2]);
                    } else if (tokens[0].equalsIgnoreCase("tower-location-y")) {
                        tower_location_y = Integer.parseInt(tokens[2]);
                    } else if (tokens[0].equalsIgnoreCase("building-cost")) {
                        building_cost = Integer.parseInt(tokens[2]);
                    } else if (tokens[0].equalsIgnoreCase("level_required")) {
                        level_required = Integer.parseInt(tokens[2]);
                    } else if (tokens[0].equalsIgnoreCase("}")) {
                        parsing = false;

                        for (int i = 0; i < 4; i++) {
                            ROOM_NODE_MAP[cur_id][i] = new RoomNode(tower_location_x, tower_location_y, level_required,
                                    i, building_cost, cur_name, obj_);
                            ROOM_NODE_MAP[cur_id][i].setType(cur_room_type);
                        }
                    } else if (tokens[0].equalsIgnoreCase("node")) {
                        GameObject obj = new GameObject();
                        int        id  = Text.parseInt(tokens[2]);

                        obj.setIdentifier(Text.parseInt(tokens[3]));

                        int obj_x = Text.parseInt(tokens[4]);
                        int obj_y = Text.parseInt(tokens[5]);

                        obj.setCurrentRotation(Text.parseInt(tokens[6]));
                        obj.setObjectType(Text.parseInt(tokens[7]));

                        int type = ObjectNode.TYPE_NORMAL;

                        if (tokens[8].equalsIgnoreCase("window")) {
                            type = ObjectNode.TYPE_WINDOW;
                        } else if (tokens[8].equalsIgnoreCase("door")) {
                            type = ObjectNode.TYPE_DOOR;
                        } else if (tokens[8].equalsIgnoreCase("stairs")) {
                            type = ObjectNode.TYPE_STAIRS;
                        } else if (tokens[8].equalsIgnoreCase("npc")) {
                            type = ObjectNode.TYPE_MONSTER;
                        } else if (tokens[8].equalsIgnoreCase("trap")) {
                            type = ObjectNode.TYPE_TRAP;
                        }

                        obj_[id] = new ObjectNode(id, obj, obj_x, obj_y, type);
                    }
                }
            }

            reader.close();
            writer.close();
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method dump
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public static void dump(int id) {
        RoomNode room = ROOM_NODE_MAP[id][0];

        if (room == null) {
            return;
        }

        try {
            writer.newLine();
            writer.write("room " + id + " \"" + room.getLabel() + "\" {");
            writer.newLine();
            writer.write("tower-location-x = " + room.getPaletteX());
            writer.newLine();
            writer.write("tower-location-y = " + room.getPaletteY());
            writer.newLine();
            writer.write("building-cost = " + room.build_cost);
            writer.newLine();
            writer.write("level_required = " + room.getLevel_requirement());
            writer.newLine();

            for (int i = 0; i < room.localObjectList.length; i++) {
                if (room.localObjectList[i] != null) {
                    String type = "normal";

                    if (CacheObjectDefinition.forId(room.localObjectList[i].getObject().getId()).name.equalsIgnoreCase(
                            "Door hotspot")) {
                        type = "door";
                    } else if (room.localObjectList[i].getObject().getId() == 13830) {
                        type = "window";
                    }

                    writer.write("node = " + i + " " + room.localObjectList[i].getObject().getId() + " "
                                 + room.localObjectList[i].getRegionX() + " " + room.localObjectList[i].getRegionY()
                                 + " " + room.localObjectList[i].getObject().getRotation() + " "
                                 + room.localObjectList[i].getObject().getType() + " " + type);
                    writer.newLine();
                }
            }

            writer.newLine();
            writer.write("}");
        } catch (Exception ee) {}
    }

    /**
     * Method getNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public ObjectNode getNode(int id) {
        return localObjectList[id];
    }

    /**
     * Method getNodeList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ObjectNode[] getNodeList() {
        return localObjectList;
    }

    private int getDoorIndex(int id) {
        for (int k = 0; k < DOOR_HOTSPOT_IDS.length; k++) {
            for (int x = 0; x < DOOR_HOTSPOT_IDS[k].length; x++) {
                if (DOOR_HOTSPOT_IDS[k][x] == id) {
                    return x;
                }
            }
        }

        return -1;
    }

    /**
     * Method clearDoors
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param regionX
     * @param regionY
     */
    public void clearDoors(Player player, int regionX, int regionY) {
        int offsetX = getDrawOffsetX(player, regionX);
        int offsetY = getDrawOffsetY(player, regionY);

        // / clear previous doors
        for (ObjectNode n : localObjectList) {
            if (n == null) {
                return;
            }

            if (n.getType() == ObjectNode.TYPE_DOOR) {
                player.getActionSender().deleteDoor(offsetX + n.getRegionX(), offsetY + n.getRegionY(),
                        n.getObject().getRotation(), player.getHeight());
            }
        }
    }

    /**
     * Method renderDoors
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param regionX
     * @param regionY
     */
    public void renderDoors(Player player, int regionX, int regionY) {
        int offsetX = getDrawOffsetX(player, regionX);
        int offsetY = getDrawOffsetY(player, regionY);

        // / clear previous doors
        for (ObjectNode n : localObjectList) {
            if (n == null) {
                return;
            }

            if (n.getType() == ObjectNode.TYPE_DOOR) {
                player.getActionSender().sendPlainObject(n.getObject().getId(), offsetX + n.getRegionX(),
                        offsetY + n.getRegionY(), n.getObject().getType(), n.getObject().getRotation(),
                        player.getHeight());
            }
        }
    }

    /**
     * Method renderDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param render_x
     * @param render_y
     * @param object_id
     * @param rotation
     * @param theme
     * @param plane
     * @param config
     * @param rendered
     * @param rx
     * @param ry
     */
    public void renderDoor(Player pla, int render_x, int render_y, int object_id, int rotation, int theme, int plane,
                           RoomConfig config, boolean[][][] rendered, int rx, int ry) {
        int index = getDoorIndex(object_id);

        if ((index == -1) || (plane != 1) || (DOOR_CLOSED_IDS.length <= theme)) {
            pla.getActionSender().sendPlainObject(THEME_WALL_IDS[theme], render_x, render_y, 0, rotation, plane);

            if (plane == 0) {}

            return;
        }

        pla.getActionSender().sendPlainObject(DOOR_CLOSED_IDS[theme][index], render_x, render_y, 0, rotation, plane);

        if ((ry == 0) || (rx == 0)) {
            return;
        }

        int[] transformX = { -1, 0, 1, 0 };
        int[] transformY = { 0, 1, 0, -1 };

        // render open prototype
        pla.getActionSender().deleteDoor(render_x, render_y, rotation, plane);
        pla.getActionSender().sendPlainObject(DOOR_CLOSED_IDS[theme][index], render_x + transformX[rotation],
                render_y + transformY[rotation], 0, ((index == 1)
                ? (rotation - 1) & 3
                : rotation + 1) & 3, plane);
        rendered[plane][rx + transformX[rotation]][ry + transformY[rotation]] = true;
    }

    /**
     * Method getRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRotation() {
        return rotation;
    }

    /**
     * Method isFloorType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isFloorType(int id) {
        int[] types = {
            13334, 13333, 13337, 13335, 13332, 13334, 13331, 15349, 15348, 15350, 15347
        };

        for (int i = 0; i < types.length; i++) {
            if (types[i] == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isPrison
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isPrison(int id) {
        if ((id == 15353) || (id == 15352)) {
            return true;
        }

        for (int i = 0; i < ObjectNode.PRISON_WALL_TYPES.length; i++) {
            if (ObjectNode.PRISON_WALL_TYPES[i] == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isRing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isRing(int id) {
        int[] rings = {
            15277, 15280, 15282, 15287, 15281, 15278, 15286, 25279, 15279
        };

        for (Integer i : rings) {
            if (id == i) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method isHotspot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isHotspot(int id) {
        for (int i = 0; i < DOOR_HOTSPOT_IDS.length; i++) {
            if (isHotspot(id, i)) {
                return true;
            }
        }

        return false;
    }

    private static boolean isHotspot(int id, int k) {
        for (int y = 0; y < DOOR_HOTSPOT_IDS[k].length; y++) {
            if (DOOR_HOTSPOT_IDS[k][y] == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getDrawOffsetX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param regionX
     *
     * @return
     */
    public int getDrawOffsetX(Player player, int regionX) {
        int startDrawX    = (player.getKnownRegion()[0] - 5);
        int plrCurRegionX = (player.getKnownRegion()[1] - startDrawX);

        return ((startDrawX + (regionX) - 1) + 6) * 8;
    }

    /**
     * Method getDrawOffsetY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param regionY
     *
     * @return
     */
    public int getDrawOffsetY(Player player, int regionY) {
        int startDrawY    = (player.getKnownRegion()[1] - 5);
        int plrCurRegionY = (player.getLocation().getRegionY() - startDrawY);

        return ((startDrawY + (regionY) - 1) + 6) * 8;
    }

    /**
     * Method rotateRegion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rotation
     */
    public void rotateRegion(int rotation) {
        if (rotation == 0) {
            return;
        }

        int                   object_width  = -1;
        int                   object_length = -1;
        int                   tile_x        = -1;
        int                   tile_y        = -1;
        CacheObjectDefinition def           = null;

        for (int i = 0; i < localObjectList.length; i++) {
            ObjectNode node = localObjectList[i];

            if (node != null) {
                def = CacheObjectDefinition.forId(node.getObject().getId());

                switch (node.getObject().getRotation()) {
                case 3 :
                case 1 :
                    object_width  = def.sizeY;
                    object_length = def.sizeX;

                    break;

                default :
                    object_width  = def.sizeX;
                    object_length = def.sizeY;

                    break;
                }

                tile_x = Landscape.rotate_object_block_x(node.getRegionX(), node.getRegionY(), object_width,
                        object_length, rotation);
                tile_y = Landscape.rotate_object_block_y(node.getRegionX(), node.getRegionY(), object_width,
                        object_length, rotation);

                GameObject new_obj = new GameObject();

                new_obj.setIdentifier(node.getObject().getId());
                new_obj.setCurrentRotation(node.getObject().getRotation() + rotation & 3);
                new_obj.setObjectType(node.getObject().getType());
                localObjectList[i] = new ObjectNode(i, new_obj, tile_x, tile_y, node.getType());
            }
        }
    }

    /**
     * Method buildClippingData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param map
     * @param offsetX
     * @param offsetY
     * @param height
     * @param surrounding
     * @param config
     */
    public void buildClippingData(DynamicClippingMap map, int offsetX, int offsetY, int height, boolean[] surrounding,
                                  RoomConfig config) {
        objectMap = getObjectMap();

        ObjectNode            current_node;
        CacheObjectDefinition def;

        for (int node_index = 0; node_index < localObjectList.length; node_index++) {
            if (localObjectList[node_index] == null) {
                break;
            }

            current_node = localObjectList[node_index];
            def          = CacheObjectDefinition.forId(current_node.getObject().getId());

            switch (current_node.getType()) {
            case ObjectNode.TYPE_WINDOW :
                map.addClippingForVariableObject(offsetX + current_node.getRegionX(),
                                                 offsetY + current_node.getRegionY(), height,
                                                 current_node.getObject().getType(),
                                                 current_node.getObject().getRotation(), def.isProjectileClipped(),
                                                 !def.isClippingFlag());

                break;

            case ObjectNode.TYPE_STAIRS :
                map.addClippingForSolidObject(offsetX + current_node.getRegionX(), offsetY + current_node.getRegionY(),
                                              height, def.sizeX, def.sizeY, def.isProjectileClipped(), true);

                break;

            case ObjectNode.TYPE_NORMAL :
                if ((config.getBuilt(node_index) > 0) && (def.getClippingType() != 0)) {
                    map.addClippingForSolidObject(offsetX + current_node.getRegionX(),
                                                  offsetY + current_node.getRegionY(), height, def.sizeX, def.sizeY,
                                                  def.isProjectileClipped(), def.isClippingFlag());
                } else if (isRing(current_node.getObject().getId()) && (config.getRingType() >= 0)) {
                    map.addClippingForVariableObject(offsetX + current_node.getRegionX(),
                                                     offsetY + current_node.getRegionY(), height,
                                                     current_node.getObject().getType(),
                                                     current_node.getObject().getRotation(), true, true);
                }

                break;

            case ObjectNode.TYPE_DOOR :
                boolean render = !surrounding[current_node.getObject().getRotation()];

                if ((type == TYPE_GARDEN) || (type == TYPE_DUNGEON)) {
                    render = false;    // dont draw doors on gardens
                }

                if (render) {
                    int[] transformX = { -1, 0, 1, 0 };
                    int[] transformY = { 0, 1, 0, -1 };

                    if (true || (config.getBuilt(node_index) > 0)) {
                        int index = getDoorIndex(current_node.getObject().getId());

                        // Logger.log(current_node.getObject().getRotation()+" "+index+" rendered: "+(offsetX + current_node.getRegionX() + transformX[current_node.getObject().getRotation()])+" "+(offsetY+current_node.getRegionY()+transformY[current_node.getObject().getRotation()]));
                        map.addClippingForVariableObject(
                            offsetX + current_node.getRegionX() + transformX[current_node.getObject().getRotation()],
                            offsetY + current_node.getRegionY() + transformY[current_node.getObject().getRotation()],
                            height, current_node.getObject().getType(), (((index == 1)
                                ? (current_node.getObject().getRotation() - 1) & 3
                                : current_node.getObject().getRotation() + 1) & 3), def.isProjectileClipped(),
                                !def.isClippingFlag());
                    } else {
                        map.addClippingForVariableObject(offsetX + current_node.getRegionX(),
                                                         offsetY + current_node.getRegionY(), height,
                                                         current_node.getObject().getType(),
                                                         current_node.getObject().getRotation(),
                                                         def.isProjectileClipped(), !def.isClippingFlag());
                    }
                }

                break;
            }
        }

        for (int x = 0; x < 8; x++) {
            for (int y = 0; y < 8; y++) {
                for (int objInd = 0; objInd < 3; objInd++) {
                    if (objectMap[x][y][objInd] != null) {
                        map.addObject(objectMap[x][y][objInd].getId(), offsetX + x, offsetY + y, height,
                                      objectMap[x][y][objInd].getType(), objectMap[x][y][objInd].getRotation(), true);
                    }
                }
            }
        }
    }

    /**
     * Method redrawStandardObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param node_id
     * @param regionX
     * @param regionY
     * @param config
     */
    public void redrawStandardObject(Player player, int node_id, int regionX, int regionY, RoomConfig config) {
        int startDrawX = (player.getKnownRegion()[0] - 5);
        int startDrawY = (player.getKnownRegion()[1] - 5);
        int offsetX    = 0;
        int offsetY    = 0;

        offsetX = startDrawX + (regionX) - 1;
        offsetY = startDrawY + (regionY) - 1;
        offsetX = (offsetX + 6) * 8;
        offsetY = (offsetY + 6) * 8;

        if (localObjectList[node_id] != null) {
            int built_id = config.getBuilt(node_id);

            if (built_id == -1) {
                built_id = localObjectList[node_id].getObject().getId();
            }

            player.getActionSender().sendPlainObject(built_id, offsetX + localObjectList[node_id].getRegionX(),
                    offsetY + localObjectList[node_id].getRegionY(), localObjectList[node_id].getObject().getType(),
                    localObjectList[node_id].getObject().getRotation(), player.getHeight());
        }
    }

    /**
     * Method redrawRugObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param plane
     * @param regionX
     * @param regionY
     * @param roomConfig
     */
    public void redrawRugObjects(Player player, int plane, int regionX, int regionY, RoomConfig roomConfig) {
        int startDrawX = (player.getKnownRegion()[0] - 5);
        int startDrawY = (player.getKnownRegion()[1] - 5);
        int offsetX    = 0;
        int offsetY    = 0;

        offsetX = startDrawX + (regionX) - 1;
        offsetY = startDrawY + (regionY) - 1;
        offsetX = (offsetX + 6) * 8;
        offsetY = (offsetY + 6) * 8;
        player.getActionSender().setPlane(plane);

        int render_x = -1;
        int render_y = -1;

        for (ObjectNode current_node : localObjectList) {
            if (current_node == null) {
                return;
            }

            render_x = offsetX + current_node.getRegionX();
            render_y = offsetY + current_node.getRegionY();

            if (ObjectNode.getRugType(current_node.getObject().getId()) != -1) {
                int render_obj = current_node.getObject().getId();

                if (roomConfig.getRugID() > -1) {
                    switch (ObjectNode.getRugType(current_node.getObject().getId())) {
                    case 0 :
                        render_obj = ObjectNode.RUG_MAPPINGS_OUTSKIRTS[roomConfig.getRugID()];

                        break;

                    case 1 :
                        render_obj = ObjectNode.RUG_MAPPINGS_CENTER[roomConfig.getRugID()];

                        break;

                    case 2 :
                        render_obj = ObjectNode.RUG_MAPPINGS_CORNER[roomConfig.getRugID()];

                        break;
                    }
                }

                player.getActionSender().sendPlainObject(render_obj, render_x, render_y, 22,
                        current_node.getObject().getRotation(), plane);
            }
        }
    }

    // this renders all the objects according to the room config
    // ie it will delete the ghost objects, and replace with the set objects, or it will modify windows and turn into walls if stuffs intersecting etc
    // this file is a room definition

    /**
     * Method render_objects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param plane
     * @param regionX
     * @param regionY
     * @param theme
     * @param building_mode
     * @param room_data
     * @param surrounding
     * @param rendered
     */
    public void render_objects(Player player, int plane, int regionX, int regionY, int theme, boolean building_mode,
                               RoomConfig room_data, boolean[] surrounding, boolean[][][] rendered) {
        int startDrawX = (player.getKnownRegion()[0] - 5);
        int startDrawY = (player.getKnownRegion()[1] - 5);
        int offsetX    = 0;
        int offsetY    = 0;

        offsetX = startDrawX + (regionX) - 1;
        offsetY = startDrawY + (regionY) - 1;
        offsetX = (offsetX + 6) * 8;
        offsetY = (offsetY + 6) * 8;
        player.getActionSender().setPlane(plane);

        GameObject current_obj;
        ObjectNode current_node;
        int        render_x = -1;
        int        render_y = -1;

        // loop through the node list (ghost object list)
        for (int node_index = 0; node_index < localObjectList.length; node_index++) {
            if (localObjectList[node_index] == null) {
                break;
            }

            current_node = localObjectList[node_index];
            current_obj  = current_node.getObject();
            render_x     = offsetX + current_node.getRegionX();
            render_y     = offsetY + current_node.getRegionY();

            if ((room_data.getRoom() == 6) && (current_obj.getType() == 22)) {
                if (!building_mode) {
                    if (room_data.getRingType() == -1) {
                        continue;
                    }
                }

                if (room_data.getRingType() >= 0) {
                    int render_obj = -1;

                    if ((current_obj.getId() == 15294) || (current_obj.getId() == 15289)
                            || (current_obj.getId() == 15290) || (current_obj.getId() == 15295)) {
                        render_obj = ObjectNode.MAT_MAPPINGS_CORNER[room_data.getRingType()];
                    } else if ((current_obj.getId() == 15293) || (current_obj.getId() == 15291)
                               || (current_obj.getId() == 15288)) {
                        render_obj = ObjectNode.MAT_MAPPINGS_OUTSKIRTS[room_data.getRingType()];
                    } else if (current_obj.getId() == 15292) {
                        render_obj = ObjectNode.MAT_MAPPINGS_CENTER[room_data.getRingType()];
                    }

                    player.getActionSender().sendPlainObject(render_obj, render_x, render_y, 22,
                            current_obj.getRotation(), plane);

                    continue;
                }

                continue;
            }

            if (room_data.getRoom() == 13) {
                int obj_id = current_obj.getId();

                if ((obj_id == 15370) || (obj_id == 15371) || (obj_id == 15372)) {
                    if (room_data.getHedge() > -1) {
                        int render = ObjectNode.HEDGING_TYPES[room_data.getHedge()];

                        if (obj_id == 15371) {
                            render += 2;
                        }

                        if (obj_id == 15372) {
                            render++;
                        }

                        player.getActionSender().sendPlainObject(render, render_x, render_y, current_obj.getType(),
                                current_obj.getRotation(), plane);

                        continue;
                    }
                } else if (obj_id == 15369) {
                    if (room_data.getFence() > -1) {
                        player.getActionSender().sendPlainObject(ObjectNode.WALL_TYPES[room_data.getFence()], render_x,
                                render_y, current_obj.getType(), current_obj.getRotation(), plane);

                        continue;
                    }
                }
            }

            if ((current_node.getType() == ObjectNode.TYPE_TRAP)
                    || (current_node.getType() == ObjectNode.TYPE_MONSTER)) {
                if ((room_data.getBuilt(current_node.getId()) > 0) && building_mode) {
                    player.getActionSender().sendPlainObject(room_data.getBuilt(current_node.getId()), render_x,
                            render_y, current_obj.getType(), current_obj.getRotation(), plane);

                    continue;
                }else if(!building_mode){
                    player.getActionSender().deleteObject(render_x, render_y,  current_obj.getRotation(),10, plane);
                }
            }

            if (room_data.getRoom() == 15) {
                boolean break2 = false;

                if ((room_data.getFloorType() > -1) || (room_data.getPrisonType() > -1)) {
                    int render_id = -1;

                    switch (current_obj.getId()) {
                    case 15349 :
                        if (room_data.getFloorType() == -1) {
                            break2 = true;
                        } else {
                            render_id = ObjectNode.FLOOR_TYPES_CORNER[room_data.getFloorType()];
                        }

                        break;

                    case 15348 :
                        if (room_data.getFloorType() == -1) {
                            break2 = true;
                        } else {
                            render_id = ObjectNode.FLOOR_TYPES_OUTSKIRT[room_data.getFloorType()];
                        }

                        break;

                    case 15353 :
                    case 15352 :
                        if (room_data.getPrisonType() > -1) {
                            render_id = ObjectNode.PRISON_WALL_TYPES[room_data.getPrisonType()]
                                        + ((current_obj.getId() == 15353)
                                           ? 1
                                           : 0);
                        } else {
                            break2 = true;
                        }

                        break;

                    case 15350 :
                    case 15347 :
                        if (room_data.getFloorType() == -1) {
                            break2 = true;
                        } else {
                            render_id = ObjectNode.FLOOR_TYPES_CENTER[room_data.getFloorType()];
                        }

                        break;

                    default :
                        break2 = true;
                    }

                    if (!break2) {
                        if (render_id == 15337) {
                            if (building_mode) {
                                player.getActionSender().deleteWorldObject(render_x, render_y,
                                        current_obj.getRotation(), current_obj.getType(), plane);
                            }

                            player.getActionSender().sendPlainObject(render_id, render_x, render_y, 10,
                                    current_obj.getRotation(), plane);
                        } else {
                            player.getActionSender().sendPlainObject(render_id, render_x, render_y,
                                    current_obj.getType(), current_obj.getRotation(), plane);
                        }

                        continue;
                    }
                }
            }

            if ((current_obj.getType() == 22) && (ObjectNode.getRugType(current_obj.getId()) != -1)) {
                if (room_data.getRugID() != -1) {
                    int render_obj = -1;

                    switch (ObjectNode.getRugType(current_obj.getId())) {
                    case 0 :
                        render_obj = ObjectNode.RUG_MAPPINGS_OUTSKIRTS[room_data.getRugID()];

                        break;

                    case 1 :
                        render_obj = ObjectNode.RUG_MAPPINGS_CENTER[room_data.getRugID()];

                        break;

                    case 2 :
                        render_obj = ObjectNode.RUG_MAPPINGS_CORNER[room_data.getRugID()];

                        break;
                    }

                    if (render_obj != -1) {
                        player.getActionSender().sendPlainObject(render_obj, render_x, render_y, 22,
                                current_obj.getRotation(), plane);
                    }
                } else if (!building_mode) {
                    player.getActionSender().deleteWorldObject(render_x, render_y, current_obj.getRotation(), 22,
                            plane);
                }
            } else if (current_node.getType() == ObjectNode.TYPE_DOOR) {
                if (!building_mode) {
                    boolean render = !surrounding[current_obj.getRotation()];

                    if (type == TYPE_GARDEN) {
                        render = false;    // dont draw doors on gardens
                    }

                    if (render) {
                        renderDoor(player, render_x, render_y, current_obj.getId(), current_obj.getRotation(), theme,
                                   plane, room_data, rendered, (regionX * 8) + current_node.getRegionX(),
                                   (regionY * 8) + current_node.getRegionY());
                    } else {
                        if (rendered[plane][(regionX * 8) + current_node.getRegionX()][(regionY * 8) + current_node.getRegionY()]) {
                            continue;
                        }

                        player.getActionSender().deleteDoor(render_x, render_y, current_obj.getRotation(), plane);
                    }
                }
            } else if (current_node.getType() == ObjectNode.TYPE_WINDOW) {
                boolean render = !surrounding[current_obj.getRotation()];

                if (plane == 0) {
                    render = false;
                }

                if (!building_mode) {
                    if (render) {
                        player.getActionSender().sendPlainObject(WINDOW_IDS[theme], render_x, render_y, 0,
                                current_obj.getRotation(), plane);
                    } else {
                        player.getActionSender().sendPlainObject(THEME_WALL_IDS[theme], render_x, render_y, 0,
                                current_obj.getRotation(), plane);
                    }
                }
            } else if (current_node.getType() == ObjectNode.TYPE_NORMAL) {

                // ring perimter
                if (isRing(current_obj.getId())) {
                    int type = -1;

                    if (room_data.getRingType() == 0) {
                        type = 13129;
                    } else if (room_data.getRingType() == 1) {
                        type = 13133;
                    } else if (room_data.getRingType() == 2) {
                        type = 13137;
                    }

                    if (building_mode && (type == -1)) {
                        continue;
                    } else if (!building_mode && (type == -1)) {
                        player.getActionSender().deleteWorldObject(render_x, render_y, current_obj.getRotation(),
                                current_obj.getType(), plane);
                    } else {
                        player.getActionSender().sendPlainObject(type, render_x, render_y, current_obj.getType(),
                                current_obj.getRotation(), plane);
                    }

                    continue;
                }

                if (room_data.getBuilt(current_node.getId()) != -1) {
                    int current = room_data.getCurrent(current_node.getId());

                    if (current == -1) {
                        player.getActionSender().deleteWorldObject(render_x, render_y, current_obj.getRotation(),
                                current_obj.getType(), plane);
                    } else {
                        player.getActionSender().sendPlainObject(room_data.getCurrent(current_node.getId()), render_x,
                                render_y, current_obj.getType(), current_obj.getRotation(), plane);
                    }
                } else if (!building_mode) {
                    player.getActionSender().deleteWorldObject(render_x, render_y, current_obj.getRotation(),
                            current_obj.getType(), plane);
                }
            } else if (current_node.getType() == ObjectNode.TYPE_STAIRS) {
                if (room_data.getStairsType() != -1) {
                    player.getActionSender().sendPlainObject(room_data.getStairsType(), render_x, render_y,
                            current_obj.getType(), current_obj.getRotation(), plane);
                } else if (!building_mode) {
                    player.getActionSender().deleteWorldObject(render_x, render_y, current_obj.getRotation(),
                            current_obj.getType(), plane);
                }
            }
        }

        player.getActionSender().setPlane(4);
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static String getType(int id) {
        if (isHotspot(id)) {
            return "door";
        }

        if (id == 13830) {
            return "window";
        }

        return "normal";
    }

    /**
     * Method dumpObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     */
    public static void dumpObjects(int x, int y) {
        Landscape land = MapFetcher.get(x / 64, y / 64, 0);

        if (land != null) {
            GameObject[][][] objectMap = getObjectMap(land, x, y);
            int              i         = 0;

            for (int rgx = 0; rgx < 8; rgx++) {
                for (int rgy = 0; rgy < 8; rgy++) {
                    for (int ic = 0; ic < 2; ic++) {
                        if (objectMap[rgx][rgy][ic] != null) {
                            if (CacheObjectDefinition.forId(objectMap[rgx][rgy][ic].getId()).hasActions()) {
                                System.out.println("node = " + i + " " + objectMap[rgx][rgy][ic].getId() + " " + rgx
                                                   + " " + rgy + " " + objectMap[rgx][rgy][ic].getRotation() + " "
                                                   + objectMap[rgx][rgy][ic].getType() + " "
                                                   + getType(objectMap[rgx][rgy][ic].getId()));
                                i++;
                            }
                        }
                    }
                }
            }
        }
    }

    private GameObject[][][] getObjectMap() {
        List<GameObject>[][][] object_list   = landscape.getObjects();
        int                    mainRegionX   = ((paletteX / 64) * 8) - 6;
        int                    mainRegionY   = ((paletteY / 64) * 8) - 6;
        int                    ourRegionX    = (paletteX / 8) - 6;
        int                    ourRegionY    = (paletteY / 8) - 6;
        int                    offsetX       = ourRegionX - mainRegionX;
        int                    offsetY       = ourRegionY - mainRegionY;
        int                    startX        = (offsetX * 8);
        int                    startY        = (offsetY * 8);
        GameObject[][][]       returnObjects = new GameObject[8][8][3];
        int                    ic            = 0;

        for (int x = startX; x < startX + 8; x++) {
            for (int y = startY; y < startY + 8; y++) {
                if ((object_list[0][x][y] != null) && (object_list[0][x][y].size() > 0)) {
                    ic = 0;

                    for (GameObject obj : object_list[0][x][y]) {
                        switch (obj.getType()) {
                        case 10 :
                            if (CacheObjectDefinition.forID(obj.getId()).getClippingType() == 0) {
                                continue;
                            }
                        case 0 :
                        case 2 :
                            returnObjects[x - startX][y - startY][ic++] = obj;

                            break;
                        }
                    }
                }
            }
        }

        return returnObjects;
    }

    private static GameObject[][][] getObjectMap(Landscape landscape, int paletteX, int paletteY) {
        List<GameObject>[][][] object_list   = landscape.getObjects();
        int                    mainRegionX   = ((paletteX / 64) * 8) - 6;
        int                    mainRegionY   = ((paletteY / 64) * 8) - 6;
        int                    ourRegionX    = (paletteX / 8) - 6;
        int                    ourRegionY    = (paletteY / 8) - 6;
        int                    offsetX       = ourRegionX - mainRegionX;
        int                    offsetY       = ourRegionY - mainRegionY;
        int                    startX        = (offsetX * 8);
        int                    startY        = (offsetY * 8);
        GameObject[][][]       returnObjects = new GameObject[8][8][3];

        for (int x = startX; x < startX + 8; x++) {
            for (int y = startY; y < startY + 8; y++) {
                int ic = 0;

                if ((object_list[0][x][y] != null) && (object_list[0][x][y].size() > 0)) {
                    for (GameObject obj : object_list[0][x][y]) {
                        switch (obj.getType()) {
                        case 0 :
                        case 2 :
                        case 22 :
                        case 10 :
                        case 11 :
                            returnObjects[x - startX][y - startY][ic++] = obj;

                            break;
                        }
                    }
                }
            }
        }

        return returnObjects;
    }

    /**
     * Method get_object_map
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<GameObject>[][][] get_object_map() {
        List<GameObject>[][][] object_list = landscape.getObjects();
        int                    mainRegionX = ((paletteX / 64) * 8) - 6;
        int                    mainRegionY = ((paletteY / 64) * 8) - 6;
        int                    ourRegionX  = (paletteX / 8) - 6;
        int                    ourRegionY  = (paletteY / 8) - 6;
        int                    offsetX     = ourRegionX - mainRegionX;
        int                    offsetY     = ourRegionY - mainRegionY;
        int                    startX      = (offsetX * 8);
        int                    startY      = (offsetY * 8);

        return null;
    }

    /**
     * Method getSubDoor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getSubDoor(int id) {
        for (int i = 0; i < DOOR_HOTSPOT_IDS.length; i++) {
            for (int g = 0; g < DOOR_HOTSPOT_IDS[g].length; g++) {
                if (DOOR_HOTSPOT_IDS[i][g] == id) {
                    return DOOR_CLOSED_IDS[i][g];
                }
            }
        }

        return -1;
    }

    /**
     * Method isHSP
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean isHSP(int id) {
        for (int i = 0; i < DOOR_HOTSPOT_IDS.length; i++) {
            for (int g = 0; g < DOOR_HOTSPOT_IDS[g].length; g++) {
                if (DOOR_HOTSPOT_IDS[i][g] == id) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method getObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public GameObject getObject(int x, int y) {
        if ((objects[x][y] == null) || (objects[x][y].size() == 0)) {
            return null;
        }

        return objects[x][y].get(0);
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void setType(int t) {
        this.type = t;
    }

    /**
     * Method connects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     * @param rx1
     * @param ry1
     * @param rx2
     * @param ry2
     *
     * @return
     */
    public boolean connects(RoomNode i, int rx1, int ry1, int rx2, int ry2) {
        int r1_local_x = -1;
        int r1_local_y = -1;
        int r2_local_x = -1;
        int r2_local_y = -1;
        int count      = 0;

        for (ObjectNode object_node : localObjectList) {
            if (object_node == null) {
                break;
            }

            if (object_node.getType() == ObjectNode.TYPE_DOOR) {
                count++;
            }
        }

        for (ObjectNode object_node : localObjectList) {
            if (object_node == null) {
                return false;
            }

            if (object_node.getType() == ObjectNode.TYPE_DOOR) {
                for (int k = 0; k < i.localObjectList.length; k++) {
                    if (i.localObjectList[k] == null) {
                        break;
                    }

                    if (i.localObjectList[k].getType() == ObjectNode.TYPE_DOOR) {
                        r1_local_x = (rx1 * 8) + object_node.getRegionX();
                        r1_local_y = (ry1 * 8) + object_node.getRegionY();
                        r2_local_x = (rx2 * 8) + i.localObjectList[k].getRegionX();
                        r2_local_y = (ry2 * 8) + i.localObjectList[k].getRegionY();

                        if (Point.getDistance(r1_local_x, r1_local_y, r2_local_x, r2_local_y) == 1) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    /**
     * Method getLabel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getLabel() {
        return label;
    }

    /**
     * Method setLabel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Method getLevel_requirement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevel_requirement() {
        return level_requirement;
    }

    /**
     * Method setLevel_requirement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param level_requirement
     */
    public void setLevel_requirement(int level_requirement) {
        this.level_requirement = level_requirement;
    }

    /**
     * Method getExperience
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getExperience() {
        return experience;
    }

    /**
     * Method setExperience
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param experience
     */
    public void setExperience(int experience) {
        this.experience = experience;
    }

    /**
     * Method getPaletteY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPaletteY() {
        return paletteY;
    }

    /**
     * Method setPaletteY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param paletteY
     */
    public void setPaletteY(int paletteY) {
        this.paletteY = paletteY;
    }

    /**
     * Method getPaletteX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPaletteX() {
        return paletteX;
    }

    /**
     * Method setPaletteX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param paletteX
     */
    public void setPaletteX(int paletteX) {
        this.paletteX = paletteX;
    }

    /**
     * Method getHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getHeight() {
        return height;
    }

    /**
     * Method setHeight
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param height
     */
    public void setHeight(int height) {
        this.height = height;
    }
}
