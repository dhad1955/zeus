package net.tazogaming.hydra.game.skill.construction;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.entity3d.player.actions.BonesOnAltarAction;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.map.*;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.skill.construction.obj.ObjectNode;
import net.tazogaming.hydra.game.ui.Dialog;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/01/14
 * Time: 23:13
 */
public class House extends Instance implements RecurringTickEvent {
    public static final Zone HOUSE_ZONE = new Zone(1857, 5033, 1958, 5132, -1, false, false,
                                              "house").setBankingAllowed(false);
    public static final int     HOUSE_AREA_WIDTH    = 8;
    public static final int     HOUSE_AREA_LENGTH   = 8;
    public static final int     HOUSE_WIDTH         = 13;
    public static final int     HOUSE_LENGTH        = 13;
    public static final int     HOUSE_HEIGHT        = 4;
    private static DeathHandler HOUSE_DEATH_HANDLER = new DeathHandler() {
        @Override
        public void handleDeath(Killable killed, Killable killedBy) {
            Player player = (Player) killed;

            player.restore_all_stats();

            House house = player.getCurrentHouse();

            if (house.isInRing(player)) {
                killedBy.getPlayer().getActionSender().sendMessage("You have defeated :"
                        + killed.getPlayer().getUsername());
                player.teleport(RENDER_START_X + ((player.getKnownLocalX() / 8) * 8),
                                RENDER_START_Y + ((player.getKnownLocalY() / 8) * 8), player.getHeight());
                house.destructRing(player);
            } else {
                Point p = house.teleportToPortal();

                if (p != null) {
                    player.teleport(p.getX(), p.getY(), 1);
                }
            }
        }
    };
    public static final int
        RENDER_START_X                               = 1856,
        RENDER_START_Y                               = 5032;
    public static int          GARDEN_COLOR          = 0;
    private boolean            isDestructed          = false;
    private ArrayList<NPC>     roamingNpcs           = new ArrayList<NPC>();
    private int                floorColor            = 13;
    private boolean            building_mode         = true;
    private int                theme                 = 0;
    private RoomConfig[][][]   house_layout          = new RoomConfig[4][13][13];
    private DynamicClippingMap clipping_map          = new DynamicClippingMap(3, 104, 104);
    private int                current_draw_rotation = 0;
    private int                current_draw_id       = 0;
    private int                current_draw_x        = 0,
                               current_draw_y        = 0;
    private int                draw_from_x           = 0,
                               draw_from_y           = 0;
    private boolean            is_drawing_mode       = false;
    private int                remove_id             = -1;
    private ArrayList<Player>  players               = new ArrayList<Player>();
    private Butler             butler;
    private Player             player;

    /**
     * Constructs ...
     *
     *
     * @param player1
     */
    public House(Player player1) {
        this(player1, true);
    }

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param building_mode
     */
    public House(Player player, boolean building_mode) {
        this.building_mode = building_mode;
        Account.forceSetting(player, 441, building_mode
                ? 0
                : 1);
        house_layout = player.getAccount().getHouseLayout();

        if (player.getAccount().hasVar("house_theme")) {
            theme = player.getAccount().getInt32("house_theme") - 1;
        } else {}

        if (player.getAccount().hasVar("floor_color")) {
            floorColor = player.getAccount().getInt32("floor_color");
        } else {
            floorColor = 0;
        }

        this.player = player;
        players.add(player);

        // house_layout[1][6][6] = build_basic_garden();
        player.getActionSender().sendConstructionStatus(building_mode
                ? 1
                : 5);
        construct_object_mappings();
        player.setCurrentHouse(this);
        construct_house(player);
        player.setCurrentInstance(this);
        player.getActionSender().sendConstructionStatus(0);
        Core.submitTask(this);

        if (player.getAccount().hasVar("butler_type")) {
            butler = new Butler(player.getAccount().getInt32("butler_type"), this);
        }

        if (!building_mode) {
            spawnNpcs();
        }

        Point p = teleportToPortal();

        if (p != null) {
            player.teleport(p.getX(), p.getY(), 1);
        } else {
            player.teleport(player.getX(), player.getY(), 1);
        }
    }

    /**
     * Method sendToCorner
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void sendToCorner(Player player) {
        player.teleport(RENDER_START_X + ((player.getKnownLocalX() / 8) * 8),
                        RENDER_START_Y + ((player.getKnownLocalY() / 8) * 8), player.getHeight());
    }

    /**
     * Method setFloor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cool
     */
    public void setFloor(int cool) {
        floorColor = cool;
        reload();
    }

    /**
     * Method addRoamingNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param roomX
     * @param roomY
     * @param id
     */
    public void addRoamingNpc(int roomX, int roomY, int id) {}

    /**
     * Method getLayout
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RoomConfig[][][] getLayout() {
        return house_layout;
    }

    /**
     * Method getStaticHouseDimensions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static int getStaticHouseDimensions(Player player) {
        return 30;
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Player> getPlayers() {
        return players;
    }

    /**
     * Method getMaxRooms
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     *
     * @return
     */
    public static int getMaxRooms(Player plr) {
        int staticAmt = getStaticHouseDimensions(plr);


        if (staticAmt > 100) {
            staticAmt = 100;
        }

        return staticAmt;
    }

    /**
     * Method getRoomID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRoomID() {
        return house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8].getRoom();
    }

    /**
     * Method getClippingMap
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public DynamicClippingMap getClippingMap() {
        return clipping_map;
    }

    /**
     * Method getButler
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Butler getButler() {
        return butler;
    }

    /**
     * Method addPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     *
     * @return
     */
    public boolean addPlayer(Player plr) {
        if (!players.contains(plr)) {
            players.add(plr);

            if (plr.getCurrentFamiliar() != null) {
                plr.getCurrentFamiliar().setCurrentInstance(this);
            }

            plr.getActionSender().sendConstructionStatus(5);
            construct_house(plr);
            plr.getActionSender().sendConstructionStatus(0);
            plr.teleport(plr.getLocation().getX(), plr.getLocation().getY(), 1);
            plr.setCurrentInstance(this);
            plr.setCurrentHouse(this);

            Point p = teleportToPortal();

            if (p != null) {
                plr.teleport(p.getX(), p.getY(), 1);
            }

            return true;
        }

        return false;
    }

    /**
     * Method destructHouse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public void destructHouse(Player plr) {
        plr.is_con = false;
        plr.getWatchedNPCs().clear();
        plr.getWatchedPlayers().clear();
        plr.teleport(3214, 3434, 0);
        plr.setCurrentInstance(null);
        plr.cancelDeaths();


        plr.setCurrentHouse(null);
        plr.setCurDeathHandler(new DefaultDeathHandler());
        plr.getAccount().enable_all_actions();

        destructRing(plr);

        if (plr.getAccount().getSmallSetting(Account.CHAIR_SITTING_ID) > 0) {
            plr.getAccount().putSmall(Account.CHAIR_SITTING_ID, 0);
            changeNode(plr, plr.getAccount().getSmallSetting(Account.CHAIR_SITTING_ID), 0, 0);
        }
    }

    /**
     * Method destructRing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void destructRing(Player player) {
        player.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "null");
        player.restore_all_stats();
        player.setPoisonTime(0);
        player.getPrayerBook().addPoints(99);
        player.getActionSender().sendStat(5);
        player.setCurrentSpecial(5);
        SpecialAttacks.updateSpecial(player);
        player.setCurrentHealth(player.getMaxHealth());
        player.getAccount().enable_all_actions();
    }

    /**
     * Method notifyRemove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public void notifyRemove(Player plr) {
        destructHouse(plr);
        plr.setCurrentInstance(null);
        players.remove(plr);
    }

    /**
     * Method changeRoom
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param h
     * @param id
     */
    public void changeRoom(int h, int id) {
        int localX = player.getKnownLocalX();
        int localY = player.getKnownLocalY();

        house_layout[h][localX / 8][localY / 8].changeId(id);
    }

    /**
     * Method modifyRotations
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void modifyRotations() {
        int localX          = player.getKnownLocalX();
        int localY          = player.getKnownLocalY();
        int curRoomRoations = house_layout[player.getHeight()][localX / 8][localY / 8].getRotation();
    }

    /**
     * Method destruct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void destruct() {
        for (Player player1 : players) {
            destructHouse(player1);
        }

        players.clear();

        if (butler != null) {
            butler.unlink();
        }

        isDestructed = true;
    }

    /**
     * Method bootAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void bootAll() {
        for (Player player1 : players) {
            if (player1 != player) {
                destructHouse(player1);
            }
        }

        players.clear();
        players.add(player);
    }

    /**
     * Method bootGuest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void bootGuest(String name) {
        Player pla = null;

        for (Player player1 : players) {
            if (player1.getUsername().equalsIgnoreCase(name)) {
                pla = player1;
            }
        }

        if (pla == null) {
            player.getActionSender().sendMessage("invalid player");

            return;
        }

        destructHouse(pla);
        players.remove(pla);
    }

    /**
     * Method setupCombatStone
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param regionX
     * @param regionY
     * @param npcId
     * @param lifePoints
     * @param object
     */
    public void setupCombatStone(final Player player, int regionX, int regionY, int npcId, int lifePoints,
                                 final ObjectNode object) {
        changeNode(player, object.getId(), -1, 1000);

        NPC npc = new NPC(npcId,
                          translate(regionX, regionY, player.getHeight(), object.getRegionX(), object.getRegionY()), 0);

        npc.setCurrentInstance(this);
        World.getWorld().getNpcs().add(npc);
        npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.FAST_ACTION, DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                changeNode(player, object.getId(), 0, 0);
            }
        });
        roamingNpcs.add(npc);
    }

    /**
     * Method messageEveryone
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param msg
     */
    public void messageEveryone(String msg) {
        for (Player plr : players) {}
    }

    /**
     * Method remove_room
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void remove_room() {
        int x = ((remove_id >> 16) & 0xFFFF);
        int y = (((remove_id & 0xFFFF)));

        house_layout[player.getHeight()][x][y] = null;
        reload();
    }

    /**
     * Method bonesOnAltarCheck
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param objectId
     * @param player1
     *
     * @return
     */
    public boolean bonesOnAltarCheck(int itemId, int objectId, Player player1) {
        if (getCurrentRoom(player1).getRoom() == 10) {
            int foundIndex = 0;


            for (int i = 0; i < ObjectNode.PRAYER_ALTAR_IDS.length; i++) {
                if (ObjectNode.PRAYER_ALTAR_IDS[i] == objectId) {
                    if (true) {
                        foundIndex = i;

                        int exp = 0;

                        for (int id = 0; id < ObjectNode.PRAYER_XP.length; id++) {
                            if (ObjectNode.PRAYER_XP[id][0] == itemId) {
                                exp = ObjectNode.PRAYER_XP[id][1];

                                break;
                            }
                        }

                        if (exp == 0) {
                            return false;
                        }

                        if (!getCurrentRoom(player1).containsNode(objectId)) {
                            return false;
                        }



                        if (isBuildingMode()) {
                            Dialog.printStopMessage(player1, "You can't do this in building mode");

                            return false;
                        }

                        player1.terminateActions();
                        player1.terminateScripts();

                        double bonus = ObjectNode.PRAYER_ALTAR_XP_BONUS[foundIndex];

                        for (int ix = 0; ix < ObjectNode.INCENSE_BURNERS.length; ix++) {
                            int incenseCount = getCurrentRoom(player1).getCount(ObjectNode.INCENSE_BURNERS[ix]);

                            bonus += (incenseCount * 0.25);
                        }

                        player1.setCurrentAction(new BonesOnAltarAction(itemId,
                                (int) (exp * (ObjectNode.PRAYER_ALTAR_XP_BONUS[foundIndex] + bonus)), player1));

                        return true;
                    } else {
                        Dialog.printStopMessage(player1, "You need to be the house owner to do this.");

                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Method change_mode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void change_mode() {
        building_mode = !building_mode;

        if (building_mode) {
            for (Player player1 : players) {
                if (player1 != player) {
                    destructHouse(player1);
                    player1.getActionSender().sendMessage(player.getUsername() + " has booted you [building mode]");
                }
            }

            players.clear();
            players.add(player);
        }

        construct_object_mappings();
        reload();
        clearNpcs();

        if (!building_mode) {
            spawnNpcs();
        }
    }

    /**
     * Method rotate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param added
     */
    public void rotate(int added) {
        if (!is_drawing_mode) {
            return;
        }

        RoomNode r       = null;
        int      cur_rot = current_draw_rotation;


        do {
            cur_rot += added;
            cur_rot &= 3;
            r       = RoomNode.getNode(current_draw_id, cur_rot);
        } while (!r.connects(house_layout[player.getHeight()][draw_from_x][draw_from_y].getRoomNode(), current_draw_x,
                             current_draw_y, draw_from_x, draw_from_y));

        RoomNode.getNode(current_draw_id, current_draw_rotation).clearDoors(player, current_draw_x, current_draw_y);
        current_draw_rotation = cur_rot;
        r                     = RoomNode.getNode(current_draw_id, current_draw_rotation);
        r.renderDoors(player, current_draw_x, current_draw_y);
    }

    /**
     * Method build_basic_garden
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static RoomConfig build_basic_garden() {
        RoomConfig config = new RoomConfig(12, 0);

        config.set_node(6, 13405);

        return config;
    }

    /**
     * Method getRoomCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRoomCount() {
        int accumulated = 0;

        for (int i = 0; i < 2; i++) {
            for (int x = 0; x < 13; x++) {
                for (int y = 0; y < 13; y++) {
                    if (house_layout[i][x][y] != null) {
                        accumulated++;
                    }
                }
            }
        }

        return accumulated;
    }

    /**
     * Method getRoomCountForID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public int getRoomCountForID(int id) {
        int accumulated = 0;

        for (int i = 0; i < 2; i++) {
            for (int x = 0; x < 13; x++) {
                for (int y = 0; y < 13; y++) {
                    if (house_layout[i][x][y] != null) {
                        if (house_layout[i][x][y].getRoom() == id) {
                            accumulated++;
                        }
                    }
                }
            }
        }

        return accumulated;
    }

    /**
     * Method getCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plane
     * @param id
     *
     * @return
     */
    public int getCount(int plane, int id) {
        int c = 0;

        for (int i = 0; i < house_layout[plane].length; i++) {
            for (int k = 0; k < house_layout[plane][i].length; k++) {
                if (house_layout[plane][k][i] != null) {
                    c += house_layout[plane][k][i].getCount(id);
                }
            }
        }

        return c;
    }

    /**
     * Method teleportToPortal
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point teleportToPortal() {
        for (int i = 0; i < 13; i++) {
            for (int k = 0; k < 13; k++) {
                if ((house_layout[1][i][k] != null) && house_layout[1][i][k].containsNode(13405)) {
                    return translate(i, k, 1, 2, 2);
                }
            }
        }

        return null;
    }

    /**
     * Method translate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param regionX
     * @param regionY
     * @param height
     * @param rgx
     * @param rgy
     *
     * @return
     */
    public Point translate(int regionX, int regionY, int height, int rgx, int rgy) {
        int startX = RENDER_START_X + (regionX * 8) + rgx;
        int startY = RENDER_START_Y + (regionY * 8) + rgy;

        return Point.location(startX, startY, height);
    }

    /**
     * Method translate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param rgx
     * @param rgy
     * @param h
     * @param obj
     *
     * @return
     */
    public Point translate(int rgx, int rgy, int h, ObjectNode obj) {
        return translate(rgx, rgy, h, obj.getRegionX(), obj.getRegionY());
    }

    /**
     * Method sitOnChar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param chair
     * @param built
     */
    public void sitOnChar(Player pla, ObjectNode chair, int built) {
        changeNode(pla, chair.getId(), -1, 100);

        Point translatedPoint = translate(pla.getKnownLocalX() / 8, pla.getKnownLocalY() / 8, pla.getHeight(), chair);

        pla.teleport(translatedPoint.getX(), translatedPoint.getY(), pla.getHeight());

        int viewPointTransformX = 0;
        int viewPointTransformY = 0;

        switch (chair.getObject().getRotation()) {
        case 0 :
            viewPointTransformY--;

            break;

        case 1 :
            viewPointTransformX--;

            break;

        case 2 :
            viewPointTransformY++;

            break;

        case 3 :
            viewPointTransformX++;

            break;
        }

        pla.getFacingLocation().focus(translatedPoint.getX() + viewPointTransformX,
                                      translatedPoint.getY() + viewPointTransformY);

        int animid = ObjectNode.getAnim(built);

        if (chair.getObject().getType() == 11) {
            animid++;
        }

        pla.getAnimation().animate(animid, 0, 1);
        pla.getAccount().putSmall(Account.CHAIR_SITTING_ID, chair.getId());
    }

    /**
     * Method changeNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param nodeId
     * @param newId
     * @param time
     */
    public void changeNode(Player player, int nodeId, int newId, int time) {
        RoomConfig conf = house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8];

        conf.changeObject(nodeId, newId, time);
        resendNode(conf, player.getKnownLocalX() / 8, player.getKnownLocalY() / 8, player.getHeight(), nodeId);
    }

    /**
     * Method changeFence
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void changeFence(int id) {
        RoomConfig config =
            house_layout[1][player.getAccount().getInt32("make_room_x")][player.getAccount().getInt32("make_room_y")];

        config.setFence(id);
        reload();
    }

    /**
     * Method changeHedge
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void changeHedge(int id) {
        getCurrentRoom(player).setHedge(id);
        reload();
    }

    /**
     * Method changeRing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void changeRing(int id) {
        getCurrentRoom(player).setRingType(id);
        reload();
    }

    /**
     * Method changeJail
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void changeJail(int id) {
        getCurrentRoom(player).changeJail(id);
        reload();
    }

    /**
     * Method changeFloor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param j
     */
    public void changeFloor(int j) {
        getCurrentRoom(player).changeFloorType(j);
        reload();
    }

    /**
     * Method resendNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param room
     * @param regionX
     * @param regionY
     * @param plane
     * @param nodeId
     */
    public void resendNode(RoomConfig room, int regionX, int regionY, int plane, int nodeId) {
        RoomNode   node     = room.getRoomNode();
        ObjectNode obj_node = node.getNode(nodeId);

        if (obj_node == null) {
            throw new NullPointerException("invalid object: " + nodeId);
        }

        for (Player plr : players) {
            int startDrawX = (player.getKnownRegion()[0] - 5);
            int startDrawY = (player.getKnownRegion()[1] - 5);
            int offsetX    = 0;
            int offsetY    = 0;

            offsetX = startDrawX + (regionX) - 1;
            offsetY = startDrawY + (regionY) - 1;
            offsetX = (offsetX + 6) * 8;
            offsetY = (offsetY + 6) * 8;
            plr.getActionSender().setPlane(plane);

            if (room.getCurrent(nodeId) == -1) {
                plr.getActionSender().deleteWorldObject(offsetX + obj_node.getRegionX(),
                        offsetY + obj_node.getRegionY(), obj_node.getObject().getRotation(),
                        obj_node.getObject().getType());
            } else {
                plr.getActionSender().sendPlainObject(room.getCurrent(nodeId), offsetX + obj_node.getRegionX(),
                        offsetY + obj_node.getRegionY(), obj_node.getObject().getType(),
                        obj_node.getObject().getRotation());
            }

            plr.getActionSender().setPlane(4);
        }
    }

    private boolean check_garden_removal() {
        return getCount(1, 13405) - 1 >= 1;
    }

    /**
     * Method get_stair_count
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param height_lvl
     *
     * @return
     */
    public int get_stair_count(int height_lvl) {
        int stair_count = 0;

        for (int x = 0; x < house_layout[height_lvl].length; x++) {
            for (int y = 0; y < house_layout[height_lvl][x].length; y++) {
                if ((house_layout[height_lvl][x][y] != null) && (house_layout[height_lvl][x][y].getStairsType() > 0)) {
                    stair_count++;
                }
            }
        }

        return stair_count;
    }

    /**
     * Method build_room
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void build_room() {
        if (!is_drawing_mode) {
            return;
        }

        house_layout[player.getHeight()][current_draw_x][current_draw_y] = new RoomConfig(current_draw_id,
                current_draw_rotation);
        reload();
        is_drawing_mode = false;
    }

    /**
     * Method getLocalX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param point
     *
     * @return
     */
    public static int getLocalX(int point) {
        return point - RENDER_START_X;
    }

    /**
     * Method getLocalY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param point
     *
     * @return
     */
    public static int getLocalY(int point) {
        return point - RENDER_START_Y;
    }

    /**
     * Method leave_draw_mode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void leave_draw_mode() {
        if (!this.is_drawing_mode) {
            return;
        }

        this.is_drawing_mode = false;

        RoomNode r = RoomNode.getNode(current_draw_id, current_draw_rotation);

        r.clearDoors(player, current_draw_x, current_draw_y);
    }

    /**
     * Method isRegistered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     *
     * @return
     */
    public boolean isRegistered(Player plr) {
        return players.contains(plr);
    }

    /**
     * Method reload
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void reload() {
        player.getActionSender().sendConstructionStatus(building_mode
                ? 1
                : 5);

        Point location = player.getLocation();

        player.is_con = false;
        player.teleport(300, 300, 1);
        player.getActionSender().sendMapArea();
        construct_object_mappings();
        construct_house(player);
        player.teleport(location.getX(), location.getY(), location.getHeight());
        player.getActionSender().sendConstructionStatus(0);
    }

    /**
     * Method getClipping
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param h
     * @param x
     * @param y
     *
     * @return
     */
    public int getClipping(int h, int x, int y) {
        return clipping_map.getClipping(x, y, h);
    }

    /**
     * Method build_room_above
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param room_x
     * @param room_y
     */
    public void build_room_above(int room_x, int room_y) {
        RoomConfig config = house_layout[player.getHeight()][room_x][room_y];

        if (config == null) {
            player.getActionSender().sendMessage("error invalid room");

            return;
        }
    }

    /**
     * Method construct_object_mappings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void construct_object_mappings() {
        int region_mapping_x = -1;
        int region_mapping_y = -1;
        int offset_x         = -1;
        int offset_y         = -1;

        clipping_map = new DynamicClippingMap(3, 104, 104);

        for (int height = 0; height < 3; height++) {
            for (int map_x = 0; map_x < 13; map_x++) {
                for (int map_y = 0; map_y < 13; map_y++) {
                    region_mapping_x = map_x * 8;
                    region_mapping_y = map_y * 8;

                    if ((house_layout[height][map_x][map_y] == null)) {
                        for (int i = region_mapping_x; i < region_mapping_x + 8; i++) {
                            for (int k = region_mapping_y; k < region_mapping_y + 8; k++) {
                                clipping_map.addClipping(i, k, height, 256);
                            }
                        }
                    } else if (house_layout[height][map_x][map_y] != null) {
                        ;
                        house_layout[height][map_x][map_y].getRoomNode().buildClippingData(clipping_map,
                                region_mapping_x, region_mapping_y, height, getSurrounding(height, map_x, map_y),
                                house_layout[height][map_x][map_y]);
                    }
                }
            }
        }
    }

    /**
     * Method remove_object
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     */
    public void remove_object(int id, int x, int y) {
        replace_object(player, -1, id, x, y);
    }

    /**
     * Method getCurRoomRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurRoomRotation() {
        return house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8].getRotation();
    }

    /**
     * Method replace_object
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param new_node
     * @param node
     * @param x
     * @param y
     */
    public void replace_object(Player player, int new_node, int node, int x, int y) {
        RoomConfig room = house_layout[player.getHeight()][x][y];

        if (room != null) {
            if (room.getRoomNode().getNode(node).getType() == ObjectNode.TYPE_STAIRS) {
                room.setStairs(new_node);
            }

            if (RoomNode.isRing(room.getRoomNode().getNode(node).getObject().getId())) {
                room.setRingType(-1);
                reload();

                return;
            }

            if (ObjectNode.getRugType(room.getRoomNode().getNode(node).getObject().getId()) != -1) {
                room.setRugID(-1);
                room.getRoomNode().redrawRugObjects(player, player.getHeight(), x, y, room);

                return;
            }

            if (RoomNode.isFloorType(room.getRoomNode().getNode(node).getObject().getId())) {
                room.changeFloorType(-1);
                reload();

                return;
            }

            if (RoomNode.isPrison(room.getRoomNode().getNode(node).getObject().getId())) {
                room.changeJail(-1);
                reload();

                return;
            }

            if (ObjectNode.isFence(room.getRoomNode().getNode(node).getObject().getId())) {
                room.setFence(-1);
                reload();

                return;
            }

            if (ObjectNode.isHedge(room.getRoomNode().getNode(node).getObject().getId())) {
                room.setHedge(-1);
                reload();

                return;
            }

            room.set_node(node, new_node);
            room.getRoomNode().redrawStandardObject(player, node, x, y, room);
        } else {
            player.getActionSender().sendMessage("nul: " + node + " " + new_node + " " + x + " " + y);
        }
    }

    /**
     * Method changeRug
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void changeRug(int id) {
        RoomConfig config = house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8];

        if (config != null) {
            config.setRugID(id);
            config.getRoomNode().redrawRugObjects(player, player.getHeight(), player.getKnownLocalX() / 8,
                    player.getKnownLocalY() / 8, config);
        }
    }

    /**
     * Method set_room_make_id
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void set_room_make_id(int id) {
        this.current_draw_id = id;
    }

    /**
     * Method can_go_up
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     *
     * @return
     */
    public boolean can_go_up(Player plr) {
        int        stairsType = getCurrentRoom(plr).getStairsType();
        RoomConfig conf       = house_layout[plr.getHeight() + 1][plr.getKnownLocalX() / 8][plr.getKnownLocalY() / 8];    // .getStairsType();

        if ((conf != null) && ((conf.getStairsType() == stairsType) || (conf.getStairsType() == stairsType + 1))) {
            return true;
        }

        return false;
    }

    /**
     * Method can_go_down
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     *
     * @return
     */
    public boolean can_go_down(Player plr) {
        int        stairsType = getCurrentRoom(plr).getStairsType();
        RoomConfig conf       = house_layout[plr.getHeight() - 1][plr.getKnownLocalX() / 8][plr.getKnownLocalY() / 8];    // .getStairsType();

        if ((conf != null) && ((conf.getStairsType() == stairsType) || (conf.getStairsType() == stairsType - 1))) {
            return true;
        }

        return false;
    }

    /**
     * Method set_stairs_above
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void set_stairs_above(int id) {
        RoomConfig curConfig =
            house_layout[player.getHeight() + 1][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8];

        if (player.getHeight() == 0) {
            RoomConfig top_floor =
                house_layout[player.getHeight() + 2][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8];

            if (top_floor != null) {
                if (top_floor.getStairsType() != -1) {
                    if (id == 13504) {
                        top_floor.setStairs(id);
                        id--;
                    } else {
                        return;
                    }
                }
            }
        }

        if ((curConfig.getRoom() == 22) || (curConfig.getRoom() == 7) || (curConfig.getRoom() == 20)
                || (curConfig.getRoom() == 21)) {
            if (curConfig.getRoom() == 7) {
                curConfig.changeId(22);
            }

            if (curConfig.getRoom() == 20) {
                curConfig.changeId(21);
            }

            curConfig.setStairs(id);
            curConfig
                .setRotation(house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8]
                    .getRotation());
        }
    }

    /**
     * Method set_stairs_below
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void set_stairs_below(int id) {
        if (player.getHeight() == 0) {
            return;
        }

        RoomConfig curConfig =
            house_layout[player.getHeight() - 1][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8];

        if ((curConfig.getRoom() == 22) || (curConfig.getRoom() == 7) || (curConfig.getRoom() == 20)
                || (curConfig.getRoom() == 21) || (curConfig.getRoom() == 17)) {
            if (curConfig.getRoom() == 22) {
                curConfig.changeId(7);
            }

            if (curConfig.getRoom() == 21) {
                curConfig.changeId(20);
            }

            curConfig.setStairs(id);
            curConfig
                .setRotation(house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8]
                    .getRotation());
        }
    }

    /**
     * Method isRoomAbove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isRoomAbove(Player player) {
        int local_x = player.getKnownLocalX();
        int local_y = player.getKnownLocalY();

        if (player.getHeight() == 2) {
            return false;
        }


        return house_layout[player.getHeight() + 1][local_x / 8][local_y / 8] != null;
    }

    /**
     * Method isRoomAbove
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     *
     * @return
     */
    public boolean isRoomAbove(Player player, int id) {
        int local_x = player.getKnownLocalX();
        int local_y = player.getKnownLocalY();

        if (player.getHeight() == 2) {
            return false;
        }

        return (house_layout[player.getHeight() + 1][local_x / 8][local_y / 8] != null)
               && (house_layout[player.getHeight() + 1][local_x / 8][local_y / 8].getRoom() == id);
    }

    /**
     * Method isRoomBelow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     *
     * @return
     */
    public boolean isRoomBelow(Player player, int id) {
        int local_x = player.getKnownLocalX();
        int local_y = player.getKnownLocalY();

        if (player.getHeight() == 2) {
            return false;
        }

        return (house_layout[player.getHeight() - 1][local_x / 8][local_y / 8] != null)
               && (house_layout[player.getHeight() - 1][local_x / 8][local_y / 8].getRoom() == id);
    }

    /**
     * Method isStairRoom
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param above
     *
     * @return
     */
    public boolean isStairRoom(boolean above) {
        int id = house_layout[player.getHeight() + (above
                ? 1
                : -1)][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8].getRoom();

        if (above) {
            return (id == 22) || (id == 21);
        } else {
            return (id == 7) || (id == 17) || (id == 20);
        }
    }

    /**
     * Method isRoomBelow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isRoomBelow(Player player) {
        int local_x = player.getKnownLocalX();
        int local_y = player.getKnownLocalY();

        if (player.getHeight() == 0) {
            return false;
        }

        return house_layout[player.getHeight() - 1][local_x / 8][local_y / 8] != null;
    }

    /**
     * Method isBuildingMode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBuildingMode() {
        return building_mode;
    }

    /**
     * Method getRoomCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lvl
     *
     * @return
     */
    public int getRoomCount(int lvl) {
        int count = 0;

        for (int x = 0; x < house_layout[lvl].length; x++) {
            for (int y = 0; y < house_layout[lvl][x].length; y++) {
                if (house_layout[lvl][x][y] != null) {
                    count++;
                }
            }
        }

        return count;
    }

    /**
     * Method remove_room
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lvl
     */
    public void remove_room(int lvl) {
        house_layout[lvl][getPlayer().getKnownLocalX() / 8][getPlayer().getKnownLocalY() / 8] = null;
        reload();
    }

    /**
     * Method isInRing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isInRing(Player player) {
        RoomConfig config = house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8];

        if (config == null) {
            return false;
        }

        if (config.getRingType() == -1) {
            return false;
        }

        if (config.getRoom() != 6) {
            return false;
        }

        int room_localX = player.getKnownLocalX() % 8;
        int room_localY = player.getKnownLocalY() % 8;

        return (room_localX >= 2) && (room_localX <= 5) && (room_localY >= 2) && (room_localY <= 5);
    }

    /**
     * Method isinCenter
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isinCenter(Player player) {
        int room_localX = player.getKnownLocalX() % 8;
        int room_localY = player.getKnownLocalY() % 8;

        return (room_localX >= 2) && (room_localX <= 5) && (room_localY >= 2) && (room_localY <= 5);
    }

    /**
     * Method isInBoxingRing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player1
     *
     * @return
     */
    public boolean isInBoxingRing(Player player1) {
        return isInRing(player1) && (getCurrentRoom(player1).getRingType() == 0);
    }

    /**
     * Method isInFencingRing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player1
     *
     * @return
     */
    public boolean isInFencingRing(Player player1) {
        return isInRing(player1) && (getCurrentRoom(player1).getRingType() == 1);
    }

    private RoomConfig getCurrentRoom(Player player) {
       try {
           return house_layout[player.getHeight()][player.getKnownLocalX() / 8][player.getKnownLocalY() / 8];
       }catch (Exception err) {
           return null;
       }
    }

    /**
     * Method enterRing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param node
     * @param player
     */
    public void enterRing(ObjectNode node, final Player player) {
        int           transformX = 0,
                      transformY = 0;
        final boolean inRing     = isInRing(player);

        if (!isInRing(player)) {
            switch (getCurrentRoom(player).getRingType()) {
            case 1 :
                for (int i = 0; i < 14; i++) {
                    if (player.getEquipment().isEquipped(i)) {
                        if (i != 3) {
                            player.getActionSender().sendMessage("Your only allowed weapons in this ring.");

                            return;
                        }
                    }
                }

                break;

            case -1 :
                return;

            case 0 :
                for (int i = 0; i < 14; i++) {
                    if (player.getEquipment().isEquipped(i)) {
                        if ((i == 3)
                                && ((player.getEquipment().getId(3) != 7671)
                                    && (player.getEquipment().getId(3) != 7673))) {
                            player.getActionSender().sendMessage(
                                "Your only allowed to wear boxing gloves in the boxing ring.");

                            return;
                        }

                        if (i != 3) {
                            player.getActionSender().sendMessage(
                                "Your only allowed to wear boxing gloves in the boxing ring.");

                            return;
                        }
                    }
                }

                player.restore_all_stats();
                player.getAccount().set_action_disabled(Account.PRAYER_DISABLED, true,
                        "You can't use prayer inside a boxing ring.");
                player.getAccount().set_action_disabled(Account.EATING_DISABLED, true,
                        "You can't use food inside a boxing ring");
                player.getAccount().set_action_disabled(Account.SPELLS_DISABLED, true,
                        "You can't use magic inside a boxing ring.");
                player.getPrayerBook().resetAll();
                player.setCurrentHealth(player.getMaxHealth());
                player.getActionSender().sendStat(3);
            }
        }

        switch (node.getObject().getRotation()) {
        case 2 :
            if (inRing) {
                transformX--;
            } else {
                transformX++;
            }

            break;

        case 1 :
            if (inRing) {
                transformY--;
            } else {
                transformY++;
            }

            break;

        case 3 :
            if (inRing) {
                transformY++;
            } else {
                transformY--;
            }

            break;

        case 0 :
            if (inRing) {
                transformX++;
            } else {
                transformX--;
            }
        }

        if (!inRing) {
            player.getGameFrame().getContextMenu().sendPlayerCommand(1, true, "Attack");
        }

        player.setActionsDisabled(true);
        player.getFacingLocation().focus(player.getX() + transformX, player.getY() + transformY);
        player.setActionsDisabled(true);

        final int toX = transformX;
        final int toY = transformY;

        if (inRing) {
            destructRing(player);
        }

        player.teleport(player.getX() + toX, player.getY() + toY, player.getHeight());
        player.setActionsDisabled(false);
    }

    /**
     * Method getObjectRotation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param height
     * @param pla
     *
     * @return
     */
    public int getObjectRotation(int id, int x, int y, int height, Player pla) {
        Point      location = Point.location(x, y, pla.getHeight());
        int        localx   = Point.getLocalX(pla, location);
        int        localy   = Point.getLocalY(pla, location);
        int        room_x   = localx / 8;
        int        room_y   = localy / 8;
        RoomConfig room     = house_layout[pla.getHeight()][room_x][room_y];

        return room.getRoomNode().getObjectNode(id, localx % 8, localy % 8, theme, room).getObject().getRotation();
    }

    /**
     * Method getObjectNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public ObjectNode getObjectNode(int id, int x, int y, int h) {
        int        room_x = x / 8;
        int        room_y = y / 8;
        RoomConfig room   = house_layout[h][room_x][room_y];

        if (house_layout[h][room_x][room_y] == null) {
            Logger.err("invalid object node");

            return null;
        }

        return house_layout[h][room_x][room_y].getRoomNode().getObjectNode(id, x % 8, y % 8, theme, room);
    }

    /**
     * Method on_object_click
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param id
     * @param x
     * @param y
     * @param slot
     */
    public void on_object_click(Player pla, int id, int x, int y, int slot) {
        Point      location = Point.location(x, y, pla.getHeight());
        int        localx   = Point.getLocalX(pla, location);
        int        localy   = Point.getLocalY(pla, location);
        int        room_x   = localx / 8;
        int        room_y   = localy / 8;
        RoomConfig room     = house_layout[pla.getHeight()][room_x][room_y];

        if (house_layout[pla.getHeight()][room_x][room_y] == null) {
            pla.getActionSender().sendMessage("This object isn't in a configured room: " + room_x + " " + room_y);

            return;
        }

        final ObjectNode node = house_layout[pla.getHeight()][room_x][room_y].getRoomNode().getObjectNode(id,
                                    localx % 8, localy % 8, theme, room);

        if (node == null) {
            pla.getActionSender().sendMessage("Error invalid node: " + id + " " + x + " " + y);

            return;
        }

        switch (slot) {
        case 1 :
        case 2 :
        case 3 :
            pla.getAccount().setSetting("room_c_x", room_x);
            pla.getAccount().setSetting("room_c_y", room_y);
            pla.getAccount().setSetting("node_id", node.getId());
            pla.getAccount().setSetting("obj_id", node.getObject().getId());

            int obj_id = room.getBuilt(node.getId());

            if (node.getType() == ObjectNode.TYPE_STAIRS) {
                obj_id = room.getStairsType();
            }

            if ((id == 13129) || (id == 13137) || (id == 13133)) {
                enterRing(node, pla);

                return;
            }

            if (slot == 1) {
                switch (obj_id) {
                case 13392 :
                }
            }

            if (ObjectNode.getCostumeBoxID(obj_id) != -1) {
                if (!pla.getAccount().hasRoomBox(ObjectNode.getCostumeBoxID(obj_id))) {
                    pla.getActionSender().sendMessage(
                        "You don't have any items in here yet, use the items on the box to insert them.");

                    return;
                }

                pla.getWindowManager().showWindow(2156);
                pla.getWindowManager().setCostume(pla.getAccount().getRoomBox(ObjectNode.getCostumeBoxID(obj_id)));
                pla.getAccount().getRoomBox(ObjectNode.getCostumeBoxID(obj_id)).render(player);

                return;
            }

            if (ObjectNode.isChair(obj_id)) {
                sitOnChar(pla, node, obj_id);

                return;
            }

            if (obj_id == -1) {
                obj_id = id;
            }

            ScriptVariableInjector injector = new ScriptVariableInjector() {
                @Override
                public void injectVariables(Scope scope) {
                    scope.setAttachement(node);
                }
            };

            if (!World.getWorld().getScriptManager().directTrigger(pla, injector, Trigger.CONST_OBJECT_CLICK, obj_id, slot)) {
                pla.getActionSender().sendMessage("This feature is not yet available in construction ["+obj_id+"] ["+slot+"].");

                return;
            }

            break;

        case 5 :
            pla.getAccount().setSetting("room_c_x", room_x);
            pla.getAccount().setSetting("room_c_y", room_y);
            pla.getAccount().setSetting("node_id", node.getId());
            pla.getAccount().setSetting("obj_id", node.getObject().getId());

            if (node.getType() == ObjectNode.TYPE_STAIRS) {
                if (room.getStairsType() <= 0) {
                    Dialog.printStopMessage(pla, "There is no staircase here.", "Make one first.");

                    return;
                } else if (!isBuildingMode()) {
                    Dialog.printStopMessage(pla, "Your not in building mode.");

                    return;
                }

                SignedBlock control_panel = World.getWorld().getScriptManager().get_block("staircase_remove_room");

                if (control_panel != null) {
                    Scope script = new Scope();

                    script.setRunning(control_panel);
                    script.setAttachement(node);
                    script.setController(pla);
                    pla.getNewScriptQueue().add(script);

                    return;
                }
            }

            break;

        case 4 :
            Logger.log("v-1");

            if (!building_mode) {
                pla.getActionSender().sendMessage("You are not in building mode.");

                return;
            }

            if (node.getType() == ObjectNode.TYPE_DOOR) {
                is_drawing_mode = true;

                int[] transform_x = { -1, 0, 1, 0 };
                int[] transform_y = { 0, 1, 0, -1 };
                int   region_x    = room_x + transform_x[node.getObject().getRotation()];
                int   region_y    = room_y + transform_y[node.getObject().getRotation()];

                this.current_draw_x = region_x;
                this.current_draw_y = region_y;
                this.draw_from_x    = room_x;
                this.draw_from_y    = room_y;

                String goto_ = "room_make";

                if (house_layout[pla.getHeight()][region_x][region_y] != null) {
                    is_drawing_mode = false;

                    if ((house_layout[pla.getHeight() + 1][region_x][region_y] != null) && (pla.getHeight() == 1)) {
                        Dialog.printStopMessage(player, "You can't remove a room that's supporting another room");

                        return;
                    }

                    if (house_layout[pla.getHeight()][region_x][region_y].getRoom() == 9) {
                        for (int i = 0; i < 10; i++) {
                            if (pla.getAccount().hasRoomBox(i)) {
                                Dialog.printStopMessage(player, "You still have items inside the furniture in here.",
                                                        "Please remove them first.");

                                return;
                            }
                        }
                    }

                    if ((house_layout[pla.getHeight()][region_x][region_y].getStairsType() > 0)
                            && (pla.getHeight() == 1)) {
                        int c = get_stair_count(pla.getHeight()) - 1;

                        if (c <= 0) {
                            Dialog.printStopMessage(pla, "You can't remove your only staircase",
                                                    "to the ground floor.");

                            return;
                        }
                    }

                    if ((house_layout[pla.getHeight()][region_x][region_y].getRoomNode().getType() == RoomNode
                            .TYPE_GARDEN) && house_layout[pla.getHeight()][region_x][region_y].containsNode(13405)) {
                        if (!check_garden_removal()) {
                            Dialog.printStopMessage(pla, "You must have atleast 1 exit portal to remove this.");

                            return;
                        }
                    }

                    goto_          = "confirm_remove";
                    this.remove_id = ((region_x & 0xFFFF) << 16 | (region_y & 0xFFFF));
                    player.getAccount().setSetting(
                        "remove_name", house_layout[pla.getHeight()][region_x][region_y].getRoomNode().getLabel());
                } else {
                    if (getRoomCount() >= getMaxRooms(pla)) {
                        Dialog.printStopMessage(pla, "You have ran out of rooms", "Prestige to increase this limit.",
                                                "Max rooms: " + getMaxRooms(pla));

                        return;
                    }

                    if ((region_x >= 11) || (region_y >= 11) || (region_x <= 1) || (region_y <= 1)) {
                        Dialog.printStopMessage(player, "Out of bounds.");

                        return;
                    }

                    if (pla.getHeight() > 1) {
                        RoomConfig below = house_layout[pla.getHeight() - 1][region_x][region_y];

                        if ((below == null) || (below.getRoomNode().getType() == RoomNode.TYPE_GARDEN)) {
                            Dialog.printStopMessage(pla, "You need a room below to support this");

                            return;
                        }
                    }

                    this.current_draw_id = 2;
                }

                SignedBlock control_panel = World.getWorld().getScriptManager().get_block(goto_);

                if (control_panel != null) {
                    Scope script = new Scope();

                    script.setRunning(control_panel);
                    script.setAttachement(node);
                    script.setController(pla);
                    pla.getNewScriptQueue().add(script);

                    return;
                }
            }

            if ((room.hasBuilt(node.getId())
                    || ((ObjectNode.getRugType(node.getObject().getId()) > -1) && (room.getRugID() > -1))
                    || ((RoomNode.isPrison(node.getObject().getId()) && (room.getPrisonType() > -1))
                        || ((RoomNode.isFloorType(node.getObject().getId()) && (room.getFloorType() > -1)))
                        || (RoomNode.isRing(node.getObject().getId())
                            && (room.getRingType() > -1))) || (room.getStairsType() == id)
                                || (ObjectNode.isFence(node.getObject().getId()) && (room.getFence() > -1))
                                || (ObjectNode.isHedge(node.getObject().getId())
                                    && (room.getHedge() > -1))) && building_mode) {
                pla.getAccount().setSetting("node_id", node.getId());
                pla.getAccount().setSetting("remove_r_x", room_x);
                pla.getAccount().setSetting("remove_r_y", room_y);

                if (ObjectNode.getCostumeBoxID(room.getBuilt(node.getId())) > 0) {
                    if (pla.getAccount().hasRoomBox(ObjectNode.getCostumeBoxID(room.getBuilt(node.getId())))) {
                        Dialog.printStopMessage(player, "This storage unit still has items in it",
                                                "Please empty it first.");

                        return;
                    }
                }

                if (node.getObject().getId() == 13405) {
                    if (!check_garden_removal()) {
                        Dialog.printStopMessage(player, "You can't remove your only exit portal.");

                        return;
                    }
                }

                Logger.log("removing");

                SignedBlock block  = World.getWorld().getScriptManager().get_block("remove_confirm");
                Scope       script = new Scope();

                script.setRunning(block);
                script.setAttachement(node);
                script.setController(pla);
                pla.getNewScriptQueue().add(script);

                return;
            }

            Logger.log("eh ???");

            int type  = ObjectNode.getRugType(node.getObject().getId());
            int type2 = -1;

            if (type != -1) {
                type2 = 9090;
            }

            if (RoomNode.isRing(node.getObject().getId())) {
                type2 = 301;
            }

            if (RoomNode.isFloorType(id)) {
                type2 = 331;
            }

            if (RoomNode.isPrison(id)) {
                type2 = 441;
            }

            if (ObjectNode.isFence(id)) {
                type2 = 443;
            }

            if (ObjectNode.isHedge(id)) {
                type2 = 444;
            }

            Trigger trigger = World.getWorld().getScriptManager().getTrigger(Trigger.BUILD, (type2 != -1)
                    ? type2
                    : node.getObject().getId());


            if ((trigger != null) &&!pla.isTriggerAlreadyRunning(trigger)) {
                Scope script = new Scope();

                script.setRunning(trigger);
                script.setAttachement(node);
                script.setController(pla);
                pla.getNewScriptQueue().add(script);
                pla.getAccount().setSetting("node_id", node.getId());
                pla.getAccount().setSetting("make_room_x", room_x);
                pla.getAccount().setSetting("make_room_y", room_y);

                return;
            } else {
                pla.getActionSender().sendMessage("Something went wrong, contact the dev team");
                pla.getActionSender().sendMessage("message: not defined code: " + node.getObject().getId());

                // pla.getActionSender().sendMessage("Undefined: " + node.getObject().getId());
            }

            break;
        }
    }

    /**
     * Method add_room
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param h
     * @param x
     * @param y
     * @param id
     * @param rotation
     */
    public void add_room(int h, int x, int y, int id, int rotation) {
        house_layout[h][x][y] = new RoomConfig(id, rotation);
    }

    /**
     * Method render_build_room
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pending_id
     * @param location_x
     * @param location_y
     * @param rotation
     */
    public void render_build_room(int pending_id, int location_x, int location_y, int rotation) {
        RoomNode r = RoomNode.ROOM_NODE_MAP[pending_id][rotation];
    }

    private boolean[] getSurrounding(int h, int x, int y) {
        boolean[] surround    = { true, true, true, true };
        int[]     transform_x = { -1, 0, 1, 0 };
        int[]     transform_y = { 0, 1, 0, -1 };

        for (int i = 0; i < surround.length; i++) {
            int room_x = x + transform_x[i];
            int room_y = y + transform_y[i];

            if ((room_x < 0) || (room_x > 12) || (room_y > 12) || (room_y < 0)) {
                surround[i] = false;
            } else if ((house_layout[h][room_x][room_y] == null)
                       || (house_layout[h][room_x][room_y].getRoomNode().getType() == RoomNode.TYPE_GARDEN)) {
                surround[i] = false;

                if (house_layout[h][x][y] == null) {
                    throw new NullPointerException("boobies");
                }

                if ((RoomNode.getNode(house_layout[h][x][y].getRoom(), house_layout[h][x][y].getRotation()).getType()
                        != RoomNode.TYPE_ROOM)) {
                    surround[i] = true;
                }
            }
        }

        return surround;
    }

    /**
     * Method on_unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public void on_unlink(Player plr) {
        destructHouse(plr);
        players.remove(plr);
    }

    /**
     * Method build_upstairs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean build_upstairs(int id) {
        int local_x = player.getKnownLocalX();
        int local_y = player.getKnownLocalY();

        return build_room_above(id, player.getKnownLocalX() / 8, player.getKnownLocalY() / 8, player.getHeight());
    }

    /**
     * Method teleCage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void teleCage(Player player) {
        for (int x = 0; x < 13; x++) {
            for (int y = 0; y < 13; y++) {
                if ((house_layout[0][x][y] != null) && (house_layout[0][x][y].getRoom() == 15)) {
                    Point p = translate(x, y, 0, 3, 3);

                    player.teleport(p.getX(), p.getY(), 0);

                    return;
                }
            }
        }
    }

    /**
     * Method re_render
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param house
     */
    public static void re_render(Player player, House house) {
        player.getActionSender().sendConstructionStatus(house.building_mode
                ? 1
                : 5);

        int x = player.getX();
        int y = player.getY();
        int h = player.getHeight();

        player.is_con = false;
        player.getActionSender().sendMapArea();
        house.construct_house(player);
        player.setLocation(Point.location(x, y, h), true);
        player.setMapAreaChanged(true);
        player.setDidTeleport(true);
        player.getActionSender().sendConstructionStatus(0);
    }

    /**
     * Method trap_trigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param location
     *
     * @return
     */
    public boolean trap_trigger(Player player, Point location) {
        int localX = getLocalX(location.getX());
        int localY = getLocalY(location.getY());

        if (house_layout[player.getHeight()][localX / 8][localY / 8] != null) {
            ObjectNode node = house_layout[player.getHeight()][localX / 8][localY / 8].getRoomNode().getAbs(localX % 8,
                                  localY % 8, 22);

            if (node == null) {
                return false;
            }

            if (node.getType() == ObjectNode.TYPE_TRAP) {
                if (house_layout[player.getHeight()][localX / 8][localY / 8].getBuilt(node.getId()) > 0) {
                    if (World.getWorld().getScriptManager().directTrigger(player, null, Trigger.CONST_TRAP_TRIGGERED,
                            house_layout[player.getHeight()][localX / 8][localY / 8].getBuilt(node.getId()))) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Method build_downstairs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     */
    public void build_downstairs(int id) {
        build_room_below(id, player.getKnownLocalX() / 8, player.getKnownLocalY() / 8, player.getHeight());
    }

    /**
     * Method build_room_above
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public boolean build_room_above(int id, int x, int y, int h) {
        RoomConfig config = house_layout[h][x][y];

        if (config == null) {
            return false;
        }

        house_layout[h + 1][x][y] = new RoomConfig(id, config.getRotation());
        house_layout[h + 1][x][y].setStairs(config.getStairsType() + 1);
        construct_object_mappings();
        reload();

        return true;
    }

    /**
     * Method build_room_below
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     * @param h
     *
     * @return
     */
    public boolean build_room_below(int id, int x, int y, int h) {
        RoomConfig config = house_layout[h][x][y];

        if (config == null) {
            return false;
        }

        house_layout[h - 1][x][y] = new RoomConfig(id, config.getRotation());
        house_layout[h - 1][x][y].setStairs(config.getStairsType() - 1);
        construct_object_mappings();
        reload();

        return true;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method withinLength
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param coord
     *
     * @return
     */
    public boolean withinLength(int coord) {
        return (coord >= (3 - HOUSE_AREA_LENGTH / 2)) && (coord <= (3 + HOUSE_AREA_LENGTH / 2));
    }

    /**
     * Method withinWidth
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param coord
     *
     * @return
     */
    public boolean withinWidth(int coord) {
        return (coord >= (3 - HOUSE_AREA_WIDTH / 2)) && (coord <= (3 + HOUSE_AREA_LENGTH / 2));
    }

    /**
     * Method construct_house
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void construct_house(Player pla) {
        long start = System.currentTimeMillis();
        pla.getIoSession().setMaxOutput(5000);

        pla.setCurDeathHandler(HOUSE_DEATH_HANDLER);

        if (player == pla) {
            pla.getActionSender().sendBConfig(944, pla.getAccount().getHouseRoomCount());
        }

        int      tile_offset_y = 0;
        int      tile_offset_x = 0;
        Palette  pallette      = new Palette();
        RoomNode curNode       = null;

        if (isBuildingMode()) {
            pla.getActionSender().sendVarBit(2176, 1);
        } else {
            pla.getActionSender().sendVarBit(2176, 0);
        }

        for (int h = 0; h < HOUSE_HEIGHT; h++) {
            for (int x = 0; x < HOUSE_WIDTH; x++) {
                for (int y = 0; y < HOUSE_LENGTH; y++) {
                    if ((x == 0) || (y == 0) || (y == 12) || (x == 12) || (x == 1) || (y == 1) || (x == 11)
                            || (y == 11)) {
                        continue;
                    }

                    if ((house_layout[h][x][y] == null) && (h == 1)) {
                        pallette.setTile(tile_offset_x + x, tile_offset_y + y, 1,
                                         new Palette.PaletteTile(1871 + ((theme > 3)
                                ? 64
                                : 0), ((theme > 3)
                                       ? 0
                                       : 0) + 5056, theme & 3, 0, (((theme != 3) && (theme != 2))
                                ? 0
                                : 0)));
                    } else if ((house_layout[h][x][y] == null) && (h == 0)) {

                        pallette.setTile(tile_offset_x + x, tile_offset_y + y, 0,
                                new Palette.PaletteTile(1944, 5056, 0, 0, 0));

                    } else if (house_layout[h][x][y] != null) {
                        curNode = RoomNode.getNode(house_layout[h][x][y].getRoom(),
                                                   house_layout[h][x][y].getRotation());

                        int id = house_layout[h][x][y].getRoom();

                        if ((id >= 15) && (id <= 19)) {
                            pallette.setTile(tile_offset_x + x, tile_offset_y + y, h,
                                             new Palette.PaletteTile(curNode.getPaletteX(), curNode.getPaletteY(), 0,
                                                 house_layout[h][x][y].getRotation(), floorColor));
                        } else {
                            if (house_layout[h][x][y].getRoomNode().getType() == RoomNode.TYPE_GARDEN) {
                                pallette.setTile(tile_offset_x + x, tile_offset_y + y, h,
                                                 new Palette.PaletteTile(curNode.getPaletteX() + ((theme > 3)
                                        ? 64
                                        : 0), curNode.getPaletteY() + ((theme > 3)
                                        ? 0
                                        : 0), theme & 3, house_layout[h][x][y].getRotation(),
                                              (((theme != 3) && (theme != 2))
                                               ? 0
                                               : 0)));
                            } else {
                                pallette.setTile(tile_offset_x + x, tile_offset_y + y, h,
                                                 new Palette.PaletteTile(curNode.getPaletteX() + ((theme > 3)
                                        ? 64
                                        : 0), curNode.getPaletteY() + ((theme > 3)
                                        ? 0
                                        : 0), theme & 3, house_layout[h][x][y].getRotation(), floorColor));
                            }
                        }
                    }

                    if ((h < 3) && (house_layout[h + 1][x][y] == null) && (house_layout[h][x][y] != null)
                            && (RoomNode.getNode(
                                house_layout[h][x][y].getRoom(),
                                house_layout[h][x][y].getRotation()).getType() == RoomNode.TYPE_ROOM) && (h != 0)) {
                        pallette.setTile(tile_offset_x + x, tile_offset_y + y, h + 1,
                                         new Palette.PaletteTile(1868 + ((theme > 3)
                                ? 64
                                : 0), 5076, theme & 3));
                    }
                }
            }
        }

        pla.getActionSender().sendConstructMapRegion(pallette);
        pla.is_con = true;
        pla.getAccount().setSetting("house_load_x", player.getX());
        pla.getAccount().setSetting("house_load_y", player.getY());
        pla.setKnownRegion(pla.getLocation().getRegionX(), pla.getLocation().getRegionY());

        final boolean[][][] rendered = new boolean[4][13 * 8][13 * 8];

        for (int h = 0; h < HOUSE_HEIGHT; h++) {
            for (int x = 0; x < HOUSE_WIDTH; x++) {
                for (int y = 0; y < HOUSE_LENGTH; y++) {
                    if (house_layout[h][x][y] != null) {
                        RoomNode.getNode(house_layout[h][x][y].getRoom(),
                                         house_layout[h][x][y].getRotation()).render_objects(pla, h, x, y, theme,
                                             building_mode, house_layout[h][x][y], getSurrounding(h, x, y), rendered);
                    }
                }
            }
        }    // To change body of implemented methods use File | Settings | File Templates.

        long end = System.currentTimeMillis() - start;
        pla.getIoSession().setMaxOutput(PlaySession.MAX_OUTPUT_PER_TICK);
    }

    private void clearNpcs() {
        for (NPC npc : roamingNpcs) {
            npc.unlink();
        }

        roamingNpcs.clear();
    }

    /**
     * Method spawnNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void spawnNpcs() {
        clearNpcs();

        for (int i = 0; i < 13; i++) {
            for (int k = 0; k < 13; k++) {
                if (house_layout[0][i][k] != null) {
                    if ((house_layout[0][i][k].getRoom() >= 15) && (house_layout[0][i][k].getRoom() <= 19)) {
                        if (house_layout[0][i][k].getRoom() == 15) {
                            if (house_layout[0][i][k].getFloorType() == 1) {
                                int[] vectorX = { 2, 5 };
                                int[] vectorY = { 2, 5 };

                                for (int x = 0; x < 2; x++) {
                                    for (int y = 0; y < 2; y++) {
                                        Point p   = translate(i, k, 0, vectorX[x], vectorY[y]);
                                        NPC   npc = new NPC(3580, p, 0);

                                        World.getWorld().registerNPC(npc);
                                        npc.setCurrentInstance(this);
                                        roamingNpcs.add(npc);
                                    }
                                }
                            }
                        }

                        for (int ii = 0; ii < house_layout[0][i][k].getRoomNode().getNodeList().length; ii++) {
                            if ((house_layout[0][i][k].getRoomNode().getNode(ii) != null)
                                    && (house_layout[0][i][k].getRoomNode().getNode(ii).getType()
                                        == ObjectNode.TYPE_MONSTER)) {
                                if (house_layout[0][i][k].getBuilt(ii) >= 0) {
                                    int   npcId  = ObjectNode.npcFromObj(house_layout[0][i][k].getBuilt(ii));
                                    int   localx = house_layout[0][i][k].getRoomNode().getNode(ii).getRegionX();
                                    int   localy = house_layout[0][i][k].getRoomNode().getNode(ii).getRegionY();
                                    Point p      = translate(i, k, 0, localx, localy);

                                    if (npcId == -1) {
                                        System.err.println("Npc is null "+house_layout[0][i][k].getBuilt(ii));
                                        continue;
                                    }

                                    NPC npc = new NPC(npcId, p, 6);

                                    npc.setCurrentInstance(this);
                                    World.getWorld().registerNPC(npc);
                                    roamingNpcs.add(npc);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        if (!player.isInArea(HOUSE_ZONE) ||!player.isLoggedIn()) {
            destruct();

            return;
        }



        if (butler != null) {
            butler.tick();
        }

        RoomConfig curRoom = null;

        for (Player player1 : players) {
            if (player1.getCurrentHouse() != this) {
                continue;
            }

            curRoom = getCurrentRoom(player1);

            if ((curRoom != null) && (curRoom.getRoom() == 15) &&!building_mode) {
                if (isinCenter(player1)) {
                    if (curRoom.getFloorType() == 2) {
                        player1.hit(player1, GameMath.rand3(5), 8, 0);
                    } else if ((curRoom.getFloorType() == 1) && (Core.currentTime % 3 == 0)) {
                        player1.hit(player1, GameMath.rand3(5), 8, 0);

                        for (NPC npc : roamingNpcs) {
                            if (npc.getId() == 3580) {
                                npc.getFocus().focus(player1);
                                npc.getAnimation().animate(3618);
                            }
                        }
                    }
                }
            }
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return isDestructed;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
