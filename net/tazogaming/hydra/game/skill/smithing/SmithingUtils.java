package net.tazogaming.hydra.game.skill.smithing;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/07/14
 * Time: 01:53
 */
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;

/**
 * // leeched from dementhium because i cba doing the config XD
 *
 * http://runescape.wikia.com/wiki/Smithing
 *
 * @author 'Mystic Flow <Steven@rune-server.org>
 */
public class SmithingUtils {
    public static final int             SMITHING_INTERFACE = 300;
    public static final Item            HAMMER             = Item.forId(237);
    public static final int[]           CHILD_IDS          = new int[30];
    public static final int[]           HIDDEN_CHILD_IDS   = {
        65, 81, 89, 97, 161, 169, 209, 266
    };
    public static final int[]           CLICK_OPTIONS      = { 28, 32767, 5, 1 };
    private static Map<String, Integer> levelThreshold     = new HashMap<String, Integer>();

    static {
        int counter = 18;

        for (int i = 0; i < CHILD_IDS.length; i++) {
            if (counter == 250) {
                counter = 267;
            }

            CHILD_IDS[i] = counter;
            counter      += 8;
        }

        levelThreshold.put("dagger", 1);
        levelThreshold.put("hatchet", 2);
        levelThreshold.put("mace", 2);
        levelThreshold.put("bolts", 3);
        levelThreshold.put("med helm", 3);
        levelThreshold.put("sword", 4);
        levelThreshold.put("dart tip", 4);
        levelThreshold.put("nails", 4);
        levelThreshold.put("wire", 4);
        levelThreshold.put("arrowtips", 5);
        levelThreshold.put("pickaxe", 5);
        levelThreshold.put("scimitar", 5);
        levelThreshold.put("longsword", 6);
        levelThreshold.put("limbs", 6);
        levelThreshold.put("knife", 7);
        levelThreshold.put("full helm", 7);
        levelThreshold.put("sq shield", 8);
        levelThreshold.put("warhammer", 9);
        levelThreshold.put("battleaxe", 10);
        levelThreshold.put("chainbody", 11);
        levelThreshold.put("kiteshield", 12);
        levelThreshold.put("claws", 13);
        levelThreshold.put("2h sword", 14);
        levelThreshold.put("plateskirt", 16);
        levelThreshold.put("platelegs", 16);
        levelThreshold.put("platebody", 18);
        levelThreshold.put("iron spit", 2);
        levelThreshold.put("oil lantern", 11);
        levelThreshold.put("grapple tip", 9);
        levelThreshold.put("studs", 6);
        levelThreshold.put("bullseye lantern", 19);
    }

    public enum ForgingBar {
        BRONZE(2349, 0, new int[] {
            1205, 1351, 1422, 1139, 877, 1277, 819, 4819, 1794, -1, -1, 39, 1321, 9420, 1291, 864, 1155, 1173, -1, -1,
            1337, 1375, 1103, 1189, 3095, 1307, 1087, 1075, 1117, 1265
        }, new double[] { 12.5, 25, 37.5, 62.5 }, new int[] { 66, 82, 210, 267 }), IRON(2351, 15, new int[] {
            1203, 1349, 1420, 1137, 9377, 1279, 820, 4820, -1, 7225, -1, 40, 1323, 9423, 1293, 863, 1153, 1175, 4540,
            -1, 1335, 1363, 1101, 1191, 3096, 1309, 1081, 1067, 1115, 1267
        }, new double[] { 25, 50, 75, 125 }, new int[] { 66, 90, 162, 210, 267 }), STEEL(2353, 30, new int[] {
            1207, 1353, 1424, 1141, 9378, 1281, 821, 1539, -1, -1, 2370, 41, 1325, 9425, 1295, 865, 1157, 1177, 4544,
            -1, 1339, 1365, 1105, 1193, 3097, 1311, 1083, 1069, 1119, 1269
        }, new double[] { 37.5, 75, 112.5, 187.5 }, new int[] { 66, 98, 162, 210, 267 }), MITHRIL(2359, 50, new int[] {
            1209, 1355, 1428, 1143, 9379, 1285, 822, 4822, -1, -1, -1, 42, 1329, 9427, 1299, 866, 1159, 1181, -1, 9416,
            1343, 1369, 1109, 1197, 3099, 1315, 1085, 1071, 1121, 1273
        }, new double[] { 50, 100, 150, 250 }, new int[] { 66, 170, 210, 267 }), ADAMANT(2361, 70, new int[] {
            1211, 1357, 1430, 1145, 9380, 1287, 823, 4823, -1, -1, -1, 43, 1331, 9429, 1301, 867, 1161, 1183, -1, -1,
            1345, 1371, 1111, 1199, 3100, 1317, 1091, 1073, 1123, 1271
        }, new double[] { 62.5, 125, 187.5, 312.5 }, new int[] { 66, 210, 267 }), RUNE(2363, 85, new int[] {
            1213, 1359, 1432, 1147, 9381, 1289, 824, 4824, -1, -1, -1, 44, 1333, 9431, 1303, 868, 1163, 1185, -1, -1,
            1347, 1373, 1113, 1201, 3101, 1319, 1093, 1079, 1127, 1275
        }, new double[] { 75, 150, 225, 375 }, new int[] { 66, 210, 267 });

        private static Map<Integer, ForgingBar> smithingBars = new HashMap<Integer, ForgingBar>();

        static {
            for (ForgingBar bar : ForgingBar.values()) {
                smithingBars.put(bar.barId, bar);
            }
        }

        private int      barId;
        private int[]    activatedChildren;
        private int[]    items;
        private double[] experience;    // dont know what to name this lol
        private int      baseLevel;

        /**
         * Constructs ...
         *
         *
         * @param barId
         * @param baseLevel
         * @param items
         * @param experience
         * @param activatedChildren
         */
        private ForgingBar(int barId, int baseLevel, int[] items, double[] experience, int[] activatedChildren) {
            this.barId             = barId;
            this.baseLevel         = baseLevel;
            this.items             = items;
            this.experience        = experience;
            this.activatedChildren = activatedChildren;
        }

        /**
         * Method getItems
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int[] getItems() {
            return items;
        }

        /**
         * Method getBaseLevel
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getBaseLevel() {
            return baseLevel;
        }

        /**
         * Method getBarId
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getBarId() {
            return barId;
        }

        /**
         * Method getExperience
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public double[] getExperience() {
            return experience;
        }

        /**
         * Method forId
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @param id
         *
         * @return
         */
        public static ForgingBar forId(int id) {
            return smithingBars.get(id);
        }
    }

    /**
     * Method itemOnObjectInteraction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param itemUsed
     * @param objectId
     *
     * @return
     */
    public static boolean itemOnObjectInteraction(Player player, int itemUsed, int objectId) {
        ForgingBar bar = ForgingBar.forId(itemUsed);

        if ((objectId != 2783) && (objectId != 2572)) {
            return false;
        }

        if (itemUsed == SmithingUtils.HAMMER.getIndex()) {
            player.getActionSender().sendMessage("To forge items use the metal you wish to work with the anvil.");

            return true;
        }

        if (bar != null) {
            if (player.getCurStat(13) < bar.getBaseLevel()) {
                player.getActionSender().sendMessage("You need a Smithing level of at least " + bar.getBaseLevel()
                        + " to work " + bar.toString().toLowerCase() + " bars.");

                return true;
            }

            SmithingUtils.activateChildren(player, bar);

            for (int i = 0; i < bar.getItems().length; i++) {
                if (bar.getItems()[i] != -1) {
                    player.getActionSender().sendItemOnInterface(player, SmithingUtils.SMITHING_INTERFACE,
                            SmithingUtils.CHILD_IDS[i], SmithingUtils.getItemAmount(bar.getItems()[i]),
                            bar.getItems()[i]);

                    String[] name = SmithingUtils.getNameForBar(player, bar, i, bar.getItems()[i]);

                    if (name != null) {
                        player.getGameFrame().sendString(Text.ucFirst(name[0]), 300, SmithingUtils.CHILD_IDS[i] + 1);
                        player.getGameFrame().sendString(Text.ucFirst(name[1]), 300, SmithingUtils.CHILD_IDS[i] + 2);
                    }
                } else {
                    player.getActionSender().sendItemOnInterface(player, SmithingUtils.SMITHING_INTERFACE,
                            SmithingUtils.CHILD_IDS[i], 1, 1);
                }
            }

            player.getAccount().setAttribute("forgingBar", bar);
            player.getGameFrame().sendString(Text.ucFirst(bar.toString()) + " Smithing", 300, 14);
            player.getGameFrame().openWindow(SMITHING_INTERFACE);

            return true;
        }

        return false;
    }

    /**
     * Method getNameForBar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param bar
     * @param index
     * @param itemId
     *
     * @return
     */
    public static String[] getNameForBar(Player player, ForgingBar bar, int index, int itemId) {
        if ((itemId == -1) || (index < 0) || (index >= bar.getItems().length)) {
            return null;
        }

        StringBuilder barName         = new StringBuilder();
        StringBuilder levelString     = new StringBuilder();
        String        name            = Item.forId(itemId).getName().toLowerCase();
        String        barVariableName = bar.toString().toLowerCase();
        int           levelRequired   = bar.baseLevel + getLevelIncrement(bar, bar.getItems()[index]);
        int           barAmount       = getBarAmount(levelRequired, bar, itemId);

        if (player.getInventory().getMaxItem(bar.barId) >= barAmount) {
            barName.append("<col=00FF00>");
        }

        barName.append(barAmount).append(" ").append((barAmount > 1)
                ? "bars"
                : "bar");

        if (levelRequired >= 99) {
            levelRequired = 99;
        }

        if (player.getCurStat(13) >= levelRequired) {
            levelString.append("<col=FFFFFF>");
        }

        levelString.append(name.replace(Text.ucFirst(barVariableName) + " ", ""));

        return new String[] { levelString.toString(), barName.toString() };
    }

    /**
     * Method getBarAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param levelRequired
     * @param bar
     * @param id
     *
     * @return
     */
    public static int getBarAmount(int levelRequired, ForgingBar bar, int id) {
        if (levelRequired >= 99) {
            levelRequired = 99;
        }

        int    level = levelRequired - bar.baseLevel;
        String name  = Item.forId(id).getName().toLowerCase();

        if ((level >= 0) && (level <= 4)) {
            return 1;
        } else if ((level >= 4) && (level <= 8)) {
            if (name.contains("knife") || name.contains("arrowtips") || name.contains("limb")
                    || name.contains("studs")) {
                return 1;
            }

            return 2;
        } else if ((level >= 9) && (level <= 16)) {
            if (name.contains("grapple")) {
                return 1;
            } else if (name.contains("claws")) {
                return 2;
            }

            return 3;
        } else if (level >= 17) {
            if (name.contains("bullseye")) {
                return 1;
            }

            return 5;
        }

        return 1;
    }

    /**
     * Method getLevelIncrement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bar
     * @param id
     *
     * @return
     */
    public static int getLevelIncrement(ForgingBar bar, int id) {
        String name = Item.forId(id).getName().toLowerCase();

        for (Map.Entry<String, Integer> entry : levelThreshold.entrySet()) {
            if (name.contains(entry.getKey())) {
                int increment = entry.getValue();

                if (name.contains("dagger") && (bar != ForgingBar.BRONZE)) {
                    increment--;
                } else if (name.contains("hatchet") && (bar == ForgingBar.BRONZE)) {
                    increment--;
                }

                return increment;
            }
        }


        return 1;
    }

    /**
     * Method getItemAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static int getItemAmount(int id) {
        String name = Item.forId(id).getName();

        if (name.contains("knife")) {
            return 5;
        } else if (name.contains("bolts") || name.contains("dart tip")) {
            return 10;
        } else if (name.contains("arrowtips") || name.contains("nails")) {
            return 15;
        }

        return 1;
    }

    /**
     * Method activateChildren
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param bar
     */
    public static void activateChildren(Player player, ForgingBar bar) {
        for (int i : bar.activatedChildren) {
            player.getGameFrame().sendInterfaceConfig(player, SMITHING_INTERFACE, i - 1, true);
        }
    }
}
