package net.tazogaming.hydra.game.skill.farming.patch;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.player.actions.WoodcuttingAction;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.entity.Tree;
import net.tazogaming.hydra.game.skill.farming.FarmingEntity;
import net.tazogaming.hydra.game.skill.farming.Growing;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/11/13
 * Time: 12:29
 */
public class Patch {
    public static final int
        PHASE_WEEDS_0                     = 0,
        PHASE_WEEDS_1                     = 1,
        PHASE_WEEDS_2                     = 2,
        PHASE_WEEDS_3                     = 3,
        PHASE_READY                       = 4;
    public static final int
        TYPE_ALLOTMENT                    = 0,
        TYPE_NORMAL                       = 2;
    private int         weedTimer         = -1;
    private int         currentId         = 0;
    private boolean     terminated        = false;
    private int         lastInteraction   = Core.currentTime;
    private int         treeHarvestAmount = 10;
    private boolean     treeRemoved       = false;
    private int         chopDownTime      = Core.currentTime;
    private int         compostType       = 0;
    private int         phase;
    private int         playerId;
    private Growing     growing;
    private PatchEntity entity;

    /**
     * Method isTreeRemoved
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTreeRemoved() {
        return treeHarvestAmount == 0;
    }

    /**
     * Method chopLogFromTree
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void chopLogFromTree() {
        treeHarvestAmount--;

        if (treeHarvestAmount == 0) {
            next();
            chopDownTime = Core.currentTime;
        }
    }

    /**
     * Method isTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminated() {
        return terminated;
    }

    /**
     * Method getPlayerId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPlayerId() {
        return playerId;
    }

    /**
     * Method setPlayerId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param playerId
     */
    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    /**
     * Method getGrowing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Growing getGrowing() {
        return growing;
    }

    /**
     * Method setGrowing
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param growing
     */
    public void setGrowing(Growing growing) {
        this.growing = growing;
    }

    /**
     * Method getEntity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PatchEntity getEntity() {
        return entity;
    }

    /**
     * Method setEntity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param entity
     */
    public void setEntity(PatchEntity entity) {
        this.entity = entity;
    }

    /**
     * Method getPhase
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPhase() {
        return phase;
    }

    /**
     * Method setPhase
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param phase
     */
    public void setPhase(int phase) {
        this.phase = phase;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {
        currentId++;
    }

    /**
     * Method getCurrentId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentId() {
        return currentId;
    }

    /**
     * Method getCompostType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCompostType() {
        return 0;
    }

    /**
     * Method isReady
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isReady() {
        return phase == entity.getWeedAmount();
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products onlgy!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void tick() {
        if ((growing != null) && (entity.getType() == PatchEntity.TYPE_TREE) && growing.isComplete()
                && isTreeRemoved()) {
            if (Core.timeSince(chopDownTime) > 350) {
                treeHarvestAmount = 50;
            }

            next();
        }

        if (growing != null) {
            growing.update();
        } else if (isReady() && (Core.timeSince(weedTimer) > 200)) {
            phase = 0;
            next();
        } else if ((growing == null) && (phase == 0) && (Core.timeSince(lastInteraction) > 200)) {
            terminated = true;
        }
    }

    private boolean handleGrowingClick_1(Player player) {
        if (entity.getType() == PatchEntity.TYPE_TREE) {
            if (growing.getPhase() == growing.getEntity().phaseAmount - 1) {
                growing.next();
                player.addXp(19, growing.getEntity().experienceGiven);
                player.getActionSender().sendMessage("Your inspection reveals the tree is in good health.");

                return true;
            } else if (growing.isComplete() &&!isTreeRemoved()) {
                Tree tree = WoodcuttingAction.getTree(growing.getCurrentObjectID());

                if ((tree != null) && (player.getCurrentAction() == null)) {
                    return true;
                }
            }
        }

        if (!growing.isComplete()) {
            if (growing.isDiseased()) {
                player.getActionSender().sendMessage("Your crops are diseased.");

                if (entity.getType() == PatchEntity.TYPE_TREE) {
                    player.getActionSender().sendMessage("You need to cut the mould off with some secateurs.");
                } else {
                    player.getActionSender().sendMessage("You need to cure your crops with some plant cure.");
                }

                return true;
            } else if (growing.isDead()) {
                player.getActionSender().sendMessage("Your crops are dead. All you can do is dig them out now.");

                return true;
            }

            player.getActionSender().sendMessage("Your inspection reveals that these crops will be ready in "
                    + getGrowing().getTimeRemaining() + "");

            return true;
        }

        if (growing.isComplete() && (entity.getType() == PatchEntity.TYPE_FLOWERS)) {
            if (player.getInventory().getFreeSlots(-1) == 0) {
                player.getActionSender().sendMessage("You don't have enough free slots.");

                return true;
            }

            player.addXp(19, getGrowing().getEntity().experienceGiven);
            player.getInventory().addItem(getGrowing().getEntity().harvestedId, 1);
            player.getActionSender().sendMessage("You pick the flowers.");
            growing = null;
            next();

            return true;
        }

        if (growing.isComplete()
                && ((entity.getType() == PatchEntity.TYPE_ALLOTMENT) || (entity.getType() == PatchEntity.TYPE_HERBS))) {
            final PatchEntity theEntity = entity;
            final Growing     growing   = getGrowing();
            final Patch       patch     = this;

            player.terminateActions();
            player.terminateScripts();
            player.getTickEvents().add(new PlayerTickEvent(player, 2, false, false) {
                @Override
                public void doTick(Player owner) {
                    if (owner.getInventory().getFreeSlots(-1) == 0) {
                        owner.getActionSender().sendMessage("You don't have enough free slots.");
                        owner.getAnimation().animate(65535);
                        terminate();

                        return;
                    }

                    if (theEntity.getType() == PatchEntity.TYPE_ALLOTMENT) {
                        owner.getAnimation().animate(830, 0);
                    } else {
                        owner.getAnimation().animate(2279, 0);
                    }

                    if (!growing.pickFromHarvest(owner)) {
                        patch.growing = null;
                        patch.next();
                        patch.weedTimer = Core.currentTime;
                        terminate();
                        owner.getAnimation().animate(65535);
                    }

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
        }

        return false;
    }

    /**
     * Method onInteraction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interactionType
     * @param player
     *
     * @return
     */
    public boolean onInteraction(int interactionType, Player player) {
        lastInteraction = Core.currentTime;

        if (interactionType == 0) {
            if (growing != null) {
                return handleGrowingClick_1(player);
            } else {
                if (!player.getInventory().hasItem(5341, 1)) {
                    player.getActionSender().sendMessage("You don't have a rake.");

                    return true;
                }

                if (isReady()) {
                    player.getActionSender().sendMessage("You've already raked this patch.");

                    return true;
                }

                phase++;
                player.getAnimation().animate(2273, 0);
            }
        }

        // first slot click
        // second slot click
        // interfaces at object
        if (interactionType > 1) {
            int itemId = interactionType;

            if (itemId == 5341) {    // rake
                if (growing != null) {
                    player.getActionSender().sendMessage("You can't rake away growing plants. Use a shovel instead.");

                    return true;
                }

                if (isReady()) {
                    player.getActionSender().sendMessage("You've already raked this patch.");

                    return true;
                }

                phase++;
                player.getAnimation().animate(2273, 0);
                weedTimer = Core.currentTime;
                next();
            } else if ((itemId == 6032) || (itemId == 6034)) {
                if ((growing == null) && isReady()) {
                    if (getCompostType() != 0) {
                        player.getActionSender().sendMessage("You've already added compost to this.");

                        return true;
                    }

                    player.getInventory().deleteItem(itemId, 1);
                    player.getAnimation().animate(2283);
                    player.getActionSender().sendMessage("You add the " + ((itemId == 6032)
                            ? "Compost"
                            : "Supercompost") + " to your plants.");
                    compostType = (itemId == 6032)
                                  ? 1
                                  : 2;

                    return true;
                }
            }

            if (FarmingMap.isWateringCan(interactionType)) {
                if ((entity.getType() == PatchEntity.TYPE_ALLOTMENT)
                        || (entity.getType() == PatchEntity.TYPE_FLOWERS)) {
                    if (growing == null) {
                        return true;
                    }

                    if (growing.isDiseased() || growing.isComplete() || growing.isDead()) {
                        return false;
                    }

                    if (growing.isWatered()) {
                        return false;
                    }

                    if (growing != null) {
                        growing.water();
                        player.getInventory().deleteItem(interactionType, 1);
                        player.getInventory().addItem(FarmingMap.decantWateringCan(interactionType), 1);
                        player.getAnimation().animate(2293);

                        return true;
                    }
                } else {
                    player.getActionSender().sendMessage("You cant water these crops");
                }
            }

            if (itemId == 6036) {
                if ((entity.getType() == PatchEntity.TYPE_ALLOTMENT) || (entity.getType() == PatchEntity.TYPE_FLOWERS)
                        || (entity.getType() == PatchEntity.TYPE_HERBS)) {
                    if (growing != null) {
                        if (growing.isDiseased()) {
                            player.getInventory().deleteItem(6036, 1);
                            growing.cure();
                            player.getAnimation().animate(2288);
                            player.getActionSender().sendMessage("You cure your patch.");

                            return true;
                        } else {
                            player.getActionSender().sendMessage("Your patch isn't diseased.");
                        }
                    }

                    return false;
                }
            }

            if (itemId == 5329) {
                if (entity.getType() == PatchEntity.TYPE_TREE) {
                    if (growing != null) {
                        if (growing.isDiseased()) {
                            player.getAnimation().animate(2276, 0);

                            final Growing g = growing;

                            player.addEventIfAbsent(new PlayerTickEvent(player, 8) {
                                @Override
                                public void doTick(Player owner) {
                                    if (g.isDiseased()) {
                                        g.cure();
                                    }

                                    owner.getAnimation().animate(65535);
                                    terminate();

                                    // To change body of implemented methods use File | Settings | File Templates.
                                }
                            });
                        }
                    }
                }
            }

            if (itemId == 952) {
                if (growing != null) {
                    growing           = null;
                    weedTimer         = Core.currentTime;
                    treeHarvestAmount = 10;
                    player.getAnimation().animate(830);
                    phase = entity.getWeedAmount();
                    next();
                }
            } else {
                if (growing == null) {
                    FarmingEntity e = FarmingEntity.getBySeed(interactionType);

                    if (e == null) {
                        return false;
                    }

                    if (e.type != entity.getType()) {
                        player.getActionSender().sendMessage("You cant plant that here.");

                        return true;
                    }

                    if (!player.getInventory().hasItem(5343, 1) && (entity.getType() != PatchEntity.TYPE_TREE)) {
                        player.getActionSender().sendMessage("You need a seed dibber.");

                        return true;
                    }

                    if (player.getCurStat(19) < e.levelRequired) {
                        player.getActionSender().sendMessage("You need a farming level of atleast " + e.levelRequired
                                + " to plant this.");

                        return true;
                    }

                    player.getAnimation().animate(2286);
                    player.getInventory().deleteItem(e.seedId, 1);
                    player.addXp(19, (int) (e.experienceGiven * 0.2));

                    if (entity.getType() == PatchEntity.TYPE_TREE) {
                        player.getActionSender().sendMessage("You plant your tree.");
                    }

                    if (entity.getType() == PatchEntity.TYPE_FLOWERS) {
                        player.getAnimation().animate(2291);

                        // player.getInventory().deleteItem(interactionType, 1);
                        player.getActionSender().sendMessage("You plant the flowers.");
                    }

                    if (entity.getType() == PatchEntity.TYPE_ALLOTMENT) {
                        player.getAnimation().animate(2291, 0);

                        int siz = entity.getSubPatches().size();

                        if (!player.getInventory().hasItem(interactionType, siz)) {
                            player.getActionSender().sendMessage("You need atleast " + siz + " seeds to plant here.");

                            return true;
                        }

                        player.getInventory().deleteItem(interactionType, siz);
                        player.getActionSender().sendMessage("You plant the seeds.");
                    }

                    this.growing = new Growing(e, this, player);
                    next();
                }
            }
        }

        return false;
    }

    /**
     * Method renderPatch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void renderPatch(Player player) {
        if (entity.getType() == PatchEntity.TYPE_TREE) {
            if ((growing != null) && growing.isComplete()) {
                Point centerPoint = entity.getCenterPoint();

                if (isTreeRemoved()) {
                    player.getActionSender().deleteWorldObject(entity.getPatchX(), entity.getPatchY(), 0);
                    player.getActionSender().sendPlainObject(growing.getCurrentObjectID() + 1, centerPoint.getX(),
                            centerPoint.getY(), 10, 0);

                    return;
                }
            }
        }

        if ((entity.getType() == PatchEntity.TYPE_HERBS) || (entity.getType() == PatchEntity.TYPE_FLOWERS)) {
            if (growing == null) {
                if (!isReady()) {
                    player.getActionSender().sendPlainObject(entity.getWeedsObject(), entity.getPatchX(),
                            entity.getPatchY(), 10, 0);
                } else {
                    player.getActionSender().sendPlainObject(entity.getBlankObject(), entity.getPatchX(),
                            entity.getPatchY(), 10, 0);
                }
            } else {
                player.getActionSender().sendPlainObject(growing.getCurrentObjectID(), entity.getPatchX(),
                        entity.getPatchY(), 10, 3);
            }
        }

        if (entity.getType() == PatchEntity.TYPE_TREE) {
            if (growing == null) {
                if (!isReady()) {
                    player.getActionSender().deleteWorldObject(entity.getCenterPoint().getX(),
                            entity.getCenterPoint().getY(), 0);
                    player.getActionSender().sendPlainObject(entity.getWeedsObject() + phase, entity.getPatchX(),
                            entity.getPatchY(), 10, 0);
                } else {
                    Point centerPoint = entity.getCenterPoint();

                    player.getActionSender().deleteWorldObject(entity.getPatchX(), entity.getPatchY(), 0);
                    player.getActionSender().sendPlainObject(entity.getWeedsObject() + phase, centerPoint.getX(),
                            centerPoint.getY(), 10, 0);
                }
            } else {
                player.getActionSender().sendPlainObject(growing.getCurrentObjectID(), entity.getPatchX(),
                        entity.getPatchY(), 10, 0);

                // player.getActionSender().deleteWorldObject(entity.getPatchX(), entity.getPatchY(), 0);
            }
        } else {
            for (Point p : entity.getSubPatches()) {
                if (growing == null) {
                    player.getActionSender().sendPlainObject(isReady()
                            ? 8573
                            : getWeedsID(p.getX(), p.getY()), p.getX(), p.getY(), 10, 0);
                } else {
                    player.getActionSender().sendPlainObject(growing.getCurrentObjectID(), p.getX(), p.getY(), 10, 0);
                }
            }
        }
    }

    /**
     * Method getWeedsID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param x
     * @param y
     *
     * @return
     */
    public static final int getWeedsID(int x, int y) {
        int id = 8550;

        if (World.getWorld().getTile(x, y, 0).hasMappedObject()) {
            return World.getWorld().getTile(x, y, 0).getMappedObjectId();
        }

        return id;
    }
}
