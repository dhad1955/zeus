package net.tazogaming.hydra.game.skill.farming.patch;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/11/13
 * Time: 13:02
 */
public class FarmingMap implements RecurringTickEvent {
    private static ArrayList<Patch>       active_patches = new ArrayList<Patch>();
    private static ArrayList<PatchEntity> patches        = new ArrayList<PatchEntity>();

    static {
        Logger.log("load farming");

        // load allotment patches
        PatchEntity.loadMultiPatches("config/iskill/farming/subpatches.cfg", patches);

        PatchEntity falador_tree_1 = new PatchEntity();

        /*
         * Tree patches
         */
        falador_tree_1.setPatchX(3003);
        falador_tree_1.setPatchY(3372);
        falador_tree_1.setType(PatchEntity.TYPE_TREE);
        addPatch(falador_tree_1);
        ClippingDecoder.removeClipping(3003, 3372, 0, 4, 4);

        PatchEntity varrock_cy_tree = new PatchEntity();

        varrock_cy_tree.setType(PatchEntity.TYPE_TREE);
        varrock_cy_tree.setPatchX(3228);
        varrock_cy_tree.setPatchY(3458);
        addPatch(varrock_cy_tree);
        ClippingDecoder.removeClipping(3228, 3458, 0, 4, 4);

        PatchEntity tav_donor_patch = new PatchEntity();

        tav_donor_patch.setPatchX(2935);
        tav_donor_patch.setPatchY(3437);
        ClippingDecoder.removeClipping(2935, 3437, 0, 4, 4);
        tav_donor_patch.setType(PatchEntity.TYPE_TREE);
        tav_donor_patch.setDonorOnly(true);
        addPatch(tav_donor_patch);

        /*
         * Herb patches
         */
        PatchEntity draynor_herb_patch = new PatchEntity();

        draynor_herb_patch.setPatchX(3058);
        draynor_herb_patch.setPatchY(3311);
        draynor_herb_patch.setType(PatchEntity.TYPE_HERBS);
        addPatch(draynor_herb_patch);

        PatchEntity ardy_herb_patch = new PatchEntity();

        ardy_herb_patch.setPatchX(2670);
        ardy_herb_patch.setPatchY(3374);
        ardy_herb_patch.setType(PatchEntity.TYPE_HERBS);
        addPatch(ardy_herb_patch);

        PatchEntity port_herb_patch = new PatchEntity();

        port_herb_patch.setType(PatchEntity.TYPE_HERBS);
        port_herb_patch.setPatchX(3605);
        port_herb_patch.setPatchY(3529);
        addPatch(port_herb_patch);

        PatchEntity catherby_herb_patch = new PatchEntity();

        catherby_herb_patch.setType(PatchEntity.TYPE_HERBS);
        catherby_herb_patch.setPatchX(2813);
        catherby_herb_patch.setPatchY(3463);
        addPatch(catherby_herb_patch);

        /*
         *
         */

        /*
         * Flower patches
         */
        PatchEntity draynor_flower_patch = new PatchEntity();

        draynor_flower_patch.setPatchX(3054);
        draynor_flower_patch.setPatchY(3307);
        draynor_flower_patch.setType(PatchEntity.TYPE_FLOWERS);
        addPatch(draynor_flower_patch);

        PatchEntity ardy_flower_patch = new PatchEntity();

        ardy_flower_patch.setPatchX(2666);
        ardy_flower_patch.setPatchY(3374);
        ardy_flower_patch.setType(PatchEntity.TYPE_FLOWERS);
        addPatch(ardy_flower_patch);

        PatchEntity port_flower_patch = new PatchEntity();

        port_flower_patch.setPatchX(3605);
        port_flower_patch.setPatchY(3529);
        port_flower_patch.setType(PatchEntity.TYPE_FLOWERS);
        addPatch(port_flower_patch);

        PatchEntity catherby_flower_patch = new PatchEntity();

        catherby_flower_patch.setPatchX(2809);
        catherby_flower_patch.setPatchY(3463);
        addPatch(catherby_flower_patch);
        Core.submitTask(new FarmingMap());
    }

    /**
     * Method update_patches
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void update_patches() {
        for (Iterator<Patch> patch_iter = active_patches.iterator(); patch_iter.hasNext(); ) {
            Patch patch = patch_iter.next();

            patch.tick();
        }
    }

    /**
     * Method getForPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static Patch getForPlayer(Player pla) {
        for (Patch p : active_patches) {
            if (p.getPlayerId() == pla.getId()) {
                return p;
            }
        }

        return null;
    }

    static private void addPatch(PatchEntity patch) {
        patches.add(patch);
    }

    /**
     * Method onObjectClick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param id
     * @param x
     * @param y
     * @param slot
     *
     * @return
     */
    public static boolean onObjectClick(Player pla, int id, int x, int y, int slot) {
        for (PatchEntity e : patches) {
            if (((e.getPatchX() == x) && (e.getPatchY() == y))
                    || ((e.getCenterPoint().getX() == x) && (e.getCenterPoint().getY() == y))
                    || e.contains(Point.location(x, y, 0))) {
                Patch[] active_patches = getPatchesInView(pla);

                for (Patch p : active_patches) {
                    if ((p != null) && (p.getEntity() == e)) {

                        // handle object click here.
                        p.onInteraction(slot, pla);

                        return true;
                    }
                }

                Patch patch = new Patch();

                patch.setPlayerId(pla.getId());
                patch.setEntity(e);
                patch.renderPatch(pla);
                FarmingMap.active_patches.add(patch);
                patch.onInteraction(slot, pla);

                return true;
            }
        }

        return false;
    }

    /**
     * Method updateForPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void updateForPlayer(Player pla) {

        // iterate through out of range farm patches
        if (pla.getFarmPatches().size() != 0) {
            for (Iterator<Patch> patch_iterator =
                    pla.getFarmPatches().keySet().iterator(); patch_iterator.hasNext(); ) {
                Patch p = patch_iterator.next();

                if (Point.getDistance(p.getEntity().getPatchX(), p.getEntity().getPatchY(), pla.getLocation().getX(),
                                      pla.getLocation().getY()) > 15) {
                    patch_iterator.remove();
                }
            }
        }

        Patch[] patches = getPatchesInView(pla);

        for (int i = 0; i < patches.length; i++) {
            if (patches[i] == null) {
                return;
            }

            if (!pla.getFarmPatches().containsKey(patches[i])
                    || (pla.getFarmPatches().get(patches[i]) < patches[i].getCurrentId())) {
                patches[i].renderPatch(pla);
                pla.getFarmPatches().put(patches[i], patches[i].getCurrentId());
            }
        }
    }

    /**
     * Method getPatchesInView
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public static Patch[] getPatchesInView(Player pla) {
        int     patchCount    = 0;
        Patch[] returnPatches = new Patch[5];

        for (Patch p : active_patches) {
            if ((p.getPlayerId() == pla.getId())
                    && (Point.getDistance(pla.getX(), pla.getY(), p.getEntity().getPatchX(), p.getEntity().getPatchY())
                        < 15)) {
                returnPatches[patchCount++] = p;
            }
        }

        return returnPatches;
    }

    /**
     * Method isWateringCan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static boolean isWateringCan(int id) {
        return (id >= 5332) && (id <= 5340);
    }

    /**
     * Method decantWateringCan
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static int decantWateringCan(int id) {
        return id - 1;
    }

    /**
     * Method getByUID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param x
     * @param y
     *
     * @return
     */
    public static PatchEntity getByUID(int id, int x, int y) {
        for (PatchEntity e : patches) {
            if ((e.getWeedsObject() == id) && (e.getPatchX() == x) && (y == e.getPatchY())) {
                return e;
            }
        }

        return null;
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        update_patches();

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
