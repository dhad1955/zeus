package net.tazogaming.hydra.game.skill.farming.patch;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/11/13
 * Time: 12:43
 */
public class PatchEntity {
    public static final int
        TYPE_ALLOTMENT                  = 1,
        TYPE_HERBS                      = 2,
        TYPE_TREE                       = 3,
        TYPE_BUSH                       = 4,
        TYPE_HOPS                       = 5,
        TYPE_FLOWERS                    = 6;
    private int              patchX     = 0,
                             patchY     = 0,
                             weedamount = 0;
    private int              type       = 0;
    private boolean          donorOnly  = false;
    private ArrayList<Point> subPatches = new ArrayList<Point>();
    private int              weedsObject;

    /**
     * Method loadMultiPatches
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     * @param list_to_Add
     */
    public static void loadMultiPatches(String path, ArrayList<PatchEntity> list_to_Add) {
        File f = new File(path);

        if (!f.exists()) {
            return;
        }

        Logger.log("[Farming]: loading list from " + path);

        HashMap<Integer, ArrayList<Point>> subPatches = new HashMap<Integer, ArrayList<Point>>();
        ArrayList<Point>                     patches    = new ArrayList<Point>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String         line;

            while ((line = br.readLine()) != null) {
                if ((line.length() == 0) || line.startsWith("//")) {
                    continue;
                }

                String[] split = line.split(" ");

                if (split[0].equals("subpatch")) {
                    int              id = Integer.parseInt(split[2]);
                    int              x  = Integer.parseInt(split[3]);
                    int              y  = Integer.parseInt(split[4]);
                    ArrayList<Point> pt = subPatches.get(id);

                    if (pt == null) {
                        pt = new ArrayList<Point>();
                        subPatches.put(id, pt);
                    }

                    pt.add(Point.location(x, y, 0));
                }
            }

            br.close();

            for (int key : subPatches.keySet()) {
                ArrayList<Point> pt       = subPatches.get(key);    // = new ArrayList<SubPatch>();
                PatchEntity      newPatch = new PatchEntity();      // (key, pt);
                Point            st       = pt.get(0);

                newPatch.patchX = st.getX();
                newPatch.type   = TYPE_ALLOTMENT;
                newPatch.patchY = st.getY();
                newPatch.getSubPatches().addAll(pt);
                list_to_Add.add(newPatch);

                // allotmentPatches.add(newPatch);
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method contains
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     *
     * @return
     */
    public boolean contains(Point location) {
        for (Point p : subPatches) {
            if ((p.getX() == location.getX()) && (p.getY() == location.getY())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getWeedAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWeedAmount() {
        return 1;
    }

    /**
     * Method setWeedamount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void setWeedamount(int i) {
        weedamount = i;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Method getPatchX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPatchX() {
        return patchX;
    }

    /**
     * Method setPatchX
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param patchX
     */
    public void setPatchX(int patchX) {
        this.patchX = patchX;
    }

    /**
     * Method getPatchY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPatchY() {
        return patchY;
    }

    /**
     * Method setPatchY
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param patchY
     */
    public void setPatchY(int patchY) {
        this.patchY = patchY;
    }

    /**
     * Method getSubPatches
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Point> getSubPatches() {
        return subPatches;
    }

    /**
     * Method setSubPatches
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param subPatches
     */
    public void setSubPatches(ArrayList<Point> subPatches) {
        this.subPatches = subPatches;
    }

    /**
     * Method getGrowDelay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public final int getGrowDelay() {
        switch (type) {
        case TYPE_HERBS :
            return Core.getTicksForMinutes(3);

        case TYPE_FLOWERS :
            return Core.getTicksForMinutes(3);

        case TYPE_TREE :
            return Core.getTicksForMinutes(8);

        case TYPE_ALLOTMENT :
            return Core.getTicksForMinutes(5);
        }

        return Core.getTicksForMinutes(20);
    }

    /**
     * Method getBlankObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getBlankObject() {
        if (type == TYPE_HERBS) {
            return 8132;
        }

        if (type == TYPE_FLOWERS) {
            return 7840;
        }

        return weedsObject;
    }

    /**
     * Method getWeedsObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getWeedsObject() {
        if (type == TYPE_TREE) {
            return 8391;
        }

        if (type == TYPE_ALLOTMENT) {
            return 8550;
        }

        if (type == TYPE_HERBS) {
            return 8150;
        }

        if (type == TYPE_FLOWERS) {
            return 7847;
        }

        return weedsObject;
    }

    /**
     * Method getCenterPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getCenterPoint() {
        return Point.location(patchX + 1, patchY + 1, 0);
    }

    /**
     * Method setDonorOnly
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setDonorOnly(boolean b) {
        donorOnly = b;
    }

    /**
     * Method setWeedsObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param weedsObject
     */
    public void setWeedsObject(int weedsObject) {
        this.weedsObject = weedsObject;
    }
}
