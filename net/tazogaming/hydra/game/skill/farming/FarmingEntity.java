package net.tazogaming.hydra.game.skill.farming;

//~--- non-JDK imports --------------------------------------------------------



/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/10/13
 * Time: 07:37
 */
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.farming.patch.PatchEntity;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Danny
 * Date: 11/12/12
 * Time: 20:59
 * To change this template use File | Settings | File Templates.
 */
public class FarmingEntity {
    public static final int
        TYPE_ALLOTMENT                                 = 8,
        TYPE_HERBS                                     = 1,
        TYPE_TREE                                      = 2,
        TYPE_HOPS                                      = 3;
    public static ArrayList<FarmingEntity> entityCache = new ArrayList<FarmingEntity>();

    static {
        FarmingEntity potato = new FarmingEntity();

        potato.seedId          = 5318;
        potato.levelRequired   = 0;
        potato.experienceGiven = 9;
        potato.phaseAmount     = 5;
        potato.phaseAmount     = 4;
        potato.type            = 1;
        potato.harvestedId     = 1942;
        potato.type            = PatchEntity.TYPE_ALLOTMENT;
        entityCache.add(potato);
        potato                 = new FarmingEntity();
        potato.seedId          = 5320;
        potato.levelRequired   = 20;
        potato.experienceGiven = 19;
        potato.phaseAmount     = 6;
        potato.type            = PatchEntity.TYPE_ALLOTMENT;
        potato.harvestedId     = 5986;
        potato.type            = 1;
        entityCache.add(potato);

        // t0mat0
        potato                 = new FarmingEntity();
        potato.seedId          = 5322;
        potato.levelRequired   = 12;
        potato.experienceGiven = 14;
        potato.phaseAmount     = 5;
        potato.type            = PatchEntity.TYPE_ALLOTMENT;
        potato.harvestedId     = 1982;
        potato.type            = 1;
        entityCache.add(potato);

        // onion
        potato                 = new FarmingEntity();
        potato.seedId          = 5319;
        potato.levelRequired   = 5;
        potato.experienceGiven = 10;
        potato.phaseAmount     = 4;
        potato.phaseAmount     = 4;
        potato.type            = PatchEntity.TYPE_ALLOTMENT;
        potato.harvestedId     = 1957;
        potato.type            = 1;
        entityCache.add(potato);

        // cabbage
        potato                 = new FarmingEntity();
        potato.seedId          = 5324;
        potato.levelRequired   = 7;
        potato.experienceGiven = 11;
        potato.phaseAmount     = 4;
        potato.type            = PatchEntity.TYPE_ALLOTMENT;
        potato.phaseAmount     = 4;
        potato.harvestedId     = 1965;
        potato.type            = 1;
        entityCache.add(potato);

        // strawberries
        potato                 = new FarmingEntity();
        potato.seedId          = 5323;
        potato.levelRequired   = 31;
        potato.experienceGiven = 29;
        potato.phaseAmount     = 4;
        potato.phaseAmount     = 6;
        potato.harvestedId     = 5504;
        potato.type            = PatchEntity.TYPE_ALLOTMENT;
        potato.type            = 1;
        entityCache.add(potato);

        // water melon
        potato                 = new FarmingEntity();
        potato.seedId          = 5321;
        potato.levelRequired   = 47;
        potato.experienceGiven = 54;
        potato.phaseAmount     = 8;
        potato.harvestedId     = 5982;
        potato.type            = 1;
        entityCache.add(potato);

        /*
         * Trees
         */
        FarmingEntity oak_tree = new FarmingEntity();

        oak_tree.seedId          = 5370;
        oak_tree.type            = PatchEntity.TYPE_TREE;
        oak_tree.phaseAmount     = 5;
        oak_tree.diseasedOffset  = 73;
        oak_tree.plantedOffset   = 8;
        oak_tree.deadOffset      = 141;
        oak_tree.experienceGiven = 467;
        oak_tree.levelRequired   = 15;
        entityCache.add(oak_tree);

        FarmingEntity willow_tree = new FarmingEntity();

        willow_tree.seedId          = 5371;
        willow_tree.plantedOffset   = 15;
        willow_tree.diseasedOffset  = 80;
        willow_tree.type            = PatchEntity.TYPE_TREE;
        willow_tree.phaseAmount     = 7;
        willow_tree.levelRequired   = 30;
        willow_tree.experienceGiven = 1456;
        entityCache.add(willow_tree);

        FarmingEntity maple_tree = new FarmingEntity();

        maple_tree.phaseAmount     = 9;
        maple_tree.seedId          = 5372;
        maple_tree.type            = PatchEntity.TYPE_TREE;
        maple_tree.levelRequired   = 45;
        maple_tree.experienceGiven = 3403;
        entityCache.add(maple_tree);

        FarmingEntity yew_tree = new FarmingEntity();

        yew_tree.phaseAmount     = 10;
        yew_tree.type            = PatchEntity.TYPE_TREE;
        yew_tree.seedId          = 5373;
        yew_tree.experienceGiven = 7069;
        yew_tree.levelRequired   = 60;
        entityCache.add(yew_tree);

        FarmingEntity magic_tree = new FarmingEntity();

        magic_tree.experienceGiven = 13768;
        magic_tree.levelRequired   = 75;
        magic_tree.phaseAmount     = 13;

        // magic_tree.seededId =
        magic_tree.type   = PatchEntity.TYPE_TREE;
        magic_tree.seedId = 5374;
        entityCache.add(magic_tree);

        /*
         * flowers
         */
        FarmingEntity marigolds = new FarmingEntity();

        marigolds.seedId          = 5096;
        marigolds.phaseAmount     = 4;
        marigolds.levelRequired   = 5;
        marigolds.experienceGiven = 55;
        marigolds.harvestedId     = 6011;
        marigolds.type            = PatchEntity.TYPE_FLOWERS;
        entityCache.add(marigolds);

        FarmingEntity rosemary = new FarmingEntity();

        rosemary.phaseAmount     = 4;
        rosemary.harvestedId     = 6014;
        rosemary.seedId          = 5097;
        rosemary.type            = PatchEntity.TYPE_FLOWERS;
        rosemary.experienceGiven = 66;
        rosemary.levelRequired   = 11;
        entityCache.add(rosemary);

        FarmingEntity nasturitum = new FarmingEntity();

        nasturitum.levelRequired   = 24;
        nasturitum.experienceGiven = 130;
        nasturitum.harvestedId     = 6012;
        nasturitum.seedId          = 5098;
        nasturitum.phaseAmount     = 4;
        entityCache.add(nasturitum);

        FarmingEntity woad = new FarmingEntity();

        woad.phaseAmount     = 4;
        woad.type            = PatchEntity.TYPE_FLOWERS;
        woad.levelRequired   = 25;
        woad.harvestedId     = 1793;
        woad.experienceGiven = 136;
        entityCache.add(woad);

        FarmingEntity limpwurt = new FarmingEntity();

        limpwurt.phaseAmount     = 4;
        limpwurt.type            = PatchEntity.TYPE_FLOWERS;
        limpwurt.levelRequired   = 26;
        limpwurt.experienceGiven = 160;
        limpwurt.harvestedId     = 225;
        entityCache.add(limpwurt);

        /*
         * Herbs
         */

        // Guam - Level 9
        addHerb(5291, 9, 11, 12, 199);

//      Marrentill - Level 14
        addHerb(5292, 14, 13, 15, 201);

//      Tarromin - Level 19
        addHerb(5293, 19, 16, 18, 203);

//      Harralander - Level 26
        addHerb(5294, 26, 21, 24, 205);

//      Goutweed - Level 29
        addHerb(6311, 29, 105, 45, 3261);

//      Ranarr Weed - Level 33
        addHerb(5295, 33, 26, 30, 207);

//      Toadflax - Level 38
        addHerb(5296, 38, 34, 38, 3049);

//      Irit Leaf - Level 44
        addHerb(5297, 44, 43, 48, 209);

//      Avantoe - Level 50
        addHerb(5298, 50, 54, 61, 211);

//      Kwuarm - Level 56
        addHerb(5299, 56, 69, 78, 213);

//      Snapdragon - Level 62
        addHerb(5300, 62, 87, 98, 3051);

//      Cadantine - Level 67
        addHerb(5301, 67, 106, 120, 215);

//      Lantadyme - Level 73
//      Dwarf Weed - Level 79
        addHerb(5303, 79, 170, 192, 217);

//      Torstol - Level 85
        addHerb(5304, 85, 199, 224, 219);
        addHerb(5302, 99, 260, 260, 2485);
    }

    public int  levelRequired   = 0;
    public int  experienceGiven = 0;
    public int  growDelay       = 0;
    public int  seedId          = 0;
    public int  prestige_req    = 0;
    public int  phaseAmount     = 0;
    public int  type            = 1;
    public int  harvestedId;
    private int plantedOffset;
    private int wateredOffset;
    private int deadOffset;
    private int diseasedOffset;

    private static final int getDelay(FarmingEntity entity) {
        switch (entity.type) {
        case PatchEntity.TYPE_HERBS :
            return Core.getTicksForMinutes(4);

        case PatchEntity.TYPE_FLOWERS :
            return Core.getTicksForMinutes(3);

        case PatchEntity.TYPE_TREE :
            return Core.getTicksForMinutes(15);

        case PatchEntity.TYPE_ALLOTMENT :
            return Core.getTicksForMinutes(8);
        }

        return Core.getTicksForMinutes(20);
    }

    private static int getSpeed(FarmingEntity e) {
        int delay         = e.growDelay;
        int maxHarvestAmt = 0;
        int patchAmount   = 0;

        switch (e.type) {
        case PatchEntity.TYPE_HERBS :
            maxHarvestAmt = 20;
            patchAmount   = 4;

            break;

        case PatchEntity.TYPE_ALLOTMENT :
            maxHarvestAmt = 22;
            patchAmount   = 4;

            break;
        }

        int amt = getDelay(e) * e.phaseAmount;

        return amt * patchAmount;
    }


    /**
     * Method addHerb
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param requirement
     * @param plantxp
     * @param xp
     * @param harvest
     */
    public static void addHerb(int id, int requirement, int plantxp, int xp, int harvest) {
        FarmingEntity herb = new FarmingEntity();

        herb.phaseAmount     = 4;
        herb.type            = PatchEntity.TYPE_HERBS;
        herb.seedId          = id;
        herb.experienceGiven = xp;
        herb.levelRequired   = requirement;
        herb.harvestedId     = harvest;
        entityCache.add(herb);
    }

    /**
     * Method getBySeed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param seed
     *
     * @return
     */
    public static FarmingEntity getBySeed(int seed) {
        for (FarmingEntity et : entityCache) {
            if (et.seedId == seed) {
                return et;
            }
        }

        return null;
    }
}
