package net.tazogaming.hydra.game.skill.farming;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.skill.farming.patch.Patch;
import net.tazogaming.hydra.game.skill.farming.patch.PatchEntity;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 04/11/13
 * Time: 12:30
 */
public class Growing {
    private static final int GROW_DELAY_TIMER = 0;
    private int              currentPhase     = 0;
    private boolean          isDead           = false;
    private TimingUtility    utility          = new TimingUtility();
    private boolean          isDiseased       = false;
    private int              harvestAmount    = 0;
    private int              boost            = 0;
    private Patch            thePatch;
    private FarmingEntity    currently_growing;
    private int              player_id;
    private boolean          isWatered;

    /**
     * Constructs ...
     *
     *
     * @param e
     * @param thePatch
     * @param player
     */
    public Growing(FarmingEntity e, Patch thePatch, Player player) {
        this.currently_growing = e;
        this.thePatch          = thePatch;
        this.player_id         = player.getId();

        if (player.isTenth(19)) {
            boost = 4;
        }

        utility.setTime(GROW_DELAY_TIMER, getDelay());
    }

    private int getTicksRemaining() {
        if (isComplete()) {
            return 0;
        }

        int base            = utility.getAbsTime(GROW_DELAY_TIMER);
        int delay           = getDelay();
        int cyclesRemaining = (getEntity().phaseAmount - (currentPhase + 1)) * delay;

        base += cyclesRemaining;

        return base;
    }

    /**
     * Method getTimeRemaining
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getTimeRemaining() {
        int minutes = getTicksRemaining() / 100;

        return minutes + " minutes";
    }

    /**
     * Method isDiseased
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDiseased() {
        return isDiseased;
    }

    private int getDelay() {
        return thePatch.getEntity().getGrowDelay();
    }

    /**
     * Method cure
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void cure() {
        isDiseased = false;
        utility.setTime(GROW_DELAY_TIMER, getDelay());
        next();
        thePatch.next();
    }

    /**
     * Method isComplete
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isComplete() {
        return currentPhase == currently_growing.phaseAmount;
    }

    /**
     * Method getPhase
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPhase() {
        return currentPhase;
    }

    /**
     * Method getEntity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public FarmingEntity getEntity() {
        return currently_growing;
    }

    /**
     * Method kill
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void kill() {
        isDead = true;
    }

    /**
     * Method randomDiseaseChance
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean randomDiseaseChance() {
        int min = 700;

        if (isWatered) {
            min -= 200;
        }

        if (thePatch.getCompostType() == 1) {
            min -= 200;
        }

        if (thePatch.getCompostType() == 2) {
            min -= 500;
        }

        if (min < 0) {
            return false;
        }

        return GameMath.rand(1000) > min;
    }

    /**
     * Method isWatered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isWatered() {
        return isWatered;
    }

    /**
     * Method disease
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void disease() {
        this.isDiseased = true;
    }

    /**
     * Method getCurrentObjectID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentObjectID() {
        return 0;
    }

    /**
     * Method water
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void water() {
        isWatered = true;
        thePatch.next();
    }

    /**
     * Method pickFromHarvest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean pickFromHarvest(Player pla) {
        if (harvestAmount == 0) {
            return false;
        }

        harvestAmount--;
        pla.getInventory().addItem(currently_growing.harvestedId, 1);
        pla.addXp(19, currently_growing.experienceGiven);
        pla.addPoints(Points.SKILLING_POINTS, currently_growing.levelRequired / 2);

        return true;
    }

    /**
     * Method setHarvest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void setHarvest() {
        if (thePatch.getEntity().getType() == PatchEntity.TYPE_HERBS) {
            harvestAmount = 15 + GameMath.rand(25);
        } else if (thePatch.getEntity().getType() == PatchEntity.TYPE_ALLOTMENT) {
            harvestAmount = thePatch.getEntity().getSubPatches().size();
        }

        if (boost > 0) {
            harvestAmount *= boost;
        }
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void next() {
        if (currentPhase != currently_growing.phaseAmount) {
            currentPhase++;
            thePatch.next();

            if (currentPhase == currently_growing.phaseAmount) {
                setHarvest();
            }
        }
    }

    /**
     * Method isDead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDead() {
        return isDead;
    }

    /**
     * Method update
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        if ((thePatch.getEntity().getType() == PatchEntity.TYPE_TREE)
                && (currentPhase == currently_growing.phaseAmount - 1)) {
            return;    // health check is required.
        }

        if (!isDead &&!utility.timerActive(GROW_DELAY_TIMER)) {
            if (isDiseased) {
                isDead = true;
                thePatch.next();
                isDiseased = false;

                return;
            }

            if ((currentPhase > 0) &&!isDead &&!isComplete()) {
                isWatered = false;

                if (randomDiseaseChance()) {
                    if ((thePatch.getEntity().getType() != PatchEntity.TYPE_TREE)
                            || ((thePatch.getEntity().getType() == PatchEntity.TYPE_TREE)
                                && (currentPhase != currently_growing.phaseAmount - 1))) {
                        disease();
                    }
                }
            }

            if (!isDiseased) {
                next();
            }

            utility.setTime(GROW_DELAY_TIMER, getDelay());
            thePatch.next();
        }
    }
}
