package net.tazogaming.hydra.game.skill.fishing;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.util.ArgumentTokenizer;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
class Assignment {
    int id;
    int pool1;
    int pool2;
}


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/03/14
 * Time: 16:41
 */
public class FishingAssignments {
    private static ArrayList<Assignment> assignments = new ArrayList<Assignment>();

    /**
     * Method getPool
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param slot
     *
     * @return
     */
    public static int getPool(int id, int slot) {
        for (Assignment a : assignments) {
            if (a.id == id) {
                switch (slot) {
                case 0 :
                    return a.pool1;

                case 1 :
                    return a.pool2;
                }
            }
        }

        return -1;
    }

    /**
     * Method loadAssignments
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void loadAssignments() {
        assignments.clear();

        File f = new File("config/iskill/fishing/assignments.conf");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));

            while (true) {
                String line = reader.readLine();

                if (line == null) {
                    reader.close();

                    break;
                }

                if ((line.length() == 0) || line.startsWith("//")) {
                    continue;
                }

                ArgumentTokenizer parser = new ArgumentTokenizer(line.split(" "));
                Assignment        a      = new Assignment();

                a.id    = parser.nextInt();
                a.pool1 = parser.nextInt();
                a.pool2 = parser.nextInt();
                assignments.add(a);
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }
}
