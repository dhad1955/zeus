package net.tazogaming.hydra.game.skill.fishing;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.util.ArgumentTokenizer;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/03/14
 * Time: 12:57
 */
public class FishPool {
    public static final int
        TYPE_NET                           = 0,
        TYPE_BIG_NET                       = 1,
        TYPE_BAIT                          = 2,
        TYPE_HARPOON                       = 3,
        TYPE_LOB_CAGE                      = 4;
    public static FishPool[] POOLS         = new FishPool[100];
    private int              type          = -1;
    private ArrayList<Fish>  normalFish    = new ArrayList<Fish>();
    private ArrayList<Fish>  rareFish      = new ArrayList<Fish>();
    private ArrayList<Fish>  ultraRareFish = new ArrayList<Fish>();

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getAnimForPool
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pool
     *
     * @return
     */
    public static final int getAnimForPool(int pool) {
        switch (POOLS[pool].getType()) {
        case TYPE_HARPOON :
            return 618;

        case TYPE_LOB_CAGE :
            return 619;

        case TYPE_BIG_NET :
            return 620;

        case TYPE_NET :
            return 621;

        case TYPE_BAIT :
            return 623;
        }

        return 623;
    }

    /**
     * Method checkRequirement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param pool
     *
     * @return
     */
    public static boolean checkRequirement(Player player, int pool) {
        Fish lowest   = null;
        int  highestP = 0;
        int  highestL = 0;

        for (Fish f : POOLS[pool].normalFish) {
            if ((highestL == 0) || ((f.getLevel_requirement() < highestL) && (f.getLevel_requirement() <= highestP))) {
                highestP = f.getPrestigeRequirement();
                highestL = f.getLevel_requirement();
                lowest   = f;
            }
        }

        if (lowest != null) {

            if (lowest.getLevel_requirement() > player.getCurStat(10)) {
                player.getActionSender().sendMessage("You need at least level " + lowest.getLevel_requirement()
                        + " fishing to fish here.");

                return false;
            }
        }

        return true;
    }

    private static Fish randomFish(Player player, ArrayList<Fish> table) {
        ArrayList<Fish> possibleFish = new ArrayList<Fish>();

        for (Fish f : table) {
            if ((f.getLevel_requirement() <= player.getCurStat(10))) {
                possibleFish.add(f);
            }
        }

        if (possibleFish.size() == 0) {
            return null;
        }

        return possibleFish.get(GameMath.rand3(possibleFish.size()));
    }

    /**
     * Method randomFish
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param pool_id
     *
     * @return
     */
    public static Fish randomFish(Player player, int pool_id) {
        FishPool pool = POOLS[pool_id];

        if (pool == null) {
            return null;
        }

        if ((pool.ultraRareFish.size() > 0) && (GameMath.rand3(3000) < 3)) {
            Fish f = randomFish(player, pool.ultraRareFish);

            if (f != null) {
                return f;
            }
        } else if ((pool.rareFish.size() > 0) && (GameMath.rand3(1250) == 5)) {
            Fish f = randomFish(player, pool.rareFish.size());

            if (f != null) {
                return f;
            }
        }

        return randomFish(player, pool.normalFish);
    }

    /**
     * Method check_tools
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param type
     *
     * @return
     */
    public static final boolean check_tools(Player player, int type) {
        switch (type) {
        case TYPE_NET :
            if (!player.getInventory().hasItem(303, 1)) {
                player.getActionSender().sendMessage("You need a small fishing net to fish here.");

                return false;
            }

            return true;

        case TYPE_BIG_NET :
            if (!player.getInventory().hasItem(305, 1)) {
                player.getActionSender().sendMessage("You need a big fishing net to fish here.");

                return false;
            }

            return true;

        case TYPE_HARPOON :
            if (!player.getInventory().hasItem(311, 1) &&!player.getInventory().hasItem(10129, 1)) {
                player.getActionSender().sendMessage("You need a harpoon to fish here.");

                return false;
            }

            return true;

        case TYPE_BAIT :
            if (!player.getInventory().hasItem(307, 1)) {
                player.getActionSender().sendMessage("You need a fishing rod to fish here.");

                return false;
            }

            if (!player.getInventory().hasItem(313, 1)) {
                player.getActionSender().sendMessage("You don't have any bait.");

                return false;
            }

            return true;

        case TYPE_LOB_CAGE :
            if (!player.getInventory().hasItem(301, 1)) {
                player.getActionSender().sendMessage("You need a lobster pot to fish here.");

                return false;
            }

            return true;
        }

        return true;
    }

    /**
     * Method load_fish
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     */
    public static final void load_fish(File location) {
        FishingAssignments.loadAssignments();

        try {
            if (!location.exists()) {
                Logger.err("Unable to load fishing config");
                System.exit(-1);

                return;
            }

            BufferedReader reader  = new BufferedReader(new FileReader(location));
            String         line    = null;
            boolean        reading = false;
            int            lineNo  = -1;
            int            id      = -1;

            do {
                lineNo++;
                line = reader.readLine();

                if (line == null) {
                    break;
                }

                if (line.length() == 0) {
                    continue;
                }

                if (line.startsWith("container")) {
                    if (reading) {
                        throw new RuntimeException("ERROR[Fishing] unable to parse line " + lineNo);
                    }

                    id        = Integer.parseInt(line.split(" ")[1]);
                    POOLS[id] = new FishPool();
                    reading   = true;
                }

                if (line.equalsIgnoreCase("}")) {
                    reading = false;
                } else {
                    if (line.startsWith("//")) {
                        continue;
                    }

                    ArgumentTokenizer tokenizer = new ArgumentTokenizer(line.split(" "));
                    String            operation = tokenizer.nextString();

                    tokenizer.nextString();    // =

                    if (operation.equalsIgnoreCase("type")) {
                        String type = tokenizer.nextString();
                        int    to   = -1;

                        if (type.equalsIgnoreCase("BIG_NET")) {
                            to = TYPE_BIG_NET;
                        } else if (type.equalsIgnoreCase("small_net")) {
                            to = TYPE_NET;
                        } else if (type.equalsIgnoreCase("BAIT")) {
                            to = TYPE_BAIT;
                        } else if (type.equalsIgnoreCase("harpoon")) {
                            to = TYPE_HARPOON;
                        } else if (type.equalsIgnoreCase("lob_cage")) {
                            to = TYPE_LOB_CAGE;
                        }

                        if (to == -1) {
                            throw new RuntimeException("Error[fishing] invalid type: 'wed" + type + "' at line "
                                                       + lineNo);
                        }

                        POOLS[id].type = to;
                    } else if (operation.equalsIgnoreCase("fish")) {
                        Fish fish = new Fish(tokenizer.nextInt(), tokenizer.nextInt(), tokenizer.nextInt(),
                                             tokenizer.nextInt(), 0);

                        switch (tokenizer.nextInt()) {
                        case 0 :
                            POOLS[id].normalFish.add(fish);

                            break;

                        case 1 :
                            POOLS[id].rareFish.add(fish);

                            break;

                        case 2 :
                            POOLS[id].ultraRareFish.add(fish);

                            break;
                        }
                    }
                }
            } while (true);
        } catch (Exception error) {
            error.printStackTrace();
        }
    }
}
