package net.tazogaming.hydra.game.skill.fishing;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 27/03/14
 * Time: 12:57
 */
public class Fish {
    private int prestigeRequirement;
    private int rarity;
    private int exp;
    private int level_requirement;
    private int id;

    /**
     * Constructs ...
     *
     *
     * @param id
     * @param lvl
     * @param xp
     * @param prestige
     * @param rarity
     */
    public Fish(int id, int lvl, int xp, int prestige, int rarity) {
        this.id                  = id;
        this.level_requirement   = lvl;
        this.exp                 = xp;
        this.rarity              = rarity;
        this.prestigeRequirement = prestige;
    }

    /**
     * Method getExp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getExp() {
        return exp;
    }

    /**
     * Method getId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Method getLevel_requirement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevel_requirement() {
        return level_requirement;
    }

    /**
     * Method getRarity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRarity() {
        return rarity;
    }

    /**
     * Method getPrestigeRequirement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPrestigeRequirement() {
        return 0;
    }
}
