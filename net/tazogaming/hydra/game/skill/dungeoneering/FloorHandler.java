package net.tazogaming.hydra.game.skill.dungeoneering;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.skill.combat.combat.DamageFilter;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.map.Point;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/13
 * Time: 13:46
 */
public abstract class FloorHandler {
    private boolean          chestOpened   = false;
    private int              pointIndex    = 0;
    private int              floorId       = 0;
    protected ArrayList<NPC> npcs          = new ArrayList<NPC>();
    private boolean          isConstructed = false;
    private Dungeon          dungeon;
    protected FloorEntity    entity;
    private HashMap<Player, Integer> damageMap = new HashMap<Player, Integer>();


    public void registerDamage(Player player, int dmg){
       int curDmg = 0;
        if(damageMap.containsKey(player))
            curDmg = damageMap.get(player);
        damageMap.put(player, curDmg + dmg);

    }

    public boolean damageAbove(int amt, Player player){
        return damageMap.containsKey(player) && damageMap.get(player) >= amt;
    }


    /**
     * Constructs ...
     *
     *
     * @param dungeon
     * @param entity
     * @param floorId
     */
    public FloorHandler(Dungeon dungeon, FloorEntity entity, int floorId) {
        this.entity  = entity;
        this.dungeon = dungeon;
        this.floorId = floorId;
    }

    /**
     * Method getFloorID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFloorID() {
        return floorId;
    }

    /**
     * Method injectPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void injectPlayer(Player player) {
        player.teleport(entity.getStartingPoint().getX(), entity.getStartingPoint().getY(), 0);
    }

    /**
     * Method addGroundItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param amt
     * @param x
     * @param y
     */
    public void addGroundItem(int id, int amt, int x, int y) {
        dungeon.registerFloorItem(new DungeoneeringStaticItem(id, amt, x, y, 0, this.dungeon));
    }

    protected void addRoamingNPC(int id, int difficulty, int range, int x, int y) {
        NPC npc = Dungeon.npcs[id].getNPC(difficulty, Point.location(x, y, 0), range);

        World.getWorld().getNpcs().add(npc);
        npc.setCurrentInstance(dungeon);
        npc.setHealth(npc.getDefinition().getStartingHealth());
        npc.setMaxHealth(npc.getDefinition().getStartingHealth());

        npc.addDamageFilter("dmg_snagger", new DamageFilter() {
            @Override
            public double filterDamage(double damage, Killable myMob, Killable hitBy, int hitType) {
                registerDamage(hitBy.getPlayer(), (int)damage);
                return damage;
            }
        });
        npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH, DeathPolicy.DONT_DROP_LOOT) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                on_npc_killed(killed, killedBy);


                // To change body of implemented methods use File | Settings | File Templates.
                killedBy.getPlayer().getAccount().increaseVar("dung_kills", 1);
            }
        });
        npcs.add(npc);
    }

    protected NPC addBoss(int id, int x, int y) {
        NPC npc = new NPC(id, Point.location(x,y, 0), 30, NpcDef.FOR_ID(id));

        World.getWorld().getNpcs().add(npc);
        npc.setCurrentInstance(dungeon);
        npc.setDefinition(NpcDef.FOR_ID(id));
        npc.setHealth(npc.getDefinition().getStartingHealth());
        npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH, DeathPolicy.DONT_DROP_LOOT) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                on_npc_killed(killed, killedBy);

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });
        npcs.add(npc);
        return npc;
    }

    /**
     * Method addRoamingNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param difficulty
     * @param range
     */
    public void addRoamingNPC(int id, int difficulty, int range) {
        Point p = nextPoint();

        addRoamingNPC(id, difficulty, range, p.getX(), p.getY());
    }

    public void addNormalNPC(int id, Point position){
        NPC npc = new NPC(id, position, 0);
        World.getWorld().getNpcs().add(npc);
        npc.setCurrentInstance(this.getDungeon());
        dungeon.add(npc);
    }
    private Point nextPoint() {
        ArrayList<Point> points = entity.getSpawnPoints();

        if (pointIndex == points.size()) {
            pointIndex = 0;
        }

        return points.get(pointIndex++);
    }

    /**
     * Method setConstructed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setConstructed(boolean b) {
        this.isConstructed = b;
    }

    protected Dungeon getDungeon() {
        return dungeon;
    }

    /**
     * Method on_npc_killed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     */
    public void on_npc_killed(NPC npc, Killable killedby) {
        npcs.remove(npc);

        if (npcs.size() == 0) {
            floorCompleted();
        }

        npcKilled(npc, killedby);
    }

    /**
     * Method isConstructed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isConstructed() {
        return isConstructed;
    }

    /**
     * Method unlinkAll
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unlinkAll() {
        for (NPC npc : npcs) {
            npc.unlink();
        }

        npcs.clear();
    }

    /**
     * Method getNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<NPC> getNpcs() {
        return npcs;
    }

    /**
     * Method isChestOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isChestOpened() {
        return chestOpened;
    }

    /**
     * Method setChestOpened
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setChestOpened(boolean b) {
        chestOpened = b;
    }

    /**
     * Method construct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void construct();

    protected abstract void objectClicked(int id, int x, int y, int slot);

    protected abstract void npcKilled(NPC npc, Killable killedby);

    /**
     * Method isCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public abstract boolean isCompleted(Player player);

    /**
     * Method floorCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public abstract void floorCompleted();
}
