package net.tazogaming.hydra.game.skill.dungeoneering;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.util.RecurringTickEvent;

//~--- JDK imports ------------------------------------------------------------

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/12/13
 * Time: 17:58
 */
public class Party extends Instance implements RecurringTickEvent {
    private static Zone dungeoneering_lobby_zone = new Zone(2629, 4886, 2636, 4893, -1, false, true,
                                                       "dung_lobby").setBankingAllowed(false).setStatic(true);
    private LinkedList<Player> subPlayers = new LinkedList<Player>();
    private Player             leader;

    /**
     * Constructs ...
     *
     *
     * @param leader
     */
    public Party(Player leader) {
        this.leader = leader;
        registerPlayer(leader);
    }

    /**
     * Method getLeader
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getLeader() {
        return leader;
    }

    /**
     * Method registerPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean registerPlayer(Player player) {
        boolean enter = false;

        if (leader.getAccount().hasVar("dung_mode")) {
            int setting = leader.getAccount().getInt32("dung_mode");

            if (setting == 1) {
                enter = true;
            } else if (setting == 2) {
                enter = leader.getPlayerFriends().contains(player.getUsernameHash());
            } else {
                enter = false;
            }
        } else {
            enter = true;
        }

        if (player == leader) {
            enter = true;
        }

        if (!enter) {
            player.getActionSender().sendMessage("This player has joining turned off.");

            return false;
        }

        if (!subPlayers.contains(player)) {
            player.teleport(2632, 4890, 2);
            player.setCurrentInstance(this);
            subPlayers.add(player);

            return true;
        } else {
            return true;
        }
    }

    /**
     * Method unlink
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     */
    public void unlink(Player plr) {
        plr.setCurrentInstance(null);
        plr.teleport(1707, 5599, 0);
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        for (Iterator<Player> lobby_players = subPlayers.iterator(); lobby_players.hasNext(); ) {
            Player plr = lobby_players.next();

            if (!plr.isLoggedIn() ||!plr.isInZone("dung_lobby")) {
                unlink(plr);
                lobby_players.remove();
            }
        }

        if ((subPlayers.size() == 0) || (leader.getCurrentInstance() != this)) {
            for (Player plr : subPlayers) {
                unlink(plr);
                plr.getActionSender().sendMessage("This party has closed.");
            }

            leader.setParty(null);
            subPlayers.clear();
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<Player> getPlayers() {
        return subPlayers;
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return subPlayers.size() == 0;    // /To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method filterItems
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public static boolean filterItems(Player player) {
        for (int i = 0; i < player.getInventory().getItems().length; i++) {
            if (player.getInventory().getItem(i) != null) {

                    return true;

            }
        }

        for (int i = 0; i < 14; i++) {
            if ((player.getEquipment().getId(i) > 0)) {
                return true;
            }
        }

        return false;
    }
}
