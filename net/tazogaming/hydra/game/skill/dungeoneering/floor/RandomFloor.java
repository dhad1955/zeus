package net.tazogaming.hydra.game.skill.dungeoneering.floor;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorEntity;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorHandler;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.game.LootTable;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/13
 * Time: 16:41
 */
public class RandomFloor extends FloorHandler {
    public static final int
        DIFFICULTY_SUPER_EASY = 0,
        DIFFICULTY_EASY       = 1,
        DIFFICULTY_MED        = 2,
        DIFFICULTY_HARD       = 3;

    /**
     * Constructs ...
     *
     *
     * @param dung
     * @param ent
     * @param id
     */
    public RandomFloor(Dungeon dung, FloorEntity ent, int id) {
        super(dung, ent, id);
    }

    private static double[] getPercentages(int difficulty) {
        double[] percentages = { 100, 0, 0, 0 };

        switch (difficulty) {
        case DIFFICULTY_SUPER_EASY :
            percentages[3] = 0;
            percentages[2] = 5;
            percentages[1] = 25;
            percentages[0] = 70;

            break;

        case DIFFICULTY_EASY :
            percentages[3] = 10;
            percentages[2] = 20;
            percentages[1] = 30;
            percentages[0] = 40;

            break;

        case DIFFICULTY_MED :
            percentages[3] = 30;
            percentages[2] = 30;
            percentages[1] = 20;
            percentages[0] = 20;

            break;

        case DIFFICULTY_HARD :
            percentages[3] = 90;
            percentages[2] = 3;
            percentages[1] = 3;
            percentages[0] = 4;

            break;
        }

        return percentages;
    }

    /**
     * Method construct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void construct() {
        double[] percentages = getPercentages(getDungeon().getDifficulty());
        int      tot         = 0;

        for (double i : percentages) {
            tot += i;
        }

        if (tot != 100) {
            throw new IndexOutOfBoundsException("Error, percentages must add up to 100!");
        }

        if (entity == null) {
            throw new UnknownError();
        }

        for (int i = 0; i < percentages.length; i++) {
            percentages[i] /= 100d;
        }

        int   resourceCount = entity.getSpawnPoints().size();
        int[] amts          = new int[4];

        for (int i = 0; i < amts.length; i++) {
            amts[i] = (int) (resourceCount * percentages[i]);
        }

        for (int i = 0; i < amts.length; i++) {
            for (int k = 0; k < amts[i]; k++) {
                this.addRoamingNPC(GameMath.rand(9), i, 20);
            }
        }
    }

    /**
     * Method floorCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void floorCompleted() {
        ArrayList<Player> rewardingPlayers = new ArrayList<Player>();

        for (Player player : getDungeon().getPlayers()) {
            if (player.getAccount().hasVar("dung_floor")
                    && (player.getAccount().getInt32("dung_floor") == getFloorID()) && damageAbove(80, player)) {
                rewardingPlayers.add(player);
            }
        }

        int    points = 1;
        double bonus  = 1.00;

        switch (getDungeon().getDifficulty()) {
        case DIFFICULTY_EASY :
            bonus = 1.20;

            break;

        case DIFFICULTY_MED :
            bonus = 2.80;

            break;

        case DIFFICULTY_HARD :
            bonus = 6.80;

            break;
        }

        points = (int) ((getFloorID() + 1) * bonus);
        points *= 1.15;

        if (Config.doubleDung) {
            points *= 2;
        }

        if(getDungeon().getPlayers().size() == 1) {
            points *= 3;
        }
        for (Player player : rewardingPlayers) {
            player.addHealth(9999);
            player.addXp(24, 30 * (points));
            player.getActionSender().sendMessage("Floor cleared");

            player.addAchievementProgress2(Achievement.DUNGEON_RAIDER, 1);
            player.addAchievementProgress2(Achievement.DUNGEON_MASTER, 1);

            int bonusPts = player.getAccount().getInt32("dung_kills") * 10;

            player.addPoints(Points.DUNGEONEERING_POINTS, points + bonusPts);
            player.getAccount().setSetting("dung_kills", 0);
        }

        for(Player player : getDungeon().getPlayers()){
            player.getActionSender().sendMessage("Only players that dealt atleast 50 damage on this floor will be rewarded.");
        }
        for (Point p : entity.getSpawnPoints()) {
            if (new Random().nextInt(5000) < 100) {
                FloorItem f = new FloorItem(18169, 1, p.getX(), p.getY(), 0, null);

                f.setCurrentInstance(this.getDungeon());
                f.globalise();
            }
        }

        if ((getFloorID() + 1) == getDungeon().length()) {
            for (Player player : getDungeon().getPlayers()) {
                player.getActionSender().sendMessage("Dungeon completed well done.");
            }

            getDungeon().destruct();
        }
    }

    /**
     * Method isCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isCompleted(Player player) {
        if (npcs.size() != 0) {
            player.getActionSender().sendMessage("Please clear the floor before moving to the next");

            return false;
        }

        return true;
    }

    @Override
    protected void objectClicked(int id, int x, int y, int slot) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void npcKilled(NPC npc, Killable killedby) {

        int lootIndex = getDungeon().getLoot(getDungeon().getDifficulty(), false);
        Loot loot = LootTable.getTable(Dungeon.LOOT_ID).randomLoot(lootIndex);

        FloorItem floor = new FloorItem(loot.getItemID(), loot.getAmount(), npc.getX(), npc.getY(), npc.getHeight(), killedby.getPlayer());
        floor.setCurrentInstance(this.getDungeon());
        getDungeon().registerFloorItem(floor);
    }
}
