package net.tazogaming.hydra.game.skill.dungeoneering.floor;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorEntity;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/13
 * Time: 14:03
 */
public class HomeFloor extends FloorHandler {

    /**
     * Constructs ...
     *
     *
     * @param dung
     */
    public HomeFloor(Dungeon dung) {
        super(dung, FloorEntity.homeFloor, 0);
    }

    /**
     * Method construct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void construct() {
        addGroundItem(18169, 10, 3166, 5542);

        // rejuvination potion
        addGroundItem(17594, 1, 3166, 5536);
        addGroundItem(16939, 1, 3160, 5540);
        addGroundItem(857, 1, 3154, 5540);
        addGroundItem(884, 150, 3153, 5535);
        addGroundItem(892, 100, 3169, 5539);
        addGroundItem(16977, 1, 3156, 5536);
        addGroundItem(882, 100, 3160, 5538);
        addNormalNPC(11226, Point.location(3155, 5539, 0));
        // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method isCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isCompleted(Player player) {
        return true;
    }

    /**
     * Method floorCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void floorCompleted() {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void objectClicked(int id, int x, int y, int slot) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void npcKilled(NPC npc, Killable k) {

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
