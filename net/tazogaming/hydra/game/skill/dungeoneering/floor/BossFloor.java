package net.tazogaming.hydra.game.skill.dungeoneering.floor;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorEntity;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorHandler;
import net.tazogaming.hydra.game.Loot;
import net.tazogaming.hydra.game.LootTable;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/12/13
 * Time: 00:58
 */
public class BossFloor extends FloorHandler {

    /*
     * easy
     * 10029 bloodchiller
     * 12769 warmonger
     * 9843 Lexicus
     *
     * Medium
     * 12519 world-gorger
     * 12898 hope
     * 10156 shadowforger
     *
     * Hard:
     * 12739 tentactle
     * 8528 nomad
     * 11886 thunderous
     */
    public static final int
        EASY   = 0,
        MEDIUM = 1,
        HARD   = 2;

    /*
     *
     * Bosses
     * 10156 - Shadow gazer easy
     * 3200 - chaos elemental easy
     * 5902 - the inadaquecy medium
     * 3494 - flambeed medium
     * 5666 - barrelchest medium
     * 8528 - Nomad Super hard
     * 12841 - Warmonger (hard)
     * 11886 - Thunderous (hard)
     *
     *
     *
     */

    /**
     * Constructs ...
     *
     *
     * @param dung
     * @param ent
     * @param floorId
     */
    public BossFloor(Dungeon dung, FloorEntity ent, int floorId) {
        super(dung, ent, floorId);
    }

    public enum Bosses {
        BLOODCHILLER(10029, EASY), WARMONGER(12769, EASY), LEXICUS(9843, EASY), WORLDGORGER(12519, MEDIUM),
        HOPE(12898, MEDIUM), SHADOWFORGER(10156, MEDIUM), TENTACLE(12739, HARD), NOMAD(8528, HARD),
        THUNDEROUS(11886, HARD);

        /** npcId made: 14/09/19 **/
        private int npcId;

        /** difficulty made: 14/09/19 **/
        private int difficulty;

        Bosses(int npcid, int difficulty) {
            this.npcId      = npcid;
            this.difficulty = difficulty;
        }

        /**
         * Method getDifficulty
         * Created on 14/09/19
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getDifficulty() {
            return difficulty;
        }

        /**
         * Method getNpcId
         * Created on 14/09/19
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public int getNpcId() {
            return npcId;
        }
    }

    /**
     * Method getHPModifier
     * Created on 14/09/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param players
     *
     * @return
     */
    public double getHPModifier(int players) {
        double d = 1;

        d += (0.7 * players);

        return d;
    }

    /**
     * Method getBoss
     * Created on 14/09/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param difficulty
     * @param players
     *
     * @return
     */
    public NPC getBoss(int difficulty, int players) {
        return null;
    }

    /**
     * Method getBoss
     * Created on 14/09/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param difficulty
     *
     * @return
     */
    public int getBoss(int difficulty) {
        switch (difficulty) {
        case RandomFloor.DIFFICULTY_SUPER_EASY :
        case RandomFloor.DIFFICULTY_EASY :
            return EASY;

        case RandomFloor.DIFFICULTY_HARD :
            return HARD;

        case RandomFloor.DIFFICULTY_MED :
            return MEDIUM;
        }

        throw new IllegalArgumentException("Bad difficulty");
    }

    /**
     * Method construct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void construct() {
        int           players        = getDungeon().getPlayers().size();
        int           difficulty     = getBoss(getDungeon().getDifficulty());
        List<Integer> possibleBosses = new ArrayList<Integer>();

        for (Bosses boss : Bosses.values()) {
            if (boss.getDifficulty() == difficulty) {
                possibleBosses.add(boss.getNpcId());
            }
        }

        if (possibleBosses.size() == 0) {
            throw new RuntimeException("woops");
        }

        NPC npc = addBoss(possibleBosses.get(GameMath.rand3(possibleBosses.size())), 3039, 5345);

        npc.setMaxHealth((int) (npc.getMaxHealth() * getHPModifier(players)));
        npc.setHealth((int)npc.getMaxHealth());
    }

    @Override
    protected void objectClicked(int id, int x, int y, int slot) {

        // To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    protected void npcKilled(NPC npc, Killable k) {

        int lootIndex = getDungeon().getLoot(getDungeon().getDifficulty(), true);
        Loot loot = LootTable.getTable(Dungeon.LOOT_ID).randomLoot(lootIndex);

        FloorItem floor = new FloorItem(loot.getItemID(), loot.getAmount(), npc.getX(), npc.getY(), npc.getHeight(), k.getPlayer());
        floor.setCurrentInstance(this.getDungeon());
        getDungeon().registerFloorItem(floor);
    }

    /**
     * Method isCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    @Override
    public boolean isCompleted(Player player) {
        if (npcs.size() == 0) {
            return true;
        }

        player.getActionSender().sendMessage("You need to kill the boss first.");

        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }

    /**
     * Method floorCompleted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void floorCompleted() {
        int base = (getFloorID() * (getDungeon().getDifficulty() + 1)) * 3;


        base *= (double)(1 + ((getDungeon().getDifficulty() * 8) / 100));
        for (Player player : getDungeon().getPlayers()) {
            player.addPoints(Points.DUNGEONEERING_POINTS, base);
            player.addXp(24, base * 16);
            player.getActionSender().sendMessage("Floor cleared");
            player.addPoints(Points.DUNGEONEERING_POINTS, 10);
        }

        // To change body of implemented methods use File | Settings | File Templates.
    }
}
