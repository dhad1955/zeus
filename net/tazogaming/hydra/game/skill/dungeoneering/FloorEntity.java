package net.tazogaming.hydra.game.skill.dungeoneering;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.FileReader;

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/13
 * Time: 13:37
 */
public class FloorEntity {
    public static ArrayList<FloorEntity> floor_map = new ArrayList<FloorEntity>();
    public static FloorEntity            homeFloor;
    public static FloorEntity            bossFloor;

    static {
        homeFloor = new FloorEntity();
        homeFloor.setStartingPoint(Point.location(3160, 5538, 0));
        bossFloor = new FloorEntity();
        bossFloor.setStartingPoint(Point.location(3038, 5354, 0));

        try {
            BufferedReader reader     = new BufferedReader(new FileReader("config/iskill/dungeoneering/dungeon1.conf"));
            int            curRoomID  = -1;
            int            nextRoomId = 0;
            FloorEntity    curRoom    = null;

            do {
                String line = reader.readLine();

                if (line == null) {
                    break;
                }

                if ((line.length() == 0) || line.startsWith("//")) {
                    continue;
                }

                if (line.startsWith("room")) {
                    curRoomID = nextRoomId++;
                    curRoom   = new FloorEntity();

                    continue;
                }

                if (curRoomID != -1) {
                    String[] tokens = line.split(" ");

                    if (tokens[0].equalsIgnoreCase("startpoint")) {
                        int spawnX = Integer.parseInt(tokens[1]);
                        int spawnY = Integer.parseInt(tokens[2]);

                        curRoom.setStartingPoint(Point.location(spawnX, spawnY, 0));
                    } else if (tokens[0].equalsIgnoreCase("spawnpoint")) {
                        int spawnX = Integer.parseInt(tokens[1]);
                        int spawnY = Integer.parseInt(tokens[2]);

                        curRoom.getSpawnPoints().add(Point.location(spawnX, spawnY, 0));
                    } else if (tokens[0].equalsIgnoreCase("chestloc")) {

                    }
                }

                if (line.startsWith("}")) {
                    floor_map.add(curRoom);
                    curRoom   = null;
                    curRoomID = -1;
                }
            } while (true);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    private ArrayList<Point> spawnPoints = new ArrayList<Point>();
    private Point            chestLocation;
    private Point            startingPoint;

    /**
     * Method getChestLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getChestLocation() {
        return chestLocation;
    }

    /**
     * Method setChestLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chestLocation
     */
    public void setChestLocation(Point chestLocation) {
        this.chestLocation = chestLocation;
    }

    /**
     * Method getStartingPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getStartingPoint() {
        return startingPoint;
    }

    /**
     * Method setStartingPoint
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param startingPoint
     */
    public void setStartingPoint(Point startingPoint) {
        this.startingPoint = startingPoint;
    }

    /**
     * Method getSpawnPoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Point> getSpawnPoints() {
        return spawnPoints;
    }

    /**
     * Method setSpawnPoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param spawnPoints
     */
    public void setSpawnPoints(ArrayList<Point> spawnPoints) {
        this.spawnPoints = spawnPoints;
    }
}
