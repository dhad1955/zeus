package net.tazogaming.hydra.game.skill.dungeoneering;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.FloorItem;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.player.Points;
import net.tazogaming.hydra.map.Zone;
import net.tazogaming.hydra.map.Instance;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;
import net.tazogaming.hydra.game.skill.dungeoneering.floor.BossFloor;
import net.tazogaming.hydra.game.skill.dungeoneering.floor.HomeFloor;
import net.tazogaming.hydra.game.skill.dungeoneering.floor.RandomFloor;
import net.tazogaming.hydra.util.RecurringTickEvent;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/13
 * Time: 13:37
 */
public class Dungeon extends Instance implements RecurringTickEvent {
    public static DynamicCombatNPC[] npcs = new DynamicCombatNPC[30];

    /** dungeoneering made: 14/09/19 **/
    public static Zone dungeoneering = new Zone(
                                            "dung",
                                            new Zone(
                                                3130, 5441, 3331, 5579, 0, true, false, "dung_main").setBankingAllowed(
                                                false).setTeleportAllowed(false).setTeleportMessage(
                                                "You can't teleport out, use a portal to leave.").setBobAllowed(
                                                false).setStatic(true), new Zone(
                                                3088, 5312, 3071, 5374, 0, true, false, "dung_boss").setBankingAllowed(
                                                false).setTeleportAllowed(false).setTeleportMessage(
                                                "You can't teleport out, use a portal to leave.").setBobAllowed(
                                                false).setStatic(true));
    public static final int LOOT_ID = 567;

    static {
        npcs[0] = new DynamicCombatNPC(9914, 9916, 9918, 9920);
        npcs[1] = new DynamicCombatNPC(10042, 10045, 10047, 10049);
        npcs[2] = new DynamicCombatNPC(10059, 10062, 10064, 10066);

        // ice spider
        npcs[3] = new DynamicCombatNPC(10163, 10164, 10165, 10166);

        // hell hound
        npcs[4] = new DynamicCombatNPC(10227, 10229, 10231, 10235);
        npcs[5] = new DynamicCombatNPC(10236, 10239, 10242, 10245);
        npcs[6] = new DynamicCombatNPC(10366, 10368, 10370, 10385);
        npcs[7] = new DynamicCombatNPC(10460, 10462, 10463, 10465);
        npcs[8] = new DynamicCombatNPC(10797, 10801, 10807, 10811);
        npcs[9] = new DynamicCombatNPC(10831, 10838, 10840, 10842);
    }

    /** players made: 14/09/19 **/
    private ArrayList<Player> players = new ArrayList<Player>();

    /** currentFloors made: 14/09/19 **/
    private ArrayList<FloorHandler> currentFloors = new ArrayList<FloorHandler>();

    /** groundItems made: 14/09/19 **/
    private ArrayList<FloorItem> groundItems = new ArrayList<FloorItem>();

    /** difficulty made: 14/09/19 **/
    private int difficulty = 0;

    /** isClosed made: 14/09/19 **/
    private boolean isClosed = false;

    /** currentFloor made: 14/09/19 **/
    private int currentFloor;

    /**
     * Constructs ...
     *
     *
     * @param party
     */
    public Dungeon(Party party) {
        constructFloors(party.getLeader().getAccount().getInt32("dung_floors"));

        for (Player player : party.getPlayers()) {
            player.setCurrentInstance(this);
            player.getGameFrame().openOverlay(494);


            players.add(player);
            currentFloors.get(0).injectPlayer(player);
            player.getAccount().setSetting("dung_kills", 0);
            player.getActionSender().sendMessage("This dungeon has "
                    + party.getLeader().getAccount().getInt32("dung_floors") + " floors");
            player.setParty(null);
            player.getAccount().setSetting("dung_floor", 0);

            int[] binded = player.getEquipment().getBindedItems();

            for(int i = 0; i < binded.length; i++){
                if(binded[i] > 0){
                    Item ix = Item.forId(binded[i]);
                    player.getEquipment().forceWield(ix.getWieldSlot(), ix.getIndex(), 1);

                }
            }
            player.getAppearance().setChanged(true);
            player.getAccount().setSetting("dung_lives", 2);

            player.setCurDeathHandler(new DeathHandler() {
                @Override
                public void handleDeath(Killable killed, Killable killedBy) {

                    Point homePoint = currentFloors.get(0).entity.getStartingPoint();
                    int lives = killed.getPlayer().getAccount().getInt32("dung_lives");
                    if(lives == 0){
                        destructPlayer(killed.getPlayer());
                        killed.getPlayer().getActionSender().sendMessage("You have ran out of lives");
                        return;
                    }
                    killed.getPlayer().getActionSender().sendMessage("You have respawned, you have "+lives+" lives remaining");
                    killed.getPlayer().teleport(homePoint.getX(), homePoint.getY(), 0);
                    killed.getPlayer().getAccount().setSetting("dung_floor", 0);
                    killed.getPlayer().getAccount().setSetting("dung_lives", lives - 1);

                    // To change body of implemented methods use File | Settings | File Templates.
                }
            });
        }

        difficulty = party.getLeader().getAccount().getInt32("dung_diff");
        currentFloors.get(0).construct();
        party.getPlayers().clear();
        Core.submitTask(this);
    }


    public static boolean checkBind(Player player, int count, int stat){
        if(count > 1 && stat < 18){
            player.getActionSender().sendMessage("You need a dungeoneering level of atleast 18 to bind 2 items");
            return false;
        }

        if(count > 2 && stat < 40){
            player.getActionSender().sendMessage("You need a dungeoneering level of atleast 40 to bind 3 items");
            return false;
        }

        if(count > 3 && stat < 50){
            player.getActionSender().sendMessage("You need a dungeoneering level of atleast 50 to bind 4 items");
            return false;
        }

        if(count > 4 && stat < 60){
            player.getActionSender().sendMessage("You need a dungeoneering level of atleast 60 to bind 4 items");
            return false;
        }

        if(count > 5 && stat < 70){
            player.getActionSender().sendMessage("You need a dungeoneering level of atleast 70 to bind 5 items");
            return false;
        }

        if(count > 6 && stat < 80){
            player.getActionSender().sendMessage("You need a dungeoneering level of atleast 80 to bind 6 items");
            return false;
        }
        return true;





    }
    /**
     * Method getLoot
     * Created on 14/09/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param difficulty
     * @param boss
     *
     * @return
     */
    public int getLoot(int difficulty, boolean boss) {

        // 3 tables,
        if (boss && (difficulty == RandomFloor.DIFFICULTY_HARD) && (currentFloor > 8)) {
            return 3;    // instant win
        }

        int chance = 1000;
        int rand   = GameMath.rand3(chance);
        int bonus  = boss
                     ? 2
                     : 0;

        if (difficulty == RandomFloor.DIFFICULTY_EASY) {
            bonus += 2;
        }

        if (difficulty == RandomFloor.DIFFICULTY_MED) {
            bonus += 4;
        }

        if (difficulty == RandomFloor.DIFFICULTY_HARD) {
            bonus += 6;
        }

        bonus += currentFloor;

        if (rand < 900) {
            return 1;
        }

        if (rand < 950) {
            return 2;
        }

        if (rand < 960 + bonus) {
            return 3;
        }

        return 2;
    }

    /**
     * Method getDifficulty
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getDifficulty() {
        return difficulty;
    }

    /**
     * Method getPlayers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Player> getPlayers() {
        return players;
    }

    /**
     * Method destructPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void destructPlayer(Player player) {
        player.getWindowManager().closeWindow();
        player.getInventory().clear();
        player.getEquipment().clear();

        for (int i = 0; i < 14; i++) {
            player.getEquipment().updateEquipment(i);
        }

        player.getAppearance().setChanged(true);
        player.getGameFrame().closeOverlay();
        player.setCurrentInstance(null);
        player.getAppearance().setChanged(true);
        player.setPoisonTime(0);
        player.setCurrentHealth(player.getMaxHealth());
        player.getActionSender().sendStat(3);
        player.setCurDeathHandler(new DefaultDeathHandler());
        player.teleport(1707, 5599, 0);
    }

    /**
     * Method destruct
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void destruct() {
        for (FloorItem i : groundItems) {
            i.remove();
        }

        groundItems.clear();

        for (FloorHandler h : currentFloors) {
            h.unlinkAll();
        }

        for (Player player : players) {
            destructPlayer(player);
        }

        players.clear();
        isClosed = true;
    }

    /**
     * Method registerFloorItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void registerFloorItem(FloorItem i) {
        groundItems.add(i);
    }

    /**
     * Method removeFloorItem
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void removeFloorItem(FloorItem i) {
        groundItems.remove(i);
    }

    /**
     * Method length
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        return currentFloors.size();
    }

    /**
     * Method getFloor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public FloorHandler getFloor(int i) {
        return currentFloors.get(i);
    }

    /**
     * Method getCurrentFloor
     * Created on 14/09/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentFloor() {
        return currentFloor;
    }

    /**
     * Method nextFloor
     * Created on 14/09/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void nextFloor() {
        this.currentFloor++;
    }

    /**
     * Method constructFloors
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     */
    public void constructFloors(int amount) {
        ArrayList<FloorEntity> possibleFloors = new ArrayList<FloorEntity>();

        possibleFloors.addAll(FloorEntity.floor_map);
        currentFloors.add(new HomeFloor(this));

        int ran = GameMath.rand(amount);

        for (int i = 0; i < amount; i++) {
            FloorEntity e = possibleFloors.get(GameMath.rand3(possibleFloors.size()));

            currentFloors.add(new RandomFloor(this, e, i + 1));
            possibleFloors.remove(e);
        }

        currentFloors.add(new BossFloor(this, FloorEntity.bossFloor, amount));
    }

    /**
     * Method tick
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void tick() {
        for (int i = 0; i < players.size(); i++) {
            Player plr = players.get(i);

            if(Core.currentTime % 20 == 0){

                plr.getGameFrame().sendString("Floors Remaining: <col=ffffff>"+(currentFloors.size() - getCurrentFloor())+"</col>" +
                        "<br>Enemies remaining: <col=ffffff>"+currentFloors.get(getCurrentFloor()).getNpcs().size()+"</col><br>Points: <col=fffff>"+plr.getPoints(Points.DUNGEONEERING_POINTS), 494, 3);

            }
            if (!plr.isLoggedIn() || (!plr.isInArea(dungeoneering) &&!plr.isInZone("dungboss"))) {
                destructPlayer(plr);
                players.remove(i);
            }
        }

        if (players.size() == 0) {
            destruct();
        }
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public boolean terminate() {
        return isClosed;
    }
}
