package net.tazogaming.hydra.game.skill.dungeoneering;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.FloorItem;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/12/13
 * Time: 19:14
 */
public class DungeoneeringStaticItem extends FloorItem {

    /**
     * Constructs ...
     *
     *
     * @param itemId
     * @param amount
     * @param x
     * @param y
     * @param h
     * @param dungeon
     */
    public DungeoneeringStaticItem(int itemId, int amount, int x, int y, int h, Dungeon dungeon) {
        super(itemId, amount, x, y, h, null);
        setCurrentInstance(dungeon);
        isGlobal   = true;
        dontRemove = true;
    }


}
