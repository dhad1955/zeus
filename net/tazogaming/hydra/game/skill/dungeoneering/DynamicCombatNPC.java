package net.tazogaming.hydra.game.skill.dungeoneering;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcDef;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/13
 * Time: 14:15
 */
public class DynamicCombatNPC {
    public static final int
        SUPER_EASY_TIER  = 0,
        EASY_TIER        = 1,
        MED_TIER         = 2,
        HARD_TIER        = 3,
        SUPERHARD_TIER   = 4;
    public static final int
        ICE_FIEND        = 0,
        PLANE_FREEZER    = 1,
        ICY_BONES        = 2,
        HOBOGOBLIN       = 3,
        ICE_SPIDER       = 4,
        ICE_WARRIOR      = 5,
        HOBO_GOBLIN2     = 6,
        ZOMBIE           = 7,
        ICE_ELEMENTAL    = 8,
        BRUTE            = 9,
        MYSTERIOUS_SHADE = 10;
    private int[] tiers;

    /**
     * Constructs ...
     *
     *
     * @param ids
     */
    public DynamicCombatNPC(int... ids) {
        tiers = ids;
    }

    /**
     * Method getNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tier
     * @param location
     * @param range
     *
     * @return
     */
    public NPC getNPC(int tier, Point location, int range) {
        NpcDef definition = NpcDef.FOR_ID(tiers[0]);

        if (definition == null) {
            throw new NullPointerException();
        }

        definition = new NpcDef(definition);

        for (int i = 0; i < definition.getCombatStats().length; i++) {
            definition.combatStats[i] *= (tier + 1);
        }

        definition.combatStats[1] /= 3;

        for (int i = 0; i < definition.bonuses.length; i++) {
            definition.bonuses[i] *= (tier + 2);
        }


        definition.setStartingHealth(definition.getStartingHealth() * (tier + 1));
        definition.setMaxHitModifier(tier * 6);

        NPC npc = new NPC(tiers[tier], location, range, definition);

        return npc;
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param tier
     *
     * @return
     */
    public int get(int tier) {
        return tiers[tier];
    }
}
