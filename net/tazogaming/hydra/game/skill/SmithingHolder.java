package net.tazogaming.hydra.game.skill;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.skill.entity.SmithingItem;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/10/13
 * Time: 04:16
 */
public class SmithingHolder {
    public SmithingItem dagger;
    public SmithingItem plateLegs;
    public SmithingItem axe;
    public SmithingItem chainBody;
    public SmithingItem sword;
    public SmithingItem kiteShield;
    public SmithingItem scimmy;
    public SmithingItem mace;
    public SmithingItem warHammer;
    public SmithingItem longSword;
    public SmithingItem battleAxe;
    public SmithingItem twoHander;
    public SmithingItem plateBody;
    public SmithingItem plateSkirt;
    public SmithingItem fullHelm;
    public SmithingItem arrowTips;
    public SmithingItem knives;
    public SmithingItem medHelm;
    public SmithingItem sqShield;
    public SmithingItem dartTips;
    public Item         barId;
}
