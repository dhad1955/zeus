package net.tazogaming.hydra.game.skill.slayer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 06:39
 */
public class SlayKillable {
    private boolean isSilent = false;
    private int[]   npcIds;
    private int     levelRequired;
    private int     experienceGiven;

    /**
     * Method isSilent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSilent() {
        return isSilent;
    }

    /**
     * Method setIsSilent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param silent
     */
    public void setIsSilent(boolean silent) {
        this.isSilent = silent;
    }

    /**
     * Method is
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public boolean is(int id) {
        for (int i = 0; i < npcIds.length; i++) {
            if (npcIds[i] == id) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getLevelRequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLevelRequired() {
        return levelRequired;
    }

    /**
     * Method setLevelRequired
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param levelRequired
     */
    public void setLevelRequired(int levelRequired) {
        this.levelRequired = levelRequired;
    }

    /**
     * Method getNpcIds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getNpcIds() {
        return npcIds;
    }

    /**
     * Method setNpcIds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npcIds
     */
    public void setNpcIds(int[] npcIds) {
        this.npcIds = npcIds;
    }

    /**
     * Method getExperienceGiven
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getExperienceGiven() {
        return experienceGiven;
    }

    /**
     * Method setExperienceGiven
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param experienceGiven
     */
    public void setExperienceGiven(int experienceGiven) {
        this.experienceGiven = experienceGiven;
    }
}
