package net.tazogaming.hydra.game.skill.slayer;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.Text;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/12/19
 * @author         Daniel Hadland
 */
class SlayerPartner {
    long playerId;
    int  totalKills;

    SlayerPartner(long has) {
        this.playerId = has;
    }
}


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/12/2014
 * Time: 16:33
 */
public class SlayerTask {
    boolean         terminated = false;
    SlayerPartner[] partners   = new SlayerPartner[2];

    /** lastUpdate made: 14/12/19 **/
    private long lastUpdate = System.currentTimeMillis();
    int          taskId;
    int          killsToComplete;
    int          totalKills;

    /**
     * Constructs ...
     *
     *
     * @param player
     * @param npcId
     * @param killsToComplete
     */
    public SlayerTask(Player player, int npcId, int killsToComplete) {
        this.taskId          = npcId;
        this.killsToComplete = killsToComplete;
        this.partners[0]     = new SlayerPartner(player.getUsernameHash());
    }

    /**
     * Method isLeader
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean isLeader(Player player) {
        return partners[0].playerId == player.getUsernameHash();
    }

    /**
     * Method join
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void join(Player player) {
        this.partners[1] = new SlayerPartner(player.getUsernameHash());
    }

    /**
     * Method getOpposite
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plr
     *
     * @return
     */
    public Player getOpposite(Player plr) {

        return (partners[0].playerId == plr.getUsernameHash())
               ? World.getWorld().getPlayer(partners[1].playerId)
               : World.getWorld().getPlayer(partners[0].playerId);
    }

    /**
     * Method leaveTask
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void leaveTask(Player player) {
        if (isLeader(player)) {
            this.partners[0] = null;
            this.partners[0] = partners[1];
            this.partners[1] = null;
        } else {
            this.partners[1] = null;
        }
    }

    /**
     * Method getLastUpdate
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getLastUpdate() {
        return lastUpdate;
    }

    /**
     * Method setLastUpdate
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lastUpdate
     */
    public void setLastUpdate(long lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    /**
     * Method getPartners
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SlayerPartner[] getPartners() {
        return partners;
    }

    /**
     * Method setPartners
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param partners
     */
    public void setPartners(SlayerPartner[] partners) {
        this.partners = partners;
    }

    /**
     * Method getTotalKills
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTotalKills() {
        return totalKills;
    }

    /**
     * Method setTotalKills
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param totalKills
     */
    public void setTotalKills(int totalKills) {
        this.totalKills = totalKills;
    }

    /**
     * Method getKillsToComplete
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getKillsToComplete() {
        return killsToComplete;
    }

    /**
     * Method setKillsToComplete
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param killsToComplete
     */
    public void setKillsToComplete(int killsToComplete) {
        this.killsToComplete = killsToComplete;
    }

    /**
     * Method getTaskId
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTaskId() {
        return taskId;
    }

    /**
     * Method setTaskId
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param taskId
     */
    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    /**
     * Method isFinished
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isFinished() {
        return this.totalKills >= killsToComplete;
    }

    /**
     * Method getPartner
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPartner() {
        if (isDuoTask()) {
            return World.getWorld().getPlayer(partners[1].playerId);
        }

        return null;
    }

    /**
     * Method getPartnerName
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getPartnerName() {
        return Text.nameForLong(partners[1].playerId);
    }

    /**
     * Method registerKill
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void registerKill(Player player) {
        lastUpdate = Core.currentTimeMillis();
        this.totalKills++;

        for (SlayerPartner partner : partners) {
           if(partner != null)
            if (partner.playerId == player.getUsernameHash()) {
                partner.totalKills++;
            }
        }
    }



    /**
     * Method getKills
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public int getKills(Player player) {
        for (SlayerPartner partner : partners) {
            if (partner.playerId == player.getUsernameHash()) {
                return partner.totalKills;
            }
        }

        return 0;
    }

    /**
     * Method isDuoTask
     * Created on 14/12/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDuoTask() {
        for (SlayerPartner partner : partners) {
            if (partner == null) {
                return false;
            }
        }

        return true;
    }
}
