package net.tazogaming.hydra.game.skill.slayer;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.util.ArgumentTokenizer;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 06:39
 */
public class Slayer {
    private static HashMap<Integer, int[]> master_tables = new HashMap<Integer, int[]>();
    private static ArrayList<SlayKillable>   hunting_list  = new ArrayList<SlayKillable>();

    /**
     * Method get_slayer_npc_info
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     *
     * @return
     */
    public static SlayKillable get_slayer_npc_info(NPC npc) {
        ;
        for (SlayKillable k : hunting_list) {
            if (k.is(npc.getId())) {
                return k;
            }
        }

        return null;
    }

    /**
     * Method get_slayer_npc_info
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npcIndex
     *
     * @return
     */
    public static SlayKillable get_slayer_npc_info(int npcIndex) {
        for (SlayKillable k : hunting_list) {
            for (int i = 0; i < k.getNpcIds().length; i++) {
                if (k.getNpcIds()[i] == npcIndex) {
                    return k;
                }
            }
        }

        return null;
    }

    /**
     * Method load_masters
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void load_masters(Player pla) {
        int lineC = 0;

        try {
            BufferedReader reader =
                new BufferedReader(new FileReader(new File("config/iskill/slayer/slayer_tables.cfg")));
            String line = "";

            while ((line = reader.readLine()) != null) {
                if (line.startsWith("master")) {
                    ArgumentTokenizer parser = new ArgumentTokenizer(line.split(" "));

                    parser.next();
                    parser.next();

                    int      id      = parser.nextInt();
                    String[] npcids  = parser.nextString().split(",");
                    int[]    npcBind = new int[npcids.length];

                    for (int k = 0; k < npcids.length; k++) {
                        npcBind[k] = Integer.parseInt(npcids[k]);
                    }

                    // int level = parser.nextInt();
                    // int exp = parser.nextInt();
                    master_tables.put(id, npcBind);
                }
            }

            reader.close();
        } catch (Exception ee) {
            if (pla != null) {
                pla.getActionSender().sendMessage("Error loading slayer_tables.cfg, parse_error line: " + lineC);
            }

            ee.printStackTrace();
        }
    }

    /**
     * Method get_table
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static int[] get_table(int id) {
        return master_tables.get(id);
    }

    /**
     * Method load_list
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public static void load_list(Player pla) {
        int lineC = 0;

        try {
            BufferedReader reader =
                new BufferedReader(new FileReader(new File("config/iskill/slayer/slayer_npcs.cfg")));
            String line = "";

            while ((line = reader.readLine()) != null) {
                lineC++;

                if (line.startsWith("slay")) {
                    ArgumentTokenizer parser = new ArgumentTokenizer(line.split(" "));

                    parser.next();
                    parser.next();

                    String[] npcids  = parser.nextString().split(",");
                    int[]    npcBind = new int[npcids.length];

                    for (int k = 0; k < npcids.length; k++) {
                        npcBind[k] = Integer.parseInt(npcids[k]);
                    }

                    int          level         = parser.nextInt();
                    int          exp           = parser.nextInt();
                    SlayKillable slay_killable = new SlayKillable();

                    slay_killable.setNpcIds(npcBind);
                    slay_killable.setExperienceGiven(exp);
                    slay_killable.setLevelRequired(level);
                    slay_killable.setIsSilent(parser.hasNext());
                    hunting_list.add(slay_killable);
                }
            }

            reader.close();
        } catch (Exception ee) {
            if (pla != null) {
                pla.getActionSender().sendMessage("Error loading slayer_npcs.cfg, parse_error line: " + lineC);
            }
            System.err.println("parse error slayer: "+lineC);

            ee.printStackTrace();
        }
    }
}
