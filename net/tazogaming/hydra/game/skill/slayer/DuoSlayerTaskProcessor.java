package net.tazogaming.hydra.game.skill.slayer;

import net.tazogaming.hydra.util.RecurringTickEvent;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/12/2014
 * Time: 17:05
 */
public class DuoSlayerTaskProcessor implements RecurringTickEvent {
    private List<SlayerTask> duoTasks = new LinkedList<SlayerTask>();


    @Override
    public void tick() {
        for(Iterator<SlayerTask> duoTask = duoTasks.iterator(); duoTask.hasNext();){
              SlayerTask task = duoTask.next();
            if(!task.isDuoTask()){
                duoTask.remove();
            }
        }
    }


    public void saveTasks() {

    }

    /**
     * Method terminate
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean terminate() {
        return false;
    }


}
