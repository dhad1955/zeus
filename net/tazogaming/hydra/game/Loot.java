package net.tazogaming.hydra.game;

import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/10/13
 * Time: 11:03
 */
public class Loot {
    int itemId;
    int rarity;
    int amount;

    /**
     * Constructs ...
     *
     *
     * @param itemId
     * @param amt
     */
    public Loot(int itemId, int amt) {
        this(itemId, amt, 0);
    }

    /**
     * Constructs ...
     *
     *
     * @param itemId
     * @param amt
     * @param rarity
     */
    public Loot(int itemId, int amt, int rarity) {
        this.itemId = itemId;
        this.rarity = rarity;
        this.amount = amt;
    }

    /**
     * Method getRarity
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRarity() {
        return this.rarity;
    }

    /**
     * Method getItemID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getItemID() {
        return itemId;
    }

    /**
     * Method getAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getAmount() {
        return amount  < 0 ? GameMath.rand(Math.abs(amount)) : amount;
    }

    /**
     * Method setAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }
}
