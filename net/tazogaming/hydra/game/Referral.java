package net.tazogaming.hydra.game;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/11/2014
 * Time: 13:33
 */
public class Referral {

    public Referral(int referredFrom, int recordedPlaytime) {
        this.referredFrom = referredFrom;
        this.recordedPlaytime = recordedPlaytime;
    }

    private int referredFrom;

    public int getRecordedPlaytime() {
        return recordedPlaytime;
    }

    public void setRecordedPlaytime(int recordedPlaytime) {
        this.recordedPlaytime = recordedPlaytime;
    }

    public int getReferredFrom() {
        return referredFrom;
    }

    public void setReferredFrom(int referredFrom) {
        this.referredFrom = referredFrom;
    }

    private int recordedPlaytime;

}
