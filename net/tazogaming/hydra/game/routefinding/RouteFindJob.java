package net.tazogaming.hydra.game.routefinding;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/07/14
 * Time: 18:04
 */
public class RouteFindJob {
    public static final int
        STATUS_PROCESSING_LONG                   = 0,
        STATUS_QUEUED                            = 1,
        STATUS_CANCELLED                         = 1,
        STATUS_BEGIN                             = 2;
    private HashMap<Integer, Object> attachments = new HashMap<Integer, Object>();
    private RouteFinderFactory factory;
    private int                      status;
    private RouteFinder finder;
    private Point                    destination;
    private Player                   player;

    /**
     * Constructs ...
     *
     *
     * @param factory
     * @param destination
     * @param player
     */
    public RouteFindJob(RouteFinderFactory factory, Point destination, Player player) {
        this.destination = destination;
        this.player      = player;
        this.factory     = factory;
        this.setStatus(STATUS_BEGIN);
    }

    /**
     * Method getStateType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStatus() {
        return status;
    }

    /**
     * Method toFar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param begin
     * @param to
     *
     * @return
     */
    public static final boolean toFar(Point begin, Point to) {
        if (Point.getDistance(begin, to) > 13) {
            return true;
        }

        return ClippingDecoder.unReachable(to.getX(), to.getY(), to.getHeight());
    }

    /**
     * Method setStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param status
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * Method getPathFinder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RouteFinder getPathFinder() {
        return (status == STATUS_PROCESSING_LONG)
               ? this.factory.getThreadedRouteFinder()
               : this.factory.getRealtimeRouteFinder();
    }

    /**
     * Method getDestination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getDestination() {
        return destination;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * Method clone
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param job
     */
    public void clone(RouteFindJob job) {
        this.player      = job.getPlayer();
        this.destination = job.getDestination();
        this.attachments = job.attachments;
        this.finder      = job.finder;
        this.factory     = job.factory;
    }

    /**
     * Method setAttachment
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param o
     */
    public void setAttachment(int id, Object o) {
        this.attachments.put(id, o);
    }

    /**
     * Method getAttachment
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public Object getAttachment(int id) {
        return attachments.get(id);
    }
}
