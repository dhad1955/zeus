package net.tazogaming.hydra.game.routefinding;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/07/14
 * Time: 18:00
 */
public interface RouteFinderFactory {
    public static final int
        WALKING     = 0,
        OBJECT      = 1,
        POH_WALKING = 2,
        POH_OBJECT  = 3;

    /**
     * Method getThreadedRouteFinder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RouteFinder getThreadedRouteFinder();

    /**
     * Method getRealtimeRouteFinder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RouteFinder getRealtimeRouteFinder();
}
