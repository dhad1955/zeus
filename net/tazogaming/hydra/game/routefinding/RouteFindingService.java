package net.tazogaming.hydra.game.routefinding;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.Server;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.routefinding.impl.*;

//~--- JDK imports ------------------------------------------------------------

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/07/14
 * Time: 01:21
 */
public class RouteFindingService extends Thread {

    /** factories made: 14/08/23 **/
    private RouteFinderFactory[] factories = new RouteFinderFactory[8];

    /** jobList made: 14/08/23 **/
    private final Queue<RouteFindJob> jobList = new LinkedBlockingQueue<RouteFindJob>();

    /** processing made: 14/08/23 **/
    private RouteFindJob processing = null;

    /** isSleeping made: 14/08/23 **/
    private boolean isSleeping = false;

    /**
     * Constructs ...
     *
     */
    public RouteFindingService() {
        Server.newThread(this, "PathFindService", Thread.NORM_PRIORITY);
        setFactory(RouteFinderFactory.OBJECT, new ObjectPathFinderFactory());
        setFactory(RouteFinderFactory.WALKING, new WalkPathFinderFactory());
        setFactory(RouteFinderFactory.POH_WALKING, new DynamicRegionPFFactory());
        setFactory(RouteFinderFactory.POH_OBJECT, new DynamicObjectPathFindFactory());
    }

    /**
     * Method setFactory
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param factory
     */
    public void setFactory(int id, RouteFinderFactory factory) {
        this.factories[id] = factory;
    }

    /**
     * Method wakeUp
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void wakeUp() {
        synchronized (this) {
            this.notifyAll();
        }
    }

    private boolean updateCurrentJob(RouteFindJob job) {
        synchronized (this.jobList) {
            for (RouteFindJob queuedJob : jobList) {
                if (job.getPlayer() == queuedJob.getPlayer()) {
                    queuedJob.clone(job);
                    queuedJob.setStatus(RouteFindJob.STATUS_QUEUED);
                }
            }
        }

        return false;
    }

    private void submitJob(RouteFindJob job) {
        if (updateCurrentJob(job)) {
            return;
        }

        synchronized (this.jobList) {
            this.jobList.offer(job);
        }

        this.wakeUp();
    }

    /**
     * Method walkToLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param x
     * @param y
     */
    public void walkToLocation(Player player, int x, int y) {
        removePathIfExists(player);
        runJob(createWalkingJob(player, x, y), false);
    }

    /**
     * Method walkToObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param id
     * @param x
     * @param y
     */
    public void walkToObject(Player player, int id, int x, int y) {
        removePathIfExists(player);

        RouteFindJob objPathFinder = createObjectJob(player, id, x, y);

        runJob(objPathFinder, false);
    }


    public RouteFindingResult getQuickResult(Player player, Point location, boolean poh) {
        RouteFinderFactory factory =  factories[poh ? RouteFinderFactory.POH_WALKING : RouteFinderFactory.WALKING];

        RouteFindJob job  = new RouteFindJob(factory, location, player);
        if(factory instanceof DynamicRegionPFFactory)
            job.setAttachment(0, player.getCurrentHouse().getClippingMap());

        return factory.getRealtimeRouteFinder().findPath(job);

    }
    /**
     * Method runJob
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param job
     * @param secondAttempt
     */
    public void runJob(RouteFindJob job, boolean secondAttempt) {


        try {
            if (secondAttempt) {
                job.setStatus(RouteFindJob.STATUS_PROCESSING_LONG);
            }

            RouteFindingResult result = job.getPathFinder().findPath(job);

            job.getPlayer().setActionsDisabled(true);

            if (!secondAttempt && (result.getType() == RouteFindingResult.ROUTE_TAKING_TO_LONG)) {
                submitJob(job);

                return;
            } else if ((result.getType() == RouteFindingResult.ROUTE_CANT_BE_REACHED)
                       || (result.getType() == RouteFindingResult.ROUTE_TAKING_TO_LONG)) {
                job.getPlayer().getActionSender().sendMessage("I can't reach that.");
                job.getPlayer().setActionsDisabled(false);

                return;
            } else {
                job.getPlayer().setActionsDisabled(false);

                if ((job.getPathFinder() instanceof ObjectPathFinder)
                        || (job.getPathFinder() instanceof DynamicObjectPathFinder)) {
                    job.getPlayer().getRequest().setRequestLocation(result.getDestination());
                }

                if (result.getPoints() != null) {
                    job.getPlayer().onPathFinished(result.getPoints());
                }
            }
        } catch (Exception ee) {
            job.getPlayer().setActionsDisabled(false);
            ee.printStackTrace();
        }
    }

    /**
     * Method removePathIfExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     *
     * @return
     */
    public boolean removePathIfExists(Player player) {
        if ((processing != null) && (processing.getPlayer() == player)) {
            processing.setStatus(RouteFindJob.STATUS_CANCELLED);

            return true;
        }

        synchronized (jobList) {
            for (RouteFindJob job : jobList) {
                if (job.getPlayer() == player) {
                    job.setStatus(RouteFindJob.STATUS_CANCELLED);

                    return true;
                }
            }
        }

        return false;
    }



    private RouteFindJob createObjectJob(Player player, int id, int x, int y) {
        if (!player.is_con) {
            RouteFindJob job = new RouteFindJob(factories[RouteFinderFactory.OBJECT],
                                              Point.location(x, y, player.getHeight()), player);

            job.setAttachment(0, id);

            return job;
        } else {
            RouteFindJob job = new RouteFindJob(factories[RouteFinderFactory.POH_OBJECT],
                                              Point.location(x, y, player.getHeight()), player);

            job.setAttachment(0, player.getCurrentHouse().getClippingMap());
            job.setAttachment(1, player.getCurrentHouse());
            job.setAttachment(2, id);

            return job;
        }
    }

    private RouteFindJob createWalkingJob(Player player, int x, int y) {
        RouteFindJob job = null;

        if (player.is_con) {
            job = new RouteFindJob(factories[RouteFinderFactory.POH_WALKING], Point.location(x, y, player.getHeight()),
                                  player);
            job.setAttachment(0, player.getCurrentHouse().getClippingMap());

            return job;
        }

        return new RouteFindJob(factories[player.is_con
                                         ? RouteFinderFactory.POH_WALKING
                                         : RouteFinderFactory.WALKING], Point.location(x, y, player.getHeight()),
                                         player);
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void run() {
        do {
            try {
                if (processing != null) {
                    runJob(processing, true);
                    processing = null;
                }

                if (jobList.isEmpty()) {
                    try {
                        this.isSleeping = true;

                        synchronized (this) {
                            this.wait();
                        }

                        this.isSleeping = false;
                    } catch (InterruptedException ee) {
                        this.isSleeping = false;
                    }
                } else {
                    synchronized (jobList) {
                        this.processing = jobList.poll();
                    }
                }
            } catch (Exception ee) {
                ee.printStackTrace();
            }
        } while (true);
    }
}
