package net.tazogaming.hydra.game.routefinding;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.map.Point;

//~--- JDK imports ------------------------------------------------------------

import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/07/14
 * Time: 01:08
 */
public class RouteFindingResult {
    public static final int
        NO_ROUTE_NEEDED       = 0,
        ROUTE_CANT_BE_REACHED = 1,
        ROUTE_TAKING_TO_LONG  = 2,
        ROUTE_FOUND           = 3;
    private Queue<Point> points;
    private Point        dest;
    private int          type;

    /**
     * Constructs ...
     *
     *
     * @param opcode
     */
    public RouteFindingResult(int opcode) {
        this(opcode, null, null);
    }

    /**
     * Constructs ...
     *
     *
     * @param dest
     */
    public RouteFindingResult(Point dest) {
        this.points = new LinkedBlockingQueue<Point>();
        points.offer(dest);
        this.type = ROUTE_FOUND;
        this.dest = dest;
    }

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param points
     * @param dest
     */
    public RouteFindingResult(int type, Queue<Point> points, Point dest) {
        this.type   = type;
        this.points = points;
        this.dest   = dest;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Method getPoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Queue<Point> getPoints() {
        return points;
    }

    /**
     * Method setPoints
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param points
     */
    public void setPoints(Queue<Point> points) {
        this.points = points;
    }

    /**
     * Method setDestination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void setDestination(Point p) {
        this.dest = p;
    }

    /**
     * Method getDestination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Point getDestination() {
        return dest;
    }
}
