package net.tazogaming.hydra.game.routefinding.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.routefinding.RouteFinder;
import net.tazogaming.hydra.game.routefinding.RouteFinderFactory;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 06/08/14
 * Time: 19:00
 */
public class DynamicRegionPFFactory implements RouteFinderFactory {

    /**
     * Method getThreadedRouteFinder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public RouteFinder getThreadedRouteFinder() {
        return new DynamicRegionPathFinder(WalkPathFinderFactory.LONG_MAX_ATTEMPTS,
                                           WalkPathFinderFactory.LONG_MAX_PROCESSINGTIME);
    }

    /**
     * Method getRealtimeRouteFinder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public RouteFinder getRealtimeRouteFinder() {
        return new DynamicRegionPathFinder(WalkPathFinderFactory.REALTIME_MAX_ATTEMPTS,
                                           WalkPathFinderFactory.REALTIME_MAX_PROCESSINGTIME);
    }
}
