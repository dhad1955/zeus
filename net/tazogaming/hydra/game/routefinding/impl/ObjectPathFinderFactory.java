package net.tazogaming.hydra.game.routefinding.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.game.routefinding.RouteFinder;
import net.tazogaming.hydra.game.routefinding.RouteFinderFactory;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/07/14
 * Time: 18:21
 */
public class ObjectPathFinderFactory implements RouteFinderFactory {

    /*
     * Realtime max processing times and attempts, adjust this wisely!
     */
    public static final int REALTIME_MAX_ATTEMPTS       = 2000;
    public static final int REALTIME_MAX_PROCESSINGTIME = 11;

    /*
     * Long route finder for threaded path requests, note that we still dont want
     * a huge time on this, maybe x2 of what the short time is
     */
    public static final int LONG_MAX_ATTEMPTS       = 4500;
    public static final int LONG_MAX_PROCESSINGTIME = 38;

    /**
     * Method getThreadedRouteFinder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public RouteFinder getThreadedRouteFinder() {
        return new ObjectPathFinder(LONG_MAX_ATTEMPTS, LONG_MAX_PROCESSINGTIME);
    }

    /**
     * Method getRealtimeRouteFinder
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public RouteFinder getRealtimeRouteFinder() {
        return new ObjectPathFinder(REALTIME_MAX_ATTEMPTS, REALTIME_MAX_PROCESSINGTIME);
    }
}
