package net.tazogaming.hydra.game.routefinding.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.game.routefinding.RouteFindJob;
import net.tazogaming.hydra.game.routefinding.RouteFinder;
import net.tazogaming.hydra.game.routefinding.RouteFindingResult;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/07/14
 * Time: 18:08
 */
public class WalkingPathFinder implements RouteFinder {
    private boolean isLong = false;
    private int     maxAttempts;
    private int     getMaxProcessingTime;

    /**
     * Constructs ...
     *
     *
     * @param maxAttempts
     * @param getMaxProcessingTime
     * @param isLong
     */
    public WalkingPathFinder(int maxAttempts, int getMaxProcessingTime, boolean isLong) {
        this.maxAttempts          = maxAttempts;
        this.getMaxProcessingTime = getMaxProcessingTime;
        this.isLong               = isLong;
    }

    /**
     * Method findPath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param job
     *
     * @return
     */
    @Override
    public RouteFindingResult findPath(RouteFindJob job) {
        Player player = job.getPlayer();

        if (directRoute(player.getLocation(), job.getDestination(), job.getPlayer())) {
            return new RouteFindingResult(job.getDestination());
        }


        return findRoute(player.getLocation(), job.getDestination(), job);
    }

    private boolean directRoute(Point from, Point to, Player entity) {
        Point current = from;

        long time = Core.currentTimeMillis();

        int attempts = 0;
        while ((current.getX() != to.getX()) || (current.getY() != to.getY())) {
           if(++attempts > 15)
               return false;

            int dir = Movement.getDirectionForWaypoints(current, to);

            if (IfClipped.getMovementStatus(dir, current.getX(), current.getY(), current.getHeight()) == 0) {
                return false;
            }

            current = Movement.getPointForDir(current, dir);

            if(ClanWar.wallClipped(entity, current))
                return false;
        }

        return true;
    }

    /**
     * Method findRoute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param job
     *
     * @return
     */
    public RouteFindingResult findRoute(Point from, Point to, RouteFindJob job) {
        int                        h     = from.getHeight();
        LinkedBlockingQueue<Point> queue = new LinkedBlockingQueue<Point>();
        long                       start = System.currentTimeMillis();
        int                        destX = to.getX();
        int                        destY = to.getY();

        destX = destX - 8 * from.getRegionX();
        destY = destY - 8 * from.getRegionY();


        int[][]             via        = new int[104][104];
        int[][]             cost       = new int[104][104];
        ArrayList<Integer> tileQueueX = new ArrayList<Integer>(60);
        ArrayList<Integer> tileQueueY = new ArrayList<Integer>(60);


        for (int xx = 0; xx < 104; xx++) {
            for (int yy = 0; yy < 104; yy++) {
                cost[xx][yy] = 99999999;
            }
        }

        int curX = from.getLocalX();
        int curY = from.getLocalY();

        via[curX][curY]  = 99;
        cost[curX][curY] = 0;

        int tail = 0;

        tileQueueX.add(curX);
        tileQueueY.add(curY);

        int     pathLength  = 10000;
        boolean foundPath   = false;
        int     maxAttempts = 60000;
        int     count       = 0;
        int     curAbsX     = 0;
        int     curAbsY     = 0;
        int     thisCost    = 0;

        while ((tail != tileQueueX.size()) && (tileQueueX.size() < pathLength)) {
            if ((++count == maxAttempts) || (System.currentTimeMillis() - start > getMaxProcessingTime)) {
                if (!this.isLong) {
                    return new RouteFindingResult(RouteFindingResult.ROUTE_TAKING_TO_LONG);
                }

                break;
            }

            curX    = tileQueueX.get(tail);
            curY    = tileQueueY.get(tail);
            curAbsX = from.getRegionX() * 8 + curX;
            curAbsY = from.getRegionY() * 8 + curY;

            if ((curX == destX) && (curY == destY)) {
                foundPath = true;

                if (job.getStatus() == RouteFindJob.STATUS_PROCESSING_LONG) {
                    break;
                }

                break;
            }



            tail     = (tail + 1) % pathLength;
            thisCost = cost[curX][curY] + 1;

            if ((curY > 0) && (via[curX][curY - 1] == 0) && (!ClippingDecoder.blockedSouth(curAbsX, curAbsY, h))) {
                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX, curAbsY-1, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX);
                tileQueueY.add(curY - 1);
                via[curX][curY - 1]  = 1;
                cost[curX][curY - 1] = thisCost;
            }

            if ((curX > 0) && (via[curX - 1][curY] == 0) && (!ClippingDecoder.blockedWest(curAbsX, curAbsY, h))) {
                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX - 1, curAbsY, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY);
                via[curX - 1][curY]  = 2;
                cost[curX - 1][curY] = thisCost;
            }

            if ((curY < 104 - 1) && (via[curX][curY + 1] == 0)
                    && (!ClippingDecoder.blockedNorth(curAbsX, curAbsY, h))) {
                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX, curAbsY + 1, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX);
                tileQueueY.add(curY + 1);
                via[curX][curY + 1]  = 4;
                cost[curX][curY + 1] = thisCost;
            }

            if ((curX < 104 - 1) && (via[curX + 1][curY] == 0) && (!ClippingDecoder.blockedEast(curAbsX, curAbsY, h))) {
                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX + 1, curAbsY, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY);
                via[curX + 1][curY]  = 8;
                cost[curX + 1][curY] = thisCost;
            }

            if ((curX > 0) && (curY > 0) && (via[curX - 1][curY - 1] == 0)
                    && (!ClippingDecoder.blockedSouthWest(curAbsX, curAbsY, h))) {
                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX - 1, curAbsY - 1, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY - 1);

                via[curX - 1][curY - 1]  = 3;
                cost[curX - 1][curY - 1] = thisCost;
            }

            if ((curX > 0) && (curY < 104 - 1) && (via[curX - 1][curY + 1] == 0)
                    && (!ClippingDecoder.blockedNorthWest(curAbsX, curAbsY, h))) {
                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX - 1, curAbsY + 1, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY + 1);
                via[curX - 1][curY + 1]  = 6;
                cost[curX - 1][curY + 1] = thisCost;
            }

            if ((curX < 104 - 1) && (curY > 0) && (via[curX + 1][curY - 1] == 0)
                    && (!ClippingDecoder.blockedSouthEast(curAbsX, curAbsY, h))) {

                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX + 1, curAbsY - 1, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY - 1);
                via[curX + 1][curY - 1]  = 9;
                cost[curX + 1][curY - 1] = thisCost;
            }

            if ((curX < 104 - 1) && (curY < 104 - 1) && (via[curX + 1][curY + 1] == 0)
                    && (!ClippingDecoder.blockedNorthEast(curAbsX, curAbsY, h))) {
                if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX + 1, curAbsY + 1, job.getPlayer().getHeight()))){
                    continue;
                }
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY + 1);
                via[curX + 1][curY + 1]  = 12;
                cost[curX + 1][curY + 1] = thisCost;
            }
        }

        if (!foundPath) {

            int i_223_ = 1000;

            thisCost = 100;

            int i_225_ = 10;

            for (int x = destX - i_225_; x <= destX + i_225_; x++) {
                for (int y = destY - i_225_; y <= destY + i_225_; y++) {
                    if ((x >= 0) && (y >= 0) && (x < 104) && (y < 104) && (cost[x][y] < 100)) {
                        int i_228_ = 0;

                        if (x < destX) {
                            i_228_ = destX - x;
                        } else if (x > destX + 1 - 1) {
                            i_228_ = x - (destX + 1 - 1);
                        }

                        int i_229_ = 0;

                        if (y < destY) {
                            i_229_ = destY - y;
                        } else if (y > destY + 1 - 1) {
                            i_229_ = y - (destY + 1 - 1);
                        }

                        int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;

                        if ((i_230_ < i_223_) || ((i_230_ == i_223_) && (cost[x][y] < thisCost))) {
                            i_223_   = i_230_;
                            thisCost = cost[x][y];
                            curX     = x;
                            curY     = y;
                        }
                    }
                }
            }

            if (i_223_ == 1000) {
                return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED);
            }
        }

        tail = 0;
        tileQueueX.set(tail, curX);
        tileQueueY.set(tail++, curY);

        int l5;

        for (int j5 = l5 = via[curX][curY]; (curX != from.getLocalX()) || (curY != from.getLocalY());
                j5 = via[curX][curY]) {
            if (j5 != l5) {
                l5 = j5;
                tileQueueX.set(tail, curX);
                tileQueueY.set(tail++, curY);
            }

            if ((j5 & 2) != 0) {
                curX++;
            } else if ((j5 & 8) != 0) {
                curX--;
            }

            if ((j5 & 1) != 0) {
                curY++;
            } else if ((j5 & 4) != 0) {
                curY--;
            }
        }

        int   size = tail--;
        Point dest = (Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), from.getRegionX(), from.getRegionY(), h));

        queue.offer(dest);

        Point last = null;


        for (int i = 1; i < size; i++) {
            tail--;

            Point p2 = Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), from.getRegionX(), from.getRegionY(), h);

            queue.offer(p2);
            last = p2;
        }


        return new RouteFindingResult(RouteFindingResult.ROUTE_FOUND, queue, last);
    }
}
