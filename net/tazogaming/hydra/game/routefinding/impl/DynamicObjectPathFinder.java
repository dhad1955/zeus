package net.tazogaming.hydra.game.routefinding.impl;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/07/14
 * Time: 01:57
 */
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.game.routefinding.RouteFindJob;
import net.tazogaming.hydra.game.routefinding.RouteFinder;
import net.tazogaming.hydra.game.routefinding.RouteFindingResult;
import net.tazogaming.hydra.game.skill.construction.House;
import net.tazogaming.hydra.game.skill.construction.obj.ObjectNode;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.map.*;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.runtime.Core;

//~--- JDK imports ------------------------------------------------------------

import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/07/14
 * Time: 01:09
 */
public class DynamicObjectPathFinder implements RouteFinder {
    public static final int DEFAULT_MAX_ATTEMPTS        = 700;
    public static final int DEFAULT_MAX_PROCESSING_TIME = 11;    // max processing time

    /** maxAttempts made: 15/02/18 **/
    private int maxAttempts = DEFAULT_MAX_ATTEMPTS;

    /** getMaxProcessingTime made: 15/02/18 **/
    private int getMaxProcessingTime = DEFAULT_MAX_PROCESSING_TIME;

    /**
     * Constructs ...
     *
     *
     * @param max
     * @param processTime
     */
    public DynamicObjectPathFinder(int max, int processTime) {
        this.maxAttempts          = max;
        this.getMaxProcessingTime = processTime;
    }

    /**
     * Method getMovementStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param direction
     * @param tileX
     * @param tileY
     * @param height
     * @param map
     *
     * @return
     */
    public int getMovementStatus(int direction, int tileX, int tileY, int height, DynamicClippingMap map) {
        switch (direction) {
        case Movement.NORTH :
            if (map.blockedNorth(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH :
            if (map.blockedSouth(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.EAST :
            if (map.blockedEast(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.WEST :
            if (map.blockedWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.NORTH_EAST :
            if (map.blockedNorthEast(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.NORTH_WEST :
            if (map.blockedNorthWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH_WEST :
            if (map.blockedSouthWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH_EAST :
            if (map.blockedSouthEast(tileX, tileY, height)) {
                return 0;
            }

            break;
        }

        return 1;
    }

    private boolean reachedStairsObject(int x, int y, int curx, int cury, int sizex, int sizey, int rotation) {
        int rotationDeltaY = 0,
            rotationDeltaX = 0;

        switch (rotation) {
        case 0 :
            rotationDeltaY = -1;
            sizey          = 0;

            break;

        case 2 :
            rotationDeltaY = 1;
            sizey          = 0;

            break;

        case 1 :
            rotationDeltaX = -1;
            sizex          = 0;

            break;

        case 3 :
            rotationDeltaX = 1;
            sizex          = 0;

            break;
        }

        for (int xx = x + rotationDeltaX; xx < x + sizex; xx++) {
            for (int yy = y + rotationDeltaY; yy < y + sizey; yy++) {
                if ((xx == curx) && (yy == cury)) {
                    return true;
                }
            }
        }

        return false;
    }

    private boolean reachedStairsOrFacingObject(boolean stairs, int curx, int cury, int x, int y, int sizex, int sizey,
            int rotationInfo, int rawRotation, int h, DynamicClippingMap map) {
        if (stairs) {
            return reachedStairsObject(x, y, curx, cury, sizex, sizey, rawRotation);
        }

        return map.reachedFacingObject(y, x, curx, sizey, rotationInfo, cury, sizex, h);
    }

    private Point directRoute(Point loc, Point request, int size_x, int size_y, int rotationInfo,
                              DynamicClippingMap map, Player player, boolean stairs, int rawRotation) {
        Point current = loc;
        long  time    = Core.currentTimeMillis();

        size_x      = -1;
        size_y      = -1;
        rawRotation = 1;

        int attempts = 0;

        while (!reachedStairsOrFacingObject(stairs, current.getX(), current.getY(), request.getX(), request.getY(),
                size_x, size_y, rotationInfo, rawRotation, request.getHeight(), map)) {
            if ((current.getX() == request.getX()) && (current.getY() == request.getY())) {
                return null;
            }

            if (System.currentTimeMillis() - time > 100) {
                throw new RuntimeException("Error slow waypoints: " + loc + " " + request + " "
                                           + (System.currentTimeMillis() - time) + " " + size_x + " " + size_y + " "
                                           + current + " " + stairs);
            }

            int dir = Movement.getDirectionForWaypoints(current, request);

            if (getMovementStatus(dir, Point.getLocalX(player, current), Point.getLocalY(player, current),
                                  current.getHeight(), map) == 0) {
                return null;
            }

            current = Movement.getPointForDir(current, dir);
        }

        return current;
    }

    /**
     * Method findPath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param job
     *
     * @return
     */
    @Override
    public RouteFindingResult findPath(RouteFindJob job) {
        Player             player       = job.getPlayer();
        DynamicClippingMap map          = (DynamicClippingMap) job.getAttachment(0);
        House              currentHouse = (House) job.getAttachment(1);
        int                objectID     = (Integer) job.getAttachment(2);
        int                destHouseX   = Point.getLocalX(player, job.getDestination());
        int                destHouseY   = Point.getLocalY(player, job.getDestination());
        ObjectNode         obj          = currentHouse.getObjectNode(objectID, destHouseX, destHouseY,
                                              player.getHeight());

        if (obj == null) {
            player.getActionSender().sendMessage("Object doesnt exist!");

            return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED);
        }

        int                   rotation = obj.getObject().getRotation();
        CacheObjectDefinition def      = CacheObjectDefinition.forID(objectID);
        int                   size_x   = def.sizeX;
        int                   size_y   = def.sizeY;

        switch (rotation) {
        case 1 :
        case 3 :
            size_x = def.sizeY;
            size_y = def.sizeX;

            break;
        }

        int flag = def.getWalkBit();

        if (rotation != 0) {
            flag = (flag << rotation * 0xF) + (flag >> 4 - rotation);    // lol what the fuck jagex
        }

        boolean stairs = (def.getName().toLowerCase().contains("stair"));

        if (map.reachedFacingObject(destHouseY, destHouseX, Point.getLocalX(player, player.getLocation()), size_y,
                                    flag, size_x, Point.getLocalY(player, player.getLocation()), player.getHeight())) {
            return new RouteFindingResult(RouteFindingResult.NO_ROUTE_NEEDED, null, player.getLocation());
        }

        Point p = directRoute(player.getLocation(), job.getDestination(), size_x, size_y, flag, map, player, stairs,
                              rotation);

        if (p != null) {
            return new RouteFindingResult(p);
        }

        return findRoute(player, player.getLocation(), job.getDestination(), map, job, size_x, size_y, flag, stairs,
                         rotation);
    }

    /**
     * Method findRoute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param from
     * @param to
     * @param map
     * @param job
     * @param size_x
     * @param size_y
     * @param flag
     * @param stairs
     * @param rawRotation
     *
     * @return
     */
    public RouteFindingResult findRoute(Player player, Point from, Point to, DynamicClippingMap map, RouteFindJob job,
            int size_x, int size_y, int flag, boolean stairs, int rawRotation) {
        int                        h          = from.getHeight();
        LinkedBlockingQueue<Point> queue      = new LinkedBlockingQueue<Point>();
        long                       start      = System.currentTimeMillis();
        int                        destX      = Point.getLocalX(player, to);
        int                        destY      = Point.getLocalY(player, to);
        int[][]                    via        = new int[104][104];
        int[][]                    cost       = new int[104][104];
        LinkedList<Integer>        tileQueueX = new LinkedList<Integer>();
        LinkedList<Integer>        tileQueueY = new LinkedList<Integer>();

        for (int xx = 0; xx < 104; xx++) {
            for (int yy = 0; yy < 104; yy++) {
                cost[xx][yy] = 99999999;
            }
        }

        int curX = Point.getLocalX(player, from);    // from.getLocalX();
        int curY = Point.getLocalY(player, from);

        via[curX][curY]  = 99;
        cost[curX][curY] = 0;

        int tail = 0;

        tileQueueX.add(curX);
        tileQueueY.add(curY);

        int     pathLength  = 4000;
        boolean foundPath   = false;
        int     maxAttempts = 60000;
        int     count       = 0;

        while ((tail != tileQueueX.size()) && (tileQueueX.size() < pathLength)) {
            if ((++count == this.maxAttempts) || (System.currentTimeMillis() - start > getMaxProcessingTime)) {
                return new RouteFindingResult(RouteFindingResult.ROUTE_TAKING_TO_LONG);
            }

            curX = tileQueueX.get(tail);
            curY = tileQueueY.get(tail);

            if (reachedStairsOrFacingObject(stairs, curX, curY, destX, destY, size_x, size_y, flag, rawRotation, h,
                                            map)) {
                foundPath = true;

                break;
            }

            tail = (tail + 1) % pathLength;

            int thisCost = cost[curX][curY] + 1;

            if ((curY > 0) && (via[curX][curY - 1] == 0) && (!map.blockedSouth(curX, curY, h))) {
                tileQueueX.add(curX);
                tileQueueY.add(curY - 1);
                via[curX][curY - 1]  = 1;
                cost[curX][curY - 1] = thisCost;
            }

            if ((curX > 0) && (via[curX - 1][curY] == 0) && (!map.blockedWest(curX, curY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY);
                via[curX - 1][curY]  = 2;
                cost[curX - 1][curY] = thisCost;
            }

            if ((curY < 104 - 1) && (via[curX][curY + 1] == 0) && (!map.blockedNorth(curX, curY, h))) {
                tileQueueX.add(curX);
                tileQueueY.add(curY + 1);
                via[curX][curY + 1]  = 4;
                cost[curX][curY + 1] = thisCost;
            }

            if ((curX < 104 - 1) && (via[curX + 1][curY] == 0) && (!map.blockedEast(curX, curY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY);
                via[curX + 1][curY]  = 8;
                cost[curX + 1][curY] = thisCost;
            }

            if ((curX > 0) && (curY > 0) && (via[curX - 1][curY - 1] == 0) && (!map.blockedSouthWest(curX, curY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY - 1);
                via[curX - 1][curY - 1]  = 3;
                cost[curX - 1][curY - 1] = thisCost;
            }

            if ((curX > 0) && (curY < 104 - 1) && (via[curX - 1][curY + 1] == 0)
                    && (!map.blockedNorthWest(curX, curY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY + 1);
                via[curX - 1][curY + 1]  = 6;
                cost[curX - 1][curY + 1] = thisCost;
            }

            if ((curX < 104 - 1) && (curY > 0) && (via[curX + 1][curY - 1] == 0)
                    && (!map.blockedSouthEast(curX, curY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY - 1);
                via[curX + 1][curY - 1]  = 9;
                cost[curX + 1][curY - 1] = thisCost;
            }

            if ((curX < 104 - 1) && (curY < 104 - 1) && (via[curX + 1][curY + 1] == 0)
                    && (!map.blockedNorthEast(curX, curY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY + 1);
                via[curX + 1][curY + 1]  = 12;
                cost[curX + 1][curY + 1] = thisCost;
            }
        }

        if (!foundPath) {
            int i_223_   = 1000;
            int thisCost = 100;
            int i_225_   = 10;

            for (int x = destX - i_225_; x <= destX + i_225_; x++) {
                for (int y = destY - i_225_; y <= destY + i_225_; y++) {
                    if ((x >= 0) && (y >= 0) && (x < 104) && (y < 104) && (cost[x][y] < 100)) {
                        int i_228_ = 0;

                        if (x < destX) {
                            i_228_ = destX - x;
                        } else if (x > destX + 1 - 1) {
                            i_228_ = x - (destX + 1 - 1);
                        }

                        int i_229_ = 0;

                        if (y < destY) {
                            i_229_ = destY - y;
                        } else if (y > destY + 1 - 1) {
                            i_229_ = y - (destY + 1 - 1);
                        }

                        int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;

                        if ((i_230_ < i_223_) || ((i_230_ == i_223_) && (cost[x][y] < thisCost))) {
                            i_223_   = i_230_;
                            thisCost = cost[x][y];
                            curX     = x;
                            curY     = y;
                        }
                    }
                }
            }

            if (i_223_ == 1000) {
                return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED);
            }
        }

        tail = 0;
        tileQueueX.set(tail, curX);
        tileQueueY.set(tail++, curY);

        int l5;

        for (int j5 = l5 = via[curX][curY];
                (curX != Point.getLocalX(player, from)) || (curY != Point.getLocalY(player, from));
                j5 = via[curX][curY]) {
            if (j5 != l5) {
                l5 = j5;
                tileQueueX.set(tail, curX);
                tileQueueY.set(tail++, curY);
            }

            if ((j5 & 2) != 0) {
                curX++;
            } else if ((j5 & 8) != 0) {
                curX--;
            }

            if ((j5 & 1) != 0) {
                curY++;
            } else if ((j5 & 4) != 0) {
                curY--;
            }
        }

        int   size = tail--;
        Point dest = (Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), player.getKnownRegion()[0],
                                player.getKnownRegion()[1], h));

        queue.offer(dest);

        Point last = null;

        for (int i = 1; i < size; i++) {
            tail--;

            Point p2 = Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), player.getKnownRegion()[0],
                                 player.getKnownRegion()[1], h);

            queue.offer(p2);
            last = p2;
        }

        return new RouteFindingResult(RouteFindingResult.ROUTE_FOUND, queue, (last == null)
                ? dest
                : last);
    }
}
