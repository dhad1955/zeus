package net.tazogaming.hydra.game.routefinding.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.io.js5.format.CacheObjectDefinition;
import net.tazogaming.hydra.game.routefinding.RouteFindJob;
import net.tazogaming.hydra.game.routefinding.RouteFinder;
import net.tazogaming.hydra.game.routefinding.RouteFindingResult;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Tile;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.instr.condition.IfClipped;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/07/14
 * Time: 18:08
 */
public class ObjectPathFinder implements RouteFinder {
    public static final int[] UNREACHABLE_OBJECTS = new int[] { 3566, 43526, 2283, 43529 };

    /** maxAttempts made: 14/08/22 */
    private int maxAttempts;

    /** getMaxProcessingTime made: 14/08/22 */
    private int getMaxProcessingTime;

    /**
     * Constructs ...
     *
     *
     * @param maxAttempts
     * @param getMaxProcessingTime
     */
    public ObjectPathFinder(int maxAttempts, int getMaxProcessingTime) {
        this.maxAttempts          = maxAttempts;
        this.getMaxProcessingTime = getMaxProcessingTime;
    }

    /**
     * Method findPath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param job
     *
     * @return
     */
    @Override
    public RouteFindingResult findPath(RouteFindJob job) {
        int                   objectId = (Integer) job.getAttachment(0);
        Point                 location = job.getDestination();
        Player                player   = job.getPlayer();
        CacheObjectDefinition def      = CacheObjectDefinition.forID(objectId);

        if (player.is_con) {
            return new RouteFindingResult(location);
        }

        if (def == null) {
            return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED, null, null);
        }

        int objectX = location.getX();
        int objectY = location.getY();

        if (Point.getDistance(job.getDestination(), player.getLocation()) > 20) {
            return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED, null, null);
        }

        Tile       t        = World.getWorld().getTile(objectX, objectY, player.getHeight());
        int        x        = player.getX();
        int        y        = player.getY();
        int        size_x   = def.sizeX;
        int        size_y   = def.sizeY;
        GameObject obj      = World.getWorld().getObjectManager().getObject(objectId, objectX, objectY,
                                  player.getHeight());
        int        rotation = -1;
        int        type     = 0;

        if (obj == null) {
            if (!t.hasMappedObject()
                    || (t.hasMappedObject() && ((t.getMappedObjectId() != objectId) &&!t.isHasFarmingPatch()))) {
                player.getActionSender().sendMessage("Bad object, report this bug [" + t.getMappedObjectId() + ", "
                        + objectId + ", " + t.getMappedType() + " " + t.getMappedRotation() + "]");

                return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED, null, null);
            }

            rotation = t.getMappedRotation();
            type     = t.getMappedType();
        } else {
            rotation = obj.getRotation();
            type     = obj.getType();
        }

        switch (rotation) {
        case 1 :
        case 3 :
            size_x = def.sizeY;
            size_y = def.sizeX;

            break;
        }

        int flag = def.getWalkBit();

        if (rotation != 0) {
            flag = (flag << rotation * 0xF) + (flag >> 4 - rotation);    // lol what the fuck jagex
        }

      if(obj != null && obj.getType() != 4 && obj.getType() != 5 && obj.getType() != 6 && obj.getType() != 7 && obj.getType() != 8) {
          if (obj != null && obj.getId() == 2360 && Point.getDistance(obj.getLocation(), player.getLocation()) == 1) {
              return new RouteFindingResult(RouteFindingResult.NO_ROUTE_NEEDED, null, player.getLocation());

          }

          if (obj.getType() != 4 && obj.getType() != 5 && obj.getType() != 6 && obj.getType() != 7 && obj.getType() != 8 && ClippingDecoder.reachedFacingObject(objectY, objectX, x, def.sizeY, flag, size_x, y, player.getHeight())
                  || ((obj != null)
                  && (((obj.getType() < 2)) && (Point.getDistance(obj.getLocation(), player.getLocation()) <= 1)))) {
              return new RouteFindingResult(RouteFindingResult.NO_ROUTE_NEEDED, null, player.getLocation());
          }
      }
        Point p = directRoute(player.getLocation(), Point.location(objectX, objectY, player.getHeight()), size_x,
                              size_y, flag, type, player, rotation);

        if (p != null) {
            return new RouteFindingResult(p);
        }

        return this.findRoute(player.getLocation(), location, size_x, size_y, flag, objectId, type, rotation, job);
    }

    private Point directRoute(Point loc, Point request, int size_x, int size_y, int rotationInfo, int type, Player entity, int realRotation) {
        Point current = loc;

        long time = Core.currentTimeMillis();

        int steps = 0;

        while (!ClippingDecoder.reachedFacingObject(request.getY(), request.getX(), current.getX(), size_y,
                rotationInfo, size_x, current.getY(),
                current.getHeight()) || (((type == 0) || (type == 1)) && (Point.getDistance(request, current) <= 1)) ||

                ((type == 5 || type == 6 || type == 7 || type == 8 || type == 9 || type == 4) &&
                        !ClippingDecoder.reachedWallDecoration(request.getX(), request.getY(), current.getY(), current.getX(), realRotation, type, request.getHeight()))) {
            if ((current.getX() == request.getX()) && (current.getY() == request.getY())) {
                return null;
            }
            if(++steps > 60)
                return null;

            int dir = Movement.getDirectionForWaypoints(current, request);

            if (IfClipped.getMovementStatus(dir, current.getX(), current.getY(), current.getHeight()) == 0) {
                return null;
            }

            current = Movement.getPointForDir(current, dir);
            if(ClanWar.wallClipped(entity, current))
                return null;
        }
        return current;
    }

    /**
     * Method findRoute
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param from
     * @param to
     * @param size_x
     * @param size_y
     * @param rotationInfo
     * @param id
     * @param type
     * @param job
     *
     * @return
     */
    public RouteFindingResult findRoute(Point from, Point to, int size_x, int size_y, int rotationInfo, int id, int type, int rot,
                                    RouteFindJob job) {
        int                        h     = from.getHeight();
        LinkedBlockingQueue<Point> queue = new LinkedBlockingQueue<Point>();
        long                       start = System.currentTimeMillis();
        int                        destX = to.getX();
        int                        destY = to.getY();

        destX = destX - 8 * from.getRegionX();
        destY = destY - 8 * from.getRegionY();

        int[][]             via        = new int[104][104];
        int[][]             cost       = new int[104][104];
        ArrayList<Integer> tileQueueX = new ArrayList<Integer>(100);
        ArrayList<Integer> tileQueueY = new ArrayList<Integer>(100);

        for (int xx = 0; xx < 104; xx++) {
            for (int yy = 0; yy < 104; yy++) {
                cost[xx][yy] = 99999999;
            }
        }

        int curX = from.getLocalX();
        int curY = from.getLocalY();

        via[curX][curY]  = 99;
        cost[curX][curY] = 0;

        int tail = 0;

        tileQueueX.add(curX);
        tileQueueY.add(curY);

        int     pathLength  = 4000;
        boolean foundPath   = false;
        int     maxAttempts = 60000;
        int     count       = 0;

        while ((tail != tileQueueX.size()) && (tileQueueX.size() < pathLength)) {
            if ((++count == this.maxAttempts) || (System.currentTimeMillis() - start > getMaxProcessingTime)) {
                if (job.getStatus() == RouteFindJob.STATUS_PROCESSING_LONG) {
                    break;
                }

                return new RouteFindingResult(RouteFindingResult.ROUTE_TAKING_TO_LONG, null, null);
            }

            curX = tileQueueX.get(tail);
            curY = tileQueueY.get(tail);

            int curAbsX = from.getRegionX() * 8 + curX;
            int curAbsY = from.getRegionY() * 8 + curY;

            if ((curX == destX) && (curY == destY)) {
                foundPath = true;

                break;
            }


            if(ClanWar.wallClipped(job.getPlayer(), Point.location(curAbsX, curAbsY, job.getPlayer().getHeight()))){
                return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED, null, null);
            }

            tail = (tail + 1) % pathLength;

            int thisCost = cost[curX][curY] + 1;

            if (ClippingDecoder.reachedFacingObject(to.getY(), to.getX(), curAbsX, size_y, rotationInfo, size_x,
                    curAbsY, to.getHeight()) && type != 4 && type != 5) {
                foundPath = true;

                break;
            }


            if(type < 10 && type >= 4){

                if(ClippingDecoder.reachedWallDecoration(to.getX(), to.getY(), curAbsY, curAbsX, rot, type, to.getHeight())){
                    foundPath = true;
                    break;
                }
            }

            if (type == 0) {
                if (Point.getDistance(curAbsX, curAbsY, to.getX(), to.getY()) <= 1) {
                    foundPath = true;

                    break;
                }
            }

            if ((curY > 0) && (via[curX][curY - 1] == 0) && (!ClippingDecoder.blockedSouth(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX);
                tileQueueY.add(curY - 1);
                via[curX][curY - 1]  = 1;
                cost[curX][curY - 1] = thisCost;
            }

            if ((curX > 0) && (via[curX - 1][curY] == 0) && (!ClippingDecoder.blockedWest(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY);
                via[curX - 1][curY]  = 2;
                cost[curX - 1][curY] = thisCost;
            }

            if ((curY < 104 - 1) && (via[curX][curY + 1] == 0)
                    && (!ClippingDecoder.blockedNorth(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX);
                tileQueueY.add(curY + 1);
                via[curX][curY + 1]  = 4;
                cost[curX][curY + 1] = thisCost;
            }

            if ((curX < 104 - 1) && (via[curX + 1][curY] == 0) && (!ClippingDecoder.blockedEast(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY);
                via[curX + 1][curY]  = 8;
                cost[curX + 1][curY] = thisCost;
            }

            if ((curX > 0) && (curY > 0) && (via[curX - 1][curY - 1] == 0)
                    && (!ClippingDecoder.blockedSouthWest(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY - 1);
                via[curX - 1][curY - 1]  = 3;
                cost[curX - 1][curY - 1] = thisCost;
            }

            if ((curX > 0) && (curY < 104 - 1) && (via[curX - 1][curY + 1] == 0)
                    && (!ClippingDecoder.blockedNorthWest(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX - 1);
                tileQueueY.add(curY + 1);
                via[curX - 1][curY + 1]  = 6;
                cost[curX - 1][curY + 1] = thisCost;
            }

            if ((curX < 104 - 1) && (curY > 0) && (via[curX + 1][curY - 1] == 0)
                    && (!ClippingDecoder.blockedSouthEast(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY - 1);
                via[curX + 1][curY - 1]  = 9;
                cost[curX + 1][curY - 1] = thisCost;
            }

            if ((curX < 104 - 1) && (curY < 104 - 1) && (via[curX + 1][curY + 1] == 0)
                    && (!ClippingDecoder.blockedNorthEast(curAbsX, curAbsY, h))) {
                tileQueueX.add(curX + 1);
                tileQueueY.add(curY + 1);
                via[curX + 1][curY + 1]  = 12;
                cost[curX + 1][curY + 1] = thisCost;
            }
        }

        if (!foundPath) {
            int i_223_   = 1000;
            int thisCost = 100;
            int i_225_   = 10;

            for (int x = destX - i_225_; x <= destX + i_225_; x++) {
                for (int y = destY - i_225_; y <= destY + i_225_; y++) {
                    if ((x >= 0) && (y >= 0) && (x < 104) && (y < 104) && (cost[x][y] < 100)) {
                        int i_228_ = 0;

                        if (x < destX) {
                            i_228_ = destX - x;
                        } else if (x > destX + 1 - 1) {
                            i_228_ = x - (destX + 1 - 1);
                        }

                        int i_229_ = 0;

                        if (y < destY) {
                            i_229_ = destY - y;
                        } else if (y > destY + 1 - 1) {
                            i_229_ = y - (destY + 1 - 1);
                        }

                        int i_230_ = i_228_ * i_228_ + i_229_ * i_229_;

                        if ((i_230_ < i_223_) || ((i_230_ == i_223_) && (cost[x][y] < thisCost))) {
                            i_223_   = i_230_;
                            thisCost = cost[x][y];
                            curX     = x;
                            curY     = y;
                        }
                    }
                }
            }

            if (i_223_ == 1000) {
                return new RouteFindingResult(RouteFindingResult.ROUTE_CANT_BE_REACHED, null, null);
            }
        }

        tail = 0;
        tileQueueX.set(tail, curX);
        tileQueueY.set(tail++, curY);

        int l5;

        for (int j5 = l5 = via[curX][curY]; (curX != from.getLocalX()) || (curY != from.getLocalY());
                j5 = via[curX][curY]) {
            if (j5 != l5) {
                l5 = j5;
                tileQueueX.set(tail, curX);
                tileQueueY.set(tail++, curY);
            }

            if ((j5 & 2) != 0) {
                curX++;
            } else if ((j5 & 8) != 0) {
                curX--;
            }

            if ((j5 & 1) != 0) {
                curY++;
            } else if ((j5 & 4) != 0) {
                curY--;
            }
        }

        int   size = tail--;
        Point dest = (Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), from.getRegionX(), from.getRegionY(), h));

        queue.offer(dest);

        Point last = null;

        for (int i = 1; i < size; i++) {
            tail--;

            Point p2 = Point.abs(tileQueueX.get(tail), tileQueueY.get(tail), from.getRegionX(), from.getRegionY(), h);

            queue.offer(p2);
            last = p2;
        }

        if (!foundPath &&!isUnreachable(id)) {
            dest = null;
            last = null;
        }

        return new RouteFindingResult(RouteFindingResult.ROUTE_FOUND, queue, (last == null)
                ? dest
                : last);
    }

    private static boolean isUnreachable(int id) {
        for (int i = 0; i < UNREACHABLE_OBJECTS.length; i++) {
            if (UNREACHABLE_OBJECTS[i] == id) {
                return true;
            }
        }

        return false;
    }
}
