package net.tazogaming.hydra.script.build;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/06/14
 * Time: 11:59
 */
public class SourcePosition {
    private String fileName;
    private int    column;
    private String simpleName;
    private int    row;

    /**
     * Constructs ...
     *
     *
     * @param fileName
     * @param column
     * @param row
     */
    public SourcePosition(String fileName, int column, int row) {
        try {
            this.fileName   = fileName.substring("config/scripts/".length());;
            this.simpleName = fileName.substring(fileName.lastIndexOf(File.separator));
        } catch (Exception ee) {
            this.fileName = fileName;
        }

        this.column = column;
        this.row    = row;
    }

    /**
     * Method getFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Method getRow
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRow() {
        return row;
    }

    /**
     * Method getColumn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getColumn() {
        return column;
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        return fileName + " at line " + row;
    }

    /**
     * Method getSimpleName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getSimpleName() {
        return simpleName;
    }
}
