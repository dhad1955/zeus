package net.tazogaming.hydra.script.build;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.script.build.lexer.Lexer;
import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.runtime.*;
import net.tazogaming.hydra.script.runtime.core.ForEachLoop;
import net.tazogaming.hydra.script.runtime.core.ForLoop;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.core.condition.impl.*;
import net.tazogaming.hydra.script.runtime.data.GlobalVariable;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.data.Struct;
import net.tazogaming.hydra.script.runtime.expr.*;
import net.tazogaming.hydra.script.runtime.expr.CallNode;
import net.tazogaming.hydra.script.runtime.instr.ConditionMap;
import net.tazogaming.hydra.script.runtime.instr.ReturnOperationMap;
import net.tazogaming.hydra.script.runtime.instr.op.impl.RuntimeOperationMap;
import net.tazogaming.hydra.script.runtime.js.JPlugin;
import net.tazogaming.hydra.script.runtime.runnable.*;
import net.tazogaming.hydra.script.runtime.runnable.BlockNode;
import net.tazogaming.hydra.script.sched.TimingType;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/06/14
 * Time: 16:43
 */
public class ScriptCompiler {
    public static final int BLOCK_DELIMITER = 5612;

    /*
     * Current block ID
     */

    /** block_id made: 14/08/19 */
    private static int block_id = 0;

    /*
     * Current enum ID
     */

    /** enum_id made: 14/08/19 */
    private static int enum_id = 0;

    /*
     * HashMap containing the indexes of a global list
     */

    /** list_ids made: 14/08/19 */
    private static final HashMap<String, Integer> list_ids = new HashMap<String, Integer>();

    /*
     * Current (Caret) of the global list
     */

    /** list_id made: 14/08/19 */
    private static int list_id = 0;

    /** struct_id made: 14/08/25 */
    private static int struct_id = 0;

    /*
     * HashMap containing the indexes for blocks
     */

    /** block_ids made: 14/08/19 */
    private static final HashMap<String, Integer> block_ids = new HashMap<String, Integer>();

    /*
     * HashMap containing the indexes for enums
     */
    public static final HashMap<String, Integer> enum_ids = new HashMap<String, Integer>();
    public static final HashMap<String, Integer> structs  = new HashMap<String, Integer>();

    /*
     * Local list of logical operators
     */

    /** COMPARATOR_OPERATORS[] made: 14/08/19 */
    private static final int COMPARATOR_OPERATORS[] = {
        Token.TYPE_MORE_THAN, Token.TYPE_MORE_THAN_OR_EQUAL_TO, Token.TYPE_LESS_THAN_OR_EQUAL_TO, Token.TYPE_LESS_THAN,
        Token.NOT_EQUAL_TO, Token.TYPE_EQUALS
    };

    /*
     * Constants loaded from constants.rs
     */

    /** global_constants made: 14/08/19 */
    private static final HashMap<String, String> global_constants      = new HashMap<String, String>();
    public static final String                   VARIABLE_REFERENCER   = "$";
    public static final String                   PLAYER_VAR_REFERENCER = "%";
    public static final String                   CONCENTATION_BEGIN    = "{";
    public static final String                   CONCENTATION_END      = "}";
    public static final String                   VAR_DECLARATION       = "var";
    public static final String                   GLOBALVAR_DECLARATION = "global_var";
    public static final String                   TRIGGER_NAME          = "directTrigger";
    public static final String                   BLOCK_NAMES           = "block";

    /*
     * Current caret(pointer) of the current token list
     */

    /** current made: 14/08/19 */
    private int current = 0;

    /*
     * Local constants inside the script file
     */

    /** constants made: 14/08/19 */
    private HashMap<String, String> constants = new HashMap<String, String>();

    /*
     * List of javascript plugins within the current scope
     */

    /** plugins made: 14/08/19 */
    private final ArrayList<JPlugin> plugins = new ArrayList<JPlugin>();

    /** scheduledDelay made: 14/08/19 */
    private int scheduledDelay = 0;

    /** scheduledName made: 14/08/19 */
    private String scheduledName = null;

    /** localFunctions made: 14/08/19 */
    private Map<String, Integer> localFunctions = new HashMap<String, Integer>();

    /** curLocal made: 14/08/19 */
    private int curLocal = 0;

    /** scheduledMode made: 14/08/19 */
    private TimingType scheduledMode;

    /*
     * Current Lexer
     */

    /** lexer made: 14/08/19 */
    private Lexer lexer;

    /*
     * List container the tokens to parse
     */

    /** tokens made: 14/08/19 */
    private ArrayList<Token> tokens;

    /*
     * Current main block that is parsing
     */

    /** currentBlock made: 14/08/19 */
    private ScriptRunnable currentBlock;

    /*
     * Current compiler output for developing
     */

    /** output made: 14/08/19 */
    private ScriptCompilerOutput output;

    /*
     * Current file
     */

    /** file made: 14/08/19 */
    private File file;

    /*
     * Construct a script compiler based of a file without an output
     */

    /** context made: 14/08/19 */
    private Context context;

    /**
     * Constructs ...
     *
     *
     * @param f
     */
    private ScriptCompiler(File f) {
        this.lexer  = new Lexer(f);
        this.tokens = lexer.getTokens();
        this.file   = f;
    }

    /*
     * Construct a script compiler from a file with an output stream
     */

    /**
     * Constructs ...
     *
     *
     * @param file
     * @param output
     */
    private ScriptCompiler(File file, ScriptCompilerOutput output) {
        this.output  = output;
        this.lexer   = new Lexer(file);
        this.tokens  = lexer.getTokens();
        this.context = context;
        this.file    = file;
        lexer.close();
    }

    /*
     * Construct a script compiler based off a token array list
     */

    /**
     * Constructs ...
     *
     *
     * @param tokens
     * @param constants
     * @param context
     * @param functionList
     */
    private ScriptCompiler(ArrayList<Token> tokens, HashMap constants, Context context,
                           Map<String, Integer> functionList) {
        this.tokens         = tokens;
        this.constants      = constants;
        this.context        = context;
        this.localFunctions = functionList;

        if (this.context == null) {
            this.context = new Context("err", 0);
        }

        if (this.localFunctions == null) {
            this.localFunctions = new HashMap<String, Integer>();
        }
    }

    /*
     * Construct a script compiler from an already loaded lexer
     */

    /**
     * Constructs ...
     *
     *
     * @param lexer
     * @param orig
     * @param out
     * @param context2
     * @param localFunctions
     */
    private ScriptCompiler(Lexer lexer, File orig, ScriptCompilerOutput out, Context context2,
                           Map<String, Integer> localFunctions) {
        this.output         = out;
        this.lexer          = lexer;
        this.file           = orig;
        this.context        = context2;
        this.tokens         = lexer.getTokens();
        this.localFunctions = localFunctions;

        if (this.context == null) {
            this.context = new Context("err", 0);
        }

        if (this.localFunctions == null) {
            this.localFunctions = new HashMap<String, Integer>();
        }

        lexer.close();
    }

    /**
     * Method getBlockID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public static int getBlockID(String name) {
        try {
            return block_ids.get(name);
        } catch (Exception ee) {
            return -1;
        }
    }

    /*
     * Load constants
     * @param file the PATH of the file to load the constants from
     */
    private static void loadConstants(String file) throws Exception {
        global_constants.clear();

        Lexer            lexer  = new Lexer(new File("config/scripts/constants.rs"));
        ArrayList<Token> tokens = lexer.getTokens();
        int              caret  = 0;

        while (caret < tokens.size()) {
            if (tokens.get(caret) == null) {
                caret++;

                continue;
            }

            if (tokens.get(caret).getData().equalsIgnoreCase("const")) {
                Token assignment         = tokens.get(caret + 1);
                Token assignmentOperator = tokens.get(caret + 2);
                Token value              = tokens.get(caret + 3);

                if (assignmentOperator.getType() != Token.TYPE_ASSIGNMENT) {
                    throw new ScriptParseException("Error expecting assignment operator for constant got "
                                                   + assignmentOperator.getData(), assignmentOperator.position());
                }

                int type = value.getType();

                if ((type != Token.TYPE_NUMBER) && (type != Token.TYPE_STRING) && (type != Token.TYPE_STRING_LITERAL)) {
                    throw new ScriptParseException("Error expecting value for constant got " + value.getData(),
                                                   value.position());
                }

                global_constants.put(assignment.getData(), value.getData());
                caret += 4;
            } else {
                caret++;    // ignore
            }
        }

        lexer.close();
    }

    /*
     * Compile all scripts in a directory
     * and load them into the virtual machine
     */

    /**
     * Method compile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param output
     * @param path
     */
    public static void compile(ScriptCompilerOutput output, String path) {
        block_id  = 0;
        enum_id   = 0;
        struct_id = 0;

        File       file2 = new File(path);
        List<File> files = (List<File>) FileUtils.listFiles(file2, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);

        block_ids.clear();
        structs.clear();
        enum_ids.clear();
        global_constants.clear();

        // Analyze and group block ids for optimized speed
        File curFile;

        output.messageSent(null, "Compiling Javascript main file (core.js)");
        output.messageSent(null, "Compiled native javascript library");
        output.messageSent(null, "Fetching global constants, (constants.rs)");

        try {
            loadConstants("config/scripts/constants.rs");
            output.messageSent(null, "Loaded " + global_constants.size() + " constants");
        } catch (Exception ee) {
            ee.printStackTrace();
            output.messageSent(null, "@red@Constant loading failed: " + ee.getMessage());
        }

        output.messageSent(null, "Compiling scripts: " + path);
        output.messageSent(null, "Analyzing block ids for optimization");

        for (File scriptFile : files) {
            if (!scriptFile.getName().endsWith(".rs")) {
                continue;
            }

            curFile = scriptFile;

            try {
                preAnalyze(scriptFile);
            } catch (Exception ee) {
                ee.printStackTrace();

                if (curFile != null) {
                    output.messageSent(null, "@red@Error failed to compile file: " + curFile.getPath());
                    output.messageSent(null, "Error message: @red@: " + ee.getMessage());
                }
            }
        }

        output.messageSent(null, "Block analysis complete, found " + block_ids.size() + " blocks analyzed");
        output.messageSent(null, "ScriptEnum analysis complete found " + enum_ids.size() + " enums");

        int            compiled = 0;
        int            total    = 0;
        ScriptCompiler compiler = null;

        for (File scriptFile : files) {
            if (!scriptFile.getName().endsWith(".rs")) {
                continue;
            }

            try {
                total++;
                compiler = new ScriptCompiler(scriptFile, output);
                compiler.analyzeLocals();
                compiler.context = new Context(scriptFile.getName(), compiler.localFunctions.size());
                compiler.tokens  = compiler.loadAndSkipLocalContext();
                compiler.current = 0;
                compiler.parse();
                compiled++;
            } catch (ScriptParseException ee) {
                output.exceptionCaught(ee);
                ee.printStackTrace();
            } catch (Exception ee) {
                if ((compiler != null) && (compiler.current() != null)) {
                    output.messageSent(compiler.current().position(),
                                       Text.RED("[ERROR: " + compiler.current().position().toString() + "] "
                                                + ee.getMessage()));
                } else {
                    output.exceptionCaught(ee);
                }

                ee.printStackTrace();
            }
        }

        output.messageSent(null,
                           "Successfully compiled: " + compiled + " scripts out of " + total + " failed: "
                           + (total - compiled));
    }

    private void analyzeLocals() {
        while (hasNext()) {
            if ((nextToken().getType() == Token.TYPE_CONSTRUCTOR) && hasNext()
                    && nextToken().getData().equalsIgnoreCase("localblock")) {
                if (lookAhead(1).getType() == Token.TYPE_OPENING_PARENTHESIS) {
                    matchReturnType();
                }

                if (block_ids.containsKey(lookAhead(1).getData())) {
                    throw new ScriptParseException(
                        "Error cannot declare local block as the same name as a global block: "
                        + lookAhead(1).getData(), current());
                }

                localFunctions.put(nextToken().getData(), curLocal++);
            }
        }

        current = 0;
    }

    /*
     * Analyze the current file, and get the scriptblock or enum ids
     */
    private static void preAnalyze(File file) {
        Lexer            lexer  = new Lexer(file);
        ArrayList<Token> tokens = lexer.getTokens();

        for (int i = 0; i < tokens.size(); i++) {
            try {
                if (tokens.get(i).getType() == Token.TYPE_CONSTRUCTOR) {
                    if (tokens.get(i + 1).getData().equalsIgnoreCase("block")) {
                        i += 2;
                        block_ids.put(tokens.get(i).getData(), block_id++);
                    } else if (tokens.get(i + 1).getData().equalsIgnoreCase("enum")) {
                        i += 2;
                        enum_ids.put(tokens.get(i).getData(), enum_id++);
                    } else if (tokens.get(i + 1).getData().equalsIgnoreCase("struct")) {
                        i += 2;
                        structs.put(tokens.get(i).getData(), struct_id++);
                    }
                } else if ((tokens.get(i).getType() == Token.TYPE_STRING)
                           && tokens.get(i).getData().equalsIgnoreCase("def_list")) {
                    list_ids.put(tokens.get(i + 1).getData(), list_id++);
                    i += 2;
                }
            } catch (Exception ee) {
                throw new ScriptParseException("error: file: " + ee.getMessage());
            }
        }

        lexer.close();
    }

    /*
     * Look what the next token is before its selected
     * @param cur the amount of tokens to look ahead
     */
    private Token lookAhead(int cur) {
        cur -= 1;

        if (tokens.size() < cur + current) {
            return null;
        }

        return tokens.get(cur + current);
    }

    /*
     * Skip the current caret by set amount
     */
    private void skip(int toks) {
        current += toks;

        if (current >= tokens.size()) {
            throw new ScriptParseException("reached end of file whilst parsing:", tokens.get(tokens.size() - 1));
        }
    }

    /*
     * Return the next token and move the caret to the right
     */
    private Token nextToken() {
        return tokens.get(current++);
    }

    /*
     * Returns true if theres more tokens to parse
     */
    private boolean hasNext() {
        return current < tokens.size();
    }

    /*
     * Match a signature or param list
     * Example: param, param, parm, parm, param, param
     * Will also trim off the closing parenthesis if present
     */
    private String[] matchArguments() {
        ArrayList<String> list = new ArrayList<String>();

        while (hasNext()) {
            Token current = nextToken();

            if (current.getType() == Token.TYPE_CLOSING_PARENTHESIS) {
                break;
            }

            if ((current.getType() == Token.TYPE_STRING) || (current.getType() == Token.TYPE_NUMBER)
                    || (current.getType() == Token.TYPE_STRING_LITERAL)) {
                list.add(current.getData());
            }
        }

        String[] ret = new String[list.size()];

        return list.toArray(ret);
    }

    /*
     * Optimize parameters
     *
     */
    private Node[] optimizeArguments(Node[] parameters, String callname) {
        if (callname.equalsIgnoreCase("enumdata") || callname.equalsIgnoreCase("enum_size")
                || callname.equalsIgnoreCase("enum_key")) {
            if (!enum_ids.containsKey(parameters[0].getData())) {
                throw new ScriptParseException("invalid enum: " + parameters[0].getData());
            }

            parameters[0] = new IntegerNode(enum_ids.get(parameters[0].getData()));
        } else if (callname.equalsIgnoreCase("get_list")) {
            if (!list_ids.containsKey(parameters[0].getData())) {
                throw new ScriptParseException("Error invalid list: " + parameters[0].getData());
            }

            parameters[0] = new IntegerNode(list_ids.get(parameters[0].getData()));
        }

        return parameters;
    }

    /*
     * Match a call or invocation
     * Example, will match: method(something)
     */
    private CallNode matchCallNode() {
        String callName = current().getData();

        if (callName.equalsIgnoreCase("if")) {
            throw new ScriptParseException("found if inside call expression " + current().position(),
                                           current().position());
        }

        nextToken();

        Node[] nodes = optimizeArguments(matchExpressionParams(), callName);

        if ((lookAhead(1).getType() != Token.TYPE_CLOSING_PARENTHESIS)
                && (current().getType() != Token.TYPE_CLOSING_PARENTHESIS)) {
            throw new ScriptParseException("Expecting closing parenthesis for call got " + lookAhead(1).getData() + " "
                                           + lookAhead(1).position(), lookAhead(1).position());
        }

        if (lookAhead(1).getType() == Token.TYPE_CLOSING_PARENTHESIS) {
            nextToken();    // get rid of parenthesis
        }

        return new CallNode(callName, nodes, this);
    }

    private CallNode matchStructInitializer() {
        String callName = current().getData();

        nextToken();

        Node[] nodes = optimizeArguments(matchExpressionParams(), callName);

        if ((lookAhead(1).getType() != Token.TYPE_CLOSING_PARENTHESIS)
                && (current().getType() != Token.TYPE_CLOSING_PARENTHESIS)) {
            throw new ScriptParseException("Expecting closing parenthesis for call got " + lookAhead(1).getData() + " "
                                           + lookAhead(1).position(), lookAhead(1).position());
        }

        if (lookAhead(1).getType() == Token.TYPE_CLOSING_PARENTHESIS) {
            nextToken();    // get rid of parenthesis
        }

        if (nodes == null) {
            throw new ScriptParseException("Error no data found for struct initiliazer " + current().position());
        }

        Node[] inject = new Node[nodes.length + 1];

        try {
            inject[0] = new IntegerNode(structs.get(callName));
            System.arraycopy(nodes, 0, inject, 1, nodes.length);
        } catch (Exception ee) {
            throw new RuntimeException("Struct not found: " + callName);
        }

        return new CallNode("createstruct", inject, this);
    }

    private void checkBlockName(String name) {
        if (RuntimeOperationMap.getByName(name) != null) {
            throw new ScriptParseException("Error blockname: " + name + " overwrites runtime instruction");
        }

        if (ConditionMap.get(name) != null) {
            throw new ScriptParseException("Error blockname: " + name + " overwrites internal conditional");
        }

        if (ReturnOperationMap.getCall(name) != null) {
            throw new ScriptParseException("Error blockname: " + name + " overwrites internal evaluation");
        }
    }

    /*
     * Parse a string, and split it into expressions
     * for evaluation
     * For example, "string $concentation", or "string {concentation()}
     */
    private ConcentationNode matchConcentationNode() {
        StringBuilder currentExpression = new StringBuilder();
        List<Node>    nodes             = new ArrayList<Node>();

        nextToken();

        do {
            Token current = current();

            if (current.getData().startsWith(VARIABLE_REFERENCER)) {
                if (currentExpression.toString().length() > 0) {
                    nodes.add(new StringLiteralNode(currentExpression.toString()));
                    currentExpression = new StringBuilder();
                }

                if (current.getData().length() > 12) {
                    throw new ScriptParseException("Error '" + current + "' variable names must be less than 12 chars",
                                                   current);
                }

                nodes.add(new VariableDataNode(Text.longForName(current.getData().substring(1))));

                if (!hasNext()) {
                    break;
                }

                nextToken();
            } else if (current.getType() == Token.TYPE_OPENING_BRACE) {
                if (currentExpression.toString().length() > 0) {
                    nodes.add(new StringLiteralNode(currentExpression.toString()));
                    currentExpression = new StringBuilder();
                }

                ArrayList<Token> tmpTokens = new ArrayList<Token>();

                while (hasNext()) {
                    Token t = lookAhead(1);

                    if (t.getType() == Token.CLOSING_BRACE) {
                        break;
                    }

                    if (t.getType() != Token.TYPE_WHITESPACE) {
                        tmpTokens.add(t);
                    }

                    nextToken();
                }

                nextToken();
                nodes.add(new ScriptCompiler(tmpTokens, constants, context,
                                             localFunctions).matchConcentationEvaluation());

                if (current().getType() == Token.TYPE_CLOSING_PARENTHESIS) {
                    nextToken();    // trim that shit off
                }

                if (current().getType() != Token.CLOSING_BRACE) {
                    throw new ScriptParseException("Error expecting closing brace got: " + current().getData());
                }

                if (!hasNext()) {
                    break;
                }

                nextToken();
            } else {
                currentExpression.append(current.getData());

                if (!hasNext()) {
                    break;
                }

                nextToken();
            }
        } while ((tokens.size() - current) > -1);

        if (currentExpression.toString().length() > 0) {
            nodes.add(new StringLiteralNode(currentExpression.toString()));
        }

        return new ConcentationNode(nodes);
    }

    /**
     * Method getContext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Context getContext() {
        return this.context;
    }

    /*
     * Match concentation or a plain variable call
     * example: $var will return a VariableDataExpression
     * example: "something $var" will return a StringConcentationExpression
     */
    private Node matchStringOrVariableReference(String data, Token type) {
        if (data.equals(CONCENTATION_BEGIN)) {
            throw new RuntimeException("error?");
        }

        if (data.contains(CONCENTATION_BEGIN) || (type.getType() == Token.TYPE_STRING_LITERAL)) {
            ScriptCompiler concentationCompiler = new ScriptCompiler(new Lexer(this.file, data, false), this.file,
                                                      output, context, localFunctions);

            try {
                concentationCompiler.constants = this.constants;

                Node ret = concentationCompiler.matchConcentationNode();

                concentationCompiler.lexer.close();

                return ret;
            } catch (Exception ee) {
                ee.printStackTrace();

                throw new ScriptParseException("Error in string concentation \"" + data + "\" "
                                               + ee.getClass().getSimpleName() + " "
                                               + ee.getMessage(), current().position());
            }
        } else {
            if (data.length() > 12) {
                throw new ScriptParseException("Error variable names must be less than 12 chars " + data,
                                               current().position());
            }

            if (hasNext() && (lookAhead(1).getType() == Token.TYPE_DOT)) {
                nextToken();

                return new StructDataNode(new GetStructVariableNode(Text.longForName(data.substring(1)),
                        Text.longForName(nextToken().getData())));
            }

            return new VariableDataNode(Text.longForName(data.substring(1)));
        }
    }

    /*
     * Match a root expression
     * This is the most commonly used,
     * This will match almost anything, from method() to $var to "var" to method() / 4;
     */
    private Node matchStatement() {
        long l = 0;

        if (isThirdPartyCall(1)) {
            l = matchExternalCallIdentifier(1);
        }

        Token current = nextToken();

        if (current.getType() == Token.TYPE_AND) {
            nextToken();

            return matchStructInitializer();
        }

        if (current.getType() == Token.TYPE_OPENING_PARENTHESIS) {
            current = nextToken();    // skip that
        }

        if (current.getType() == Token.TYPE_CLOSING_PARENTHESIS) {
            return null;
        }

        if (current.getType() == Token.TYPE_STRING_LITERAL) {
            if (current.getData().contains(CONCENTATION_BEGIN) || current.getData().contains(VARIABLE_REFERENCER)) {
                return matchStringOrVariableReference(replaceConstants(current.getData()),
                        current).setEvaluationIdentifier(l);
            }

            return new StringLiteralNode(current.getData());
        }

        if (current.getType() == Token.TYPE_NUMBER) {
            return new IntegerNode(Integer.parseInt(current.getData()));
        }

        if (current.getType() == Token.TYPE_STRING) {
            if (current.getData().contains(VARIABLE_REFERENCER) || current.getData().contains(CONCENTATION_BEGIN)) {
                return matchStringOrVariableReference(current.getData(), current).setEvaluationIdentifier(l);
            }

            if (lookAhead(1).getType() == Token.TYPE_OPENING_PARENTHESIS) {
                return matchCallNode().setEvaluationIdentifier(l);
            }

            String data = replaceConstants(current.getData());

            try {
                return new IntegerNode(Integer.parseInt(replaceConstants(data)));
            } catch (NumberFormatException ee) {
                return new StringLiteralNode(replaceConstants(current.getData()));
            }
        }

        Logger.debug("prev: " + this.tokens.get(this.current - 2) + " " + this.tokens.get(this.current - 2).getType()
                     + " " + this.tokens.get(this.current - 1).getData() + " "
                     + this.tokens.get(this.current - 3).getData());

        throw new ScriptParseException("Error unexpected token in expression " + current.getData(),
                                       current().position());
    }

    /*
     * Check if the token is an calculation expression operator
     * Eg: / * %  -
     */
    private static boolean isMathOperator(Token t) {
        return (t.getType() == Token.TYPE_ADD) || (t.getType() == Token.TYPE_MODULUS_OPERATOR)
               || (t.getType() == Token.TYPE_DIVIDE_OPERATOR) || (t.getType() == Token.TYPE_MINUS)
               || (t.getType() == Token.TYPE_MULTIPLY);
    }

    /**
     * Method test
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void test() {}

    /*
     * Match an expression inside an evaluation inside string concentation
     * Example {method()}
     */

    /**
     * Method matchConcentationEvaluation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    Node matchConcentationEvaluation() {
        Node ex = matchStatement();

        if (!hasNext()) {
            return ex;
        }

        Token peek = lookAhead(1);

        if ((peek.getType() == Token.CLOSING_BRACE) || (current().getType() == Token.CLOSING_BRACE)) {
            return ex;
        }

        ArrayList<Integer> tokens = new ArrayList<Integer>();
        ArrayList<Node>    expr   = new ArrayList<Node>();

        expr.add(ex);

        while (hasNext() && isMathOperator(lookAhead(1))) {
            Token t = nextToken();

            tokens.add(t.getType());
            expr.add(matchStatement());
        }

        if (expr.size() == 1) {
            return ex;
        }

        return new ExpressionNode(tokens, expr);
    }

    /*
     * Match a variable expression
     * Specifically tailored for matching var = EXPRESSION
     */

    /**
     * Method matchVariableNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    Node matchVariableNode() {
        if (hasNext() && (lookAhead(1).getType() == Token.TYPE_ENDSTATEMENT)) {
            return null;
        }

        Node  ex   = matchStatement();
        Token peek = lookAhead(1);

        if ((peek.getType() == Token.TYPE_ENDSTATEMENT) || (current().getType() == Token.TYPE_ENDSTATEMENT)) {
            return ex;
        }

        List<Integer> tokens = new ArrayList<Integer>();
        List<Node>    expr   = new ArrayList<Node>();

        expr.add(ex);

        while (isMathOperator(lookAhead(1))) {
            Token t = nextToken();

            tokens.add(t.getType());
            expr.add(matchStatement());
        }

        if (((lookAhead(1).getType() != Token.TYPE_CLOSING_PARENTHESIS)
                && (lookAhead(1).getType() != Token.TYPE_ENDSTATEMENT)) && (current().getType()
                   != Token.TYPE_ENDSTATEMENT)) {
            throw new ScriptParseException("Need end statement operator on var declaration got " + current().getData()
                                           + " " + current().position(), current().position());
        }

        return new ExpressionNode(tokens, expr);
    }

    /*
     * Match a parameter list of expressions
     * eg, expression,expression, expression,expression
     * Also trims off any parenthesis
     */

    /**
     * Method matchExpressionParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    Node[] matchExpressionParams() {
        boolean    end   = false;
        List<Node> nodes = new ArrayList<Node>();

        if ((lookAhead(1).getType() == Token.TYPE_CLOSING_PARENTHESIS)
                || (current().getType() == Token.TYPE_CLOSING_PARENTHESIS)) {
            return null;
        }

        if (lookAhead(1).getType() == Token.TYPE_OPENING_PARENTHESIS) {
            nextToken();    // skip that
        }

        while (!end) {
            Node  ex   = matchStatement();
            Token peek = lookAhead(1);

            if (peek.getType() == Token.TYPE_CLOSING_PARENTHESIS) {
                nodes.add(ex);

                break;
            } else if (peek.getType() == Token.TYPE_COMMA) {
                nodes.add(ex);
                nextToken();

                continue;
            }

            List<Integer> tokens = new ArrayList<Integer>();
            List<Node>    expr   = new ArrayList<Node>();

            expr.add(ex);

            while (isMathOperator(lookAhead(1))) {
                Token t = nextToken();

                tokens.add(t.getType());
                expr.add(matchStatement());
            }

            if (lookAhead(1).getType() == Token.TYPE_COMMA) {
                nextToken();
            }

            nodes.add(new ExpressionNode(tokens, expr));

            if (lookAhead(1).getType() == Token.TYPE_CLOSING_PARENTHESIS) {
                end = true;
            }
        }

        Node[] ret = new Node[nodes.size()];

        return nodes.toArray(ret);
    }

    /*
     * Returns all the combinations of a String[][] array
     * For example:
     */
    private static List<String[]> combinations(String[][] twoDimStringArray) {
        int sizeArray[]           = new int[twoDimStringArray.length];
        int counterArray[]        = new int[twoDimStringArray.length];
        int totalCombinationCount = 1;

        for (int i = 0; i < twoDimStringArray.length; ++i) {
            sizeArray[i]          = twoDimStringArray[i].length;
            totalCombinationCount *= twoDimStringArray[i].length;
        }

        List<String[]> combinationList = new ArrayList<String[]>(totalCombinationCount);
        StringBuilder  sb;

        for (int countdown = totalCombinationCount; countdown > 0; --countdown) {
            ArrayList<String> tmp = new ArrayList<String>();

            for (int i = 0; i < twoDimStringArray.length; ++i) {
                tmp.add(twoDimStringArray[i][counterArray[i]]);
            }

            combinationList.add(tmp.toArray(new String[tmp.size()]));

            for (int incIndex = twoDimStringArray.length - 1; incIndex >= 0; --incIndex) {
                if (counterArray[incIndex] + 1 < sizeArray[incIndex]) {
                    ++counterArray[incIndex];

                    break;
                }

                counterArray[incIndex] = 0;
            }
        }

        return combinationList;
    }

    /*
     * Match a multiple list of parameters,
     * Eg, param, param, param, [param, param, param, param]
     */
    private String[][] matchMultiParamList(String[] params) {
        @SuppressWarnings("unchecked") ArrayList<String>[] combos = new ArrayList[params.length];

        for (int i = 0; i < combos.length; i++) {
            combos[i] = new ArrayList<String>();
        }

        for (int i = 0; i < params.length; i++) {
            if (!params[i].contains("[")) {
                combos[i].add(replaceConstants(params[i]));
            } else {
                String[] split = params[i].substring(1, params[i].length() - 1).replace("\n", "").replace("\t",
                                     "").split(",");

                for (String aSplit : split) {
                    combos[i].add(replaceConstants(aSplit.replace(" ", "")));
                }
            }
        }

        String[][] ret = new String[params.length][];

        for (int i = 0; i < combos.length; i++) {
            ret[i] = new String[combos[i].size()];
            combos[i].toArray(ret[i]);
        }

        return ret;
    }

    /*
     * Debug
     */

    /**
     * Method debug
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param name
     * @param params
     */
    void debug(String type, String name, String[] params) {
        if (true) {
            return;
        }

        Logger.err("Found: " + type);
        Logger.err("name: " + name);
        Logger.err("parameters: " + params.length);

        for (String param : params) {
            Logger.err(param);
        }
    }

    /*
     * Replaces a block name with the id allocated from
     * pre analysis
     */

    /**
     * Method replaceBlockName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public int replaceBlockName(String name) {
        if (localFunctions.containsKey(name)) {
            return localFunctions.get(name) + BLOCK_DELIMITER;
        } else if (block_ids.containsKey(name)) {
            return block_ids.get(name);
        }

        return -1;
    }

    /**
     * Method replaceBlockNameWithID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public String replaceBlockNameWithID(String name) {
        if (name.equalsIgnoreCase("null")) {
            return name;
        }

        try {
            if (localFunctions.containsKey(name)) {
                return Integer.toString((BLOCK_DELIMITER + localFunctions.get(name)));
            }

            return Integer.toString(block_ids.get(name));
        } catch (Exception ee) {
            throw new ScriptParseException("Error invalid block: " + name + " " + current().position());
        }
    }

    /*
     * Dump the current loaded tokens for debugging purposes
     */
    private void dumpTokens() {
        for (Token t : tokens) {
            Logger.err("token[" + t.getType() + "]: " + t.getData());
        }
    }

    private Token current() {
        return lookAhead(0);
    }

    /**
     * Method matchCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    BooleanCondition matchCondition() {
        boolean not        = false;
        boolean javascript = false;

        if (lookAhead(1).getData().startsWith("~")) {
            lookAhead(1).setData(lookAhead(1).getData().substring(1));
            javascript = true;
            lookAhead(1).setType(Token.TYPE_STRING);
        }

        if (lookAhead(1).getData().equalsIgnoreCase("!")) {
            not = true;
            nextToken();

            if (lookAhead(1).getData().startsWith("~")) {
                lookAhead(1).setData(lookAhead(1).getData().substring(1));
                javascript = true;
                lookAhead(1).setType(Token.TYPE_STRING);
            }
        }

        Node m = matchStatement();

        return new BooleanCondition(m, not, javascript);
    }

    /**
     * Method isComparator
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     *
     * @return
     */
    boolean isComparator(Token t) {
        for (int LOGICAL_OPERATOR : COMPARATOR_OPERATORS) {
            if (LOGICAL_OPERATOR == t.getType()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method RunTest
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void RunTest() {
        new ScriptCompiler(new File("./config/scripts/ui/telemenu/impl/teleports/bestiary.rs"),
                           new ScriptCompilerOutput() {
            @Override
            public void exceptionCaught(Exception exception) {
                exception.printStackTrace();
            }
            @Override
            public void messageSent(SourcePosition position, String message) {
                Logger.debug(message);
            }
        });
    }

    /**
     * Method matchConditionList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param current
     *
     * @return
     */
    ConditionalEvaluator matchConditionList(Token current) {
        ConditionalEvaluator builder = new ConditionalEvaluator();

        do {
            BooleanCondition cond = matchCondition();

            nextToken();

            if (isComparator(current())) {
                Token            comparator  = current();
                BooleanCondition compareWith = matchCondition();

                builder.addExpression(new ComparatorBoolean(cond.getExpr(), comparator, compareWith.getNode()));
                nextToken();
            } else {
                CallNode expression = (CallNode) cond.getNode();

                if (cond.isJs()) {
                    builder.addExpression(new NormalBooleanExpression(new JavascriptCondition(expression.getData()),
                            expression.getParams(),
                            cond.isNot()).setExternalIdentifier(expression.getEvaluationIdentifier()));
                } else {
                    builder.addExpression(new NormalBooleanExpression(new NativeCondition(expression.getData()),
                            expression.getParams(),
                            cond.isNot()).setExternalIdentifier(expression.getEvaluationIdentifier()));

                    if (expression.getData().equalsIgnoreCase("enum_get")) {
                        try {
                            expression.getParams()[0] =
                                new IntegerNode(enum_ids.get(expression.getParams()[0].getData()));
                        } catch (NullPointerException EE) {
                            throw new ScriptParseException("invalid enum " + expression.getParams()[0].getData(),
                                                           current.position());
                        }
                    }
                }

                if ((current().getType() == Token.TYPE_CLOSING_PARENTHESIS)
                        || (current().getType() == Token.TYPE_CLOSING_PARENTHESIS)
                        || (current.getType() == Token.TYPE_ENDSTATEMENT)) {
                    break;
                }
            }

            if ((current().getType() == Token.TYPE_CLOSING_PARENTHESIS)
                    || (current.getType() == Token.TYPE_ENDSTATEMENT)) {
                return builder;
            }

            if ((current().getType() == Token.TYPE_AND) || (current().getType() == Token.TYPE_OR)) {
                builder.addBooleanOperator(current());
            } else {
                throw new RuntimeException("ERROR!!!:  " + current() + " " + current().position());
            }
        } while (true);

        return builder;
    }

    /**
     * Method matchForEachLoop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    RuntimeInstruction matchForEachLoop() {
        if (lookAhead(1).getType() != Token.TYPE_STRING) {
            throw new ScriptParseException("Error expecting var name in for loop got " + lookAhead(1).getData() + " "
                                           + current().position(), current().position());
        }

        String varName = nextToken().getData();

        if ((lookAhead(1).getType() != Token.TYPE_STRING) && lookAhead(1).getData().equalsIgnoreCase("in")) {
            throw new ScriptParseException("Error foreach, expecting IN keyword, got " + lookAhead(1).getData());
        }

        nextToken();

        Node evaluation = matchStatement();

        if (lookAhead(1).getType() != Token.TYPE_CLOSING_PARENTHESIS) {
            throw new ScriptParseException("Error expecing closing parenthesis in foreach loop got " + lookAhead(1));
        }

        nextToken();

        BlockNode                block               = new BlockNode();
        List<RuntimeInstruction> runtimeInstructions = new LinkedList<RuntimeInstruction>();

        if (lookAhead(1).getType() != Token.TYPE_OPENING_BRACE) {
            throw new ScriptParseException("Error expecting opening brace got " + lookAhead(1).getData(), current());
        }

        nextToken();
        parseBlockNode(block, runtimeInstructions);
        block.setOperations(runtimeInstructions);
        block.setContext(context);

        ForEachLoop loop = new ForEachLoop(block, Text.longForName(varName), evaluation);

        if (lookAhead(1).getType() == Token.CLOSING_BRACE) {
            nextToken();
        }

        return new RuntimeInstruction(loop, current().position());
    }

    /**
     * Method matchForLoop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    RuntimeInstruction matchForLoop() {
        SourcePosition beginPos = current().position();

        if (lookAhead(1).getType() != Token.TYPE_STRING) {
            throw new ScriptParseException("Error expecting var name in for loop got " + lookAhead(1).getData() + " "
                                           + current().position(), current().position());
        }

        String varName = nextToken().getData();

        if (lookAhead(1).getType() != Token.TYPE_ASSIGNMENT) {
            throw new ScriptParseException("Error expecting assignment for for loop variable got: "
                                           + lookAhead(1).getData(), current().position());
        }

        nextToken();

        Node assignmentNode = matchVariableNode();

        if (lookAhead(1).getType() != Token.TYPE_ENDSTATEMENT) {
            throw new ScriptParseException("Error forloop, expecting endstatement got " + lookAhead(1).getData(),
                                           current().position());
        }

        ConditionalEvaluator conditionalCode = matchConditionList(nextToken());

        if (lookAhead(1).getType() == Token.TYPE_ENDSTATEMENT) {
            throw new ScriptParseException("Error expecting endstatement got " + lookAhead(1).getData(),
                                           current().position());
        }

        nextToken();

        RuntimeInstruction afterRuntimeInstruction = matchCall(current());

        if (lookAhead(1).getType() == Token.TYPE_CLOSING_PARENTHESIS) {
            nextToken();
        }

        SwitchBlock                   block               = new SwitchBlock();
        ArrayList<RuntimeInstruction> runtimeInstructions = new ArrayList<RuntimeInstruction>();

        parseBlockNode(block, runtimeInstructions);
        block.setOperations(runtimeInstructions);
        block.setContext(context);

        ForLoop l = new ForLoop(varName, assignmentNode, conditionalCode, afterRuntimeInstruction, block);

        if (lookAhead(1).getType() == Token.CLOSING_BRACE) {
            nextToken();
        }

        return new RuntimeInstruction(l, beginPos);
    }

    /**
     * Method matchCall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param current
     *
     * @return
     */
    private boolean hasNext(int count) {
        return this.current + count < tokens.size();
    }

    private boolean isThirdPartyCall(int ahead) {
        if (!hasNext(ahead + 1)) {
            return false;
        }

        if ((lookAhead(ahead).getType() == Token.TYPE_STRING)
                && (lookAhead(ahead + 1).getType() == Token.TYPE_DBL_COLON)) {
            return true;
        }

        return false;
    }

    private long matchExternalCallIdentifier(int ahead) {
        long l = Text.longForName(lookAhead(ahead).getData());

        nextToken();

        for (int i = 0; i < ahead; i++) {
            nextToken();
        }

        return l;
    }

    /**
     * Method matchCall
     * Created on 14/08/22
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param current
     *
     * @return
     */
    RuntimeInstruction matchCall(Token current) {
        int statementType = RuntimeInstruction.NONE;

        if (current.getType() == Token.TYPE_ADD) {
            statementType = RuntimeInstruction.TRUE;
        }

        if (current.getType() == Token.TYPE_MINUS) {
            statementType = RuntimeInstruction.FALSE;
        }

        if ((current.getType() == Token.TYPE_MODULUS_OPERATOR)
                || (lookAhead(1).getType() == Token.TYPE_MODULUS_OPERATOR)) {
            if (lookAhead(1).getType() == Token.TYPE_MODULUS_OPERATOR) {
                nextToken();
            }

            return matchVariableAssignmentOrEdit(nextToken(), statementType, true);
        }

        if (statementType != RuntimeInstruction.NONE) {
            current = nextToken();
        }

        if (current.getType() != Token.TYPE_STRING) {
            throw new ScriptParseException("Expected statement name got " + current.getData(), current);
        }

        String name = current.getData();

        if (name.equalsIgnoreCase("return")) {
            Node x = matchVariableNode();

            if (x == null) {
                Node[] expr = null;

                return new RuntimeInstruction(current().position(), RuntimeOperationMap.getByName("return"),
                                              statementType, "return", expr);
            }

            return new RuntimeInstruction(current().position(), RuntimeOperationMap.getByName("return"), statementType,
                                          "return", x);
        }

        if (name.equalsIgnoreCase("var")) {
            return matchVariableDeclaration(current, statementType);
        } else if (name.startsWith(VARIABLE_REFERENCER) || name.startsWith(PLAYER_VAR_REFERENCER)) {
            return matchVariableAssignmentOrEdit(current, statementType, false);
        }

        if (name.equalsIgnoreCase("if")) {
            if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                throw new ScriptParseException("Error, expecting opening brace on ifnode", lookAhead(1));
            }

            nextToken();

            ConditionalEvaluator builder = matchConditionList(current);

            if (hasNext() && (lookAhead(1).getType() == Token.TYPE_OPENING_BRACE)) {
                BlockNode                     blockNode     = new BlockNode();
                ArrayList<RuntimeInstruction> operationList = new ArrayList<RuntimeInstruction>();

                parseBlockNode(blockNode, operationList);

                if (lookAhead(1).getType() != Token.CLOSING_BRACE) {
                    throw new ScriptParseException("Expecting closing brace got " + lookAhead(1));
                }

                blockNode.setOperations(operationList);
                blockNode.setContext(context);
                nextToken();

                return new RuntimeInstruction(builder, blockNode);
            }

            return new RuntimeInstruction(builder);
        }

        if ((RuntimeOperationMap.getByName(name) == null) && block_ids.containsKey(name)
                || localFunctions.containsKey(name)) {
            if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                throw new ScriptParseException("Error need opening parenthesis '(' required for jump/fork got "
                                               + lookAhead(1).getData(), current().position());
            }

            nextToken();

            Node[] tmp   = new Node[1];
            Node[] nodes = matchExpressionParams();

            if (lookAhead(1).getType() != Token.TYPE_CLOSING_PARENTHESIS) {
                throw new ScriptParseException("Expecting closing parenthesis for call " + lookAhead(1).getData(),
                                               current().position());
            }

            nextToken();

            if (nodes != null) {
                tmp = new Node[nodes.length + 1];
                System.arraycopy(nodes, 0, tmp, 1, nodes.length);
            }

            if (currentBlock instanceof SignedBlock) {
                if (name.equalsIgnoreCase(((SignedBlock) currentBlock).getName())) {
                    throw new RuntimeException("stack error");
                }
            }

            tmp[0] = new IntegerNode(Integer.parseInt(replaceBlockNameWithID(name)));

            return new RuntimeInstruction(RuntimeOperationMap.getByName("gosub"), statementType, name, tmp);
        }

        if (lookAhead(1).getType() == Token.TYPE_GOTO) {
            if (name.equalsIgnoreCase("jump") || name.equalsIgnoreCase("gosub") || name.equalsIgnoreCase("fork")) {
                nextToken();

                String blockName = nextToken().getData();

                if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                    throw new ScriptParseException("Error need opening parenthesis '(' required for jump/fork got "
                                                   + lookAhead(1).getData(), current().position());
                }

                nextToken();

                Node[] tmp   = new Node[1];
                Node[] nodes = matchExpressionParams();

                if (nodes != null) {
                    tmp = new Node[nodes.length + 1];
                    System.arraycopy(nodes, 0, tmp, 1, nodes.length);
                }

                if (currentBlock instanceof SignedBlock) {
                    if (name.equalsIgnoreCase(((SignedBlock) currentBlock).getName())) {
                        throw new RuntimeException("stack error");
                    }
                }

                tmp[0] = new IntegerNode(Integer.parseInt(replaceBlockNameWithID(blockName)));

                if (lookAhead(1).getType() == Token.TYPE_CLOSING_PARENTHESIS) {
                    nextToken();
                }

                return new RuntimeInstruction(RuntimeOperationMap.getByName(name), statementType, name, tmp);
            } else if (name.equalsIgnoreCase("jsjump") || name.equalsIgnoreCase("jsfork")) {
                nextToken();

                String callName = nextToken().getData();

                nextToken();

                Node[] params = matchExpressionParams();

                if (lookAhead(1).getType() != Token.TYPE_CLOSING_PARENTHESIS) {
                    throw new ScriptParseException("Error expecting closing parenthesis got " + lookAhead(1).getData(),
                                                   current);
                }

                nextToken();

                return new RuntimeInstruction(current().position(), RuntimeOperationMap.getByName(name), statementType,
                                              callName, params);
            }
        }

        nextToken();

        Node[] tst = null;

        if (name.equalsIgnoreCase("for")) {
            return matchForLoop();
        } else if (name.equalsIgnoreCase("foreach")) {
            return matchForEachLoop();
        }

        Node[] params = matchExpressionParams();

        if (name.equalsIgnoreCase("loopblock") || (name.equalsIgnoreCase("set_death_handler"))) {
            if (name.equalsIgnoreCase("loopblock") || (name.equalsIgnoreCase("set_death_handler"))) {
                try {
                    params[0] = new IntegerNode(Integer.parseInt(replaceBlockNameWithID(params[0].getData())));
                } catch (Exception ee) {}
            }
        }

        if (name.equalsIgnoreCase("sd_begin")) {
            params[2] = new IntegerNode(Integer.parseInt(replaceBlockNameWithID(params[2].getData())));
        }

        RuntimeInstruction nextInstruction = new RuntimeInstruction(current.position(),
                                                 RuntimeOperationMap.getByName(name), statementType, name, params);

        if (name.equalsIgnoreCase("switch")) {
            if (params.length > 1) {
                throw new ScriptParseException("error switch block can only contain one parameter, got "
                                               + params.length, current.position());
            }

            if ((params == null) || (params.length == 0)) {
                throw new ScriptParseException("Switch body is empty");
            }

            SwitchBlock block = matchSwitchBlock();

            if (block == null) {
                throw new ScriptParseException("Error switch block body is empty", current.position());
            }

            nextInstruction.setSwitchBlock(block);
        }

        return nextInstruction;
    }

    /*
     * Matches a switch block
     * @return a switch block
     * @throws ScriptParseException if a parse error was found
     */

    /**
     * Method matchSwitchBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    SwitchBlock matchSwitchBlock() {
        Token       cur      = nextToken();
        SwitchBlock curBlock = new SwitchBlock();

        if (cur.getType() == Token.TYPE_CLOSING_PARENTHESIS) {
            cur = nextToken();
        }

        if (cur.getType() != Token.TYPE_OPENING_BRACE) {
            throw new ScriptParseException("Error expecting opening brace got " + cur.getData(), cur.position());
        }

        BlockNode                     block                  = new BlockNode();
        ArrayList<RuntimeInstruction> runtimeInstructionList = new ArrayList<RuntimeInstruction>();

        cur = lookAhead(1);

        while (hasNext()) {
            cur = nextToken();

            if (cur.getType() == Token.TYPE_CASE) {
                int    data;
                String nxt = replaceConstants(nextToken().getData());

                try {
                    data = Integer.parseInt(nxt);
                } catch (NumberFormatException ee) {
                    throw new ScriptParseException("Error case " + nxt
                                                   + " in switch block must resolve to a constant or number: " + nxt);
                }

                parseBlockNode(block, runtimeInstructionList);
                block.setOperations(runtimeInstructionList);
                block.setContext(context);

                Token curTmp = nextToken();

                curBlock.put(data, block);
                runtimeInstructionList.clear();
                block = new BlockNode();

                if (curTmp.getType() == Token.CLOSING_BRACE) {
                    return curBlock;
                } else if (curTmp.getType() != Token.TYPE_BREAK) {
                    throw new ScriptParseException("Expecting break got " + curTmp.getData(), curTmp.position());
                }
            } else if (cur.getType() == Token.CLOSING_BRACE) {
                return curBlock;
            } else if (cur.getType() == Token.TYPE_ENDSTATEMENT) {}
            else if (cur.getType() == Token.TYPE_DEFAULT) {
                throw new RuntimeException();
            } else {
                throw new ScriptParseException("Error parsing switch block, expecting case or default got "
                                               + cur.getData());
            }
        }

        throw new ScriptParseException("reached end of file whilst parsing", current().position());
    }

    /*
     * Replace constants within a string array
     */
    private String[] replaceConstants(String[] params) {
        for (int i = 0; i < params.length; i++) {
            params[i] = replaceConstants(params[i]);
        }

        return params;
    }

    private boolean isStringParseable(char str) {
        return Character.isLetterOrDigit(str) || (str == '_');
    }

    private String replaceConstants(String parameter) {
        if ((constants.size() == 0) && (global_constants.size() == 0)) {
            return parameter;
        }

        String[] split = parameter.split(" ");

        for (String word : split) {
            while (word.contains("@")) {
                int offset = word.indexOf("@");

                if ((offset != 0) && (word.charAt(offset) == '\\')) {
                    break;
                }

                int          save   = offset + 1;
                StringBuffer buffer = new StringBuffer();

                while ((offset + 1 < word.length()) && isStringParseable(word.charAt(++offset))) {
                    buffer.append(word.charAt(offset));
                }

                if (constants.containsKey(buffer.toString())) {
                    parameter = parameter.replace("@" + buffer.toString(), constants.get(buffer.toString()));

                    break;
                } else if (global_constants.containsKey(buffer.toString())) {
                    parameter = parameter.replace("@" + buffer.toString(), global_constants.get(buffer.toString()));

                    break;
                } else {
                    output.messageSent(current().position(),
                                       "Warning undefined constant: '" + buffer.toString() + "' in "
                                       + this.current().position().getFileName() + ":" + current().position().getRow());

                    break;
                }
            }

            if (constants.containsKey(word)) {
                parameter = parameter.replaceAll("(\\b" + word + "\\b)", constants.get(word));
            }

            if (global_constants.containsKey(word)) {
                while (parameter.contains(word)) {
                    parameter = parameter.replace(word, global_constants.get(word));
                }
            }
        }

        return parameter;
    }

    private ScriptEnum matchEmum() {
        String     name    = nextToken().getData();
        ScriptEnum theEnum = new ScriptEnum(name);

        if (lookAhead(1).getType() != Token.TYPE_OPENING_BRACE) {
            throw new ScriptParseException("Error reading enum " + name + " expecting opening brace got "
                                           + lookAhead(1).getData());
        }

        nextToken();

        while (lookAhead(1).getType() != Token.CLOSING_BRACE) {
            Token current = nextToken();

            if (current.getData().equalsIgnoreCase(";")) {
                continue;
            }

            if (current.getType() != Token.TYPE_STRING) {
                throw new ScriptParseException("Expecting sub-enum name but got " + current.getData(),
                                               current().position());
            }

            String subName = current.getData();

            if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                throw new ScriptParseException("Error, expecting opening bracket for enum got : "
                                               + lookAhead(1).getData(), current().position());
            }

            String[] params = replaceConstants(matchArguments());

            current = nextToken();

            if (current.getType() != Token.TYPE_ASSIGNMENT) {
                throw new ScriptParseException("Error expecting assignment got " + current.getData() + " "
                                               + current().position(), current().position());
            }

            current = nextToken();

            String enumData = current.getData();

            if (!enumData.startsWith("[") ||!enumData.endsWith("]")) {
                throw new ScriptParseException("Error enum data must start with [ and end with ]");
            }

            String enumActualData = enumData.substring(1, enumData.length() - 1);

            enumActualData = enumActualData.replace(" ", "");

            String[] split = enumActualData.split(",");
            String[] tmp   = new String[split.length];

            for (int i = 0; i < split.length; i++) {
                try {
                    String tmpStr = replaceConstants(split[i]);

                    Integer.parseInt(tmpStr);
                    tmp[i] = tmpStr;
                } catch (NumberFormatException ee) {
                    throw new ScriptParseException("Error with enum " + name + ":" + subName
                                                   + " data must be integer, got " + split[i]);
                }
            }

            ScriptEnumerator type = new ScriptEnumerator(name, params, tmp);

            theEnum.addType(type);
        }

        nextToken();    // get rid of bracket

        return theEnum;
    }

    /**
     * Method matchInstructionCall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     * @param file
     *
     * @return
     */
    public static RuntimeInstruction matchInstructionCall(String data, File file) {
        ScriptCompiler compiler = new ScriptCompiler(new Lexer(file, data, true), file, new ScriptCompilerOutput() {
            @Override
            public void exceptionCaught(Exception exception) {
                exception.printStackTrace();
            }
            @Override
            public void messageSent(SourcePosition position, String message) {
                Logger.debug("Error: " + message);
            }
        }, null, null);

        return compiler.matchCall(compiler.nextToken());
    }

    private Class matchReturnType() {
        nextToken();

        String data = nextToken().getData();

        if (lookAhead(1).getType() != Token.TYPE_CLOSING_PARENTHESIS) {
            throw new ScriptParseException("Error type declaration expecting closing parenthesis got "
                                           + lookAhead(1).getData());
        }

        nextToken();

        if (data.equalsIgnoreCase("npc")) {
            return NPC.class;
        }

        if (data.equalsIgnoreCase("string")) {
            return String.class;
        }

        if (data.equalsIgnoreCase("gameobject")) {
            return GameObject.class;
        }

        if (data.equalsIgnoreCase("int")) {
            return Integer.class;
        }

        if (data.equalsIgnoreCase("list")) {
            return List.class;
        }

        throw new ScriptParseException("Error invalid return type not " + data);
    }

    private SignedBlock matchSignedBlock(String operation, ArrayList<RuntimeInstruction> tmpList) {
        Class returnType = null;

        if (lookAhead(1).getType() == Token.TYPE_OPENING_PARENTHESIS) {
            returnType = matchReturnType();
        }

        if (lookAhead(1).getType() != Token.TYPE_STRING) {
            throw new ScriptParseException("Error, expecting block name, found: " + lookAhead(1).getData(), current());
        }

        String blockName = nextToken().getData();

        checkBlockName(blockName);

        if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
            throw new ScriptParseException("Error, expecting opening brace found: " + lookAhead(1).getData(),
                                           current());
        }

        String[]    parameters = matchArguments();
        SignedBlock block      = null;

        if (operation.contains("local")) {
            block = new LocalSignedBlock(blockName, Integer.parseInt(replaceBlockNameWithID(blockName)), parameters);
        } else {
            block = new LocalSignedBlock(blockName, Integer.parseInt(replaceBlockNameWithID(blockName)), parameters);
        }

        block.setReturnType(returnType);

        if (lookAhead(1).getType() != Token.TYPE_OPENING_BRACE) {
            throw new ScriptParseException("Error, expecting opening bracket got: " + lookAhead(1).getData(),
                                           current());
        }

        nextToken();
        parseBlockNode(block, tmpList);
        debug("block", blockName, parameters);

        return block;
    }

    /**
     * Method loadAndSkipLocalContext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    ArrayList<Token> loadAndSkipLocalContext() {
        ArrayList<RuntimeInstruction> tmp         = new ArrayList<RuntimeInstruction>();
        ArrayList<Token>              contextList = new ArrayList<Token>(1000);

        while (hasNext()) {
            Token current = nextToken();

            if ((current.getType() == Token.TYPE_STRING) && (currentBlock == null) && current.getData()
                    .equalsIgnoreCase("def_var") || current.getData().equalsIgnoreCase("global_var")) {
                RuntimeInstruction instr = matchVariableDeclaration(current, 0);
                Node[]             nodes = instr.getParams();

                if ((nodes == null) || (nodes.length == 0)) {
                    throw new ScriptParseException("Error def_var no var name or declaration");
                }

                if (current.getData().equalsIgnoreCase("def_var")) {
                    context.addVar(new ScriptVar((Long) nodes[0].eval(context.getScope(), null),
                                                 nodes[1].eval(context.getScope(), null)));
                } else {
                    World.getWorld().getScriptManager().registerVariable(
                        new GlobalVariable(
                            new ScriptVar((Long) nodes[0].eval(null, null), nodes[1].eval(null, null)), false));
                }
            } else if ((current.getType() == Token.TYPE_CONSTRUCTOR) && hasNext()) {
                if (lookAhead(1).getData().equalsIgnoreCase("localblock")) {
                    tmp = new ArrayList<RuntimeInstruction>();

                    SignedBlock block = matchSignedBlock(nextToken().getData(), tmp);

                    block.setOperations(tmp);
                    block.setContext(context);

                    if (lookAhead(1).getType() != Token.CLOSING_BRACE) {
                        throw new ScriptParseException("Error parsing local block, expected closing brace got "
                                                       + lookAhead(1).getType());
                    }

                    nextToken();
                    context.setBlock(localFunctions.get(block.getName()), block);
                } else {
                    contextList.add(current);
                }
            } else if (hasNext() && (current.getType() == Token.TYPE_STRING)) {
                if (current.getData().equalsIgnoreCase("def_var")) {
                    if (lookAhead(1).getType() != Token.TYPE_STRING) {
                        throw new ScriptParseException("Error def_var expected var name got " + lookAhead(1).getData(),
                                                       current);
                    }

                    String varName = nextToken().getData();

                    if (lookAhead(1).getType() != Token.TYPE_ASSIGNMENT) {
                        throw new ScriptParseException("Error def_var expecting assignment operator got "
                                                       + lookAhead(1).getData(), current());
                    }

                    nextToken();

                    Token     data = nextToken();
                    ScriptVar var;

                    try {
                        Integer.parseInt(data.getData());
                        data.setType(Token.TYPE_NUMBER);
                    } catch (NumberFormatException ignored) {}

                    if (data.getType() == Token.TYPE_NUMBER) {
                        var = new ScriptVar(Text.longForName(varName),
                                            Integer.parseInt(replaceConstants(data.getData())));
                    } else if ((data.getType() == Token.TYPE_STRING) || (data.getType() == Token.TYPE_STRING_LITERAL)) {
                        var = new ScriptVar(Text.longForName(varName), replaceConstants(data.getData()));
                    } else {
                        throw new ScriptParseException("Error expecting string or number, ");
                    }

                    if (lookAhead(1).getType() == Token.TYPE_ENDSTATEMENT) {
                        nextToken();
                    }

                    this.context.addVar(var);
                } else if (current.getData().equalsIgnoreCase("def_list")) {
                    World.getWorld().getScriptManager().addList(list_ids.get(lookAhead(1).getData()));
                    nextToken();

                    continue;
                } else if (current.getData().equalsIgnoreCase("global_var")) {
                    String varName = lookAhead(1).getData();

                    if (lookAhead(2).getType() != Token.TYPE_ASSIGNMENT) {
                        throw new ScriptParseException("Error expecting assignment for global var got "
                                                       + lookAhead(2).getData());
                    }

                    Token     data = lookAhead(3);
                    ScriptVar var  = null;

                    data.setData(replaceConstants(data.getData()));

                    try {
                        Integer.parseInt(data.getData());
                        data.setType(Token.TYPE_NUMBER);
                    } catch (NumberFormatException ignored) {}

                    if (data.getType() == Token.TYPE_NUMBER) {
                        var = new ScriptVar(Text.longForName(varName),
                                            Integer.parseInt(replaceConstants(data.getData())));
                    } else if ((data.getType() == Token.TYPE_STRING) || (data.getType() == Token.TYPE_STRING_LITERAL)) {
                        var = new ScriptVar(Text.longForName(varName), replaceConstants(data.getData()));
                    } else {
                        throw new ScriptParseException("Error expecting string or number, ");
                    }

                    skip(3);
                    World.getWorld().getScriptManager().registerVariable(new GlobalVariable(var, false));

                    break;
                } else if (current.getData().equalsIgnoreCase("const")) {
                    if (lookAhead(1).getType() != Token.TYPE_STRING) {
                        throw new ScriptParseException("Expected var name got " + lookAhead(1).getData(), lookAhead(1));
                    }

                    matchConstants(current);
                } else {
                    contextList.add(current);
                }
            } else {
                contextList.add(current);
            }
        }

        return contextList;
    }

    /**
     * Method parse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    void parse() {
        List<RuntimeInstruction> tmpList = new LinkedList<RuntimeInstruction>();

        while (hasNext()) {
            Token current = nextToken();

            try {
                switch (current.getType()) {
                case Token.TYPE_JAVASCRIPT :
                    plugins.add(new JPlugin(current.getData()));

                    break;

                case Token.TYPE_CONSTRUCTOR :
                    if (currentBlock != null) {
                        throw new ScriptParseException("error, cannot redeclare a block inside another block", current);
                    }

                    if (lookAhead(1).getType() != Token.TYPE_STRING) {
                        throw new ScriptParseException("Error, invalid constructor type: " + lookAhead(1).getData(),
                                                       current);
                    }

                    String operation = nextToken().getData();

                    if (operation.equalsIgnoreCase("instanceloader")) {
                        if (lookAhead(1).getType() != Token.TYPE_STRING) {
                            throw new ScriptParseException("Error, expecting INSTANCE LOADER name, found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        String blockName = nextToken().getData();

                        if (lookAhead(1).getType() != Token.TYPE_OPENING_BRACE) {
                            throw new ScriptParseException("Error, expecting opening bracket found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        currentBlock = new InstanceLoader(blockName);
                        parseBlockNode(currentBlock, tmpList);
                    } else if (operation.equalsIgnoreCase("schedule")) {
                        if (lookAhead(1).getType() != Token.TYPE_STRING) {
                            throw new ScriptParseException("Error, expecting block name, found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        String blockName = nextToken().getData();

                        scheduledName = blockName;

                        if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                            throw new ScriptParseException("Error, expecting opening brace found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        String[] parameters = matchArguments();

                        currentBlock = new BlockNode();

                        if ((parameters == null) || (parameters.length == 0)) {
                            throw new ScriptParseException("Error, @schedule needs delay set", current());
                        }

                        if (lookAhead(1).getType() != Token.TYPE_OPENING_BRACE) {
                            throw new ScriptParseException("Error, expecting opening bracket got: "
                                                           + lookAhead(1).getData(), current);
                        }

                        scheduledDelay = Integer.parseInt(replaceConstants(parameters[1]));

                        int scheduledMode = Integer.parseInt(replaceConstants(parameters[0]));

                        if (scheduledMode > 1) {
                            throw new ScriptParseException("error in @schedule invalid timing type " + scheduledMode
                                                           + " needs to be 0-1", current());
                        }

                        this.scheduledMode = TimingType.values()[scheduledMode];
                        nextToken();
                        parseBlockNode(currentBlock, tmpList);
                    }

                    if (operation.equalsIgnoreCase("struct")) {
                        if (lookAhead(1).getType() != Token.TYPE_STRING) {
                            throw new ScriptParseException("Error, expecting struct name, found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        String structName = nextToken().getData();

                        if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                            throw new ScriptParseException("Error, expecting opening brace found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        String[] parameters = matchArguments();
                        int      structID   = structs.get(structName);

                        ScriptRuntime.structs[structID] = new Struct(parameters);

                        continue;
                    }

                    if (operation.equalsIgnoreCase("block")) {
                        if (lookAhead(1).getType() != Token.TYPE_STRING) {
                            throw new ScriptParseException("Error, expecting block name, found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        Class returnType = null;

                        if (lookAhead(1).getType() == Token.TYPE_OPENING_PARENTHESIS) {
                            returnType = matchReturnType();
                        }

                        String blockName = nextToken().getData();

                        checkBlockName(blockName);

                        if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                            throw new ScriptParseException("Error, expecting opening brace found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        String[]    parameters = matchArguments();
                        SignedBlock block      = null;

                        if (operation.contains("local")) {
                            block = new LocalSignedBlock(blockName,
                                                         Integer.parseInt(replaceBlockNameWithID(blockName)),
                                                         parameters);
                        } else {
                            block = new LocalSignedBlock(blockName,
                                                         Integer.parseInt(replaceBlockNameWithID(blockName)),
                                                         parameters);
                        }

                        block.setReturnType(returnType);
                        currentBlock = block;

                        if (lookAhead(1).getType() != Token.TYPE_OPENING_BRACE) {
                            throw new ScriptParseException("Error, expecting opening bracket got: "
                                                           + lookAhead(1).getData(), current);
                        }

                        nextToken();
                        parseBlockNode(currentBlock, tmpList);
                        debug("block", blockName, parameters);
                    } else if (operation.equalsIgnoreCase("trigger")) {
                        if (lookAhead(1).getType() != Token.TYPE_STRING) {
                            throw new ScriptParseException("error, expecting trigger name found: "
                                                           + lookAhead(1).getData(), current);
                        }

                        current = nextToken();

                        String triggerName          = current.getData();
                        int    triggerOperationCode = Trigger.getOpcodeForName(current.getData());

                        if (triggerOperationCode == -1) {
                            throw new ScriptParseException("Invalid event trigger " + current.getData(), current);
                        }

                        if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
                            throw new ScriptParseException("error, expected opening brace for directTrigger "
                                                           + current.getData() + " got "
                                                           + lookAhead(1).getData(), lookAhead(1));
                        }

                        nextToken();

                        List<String[]> parameters =
                            combinations(matchMultiParamList(replaceConstants(matchArguments())));

                        currentBlock = new Trigger(triggerName, triggerOperationCode, parameters,
                                                   this.current().position());
                        parseBlockNode(currentBlock, tmpList);
                    } else if (operation.equalsIgnoreCase("enum")) {
                        ScriptEnum match = matchEmum();

                        World.getWorld().getScriptManager().addEnum(match, enum_ids.get(match.getName()));
                    }

                    break;

                case Token.CLOSING_BRACE :
                    if (currentBlock == null) {
                        throw new ScriptParseException("error, got a closing brace when there is no block declared "
                                                       + current.position(), current);
                    }

                    if (currentBlock instanceof SignedBlock) {
                        World.getWorld().getScriptManager().put_block((SignedBlock) currentBlock);
                    } else if (currentBlock instanceof Trigger) {
                        World.getWorld().getScriptManager().addTrigger((Trigger) currentBlock);
                    } else if (currentBlock instanceof InstanceLoader) {
                        World.getWorld().getScriptManager().addInstanceLoader((InstanceLoader) currentBlock);
                    } else if ((currentBlock instanceof BlockNode) && (scheduledDelay != -1)) {
                        World.getWorld().getServerTaskRegister().register(scheduledName, currentBlock,
                                (long) scheduledDelay, scheduledMode);
                        scheduledDelay = -1;
                    }

                    currentBlock.setContext(context);
                    currentBlock.setOperations(tmpList);
                    currentBlock.addPlugins(plugins);
                    currentBlock = null;
                    tmpList.clear();

                    break;
                }
            } catch (ScriptParseException er) {
                er.printStackTrace();

                throw new ScriptParseException(er.getMessage(), er.position());
            } catch (Exception ee) {
                ee.printStackTrace();

                throw new ScriptParseException(ee.getClass().getSimpleName() + " " + ee.getMessage() + " "
                                               + current.position(), current);
            }
        }
    }

    private boolean matchConstants(Token current) {
        while (current.getType() == Token.TYPE_ENDSTATEMENT) {
            current = nextToken();
        }

        if (current.getData().equalsIgnoreCase("const") || (current.getType() == Token.TYPE_COMMA)) {
            if (lookAhead(1).getType() != Token.TYPE_STRING) {
                throw new ScriptParseException("Expected var name got " + lookAhead(1).getData() + " "
                                               + lookAhead(1).getType() + " " + current.getData() + " "
                                               + tokens.get(this.current - 1).getData() + " " + lookAhead(2).getData()
                                               + " " + tokens.get(this.current - 3).getData(), lookAhead(1));
            }

            String name = nextToken().getData();

            nextToken();

            String value = nextToken().toString();

            constants.put(name, value);

            if (!hasNext()
                    || ((lookAhead(1).getType() != Token.TYPE_COMMA)
                        &&!lookAhead(1).getData().equalsIgnoreCase("const"))) {
                return false;
            }

            return matchConstants(nextToken());
        } else {
            return false;
        }
    }

    /**
     * Method parseBlockNode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param runnable
     * @param runtimeInstructionList
     */
    void parseBlockNode(ScriptRunnable runnable, List<RuntimeInstruction> runtimeInstructionList) {
        while ((lookAhead(1) != null) && (lookAhead(1).getType() != Token.CLOSING_BRACE)) {
            if (lookAhead(1).getType() == Token.TYPE_BREAK) {
                return;
            }

            Token current = nextToken();

            switch (current.getType()) {
            case Token.TYPE_ADD :
            case Token.TYPE_MINUS :
            case Token.TYPE_MODULUS_OPERATOR :
            case Token.TYPE_STRING :
            case Token.TYPE_STRING_LITERAL :
                if (current.getData().equalsIgnoreCase("const")) {
                    if (lookAhead(1).getType() != Token.TYPE_STRING) {
                        throw new ScriptParseException("Expected var name got " + lookAhead(1).getData(), lookAhead(1));
                    }

                    matchConstants(current);
                } else if (current.getData().startsWith("[")) {
                    String labelData = current.getData().substring(1, current.getData().length() - 1);

                    runnable.setLabel(labelData, runtimeInstructionList.size());
                } else {
                    long l = 0;

                    if (isThirdPartyCall(0)) {
                        l       = matchExternalCallIdentifier(0);
                        current = nextToken();
                    }

                    RuntimeInstruction oh = matchCall(current);

                    oh.setExternalIdentifier(l);
                    runtimeInstructionList.add(oh);

                    if (current().getType() == Token.TYPE_CONSTRUCTOR) {
                        throw new ScriptParseException("error found constructor inside block ", current().position());
                    }
                }
            }
        }
    }

    /**
     * Method matchVariableAssignmentOrEdit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param current
     * @param statementType
     * @param playerVar
     *
     * @return
     */
    RuntimeInstruction matchVariableAssignmentOrEdit(Token current, int statementType, boolean playerVar) {
        if (current.getData().startsWith(VARIABLE_REFERENCER)) {
            Token operator = nextToken();

            if ((lookAhead(1).getType() != Token.TYPE_ASSIGNMENT) && (operator.getType() != Token.TYPE_ASSIGNMENT)) {
                throw new ScriptParseException("Error invalid assignment type: " + current().getData()
                                               + lookAhead(1).getData() + " "
                                               + current().position(), current().position());
            }

            if (operator.getType() != Token.TYPE_ASSIGNMENT) {
                nextToken();
            }

            Node ex = matchVariableNode();

            return new RuntimeInstruction(current.position(), RuntimeOperationMap.getByName("mod_var"), statementType,
                                          "mod_var",
                                          new GetVariableNode(Text.longForName(current.getData().substring(1))),
                                          new IntegerNode(operator.getType()), ex);
        } else if (playerVar) {
            String var      = current.getData();
            Token  operator = nextToken();

            if ((lookAhead(1).getType() != Token.TYPE_ASSIGNMENT) && (operator.getType() != Token.TYPE_ASSIGNMENT)) {
                throw new ScriptParseException("Error invalid assignment type: " + current().getData()
                                               + lookAhead(1).getData());
            }

            if (operator.getType() != Token.TYPE_ASSIGNMENT) {
                nextToken();
            }

            if ((current().getType() != Token.TYPE_ASSIGNMENT) && (operator.getType() != Token.TYPE_ASSIGNMENT)) {
                throw new ScriptParseException("Error invalid assignment type: " + current().getData()
                                               + lookAhead(1).getData() + " " + operator.getData());
            }

            Node value = matchVariableNode();

            if (value instanceof StringLiteralNode) {
                if (value.eval(null, null).toString().equalsIgnoreCase("null")) {
                    return new RuntimeInstruction(current().position(),
                                                  RuntimeOperationMap.getByName("remove_playervar"), statementType,
                                                  "remove_playervar", new StringLiteralNode(var));
                }
            }

            return new RuntimeInstruction(current().position(), RuntimeOperationMap.getByName("mod_pvar"),
                                          statementType, "mod_pvar", new StringLiteralNode(var),
                                          new IntegerNode(operator.getType()), value);
        }

        throw new ScriptParseException("Error invalid variable operator:", current().position());
    }

    /**
     * Method matchVariableDeclaration
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param current
     * @param statementType
     *
     * @return
     */
    RuntimeInstruction matchVariableDeclaration(Token current, int statementType) {
        if (lookAhead(1).getType() != Token.TYPE_STRING) {
            throw new ScriptParseException("Expected variable name, got " + lookAhead(1).getData(),
                                           current().position());
        }

        String varName = nextToken().getData();

        if (nextToken().getType() != Token.TYPE_ASSIGNMENT) {
            dumpTokens();

            throw new ScriptParseException("Expected assignment operator, got: " + current.getData() + " " + varName,
                                           current.position());
        }

        return new RuntimeInstruction(current.position(), RuntimeOperationMap.getByName("declare"), statementType,
                                      "declare", new LongNode(Text.longForName(varName)), matchVariableNode());
    }

    /**
     * Method matchParamListFromSource
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    String[] matchParamListFromSource() {
        if (lookAhead(1).getType() != Token.TYPE_OPENING_PARENTHESIS) {
            throw new ScriptParseException("error, expecting opening brace, got " + lookAhead(1).getData(),
                                           lookAhead(1));
        }

        skip(1);

        // parse parameters
        return matchArguments();
    }
}
