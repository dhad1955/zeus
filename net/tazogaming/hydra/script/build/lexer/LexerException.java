package net.tazogaming.hydra.script.build.lexer;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/04/14
 * Time: 00:15
 */
public class LexerException extends RuntimeException {

    /**
     * Constructs ...
     *
     *
     * @param message
     */
    public LexerException(String message) {
        super(message);
    }

    /**
     * Constructs ...
     *
     *
     * @param position
     * @param fileName
     * @param message
     */
    public LexerException(SourcePosition position, String fileName, String message) {
        this("Error[" + message + "] " + fileName + "@" + position.toString());
    }
}
