package net.tazogaming.hydra.script.build.lexer;

//~--- non-JDK imports --------------------------------------------------------

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;

import java.nio.CharBuffer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/04/14
 * Time: 00:23
 */
public class PeekReader {
    private Reader     in;
    private CharBuffer peakBuffer;
    int                size;

    /**
     * Constructs ...
     *
     *
     * @param in
     * @param peekLimit
     *
     * @throws IOException
     */
    public PeekReader(Reader in, int peekLimit) throws IOException {
        if (!in.markSupported()) {

            // Wrap with buffered reader, since it supports marking
            in = new BufferedReader(in);
        }

        this.size  = peekLimit;
        this.in    = in;
        peakBuffer = CharBuffer.allocate(peekLimit);
        fillPeekBuffer();
    }

    /**
     * Method limit
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int limit() {
        return size;
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @throws IOException
     */
    public void close() throws IOException {
        in.close();
    }

    private void fillPeekBuffer() throws IOException {
        peakBuffer.clear();
        in.mark(peakBuffer.capacity());
        in.read(peakBuffer);
        in.reset();
        peakBuffer.flip();
    }

    /**
     * Method read
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int read() throws IOException {
        int c = in.read();

        fillPeekBuffer();

        return c;
    }

    /**
     * Return a character that is further in the stream.
     *
     * @param lookAhead How far to look into the stream.
     * @return Character that is lookAhead characters into the stream.
     */
    public int peek(int lookAhead) {
        if ((lookAhead < 1) || (lookAhead > peakBuffer.capacity())) {
            throw new IndexOutOfBoundsException("lookAhead must be between 1 and " + peakBuffer.capacity());
        }

        if (lookAhead > peakBuffer.limit()) {
            return -1;
        }

        return peakBuffer.get(lookAhead - 1);
    }
}
