package net.tazogaming.hydra.script.build.lexer;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.build.SourcePosition;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/04/14
 * Time: 23:07
 */
public class Lexer {

    /** columnNo made: 14/08/24 */
    private int columnNo = 0;

    /** lineNo made: 14/08/24 */
    private int lineNo = 0;

    /** stringMode made: 14/08/24 */
    private boolean stringMode = true;

    /** in made: 14/08/24 */
    private PeekReader in;

    /** file made: 14/08/24 */
    private File file;

    /**
     * Constructs ...
     *
     *
     * @param file
     */
    public Lexer(File file) {
        try {
            this.file = file;
            in        = new PeekReader(new FileReader(file), 5);
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param origFile
     * @param data
     * @param skipWhite
     */
    public Lexer(File origFile, String data, boolean skipWhite) {
        try {
            this.file = origFile;

            if (file == null) {
                throw new RuntimeException("null file");
            }

            in         = new PeekReader(new StringReader(data), 5);
            stringMode = skipWhite;
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Method getTokens
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<Token> getTokens() {
        ArrayList<Token> tokens = new ArrayList<Token>();
        Token            t      = null;

        while ((t = nextToken()) != null) {
            if (t.getType() == Token.COMMENT_BEGIN) {
                while (((t = nextToken()) != null && (t.getType() != Token.COMMENT_END)));

                continue;
            }

            tokens.add(t);
        }

        return tokens;
    }

    private int lookAhead(int i) {
        return in.peek(i);
    }

    private int read() {
        try {
            int c = in.read();

            if (c == '\n') {
                lineNo++;
                columnNo = 0;
            }

            columnNo++;

            return c;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * Method close
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void close() {
        try {
            in.close();
        } catch (IOException e) {}
    }

    private int next() {
        read();

        return lookAhead(1);
    }

    private SourcePosition curpos() {
        return new SourcePosition(file.getPath(), columnNo, this.lineNo);
    }

    private Token matchToken(String data, int type) {
        next();

        return new Token(data, type, curpos());
    }

    private boolean matches(String match) {
        if (match.length() > in.limit()) {
            throw new ArrayIndexOutOfBoundsException("Error string must be less than " + in.limit());
        }

        for (int i = 0; i < match.length(); i++) {
            if (lookAhead(i + 1) != match.charAt(i)) {
                return false;
            }
        }

        return true;
    }

    private Token matchJavascript() {
        int next = 0;

        for (int i = 0; i < 3; i++) {
            next = next();
        }

        StringBuffer jsBuffer = new StringBuffer();

        long time = System.currentTimeMillis();
        do {
            char c = (char) next();

            if(System.currentTimeMillis() - time > 100){
                throw new RuntimeException();

            }

            jsBuffer.append(c);
        } while (!matches("</js>"));

        if (jsBuffer.toString().endsWith("<")) {
            jsBuffer = new StringBuffer(jsBuffer.toString().substring(0, jsBuffer.toString().length() - 1));
        }

        for (int i = 0; i < 3; i++) {
            next();
        }

        return matchToken(jsBuffer.toString(), Token.TYPE_JAVASCRIPT);
    }

    /**
     * Method skipWhiteSPace
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean skipWhiteSPace() {
        return this.stringMode;
    }

    private Token nextToken() {
        int character = lookAhead(1);

        while ((character == '\t') || (character == '\r') || (character == '\n')
                || (stringMode && (character == ' '))) {
            character = next();
        }

        switch (character) {
        case ' ' :
            return matchToken(" ", Token.TYPE_WHITESPACE);

        case '@' :
            return matchToken("@", Token.TYPE_CONSTRUCTOR);

        case ',' :
            return matchToken(",", Token.TYPE_COMMA);

        case '{' :
            return matchToken("{", Token.TYPE_OPENING_BRACE);

        case '}' :
            return matchToken("}", Token.CLOSING_BRACE);

        case '%' :
            return matchToken("%", Token.TYPE_MODULUS_OPERATOR);

        case '=' :
            if (lookAhead(2) == '=') {
                next();

                return matchToken("==", Token.TYPE_EQUALS);
            }

            return matchToken("=", Token.TYPE_ASSIGNMENT);

        case '(' :
            return matchToken("(", Token.TYPE_OPENING_PARENTHESIS);

        case '*' :
            if (lookAhead(2) == '/') {
                next();

                return matchToken("*/", Token.COMMENT_END);
            }

            return matchToken("*", Token.TYPE_MULTIPLY);

        case ')' :
            return matchToken(")", Token.TYPE_CLOSING_PARENTHESIS);

        case ';' :
            return matchToken(";", Token.TYPE_ENDSTATEMENT);

        case '/' :
            if (lookAhead(2) == '/') {
                while (true) {
                    int next = next();

                    if ((next == 65535) || (next == -1)) {
                        return null;
                    }

                    if (next == '\n') {
                        return nextToken();
                    }
                }
            } else if (lookAhead(2) == '*') {
                next();

                while (true) {
                    int next = next();

                    if (next == '*') {
                        next = next();

                        if (next == '/') {
                            next();

                            break;
                        }
                    } else if ((next == -1) || (next == 65535)) {
                        break;
                    }
                }

                return nextToken();
            }

            return matchToken("/", Token.TYPE_DIVIDE_OPERATOR);

        case '-' :
            if (Character.isDigit((char) lookAhead(2))) {
                next();

                StringBuffer sb = new StringBuffer();

                sb.append("-");

                while (Character.isDigit((char) lookAhead(1))) {
                    sb.append((char) lookAhead(1));

                    if (!Character.isDigit(lookAhead(2))) {
                        break;
                    }

                    next();
                }

                return matchToken(sb.toString(), Token.TYPE_NUMBER);
            }

            if (lookAhead(2) == '>') {
                next();

                return matchToken("->", Token.TYPE_GOTO);
            } else {
                return matchToken("-", Token.TYPE_MINUS);
            }
        case '+' :
            return matchToken("+", Token.TYPE_ADD);

        case '!' :
            if (lookAhead(2) == '=') {
                next();

                return matchToken("!=", Token.NOT_EQUAL_TO);
            }

            return matchToken("!", Token.TYPE_NOT);

        case '.' :
            return matchToken(".", Token.TYPE_DOT);

        case '|' :
            return matchToken("|", Token.TYPE_OR);

        case '&' :
            return matchToken("&", Token.TYPE_AND);

        case ':' :
            if (lookAhead(2) == ':') {
                next();

                return matchToken("::", Token.TYPE_DBL_COLON);
            }

            return matchToken(":", Token.TYPE_COLON);

        case '>' :
            if (lookAhead(2) == '=') {
                next();

                return matchToken(">=", Token.TYPE_MORE_THAN_OR_EQUAL_TO);
            }

            return matchToken(">", Token.TYPE_MORE_THAN);

        case '<' :
            if (matches("<js>")) {
                return matchJavascript();
            }

            if (lookAhead(2) == '=') {
                next();

                return matchToken("<=", Token.TYPE_LESS_THAN_OR_EQUAL_TO);
            }

            return matchToken("<", Token.TYPE_LESS_THAN);

        case '"' :
        case '[' :    // expression
            if ((character == '[') &&!stringMode) {
                return matchToken("[", Token.TYPE_STRING);
            }

            // String literal
            int          char2 = read();
            int          curChar;
            StringBuffer buffer    = new StringBuffer();
            char         closeChar = '"';

            if (character == '[') {
                closeChar = ']';
                buffer.append('[');
            }

            while ((curChar = lookAhead(1)) != closeChar) {
                buffer.append((char) read());

                if (curChar == -1) {
                    throw new LexerException("Reached end of file when parsing string");
                }
            }

            if (closeChar == ']') {
                buffer.append(closeChar);
            }

            return matchToken(buffer.toString(), Token.TYPE_STRING_LITERAL);

        default :
            curChar = (char) character;

            SourcePosition pos = curpos();

            if (Character.isDigit(curChar)) {
                buffer = new StringBuffer();

                do {
                    curChar = read();
                    buffer.append((char) curChar);
                } while (Character.isDigit(in.peek(1)));

                return new Token(buffer.toString(), Token.TYPE_NUMBER, pos);
            }

            SourcePosition pos2 = curpos();

            if (isStringParseable((char) curChar)) {
                buffer = new StringBuffer();

                while (isStringParseable((char) lookAhead(1))) {
                    if (curChar == -1) {
                        break;
                    }

                    buffer.append((char) read());
                }

                if (buffer.toString().equalsIgnoreCase("case")) {
                    return new Token(buffer.toString(), Token.TYPE_CASE, pos2);
                } else if (buffer.toString().equalsIgnoreCase("break")) {
                    return new Token(buffer.toString(), Token.TYPE_BREAK, pos2);
                }

                return new Token(buffer.toString(), Token.TYPE_STRING, pos2);
            }

            if (curChar == -1) {
                return null;
            }

            if (curChar != 65535 & !stringMode) {
                return matchToken(Character.toString((char) curChar), Token.TYPE_STRING);
            }

            if (curChar != 65535) {

                // throw new LexerException("unrecognized character: " + (char) curChar + " at line: " + lineNo + " "
                // + file.getName());
            }
        }

        return null;

//      /      throw new LexerException("Invalid token: " + new StringBuffer((char) curChar).toString());
    }

    private boolean isStringParseable(char c) {
        return Character.isLetterOrDigit((char) c) || (c == '$') || (c == '<') || (c == '>') || (c == '_')
               || (c == '~');
    }
}
