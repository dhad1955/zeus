package net.tazogaming.hydra.script.build.lexer;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.build.SourcePosition;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/12/13
 * Time: 01:13
 */
public class Token {
    public static final int
        TYPE_OPENING_PARENTHESIS   = 0,
        TYPE_CLOSING_PARENTHESIS   = 1,
        TYPE_OPENING_BRACE         = 2,
        CLOSING_BRACE              = 3,
        TYPE_COMMA                 = 4,
        TYPE_STRING_LITERAL        = 5,
        TYPE_NUMBER                = 6,
        TYPE_MODULUS_OPERATOR      = 7,
        TYPE_MULTIPLY              = 8,
        TYPE_DIVIDE_OPERATOR       = 9,
        TYPE_ENDSTATEMENT          = 32,
        TYPE_IF                    = 34,
        TYPE_LETTER                = 7,
        TYPE_FUNCTION_CALL         = 8,
        TYPE_STRING                = 312,
        TYPE_AND                   = 10,
        TYPE_OR                    = 11,
        TYPE_MORE_THAN             = 12,
        TYPE_LESS_THAN             = 13,
        TYPE_MORE_THAN_OR_EQUAL_TO = 14,
        TYPE_LESS_THAN_OR_EQUAL_TO = 15,
        TYPE_EQUALS                = 16,
        TYPE_ASSIGNMENT            = 17,
        TYPE_REF_POINTER           = 18,
        TYPE_NAME                  = 19,
        TYPE_CONSTRUCTOR           = 20,
        TYPE_ADD                   = 21,
        TYPE_MINUS                 = 22,
        TYPE_GOTO                  = 23,
        TYPE_NOT                   = 24,
        TYPE_JAVASCRIPT            = 25,
        NOT_EQUAL_TO               = 28,
        COMMENT_BEGIN              = 29,
        COMMENT_END                = 30,
        TYPE_CASE                  = 31,
        TYPE_DEFAULT               = 32,
        TYPE_BREAK                 = 33,
        TYPE_COLON                 = 34,
        TYPE_WHITESPACE            = 102,
        TYPE_DBL_COLON             = 103;
    public static final int TYPE_DOT = 105
            ;
    private String         data;
    private int            type;
    private SourcePosition pos;

    /**
     * Constructs ...
     *
     *
     * @param data
     * @param type
     */
    public Token(String data, int type) {
        this.data = data;
        this.type = type;
    }

    /**
     * Constructs ...
     *
     *
     * @param data
     * @param type
     * @param position
     */
    public Token(String data, int type, SourcePosition position) {
        this(data, type);
        this.pos = position;
    }

    /**
     * Method position
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SourcePosition position() {
        return pos;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getData() {
        return data;
    }

    /**
     * Method setData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        return data;
    }
}
