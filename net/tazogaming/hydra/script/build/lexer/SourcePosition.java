package net.tazogaming.hydra.script.build.lexer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/04/14
 * Time: 00:16
 */
public class SourcePosition {
    private int column;
    private int line;

    /**
     * Constructs ...
     *
     *
     * @param line
     * @param col
     */
    public SourcePosition(int line, int col) {
        this.column = col;
        this.line   = line;
    }

    /**
     * Method getLine
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLine() {
        return line;
    }

    /**
     * Method getColumn
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getColumn() {
        return column;
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        return line + ":" + column;
    }
}
