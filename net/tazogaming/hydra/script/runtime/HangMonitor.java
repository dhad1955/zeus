package net.tazogaming.hydra.script.runtime;

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/09/14
 * Time: 17:11
 */
public class HangMonitor extends BackgroundServiceRequest {

    // script should not take more than a second to complete.

    public static final long MAX_EXECUTION_TIME = 1000;
    /**
     * Method compute
     * Created on 14/08/18
     *
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private ScriptRuntime runtime = World.getWorld().getScriptManager();
    @Override
    public boolean compute() {
        if(ScriptRuntime.script_running && ScriptRuntime.detectedHangError == null){
            if(System.currentTimeMillis() - Core.currentTimeMillis() > MAX_EXECUTION_TIME) {
                ScriptRuntime.detectedHangError = new ScriptHangException("ERROR");
            }
        }

        return false;
    }
}
