package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 12:48
 */
public abstract class Node<T extends Object> {
    private String data;

    public long getEvaluationIdentifier() {
        return evaluationIdentifier;
    }

    public Node setEvaluationIdentifier(long evaluationIdentifier) {
        this.evaluationIdentifier = evaluationIdentifier;
        return this;
    }

    private long evaluationIdentifier;


    /**
     * Constructs ...
     *
     *
     * @param data
     */
    public Node(String data) {
        this.data = data;
    }

    /**
     * Method setData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     */
    public void setData(String data) {
        this.data = data;
    }

    /**
     * Method getData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getData() {
        return data;
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param player
     *
     * @return
     */



    public final T eval(Scope scope, Player player){
        if(this.evaluationIdentifier > 0){
            player = (Player)scope.getVar(this.evaluationIdentifier).getData();
        }

        return evaluate(scope, player);
    }

    protected abstract T evaluate(Scope scope, Player player);
}
