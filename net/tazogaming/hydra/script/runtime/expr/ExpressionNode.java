package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptParseException;

//~--- JDK imports ------------------------------------------------------------

import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 12:52
 */
public class ExpressionNode extends Node {
    private Node[] nodes;
    private Integer[]    tokenLists;

    /**
     * Constructs ...
     *
     *
     * @param tokens
     * @param nodes
     */
    public ExpressionNode(List<Integer> tokens, List<Node> nodes) {
        super(null);
        tokenLists       = new Integer[tokens.size()];
        this.nodes = new Node[nodes.size()];
        nodes.toArray(this.nodes);
        tokens.toArray(tokenLists);
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param player
     *
     * @return
     */
    @Override
    public Object evaluate(Scope scope, Player player) {
        Integer stack = 0;
        Object  eval  = nodes[0].eval(scope, player);

        if (!(eval instanceof Integer)) {
            throw new ScriptParseException("Error " + (String) eval.toString() + " is not an integer!");
        }

        Object evalObj;
        int    evalValue;

        stack = (Integer) eval;

        for (int i = 1; i < nodes.length; i++) {
            if (tokenLists[i - 1] != -1) {
                evalObj = nodes[i].eval(scope, player);

                if (!(evalObj instanceof Integer)  && !(evalObj instanceof Long) && !(evalObj instanceof Double)) {
                    throw new RuntimeException("Error " + evalObj.toString() + " is not a number!");
                }

                evalValue = (Integer) evalObj;

                if (tokenLists[i - 1] == Token.TYPE_ADD) {
                    stack += evalValue;
                } else if (tokenLists[i - 1] == Token.TYPE_DIVIDE_OPERATOR) {
                    stack = (int) Math.ceil(stack / evalValue);
                } else if (tokenLists[i - 1] == Token.TYPE_MINUS) {
                    stack -= evalValue;
                } else if (tokenLists[i - 1] == Token.TYPE_MODULUS_OPERATOR) {
                    stack %= evalValue;
                } else if (tokenLists[i - 1] == Token.TYPE_MULTIPLY) {
                    stack *= evalValue;
                } else {
                    throw new RuntimeException("error this isnt right!");
                }
            }
        }
        return stack;
    }
}
