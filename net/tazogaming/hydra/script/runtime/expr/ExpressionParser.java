package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 14:21
 */
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;

//~--- JDK imports ------------------------------------------------------------


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 00:08
 */
public class ExpressionParser {
    private Scope        scope;
    private byte         caret;
    private Node[] nodes;
    private Object       nextObj;

    /**
     * Constructs ...
     *
     *
     * @param values
     * @param scope
     */
    public ExpressionParser(Node[] values, Scope scope) {
        this.scope       = scope;
        this.caret       = 0;
        this.nodes = values;
    }

    /**
     * Method next
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object next() {
        if (!hasNext()) {
            throw new RuntimeException("Error no more arguments: " + caret);
        }

        return this.nodes[caret++].eval(scope, scope.getController());
    }

    public Long nextLong() {
        return (Long)next();
    }

    public ScriptVar nextVariable() {
        return (ScriptVar)next();
    }

    /**
     * Method nextInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int nextInt() {
        nextObj = next();

        if (!(nextObj instanceof Integer)) {
            throw new RuntimeException("Error '" + nextObj.toString() + "' is not an integer!: "
                                       + (nodes[caret - 1].getClass()));
        }

        return (Integer) nextObj;
    }

    /**
     * Method nextString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String nextString() {
        return next().toString();
    }

    /**
     * Method hasNext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasNext() {
        if (nodes == null) {
            return false;
        }

        return this.caret < nodes.length;
    }

    /**
     * Method length
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        return nodes.length;
    }

    /**
     * Method remainingStrings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] remainingStrings() {
        String[] data = new String[this.nodes.length - caret];

        for (int i = 0; i < data.length; i++) {
            data[i] = nextString();
        }

        return data;
    }

    /**
     * Method remainingObjects
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object[] remainingObjects() {
        Object[] data = new Object[this.nodes.length - caret];

        for (int i = 0; i < data.length; i++) {
            data[i] = next();
        }

        return data;
    }

    /**
     * Method nextByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte nextByte() {
        int i = nextInt();

        if (i > 128) {
            i = 128;
        }

        return (byte) i;
    }

    /**
     * Method remainingIntegers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] remainingIntegers() {
        int[] data = new int[this.nodes.length - caret];

        for (int i = 0; i < data.length; i++) {
            data[i] = nextInt();
        }

        return data;
    }
}
