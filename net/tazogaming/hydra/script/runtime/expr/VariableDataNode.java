package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.PlayerVariable;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 17:30
 */
public class VariableDataNode extends Node {
    private long           var;
    private ScriptVar      cached;
    private Scope          lastScope;
    private PlayerVariable cachedPlayerVariable;

    /**
     * Constructs ...
     *
     *
     * @param varName
     */
    public VariableDataNode(long varName) {
        super(Text.nameForLong(varName));
        this.var = varName;
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param player
     *
     * @return
     */
    @Override
    public Object evaluate(Scope scope, Player player) {


        lastScope = scope;
        cached    = scope.getVar(var);

        if (cached != null) {
            return cached.getData();
        }

        if (player == null) {
            for(ScriptVar var : scope.getVarDump(null)) {
                player.getActionSender().sendMessage("var: "+var.getHash()+" "+ Text.nameForLong(var.getHash())+" "+scope.depthTrace());
            }


            throw new RuntimeException("Variable not found: " + Text.nameForLong(var));
        }


        cachedPlayerVariable = player.getAccount().getVar(var);

        if (cachedPlayerVariable == null) {
            throw new RuntimeException("Variable not found: " + Text.nameForLong(var));
        }

        return cachedPlayerVariable.getFullData();
    }
}
