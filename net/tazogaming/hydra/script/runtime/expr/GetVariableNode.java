package net.tazogaming.hydra.script.runtime.expr;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 23:13
 */
public class GetVariableNode extends Node<ScriptVar> {

    long varName = 0;
    ScriptVar cached;


    public GetVariableNode(long varName){
        super(Text.nameForLong(varName));
        this.varName = varName;
    }

    @Override
    protected ScriptVar evaluate(Scope scope, Player player) {
        cached = scope.getVar(varName);
        if(cached == null)
            throw new RuntimeException("Variable not found: "+ Text.nameForLong(varName));

        return cached;

    }
}
