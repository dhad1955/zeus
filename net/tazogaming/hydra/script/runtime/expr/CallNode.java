package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.build.ScriptCompiler;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;
import net.tazogaming.hydra.script.runtime.instr.ReturnOperationMap;
import net.tazogaming.hydra.script.runtime.instr.impl.BlockEvaluation;
import net.tazogaming.hydra.script.runtime.js.JPlugin;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 13:07
 */
public class CallNode extends Node {
    private Node[]   params;
    private ReturnFunction call;
    private String         callName;

    /**
     * Constructs ...
     *
     *
     * @param callname
     * @param parameters
     * @param current
     */
    public CallNode(String callname, Node[] parameters, ScriptCompiler current) {
        super(callname);
        this.params = parameters;
        this.call   = ReturnOperationMap.getCall(callname);

        if (call == null) {
            if (current.replaceBlockName(callname) != -1) {
                Node[] expr = new Node[(params == null)
                                                   ? 1
                                                   : params.length + 1];

                expr[0] = new IntegerNode(current.replaceBlockName(callname));

                if (params != null) {
                  try {
                    System.arraycopy(params, 0, expr, 1, params.length);
                  }catch (ArrayIndexOutOfBoundsException o){
                      throw new RuntimeException("param mismatch "+params.length+" "+expr.length);
                  }
                }

                this.params = expr;
                this.call   = new BlockEvaluation();
            }
        }

        this.callName = callname;
    }



    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param player
     *
     * @return
     */
    @Override
    public Object evaluate(Scope scope, Player player) {
        if (call == null) {
            for (JPlugin plugin : scope.getPlugins()) {
                if ((plugin != null) && plugin.hasFunction(callName)) {
                    try {
                        return plugin.get(player, scope, callName, ((params == null) || (params.length == 0))
                                ? null
                                : new ExpressionParser(params, scope).remainingObjects());
                    } catch (Exception ee) {
                        ee.printStackTrace();

                        throw new RuntimeException("[Javascript Error]: " + ee.getClass().getSimpleName() + " "
                                                   + ee.getMessage());
                    }
                }
            }
        }

        if (call == null) {
            throw new RuntimeException("Error could not find method " + callName);
        }

        return call.eval(player, scope, new ExpressionParser(params, scope));
    }

    /**
     * Method getParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Node[] getParams() {
        return params;
    }
}
