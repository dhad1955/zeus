package net.tazogaming.hydra.script.runtime.expr;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 22:50
 */
public class LongNode extends Node<Long> {
    private long data;

    public LongNode(long l) {
        super("");
        this.data = l;
    }

    @Override
    protected Long evaluate(Scope scope, Player player) {
        return data;
    }
}
