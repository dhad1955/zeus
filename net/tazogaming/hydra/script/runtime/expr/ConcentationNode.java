package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 12:49
 */
public class ConcentationNode extends Node {
    private StringBuffer          buffer;
    private List<Node> data;

    /**
     * Constructs ...
     *
     *
     * @param data
     */
    public ConcentationNode(List<Node> data) {
        super("expression");
        this.data = data;
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param player
     *
     * @return
     */
    @Override
    public String evaluate(Scope scope, Player player) {
        buffer = new StringBuffer(120);

        for (int i = 0; i < data.size(); i++) {
            buffer.append(data.get(i).eval(scope, player));
        }

        return buffer.toString();
    }
}
