package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/08/14
 * Time: 11:24
 */
public class IntegerNode extends Node<Integer> {
    private int data;

    /**
     * Constructs ...
     *
     *
     * @param data
     */
    public IntegerNode(int data) {
        super(Integer.toString(data));
        this.data = data;
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param player
     *
     * @return
     */
    @Override
    public Integer evaluate(Scope scope, Player player) {
        return data;
    }
}
