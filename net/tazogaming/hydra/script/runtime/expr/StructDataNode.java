package net.tazogaming.hydra.script.runtime.expr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 23:50
 */
public class StructDataNode extends Node {

    /** evaluator made: 14/10/28 **/
    private GetStructVariableNode evaluator;

    /**
     * Constructs ...
     *
     *
     * @param evaluator
     */
    public StructDataNode(GetStructVariableNode evaluator) {
        super("");
        this.evaluator = evaluator;
    }

    @Override
    protected Object evaluate(Scope scope, Player player) {
        ScriptVar var = evaluator.evaluate(scope, player);

        if (var != null) {
            return var.getData();
        } else {
            throw new RuntimeException("how can it be null");
        }
    }
}
