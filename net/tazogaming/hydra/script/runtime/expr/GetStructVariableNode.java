package net.tazogaming.hydra.script.runtime.expr;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.data.StructInstance;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 23:39
 */
public class GetStructVariableNode extends Node<ScriptVar> {
    private GetVariableNode root;
    private long accessor;
    private ScriptVar cache;

    public GetStructVariableNode(long var, long access){
        super(Text.nameForLong(var)+"."+ Text.nameForLong(access));
        this.root = new GetVariableNode(var);
        this.accessor = access;
    }

    @Override
    protected ScriptVar evaluate(Scope scope, Player player) {
          cache = root.evaluate(scope, player);
         if(!(cache.getData() instanceof StructInstance))
             throw new RuntimeException("Error ("+getData()+") "+cache.getData()+" is not a struct!");

          return ((StructInstance)cache.getData()).getVar(accessor);

    }
}