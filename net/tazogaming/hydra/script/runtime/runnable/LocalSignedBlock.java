package net.tazogaming.hydra.script.runtime.runnable;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/08/14
 * Time: 22:46
 */
public class LocalSignedBlock extends SignedBlock {

    /**
     * Constructs ...
     *
     *
     * @param runtimeInstructions
     */
    public LocalSignedBlock(ArrayList<RuntimeInstruction> runtimeInstructions) {
        super(runtimeInstructions);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     */
    public LocalSignedBlock(String name) {
        super(name);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param instance
     */
    public LocalSignedBlock(String name, boolean instance) {
        super(name, instance);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param id
     */
    public LocalSignedBlock(String name, int id) {
        super(name, id);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param id
     * @param parameters
     */
    public LocalSignedBlock(String name, int id, String... parameters) {
        super(name, id, parameters);
    }
}
