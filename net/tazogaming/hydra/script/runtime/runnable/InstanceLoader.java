package net.tazogaming.hydra.script.runtime.runnable;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/06/14
 * Time: 11:58
 */
public class InstanceLoader extends ScriptRunnable {
    private String name;

    /**
     * Constructs ...
     *
     *
     * @param name
     */
    public InstanceLoader(String name) {
        this.name = name;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return this.name;
    }
}
