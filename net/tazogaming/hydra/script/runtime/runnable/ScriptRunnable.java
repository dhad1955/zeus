package net.tazogaming.hydra.script.runtime.runnable;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.Context;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.build.SourcePosition;
import net.tazogaming.hydra.script.runtime.js.JPlugin;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 00:00
 */
public abstract class ScriptRunnable {
    private ArrayList<JPlugin>       plugins      = new ArrayList<JPlugin>();
    private SwitchBlock[]            switchBlocks = new SwitchBlock[20];
    private int                      switchCaret  = 0;
    private HashMap<String, Integer> labels       = new HashMap<String, Integer>();
    private String                   fileName;
    private Context                  context;
    protected JPlugin                plugin;
    private RuntimeInstruction[]     operationList;


    public String getSourcePosition() {
        try {
            SourcePosition pos =  this.operationList[0].getPosition();
            if(pos == null)
                throw  new RuntimeException();
            return pos.getFileName();

        }catch (Exception ee) {

        }
        return "Unknown position";
    }
    /**
     * Constructs ...
     *
     */
    public ScriptRunnable() {}

    /**
     * Method getContext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Context getContext() {
        return context;
    }

    /**
     * Method setContext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param context
     */
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * Method getFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Method setOperations
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param runtimeInstructions
     */
    public void setOperations(List<RuntimeInstruction> runtimeInstructions) {
        operationList = new RuntimeInstruction[runtimeInstructions.size()];
        runtimeInstructions.toArray(operationList);
    }

    /**
     * Method setOperations
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param operations
     */
    public void setOperations(RuntimeInstruction[] operations) {
        this.operationList = operations;
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public RuntimeInstruction get(int index) {
        return operationList[index];
    }

    /**
     * Method length
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int length() {
        return operationList.length;
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        if (this instanceof SignedBlock) {
            return ((SignedBlock) this).getName();
        } else if (this instanceof Trigger) {
            return ((Trigger) this).getSubject() + " " + Arrays.toString(((Trigger) this).getParams());
        }

        return "Unknown";
    }

    /**
     * Method getPlugins
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ArrayList<JPlugin> getPlugins() {
        return this.plugins;
    }

    /**
     * Method getPlugin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public JPlugin getPlugin() {
        return plugin;
    }

    /**
     * Method addPlugins
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plugins
     */
    public void addPlugins(List<JPlugin> plugins) {
        this.plugins.addAll(plugins);
    }

    /**
     * Method getOperationList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RuntimeInstruction[] getOperationList() {
        return operationList;
    }

    /**
     * Method setOperationList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param operationList
     */
    public void setOperationList(RuntimeInstruction[] operationList) {
        this.operationList = operationList;
    }

    /**
     * Method addSwitchBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param block
     *
     * @return
     */
    public int addSwitchBlock(SwitchBlock block) {
        this.switchBlocks[switchCaret] = block;

        return switchCaret++;
    }

    /**
     * Method getLabelIndex
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public int getLabelIndex(String name) {
        if (!labels.containsKey(name)) {
            return -1;
        }

        return labels.get(name);
    }

    /**
     * Method setLabel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param index
     */
    public void setLabel(String name, int index) {
        this.labels.put(name, index);
    }
}
