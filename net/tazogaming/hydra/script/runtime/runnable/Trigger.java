package net.tazogaming.hydra.script.runtime.runnable;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 00:00
 */
import net.tazogaming.hydra.script.build.SourcePosition;
import net.tazogaming.hydra.script.runtime.js.JPlugin;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * @author Gander
 * @Date: 14-Jul-2008
 * @Time: 23:06:17.
 */
public class Trigger extends ScriptRunnable {
    public static final int
        NPC_KILLED                                = 0,
        AT_OBJECT                                 = 1,
        AT_OBJECT_2                               = 3,
        AT_OBJECT_3                               = 4,
        ITEM_AT_OBJECT                            = 5,
        ITEM_ON_ITEM                              = 6,
        ITEM_CLICK                                = 7,
        ITEM_ACTION2                              = 8,
        ITEM_ON_NPC                               = 9,
        LEVEL_UP                                  = 10,
        MAGE_NPC                                  = 11,
        ACTON_BUTTON                              = 12,
        WALK_ON                                   = 13,
        OTHER_SLOT_NPC                            = 14,
        TALK_TO_NPC                               = 15,
        TELE_NPC                                  = 16,
        DIG                                       = 17,
        AREA_ENTERED                              = 18,
        AREA_LEFT                                 = 19,
        MAGIC_ON_ITEM                             = 20,
        PICKUP_ITEM                               = 21,
        CHAR_DESIGN_FINISHED                      = 22,
        UNWIELD_ITEM                              = 23,
        DROP_ITEM                                 = 24,
        WIELD_ITEM                                = 25,
        QUEST_COMPLETE                            = 26,
        ITEM_ACTION_4                             = 27,
        NPC_ACTION_4                              = 28,
        COMMAND                                   = 29,
        BUILD                                     = 30,
        CONSTRUCTION_MAKE                         = 31,
        CONST_OBJECT_CLICK                        = 32,
        CONST_TRAP_TRIGGERED                      = 33,
        ACHIEVEMENT_COMPLETE                      = 34,
        INTERFACE_LOADED                          = 36,
        INTERFACE_UNLOADED                        = 37,
        AT_OBJECT_4                               = 38,
        GAME_BOOT                                 = 40,
        SCRIPTS_RELOADED                          = 41,
        RELOAD_COMPLETED                          = 42,
        ON_DC                                     = 43,
        ON_UNLINK                                 = 44,
        ON_CONNECT                                = 45,
        ON_NEW_LOGIN                              = 46,
        ON_RECONNECT                              = 47,
        CAST_ON_ENTITY                           = 48,
        COMMAND_BEGINSWITH                       = 49,
        ON_BUY                                   = 48,
        DIALOG_INTERFACE_BTN                     = 50,
        SUMMONING_SPEC = 51;


    public static int                   moveId    = 0;
    public HashMap<Integer, String[]> hashMap   = new HashMap<Integer, String[]>();
    private List<int[]>                 paramList = new ArrayList<int[]>();
    private int                         type;
    public int[]                        params;
    private String[]                    stringParams;
    private String                      subject;
    private String[]                    scriptLines;
    private String[]                    parameters;
    private JPlugin                     plugin;
    private int[]                       hashCodes;
    private String                      fileName;
    private SourcePosition              pos;

    /**
     * Constructs ...
     *
     *
     * @param t
     * @param params
     * @param position
     */
    public Trigger(int t, String[] params, SourcePosition position) {
        this.type = t;
        this.pos  = position;

        if (params != null) {
            setParameters(params);
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param type
     * @param parameters
     * @param position
     */
    public Trigger(String name, int type, List<String[]> parameters, SourcePosition position) {
        this.hashCodes  = new int[parameters.size()];
        this.pos        = position;
        this.subject    = name;
        this.parameters = parameters.get(0);

        int index = 0;

        this.type = type;

        for (String[] params : parameters) {
            if ((type == AREA_ENTERED) || (type == AREA_LEFT) || (type == COMMAND) || type == COMMAND_BEGINSWITH) {
                this.hashCodes[index++] = hashcode(params);    // hashcode(tmp);

                continue;
            }

            int[] tmp = new int[params.length];

            for (int i = 0; i < params.length; i++) {
                tmp[i] = Integer.parseInt(params[i]);
            }

            if (type == ACTON_BUTTON) {
                paramList.add(tmp);
            }

            hashMap.put(hashcode(tmp), params);
            this.hashCodes[index++] = hash2(tmp.length, tmp);
        }
    }

    /**
     * Method popNumericArguments
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<int[]> popNumericArguments() {
        List<int[]> tmp = this.paramList;

        this.paramList = null;

        return tmp;
    }

    /**
     * Method hashCodes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] hashCodes() {
        return hashCodes;
    }

    /**
     * Method position
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SourcePosition position() {
        return pos;
    }

    /**
     * Method getStringParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getStringParams() {
        return stringParams;
    }

    /**
     * Method setStringParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param stringParams
     */
    public void setStringParams(String[] stringParams) {
        this.stringParams = stringParams;
    }

    /**
     * Method params
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] params() {
        return this.parameters;
    }

    /**
     * Method get_main_params
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] get_main_params() {
        return params;
    }

    /**
     * Method smear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hashCode
     *
     * @return
     */
    static int smear(int hashCode) {
        hashCode ^= (hashCode >>> 20) ^ (hashCode >>> 12);

        return hashCode ^ (hashCode >>> 7) ^ (hashCode >>> 4);
    }

    /**
     * Method hashcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param params
     *
     * @return
     */
    public static int hashcode(int... params) {
        int base     = 17;
        int constant = 37;

        for (int param : params) {
            param = smear(param);
            base  = base * constant + param;
        }

        return base;
    }

    /**
     * Method hash2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param leng
     * @param params
     *
     * @return
     */
    public static int hash2(int leng, int[] params) {
        int base     = 17;
        int constant = 37;
        int param    = 0;

        for (int i = 0; i < leng; i++) {
            param = smear(params[i]);
            base  = base * constant + param;
        }

        return base;
    }


    public static int getHash(int... params){
        return hash2(params.length, params);
    }

    /**
     * Method hashcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param params
     *
     * @return
     */
    public static int hashcode(String... params) {
        return Arrays.hashCode(params);
    }

    /**
     * Method hashcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param params
     *
     * @return
     */
    public int hashcode(int type, int... params) {
        StringBuilder b = new StringBuilder();

        b.append(type);

        if(params == null)
            return b.toString().hashCode();

        for (int i = 0; i < params.length; i++) {
            b.append(params[i]);
        }

        return b.toString().hashCode();
    }

    /**
     * Method hashCode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */


    /**
     * Method setLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param lines
     *
     * @return
     */
    public Trigger setLines(String[] lines) {
        this.scriptLines = lines;

        return this;
    }

    /**
     * Method getOpcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOpcode() {
        return type;
    }

    /**
     * Method getLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getLines() {
        return this.scriptLines;
    }

    /**
     * Method setParameters
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param params
     */
    public void setParameters(String[] params) {
        this.subject      = params[0];
        this.parameters   = params;
        this.params       = new int[params.length];
        this.stringParams = new String[params.length];

        for (int i = 0; i < parameters.length; i++) {
            try {
                this.params[i] = Integer.parseInt(parameters[i]);
            } catch (Exception ee) {}

            this.stringParams[i] = parameters[i];
        }
    }

    /**
     * Method getParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getParams() {
        return this.parameters;
    }

    /**
     * Method getSubject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getSubject() {
        return this.subject;
    }

    /**
     * Method getOpcodeForName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public static int getOpcodeForName(String name) {
        if (name.equalsIgnoreCase("itemonitem")) {
            return ITEM_ON_ITEM;
        }

        if (name.equalsIgnoreCase("actionbutton")) {
            return ACTON_BUTTON;
        }

        if (name.equalsIgnoreCase("itemaction2")) {
            return ITEM_ACTION2;
        }

        if (name.equalsIgnoreCase("c_obj_click")) {
            return CONST_OBJECT_CLICK;
        }

        if (name.equalsIgnoreCase("c_trap_triggered")) {
            return CONST_TRAP_TRIGGERED;
        }

        if (name.equalsIgnoreCase("achievement_complete")) {
            return ACHIEVEMENT_COMPLETE;
        }

        if (name.equalsIgnoreCase("itemaction3")) {
            return ITEM_ACTION_4;
        }

        if(name.equalsIgnoreCase("summon_spec"))
            return SUMMONING_SPEC;

        if (name.equalsIgnoreCase("itemclick")) {
            return ITEM_CLICK;
        }

        if (name.equalsIgnoreCase("walk_on")) {
            return WALK_ON;
        }

        if (name.equalsIgnoreCase("spell")) {
            return MAGE_NPC;
        }

        if (name.equalsIgnoreCase("quest_completed")) {
            return QUEST_COMPLETE;
        }

        if (name.equalsIgnoreCase("atobject")) {
            return AT_OBJECT;
        }

        if (name.equalsIgnoreCase("interfaceopened")) {
            return INTERFACE_LOADED;
        }

        if(name.equalsIgnoreCase("dialogbtn"))
            return DIALOG_INTERFACE_BTN;

        if (name.equalsIgnoreCase("interfaceclosed")) {
            return INTERFACE_UNLOADED;
        }

        if (name.equalsIgnoreCase("atobject2")) {
            return AT_OBJECT_2;
        }

        if (name.equalsIgnoreCase("atobject3")) {
            return AT_OBJECT_3;
        }

        if (name.equalsIgnoreCase("third")) {
            return TELE_NPC;
        }

        if (name.equalsIgnoreCase("atobject4")) {
            return AT_OBJECT_4;
        }

        if (name.equalsIgnoreCase("npcaction4")) {
            return NPC_ACTION_4;
        }

        if (name.equalsIgnoreCase("talktonpc")) {
            return TALK_TO_NPC;
        }

        if (name.equalsIgnoreCase("itematobject")) {
            return ITEM_AT_OBJECT;
        }

        if (name.equalsIgnoreCase("otherslotnpc")) {
            return OTHER_SLOT_NPC;
        }

        if (name.equalsIgnoreCase("dig")) {
            return DIG;
        }

        if (name.equalsIgnoreCase("levelup")) {
            return LEVEL_UP;
        }

        if (name.equalsIgnoreCase("npckilled")) {
            return NPC_KILLED;
        }

        if (name.equalsIgnoreCase("area_entered")) {
            return AREA_ENTERED;
        }

        if (name.equalsIgnoreCase("command")) {
            return COMMAND;
        }

        if (name.equalsIgnoreCase("area_left")) {
            return AREA_LEFT;
        }

        if (name.equalsIgnoreCase("spellinv")) {
            return MAGIC_ON_ITEM;
        }

        if (name.equalsIgnoreCase("pickupitem")) {
            return PICKUP_ITEM;
        }

        if (name.equalsIgnoreCase("dropitem")) {
            return DROP_ITEM;
        }

        if (name.equalsIgnoreCase("unwield_item")) {
            return UNWIELD_ITEM;
        }

        if (name.equalsIgnoreCase("wield_item")) {
            return WIELD_ITEM;
        }

        if (name.equalsIgnoreCase("chardesign")) {
            return CHAR_DESIGN_FINISHED;
        }

        if (name.equalsIgnoreCase("on_build")) {
            return BUILD;
        }

        if (name.equalsIgnoreCase("on_make")) {
            return CONSTRUCTION_MAKE;
        }

        if (name.equalsIgnoreCase("game_boot")) {
            return GAME_BOOT;
        }

        if(name.equalsIgnoreCase("on_buy"))
            return ON_BUY;

        if (name.equalsIgnoreCase("on_reload")) {
            return SCRIPTS_RELOADED;
        }else if(name.equalsIgnoreCase("reload_finished"))
            return RELOAD_COMPLETED;
        else if(name.equalsIgnoreCase("on_dc"))
                return ON_DC;
        else if(name.equalsIgnoreCase("on_unlink"))
                return ON_UNLINK;
        else if(name.equalsIgnoreCase("new_login"))
               return ON_NEW_LOGIN;
        else if(name.equalsIgnoreCase("on_connect"))
            return ON_CONNECT;
        else if(name.equalsIgnoreCase("castentity"))
            return CAST_ON_ENTITY;
        else if(name.equalsIgnoreCase("command_op"))
            return COMMAND_BEGINSWITH;

        return -1;
    }




    /**
     * Method getPlugin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public JPlugin getPlugin() {
        return plugin;
    }

    /**
     * Method setJPlugin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plugin
     */
    public void setJPlugin(JPlugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Method setFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void setFileName(String str) {
        fileName = str;
    }

    /**
     * Method getFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }
}
