package net.tazogaming.hydra.script.runtime.runnable;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/07/14
 * Time: 15:50
 */
public class SwitchBlock extends ScriptRunnable {
    private HashMap<Integer, ScriptRunnable> runnables = new HashMap<Integer, ScriptRunnable>();
    private ScriptRunnable                     defaultBlock;

    /**
     * Method doSwitch
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param switchId
     *
     * @return
     */
    public ScriptRunnable doSwitch(int switchId) {
        if (!runnables.containsKey(switchId)) {
            return defaultBlock;
        }

        return runnables.get(switchId);
    }

    /**
     * Method put
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param block
     */
    public void put(int id, ScriptRunnable block) {
        if (runnables.containsKey(id)) {
            throw new RuntimeException("duplicate case entry");
        }

        runnables.put(id, block);
    }

    /**
     * Method setDefaultBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param block
     */
    public void setDefaultBlock(ScriptRunnable block) {
        this.defaultBlock = block;
    }
}
