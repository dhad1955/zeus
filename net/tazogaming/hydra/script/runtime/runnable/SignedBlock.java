package net.tazogaming.hydra.script.runtime.runnable;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.js.JPlugin;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * @author Gander
 * @Date: 14-Jul-2008
 * @Time: 19:07:29.
 */
public class SignedBlock extends ScriptRunnable {
    private RuntimeInstruction[] runtimeInstructionArray;
    private int                  id;
    private boolean              isInstanceLoader;
    private long[]               signature;
    private String               fileName;
    private String               blockName;
    private Class                returnType;

    /**
     * Constructs ...
     *
     *
     * @param runtimeInstructions
     */
    public SignedBlock(ArrayList<RuntimeInstruction> runtimeInstructions) {
        this.runtimeInstructionArray = new RuntimeInstruction[runtimeInstructions.size()];
        runtimeInstructions.toArray(runtimeInstructionArray);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     */
    public SignedBlock(String name) {
        this.blockName = name;
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param instance
     */
    public SignedBlock(String name, boolean instance) {
        this.isInstanceLoader = true;
        this.blockName        = name;
        World.getWorld().getScriptManager().put_block(this);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param id
     */
    public SignedBlock(String name, int id) {
        this.id        = id;
        this.blockName = name;
        World.getWorld().getScriptManager().put_block(this);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param id
     * @param parameters
     */
    public SignedBlock(String name, int id, String... parameters) {
        setSignature(parameters, 0);
        this.blockName = name;
        this.id        = id;
    }

    /**
     * Method setReturnType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param clazz
     */
    public void setReturnType(Class clazz) {
        this.returnType = clazz;
    }

    /**
     * Method getReturnType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Class getReturnType() {
        return returnType;
    }

    /**
     * Method getSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long[] getSignature() {
        return signature;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return blockName;
    }

    /**
     * Method setSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param paramNames
     * @param offset
     */
    public void setSignature(String[] paramNames, int offset) {
        if (paramNames.length == offset) {
            return;
        }

        this.signature = new long[paramNames.length - offset];

        for (int i = 0; i < paramNames.length; i++) {
            this.signature[i] = Text.stringToLong(paramNames[i - offset]);
        }
    }

    /**
     * Method setSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param names
     */
    public void setSignature(String[] names) {
        setSignature(names, 1);
    }

    /**
     * Method getPlugin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public JPlugin getPlugin() {
        return super.plugin;
    }

    /**
     * Method setJPlugin
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param plugin
     */
    public void setJPlugin(JPlugin plugin) {
        super.plugin = plugin;
    }

    /**
     * Method isInstanceLoader
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isInstanceLoader() {
        return isInstanceLoader;
    }

    /**
     * Method setInstanceLoader
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instanceLoader
     */
    public void setInstanceLoader(boolean instanceLoader) {
        isInstanceLoader = instanceLoader;
    }

    /**
     * Method getID
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getID() {
        return id;
    }

    /**
     * Method getCompiledLines
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RuntimeInstruction[] getCompiledLines() {
        return runtimeInstructionArray;
    }

    /**
     * Method setFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void setFileName(String str) {
        fileName = str;
    }

    /**
     * Method getFileName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getFileName() {
        return fileName;
    }
}
