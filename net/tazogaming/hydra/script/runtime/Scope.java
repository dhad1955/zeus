package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.build.SourcePosition;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.js.JPlugin;
import net.tazogaming.hydra.script.runtime.runnable.ScriptRunnable;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.ui.AmountTrigger;
import net.tazogaming.hydra.game.ui.ProduceActionListener;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 00:17
 *
 * @param <T>
 */
public class Scope implements AmountTrigger, ProduceActionListener {
    public static final int
        WAIT_TYPE_AMOUNT        = 0,
        WAIT_TYPE_DIALOG        = 1,
        WAIT_TYPE_MOVEMENT      = 3,
        WAIT_TYPE_CS_NPC_MOVE   = 4,
        WAIT_TYPE_SIDEBAR_CLICK = 5,
        WAIT_TYPE_STRING_       = 6,
        WAIT_TYPE_CHILD_FINISH  = 7;

    /** currentOffset made: 14/08/19 */
    private int currentOffset = 0;

    /** passedArguments made: 14/08/19 */
    private int[] passedArguments = new int[10];

    /** ifStatement made: 14/08/19 */
    private boolean ifStatement = false;

    /** ifValue made: 14/08/19 */
    private boolean ifValue = false;

    /** loopMax made: 14/08/19 */
    private int loopMax = 0;

    /** interactingNpc made: 14/08/19 */
    private NPC[] interactingNpc = new NPC[10];
    int           varOffset      = 0;

    /** activeAction made: 14/08/19 */
    private boolean activeAction = false;

    /** pauseTicks made: 14/08/19 */
    private int pauseTicks = 0;

    /** pausedAt made: 14/08/19 */
    private int pausedAt = 0;

    /** option made: 14/08/19 */
    private int option = -1;

    /** delayedAt made: 14/08/19 */
    private int delayedAt = 0;

    /** allow_action made: 14/08/19 */
    private boolean allow_action = false;

    /** terminateOnDc made: 14/08/19 */
    private boolean terminateOnDc = false;

    /** waiting made: 14/08/19 */
    private boolean[] waiting = new boolean[11];

    /** plugins made: 14/08/19 */
    private ArrayList<JPlugin> plugins = new ArrayList<JPlugin>();

    /** produceRequestBlockID made: 14/08/19 */
    private int produceRequestBlockID = -1;

    /** vars[] made: 14/08/19 */
    private ScriptVar vars[] = new ScriptVar[25];

    /** returnValue made: 14/08/19 */
    private Object returnValue    = null;
    int            exceptionCount = 0;

    /** breakLoop made: 14/08/19 */
    private boolean breakLoop = false;

    /** originalPlayer made: 14/08/19 */
    private Player originalPlayer = null;

    /** lastException made: 14/08/19 */
    private Exception lastException = null;
    public String     name          = "unknown";

    /** tmpPlugins made: 14/10/19 **/
    private ArrayList<JPlugin> tmpPlugins = new ArrayList<JPlugin>();

    /** triggerHashcode made: 14/08/31 */
    private int triggerHashcode = -1;

    /** controller made: 14/08/19 */
    private Player controller;

    /** running made: 14/08/19 */
    private ScriptRunnable running;

    /** interactingObject made: 14/08/19 */
    private GameObject interactingObject;

    /** attachement made: 14/08/19 */
    private Object attachement;

    /** isTerminated made: 14/08/19 */
    private boolean isTerminated;

    /** delayTicks made: 14/08/19 */
    private int delayTicks;

    /** params made: 14/08/19 */
    private String[] params;

    /** current made: 14/08/19 */
    private Scope current;

    /** parent made: 14/08/19 */
    private Scope parent;

    /**
     * Constructs ...
     *
     */
    public Scope() {}

    /**
     * Constructs ...
     *
     *
     * @param pla
     * @param running
     */
    public Scope(Player pla, ScriptRunnable running) {
        this.running = running;
        addPluginIfAbsent(running.getPlugin());
        this.controller     = pla;
        this.originalPlayer = controller;
    }

    /**
     * Method getLastException
     * Created on 14/08/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Exception getLastException() {
        return lastException;
    }

    /**
     * Method exception
     * Created on 14/08/19
     * Set the last error of this scope
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    public void exception(Exception ee) {
        this.lastException = ee;

        if (this.parent != null) {
            this.parent.exception(ee);
        }
    }

    /**
     * Method getReturnValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object getReturnValue() {
        return returnValue;
    }

    /**
     * Method setReturnValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param returnValue
     */
    public void setReturnValue(Object returnValue) {
        this.returnValue = returnValue;
    }

    /**
     * Method isBreakLoop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isBreakLoop() {
        return breakLoop;
    }

    /**
     * Method setBreakLoop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param breakLoop
     */
    public void setBreakLoop(boolean breakLoop) {
        this.breakLoop = breakLoop;
    }

    /**
     * Method clearVariables
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void clearVariables() {
        this.varOffset = 0;
    }

    /**
     * Method getContext
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Context getContext() {
        return running.getContext();
    }

    /**
     * Method getVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @return
     */
    public Scope getParent() {
        return this.parent;
    }

    /**
     * Method getVar
     * Created on 14/08/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param var
     *
     * @return
     */
    public ScriptVar getVar(long var) {
        ScriptVar globalVar = World.getWorld().getScriptManager().getVariable(var);

        if (globalVar != null) {
            return globalVar;
        }

        if (running != null) {
            if (running.getContext() != null) {
                globalVar = running.getContext().getVar(var);
            }
        }

        if (globalVar != null) {
            return globalVar;
        }

        for (int i = 0; i < varOffset; i++) {
            if (vars[i].getHash() == var) {
                return vars[i];
            }
        }

        if (parent != null) {
            return parent.getVar(var);
        }

        return null;
    }

    /**
     * Method declareVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param varName
     * @param var
     */
    public void declareVar(long varName, ScriptVar var) {
        vars[varOffset++] = var;
    }

    /**
     * Method replaceOrDeclare
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param varname
     * @param value
     */
    public void replaceOrDeclare(long varname, Object value) {
        for (int i = 0; i < varOffset; i++) {
            if (vars[i].getHash() == varname) {
                vars[i].set(value);

                return;
            }
        }

        declareVar(varname, new ScriptVar(varname, value));
    }

    /**
     * Method terminateCurrentMethod
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void terminateCurrentMethod() {
        if (parent == null) {
            return;
        }

        Scope current = parent;

        while (!(current.getRunning() instanceof SignedBlock) & !(current.getRunning() instanceof Trigger)) {
            Scope t = current;

            current = current.parent;
            current.terminate();
        }
    }

    /**
     * Method getCurrent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Scope getCurrent() {
        if (current == null) {
            return this;
        }

        return current.getCurrent();
    }

    /**
     * Method bind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     */
    public void bind(Scope scope) {
        if (current != null) {
            throw new RuntimeException("Error current scope is still, discarding that could cause alot of bugs.");
        }

        current            = scope;
        scope.activeAction = isActiveAction();
        scope.setParent(this);
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String toString() {
        return "[" + running.getClass().getSimpleName() + " depth: " + depthTrace() + "]";
    }

    /**
     * Method addPluginIfAbsent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void addPluginIfAbsent(JPlugin p) {
        if (p == null) {
            return;
        }

        if (!plugins.contains(p)) {
            plugins.add(p);
        }
    }

    /**
     * Method setScriptParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param par
     */
    public void setScriptParams(String[] par) {
        this.params = par;
    }

    /**
     * Method setTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void setTerminated(boolean b) {
        isTerminated = b;
    }

    /**
     * Method isTerminateOnDc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminateOnDc() {
        return terminateOnDc;
    }

    /**
     * Method setTerminateOnDc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param terminateOnDc
     */
    public void setTerminateOnDc(boolean terminateOnDc) {
        this.terminateOnDc = terminateOnDc;
    }

    /**
     * Method prev
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void prev() {
        currentOffset--;
    }

    /**
     * Method set_allow_action
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param b
     */
    public void set_allow_action(boolean b) {
        allow_action = b;
    }

    /**
     * Method isActionAllowed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isActionAllowed() {
        return allow_action;
    }

    /**
     * Method validateNpcChat
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void validateNpcChat() {
        if ((this.getInteractingNpc(0) != null) && isWaiting()) {
            if (this.getInteractingNpc(0).isDead() || this.getInteractingNpc(0).getCombatHandler().isInCombat()) {
                controller.getWindowManager().closeWindow();
                terminate();

                return;
            }
        }
    }

    /**
     * Method setParent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scr
     */
    public void setParent(Scope scr) {
        this.parent = scr;
    }

    /**
     * Method notifyParentNodes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean notifyParentNodes() {
        if (this.parent != null) {
            parent.current = null;
            parent         = null;

            return true;
        }

        return false;
    }

    /**
     * Method terminateMethod
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param returnValue
     *
     * @return
     */
    public boolean terminateMethod(Object returnValue) {
        setReturnValue(returnValue);

        boolean endOfStack = false;

        isTerminated = true;

        if ((getRunning() instanceof Trigger) || (getRunning() instanceof SignedBlock)) {
            endOfStack = true;
        }

        if (parent == null) {
            return endOfStack;
        }

        return endOfStack || parent.terminateMethod(returnValue);
    }

    /**
     * Method terminateParentNodes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean terminateParentNodes() {
        if (parent == null) {
            return false;
        }

        parent.terminate();

        return parent.terminateParentNodes();
    }

    /**
     * Method depthTrace
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int depthTrace() {
        int   i        = 0;
        Scope currents = this;

        while (currents != null) {
            currents = currents.parent;

            if (currents != null) {
                i++;
            }
        }

        return i;
    }

    /**
     * Method isAwaitingAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isAwaitingAmount() {
        return waiting[WAIT_TYPE_AMOUNT];
    }

    /**
     * Method awaitAmount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void awaitAmount() {
        waitFor(WAIT_TYPE_AMOUNT);
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return running.length();
    }

    /**
     * Method hasNext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasNext() {
        return currentOffset < size();
    }

    /**
     * Method getNext
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RuntimeInstruction getNext() {
        return this.running.get(currentOffset++);
    }

    /**
     * Method offset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int offset() {
        return currentOffset;
    }

    /**
     * Method canRun
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean canRun() {
        if (current != null) {
            return getCurrent().canRun();
        }

        if (pausedAt == 0) {
            return true;
        }

        return Core.timeSince(pausedAt) >= pauseTicks;
    }

    /**
     * Method getLoopMax
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLoopMax() {
        return loopMax;
    }

    /**
     * Method isConditionFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isConditionFlag() {
        return ifValue;
    }

    /**
     * Method setConditionFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ifValue
     */
    public void setConditionFlag(boolean ifValue) {
        this.ifValue = ifValue;
    }

    /**
     * Method getPassedArguments
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getPassedArguments() {
        return passedArguments;
    }

    /**
     * Method setPassedArguments
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param passedArguments
     */
    public void setPassedArguments(int[] passedArguments) {
        this.passedArguments = passedArguments;
    }

    /**
     * Method getRunning
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptRunnable getRunning() {
        return running;
    }

    /**
     * Method setRunning
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param running
     */
    public void setRunning(ScriptRunnable running) {
        this.running = running;

        for (JPlugin plugin : running.getPlugins()) {
            addPluginIfAbsent(plugin);
        }
    }

    /**
     * Method getPlugins
     * Created on 14/08/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<JPlugin> getPlugins() {
        if (parent == null) {
            return plugins;
        }

        tmpPlugins = new ArrayList<JPlugin>();
        tmpPlugins.addAll(plugins);
        tmpPlugins.addAll(parent.getPlugins());

        return tmpPlugins;
    }

    /**
     * Method getController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getController() {
        return controller;
    }

    /**
     * Method setController
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param controller
     */
    public void setController(Player controller) {
        this.controller = controller;

        if (this.originalPlayer == null) {
            this.originalPlayer = controller;
        }
    }

    /**
     * Method getOriginalPlayer
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Player getOriginalPlayer() {
        return this.originalPlayer;
    }

    /**
     * Method getCurrentOffset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCurrentOffset() {
        return currentOffset;
    }

    /**
     * Method setCurrentOffset
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param currentOffset
     */
    public void setCurrentOffset(int currentOffset) {
        this.currentOffset = currentOffset;
    }

    /**
     * Method isIfStatement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isIfStatement() {
        return ifStatement;
    }

    /**
     * Method setIsConditionFlag
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ifStatement
     */
    public void setIsConditionFlag(boolean ifStatement) {
        this.ifStatement = ifStatement;
    }

    /**
     * Method getInteractingNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public NPC[] getInteractingNpcs() {
        return interactingNpc;
    }

    /**
     * Method setInteractingNpcs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interactingNpc
     */
    public void setInteractingNpcs(NPC[] interactingNpc) {
        this.interactingNpc = interactingNpc;
    }

    /**
     * Method waitFor
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void waitFor(int type) {
        this.waiting[type] = true;
    }

    /**
     * Method debug
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param msg
     */
    public void debug(String msg) {
        if ((this.getController() != null) && (this.getController().getRights() >= Player.ADMINISTRATOR)) {
            controller.getActionSender().sendDev(devprefix() + msg + " " + depthTrace());
        } else {
            World.getWorld().getScriptManager().debug(msg);
        }
    }

    private String devprefix() {
        StringBuilder builder = new StringBuilder();

        if (this.running != null) {
            try {
                RuntimeInstruction current = this.running.get(currentOffset);

                if (current != null) {
                    SourcePosition pos = current.getPosition();

                    builder.append("[" + pos.getSimpleName() + ":" + pos.getRow() + "]" + "<col=FFFF00>: ");
                }
            } catch (Exception ignored) {}
        }

        return builder.toString();
    }

    /**
     * Method rewind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void rewind() {
        this.currentOffset = 0;
    }

    /**
     * Method isWaiting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isWaiting() {
        if (current != null) {
            return getCurrent().isWaiting();
        }

        for (int i = 0; i < waiting.length; i++) {
            if (waiting[i]) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method release
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void release(int type) {
        if (current != null) {
            getCurrent().release(type);

            return;
        }

        waiting[type] = false;
    }

    /**
     * Method getInteractingObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public GameObject getInteractingObject() {
        return interactingObject;
    }

    /**
     * Method setInteractingObject
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interactingObject
     */
    public void setInteractingObject(GameObject interactingObject) {
        this.interactingObject = interactingObject;
    }

    /**
     * Method getInteractingNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @return
     */
    public NPC getInteractingNpc(int i) {
        try {
            if (controller != null) {
                if (controller.getCutScene() != null) {
                    return controller.getCutScene().getNpc(i);
                }
            }

            if (interactingNpc == null) {
                return null;
            }

            return interactingNpc[i];
        } catch (Exception ee) {
            return null;
        }
    }

    /**
     * Method setInteractingNpc
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interactingNpc
     */
    public void setInteractingNpc(NPC interactingNpc) {
        this.interactingNpc[0] = interactingNpc;
    }

    /**
     * Method setInteractingNPC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param interactingNPC
     * @param i
     */
    public void setInteractingNPC(NPC interactingNPC, int i) {
        this.interactingNpc[i] = interactingNPC;
    }

    /**
     * Method getPauseTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPauseTicks() {
        return pauseTicks;
    }

    /**
     * Method setPauseTicks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pauseTicks
     */
    public void setPauseTicks(int pauseTicks) {
        this.pauseTicks = pauseTicks;
        pausedAt        = Core.currentTime;
    }

    /**
     * Method isActiveAction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isActiveAction() {
        return activeAction || ((parent != null) && parent.isActiveAction());
    }

    /**
     * Method setActiveAction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param activeAction
     */
    public void setActiveAction(boolean activeAction) {
        this.activeAction = activeAction;

        if (parent != null) {
            parent.setActiveAction(activeAction);
        }
    }

    /**
     * Method delay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ticks
     *
     * @return
     */
    public boolean delay(int ticks) {
        delayedAt  = Core.currentTime;
        delayTicks = ticks;

        boolean endOfMethod = false;

        // isTerminated = true;
        if ((getRunning() instanceof Trigger) || (getRunning() instanceof SignedBlock)) {
            endOfMethod = true;
        }

        return endOfMethod || parent.delay(ticks);
    }

    /**
     * Method getCore
     * Created on 14/08/21
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptRunnable getCore() {
        return this.running;
    }

    /**
     * Method isDelayed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDelayed() {
        return delayTicks != 0;    // / && Core.timeSince(delayedAt) >= delayTicks;
    }

    /**
     * Method hasDelayTimePassed
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasDelayTimePassed() {
        return Core.timeSince(delayedAt) >= delayTicks;
    }

    /**
     * Method setTriggerHashcode
     * Created on 14/08/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hash
     */
    public void setTriggerHashcode(int hash) {
        this.triggerHashcode = hash;
    }

    /**
     * Method getTriggerHashCode
     * Created on 14/08/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTriggerHashCode() {
        if (!(this.running instanceof Trigger) && (parent != null)) {
            return parent.getTriggerHashCode();
        }

        return triggerHashcode;
    }

    /**
     * Method getRootTrigger
     * Created on 14/08/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Trigger getRootTrigger() {
        if (this.getRunning() instanceof Trigger) {
            return (Trigger) this.getRunning();
        }

        if (this.parent == null) {
            throw new RuntimeException("Error current scope is not running a trigger!");
        }

        return parent.getRootTrigger();
    }

    /**
     * Method setAttachement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param e
     */
    public void setAttachement(Object e) {
        this.attachement = e;
    }

    /**
     * Method getAttachement
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object getAttachement() {
        return attachement;
    }

    /**
     * Method varExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public boolean varExists(long name) {
        for (int i = 0; i < varOffset; i++) {
            if (vars[i].getHash() == name) {
                return true;
            }
        }

        return false;
    }

    /**
     * Method getVarDump
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param vars
     *
     * @return
     */
    public List<ScriptVar> getVarDump(List<ScriptVar> vars) {
        if (vars == null) {
            vars = new ArrayList<ScriptVar>();
        }

        for (int i = 0; i < varOffset; i++) {
            vars.add(this.vars[i]);
        }

        if (parent != null) {
            parent.getVarDump(vars);
        }

        return vars;
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void terminate() {
        for (ScriptVar var : vars) {
            if (var == null) {
                continue;
            }

            NPC npc = null;

            if (var.getData() instanceof NPC) {
                npc = (NPC) var.getData();
            }

            if ((getInteractingNpc(0) != null) && (controller != null)) {
                getInteractingNpc(0).removeChatting(controller);
            }

            if ((npc != null) && (npc.getCurrentController() == controller)) {
                npc.setCurrentController(null);

                if ((controller != null) && controller.getFocus().isFocused(npc)) {
                    controller.getFocus().unFocus();
                }
            }
        }

        if ((parent != null) &&!parent.waiting[WAIT_TYPE_CHILD_FINISH]) {
            parent.terminate();
        }

        isTerminated = true;
    }

    /**
     * Method isTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminated() {
        return isTerminated;
    }

    /**
     * Method getOption
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOption() {
        return option;
    }

    /**
     * Method setOption
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param option
     */
    public void setOption(int option) {
        this.option = option;
    }

    /**
     * Method amountEntered
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param itemSlot
     * @param amount
     */
    @Override
    public void amountEntered(Player pla, int itemSlot, int amount) {
        long hash = Text.stringToLong("amount");

        if (getVar(hash) == null) {
            declareVar(hash, new ScriptVar(hash, amount));
        } else {
            getVar(hash).set(amount);
        }

        this.release(WAIT_TYPE_AMOUNT);
    }

    /**
     * Method declare
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param obj
     */
    public void declare(long name, Object obj) {
        this.vars[varOffset++] = new ScriptVar(name, obj);
    }

    /**
     * Method onProduce
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     * @param amount
     * @param slot
     * @param player
     */
    @Override
    public void onProduce(int itemId, int amount, int slot, Player player) {
        player.terminateActions();
        player.terminateScripts();

        ScriptProduceAction action = new ScriptProduceAction(this.produceRequestBlockID, itemId, 3, player);

        action.setActionAmount(amount);
        player.setCurrentAction(action);
    }
}
