package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.runtime.login.impl.NewLoginHook;
import net.tazogaming.hydra.runtime.login.impl.RehashedLoginHook;
import net.tazogaming.hydra.script.build.ScriptCompiler;
import net.tazogaming.hydra.script.build.ScriptCompilerOutput;
import net.tazogaming.hydra.script.build.SourcePosition;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.data.GlobalVariable;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.data.ScriptVariableInjector;
import net.tazogaming.hydra.script.runtime.data.Struct;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.script.runtime.js.JPlugin;
import net.tazogaming.hydra.script.runtime.runnable.BlockNode;
import net.tazogaming.hydra.script.runtime.runnable.InstanceLoader;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.script.sched.ScriptScheduledTask;
import net.tazogaming.hydra.util.*;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

import java.util.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 00:21
 */
public class ScriptRuntime {
    public static JPlugin      GLOBAL_SCOPE  = new JPlugin(new File("config/scripts/core.js"));
    public static final long[] ARGS          = new long[20];
    static int                 errors        = 0;
    public static int          trigger_count = 0;
    public static int          block_count   = 0;
    public static int          enum_count    = 0;
    public static Struct[]     structs       = new Struct[250];

    /**
     * Method addTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    private static List<Trigger> unlinkHooks = new ArrayList<Trigger>();

    /** dcHooks made: 14/08/19 */
    private static List<Trigger>      dcHooks        = new ArrayList<Trigger>();
    public static volatile boolean    script_running = false;
    public static ScriptHangException detectedHangError;

    static {
        for (int i = 0; i < ARGS.length; i++) {
            ARGS[i] = Text.stringToLong("arg" + i);
        }
    }

    /** bootTriggers made: 14/08/18 */
    private List<Trigger> bootTriggers = new ArrayList<Trigger>();

    /** reloadTriggers made: 14/08/18 */
    private List<Trigger> reloadTriggers = new ArrayList<Trigger>();

    /** trigger_map[] made: 14/08/18 */
    private HashMap<Integer, Trigger> trigger_map[] = new HashMap[60];

    /** blocks made: 14/08/18 */
    private SignedBlock[] blocks = new SignedBlock[2500];

    /** name_map made: 14/08/18 */
    private HashMap<String, Integer> name_map = new HashMap<String, Integer>();

    /** instance_loaders made: 14/08/18 */
    private HashMap<String, InstanceLoader> instance_loaders = new HashMap<String, InstanceLoader>();

    /** enumMap made: 14/08/18 */
    private ScriptEnum[] enumMap = new ScriptEnum[100];

    /** listMap made: 14/08/18 */
    private List[] listMap = new List[100];

    /** globalVariables made: 14/08/18 */
    private GlobalVariable[] globalVariables = new GlobalVariable[30];

    /** globalVarCounter made: 14/08/18 */
    private int globalVarCounter = 0;

    /** scheduledTasks made: 14/08/18 */
    private List<ScriptScheduledTask> scheduledTasks = new ArrayList<ScriptScheduledTask>();

    /** streamPlayers made: 14/08/18 */
    private ArrayList<Player> streamPlayers = new ArrayList<Player>();

    // need a separate HashMap for interfaces due to so many collisions

    /** interfaceTriggerMap made: 14/08/18 */
    private HashMap<Integer, Trigger[][]> interfaceTriggerMap = new HashMap<Integer, Trigger[][]>();

    /** scriptErrorLog made: 14/08/18 */
    private PrintStream scriptErrorLog;

    /** scriptDebugLog made: 14/08/18 */
    private PrintStream scriptDebugLog;

    /**
     * Constructs ...
     *
     */
    public ScriptRuntime() {
        try {
            scriptErrorLog = new PrintStream(new FileOutputStream("config/SCRIPT_ERRORS.log", true));
            scriptDebugLog = new PrintStream(new FileOutputStream("config/script_debug.log", true));
        } catch (Exception ee) {}
    }

    /**
     * Method runScript
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param run
     *
     * @return
     */
    public boolean runScript(Scope run) {
        script_running = true;

        boolean eval = evaluate(run);

        script_running = false;

        return eval;
    }

    private void addInterfaceTrigger(Trigger actionButtonTrigger) {
        for (int[] buttonConditions : actionButtonTrigger.popNumericArguments()) {
            if (buttonConditions.length == 2) {
                putInterfaceTrigger(buttonConditions[0], buttonConditions[1], 0, -1, actionButtonTrigger);
            } else if (buttonConditions.length == 3) {
                putInterfaceTrigger(buttonConditions[0], buttonConditions[1], buttonConditions[2], -1,
                                    actionButtonTrigger);
            } else if (buttonConditions.length == 4) {
                putInterfaceTrigger(buttonConditions[0], buttonConditions[1], buttonConditions[2], buttonConditions[3],
                                    actionButtonTrigger);
            }
        }
    }

    private void putInterfaceTrigger(int id, int childID, int contextSlot, int interfaceSlot, Trigger t) {
        int hash = GameFrame.interfaceHash(id, childID);

        if (!interfaceTriggerMap.containsKey(hash)) {
            interfaceTriggerMap.put(hash, new Trigger[6][35]);
        }

        interfaceTriggerMap.get(hash)[contextSlot][interfaceSlot + 1] = t;
    }

    /**
     * Method getInterfaceTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param child
     * @param contextSlot
     * @param interfaceSlot
     *
     * @return
     */
    public Trigger getInterfaceTrigger(int id, int child, int contextSlot, int interfaceSlot) {
        if (!interfaceTriggerMap.containsKey(GameFrame.interfaceHash(id, child))) {
            return null;
        }

        Trigger[][] map = interfaceTriggerMap.get(GameFrame.interfaceHash(id, child));

        if (map[contextSlot][interfaceSlot + 1] != null) {
            return map[contextSlot][interfaceSlot + 1];
        }

        return null;
    }

    /**
     * Method addDeveloper
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public void addDeveloper(Player player) {
        streamPlayers.add(player);
    }

    /**
     * Method debug
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void debug(String str) {
        for (Player player : streamPlayers) {
            if (player.getIoSession().isConnected()) {
                player.getActionSender().sendDev(str);
            }
        }
    }

    /**
     * Method addReloadTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void addReloadTrigger(Trigger t) {
        this.reloadTriggers.add(t);
    }

    /**
     * Method updateTasks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateTasks() {
        for (Iterator<ScriptScheduledTask> tasks = scheduledTasks.iterator(); tasks.hasNext(); ) {
            ScriptScheduledTask taskToRun = tasks.next();

            if (taskToRun.terminated() || taskToRun.run()) {
                tasks.remove();
            }
        }
    }

    /**
     * Method registerScheduledTask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param node
     * @param name
     * @param delay
     */
    public void registerScheduledTask(BlockNode node, String name, int delay) {
        for (ScriptScheduledTask task : scheduledTasks) {
            if (task.getName().equalsIgnoreCase(name)) {
                throw new RuntimeException("Error duplicate task entry for " + name);
            }
        }

        Scope scope = new Scope();

        scope.setRunning(node);
        scheduledTasks.add(new ScriptScheduledTask(name, scope, delay, false));
    }

    /**
     * Method registerScheduledTask
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param runnable
     * @param delay
     * @param autoRemove
     */
    public void registerScheduledTask(String name, SignedBlock runnable, int delay, boolean autoRemove) {
        Scope scope = new Scope();

        scope.setRunning(runnable);
        scheduledTasks.add(new ScriptScheduledTask(name, scope, delay, autoRemove));

        for (ScriptScheduledTask task : scheduledTasks) {
            if (task.getName().equalsIgnoreCase(name)) {
                throw new RuntimeException("Error duplicate task entry for " + name);
            }
        }

        scheduledTasks.add(new ScriptScheduledTask(name, scope, delay, autoRemove));
    }

    /**
     * Method getVariable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param var
     *
     * @return
     */
    public ScriptVar getVariable(long var) {
        for (int i = 0; i < globalVarCounter; i++) {
            if ((globalVariables[i] != null) && (globalVariables[i].getVar().getHash() == var)) {
                return globalVariables[i].getVar();
            }
        }

        return null;
    }

    /**
     * Method registerVariable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param var
     */
    public void registerVariable(GlobalVariable var) {
        for (int i = 0; i < globalVarCounter; i++) {
            if (globalVariables[i] == null) {
                globalVariables[i] = var;

                return;
            }

            if (globalVariables[i].getVar().getHash() == var.getVar().getHash()) {
                throw new RuntimeException("Error variable already exists: " + var.getVar().getHash());
            }
        }

        globalVariables[globalVarCounter++] = var;
    }

    /**
     * Method updateVariable
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param data
     */
    public void updateVariable(String name, Object data) {
        for (int i = 0; i < globalVarCounter; i++) {
            if (globalVariables[i].getVar().getHash() == Text.longForName(name)) {
                globalVariables[i].setVar(new ScriptVar(Text.longForName(name), data));

                return;
            }
        }

        registerVariable(new GlobalVariable(new ScriptVar(Text.longForName(name), data), false));
    }

    /**
     * Method removeVariable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void removeVariable(long name) {
        for (int i = 0; i < globalVariables.length; i++) {
            if ((globalVariables[i] != null) && (globalVariables[i].getVar().getHash() == name)) {
                globalVariables[i] = null;

                return;
            }
        }
    }

    /**
     * Method addList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param listID
     */
    public void addList(int listID) {
        this.listMap[listID] = new ArrayList<Object>();
    }

    /**
     * Method getList
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public List<Object> getList(int id) {
        return this.listMap[id];
    }

    /**
     * Method getEnum
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param triggers
     *
     * @return
     */
    public ScriptEnumerator getEnum(int id, int... triggers) {
        if (enumMap[id] == null) {
            return null;
        }

        return enumMap[id].get(triggers);
    }

    /**
     * Method getEnum
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public ScriptEnum getEnum(int id) {
        return enumMap[id];
    }

    /**
     * Method addEnum
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param id
     */
    public void addEnum(ScriptEnum type, int id) {
        if (type == null) {
            return;
        }

        enum_count++;
        this.enumMap[id] = type;
    }

    /**
     * Method addInstanceLoader
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param loader
     */
    public void addInstanceLoader(InstanceLoader loader) {
        instance_loaders.put(loader.getName(), loader);
    }

    /**
     * Method logRuntimeError
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @param messages
     */
    public static void logErrorMessage(String... messages) {
        ScriptRuntime runtime = World.getWorld().getScriptManager();

        runtime.scriptErrorLog.println("----- ERROR MESSAGE --- " + messages[0] + " ------");

        for (String str : messages) {
            runtime.scriptErrorLog.println(str);
        }

        runtime.scriptErrorLog.println(
            "----------------------------------------------------------------------------------------------------");
    }

    /**
     * Method logRuntimeError
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param error
     * @param script
     * @param instr
     */
    public static void logRuntimeError(Exception error, Scope script, RuntimeInstruction instr) {
        script.exception(error);

        ScriptRuntime runtime = World.getWorld().getScriptManager();

        runtime.scriptErrorLog.println("  --------------- Exception occurred ----------------------");
        runtime.scriptErrorLog.println("Exception type: " + error.getClass().getSimpleName());

        if ((script.getRunning().get(0) != null) && (script.getRunning().get(0).getPosition() != null)) {
            runtime.scriptErrorLog.println("Script name:  " + script.getRunning().get(0).getPosition().getFileName());
        }

        if (script.getController() != null) {
            runtime.scriptErrorLog.println("Player username: " + script.getController().getUsername());
            runtime.scriptErrorLog.println("Player location: " + script.getController().getLocation());
        }

        if ((instr != null) && (instr.getOperation() != null) && (instr.getPosition() != null)) {
            runtime.scriptErrorLog.print("Instruction name: " + instr.getOperation().getMethodName() + "()");

            if (instr.getPosition() != null) {
                runtime.scriptErrorLog.print(" at line " + instr.getPosition().getRow() + " (approximate)");
            }

            runtime.scriptErrorLog.println();
        }

        runtime.scriptErrorLog.println("STACK TRACE:");

        for (StackTraceElement e : error.getStackTrace()) {
            runtime.scriptErrorLog.println("at " + e.getClassName() + " " + e.getMethodName() + "()" + " line: "
                                           + e.getLineNumber());
        }

        runtime.scriptErrorLog.println(
            "-----------------------------------------------------------------------------------------------------------------------");
        runtime.scriptErrorLog.println();
        runtime.scriptErrorLog.flush();
    }

    /**
     * Method put_block
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param block
     */
    public void put_block(SignedBlock block) {
        blocks[block.getID()] = block;
        name_map.put(block.getName(), block.getID());
        block_count++;
    }

    /**
     * Method get_block
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public SignedBlock get_block(String name) {
        if (!name_map.containsKey(name)) {
            return null;
        }

        return blocks[name_map.get(name)];
    }

    /**
     * Method getInstanceLoader
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public InstanceLoader getInstanceLoader(String name) {
        return instance_loaders.get(name);
    }

    /**
     * Method invalidate_blocks
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void invalidate_blocks() {
        blocks = new SignedBlock[1000];
    }

    /**
     * Method clear
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void clear() {
        block_count   = 0;
        trigger_count = 0;
        enum_count    = 0;
        trigger_map   = new HashMap[60];
        invalidate_blocks();
    }

    /**
     * Method getBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public SignedBlock getBlock(int id) {
        return blocks[id];
    }

    /**
     * Method reload
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     */
    public void reload(final Player pla) {
        World.getWorld().getServerTaskRegister().removeAllTickableScripts();

        List<Trigger> tmp = new ArrayList<Trigger>();

        for (Trigger t : reloadTriggers) {
            if (t.getOpcode() == Trigger.SCRIPTS_RELOADED) {
                Scope scope = new Scope();

                scope.setRunning(t);
                scope.setController(pla);

                if (!runScript(scope)) {
                    pla.getActionSender().sendDev(Text.RED("Script reloading failed, reload script tried to pause"));
                }
            } else {
                tmp.add(t);
            }
        }

        reloadTriggers.clear();
        clear();
        interfaceTriggerMap.clear();
        instance_loaders.clear();
        unlinkHooks.clear();
        dcHooks.clear();
        NewLoginHook.loginHooks.clear();
        RehashedLoginHook.loginHooks.clear();
        scheduledTasks.clear();
        globalVarCounter = 0;
        structs          = new Struct[30];
        this.listMap     = new List[30];
        errors           = 0;

        ScriptCompilerOutput playerOutput = new ScriptCompilerOutput() {
            @Override
            public void exceptionCaught(Exception err) {
                if (err instanceof ScriptParseException) {
                    ScriptParseException exception = (ScriptParseException) err;

                    if (exception.position() != null) {
                        pla.getActionSender().sendDev("<col=" + Text.RED + ">[Compiler error] in file "
                                                      + exception.position().getFileName() + " at line "
                                                      + exception.position().getRow());
                    } else {
                        pla.getActionSender().sendDev("[Unknown posisiton!");
                    }
                }

                errors++;
                pla.getActionSender().sendDev("<col=" + Text.RED + ">[Error message]: " + err.getMessage());
                pla.getActionSender().sendMessage(Text.RED("[Error] " + err.getMessage()));
                pla.getActionSender().sendDev(
                    "<col=ffffff>--------------------------------------------------------------------------------------------");

                if (Config.enable_debugging) {
                    err.printStackTrace();
                }
            }
            @Override
            public void messageSent(SourcePosition position, String message) {
                pla.getActionSender().sendDev("[Compiler]: " + message);
            }
        };

        ScriptCompiler.compile(playerOutput, "config/scripts");

        if (errors > 0) {
            pla.getActionSender().sendMessage(Text.RED("Warning: " + errors + " compiler errors"));
        } else {
            pla.getActionSender().sendMessage(Text.GREEN("Compilation successful total triggers: " + trigger_count
                    + " blocks: " + block_count + " enums: " + enum_count));
        }

        for (Trigger t : tmp) {
            Scope scope = new Scope(pla, t);

            runScript(scope);
        }

        tmp.clear();
    }

    /**
     * Method runBootTriggers
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void runBootTriggers() {
        for (Trigger t : bootTriggers) {
            Scope c = new Scope();

            c.setRunning(t);
            runScript(c);
        }
    }

    /**
     * Method onDC
     * Created on 14/08/22
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void onDC(Player player) {
        runTriggerList(player, dcHooks);
    }

    /**
     * Method onUnlink
     * Created on 14/08/22
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     */
    public static void onUnlink(Player player) {
        runTriggerList(player, unlinkHooks);
    }

    /**
     * Method runTriggerList
     * Created on 14/08/22
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param triggers
     */
    public static void runTriggerList(Player player, List<Trigger> triggers) {
        Scope scope;

        for (Trigger t : triggers) {
            scope = new Scope();
            scope.setRunning(t);
            scope.setController(player);

            if (!World.getWorld().getScriptManager().runScript(scope)) {
                player.getNewScriptQueue().add(scope);
            }
        }
    }

    /**
     * Method addTrigger
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void addTrigger(Trigger t) {
        if (t.getOpcode() == Trigger.GAME_BOOT) {
            bootTriggers.add(t);

            return;
        } else if ((t.getOpcode() == Trigger.SCRIPTS_RELOADED) || (t.getOpcode() == Trigger.RELOAD_COMPLETED)) {
            reloadTriggers.add(t);

            return;
        } else if (t.getOpcode() == Trigger.ACTON_BUTTON) {
            addInterfaceTrigger(t);

            return;
        } else if (t.getOpcode() == Trigger.ON_NEW_LOGIN) {
            NewLoginHook.loginHooks.add(t);

            return;
        } else if (t.getOpcode() == Trigger.ON_CONNECT) {
            RehashedLoginHook.loginHooks.add(t);

            return;
        } else if (t.getOpcode() == Trigger.ON_UNLINK) {
            unlinkHooks.add(t);

            return;
        } else if (t.getOpcode() == Trigger.ON_DC) {
            dcHooks.add(t);

            return;
        }

        if (trigger_map[t.getOpcode()] == null) {
            trigger_map[t.getOpcode()] = new HashMap<Integer, Trigger>();
        }

        if (t.getOpcode() == Trigger.WALK_ON) {
            World.getWorld().getTile(Integer.parseInt(t.params()[0]), Integer.parseInt(t.params()[1]),
                                     3).setWalkTrigger(t);

            return;
        }

        for (Integer hashcode : t.hashCodes()) {
            if (trigger_map[t.getOpcode()].containsKey(hashcode)) {
                Trigger collided = trigger_map[t.getOpcode()].get(hashcode);

                throw new RuntimeException("Error collision, " + t.position().getFileName() + ": "
                                           + t.position().getRow() + " " + Arrays.toString(t.hashMap.get(hashcode))
                                           + " " + collided.position().getFileName() + ":"
                                           + collided.position().getRow() + " "
                                           + Arrays.toString(collided.hashMap.get(hashcode)));
            }

            trigger_map[t.getOpcode()].put(hashcode, t);
            trigger_count++;
        }
    }

    /**
     * Method getTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param params
     *
     * @return
     */
    public Trigger getTrigger(int type, int... params) {
        if (trigger_map[type] == null) {
            return null;
        }

        Trigger[] returnList = new Trigger[5];
        int       off        = 0;

        for (int i = params.length; i > 0; i--) {
            Trigger k = trigger_map[type].get(Trigger.hash2(i, params));

            if (k != null) {
                returnList[off++] = k;
            }
        }

        return returnList[0];
    }

    /**
     * Method isBlocked
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param type
     * @param params
     *
     * @return
     */
    public boolean isBlocked(Player player, int type, int... params) {
        Trigger t = getTrigger(type, params);

        if (t != null) {
            Scope scr = new Scope();

            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    scr.declareVar(ARGS[i], new ScriptVar(ARGS[i], params[i]));
                }
            }

            scr.setRunning(t);
            scr.setController(player);
            World.getWorld().getScriptManager().runScript(scr);

            if (!scr.isActionAllowed()) {
                return true;
            }

            return false;
        } else {
            return false;
        }
    }

    /**
     * Method getTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     * @param params
     *
     * @return
     */
    public Trigger getTrigger(int type, String... params) {
        if (trigger_map[type] == null) {
            return null;
        }

        return trigger_map[type].get(Trigger.hashcode(params));
    }

    /**
     * Method directTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param type
     * @param params
     *
     * @return
     */
    public boolean trigger(Player pla, int type, int... params) {
        return directTrigger(pla, null, type, params);
    }

    /**
     * Method do_trigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param t
     * @param inj
     *
     * @return
     */
    public boolean do_trigger(Player pla, Trigger t, ScriptVariableInjector inj) {
        Scope script = new Scope();

        inj.injectVariables(script);
        script.setRunning(t);
        script.setController(pla);
        pla.getNewScriptQueue().add(script);

        return true;
    }

    /**
     * Method do_trigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param type
     * @param paramz
     *
     * @return
     */
    public boolean do_trigger(Player pla, int type, String... paramz) {
        Trigger t = getTrigger(type, paramz);

        if (t == null) {
            return false;
        }

        Scope script = new Scope();

        for (int ind = 0; ind < paramz.length; ind++) {
            script.declareVar(ARGS[ind], new ScriptVar(ARGS[ind], paramz[ind]));
        }

        script.setRunning(t);
        script.setController(pla);
        pla.getNewScriptQueue().add(script);

        return true;
    }

    /**
     * Method directTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param attachment
     * @param type
     * @param params
     *
     * @return
     */
    public boolean trigger(Player pla, ScriptVariableInjector attachment, int type, String... params) {
        Trigger t = getTrigger(type, params);

        if (t == null) {
            return false;
        }

        if (pla.isTriggerAlreadyRunning(t)) {
            return true;
        }

        Scope script = new Scope();

        for (int ind = 0; ind < params.length; ind++) {
            script.declareVar(ARGS[ind], new ScriptVar(ARGS[ind], params[ind]));
        }

        script.setAttachement(attachment);
        script.setRunning(t);
        script.setController(pla);

        if (attachment != null) {
            attachment.injectVariables(script);
        }

        pla.getNewScriptQueue().add(script);

        return true;
    }

    /**
     * Method directTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param attachment
     * @param type
     * @param params
     *
     * @return
     */
    public boolean directTrigger(Player pla, ScriptVariableInjector attachment, int type, int... params) {
        Trigger t       = getTrigger(type, params);
        boolean reverse = false;

        if (t == null) {
            if (type == Trigger.ITEM_ON_ITEM) {
                int[] tmp = new int[2];

                tmp[0] = params[1];
                tmp[1] = params[0];
                t      = getTrigger(type, tmp);

                if (t == null) {
                    return false;
                }
            }

            return false;
        }

        if (pla.isTriggerBanned(type, Trigger.hash2(params.length, params)) || pla.isTriggerAlreadyRunning(t)) {
            return true;
        }

        Scope script = new Scope();

        script.setAttachement(attachment);
        script.setRunning(t);

        for (int ind = 0; ind < params.length; ind++) {
            script.declareVar(ARGS[ind], new ScriptVar(ARGS[ind], params[ind]));
        }

        if (attachment != null) {
            attachment.injectVariables(script);
        }

        script.setTriggerHashcode(Trigger.hash2(params.length, params));
        script.setController(pla);

        if (attachment instanceof GameObject) {
            script.setInteractingObject((GameObject) attachment);
        } else if (attachment instanceof NPC) {
            NPC npc = (NPC) attachment;

            if (!npc.isBusy(pla)) {
                npc.setCurrentController(pla);
            }

            script.setInteractingNpc((NPC) attachment);
        }

        if (!runScript(script)) {
            pla.getNewScriptQueue().add(script);
        }

        return true;
    }

    /**
     * Method forceTrigger
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param type
     * @param injector
     * @param params
     *
     * @return
     */
    public boolean forceTrigger(Player pla, int type, ScriptVariableInjector injector, int... params) {
        Trigger t = getTrigger(type, params);

        if (t == null) {
            return false;
        }

        if (pla.isTriggerAlreadyRunning(t) || pla.isTriggerBanned(type, Trigger.hash2(params.length, params))) {
            return true;
        }

        Scope script = new Scope();

        script.setRunning(t);
        script.setController(pla);
        script.setTriggerHashcode(Trigger.hash2(params.length, params));

        if (injector != null) {
            injector.injectVariables(script);
        }

        for (int ind = 0; ind < params.length; ind++) {
            script.declareVar(ARGS[ind], new ScriptVar(ARGS[ind], params[ind]));
        }

        if (!runScript(script) || script.isDelayed()) {
            pla.getNewScriptQueue().add(script);
        }

        return true;
    }

    /**
     * Method evaluate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param script
     *
     */
    public static void checkForHang(Scope script) {
        if (detectedHangError != null) {
            ScriptHangException ee = detectedHangError;

            detectedHangError = null;

            throw new ScriptHangException("ERROR SCRIPT WAS HANGING! " + script.toString());
        }
    }

    /**
     * Method evaluate
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param script
     *
     * @return
     */
    public boolean evaluate(Scope script) {
        try {
            Scope orig = script;

            script = script.getCurrent();
            checkForHang(script);

            boolean conditionBased = script.isIfStatement();
            Player  player         = script.getController();

            while (script.hasNext()) {
                player = script.getController();

                RuntimeInstruction currentRuntimeInstruction = script.getNext();

                if (currentRuntimeInstruction == null) {
                    return !script.notifyParentNodes() || evaluate(orig);
                }

                try {
                    if (conditionBased) {
                        if (script.isConditionFlag()
                                && (currentRuntimeInstruction.getType() == RuntimeInstruction.FALSE)) {
                            continue;
                        } else if (!script.isConditionFlag()
                                   && (currentRuntimeInstruction.getType() == RuntimeInstruction.TRUE)) {
                            continue;
                        }
                    }

                    if (currentRuntimeInstruction.isCondition()) {
                        script.setIsConditionFlag(true);
                        conditionBased = true;
                        script.setConditionFlag(currentRuntimeInstruction.evaluateCondition(player, script));

                        if (script.isConditionFlag() && (currentRuntimeInstruction.getConditionalBlock() != null)) {
                            Scope trueBlock = new Scope();

                            trueBlock.setController(script.getController());
                            trueBlock.setInteractingNpc(script.getInteractingNpc(0));
                            trueBlock.setRunning(currentRuntimeInstruction.getConditionalBlock());
                            trueBlock.setParent(script);

                            if (!evaluate(trueBlock)) {
                                script.bind(trueBlock);

                                return false;
                            }
                        }

                        continue;
                    }

                    if (script.isTerminated()) {
                        return !script.notifyParentNodes() || evaluate(orig);
                    }

                    ScriptStatus status = executeInstruction(currentRuntimeInstruction, player, script);

                    if (player != null) {
                        switch (status) {
                        case WAIT :
                            return false;

                        case TERMINATE :
                            return !script.notifyParentNodes() || evaluate(orig);
                        }
                    }
                } catch (NumberFormatException ee) {
                    try {
                        script.debug(Text.RED("[Runtime Error]["
                                              + currentRuntimeInstruction.getPosition().getSimpleName()
                                              + "][NumberConversionError]" + ee.getMessage() + " at line "
                                              + currentRuntimeInstruction.getPosition().getRow()) + " "
                                                  + currentRuntimeInstruction.getName());
                    } catch (Exception jjj) {}

                    player.getActionSender().sendMessage("[" + Text.RED("ScriptError") + Text.BLACK("]+")
                            + " Check developer console for more details");

                    if (Config.enable_debugging) {
                        ee.printStackTrace();
                    }

                    logRuntimeError(ee, script, currentRuntimeInstruction);

                    return true;
                } catch (FunctionNotFoundException er) {
                    script.debug(Text.RED("[Runtime Error]Function not found: " + er.getMessage()));
                    script.terminateParentNodes();
                    script.notifyParentNodes();
                    script.terminate();
                    player.getActionSender().sendMessage("[" + Text.RED("ScriptError") + Text.BLACK("]+")
                            + " Check developer console for more details");
                    logRuntimeError(er, script, currentRuntimeInstruction);

                    return true;
                } catch (RuntimeException ee) {
                    if (Config.enable_debugging) {
                        ee.printStackTrace();
                    }

                    try {
                        if (currentRuntimeInstruction.getPosition() != null) {
                            Logger.debug(Text.RED("[Runtime Error]["
                                                  + currentRuntimeInstruction.getPosition().getSimpleName() + "]"
                                                  + ee.getMessage() + " at line "
                                                  + currentRuntimeInstruction.getPosition().getRow()));
                            script.debug(Text.RED("[Runtime Error][" + currentRuntimeInstruction.getName() + "()"
                                                  + "][" + currentRuntimeInstruction.getPosition().getSimpleName()
                                                  + "]" + ee.getMessage() + " at line "
                                                  + currentRuntimeInstruction.getPosition().getRow()));
                        } else {
                            script.debug("[RuntimeError]: " + ee.getMessage());
                            Logger.debug("RuntimeError " + currentRuntimeInstruction.toString());
                        }

                        logRuntimeError(ee, script, currentRuntimeInstruction);
                    } catch (Exception errrr) {}

                    script.terminateParentNodes();
                    script.notifyParentNodes();
                    script.terminate();

                    if (player != null) {
                        player.terminateScripts();
                        player.getActionSender().sendMessage("[" + Text.RED("ScriptError") + Text.BLACK("]+")
                                + " Check developer console for more details");
                    }

                    return true;
                } catch (Exception ee) {
                    if (Config.enable_debugging) {
                        ee.printStackTrace();
                    }

                    try {
                        script.debug(Text.RED("[Runtime Error]["
                                              + currentRuntimeInstruction.getPosition().getSimpleName() + "]"
                                              + ee.getMessage() + " at line "
                                              + currentRuntimeInstruction.getPosition().getRow()));
                    } catch (Exception eerrrrr) {}

                    script.terminateParentNodes();
                    script.notifyParentNodes();
                    script.terminate();
                    logRuntimeError(ee, script, currentRuntimeInstruction);

                    if (player != null) {
                        player.getActionSender().sendMessage("[" + Text.RED("ScriptError") + Text.BLACK("]+")
                                + " Check developer console for more details");
                    }

                    return true;
                }
            }

            if (script.notifyParentNodes()) {
                return evaluate(orig);
            }

            return true;
        } catch (StackOverflowError error) {
            if (script.getController() != null) {
                script.getController().getActionSender().sendMessage(
                    Text.RED("STACK OVERFLOW ERROR, THE SERVER SAVED YOU THIS TIME, GET IT FIXED!"));
            }

            System.err.println("WARNING STACK OVERFLOW ERROR");
            error.printStackTrace();
            script.terminate();
        }

        return true;
    }

    /**
     * Method executeInstruction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     * @param player
     * @param script
     *
     * @return
     */
    public static ScriptStatus executeInstruction(RuntimeInstruction c, Player player, Scope script) {
        if (script.isTerminated()) {
            return ScriptStatus.TERMINATE;
        }

        if (c.getExternalIdentifier() > 0) {
            player = (Player) script.getVar(c.getExternalIdentifier()).getData();
        }

        return c.eval(script, player);
    }
}
