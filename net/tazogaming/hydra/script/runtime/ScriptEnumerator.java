package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.runnable.Trigger;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/07/14
 * Time: 20:43
 */
public class ScriptEnumerator {
    private int[]  parameters;
    private String name;
    private int[]  arguments;

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param parameters
     * @param data
     */
    public ScriptEnumerator(String name, String[] parameters, String[] data) {
        this.name       = name;
        this.arguments  = new int[parameters.length];
        this.parameters = new int[data.length];

        for (int i = 0; i < parameters.length; i++) {
            this.arguments[i] = Integer.parseInt(parameters[i]);
        }

        for (int i = 0; i < this.parameters.length; i++) {
            this.parameters[i] = Integer.parseInt(data[i]);
        }
    }

    /**
     * Method args
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] args() {
        return arguments;
    }

    /**
     * Method firstKey
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int firstKey() {
        return arguments[0];
    }

    /**
     * Method hashcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int hashcode() {
        return Trigger.hashcode(arguments);
    }

    /**
     * Method getData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getData() {
        return parameters;
    }
}
