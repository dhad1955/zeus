package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Action;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/07/14
 * Time: 23:53
 */
public class ScriptProduceAction extends Action {
    private int         amount = 0;
    private int         delay  = 0;
    private int         itemId;
    private SignedBlock run;

    /**
     * Constructs ...
     *
     *
     * @param block_id
     * @param itemId
     * @param delay
     * @param player
     */
    public ScriptProduceAction(int block_id, int itemId, int delay, Player player) {
        super(0, player);
        this.itemId = itemId;
        this.delay  = delay;
        run         = World.getWorld().getScriptManager().getBlock(block_id);
    }

    @Override
    protected void tick(int time) {

        if(run == null || run.getSignature() == null)
        {
            getPlayer().getActionSender().sendMessage("Warning unable to complete action");
            terminate();
            return;

        }

        if ((time % delay == 0) || (time == 0)) {
            Scope script = new Scope(getPlayer(), run);

            script.declareVar(ScriptRuntime.ARGS[0], new ScriptVar(ScriptRuntime.ARGS[0], itemId));
            script.declareVar(run.getSignature()[0], new ScriptVar(run.getSignature()[0], itemId));
            getPlayer().getNewScriptQueue().add(script);
            amount++;
        }

        if (amount >= getActionAmount()) {
            terminate();

            return;
        }
    }

    /**
     * Method onTermination
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void onTermination() {}
}
