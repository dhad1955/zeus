package net.tazogaming.hydra.script.runtime;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/10/13
 * Time: 12:38
 */
public class FunctionNotFoundException extends RuntimeException {

    /**
     * Constructs ...
     *
     *
     * @param function
     */
    public FunctionNotFoundException(String function) {
        super("error: function not found: " + function);
    }
}
