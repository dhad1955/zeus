package net.tazogaming.hydra.script.runtime.data;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/08/14
 * Time: 12:52
 */
public interface ScriptVariableInjector {

    /**
     * Method injectVariables
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     */
    public void injectVariables(Scope scope);
}
