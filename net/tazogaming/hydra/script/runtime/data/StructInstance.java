package net.tazogaming.hydra.script.runtime.data;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.ScriptRuntime;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 22:41
 */
public class StructInstance  {

    /** values made: 14/08/30 **/
    private ScriptVar[] values;

    /** structID made: 14/08/30 **/
    private int structID;

    /**
     * Constructs ...
     *
     * @param id
     * @param values
     */
    public StructInstance(int id, Object... values) {
        this.structID = id;
        this.values   = new ScriptVar[values.length];

        for (int i = 0; i < values.length; i++) {
            this.values[i] = new ScriptVar(0, values[i]);
        }
    }




    /**
     * Method set
     * Created on 14/08/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     * @param value
     */
    public void set(int index, Object value) {
        values[index].set(value);
    }

    private ScriptVar get(int index) {
        return values[index];
    }

    /**
     * Method getVar
     * Created on 14/08/30
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param key
     *
     * @return
     */
    public ScriptVar getVar(long key) {
        return values[ScriptRuntime.structs[this.structID].indexOf(key)];
    }
}
