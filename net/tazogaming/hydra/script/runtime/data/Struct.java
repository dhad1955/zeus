package net.tazogaming.hydra.script.runtime.data;

//~--- JDK imports ------------------------------------------------------------

import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 22:34
 */
public class Struct {

    /** signature made: 14/08/24 **/
    private long[] signature;

    /**
     * Constructs ...
     *
     *
     * @param signature
     */
    public Struct(String... signature) {
        this.signature = new long[signature.length];
        for(int i = 0; i < this.signature.length; i++)
            this.signature[i] = Text.longForName(signature[i]);
    }

    /**
     * Method indexOf
     * Created on 14/08/24
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     *
     * @return
     */
    public int indexOf(long l) {
        for (int i = 0; i < signature.length; i++) {
            if (signature[i] == l) {
                return i;
            }
        }

        return -1;
    }
}
