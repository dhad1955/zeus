package net.tazogaming.hydra.script.runtime.data;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/08/14
 * Time: 10:30
 */
public class GlobalVariable {
    private ScriptVar var;
    private boolean   save;

    /**
     * Constructs ...
     *
     *
     * @param var
     * @param save
     */
    public GlobalVariable(ScriptVar var, boolean save) {
        this.var  = var;
        this.save = save;
    }

    /**
     * Method getVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptVar getVar() {
        return var;
    }

    /**
     * Method setVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param var
     */
    public void setVar(ScriptVar var) {
        this.var = var;
    }

    /**
     * Method isSave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isSave() {
        return save;
    }

    /**
     * Method setSave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param save
     */
    public void setSave(boolean save) {
        this.save = save;
    }
}
