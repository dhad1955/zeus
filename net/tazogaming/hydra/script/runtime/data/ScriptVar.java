package net.tazogaming.hydra.script.runtime.data;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 17:28
 */
public class ScriptVar {

    /*
     *  The numeric data on this variable
     */
    private int numericData;

    /*
     * The 64bit name hash
     */
    private long nameHash;

    /*
     * ASCII (String data) of this variable
     */
    private String asciiData;

    /*
     * The object data of this variable, can be anything
     */
    private Object objectData;

    /*
     * Construct a new script variable with an integer value of 0
     */

    private boolean isNull = false;

    /**
     * Constructs ...
     *
     *
     * @param hash
     */
    public ScriptVar(long hash) {
        this(hash, 0);
    }

    /**
     * Constructs ...
     *
     *
     * @param hash
     * @param numericData
     */
    public ScriptVar(long hash, int numericData) {
        this.nameHash    = hash;
        this.numericData = numericData;
    }

    /**
     * Constructs ...
     *
     *
     * @param hash
     * @param numericData
     */
    public ScriptVar(long hash, Object numericData) {
        this.nameHash = hash;
       if(numericData == null){
           this.isNull = true;
           return;
       }

        if (numericData instanceof Integer) {
            this.numericData = (Integer) numericData;
        } else if (numericData instanceof String) {
            this.asciiData = (String) numericData;
        } else {
            this.objectData = numericData;
        }
    }

    private void checkInt() {
        if (asciiData != null) {
            throw new RuntimeException("Error tried to modify a string");
        }
    }

    /**
     * Method set
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param numericData
     */
    public void set(Object numericData) {
        if(numericData != null) {
            this.isNull = false;

        }else{
            this.isNull = true;

        }
        if (numericData instanceof Integer) {
            this.numericData = (Integer) numericData;
        } else if (numericData instanceof String) {
            this.asciiData = (String) numericData;
        } else {
            this.objectData = numericData;
        }
    }

    /**
     * Method getData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Object getData() {
        if(this.isNull)
            return null;

        if(this.asciiData != null)
            return this.asciiData;

        if (objectData != null) {
            return objectData;
        }

        return numericData;
    }

    /**
     * Method divide
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param by
     */
    public void divide(int by) {
        checkInt();
        this.numericData /= by;
    }

    /**
     * Method subtract
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param by
     */
    public void subtract(int by) {
        checkInt();
        this.numericData -= by;
    }

    /**
     * Method multiply
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param by
     */
    public void multiply(int by) {
        checkInt();
        this.numericData *= by;
    }

    /**
     * Method MD5
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getHash() {
        return this.nameHash;
    }

    /**
     * Method add
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param amt
     */
    public void add(int amt) {
        checkInt();
        this.numericData += amt;
    }
}
