package net.tazogaming.hydra.script.runtime.instr.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.build.ScriptCompiler;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/08/14
 * Time: 01:08
 */
public class BlockEvaluation implements ReturnFunction {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param scope
     * @param tokenizer
     *
     * @return
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
        SignedBlock block;
        int         blockID = tokenizer.nextInt();

        if (blockID >= ScriptCompiler.BLOCK_DELIMITER) {
            block = scope.getRunning().getContext().getBlock(blockID - ScriptCompiler.BLOCK_DELIMITER);
        } else {
            block = World.getWorld().getScriptManager().getBlock(blockID);
        }

        if (block == null) {
            throw new RuntimeException("Block is null!!");
        }

        Scope scope1 = new Scope();

        scope1.setController(scope.getController());
        scope1.setRunning(block);

        if (block.getSignature() != null) {
            for (int i = 0; i < block.getSignature().length; i++) {
                scope1.declare(block.getSignature()[i], tokenizer.next());
            }
        }

        if (!World.getWorld().getScriptManager().evaluate(scope1)) {
            throw new RuntimeException("Error in evaluating block! pausing is not allowed!");
        }

        return scope1.getReturnValue();
    }
}
