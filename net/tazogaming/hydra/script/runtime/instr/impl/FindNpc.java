package net.tazogaming.hydra.script.runtime.instr.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

//~--- JDK imports ------------------------------------------------------------


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/08/14
 * Time: 18:35
 */
public class FindNpc implements ReturnFunction {
    public static final int
        IGNORE_BUSY   = 1,
        MUST_FACE     = 2,
        NOT_IN_COMBAT = 4,
        CAN_REACH_ME  = 8;

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param scope
     * @param params
     *
     * @return
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser params) {
        int search_flags = 0;
        int npcID        = params.nextInt();
        int range        = params.nextInt();

        while (params.hasNext()) {
            search_flags |= params.nextInt();
        }

        for (NPC npc : player.getWatchedNPCs().getKnownEntities()) {
            if (npc.getCurrentInstance() != player.getCurrentInstance()) {
                continue;
            }

            if (!npc.isVisible()) {
                continue;
            }

            if (npc.isDead()) {
                continue;
            }

            if ((npcID != -1) && (npc.getId() != npcID)) {
                continue;
            }

            if (!npc.isWithinDistance(player, range)) {
                continue;
            }

            if ((search_flags & NOT_IN_COMBAT) != 0 && npc.getCombatHandler().isInCombat()) {
                continue;
            }

            if (npc.isBusy() && (search_flags & IGNORE_BUSY) == 0) {
                continue;
            }

            if ((search_flags & CAN_REACH_ME) != 0) {
                if (npc.getSpawnData() == null) {
                    continue;
                }

                int x = npc.getSpawnData().getSpawnX();
                int y = npc.getSpawnData().getSpawnY();

                if ((Point.getDistance(npc.getLocation(), player.getLocation()) > x + npc.getSize() / 2)
                        || (Point.getDistance(player.getLocation(), npc.getLocation())) > y + npc.getSize() / 2) {
                    continue;
                }
            }

            return npc;
        }

        return null;
    }
}
