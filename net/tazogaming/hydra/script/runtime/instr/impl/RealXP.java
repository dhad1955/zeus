package net.tazogaming.hydra.script.runtime.instr.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;
import net.tazogaming.hydra.util.Config;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 01/11/2014
 * Time: 08:49
 */
public class RealXP implements ReturnFunction {
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param player
     * @param scope
     * @param tokenizer
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
            int skillId = tokenizer.nextInt();
            int experience = tokenizer.nextInt();

        if (Config.doubleXp) {
            experience *= 2;
        }


        if (player.getAccount().getFlag(Account.DOUBLE_XP)) {
            experience *= 2;
        }

        if (player.getAccount().getFlag(Account.DOUBLE_XP_2)) {
            experience *= 2;
        }



        if (player.getRights() == Player.DONATOR) {
            experience *= 1.05;
        }

        if (player.getRights() == Player.PLATINUM) {
            experience *= 1.10;
        }

        if ((player.getRights() == Player.PATRON) || (player.getRights() == Player.EXTREME_DONATOR)) {
            experience *= 1.15;
        }

        if (player.getTimers().timerActive(TimingUtility.DOUBLE_XP_TIMER)) {
            experience *= 2;
        }

        if (player.getTimers().timerActive(TimingUtility.DOUBLE_XP_TIMER_2)) {
            experience *= 2;
        }

        if (player.getEquipment().getId(Equipment.CAPE) == 15117) {
            experience *= 1.3;
        }

        if (player.getEquipment().getId(Equipment.HAT) == 15118) {
            experience *= 1.2;
        }

        experience *= Config.XP_RATE;

        experience *= Config.XP_MODIFIERS[skillId];
        return experience;
    }
}
