package net.tazogaming.hydra.script.runtime.instr.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 08/08/14
 * Time: 00:18
 */
public class GetCurStat implements ReturnFunction {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param scope
     * @param tokenizer
     *
     * @return
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
        int stat = tokenizer.nextInt();
        if(stat == 3)
            return (int)player.getCurrentHealth();
        else if(stat == 5)
            return player.getPrayerBook().getPoints();
        else if(stat == 23)
            return player.getCurrSummonPts();
        return player.getCurStat(stat);
    }
}
