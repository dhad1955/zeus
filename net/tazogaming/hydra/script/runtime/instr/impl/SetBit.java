package net.tazogaming.hydra.script.runtime.instr.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Created by Dan on 30/12/2015.
 */
public class SetBit implements ReturnFunction {

    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
            int value = (int)tokenizer.nextInt();
            return value | 1 << tokenizer.nextInt();
    }
}
