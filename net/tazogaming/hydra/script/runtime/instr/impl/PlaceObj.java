package net.tazogaming.hydra.script.runtime.instr.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.object.GameObject;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/08/14
 * Time: 15:36
 */
public class PlaceObj implements ReturnFunction {
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param player
     * @param scope
     * @param
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser params) {
        int id = params.nextInt();
        int x = params.nextInt();
        int y = params.nextInt();
        int h = params.nextInt();
        int type = params.nextInt();
        int rotation = params.nextInt();
        GameObject object = new GameObject();
        object.changeIndex(id);
        object.changeRotation(rotation);
        object.setObjectType(type);
        object.setIdentifier(id);
        if(params.hasNext()) {
            object.scheduleRemove(params.nextInt());
        }
        object.setLocation(Point.location(x,y,h));
        World.getWorld().getObjectManager().registerObject(object);
        return object;
    }
}
