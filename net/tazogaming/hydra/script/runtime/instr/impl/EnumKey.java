package net.tazogaming.hydra.script.runtime.instr.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.ScriptEnum;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/08/14
 * Time: 19:18
 */
public class EnumKey implements ReturnFunction {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param scope
     * @param tokenizer
     *
     * @return
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
        int        enumID   = tokenizer.nextInt();
        int        index    = tokenizer.nextInt();
        ScriptEnum enumData = World.getWorld().getScriptManager().getEnum(enumID);

        if (enumData == null) {
            throw new RuntimeException("Error enum does not exist");
        }

        if (index > enumData.size()) {
            throw new RuntimeException("Error index " + index + " is out of bounds for enum");
        }

        return enumData.getKey(index);
    }
}
