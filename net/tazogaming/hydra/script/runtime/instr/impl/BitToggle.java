package net.tazogaming.hydra.script.runtime.instr.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Created by Dan on 30/12/2015.
 */
public class BitToggle implements ReturnFunction {
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {

        int num = tokenizer.nextInt();
        int bit = tokenizer.nextInt();

        boolean bit_test = (num >> bit) % 2 != 0;

        if(bit_test) {
            return num &  ~( 1<< bit);
        } else {
            return num | 1 << bit;
        }

    }
}
