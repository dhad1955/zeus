package net.tazogaming.hydra.script.runtime.instr.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/09/14
 * Time: 17:39
 */
public class CreateQuestNpc implements ReturnFunction {

    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param player
     * @param scope
     * @param tokenizer
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser params) {
        NpcAutoSpawn spwn = new NpcAutoSpawn();

        spwn.setNpcId(params.nextInt());
        spwn.setSpawnX(params.nextInt());
        spwn.setSpawnY(params.nextInt());
        spwn.setRoamRange(params.nextInt());
        spwn.setSpawnHeight(player.getHeight());

        NPC q_npc = new NPC(spwn.getNpcId(), Point.location(spwn.getSpawnX(), spwn.getSpawnY(), player.getHeight()),
                spwn);

        World.getWorld().registerNPC(q_npc);
        q_npc.setQuestController(player);
        q_npc.setCurrentInstance(player.getCurrentInstance());
        player.validatedWatchedNpcs();
        player.updateWatchedNpcs();
        q_npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                killedBy.getPlayer().getLinked_npcs().remove((NPC) killed);

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });

        if (player.getCurrentInstance() != null) {
            player.getCurrentInstance().add(q_npc);
        }

        if (params.hasNext()) {
            player.getActionSender().markNpc(q_npc.getIndex());
        }

        player.add_linked_npc(q_npc, params.hasNext());

        return q_npc;
    }
}
