package net.tazogaming.hydra.script.runtime.instr.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/10/14
 * Time: 20:05
 */
public class Millis implements ReturnFunction {

    public static long lastCall = System.currentTimeMillis();
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param player
     * @param scope
     * @param tokenizer
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
        long l  = System.currentTimeMillis() - lastCall;

        lastCall = System.currentTimeMillis();
                return l;
    }
}
