package net.tazogaming.hydra.script.runtime.instr.op.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.ui.PrayerBook;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.game.skill.combat.impact.MagicProjectileImpactEvent;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 16:44
 */
public class SpellEffect implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        int      calc               = 0;
        int      movingGraphic      = params.nextInt();
        int      wait_speed         = params.nextInt();
        int      move_Speed         = params.nextInt();
        int      end_graphic        = params.nextInt();
        int      end_graphic_height = params.nextInt();
        int      teleblock_time     = params.nextInt();
        int      freeze_time        = params.nextInt();
        int      stat_drain_id      = params.nextInt();
        int      stat_drain_percent = params.nextInt();
        Killable target             = (Killable) scope.getAttachement();


        target.registerHit(player);

        if (player.isTenth(6)) {
            freeze_time *= 2;
        }

        if (target == null) {
            return ScriptStatus.TERMINATE;
        }

        double impact = target.preCalcDamage(player, 100, Damage.TYPE_MAGIC);

        if (impact > 1) {
            impact = 1;
        }

        if (impact > 0) {
            if (freeze_time > 0) {
                target.freeze(freeze_time);
            }

            if (teleblock_time > 0 && !target.getTimers().timerActive(TimingUtility.TELE_BLOCK_TIMER)) {
                if(target.getPlayer().getPrayerBook().prayerActive(PrayerBook.PROTECT_FROM_MAGIC) || target.getPlayer().getPrayerBook().curseActive(PrayerBook.DEFLECT_MAGIC))
                    teleblock_time /= 2;

                target.getTimers().setTime(TimingUtility.TELE_BLOCK_TIMER, teleblock_time);

                if ((target instanceof Player)) {
                    target.getPlayer().getActionSender().sendMessage("You have been tele-blocked");
                }
            }

            if (stat_drain_id > 0) {
                if (target instanceof Player) {
                    Player target_player = target.getPlayer();
                    int    curStat       = target_player.getCurStat(stat_drain_id);
                    calc = (int) (player.getMaxStat(stat_drain_id)
                                  * (double) (1 - (double) ((double) stat_drain_percent / 100d)));
                    if (curStat > calc) {
                        target_player.setCurStat(stat_drain_id, calc);

                        if (target_player.getCurStat(stat_drain_id) < 0) {
                            target_player.setCurStat(stat_drain_id, 0);
                        }

                        target_player.getActionSender().sendStat(stat_drain_id);
                    }
                }
            }
        }

        Projectile projectile_Spell = new Projectile(player, target, movingGraphic,
                                          Projectile.getWaitspeedForTick(wait_speed), move_Speed);

        projectile_Spell.setOnImpactEvent(new MagicProjectileImpactEvent(-impact, end_graphic, end_graphic_height));
        player.registerProjectile(projectile_Spell);

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {
            "movingGraphic", "wait_speed", "move_speed", "end_graphic", "teleblock_time", "freeze_time",
            "stat_drain_id", "stat_drain_amount"
        };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "movingGraphic = the moving projectile gfx id\n"
               + "wait_speed = the wait time before casting the projectile\n"
               + "move_speed = the moving speed of the projectil\n"
               + "end_graphic = the end graphic id of the impact graphic\n"
               + "teleblock_time = the amount of time to teleblock the target\n"
               + "freeze_time = the amount of time to freeze the target\n" + "stat_drain_id = the skillid to drain\n"
               + "stat_drain_amount = the percentage of the skill to drain\n";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "spelleffect|spell_effect";
    }
}
