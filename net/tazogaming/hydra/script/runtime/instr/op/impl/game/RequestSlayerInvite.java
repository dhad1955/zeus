package net.tazogaming.hydra.script.runtime.instr.op.impl.game;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/12/2014
 * Time: 18:24
 */
public class RequestSlayerInvite implements ScriptInstructionContext {
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params, Scope scope) {
            String playerName = params.nextString();
            Player requested = World.getWorld().getPlayer(Text.longForName(playerName));
            if(requested == null){
                player.getActionSender().sendMessage("Player doesnt exist");
                return ScriptStatus.NEXT_STATEMENT;
            }

            if(requested.getUID() == player.getUID())
                return ScriptStatus.NEXT_STATEMENT;

            if(player.getSlayerTask().isDuoTask()){
                player.getActionSender().sendMessage("You already have a partner");
                return ScriptStatus.NEXT_STATEMENT;
            }

            if(player.getSlayerTask().getTotalKills() > 0){
                player.getActionSender().sendMessage("You have already started your task");
                return ScriptStatus.NEXT_STATEMENT;
            }
            if(!player.getSlayerTask().isLeader(player)){
                player.getActionSender().sendMessage("You are not the leader of this task");
                return ScriptStatus.NEXT_STATEMENT;
            }


            player.getActionSender().sendMessage("Requesting slayer join");
            requested.setSlayerJoinRequest(player);
            return ScriptStatus.NEXT_STATEMENT;
        }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {"playername"};
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
        return "cba explaining";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "request_slayer_invite";
    }
}
