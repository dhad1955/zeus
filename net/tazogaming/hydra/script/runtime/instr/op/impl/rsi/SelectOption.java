package net.tazogaming.hydra.script.runtime.instr.op.impl.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.game.ui.Dialog;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:28
 */
public class SelectOption implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        player.getGameFrame().showDialog(new Dialog(Dialog.TYPE_SELECT_OPTION_1, -1, 0, params.nextString(), script,
                params.remainingStrings()));
        script.waitFor(Scope.WAIT_TYPE_DIALOG);

        return ScriptStatus.WAIT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "title", "Lines..." };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Opens a selectoption dialog, the player will now choose between 2-5 different options specified by the programmer, \n "
               + "First param is the title of the dialogue usually 'Choose option' \n there now must be atleast another 2 parameters added with a maximum of 5 to specify each option \n"
               + " selectoption(\"Choose option\", \"Option 1\", \"Option 2\");\n"
               + "selectoption(\"Choose option\", \"Option 1\", \"Option 2\", \"Option 3\");\n"
               + "This will also pause the script until an option is chosen, if the player backs out or teleports the script will terminate.\n"
               + "The option can then be tested by using the if(option(optionid)) function or a variable called $opt will be declared which can be used in switch statments,\n"
               + "or normal logical statements, or whatever needed.";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "selectoption|chooseoption";
    }
}
