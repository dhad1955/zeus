package net.tazogaming.hydra.script.runtime.instr.op.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.boot.BootTask;
import net.tazogaming.hydra.script.runtime.instr.ReturnOperationMap;
import net.tazogaming.hydra.script.runtime.instr.SetRenderAnim;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.impl.controlflow.*;
import net.tazogaming.hydra.script.runtime.instr.op.impl.controlflow.ListRemove;
import net.tazogaming.hydra.script.runtime.instr.op.impl.game.*;
import net.tazogaming.hydra.script.runtime.instr.op.impl.inv.*;
import net.tazogaming.hydra.script.runtime.instr.op.impl.minigame.FightPits;
import net.tazogaming.hydra.script.runtime.instr.op.impl.minigame.ZombieEnter;
import net.tazogaming.hydra.script.runtime.instr.op.impl.player.*;
import net.tazogaming.hydra.script.runtime.instr.op.impl.rsi.*;
import net.tazogaming.hydra.script.runtime.instr.op.impl.skill.*;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 17:15
 */
public class RuntimeOperationMap {

    /** operations made: 14/08/19 **/
    private static ScriptInstructionContext[] operations = new ScriptInstructionContext[200];

    /** functionMap made: 14/08/19 **/
    private static Map<String, Integer> functionMap = new HashMap<String, Integer>();
    public static final int
        SCRIPT_COMMANDS                             = 0,
        GAME                                        = 1,
        INV                                         = 3,
        PLAYER                                      = 2,
        RSI                                         = 4,
        SKILL                                       = 5,
        MINIGAME                                    = 6,
        NPC                                         = 7;

    /** opcode made: 14/08/19 **/
    private static int opcode = 0;

    /** internalOps made: 14/08/19 **/
    private static ArrayList<ScriptInstructionContext>[] internalOps = new ArrayList[8];

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param task
     */
    public static void load(BootTask task) {
        task.log("Loading Runtime Operations");
        loadOps();
        task.log("Loaded " + opcode + " operations");
        task.log("Loading header file information for docs");

        try {
            String fileData = Text.loadTextFromFileAsString("config/runedoc/doc_header.html");
            String dump     = dumpCalls();

            dump = dumpEvaluationMap() + dump;

            String         out    = fileData.replace("{$DATA}", dump);
            BufferedWriter writer = new BufferedWriter(new FileWriter("config/runedoc/docs.html"));

            writer.write(out);
            writer.close();
            task.log("Fully loaded operation list and dumped docs");
        } catch (Exception ee) {
            ee.printStackTrace();
            Logger.debug("Runtime operations failed, failed to load docs: " + ee.getMessage());
            System.exit(-1);
        }
    }

    private static void loadOps() {
        OPCODE(AllowAction.class, SCRIPT_COMMANDS);
        OPCODE(BanScript.class, SCRIPT_COMMANDS);
        OPCODE(Declare.class, SCRIPT_COMMANDS);
        OPCODE(Die.class, SCRIPT_COMMANDS);
        OPCODE(EnterInstance.class, SCRIPT_COMMANDS);
        OPCODE(GoSub.class, SCRIPT_COMMANDS);
        OPCODE(ForLoopOperation.class, SCRIPT_COMMANDS);
        OPCODE(Goto.class, SCRIPT_COMMANDS);
        OPCODE(JSFork.class, SCRIPT_COMMANDS);
        OPCODE(JSJump.class, SCRIPT_COMMANDS);
        OPCODE(Jump.class, SCRIPT_COMMANDS);
        OPCODE(ModPlayerVar.class, SCRIPT_COMMANDS);
        OPCODE(ModVar.class, SCRIPT_COMMANDS);
        OPCODE(Pause.class, SCRIPT_COMMANDS);
        OPCODE(RemovePlayerVar.class, SCRIPT_COMMANDS);
        OPCODE(Return.class, SCRIPT_COMMANDS);
        OPCODE(Set.class, SCRIPT_COMMANDS);
        OPCODE(SetLock.class, SCRIPT_COMMANDS);
        OPCODE(Switch.class, SCRIPT_COMMANDS);
        OPCODE(UnloadInstance.class, SCRIPT_COMMANDS);
        OPCODE(DeclarePlayerVar.class, SCRIPT_COMMANDS);
        OPCODE(VarDump.class, SCRIPT_COMMANDS);
        OPCODE(ListAdd.class, SCRIPT_COMMANDS);
        OPCODE(ListRemove.class, SCRIPT_COMMANDS);
        OPCODE(ListRemoveInd.class, SCRIPT_COMMANDS);
        OPCODE(ListClear.class, SCRIPT_COMMANDS);
        OPCODE(Sched.class, SCRIPT_COMMANDS);
        OPCODE(LoopBreak.class, SCRIPT_COMMANDS);
        OPCODE(Continue.class, SCRIPT_COMMANDS);
        OPCODE(ForEachOP.class, SCRIPT_COMMANDS);
        OPCODE(Use.class, SCRIPT_COMMANDS);
        OPCODE(TaskKill.class, SCRIPT_COMMANDS);
        OPCODE(ListSet.class, SCRIPT_COMMANDS);
        OPCODE(Fork.class, SCRIPT_COMMANDS);
        OPCODE(ChangePasswordRequest.class, GAME);

        // Game functions
        OPCODE(AnimateObject.class, GAME);
        OPCODE(CreateQuestNPC.class, GAME);
        OPCODE(PlaceGroundItem.class, GAME);
        OPCODE(PlaceObj.class, GAME);
        OPCODE(SetBConfig.class, GAME);
        OPCODE(SetConfig.class, GAME);
        OPCODE(SetVarbit.class, GAME);
        OPCODE(ObjDel.class, GAME);
        OPCODE(Sound.class, GAME);
        OPCODE(ObjChange.class, GAME);
        OPCODE(SpotGraphic.class, GAME);
        OPCODE(Projectile.class, GAME);
        OPCODE(HintIconOP.class, GAME);
        OPCODE(GlobalMessage.class, GAME);

        // Inventory functions
        OPCODE(AddItem.class, INV);
        OPCODE(DeleteItem.class, INV);
        OPCODE(InvDel.class, INV);
        OPCODE(InvReplace.class, INV);
        OPCODE(RemoveItem.class, INV);
        OPCODE(FightPits.class, MINIGAME);
        OPCODE(ZombieEnter.class, MINIGAME);
        OPCODE(AddPoints.class, PLAYER);
        OPCODE(AddXP.class, PLAYER);
        OPCODE(AdvanceClue.class, PLAYER);
        OPCODE(Animation.class, PLAYER);
        OPCODE(ClosePath.class, PLAYER);
        OPCODE(DecreaseStat.class, PLAYER);
        OPCODE(DisableAction.class, PLAYER);
        OPCODE(FaceTo.class, PLAYER);
        OPCODE(ForceWield.class, PLAYER);
        OPCODE(Gfx.class, PLAYER);
        OPCODE(HealHP.class, PLAYER);
        OPCODE(Hit.class, PLAYER);
        OPCODE(IncreaseRun.class, PLAYER);
        OPCODE(IncreaseStat.class, PLAYER);
        OPCODE(AddFoodProt.class, PLAYER);
        OPCODE(Loot.class, PLAYER);
        OPCODE(ResetCombat.class, PLAYER);
        OPCODE(ChangeUsernameRequest.class, PLAYER);
        OPCODE(ResetSkill.class, PLAYER);
        OPCODE(Say.class, PLAYER);
        OPCODE(SetDeathHandler.class, PLAYER);
        OPCODE(SetPlayerLock.class, PLAYER);
        OPCODE(SetTimer.class, PLAYER);
        OPCODE(Teleport.class, PLAYER);
        OPCODE(TerminateActions.class, PLAYER);
        OPCODE(Unpoison.class, PLAYER);
        OPCODE(WalkTo.class, PLAYER);
        OPCODE(LinkLock.class, PLAYER);
        OPCODE(Lock.class, PLAYER);
        OPCODE(SetIPFlag.class, PLAYER);
        OPCODE(SummonNPC.class, PLAYER);
        OPCODE(ClientCMD.class, PLAYER);

        // Interface functions
        OPCODE(CloseWindow.class, RSI);
        OPCODE(ConstInterfaceSet.class, RSI);
        OPCODE(Debug.class, RSI);
        OPCODE(DialogInfo.class, RSI);
        OPCODE(GetAmount.class, RSI);
        OPCODE(GetString.class, RSI);
        OPCODE(Interface.class, RSI);
        OPCODE(InterfaceVisible.class, RSI);
        OPCODE(Mes.class, RSI);
        OPCODE(NpcChat.class, RSI);
        OPCODE(OpenShop.class, RSI);
        OPCODE(Overlay.class, RSI);
        OPCODE(PlayerChat.class, RSI);
        OPCODE(SDAdditem.class, RSI);
        OPCODE(SDPack.class, RSI);
        OPCODE(SelectOption.class, RSI);
        OPCODE(SetAMask.class, RSI);
        OPCODE(SetItem.class, RSI);
        OPCODE(SetString.class, RSI);
        OPCODE(SetTab.class, RSI);
        OPCODE(SkillDialogBegin.class, RSI);

        // Skill functions
        OPCODE(AgilityJump.class, SKILL);
        OPCODE(AgilityWalk.class, SKILL);
        OPCODE(ConstructObject.class, SKILL);
        OPCODE(CreateParty.class, SKILL);
        OPCODE(DungNextFloor.class, SKILL);
        OPCODE(DungPrevFloor.class, SKILL);
        OPCODE(EnterParty.class, SKILL);
        OPCODE(StartFloor.class, SKILL);
        OPCODE(StartFloorParty.class, SKILL);
        OPCODE(Spell.class, SKILL);
        OPCODE(SpellEffect.class, SKILL);
        OPCODE(NpcUnlink.class, NPC);
        OPCODE(NpcAnim.class, NPC);
        OPCODE(NpcAttack.class, NPC);
        OPCODE(NpcSay.class, NPC);
        OPCODE(NpcMove.class, NPC);
        OPCODE(NpcGfx.class, NPC);
        OPCODE(NPCFace.class, NPC);
        OPCODE(NpcLock.class, NPC);
        OPCODE(RemovePoints.class, PLAYER);
        OPCODE(SetRenderAnim.class, PLAYER);
        OPCODE(MarkQuestBit.class, PLAYER);
        OPCODE(RequestSlayerInvite.class, PLAYER);
        OPCODE(AchievementProgress.class, PLAYER);
        OPCODE(BarrageSpell.class, PLAYER);
        OPCODE(DelayActions.class, PLAYER);
    }

    /**
     * Method catName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public static String catName(int id) {
        switch (id) {
        case INV :
            return "Inventory based functions";

        case RSI :
            return "Interface specific functions";

        case PLAYER :
            return "Player functions";

        case GAME :
            return "Misc functions";

        case MINIGAME :
            return "Minigame enter functions";

        case SKILL :
            return "Skill specific functions";
        }

        return "unknown";
    }

    private static void cat_row(String info, StringBuffer buffer) throws IOException {
        buffer.append("<tr><td class='tcat'>" + info + "</td></tr>");
        buffer.append("\n");
    }

    private static void alt1(String info, StringBuffer buffer) throws IOException {
        buffer.append("<td class='alt1'>" + info + "</td>");
        buffer.append("\n");
    }

    private static void alt2(String info, StringBuffer buffer) throws IOException {
        buffer.append("<td class='alt2'>" + info + "</td>");
        buffer.append("\n");
    }

    private static String buildMethodAndSignature(String meth, String[] sig) {
        if (sig.length == 0) {
            return "" + meth + "()";
        }

        StringBuffer buffer = new StringBuffer();

        buffer.append("" + meth + "(");

        for (int i = 0; i < sig.length; i++) {
            buffer.append("<font color='#008A00'>" + sig[i] + "</font>");

            if (i == sig.length - 1) {
                buffer.append(")");
            } else {
                buffer.append(", ");
            }
        }

        return buffer.toString();
    }

    /**
     * Method buildSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param meth
     * @param sig
     *
     * @return
     */
    public static String buildSignature(String meth, String[] sig) {
        StringBuffer br = new StringBuffer();

        if (meth == null) {
            throw new RuntimeException();
        }

        for (String str : meth.split("\\|")) {
            br.append(buildMethodAndSignature(str, sig));
            br.append("<br>");
        }

        return br.toString();
    }

    /**
     * Method dumpEvaluationMap
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public static String dumpEvaluationMap() throws IOException {
        Map<String, Integer> evaluations = ReturnOperationMap.getNameMap();
        StringBuffer         writer      = new StringBuffer(12002);

        cat_row("CATEGORY: VALUES THAT RETURN DATA", writer);

        for (String key : evaluations.keySet()) {
            int opcode = evaluations.get(key);

            writer.append("<tr>");
            writer.append("\n");
            alt1("Name: " + key, writer);
            alt2(ReturnOperationMap.getInformation()[opcode], writer);
            writer.append("</tr>");
            writer.append("\n");
        }

        return writer.toString();
    }

    /**
     * Method dumpCalls
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public static String dumpCalls() throws IOException {
        try {
            StringBuffer writer = new StringBuffer(10000);

            for (int i = 0; i < 8; i++) {
                ArrayList arrayList = internalOps[i];

                if (arrayList == null) {
                    continue;
                }

                cat_row("Category: " + catName(i), writer);

                for (ScriptInstructionContext op : internalOps[i]) {
                    if ((op == null) || (op.getUsage() == null)) {
                        continue;
                    }

                    writer.append("<tr>");
                    writer.append("\n");

                    try {
                        alt1(buildSignature(op.getMethodName(), op.getMethodSignature()), writer);
                        alt2(Text.nl2br(op.getUsage()), writer);
                        writer.append("</tr>");
                        writer.append("\n");
                    } catch (Exception ee) {
                        Logger.debug("error: " + op.getClass().getName());
                        System.exit(-1);
                    }
                }
            }

            return writer.toString();
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        throw new RuntimeException();
    }

    /**
     * Method getByName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public static ScriptInstructionContext getByName(String name) {
        try {
            return operations[functionMap.get(name.toLowerCase())];
        } catch (Exception ee) {
            return null;
        }
    }

    /**
     * Method OPCODE
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     * @param cat
     */
    public static void OPCODE(Class str, int cat) {
        try {
            if (internalOps == null) {
                internalOps = new ArrayList[20];
            }

            operations[opcode] = (ScriptInstructionContext) str.newInstance();

            if (internalOps[cat] == null) {
                internalOps[cat] = new ArrayList<ScriptInstructionContext>();
            }

            for (String split : operations[opcode].getMethodName().split("\\|")) {
                functionMap.put(split.toLowerCase(), opcode);
            }

            internalOps[cat].add(operations[opcode++]);
        } catch (InstantiationException error) {
            Logger.err("Error loading opcode: " + opcode);
        } catch (IllegalAccessException ignored) {}
        catch (NullPointerException error) {
            throw new RuntimeException("error loading: " + str.getSimpleName());
        }
    }
}
