package net.tazogaming.hydra.script.runtime.instr.op.impl.controlflow;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.PlayerVariable;
import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 17:59
 */
public class ModPlayerVar implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        String varName = params.nextString();
        int    token   = params.nextByte();
        Object value   = params.next();

        switch (token) {
        case Token.TYPE_AND :
            player.getAccount().setObject(varName, value, true);

            break;

        case Token.TYPE_ASSIGNMENT :
            player.getAccount().setObject(varName, value, false);

            break;



        case Token.TYPE_DIVIDE_OPERATOR :
        case Token.TYPE_MULTIPLY :
        case Token.TYPE_MINUS :
        case Token.TYPE_MODULUS_OPERATOR :
        case Token.TYPE_ADD :
            if (!player.getAccount().hasVar(varName)) {
                throw new RuntimeException("player variable does not exist: " + varName);
            }

            PlayerVariable var = player.getAccount().getVar(varName);

            if (!(var.getFullData() instanceof Integer)) {
                throw new RuntimeException("Error cant use a numeric operator on playervar " + varName
                                           + " this is not an integer it is a "
                                           + var.getFullData().getClass().getSimpleName());
            }

            int varData = var.getIntData();

            if (!(value instanceof Integer)) {
                throw new RuntimeException("Error trying to use a numeric operator on " + varName
                                           + " by setting it by a " + value.getClass().getSimpleName()
                                           + " must be set by an integer");
            }

            int intValue = (Integer) value;

            switch (token) {
            case Token.TYPE_DIVIDE_OPERATOR :
                varData /= intValue;
                var.setIntData(varData);

                break;

            case Token.TYPE_MULTIPLY :
                varData *= intValue;
                var.setIntData(varData);

                break;

            case Token.TYPE_MODULUS_OPERATOR :
                varData %= intValue;
                var.setIntData(varData);

                break;

            case Token.TYPE_MINUS :
                varData -= intValue;
                var.setIntData(varData);

                break;

            case Token.TYPE_ADD :
                varData += intValue;
                var.setIntData(varData);

                break;
            }

        }


        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[0];
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return null;
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "mod_pvar";
    }
}
