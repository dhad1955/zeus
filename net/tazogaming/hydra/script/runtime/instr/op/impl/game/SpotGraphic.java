package net.tazogaming.hydra.script.runtime.instr.op.impl.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/08/14
 * Time: 06:55
 */
public class SpotGraphic implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        int gfxId = params.nextInt();
        int x     = params.nextInt();
        int y     = params.nextInt();
        int h     = player.getHeight();
        int var1  = 0,
            var2  = 0,
            var3  = 0;

        if (params.hasNext()) {
            var1 = params.nextInt();
        }

        if (params.hasNext()) {
            var2 = params.nextInt();
        }

        if (params.hasNext()) {
            var3 = params.nextInt();
        }

        Point loc = Point.location(x, y, h);

        player.getActionSender().sendPositionedGraphic(loc, gfxId, var1, var2, var3);

        for (Player plr : player.getWatchedPlayers().getAllEntities()) {
            if (plr.getCurrentInstance() == player.getCurrentInstance()) {
                plr.getActionSender().sendPositionedGraphic(loc, gfxId, var1, var2, var3);
            }
        }
        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {"gfxid, x, y, var1, var2, var3"};
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
        return "spotgraphic(gfxid, x, y, var1, var2, var3) - Spawns a graphic at a set location, var1,var2 and var3 are unknown, but my guess is that \n" +
                "it has something to do with height/rotation of the graphic, by default they are 0 so you can leave them out if you wish";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "spotgfx|spotgraphic";
    }
}
