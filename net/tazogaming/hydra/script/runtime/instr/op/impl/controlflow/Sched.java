package net.tazogaming.hydra.script.runtime.instr.op.impl.controlflow;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.script.sched.RemoveSetting;
import net.tazogaming.hydra.script.sched.SaveSettings;
import net.tazogaming.hydra.script.sched.ScheduledScriptTaskRegister;
import net.tazogaming.hydra.script.sched.TimingType;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/08/14
 * Time: 11:41
 */
public class Sched implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        String                      name      = params.nextString();
        String                      blockName = params.nextString();
        int                         delay     = params.nextInt();
        int                         timeMode  = params.nextInt();
        int                         behavior  = params.nextInt();
        int                         save      = params.nextInt();
        ScheduledScriptTaskRegister register  = null;

        if (instruction.getName().equalsIgnoreCase("sched_player")) {
            register = player.getScriptedTaskScheduler();
        } else {
            register = World.getWorld().getServerTaskRegister();
        }

        try {
            register.register(player, name, blockName, (long) delay, TimingType.values()[timeMode],
                              RemoveSetting.values()[behavior], SaveSettings.values()[save], scope.getContext());
        } catch (ArrayIndexOutOfBoundsException ee) {
            ee.printStackTrace();
        }

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "name", "blockname", "delay", "auto_remove" };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "To be used with care \n"
               + "sched() schedules a task to the server to be executed in x number of cycles \n"
               + "For example, you can schedule a script block to run in 10 ticks by using sched(\"updatenpcs\", blockname, 10, false) \n"
               + "Note: that this is not supported for player scripts, so using any player commands inside these scripts without first assigning a script will result in \n"
               + "an error being thrown. \n"
               + "You can set weather the task is automatically removed once completed or if it will remain in the event scheduler \n "
               + "Pausing will also be available in this, but you cannot jump or fork blocks then pause, as it will result in catastrophic results! \n"
               + "Its VERY important that you do not add multiple tasks into the scheduler, or this may result in bugs that go un noticed that could be fatal \n"
               + "You can dump what tasks are in the scheduler by using the dev command ::showtasks \n";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "sched|sched_player";
    }
}
