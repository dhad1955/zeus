package net.tazogaming.hydra.script.runtime.instr.op.impl.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.game.skill.combat.death.DeathHandler;
import net.tazogaming.hydra.game.skill.combat.death.DefaultDeathHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:57
 */
public class SetDeathHandler implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        try {
            final int blockId = params.nextInt();

            player.setCurDeathHandler(new DeathHandler() {
                @Override
                public void handleDeath(Killable killed, Killable killedBy) {
                    SignedBlock block  = World.getWorld().getScriptManager().getBlock(blockId);
                    Player      player = killed.getPlayer();

                    if (block == null) {
                        player.getActionSender().sendMessage("Unable to locate block");

                        return;
                    }

                    Scope scr = new Scope();

                    scr.setRunning(block);
                    scr.setController(player);

                    if (killedBy instanceof NPC) {
                        scr.setInteractingNPC(killedBy.getNpc(), 0);
                    }

                    if (!World.getWorld().getScriptManager().runScript(scr)) {
                        player.getNewScriptQueue().add(scr);
                    }

                    player.setCurDeathHandler(new DefaultDeathHandler());
                }
            });
        } catch (Exception ee) {
            player.setCurDeathHandler(new DefaultDeathHandler());
        }

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "blockName" };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Sets the current Death handler for a player, which will be invoked when the player dies \n "
               + "The death handler block will decide what happens when the player dies \n "
               + "Items etc wont be lost, as the DefaultDeathHandler which is the default death handler for dieing in combat/wilderness etc (losing items and such) will not be executed \n"
               + "However, the player will be restored by default, this means hitpoints, special attack, prayer etc, this can easilly be re-modified in the death handler if wished \n"
               + "NOTE, that its MANDATORY to unset the death handler once this has been set, not unsetting the death handler \n"
               + "Can lead to players being invincible in the wilderness and various other bugs, \n"
               + "You can unset the death handler by setting the argument to NULL";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "set_death_handler";
    }
}
