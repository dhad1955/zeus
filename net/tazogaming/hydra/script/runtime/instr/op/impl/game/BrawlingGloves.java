package net.tazogaming.hydra.script.runtime.instr.op.impl.game;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/01/2015
 * Time: 04:38
 */
public class BrawlingGloves {

    public static final int getSkillID(int id){
        switch (id){
            case 13845:
                return -4;
            case 13847:
                return 6;
            case 13848:
                return 5;
            case 13849:
                return 18;
            case 13850:
                return 8;
            case 13851:
                return 9;
            case 13852:
                return 14;
            case 13853:
                return 21;
            case 13854:
                return 17;
            case 13855:
                return 13;
            case 13856:
                return 10;
            case 13857:
                return 7;
        }
        return -1;
    }
}
