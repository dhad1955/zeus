package net.tazogaming.hydra.script.runtime.instr.op.impl.player;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/12/2014
 * Time: 01:56
 */
public class DelayActions implements ScriptInstructionContext {
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params, Scope scope) {
        player.setLock(params.nextInt());
        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {"ticks"};
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming produitems.tcts only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
        return "Delays incomming actions for x ticks but does not discard them!";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "delay_actions";
    }
}
