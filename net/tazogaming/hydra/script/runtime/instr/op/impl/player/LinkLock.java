package net.tazogaming.hydra.script.runtime.instr.op.impl.player;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/08/14
 * Time: 12:13
 */
public class LinkLock implements ScriptInstructionContext {
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params, Scope scope) {
        player.setPreventUnlink(params.nextString().equalsIgnoreCase("true"));
        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {"on"};
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
        return "<strong>be VERY careful with this.</strong> \n" +
                "linklock prevents the server from unlinking the player from the game \n" +
                "Setting a link lock will mean the player cannot be logged out or removed from the game until the lock has been removed." +
                "- Games will not save \n" +
                "- The player will remain logged in even if they log out for more than 60 seconds \n" +
                "This is very useful in preventing bugs, IE you would most likely want to do this when playing a cutscene, or playing a \n" +
                "paused event that may go on for a prolonged amount of  time \n" +
                "A great example of this being used (which is why this was created) was for magic carpet rides. \n" +
                "If a magic carpet ride was 2 minutes long, that mean if the player logged out they would be logged out on the magic carpet \n" +
                "So they would respawn where the carpet was, which isnt a good thing considering the carpet goes into clipped areas. such as water and raised height levels \n";
    }


    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "linklock";
    }
}
