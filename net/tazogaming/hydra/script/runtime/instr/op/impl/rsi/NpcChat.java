package net.tazogaming.hydra.script.runtime.instr.op.impl.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.game.ui.Dialog;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:23
 */
public class NpcChat implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        NPC    interactingNpc = script.getInteractingNpc(0);
        int    id             = 0;
        String npcName        = "Unknown";

        if ((interactingNpc != null) && (player.getCutScene() == null)) {
            id      = interactingNpc.getId();
            npcName = interactingNpc.getDefinition().getName();

            if (!interactingNpc.isBusy(player)) {
                interactingNpc.getFocus().focus(player);

            }
            interactingNpc.addChatting(player);
        } else if (player.getCutScene() != null) {
            id      = player.getCutScene().get_curr_dialog_npc();
            npcName = player.getCutScene().get_npc_dialog_name();
        }

        Dialog d = new Dialog(Dialog.TYPE_NPC_TALK, id, 0, npcName, script, params.remainingStrings());

        player.getGameFrame().showDialog(d);
        script.waitFor(Scope.WAIT_TYPE_DIALOG);

        return ScriptStatus.WAIT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "Lines..." };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Displays a npc chat dialogue, and pauses the script until the user clicks 'Click here to continue' \n"
               + "Each line must be specified as a parameter, there can be 1-5 lines in a chat sequence \n"
               + "Example usage: npcChat(\"Hello\", \"Line 2\", \"Line 3\"); or npcChat(\"Hello just 1 line\");";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "npcChat|npcchat";
    }
}
