package net.tazogaming.hydra.script.runtime.instr.op.impl.game;

//~--- non-JDK imports --------------------------------------------------------

import com.mysql.jdbc.PreparedStatement;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.security.Security;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

//~--- JDK imports ------------------------------------------------------------

import java.sql.SQLException;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/11/2014
 * Time: 22:14
 */
public class ChangePasswordRequest implements ScriptInstructionContext {
    public static final String[] SALT_TABLE = {
        "A", "{", "$", "%", "£", "@", ";", "<", ">", "/", ";", ":", "'"
    };

    /** charTable made: 14/11/26 */
    private static final char[] charTable;

    static {
        StringBuilder tmp = new StringBuilder();

        for (char ch = '0'; ch <= '9'; ++ch) {
            tmp.append(ch);
        }

        for (char ch = 'a'; ch <= 'z'; ++ch) {
            tmp.append(ch);
        }

        charTable = tmp.toString().toCharArray();
    }

    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        String newPass = params.nextString();

        if (newPass.length() < 4) {    // too short
            scope.getVar(Text.longForName("result")).set(0);

            return ScriptStatus.NEXT_STATEMENT;
        }

        if (newPass.equalsIgnoreCase("password") || newPass.equalsIgnoreCase(player.getUsername())) {
            scope.getVar(Text.longForName("result")).set(1);

            return ScriptStatus.NEXT_STATEMENT;
        }

        scope.waitFor(10);

        PasswordChangeRequest req = new PasswordChangeRequest(player, scope, newPass);
        World.getWorld().getBackgroundService().submitRequest(req);
        return ScriptStatus.WAIT;
    }

    /**
     * Method getSalt
     * Created on 14/11/26
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     *
     * @return
     */

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {"password"};
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
       return  "Sends a mysql query to the database to force-change the users password.";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "sqli_passchange";
    }

    /**
     * Class description
     * Hydrascape 639 Game server
     * Copyright (C) Tazogaming 2014
     *
     *
     * @version        Enter version here..., 14/11/26
     * @author         Daniel Hadland
     */
    class PasswordChangeRequest extends BackgroundServiceRequest {

        /** player made: 14/11/26 **/
        private Player player;

        /** script made: 14/11/26 **/
        private Scope script;

        /** newPassword made: 14/11/26 **/
        private String newPassword;

        PasswordChangeRequest(Player player, Scope scope, String pass) {
            this.script      = scope;
            this.newPassword = pass;
            this.player      = player;
        }

        private void setResponse(int response) {
            script.declare(Text.longForName("pcresp"), response);
            script.release(10);
        }

        /**
         * Method compute
         * Created on 14/08/18
         *
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        @Override
        public boolean compute() {
            MysqlConnection connection = World.getWorld().getDatabaseLink();
            String          salt       = Security.randomSalt(5);
            String          passHash   = Security.MD5(this.newPassword);
            String          saltedHash = Security.MD5(Security.MD5(salt) + passHash);
            try {
                java.sql.PreparedStatement statement =
                    connection.prepareStatement(
                        "UPDATE user SET hash = ?, salt = ? WHERE id='"
                        + player.getId() + "'");

                statement.setString(1, saltedHash);
                statement.setString(2, salt);
                statement.executeUpdate();
            } catch (SQLException sqle) {
                setResponse(1);
                sqle.printStackTrace();
            }
            setResponse(2);

            return true;
        }
    }
}
