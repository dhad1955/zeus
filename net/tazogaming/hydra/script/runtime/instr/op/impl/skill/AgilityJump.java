package net.tazogaming.hydra.script.runtime.instr.op.impl.skill;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.packetbuilder.mask.ForceMovement;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:56
 */
public class AgilityJump implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        player.setMovementLock(true);
        script.setActiveAction(true);

        ForceMovement m = new ForceMovement(params.nextInt(), params.nextInt(), params.nextInt(), params.nextInt(),
                              player, params.nextInt(), script);

        player.setMovementMask(m);
        script.waitFor(Scope.WAIT_TYPE_MOVEMENT);

        return ScriptStatus.WAIT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "direction", "steps", "wait", "movespeed", "facedirection" };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "makes the player move to a certain location at a set speed, and direction, \n"
               + "Traditionally, moving a player from location to location is done at a fixed speed, runescapes tile system \n"
               + "is tile to tile, at 600ms, this will move a player \n to a tile at a set speed and direction \n"
               + "Mainly used for agility and jumping over obstacles \n" + "direction The direction to move in, 0-3 \n"
               + "steps - the number steps to move \n"
               + "waitspeed - the number of 20ms cycles to wait before moving \n"
               + "movespeed - the speed to move in 0 = instant 100 = dirt slow \n"
               + "facedirection - the direction to face whilst doing the movement";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "agil_jump";
    }
}
