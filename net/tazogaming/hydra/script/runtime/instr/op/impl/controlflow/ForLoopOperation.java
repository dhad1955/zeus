package net.tazogaming.hydra.script.runtime.instr.op.impl.controlflow;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.core.ForLoop;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 17:52
 */
public class ForLoopOperation implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        ForLoop loop        = instruction.getForLoop();
        ScriptVar initializer = new ScriptVar(loop.getInitializationVar(),
                                    loop.getInitializationNode().eval(script, player));
        long start2 = System.currentTimeMillis();
        int  count2 = 0;

        Scope loopScope = new Scope();
        loopScope.setParent(script);
        loopScope.setController(player);

        loopScope.declareVar(initializer.getHash(), initializer);


        while (loop.getConditional().eval(loopScope)) {
            Scope scope = new Scope(player, loop.getCodeBlock());
            scope.setController(player);

            count2++;

            if (count2 > 1000) {
                if (System.currentTimeMillis() - start2 > 1000) {
                    throw new RuntimeException("Error, looks like loop cannot complete, stopping");
                }
            }

            if (scope.isTerminated()) {
                break;
            }

            scope.setParent(loopScope);

            if (!World.getWorld().getScriptManager().evaluate(scope)) {
                throw new RuntimeException("Error pausing scripts not allowed in for loops");
            }

            if (scope.getLastException() != null) {
                return ScriptStatus.TERMINATE;
            }

            if (scope.isBreakLoop()) {
                break;
            }

            scope.setParent(loopScope);

            ScriptRuntime.executeInstruction(loop.getAfterThought(), player, scope);
        }

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[0];
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return null;
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "for";
    }
}
