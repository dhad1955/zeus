package net.tazogaming.hydra.script.runtime.instr.op.impl.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.security.Security;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/01/2015
 * Time: 10:08
 */
public class ChangeUsernameRequest implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, final Player player, ExpressionParser params,
                             final Scope scope) {
        final String requestedUsername = params.nextString();

        scope.replaceOrDeclare(Text.longForName("result"), 0);

        if ((requestedUsername.length() < 3) || (requestedUsername.length() > 12)) {
            scope.getVar(Text.longForName("result")).set(0);

            return ScriptStatus.NEXT_STATEMENT;
        }

        if (Security.badUsername(requestedUsername)) {
            scope.getVar(Text.longForName("result")).set(1);

            return ScriptStatus.NEXT_STATEMENT;
        }

        if (Security.isCensoredWord(requestedUsername)) {
            scope.getVar(Text.longForName("result")).set(2);

            return ScriptStatus.NEXT_STATEMENT;
        }

       ;
        scope.waitFor(10);
        player.setActionsDisabled(true);
        scope.setActiveAction(true);
        World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
            @Override
            public boolean compute() {

                try {
                    if (World.getWorld().getDatabaseLink().query("SELECT username from `user` WHERE username='" + requestedUsername + "'").next()) {
                        scope.getVar(Text.longForName("result")).set(3);
                        player.setActionsDisabled(false);
                        scope.release(10);
                        return true;
                    } else {
                        World.getWorld().getDatabaseLink().query("UPDATE `user` SET username='" + requestedUsername + "' where id='" + player.getId() + "'");
                        scope.getVar(Text.longForName("result")).set(4);
                        player.setUsername(requestedUsername);
                        player.setActionsDisabled(false);
                        scope.release(10);
                        return true;
                    }


                } catch (Exception ee) {
                    ee.printStackTrace();
                    return true;
                }
            }


        });
        return ScriptStatus.WAIT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {"username"};
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
        return "sqli_changeuser";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "sqli_changeuser";
    }
}
