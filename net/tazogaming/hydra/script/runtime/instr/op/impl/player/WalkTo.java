package net.tazogaming.hydra.script.runtime.instr.op.impl.player;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.actions.AgilityWalkAction;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:56
 */
public class WalkTo implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        Point location = Point.location(params.nextInt(), params.nextInt(), player.getHeight());

        player.getRoute().resetPath();
        player.setMovementLock(true);
        player.getActionSender().sendCloseWalkingFlag();

        AgilityWalkAction action = new AgilityWalkAction(player, location, false, -1, scope);

        if (params.hasNext()) {
            action.setStandAnim(params.nextInt());
        }

        if(params.hasNext()) {
            action.setRunning(true);
        }

        player.setCurrentAction(action);

        if(instruction.getName().equalsIgnoreCase("moveto"))
            return ScriptStatus.NEXT_STATEMENT;

        scope.waitFor(Scope.WAIT_TYPE_MOVEMENT);

        return ScriptStatus.WAIT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "x", "y" };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "walkto(x, y) - Forces the player to walk to the specified location with clipping ignored, this will also pause the script until the path has been reached, whilst walking the players actions will be disabled completely";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "walkto|moveto";
    }
}
