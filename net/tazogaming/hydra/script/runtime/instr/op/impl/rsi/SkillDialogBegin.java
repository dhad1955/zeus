package net.tazogaming.hydra.script.runtime.instr.op.impl.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptProduceAction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.game.ui.ProduceActionListener;
import net.tazogaming.hydra.game.ui.ProduceRequest;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:10
 */
public class SkillDialogBegin implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        ProduceRequest req      = new ProduceRequest(params.nextInt(), params.nextString(), player);
        final int      block_id = params.nextInt();
        int            delay    = 4;

        if (params.hasNext()) {
            delay = params.nextInt();
        }

        final int finalDelay = delay;

        req.setMaxToProduce(1000);
        player.getGameFrame().prepareProductRequest(req);
        req.setListener(new ProduceActionListener() {
            @Override
            public void onProduce(int itemId, int amount, int slot, Player player) {
                player.terminateActions();
                player.terminateScripts();

                ScriptProduceAction action = new ScriptProduceAction(block_id, itemId, finalDelay, player);

                action.setActionAmount(amount);
                player.setCurrentAction(action);
            }
        });

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "type", "Text", "Block name" };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Sets the first phase of building a skill dialogue interface, "
               + "IE Fletching/cooking/crafting/gemcutting interface that covers the chat box, \n "
               + "the first parameter is the action type (ie what will the right click menu say, look at the constants for more information) \n"
               + "The second parameter 'text' is the text do display, so for example you may put in 'how many do you want to cook?'"
               + " "
               + "\n The third parameter is the name of the block to evaluate the action to, so when the player clicks a button on the skill dialogue (specified by sd_additem) that block will directTrigger with the passed identifier (see sd_additem)";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "sd_begin";
    }
}
