package net.tazogaming.hydra.script.runtime.instr.op.impl.game;

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.game.ui.HintIcon;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/08/14
 * Time: 18:54
 */
public class HintIconOP implements ScriptInstructionContext {
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params, Scope scope) {
        Object obj = params.next();
        if(obj instanceof String){
            player.getGameFrame().setCurrentHintIcon(null);
        }else if(obj instanceof Integer){
            player.getGameFrame().setCurrentHintIcon(new HintIcon(Point.location((Integer)obj, params.nextInt(), player.getHeight())));
        }else if(obj instanceof Entity){
            player.getGameFrame().setCurrentHintIcon(new HintIcon((Entity)obj));
        }

        return ScriptStatus.NEXT_STATEMENT;

    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return new String[]{"entity"};
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
        return "Sets the current hint icon to focus on an entity, or specific location \n" +
                "To make it focus on a location: hinticon(x, y) \n" +
                "To make it focus on a entity(npc/player) hinticon(entity) \n" +
                "To remove it, hinticon(null)";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "hinticon";
    }
}
