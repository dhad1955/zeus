package net.tazogaming.hydra.script.runtime.instr.op.impl.controlflow;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.build.ScriptCompiler;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.script.runtime.instr.op.Signature;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/11/2014
 * Time: 00:18
 */
public class Fork implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        SignedBlock block   = null;
        int         blockID = params.nextInt();

        if (blockID >= ScriptCompiler.BLOCK_DELIMITER) {
            block = script.getRunning().getContext().getBlock(blockID - ScriptCompiler.BLOCK_DELIMITER);
        } else {
            block = World.getWorld().getScriptManager().getBlock(blockID);
        }

        if (block == null) {
            player.getActionSender().sendMessage("Unable to locate block");

            return ScriptStatus.TERMINATE;
        }

        Scope scr = new Scope();

        scr.setRunning(block);

        long[] methodSignature = block.getSignature();

        if (methodSignature != null) {
            for (int i = 0; i < methodSignature.length; i++) {
                scr.declareVar(methodSignature[i], new ScriptVar(methodSignature[i], params.next()));
            }
        }

        scr.setInteractingNpcs(script.getInteractingNpcs());
        scr.setController(player);
        scr.setAttachement(script.getAttachement());
        scr.setParent(script);

        if (!World.getWorld().getScriptManager().evaluate(scr)) {
            player.getNewScriptQueue().add(scr);
        }

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String[] getMethodSignature() {
        return Signature.buildSignature("block");
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getUsage() {
        return "n/a";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     *
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public String getMethodName() {
        return "fork";
    }
}
