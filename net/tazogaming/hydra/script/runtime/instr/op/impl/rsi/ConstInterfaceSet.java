package net.tazogaming.hydra.script.runtime.instr.op.impl.rsi;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.game.ui.ConstructableItem;
import net.tazogaming.hydra.game.ui.ConstructionInterfaceSettings;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:42
 */
public class ConstInterfaceSet implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        ConstructionInterfaceSettings settings = player.getGameFrame().getFurnitureBuildSettings();

        if (settings == null) {
            settings = new ConstructionInterfaceSettings();
            player.getGameFrame().setFurnitureBuildSettings(settings);
        }

        int index = params.nextInt();
        int model = params.nextInt();

        if (model == -1) {
            settings.set(index, null);

            return ScriptStatus.NEXT_STATEMENT;
        }

        int   lvl = params.nextInt();
        int[] bs  = new int[6];
        int   cur = 0;

        while (params.hasNext()) {
            bs[cur++] = params.nextInt();

            if (!params.hasNext()) {
                throw new RuntimeException("error: " + bs[cur - 1]);
            }

            bs[cur++] = params.nextInt();
        }

        if (cur < bs.length) {
            bs = Arrays.copyOf(bs, cur);
        }

        settings.set(index, new ConstructableItem(model, lvl, bs));

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "index", "level", "Items,amount..." };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Sets an item to the construction interface, with the specified required items, with a maximum limit of 3, eg use as const_set(index, level, itemid1, itemamount1, itemid2, itemamount2, itemid3, itemamount3) const_set(index, level, itemid, itemamount1) \n " + "Set to -1 to not display an item on that index \n" + "Indexes range from 0-7";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "const_set";
    }
}
