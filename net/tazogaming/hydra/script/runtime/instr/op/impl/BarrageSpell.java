package net.tazogaming.hydra.script.runtime.instr.op.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.projectile.Projectile;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.game.minigame.clanwars.ClanWar;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.game.skill.combat.combat.Damage;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.game.skill.combat.impact.MagicProjectileImpactEvent;
import net.tazogaming.hydra.game.skill.summoning.Summoning;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.util.TimingUtility;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 16:36
 */
public class BarrageSpell implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        int    endGraphic       = params.nextInt();
        int    endGraphicHeight = params.nextInt();
        int    maxHit           = params.nextInt();
        int    experience       = params.nextInt();
        int    healPercent      = params.nextInt();
        int    freezeSeconds    = params.nextInt();
        int    poisonTicks      = params.nextInt();
        int    multiRange       = params.nextInt();
        int    miasmicTime      = 0;
        double boost            = 1.0;

        if (params.hasNext()) {
            miasmicTime = params.nextInt();
        }

        Killable k = (Killable) script.getAttachement();

        if (player.isTenth(6)) {
            maxHit        *= 1.22;
            freezeSeconds *= 1.50;
        }

        if(player.getEquipment().getTotalBonuses()[Item.BONUS_MAGE_BOOST] != 0){
            boost += (double)((player.getEquipment().getTotalBonuses()[Item.BONUS_MAGE_BOOST]) / 100d);
        }

        maxHit *= boost;

        if (k == null) {
            script.debug("warning target is null");

            return ScriptStatus.TERMINATE;
        }

        ArrayList<Killable> multi = k.getViewArea().getTargetsInRange(multiRange);

        multi.add(k);

        int totDmg = 0;

        if (!k.isInMultiArea() ||!player.isInMultiArea()) {
            multi.clear();
            multi.add(k);
        }

        for (Killable k2 : multi) {
            if (k2 == player) {
                continue;
            }

            if (k2.getHeight() != player.getHeight()) {
                continue;
            }

            if (!k2.canBeAttacked(player)) {
                continue;
            }

            if (k2.getCurrentInstance() != player.getCurrentInstance()) {
                continue;
            }

            if ((k2 instanceof Player) &&!Combat.pvp_check(player, k2.getPlayer())
                    &&!(k2.isInArea(ClanWar.RED_PORTAL_PVP) || k2.isInArea(ClanWar.WHITE_PORTAL_PVP))) {
                continue;
            }


            if (k2 instanceof NPC) {
                if ((k2.getNpc().getDefinition() == null) ||!((NPC) k2).getDefinition().isAttackable() && !Summoning.PVPCheck(k2.getNpc(), player)) {
                    continue;
                }
            }


            double dmg1 = GameMath.randomMaxHit(maxHit + 1);

            double dmg = k2.preCalcDamage(player, dmg1, Damage.TYPE_MAGIC);


            totDmg += dmg;
            k2.registerHit(player);

            int distance = Point.getDistance(player.getLocation(), k2.getLocation());
            int time     = 2;

            if (distance == 3) {
                time = 4;// + ((3 - distance) / 2);
            }
            if(distance >= 4)
                time = 5;


            if (dmg > 0) {
                if (freezeSeconds > 0) {
                    if (!k2.freeze(freezeSeconds)) {
                        if (endGraphic == 369) {
                            endGraphic       = 1677;
                            endGraphicHeight = 0;
                        }
                    }
                }
            }

            Projectile projectile = new Projectile(player, k2, time, new MagicProjectileImpactEvent((maxHit == -1)
                    ? -1
                    : (dmg), endGraphic, endGraphicHeight));

            player.registerProjectile(projectile);

            if (dmg > 0) {
                if (poisonTicks > 0) {
                    k2.poison(poisonTicks);
                }

                if (healPercent > 0) {
                    player.addHealth(dmg * (1 + (healPercent / 100)));
                }

                if (miasmicTime > 0) {
                    if (!k2.getTimers().timerActive(TimingUtility.MIASMIC_TIMER)) {
                        k2.getTimers().setTime(TimingUtility.MIASMIC_TIMER, miasmicTime);

                        if (k2 instanceof Player) {
                            player.getActionSender().sendMessage("A magical force slows down your attack speed");
                        }
                    }
                }
            }
        }

        player.addXp(6, experience);
       if(totDmg > 0) {
           player.addXp(6, totDmg * 4);
           player.addXp(3, (int) (totDmg * 1.33));
       }
        player.addAchievementProgress2(Achievement.MAGE, 1);
        player.addAchievementProgress2(Achievement.MAGE_II, 1);
        player.addAchievementProgress2(Achievement.MAGE_III, 1);

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] {
            "projectile_gfx", "projectile_waitspeed", "projectile_movespeed", "endgfx", "endgfxheight", "maxhit",
            "basexp", "healpercent", "freezeticks", "poisonticks", "multirange", "miasmictime"
        };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Casts a spell, on the current interacting entity, " + "Param projectilge \n"
               + "Usage: projectile_gfx = the graphic id of the projectile\n"
               + "projectile_waitspeed = delay in ms to wait before casting the projectile\n"
               + "projectile_movespeed = speed of the moving projectile\n"
               + "endgfx = the end graphic of the projectile (impact graphic)\n"
               + "endgfxheight = the end graphic height 1 = low 0 = high\n"
               + "maxhit = the base maxhit for this spell\n" + "basexp = the base xp for this spell\n"
               + "healpercent = the amount of percent of the damage that will heal the casters hitpoint set to 0 to disable\n"
               + "freezeticks = the amount of ticks the target will be frozen for when the projectile hits set to 0 to disable\n"
               + "poisonticks = the amount of ticks that will poison the target once the projectile hits, set to 0 to disable\n"
               + "multirange = the range of multi combat spells (0 to disable)\n"
               + "miasmic time = the amount of ticks the target will be efected by miasmic abilities\n";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "spell_noproj";
    }
}
