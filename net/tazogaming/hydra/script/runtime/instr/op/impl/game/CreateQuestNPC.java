package net.tazogaming.hydra.script.runtime.instr.op.impl.game;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.game.skill.combat.combat.Killable;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.NpcAutoSpawn;
import net.tazogaming.hydra.entity3d.npc.death.DeathPolicy;
import net.tazogaming.hydra.entity3d.npc.death.NpcDeathHandler;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 16:52
 */
public class CreateQuestNPC implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        NpcAutoSpawn spwn = new NpcAutoSpawn();

        spwn.setNpcId(params.nextInt());
        spwn.setSpawnX(params.nextInt());
        spwn.setSpawnY(params.nextInt());
        spwn.setRoamRange(params.nextInt());
        spwn.setSpawnHeight(player.getHeight());

        NPC q_npc = new NPC(spwn.getNpcId(), Point.location(spwn.getSpawnX(), spwn.getSpawnY(), player.getHeight()),
                            spwn);

        World.getWorld().registerNPC(q_npc);
        q_npc.setQuestController(player);
        q_npc.setCurrentInstance(player.getCurrentInstance());
        player.validatedWatchedNpcs();
        player.updateWatchedNpcs();
        q_npc.setDeathHandler(new NpcDeathHandler(DeathPolicy.UNLINK_ON_DEATH) {
            @Override
            protected void onDeath(NPC killed, Killable killedBy) {
                killedBy.getPlayer().getLinked_npcs().remove((NPC) killed);

                // To change body of implemented methods use File | Settings | File Templates.
            }
        });

        if (player.getCurrentInstance() != null) {
            player.getCurrentInstance().add(q_npc);
        }

        if (params.hasNext()) {
            player.getActionSender().markNpc(q_npc.getIndex());
        }

        player.add_linked_npc(q_npc, params.hasNext());

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "npcid", "spawnx", "spawny", "roamrange" };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Spawns an npc that is only interactable by the current player";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "createquestnpc";
    }
}
