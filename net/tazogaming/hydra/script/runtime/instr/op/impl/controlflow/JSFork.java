package net.tazogaming.hydra.script.runtime.instr.op.impl.controlflow;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.script.runtime.js.JPlugin;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 18:10
 */
public class JSFork implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param script
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope script) {
        try {
            boolean found    = false;
            String  funcName = instruction.getName();

            if (ScriptRuntime.GLOBAL_SCOPE.hasFunction(funcName)) {
                ScriptRuntime.GLOBAL_SCOPE.get(player, script, funcName, (((params == null) ||!params.hasNext())
                        ? null
                        : params.remainingObjects()));

                return ScriptStatus.NEXT_STATEMENT;
            }

            if (script.getPlugins().size() == 0) {
                player.getActionSender().sendMessage("Unable to evaluate javascript, no script binding found "+funcName);

                return ScriptStatus.TERMINATE;
            }

            for (JPlugin plug : script.getPlugins()) {
                if (plug.hasFunction(funcName)) {
                    found = true;
                    plug.get(player, script, funcName, ((params == null) ||!params.hasNext())
                                                       ? null
                                                       : params.remainingObjects());

                    return ScriptStatus.NEXT_STATEMENT;
                }
            }

            if (!found) {
                player.getActionSender().sendMessage("Unable to evaluate javascript, unable to find function: "
                        + funcName + "()");

                System.err.println("unable to evaluate javascript cannot find function: "+funcName);
                return ScriptStatus.TERMINATE;
            }
        } catch (Exception ee) {
            ee.printStackTrace();
            player.getActionSender().sendMessage("Unable to evaluate javascript");

            return ScriptStatus.TERMINATE;
        }

        return ScriptStatus.NEXT_STATEMENT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[0];
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return null;
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "jsfork";
    }
}
