package net.tazogaming.hydra.script.runtime.instr.op.impl.skill;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.actions.AgilityWalkAction;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.core.RuntimeInstruction;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 13:56
 */
public class AgilityWalk implements ScriptInstructionContext {

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param instruction
     * @param statementType
     * @param player
     * @param params
     * @param scope
     *
     * @return
     */
    @Override
    public ScriptStatus eval(RuntimeInstruction instruction, int statementType, Player player, ExpressionParser params,
                             Scope scope) {
        Point p     = Point.location(params.nextInt(), params.nextInt(), player.getHeight());
        int   emote = params.nextInt();

        player.getRoute().resetPath();

        boolean running = params.nextInt() == 1;

        player.setMovementLock(true);

        AgilityWalkAction action = new AgilityWalkAction(player, p, running, emote, scope);

        player.getAppearance().setChanged(true);

        if (params.hasNext()) {
            action.setStandAnim(params.nextInt());
        }

        scope.waitFor(Scope.WAIT_TYPE_MOVEMENT);
        player.setCurrentAction(action);

        return ScriptStatus.WAIT;
    }

    /**
     * Method getMethodSignature
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String[] getMethodSignature() {
        return new String[] { "absx", "absy", "anim", "running" };
    }

    /**
     * Method getUsage
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getUsage() {
        return "Gets the player to walk to a certain location whilst doing a specified emote, used for agility obstacles such as monkeybars, log walks, etc \n"
               + "x = the absolute x to walkto\n" + "y = the absolutey = to walkto \n"
               + "emote = the RENDER emote, these can be found on rune-server they are not the same as anims \n"
               + "running = 1 walking = 0";
    }

    /**
     * Method getMethodName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    @Override
    public String getMethodName() {
        return "agilitywalk|agilitymove";
    }
}
