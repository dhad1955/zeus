package net.tazogaming.hydra.script.runtime.instr.op;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 11/08/14
 * Time: 12:41
 */
public enum ScriptStatus { NEXT_STATEMENT, WAIT, TERMINATE }
