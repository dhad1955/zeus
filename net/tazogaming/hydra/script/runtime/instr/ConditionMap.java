package net.tazogaming.hydra.script.runtime.instr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.instr.condition.*;
import net.tazogaming.hydra.util.Text;

//~--- JDK imports ------------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 01:42
 */
import java.util.HashMap;

/**
 * Rune War Game Server
 * http://hydra.tazogaming.org
 * Builder {$build}
 * net.tazo_.server.script
 *
 * @author Gander
 *         Date: 25-Sep-2008 {10:07:40}
 */
public class ConditionMap {
    private static HashMap<String, InternalCondition> conditions    = new HashMap<String, InternalCondition>();
    private static StringBuffer                         conditionData = new StringBuffer();

    static {
        addCondition("iftimer", "timerid", "checks if a timer is active", new IfTimer());
        addCondition("ifduelrule", "ruleid", "checks if a duel rule is set (used for duel arena only",
                     new IfDuelRule());
        addCondition("iflevelatleast", "skillid,level", "Checks if a user has the required skill level specified",
                     new IfLevelAbove());
        addCondition("ifhasitemamount", "itemid,amount", "Checks if the user has the item amount",
                     new IfHasItemAmount());
        addCondition("ifhaswaterstaff", "", "Checks if a user has a water staff", new IfHasWaterStaff());
        addCondition("ifwearing", "itemid", "Checks if the user is wearing the set itemid", new IfWearing());
        addCondition("ifhasearthstaff", "", "Checks if the user has an earth staff", new IfHasEarthStaff());
        addCondition("ifhasairstaff", "", "checks if the user has a air staff", new IfHasAirStaff());
        addCondition("ifhasfirestaff", "", "checks if the user has a fire staff", new IfHasFireStaff());
        addCondition("ifisinarea", "areaname", "Checks if the user is in an area", new IsInArea());
        addCondition("ifoption", "optionid", "Tests the last selectoption value", new IfOption());
        addCondition("ifclipped", "direction", "Checks if the user can move in the specified direction",
                     new IfClipped());
        addCondition("ifobjects", "", "Checks if the current tile has objects on it", new IfObjects());
        addCondition("ifhasspace", "itemid", "Checks if the user has space for the set item", new IfHasSpace());
        addCondition("ifdefault", "object", "Checks if an object is default", new IfObjectDefault());
        addCondition("ifclue", "clueid", "Checks if the current cluescroll id is the one specified", new IfClue());
        addCondition("ifbit_test", "num,bit", "Checks a bit", new BitSet());
        addCondition("if_has_slayer_task", "", "checks if the player has a slayer task", new IfHasSlayerTask());
        addCondition(
            "if_slayer_task", "slayertableid,maxamount",
            "Generates a new slayer task and returns false if the player doesnt have the right level for any tasks in the specified table",
            new IfSlayerTask());
        addCondition("if_skill_chance", "base,statid,requiredlevel,bonus,",
                     "Skill chance formula for failing, returns false if the user has failed", new IfChance());
        addCondition(
            "if_tele_block", "",
            "Returns true if the player cannot teleport, weather there in an area that you cant teleport out of or if there teleblocked, will also auto send the stop message",
            new IfTeleBlock());
        addCondition("ifplayervar", "varname", "Checks if a playervar exists", new IfPlayerVar());
        addCondition("ifquestnpc", "", "Checks if theres a quest npc active", new IfQuestNpc());
        addCondition("if_has_item_anywhere", "itemid",
                     "Checks if the user has the item in there bank, equipment, or inventory", new IfHasItemAnywhere());
        addCondition("ifpos", "x,y", "Checks the current users location matches the specified arguments",
                     new IfPosition());
        addCondition("ifequipped", "slot", "Checks if the user has something equipped in the specified equip slot",
                     new IfEquipped());
        addCondition("if_action_disabled", "actionid", "Checks if an action is disabled", new IfActionDisabled());
        addCondition("if_party_leader", "", "Checks if the player is the party leader of a dungeon",
                     new IfDungLeader());
        addCondition("if_dung_portal", "", "Checks if the player can go to the next floor in dungeoneering",
                     new IfDungPortal());
        addCondition("ifdbox", "", "", new IfDBox());
        addCondition("ifenum_get", "enumname,key",
                     "Loads an enum onto the stack and returns true if the enum was found", new EnumGet());
        addCondition("ifbutton", "buttonid,value", "Checks if a button config matches the set value", new IfBConfig());
        addCondition("ifconfig", "configid,value", "Checks if a config id matches the value", new IfConfig());
        addCondition("ifnull", "object", "Checks if any variable or object is null", new IfNull());
        addCondition("ifvar_exists", "varname", "Checks if a var exists", new IfVarExists());
        addCondition("iflist_contains", "list,element", "Checks if a list contains an element", new ListContains());
        addCondition("ifnpc_busy", "npc", "Checks if an npc is currently locked, talking to somebody, or pursuing somebody in combat.", new IfNpcBusy());
        addCondition("ifrandom", "chance", "Random chance between 0-100", new IfRandom());
        addCondition("iftask_exists", "taskname", "Checks the players task scheduler to see if a task exists", new IfTaskExists());
        addCondition("ifvar", "var", "checks if a var exists", new IfVarExists());
        addCondition("ifplayervarabove", "var,above", "dont use this.", new IfPlayerVarAbove());
        addCondition("ifnpc", "entity", "Tests is an entity is an npc", new IfNPC());
        addCondition("ifis_numeric", "input", "Tests if an entity is a number", new IfNumeric());
        addCondition("ifquest_completed", "questid", "Tests if the player has completed a quest", new IfQuestCompleted());
        addCondition("ifquest_started", "questid", "Tests if a player has started the specified quest", new IfQuestStarted());
        addCondition("ifcheckpoint_complete", "questid,checkpoint", "Tests if a player has completed a certain quest stage/checkpoint", new IfQuestStageCompleted());
        addCondition("ifisincombat", "", "ifisincombat() checks if the user is fighting or has been fighting within the last 20 secs", new IsInCombat());
        addCondition("iftask_leader", "", "checks if the user is the leader of the current slayer task", new IfIsTaskLeader());
        addCondition("ifhas_slayerinvite", "", "checks if the user is the leader of the current slayer task", new IfHasSlayerInvite());


        try {
            Text.dumpStringToFile("config/runedoc/conditions.html",
                                  Text.loadTextFromFileAsString("config/runedoc/doc_header.html").replace("{$DATA}",
                                      conditionData.toString()));
            conditionData = null;
        } catch (Exception ee) {}
    }

    /**
     * Method v
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void v() {}

    /**
     * Method addCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param condition1
     * @param signature
     * @param description
     * @param condition
     */
    public static void addCondition(String condition1, String signature, String description,
                                    InternalCondition condition) {
        condition1 = condition1.substring(2);
        saveConditionData(condition1, signature, description);
        conditions.put(condition1, condition);
    }

    private static void saveConditionData(String name, String signature, String description) {
        String methodString = "";

        if (signature.length() == 0) {
            methodString = name + "()";
        } else {
            String[] split = signature.split(",");

            methodString = name + "(";

            for (int i = 0; i < split.length; i++) {
                methodString += split[i] + ", ";
            }

            methodString = methodString.substring(0, methodString.length() - 2) + ")";
        }

        conditionData.append("<tr><td>" + methodString + "</td><td>" + description + "</td></tr>");
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public static InternalCondition get(String name) {
      try {
        return conditions.get(name);
      }catch (Exception ee) {
          return conditions.get(name.substring(2));
      }
    }

    /**
     * Method getCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param pla
     * @param params
     *
     * @return
     */
    public static boolean getCondition(String name, Player pla, Object... params) {
        InternalCondition condition = null;

        try {
            condition = conditions.get(name);
        } catch (Exception ee) {}

        if (condition == null) {
            condition = conditions.get(name.substring(2));
        }

        return false;
    }
}
