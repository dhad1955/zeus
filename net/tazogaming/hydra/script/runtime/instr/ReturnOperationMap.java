package net.tazogaming.hydra.script.runtime.instr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.instr.condition.GetDist;
import net.tazogaming.hydra.script.runtime.instr.condition.Mins2Ticks;
import net.tazogaming.hydra.script.runtime.instr.impl.*;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedWriter;
import java.io.FileWriter;

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 14:33
 */
public class ReturnOperationMap {

    /** calls made: 14/08/18 */
    private static ReturnFunction[] calls = new ReturnFunction[100];

    /** nameMap made: 14/08/18 */
    private static HashMap<String, Integer> nameMap = new HashMap<String, Integer>();

    /** caret made: 14/08/18 */
    private static int caret = 0;

    /** information made: 14/08/18 */
    private static String[] information = new String[100];

    static {
        put("set_bit", "Sets a bit flag", SetBit.class);
        put("bit_clear", "Clears a bit flag", BitClear.class);
        put("bit_toggle", "Toggles a bit", BitToggle.class);

        put("getkillcount", "Gets the killcount for a certain npc", GetKillCount.class);
        put("getX", "Gets the players absolute X position", GetX.class);
        put("getY", "Gets the players absolute Y position", GetY.class);
        put("getHeight", "Gets the players height", GetHeight.class);
        put("inv_total", "inv_total(amount) gets the amount of items in a players inventory", InvTotal.class);
        put("maxitem",
            "maxitem(interfaces, amount) gets the maximum number of items in the players inventory limited to the amount,"
            + "for example, maxitem(995, 10000) would return 10000 if the player had 10000 or more in there inventory, if they didnt it would return the total amount that they have", MaxItem.class);
        put("inv_space",
            "inv_space() returns the amount of free slots in the players inventory, inv_space(itemid) used for stackable items",
            GetFreeSlots.class);
        put("getGroup", "gets the players current rights", GetGroup.class);
        put("curstat", "curstat(skillid) gets the players boosted level", GetCurStat.class);
        put("maxstat", "maxstat(skillid) gets the players actual level", GetMaxStat.class);
        put("time_since", "timesince(ticks) time since the set cycle", TimeSince.class);
        put("getTime", "Gets the current server cycle", CurrentTime.class);
        put("getItemName", "Returns the name for an item", GetItemName.class);
        put("perc", "percent(subtotal,total) gets the percentage of a subtotal from a total", Percent.class);
        put("enum_key", "enum_key(name, index) - returns the key for the specified enum index", EnumKey.class);
        put("enum_size", "enum_size(name) - returns the size of an enum", EnumSize.class);
        put("rand", "rand(max) - returns a random number with the maximum, rand(min, max) also works", Rand.class);
        put("getItemPrice", "getItemPrice(itemid) - returns the price of the specified item", ItemPrice.class);
        put("getSlayerInfo", "getSlayerInfo(type) 0 = slaykillid 1 = totalkilled 2 = amounttokill",
            GetSlayerInfo.class);
        put("npc_name", "npc_name(npc) or npc_name(npcid) returns the name of the npc", GetNpcName.class);
        put("plural",
            "plural(amount, name) returns the plural for the amount specified, for example plural(2, \"shark\" would return sharks",
            Plural.class);
        put("enumdata", "enumdata(enum, key, index) - returns a piece of data from an enum", EnumData.class);
        put("cash_format", "cash_format(amount) would transfer 10,000 to 10k etc", CashFormat.class);
        put("number_format", "number_format(amount) would transform 10000 to 10,000", NumberFormat.class);
        put("getUsername", "returns the players username", GetUsername.class);
        put("entity_x", "returns the x coordinate of the given entity, flooritem, object, npc or player",
            EntityX.class);
        put("entity_y", "returns the y coordinate of the given entity, flooritem, object, npc or player",
            EntityY.class);
        put("obj_rotation", "returns the current rotation of the given object", ObjRotation.class);
        put("newnpc", "newnpc(npcid,x,y,h,range) - spawns an npc into the world", SpawnNPC.class);
        put("secs2ticks", "sec2ticks(seconds) - Converts the set amount of seconds to game ticks",
            TicksForSeconds.class);
        put("get_list", "get_list(list) - returns a global list as defined", GetList.class);
        put("list_size", "list_size(list) - returns the size of a global list", ListSize.class);
        put("list_get", "list_get(list, index) - returns the element of a list at the set index", ListGet.class);
        put("findnpc",
            "\n" + "findnpc(npcid, range, options...)\n" + "findnpc is a very powerful tool,\n"
            + "this will begin a local npc search to find an npc with the specified options\n" + "\n"
            + "npcid - the npc id to look for, note you can set this to -1 to find ANY npc\n"
            + "range - this is the range to search for, for example if you were looking for an npc that is 5 tiles or less away then set it to 5\n"
            + "options... \n" + "options are optional extras, you can add extra options using the constants \n"
            + "there are 3 options available at the moment\n"
            + "ignore_busy - add this option if you want to ignore if the npc is busy or not by busy that means talking to another player\n"
            + "not_incombat - add this option if you want to look for an npc that currently isnt in combat.\n"
            + "can_get_me - add this option if you want to specify only to look for an npc thats within its walk range to get you.\n"
            + "for example, if we wanted to search for a guard, thats within 10 tiles away, that can attack us and thats not already attacking someone we could do\n"
            + "findnpc(guard, 10, not_incombat, can_getme)\n" + "if we just wanted to find any guard we could do\n"
            + "findnpc(guard, 10)\n", FindNpc.class);
        put("createlist", "createlist(max_size) creates a list with a maximum size", List.class);
        put("me", "me() Returns the current player object.", Me.class);
        put("watchednpcs", "watchednpcs() returns a list containing all the npcs in view.", WatchedNpcs.class);
        put("watchedplayers", "watchedplayers() returns a list with all the players in view", WatchedPlayers.class);
        put("minutes", "minutes() returns milliseconds for minutes, used for the task scheduler", Minutes.class);
        put("global_players", "global_players() returns all the players in the server", GetAllPlayers.class);
        put("global_npcs", "global_npcs() returns all the npcs in the server", GetAllPlayers.class);
        put("obj_add",
            "obj_add (id, x, y, h, type, rotation) adds and returns the value of an object, add a extra parameter to schedule when it will be removed",
            PlaceObj.class);
        put("arraylist", "arraylist(element.....) construct an array list with as many elements as wanted",
            CreateArrayList.class);
        put("subtotal", "subtotal(amount, percent) gets the sub total based on %", Subtotal.class);
        put("getobj", "getobj(id,x,y,z) - returns either an object in the world or a null if it doesnt exist",
            GetObject.class);
        put("getpoints", "getpoints(id) - returns the players points specified by type", Getpoints.class);
        put("createstruct", "used by the compiler", CreateStruct.class);
        put("getdist",
            "Checks the distance between 2 points, can be used as either getdist(entity, entity) or getdist(entity, x, y) or getdist(x,y, x,y)",
            GetDist.class);
        put("percentof",
            "Gets the subtotal of a percentage, for example what is 6% of 100 would be percentof(100, 6); so 6",
            PercentOf.class);
        put("safenpc",
            "safenpc(npcid, roamrange) spawns an npc relative to the players location ensuring that it wont spawn inside a wall or any clipped area \n"
            + "This can be used for random events and a whole bunch of other things, works the same as newnpc except the location is automatic and safe", SafeNPC.class);
        put("createquestnpc", "creates a quest npc", CreateQuestNpc.class);
        put("tick2string", "Convert ticks to a string", TickToString.class);

        put("food_time", "Returns food protect timer", FoodProtTime.class);
        put("sizeof", "sizeof(arg) Returns the size of the given argument", SizeOf.class);
        put("is_numeric", "returns true if the given argument is a number.", IsNumeric.class);
        put("ticks2secs", "converts ticks (server cycles) to seconds.", TicksForSeconds.class);
        put("castint", " castint(input) Converts a string or object to an Integer", CastInt.class);
        put("split", " split(input, delimiter) Splits a string by delimiter and returns it as a list.", Split.class);
        put("getPlayerByName", "gets a player by there username", GetPlayerByName.class);
        put("substr", "substr(input, offset, leng), substr(input, offset) gets the substring of a string", SubStr.class);
        put("accept_invite", "", AcceptSlayerInvite.class);
        put("getStage", "getStage(questid) gets the first uncompleted stage of the specified quest", GetQuestStage.class);
        put("millis", "millis() returns current system.currenttimemillis()", Millis.class);
        put("mins2ticks", "mins2ticks() returns ticks for minutes" ,Mins2Ticks.class);
        put("realxp", "realxp(skillid, exp) returns the real xp gained for a set xp, so if you put 20,000 xp in there it will modify it by the current xp rate so its accurate.", RealXP.class);
        dumpFunctions();
    }

    /**
     * Method getCaret
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static int getCaret() {
        return caret;
    }

    /**
     * Method setCaret
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param caret
     */
    public static void setCaret(int caret) {
        ReturnOperationMap.caret = caret;
    }

    /**
     * Method getInformation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static String[] getInformation() {
        return information;
    }

    /**
     * Method setInformation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param information
     */
    public static void setInformation(String[] information) {
        ReturnOperationMap.information = information;
    }

    /**
     * Method getNameMap
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static HashMap<String, Integer> getNameMap() {
        return nameMap;
    }

    /**
     * Method setNameMap
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param nameMap
     */
    public static void setNameMap(HashMap<String, Integer> nameMap) {
        ReturnOperationMap.nameMap = nameMap;
    }

    /**
     * Method getCalls
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static ReturnFunction[] getCalls() {
        return calls;
    }

    /**
     * Method setCalls
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param calls
     */
    public static void setCalls(ReturnFunction[] calls) {
        ReturnOperationMap.calls = calls;
    }

    /**
     * Method put
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param information
     * @param c
     */
    static void put(String name, String information, Class c) {
        try {
            nameMap.put(name, caret);
            ReturnOperationMap.information[caret] = information;
            calls[caret++]                   = (ReturnFunction) c.newInstance();
        } catch (Exception ignored) {}
    }

    /**
     * Method getCall
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public static ReturnFunction getCall(String name) {
        try {
            return calls[nameMap.get(name)];
        } catch (Exception ee) {}

        return null;
    }

    /**
     * Method dumpFunctions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void dumpFunctions() {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("config/SCRIPT_EVAL_FUNCTIONS_READ_ME.txt"));

            for (String key : nameMap.keySet()) {
                writer.write("-----------------------------------------------");
                writer.newLine();
                writer.write("METHOD: " + key);
                writer.newLine();
                writer.write("OPCODE: " + nameMap.get(key));
                writer.newLine();
                writer.write("Usage/Information: " + information[nameMap.get(key)]);
                writer.newLine();
                writer.write("-----------------------------------------------");
            }

            writer.close();
        } catch (Exception ee) {}
    }
}
