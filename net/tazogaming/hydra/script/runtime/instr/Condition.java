package net.tazogaming.hydra.script.runtime.instr;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.Parameter;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptConditionMap;
import net.tazogaming.hydra.script.runtime.FunctionNotFoundException;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptParseException;
import net.tazogaming.hydra.script.build.ScriptCompiler;
import net.tazogaming.hydra.script.runtime.instr.condition.JSCondition;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 10/10/13
 * Time: 12:35
 */
public class Condition {
    public static final int
        TYPE_OR                                                                            = 0,
        TYPE_AND                                                                           = 1;
    boolean                                                                          isNot = false;
    private String[]                                                                 params;
    private String                                                                   name;
    private int                                                                      type;
    private InternalCondition condition;
    private Parameter[]                                                              npcParams;
    private net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptCondition npcCondtion;
    private String                                                                   shallowName;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param not
     * @param name
     * @param parameters
     */
    public Condition(int type, boolean not, String name, Parameter[] parameters) {
        this.type        = type;
        this.isNot       = not;
        this.npcCondtion = ScriptConditionMap.getCondition(name);

        if (npcCondtion == null) {
            throw new FunctionNotFoundException(name);
        }

        this.npcParams = parameters;

        if (this.params == null) {
            params = new String[1];
        }

        this.name        = name;
        this.shallowName = name;    // .substring(2);
    }

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param not
     * @param name
     * @param parameters
     */
    public Condition(int type, boolean not, String name, String[] parameters) {
        this.type      = type;
        this.isNot     = not;
        this.condition = ConditionMap.get(name);

        if (condition == null) {
            condition = ConditionMap.get(name.substring(2));

            if (condition == null) {
                condition = new JSCondition();
            }
        }

        if (name.equalsIgnoreCase("enum_get")) {
            try {
                parameters[0] = Integer.toString(ScriptCompiler.enum_ids.get(parameters[0]));
            } catch (Exception ee) {
                throw new ScriptParseException("Enum not found: " + parameters[0]);
            }
        }

        this.params = parameters;

        if (this.params == null) {
            params = new String[1];
        }

        this.name        = name;
        this.shallowName = name;    // .substring(2);

        if (condition instanceof JSCondition) {
            String[] tmp = new String[params.length + 1];

            tmp[0] = this.shallowName;

            for (int i = 1; i < tmp.length; i++) {
                tmp[i] = params[i - 1];
            }

            this.params = tmp;
        }
    }

    /**
     * Method getParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getParams() {
        return this.params;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Method getCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptCondition getCondition() {
        return npcCondtion;
    }

    /**
     * Method getReturnValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param script
     *
     * @return
     */
    public boolean getReturnValue(Player pla, Scope script) {
        return false;
    }

    /**
     * Method getReturnValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param script
     *
     * @return
     */
    public boolean getReturnValue(NPC npc, NpcScriptScope script) {
        return npcCondtion.onCondition(npc, new ParameterParser(npcParams), script);
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method getNpcParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Parameter[] getNpcParams() {
        return npcParams;
    }

    /**
     * Method setNpcParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npcParams
     */
    public void setNpcParams(Parameter[] npcParams) {
        this.npcParams = npcParams;
    }

    /**
     * Method isNot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isNot() {
        return isNot;
    }

    /**
     * Method setNot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param not
     */
    public void setNot(boolean not) {
        isNot = not;
    }
}
