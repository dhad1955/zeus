package net.tazogaming.hydra.script.runtime.instr.condition;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptCondition;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/04/2015
 * Time: 22:11
 */
public class IfDBox implements InternalCondition {
    /**
     * Method onCondition
     * Created on 14/08/18
     *
     * @param npc
     * @param parser
     * @param script
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */

    /**
     * Method onCondition
     * Created on 14/08/18
     *
     * @param pla
     * @param parser
     * @param curScript
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        long l = pla.getAccount().dboxTime;
        if(l == 0 || System.currentTimeMillis() - l > parser.nextInt()){
            pla.getAccount().dboxTime = Core.currentTimeMillis();
            return true;
        }else{
            return false;
        }
    }
}
