package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.ScriptEnumerator;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.*;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/07/14
 * Time: 21:18
 */
public class EnumGet implements InternalCondition {
    public static long[] TYPES = new long[30];

    static {
        for (int i = 0; i < 30; i++) {
            TYPES[i] = Text.stringToLong("type" + i);
        }
    }

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param parser
     * @param curScript
     *
     * @return
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        int      id   = parser.nextInt();
        int[]    args = parser.remainingIntegers();
        ScriptEnumerator type = World.getWorld().getScriptManager().getEnum(id, args);

        if (type != null) {
            for (int i = 0; i < type.getData().length; i++) {
                curScript.replaceOrDeclare(TYPES[i], type.getData()[i]);
            }

            return true;
        }

        return false;
    }
}
