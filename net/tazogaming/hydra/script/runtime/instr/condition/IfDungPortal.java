package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.*;
import net.tazogaming.hydra.game.skill.dungeoneering.Dungeon;
import net.tazogaming.hydra.game.skill.dungeoneering.FloorHandler;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/12/13
 * Time: 19:25
 */
public class IfDungPortal implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param parser
     * @param curScript
     *
     * @return
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        if (!(pla.getCurrentInstance() instanceof Dungeon)) {
            return false;
        }

        if (!pla.getAccount().hasVar("dung_floor")) {
            return false;
        }

        int mode = parser.nextInt();

        if (mode == 0) {
            if (pla.getAccount().getInt32("dung_floor") == 0) {
                pla.getActionSender().sendMessage("Floor not available.");

                return false;
            }
        }

        if (mode == 1) {
            Dungeon      d = (Dungeon) pla.getCurrentInstance();
            FloorHandler h = d.getFloor(pla.getAccount().getInt32("dung_floor"));

            return h.isCompleted(pla);
        }

        return false;
    }
}
