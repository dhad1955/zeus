package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.slayer.SlayerTask;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.*;
import net.tazogaming.hydra.game.skill.slayer.SlayKillable;
import net.tazogaming.hydra.game.skill.slayer.Slayer;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Collection;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 24/10/13
 * Time: 07:07
 */
public class IfSlayerTask implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(Player player, ExpressionParser parser, Scope script) {
        int ids        = parser.nextInt();
        int min = parser.nextInt();
        int max_amount = parser.nextInt();
        int avoid      = 0;

        if (parser.hasNext()) {
            avoid = parser.nextInt();
        }

        int[] slayer_table = Slayer.get_table(ids);

        if (slayer_table == null) {
            return false;
        }

        ArrayList<SlayKillable> possible_tasks = new ArrayList<SlayKillable>();


        for (int i = 0; i < slayer_table.length; i++) {
            SlayKillable K = Slayer.get_slayer_npc_info(slayer_table[i]);

            if ((K == null) || (K.getLevelRequired() > player.getCurStat(18))) {
                continue;
            }

            if ((avoid != 0) && (K.getNpcIds()[0] == avoid)) {
                continue;
            }

            possible_tasks.add(K);
        }

        if(player.getAccount().hasVar("ss_dodge"))
            possible_tasks.removeAll((Collection<?>) player.getAccount().getVar("ss_dodge").getFullData());

        if (possible_tasks.size() == 0) {
            return false;
        }

        SlayKillable ran       = possible_tasks.get(GameMath.rand3(possible_tasks.size()));
        int          ranAmount = min +  GameMath.roundTen(GameMath.rand(max_amount));

        if (ranAmount == 0) {
            ranAmount = 25;
        }

        if (parser.hasNext()) {
            if (player.getChannel() != null) {
                player.getChannel().setSlayerTask(ran.getNpcIds()[0], ranAmount);

                return true;
            } else {
                return false;
            }
        }

        if(player.getSlayerTask() != null && player.getSlayerTask().isDuoTask())
            player.getSlayerTask().leaveTask(player);

        player.setSlayerTask(new SlayerTask(player, ran.getNpcIds()[0], ranAmount));

        return true;
    }
}
