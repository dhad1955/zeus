package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 01:39
 */
public class IfObjects implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param parser
     * @param curScript
     *
     * @return
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        int clip = ClippingDecoder.getClipping(pla.getX(), pla.getY(), pla.getHeight());

        if ((clip & 256) != 0) {
            return true;
        }

        return (World.getWorld().getTile(pla.getLocation()).hasObjects());
    }
}
