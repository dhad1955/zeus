package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.FunctionNotFoundException;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.script.runtime.expr.*;
import net.tazogaming.hydra.script.runtime.js.JPlugin;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/01/14
 * Time: 21:17
 */
public class JSCondition implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param parser
     * @param curScript
     *
     * @return
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        try {
            String function = parser.nextString();

            if (ScriptRuntime.GLOBAL_SCOPE.hasFunction(function)) {
                return ScriptRuntime.GLOBAL_SCOPE.getBoolean(pla, curScript, function, parser.remainingObjects());
            }

            for (JPlugin p : curScript.getPlugins()) {
                if (p.hasFunction(function)) {
                    return p.getBoolean(pla, curScript, function, parser.remainingObjects());
                }
            }

            throw new FunctionNotFoundException(function);
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        return false;
    }
}
