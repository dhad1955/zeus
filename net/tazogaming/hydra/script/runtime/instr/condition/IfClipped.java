package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.ai.movement.Movement;
import net.tazogaming.hydra.map.ClippingDecoder;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/10/13
 * Time: 01:41
 */
public class IfClipped implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param parser
     * @param curScript
     *
     * @return
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        return getMovementStatus(parser.nextInt(), pla.getX(), pla.getY(), pla.getHeight()) == 0;
    }

    /**
     * Method getMovementStatus
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param direction
     * @param tileX
     * @param tileY
     * @param height
     *
     * @return
     */
    public static int getMovementStatus(int direction, int tileX, int tileY, int height) {
        switch (direction) {
        case Movement.NORTH :
            if (ClippingDecoder.blockedNorth(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH :
            if (ClippingDecoder.blockedSouth(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.EAST :
            if (ClippingDecoder.blockedEast(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.WEST :
            if (ClippingDecoder.blockedWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.NORTH_EAST :
            if (ClippingDecoder.blockedNorthEast(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.NORTH_WEST :
            if (ClippingDecoder.blockedNorthWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH_WEST :
            if (ClippingDecoder.blockedSouthWest(tileX, tileY, height)) {
                return 0;
            }

            break;

        case Movement.SOUTH_EAST :
            if (ClippingDecoder.blockedSouthEast(tileX, tileY, height)) {
                return 0;
            }

            break;
        }

        return 1;
    }
}
