package net.tazogaming.hydra.script.runtime.instr.condition;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/08/14
 * Time: 13:26
 */
public class IfTaskExists implements InternalCondition {
    /**
     * Method onCondition
     * Created on 14/08/18
     *
     * @param pla
     * @param parser
     * @param curScript
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        return pla.getScriptedTaskScheduler().getByName(parser.nextString()) != null;
    }
}
