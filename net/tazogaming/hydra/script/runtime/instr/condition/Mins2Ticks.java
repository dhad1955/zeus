package net.tazogaming.hydra.script.runtime.instr.condition;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/01/2015
 * Time: 17:33
 */
public class Mins2Ticks implements ReturnFunction {
    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param player
     * @param scope
     * @param tokenizer
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
        return Core.getTicksForMinutes(tokenizer.nextInt());
    }
}
