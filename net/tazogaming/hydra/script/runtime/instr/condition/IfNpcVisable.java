package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Mob;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.game.skill.combat.Combat;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 14:54
 */
public class IfNpcVisable implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param parser
     * @param curScript
     *
     * @return
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        int npcId = parser.nextInt();

        if (curScript.getInteractingNpc(npcId) == null) {
            return false;
        }

        NPC     npc        = curScript.getInteractingNpc(npcId);
        int     mode       = parser.nextInt();
        int     range      = parser.nextInt();
        boolean ignoreNpcs = parser.nextByte() == 1;

        if (mode == 0) {    // north south east west
            if ((npc.getLastKnownDir() == Mob.NORTH) || (npc.getLastKnownDir() == Mob.SOUTH)
                    || (npc.getLastKnownDir() == Mob.WEST) || (npc.getLastKnownDir() == Mob.EAST)) {
                return Combat.clearPath(npc.getLocation(), getPrefferedLocation(npc, pla, range));
            }
        } else if (mode == 1) {
            Combat.clearPath(npc.getLocation(), getPrefferedLocation(npc, pla, range));
        } else if (mode == 2) {
            return Combat.clearPath(npc.getLocation(), pla.getLocation());
        }

        return false;
    }

    /**
     * Method getPrefferedLocation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param following
     * @param range
     *
     * @return
     */
    public Point getPrefferedLocation(NPC npc, Player following, int range) {
        int dx = 0,
            dy = 0;

        switch (npc.getLastKnownDir()) {
        case Mob.NORTH :
            dy = -range;

            break;

        case Mob.EAST :
            dx = -range;

            break;

        case Mob.NORTH_EAST :
            dx = -range;
            dy = -range;

            break;

        case Mob.SOUTH_EAST :
            dy = range;
            dx = -range;

            break;

        case Mob.WEST :
            dx = range;

            break;

        case Mob.NORTH_WEST :
            dx = range;
            dy = -range;

            break;

        case Mob.SOUTH :
            dy = range;

            break;

        case Mob.SOUTH_WEST :
            dx = range;
            dy = range;

            break;
        }

        return Point.location(following.getX() + dx, following.getY() + dy, following.getLocation().getHeight());
    }
}
