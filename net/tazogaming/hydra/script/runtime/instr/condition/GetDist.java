package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Entity;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptParseException;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ReturnFunction;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 28/08/14
 * Time: 02:31
 */
public class GetDist implements ReturnFunction {

    /**
     * Method eval
     * Created on 14/08/18
     *
     * @param player
     * @param scope
     * @param tokenizer
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    private Point getFromDigits(Object first, ExpressionParser parser, Player player) {
        try {
            return Point.location((Integer) first, parser.nextInt(), player.getHeight());
        } catch (ClassCastException ee) {
            throw new ScriptParseException("Error with getDist() expecting x/y but parser through this error: "
                                           + ee.getClass().getName() + ": " + ee.getMessage());
        }
    }

    private Point extractFromEntity(Entity i) {
        return i.getLocation();
    }

    private Point[] getPoints(ExpressionParser parser, Player player) {
        Point[] points = new Point[2];
        Object  obj    = null;

        for (int i = 0; i < 2; i++) {
            obj = parser.next();

            if (obj instanceof Integer) {
                points[i] = getFromDigits(obj, parser, player);
            } else {
                points[i] = extractFromEntity((Entity) obj);
            }
        }

        return points;
    }

    /**
     * Method eval
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param scope
     * @param tokenizer
     *
     * @return
     */
    @Override
    public Object eval(Player player, Scope scope, ExpressionParser tokenizer) {
        Point[] pts = getPoints(tokenizer, player);

        return Point.getDistance(pts[0], pts[1]);
    }
}
