package net.tazogaming.hydra.script.runtime.instr.condition;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.util.Text;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;

import java.sql.ResultSet;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 26/09/14
 * Time: 12:58
 */
public class IfReferralCheck implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     *
     * @param pla
     * @param parser
     * @param curScript
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean onCondition(final  Player pla, ExpressionParser parser, final  Scope curScript) {
            final String referralName = parser.nextString().replace("'", "");
        if(referralName.equalsIgnoreCase(pla.getUsername())){
            return false;
        }
        curScript.waitFor(Scope.WAIT_TYPE_CHILD_FINISH);

        World.getWorld().getBackgroundService().submitRequest(new BackgroundServiceRequest() {
            @Override
            public boolean compute() {
                try {
                    if (curScript.isTerminated())
                        return true;

                    int plrId = 0;
                    ResultSet r1 = World.getWorld().getDatabaseLink().query("SELECT member_id FROM members where username='" + referralName + "'");
                    if(!r1.next()){  // member does not exist
                        r1.close();

                        curScript.declare(Text.stringToLong("ref_status"), new ScriptVar(Text.stringToLong("ref_status"), 0));
                        return true;

                    }
                    plrId = r1.getInt("member_id");

                    ResultSet r = World.getWorld().getDatabaseLink()
                            .query("SELECT uid FROM members where uid='" + pla.getUID() + "'");  // user already found w/ this referral
                    if (r.next()) {
                        r.close();
                        curScript.declare(Text.stringToLong("ref_status"), new ScriptVar(Text.stringToLong("ref_status"), 1));
                        return true;
                    }

                    // same as above, referall already in progress.
                    r = World.getWorld().getDatabaseLink().query("SELECT uid FROM referrals WHERE uid='" + pla.getUID() + "'");
                    if (r.next()) {
                        r.close();
                        curScript.declare(Text.stringToLong("ref_status"), new ScriptVar(Text.stringToLong("ref_status"), 1));
                        return true;
                    }
                    curScript.declare(Text.stringToLong("ref_status"), new ScriptVar(Text.stringToLong("ref_status"), 2));

                    World.getWorld().getDatabaseLink().query("INSERT into referrals(id, uid, userid, refer_id, status) VALUES(null, " + pla.getId() + ", " + plrId + ", 0);");
                    curScript.release(Scope.WAIT_TYPE_CHILD_FINISH);
                    return true;
                } catch (Exception ee) {
                    ee.printStackTrace();

                }
                return true;
            }

        });
        return false;
    }
}
