package net.tazogaming.hydra.script.runtime.instr.condition;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.condition.ConditionalCall;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;

/**
 * Created by Dan on 30/12/2015.
 */
public class BitSet implements InternalCondition {
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        return  ((parser.nextInt()>>parser.nextInt()) % 2 != 0);
    }
}
