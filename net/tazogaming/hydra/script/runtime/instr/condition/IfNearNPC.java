package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/10/13
 * Time: 14:34
 */
public class IfNearNPC implements InternalCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param parser
     * @param curScript
     *
     * @return
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        int npcId       = parser.nextInt();
        int range       = parser.nextInt();
        int identifier  = parser.nextInt();
        int lowestRange = 0;
        NPC lowestNpc   = null;

        for (NPC npc : pla.getWatchedNPCs().getKnownEntities()) {
            if ((npc.getId() == npcId) &&!npc.isBusy(pla) && (npc.getCurrentController() != pla)) {
                int npc_range = Point.getDistance(npc.getLocation(), pla.getLocation());    // <= range){

                if (!npc.isWithinDistance(pla, range)) {
                    continue;
                }

                for (NPC npc2 : curScript.getInteractingNpcs()) {
                    if (npc2 == npc) {
                        continue;
                    }
                }

                if ((lowestNpc == null) || (lowestRange > npc_range)) {
                    lowestNpc   = npc;
                    lowestRange = npc_range;
                }
            }
        }

        if (lowestNpc != null) {
            lowestNpc.setCurrentController(pla);
        }

        curScript.setInteractingNPC(lowestNpc, identifier);

        return lowestNpc != null;
    }
}
