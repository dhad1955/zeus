package net.tazogaming.hydra.script.runtime.instr.condition;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.npc.NPC;
import net.tazogaming.hydra.entity3d.npc.ai.script.NpcScriptScope;
import net.tazogaming.hydra.entity3d.npc.ai.script.ParameterParser;
import net.tazogaming.hydra.entity3d.npc.ai.script.condition.ScriptCondition;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.map.Region;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/11/13
 * Time: 06:25
 */
public class IfNpcNear implements ScriptCondition {

    /**
     * Method onCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param npc
     * @param parser
     * @param script
     *
     * @return
     */
    @Override
    public boolean onCondition(NPC npc, ParameterParser parser, NpcScriptScope script) {
        Region[] regions = World.getWorld().getRegionManager().getSurroundingRegions(npc.getLocation());
        int      npcId   = parser.nextInt();
        int      range   = parser.nextInt();

        for (Region r : regions) {
            if (r != null) {
                for (NPC parse : r.getNpcs()) {
                    if ((parse.getId() == npcId)
                            && (Point.getDistance(npc.getLocation(), parse.getLocation()) <= range)) {
                        script.setInteractingNPC(0, parse);

                        return true;
                    }
                }
            }
        }

        return false;    // To change body of implemented methods use File | Settings | File Templates.
    }
}
