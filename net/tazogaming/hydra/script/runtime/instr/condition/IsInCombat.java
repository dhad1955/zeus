package net.tazogaming.hydra.script.runtime.instr.condition;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 02/11/2014
 * Time: 13:31
 */
public class    IsInCombat implements InternalCondition {
    /**
     * Method onCondition
     * Created on 14/08/18
     *
     * @param pla
     * @param parser
     * @param curScript
     * @return
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public boolean onCondition(Player pla, ExpressionParser parser, Scope curScript) {
        if(Core.currentTime - pla.getLastHit() < 30 || pla.getCombatAdapter().isInCombat())
            return true;
        return false;
    }
}
