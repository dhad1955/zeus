package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.build.SourcePosition;
import net.tazogaming.hydra.script.build.lexer.Token;//~--- JDK imports ------------------------------------------------------------


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 16/06/14
 * Time: 12:04
 */
public class ScriptParseException extends RuntimeException {
    private SourcePosition pos;

    /**
     * Constructs ...
     *
     *
     * @param message
     */
    public ScriptParseException(String message) {
        super(message);
    }

    /**
     * Constructs ...
     *
     *
     * @param message
     * @param position
     */
    public ScriptParseException(String message, SourcePosition position) {
        super("PARSE ERROR: " + message+" "+(position != null ? position : ""));
        this.pos = position;
        printStackTrace();
    }

    /**
     * Constructs ...
     *
     *
     * @param message
     * @param current
     */
    public ScriptParseException(String message, Token current) {
        this(message, current.position());
    }

    /**
     * Method position
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SourcePosition position() {
        return pos;
    }
}
