package net.tazogaming.hydra.script.runtime.js;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;

//~--- JDK imports ------------------------------------------------------------

import javax.script.*;
import java.io.File;
import java.io.FileReader;

import java.util.ArrayList;
import java.util.List;


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/01/14
 * Time: 21:05
 */
public class JPlugin {

    public static final String[] DISABLED_PATTERNS = {
    };


    private ArrayList<String> functionList = new ArrayList<String>();
    private CompiledScript script;
    private String            fileName;

    /**
     * Constructs ...
     *
     *
     * @param file
     */
    public JPlugin(File file) {
        fileName = file.getName();

        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine  = manager.getEngineByExtension("js");
        Compilable c       = (Compilable) engine;

        try {
            script = c.compile(new FileReader(file));
            script.eval();

            for (String str : engine.getBindings(ScriptContext.ENGINE_SCOPE).keySet()) {
                functionList.add(str);
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    /**
     * Constructs ...
     *
     *
     * @param script
     */
    public JPlugin(String script) {
        for(String str : DISABLED_PATTERNS) {
            if(script.contains(str)){
                throw new RuntimeException("Error malicious code found in javascript file");
            }
        }
        this.data = script;
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine        engine  = manager.getEngineByExtension("js");

        if (script.contains("getRawPassword")) {
            return;
        }

        Compilable c = (Compilable) engine;

        try {
            engine.eval(new FileReader("config/scripts/core.js"));
            this.script = c.compile(script);
            this.script.eval();

            for (String str : engine.getBindings(ScriptContext.ENGINE_SCOPE).keySet()) {
                functionList.add(str);
            }
        } catch (Exception ee) {
            throw new RuntimeException("[Javascript error]: " + ee.getMessage());
        }
    }

    public String data;

    /**
     * Method hasFunction
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     *
     * @return
     */
    public boolean hasFunction(String str) {
        return functionList.contains(str);
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param runtime
     * @param functionName
     * @param params
     *
     * @return
     *
     * @throws NoSuchMethodException
     * @throws ScriptException
     */
    public Object get(Player player, Scope runtime, String functionName, Object... params)
            throws ScriptException, NoSuchMethodException {
        Bindings bindings = script.getEngine().createBindings();

        bindings.put("player", player);
        bindings.put("runtime", runtime);
        script.getEngine().getBindings(ScriptContext.ENGINE_SCOPE).put("player", player);
        script.getEngine().getBindings(ScriptContext.ENGINE_SCOPE).put("runtime", runtime);
        script.eval();
        script.getEngine().getFactory().getNames();

        Invocable inv = (Invocable) script.getEngine();

        return inv.invokeFunction(functionName, params);
    }

    public List<String> getFunctions() {
        return functionList;
    }

    /**
     * Method getBoolean
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param runtime
     * @param functionName
     * @param params
     *
     * @return
     *
     * @throws NoSuchMethodException
     * @throws ScriptException
     */
    public boolean getBoolean(Player player, Scope runtime, String functionName, Object... params)
            throws ScriptException, NoSuchMethodException {
        return (Boolean) get(player, runtime, functionName, params);
    }

    /**
     * Method getInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param runtime
     * @param functionName
     * @param params
     *
     * @return
     *
     * @throws NoSuchMethodException
     * @throws ScriptException
     */
    public int getInt(Player player, Scope runtime, String functionName, Object... params)
            throws ScriptException, NoSuchMethodException {
        return (Integer) get(player, runtime, functionName, params);
    }

    /**
     * Method getString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param runtime
     * @param functionName
     * @param params
     *
     * @return
     *
     * @throws NoSuchMethodException
     * @throws ScriptException
     */
    public String getString(Player player, Scope runtime, String functionName, Object... params)
            throws ScriptException, NoSuchMethodException {
        return get(player, runtime, functionName, params).toString();
    }
}
