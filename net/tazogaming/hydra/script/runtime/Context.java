package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.data.ScriptVar;
import net.tazogaming.hydra.script.runtime.runnable.SignedBlock;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/08/14
 * Time: 21:52
 */
public class Context {
    private SignedBlock[] functions       = new SignedBlock[20];
    private int           variablePointer = 0;
    private ScriptVar[]   variables       = new ScriptVar[30];
    private int           functionName;
    private String        name;
    private int           functionPointer;
    private Scope scope = new Scope();
    long lastVarDeclared = 0;

    boolean varDeclared = false;

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param totalBlocks
     */
    public Context(String name, int totalBlocks) {
        this.name      = name;
        this.functions = new SignedBlock[totalBlocks];
    }

    /**
     * Method addVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param var
     */
    public void addVar(ScriptVar var) {
        this.scope.declareVar(var.getHash(), var);
        varDeclared = true;
        lastVarDeclared = var.getHash();
        if(Text.nameForLong(var.getHash()).equals("invalid_name"))
            throw new RuntimeException();

    }


    public SignedBlock getBlockByName(String context){
        for(int i = 0; i < functions.length; i++){
            if(functions[i] == null)
                return null;
            else if(functions[i].getName().equalsIgnoreCase(context))
                return functions[i];
        }
        return null;
    }
    /**
     * Method getBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     *
     * @return
     */
    public SignedBlock getBlock(int id) {
        return this.functions[id];
    }

    /**
     * Method getVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hash
     *
     * @return
     */
    public ScriptVar getVar(long hash) {
       try {
        return scope.getVar(hash);
       }catch (Exception ee) {
           ee.printStackTrace();
       }
        return null;
    }

    public Scope getScope() {
        return scope;
    }

    /**
     * Method setBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param integer
     * @param block
     */
    public void setBlock(int integer, SignedBlock block) {
        this.functions[integer] = block;
    }
}
