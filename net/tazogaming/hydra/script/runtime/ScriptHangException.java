package net.tazogaming.hydra.script.runtime;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 12/09/14
 * Time: 17:05
 */
public class ScriptHangException extends RuntimeException {

    public ScriptHangException(String error) {
        super(error);
    }
}
