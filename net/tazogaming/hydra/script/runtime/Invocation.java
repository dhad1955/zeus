package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.js.JPlugin;

//~--- JDK imports ------------------------------------------------------------

import java.lang.reflect.Method;

import java.util.Arrays;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/11/13
 * Time: 23:57
 */
public class Invocation {
    private Integer[] params;
    private Class[]   ref_type;
    private String    method_name;

    /**
     * Constructs ...
     *
     *
     * @param parameters
     * @param method_name
     */
    public Invocation(Integer[] parameters, String method_name) {
        params           = parameters;
        this.method_name = method_name;
        ref_type         = new Class[parameters.length];
        Arrays.fill(ref_type, Integer.TYPE);
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param script
     *
     * @return
     */
    public String eval(Player player, Scope script) {
        try {
            if (ScriptRuntime.GLOBAL_SCOPE.hasFunction(method_name)) {
                return ScriptRuntime.GLOBAL_SCOPE.getString(player, script, method_name, params);
            }

            if (script.getPlugins().size() > 0) {
                for (JPlugin p : script.getPlugins()) {
                    if (p == null) {
                        continue;
                    }

                    if (p.hasFunction(method_name)) {
                        return p.getString(player, script, method_name, params);
                    }
                }
            }

            Method m = player.getFunctions().getClass().getDeclaredMethod(method_name, ref_type);

            return m.invoke(player.getFunctions(), params).toString();
        } catch (Exception ee) {
            ee.printStackTrace();

            return "null";
        }
    }
}
