package net.tazogaming.hydra.script.runtime;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 31/08/14
 * Time: 22:22
 */
public class ScriptSuspension {

    public ScriptSuspension(int triggerType, int argumentHash, int delay) {
        this.triggerType = triggerType;
        this.argumentHash = argumentHash;
        this.delay = delay;
    }



    public int getTriggerType() {
        return triggerType;
    }

    public void setTriggerType(int triggerType) {
        this.triggerType = triggerType;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public int getArgumentHash() {
        return argumentHash;
    }


    private int triggerType;
    private int argumentHash;
    private int delay;

}
