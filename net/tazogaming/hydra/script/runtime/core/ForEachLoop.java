package net.tazogaming.hydra.script.runtime.core;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.expr.Node;
import net.tazogaming.hydra.script.runtime.runnable.BlockNode;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 14/08/14
 * Time: 19:12
 */
public class ForEachLoop {
    private BlockNode  block;
    private long       varName;
    private Node evaluation;

    /**
     * Constructs ...
     *
     *
     * @param block
     * @param varName
     * @param evaluation
     */
    public ForEachLoop(BlockNode block, long varName, Node evaluation) {
        this.block      = block;
        this.varName    = varName;
        this.evaluation = evaluation;
    }

    /**
     * Method getVarName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getVarName() {
        return varName;
    }

    /**
     * Method setVarName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param varName
     */
    public void setVarName(long varName) {
        this.varName = varName;
    }

    /**
     * Method getBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public BlockNode getBlock() {
        return block;
    }

    /**
     * Method setBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param block
     */
    public void setBlock(BlockNode block) {
        this.block = block;
    }

    /**
     * Method getIterationEvaluation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Node getIterationEvaluation() {
        return this.evaluation;
    }
}
