package net.tazogaming.hydra.script.runtime.core;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.core.condition.impl.ConditionalEvaluator;
import net.tazogaming.hydra.script.runtime.expr.Node;
import net.tazogaming.hydra.script.runtime.runnable.ScriptRunnable;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 14:52
 */
public class ForLoop {
    private long                 initializationVar;
    private ConditionalEvaluator conditional;
    private RuntimeInstruction afterThought;
    private ScriptRunnable       codeBlock;
    private Node initializationNode;

    /**
     * Constructs ...
     *
     *
     * @param initializationVar
     * @param initNode
     * @param conditional
     * @param afterThought
     * @param codeBlock
     */
    public ForLoop(String initializationVar, Node initNode, ConditionalEvaluator conditional,
                   RuntimeInstruction afterThought, ScriptRunnable codeBlock) {
        this.initializationVar        = Text.stringToLong(initializationVar);
        this.conditional              = conditional;
        this.afterThought             = afterThought;
        this.initializationNode = initNode;
        this.codeBlock                = codeBlock;
    }

    /**
     * Method getConditional
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ConditionalEvaluator getConditional() {
        return conditional;
    }

    /**
     * Method setConditional
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param conditional
     */
    public void setConditional(ConditionalEvaluator conditional) {
        this.conditional = conditional;
    }

    /**
     * Method getAfterThought
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RuntimeInstruction getAfterThought() {
        return afterThought;
    }

    /**
     * Method setAfterThought
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param afterThought
     */
    public void setAfterThought(RuntimeInstruction afterThought) {
        this.afterThought = afterThought;
    }

    /**
     * Method getCodeBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptRunnable getCodeBlock() {
        return codeBlock;
    }

    /**
     * Method setCodeBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param codeBlock
     */
    public void setCodeBlock(ScriptRunnable codeBlock) {
        this.codeBlock = codeBlock;
    }

    /**
     * Method getInitializationVar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getInitializationVar() {
        return initializationVar;
    }

    /**
     * Method getInitializationExpression
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Node getInitializationNode() {
        return initializationNode;
    }
}
