package net.tazogaming.hydra.script.runtime.core;

//~--- non-JDK imports --------------------------------------------------------

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 30/09/13
 * Time: 00:01
 */
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.Condition;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.build.SourcePosition;
import net.tazogaming.hydra.script.runtime.core.condition.impl.ConditionalEvaluator;
import net.tazogaming.hydra.script.runtime.expr.Node;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptInstructionContext;
import net.tazogaming.hydra.script.runtime.instr.op.ScriptStatus;
import net.tazogaming.hydra.script.runtime.instr.op.impl.RuntimeOperationMap;
import net.tazogaming.hydra.script.runtime.runnable.BlockNode;
import net.tazogaming.hydra.script.runtime.runnable.SwitchBlock;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;

/**
 * Rune War Game Server
 * http://hydra.tazogaming.org
 * Builder {$build}
 * net.tazo_.server.script
 *
 * @author Gander
 *         Date: 13-Dec-2009 {16:55:54}
 */
class ArrayResult {
    int    id;
    String match;
}


/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class RuntimeInstruction {
    public static final byte
        NONE                                = 0,
        TRUE                                = 1,
        FALSE                               = 2;
    public static final int
        OBJ_X                               = 0,
        OBJ_Y                               = 1,
        POINTS                              = 2,
        SLAYCOUNT                           = 3,
        SLAYKILLS                           = 4,
        NAME                                = 5,
        ITEM_COUNT                          = 6;


    public long getExternalIdentifier() {
        return externalIdentifier;
    }

    public void setExternalIdentifier(long externalIdentifier) {
        this.externalIdentifier = externalIdentifier;
    }

    private long externalIdentifier = 0;
    private ArrayList<Condition> conditions = new ArrayList<Condition>();
    public String                funcName   = "";
    public Object[]              params;
    public byte                  type;
    public InternalCondition internalCondition;
    public int[]                 offsetsReplace;
    public int[]                 idOffsetReplace;
    private SourcePosition position;
    private SwitchBlock          switchBlock;
    private ForLoop forLoop;
    private ConditionalEvaluator condtionalCode;
    private Node[]         formattedParams;
    private ScriptInstructionContext operation;
    private BlockNode            conditionalBlock;
    private ForEachLoop forEachLoop;

    /**
     * Constructs ...
     *
     *
     * @param builder
     */
    public RuntimeInstruction(ConditionalEvaluator builder) {
        condtionalCode = builder;
    }

    /**
     * Constructs ...
     *
     *
     * @param pos
     */
    public RuntimeInstruction(SourcePosition pos) {
        this.position = pos;
    }

    /**
     * Constructs ...
     *
     *
     * @param builder
     * @param node
     */
    public RuntimeInstruction(ConditionalEvaluator builder, net.tazogaming.hydra.script.runtime.runnable.BlockNode node) {
        condtionalCode        = builder;
        this.conditionalBlock = node;
    }

    /**
     * Constructs ...
     *
     *
     * @param l
     * @param pos
     */
    public RuntimeInstruction(ForEachLoop l, SourcePosition pos) {
        this.forEachLoop = l;
        this.position    = pos;
        this.operation   = RuntimeOperationMap.getByName("foreach");
    }

    /**
     * Constructs ...
     *
     *
     * @param l
     * @param pos
     */
    public RuntimeInstruction(ForLoop l, SourcePosition pos) {
        this.forLoop   = l;
        this.position  = pos;
        this.operation = RuntimeOperationMap.getByName("for");
    }

    /**
     * Constructs ...
     *
     *
     * @param funcName
     * @param params
     */
    public RuntimeInstruction(String funcName, String... params) {}

    /**
     * Constructs ...
     *
     *
     * @param operation
     * @param type
     * @param name
     * @param params
     */
    public RuntimeInstruction(ScriptInstructionContext operation, int type, String name, Node... params) {
        if (operation == null) {
            throw new RuntimeException("Error method does not exist: " + name);
        }



        this.params          = params;
        this.operation       = operation;
        this.funcName        = name;
        this.type            = (byte) type;
        this.formattedParams = params;
    }

    /**
     * Constructs ...
     *
     *
     * @param operation
     * @param type
     * @param name
     * @param params
     */
    public RuntimeInstruction(ScriptInstructionContext operation, int type, String name, String... params) {
        if (operation == null) {
            throw new RuntimeException("Error method does not exist: " + name);
        }

        this.params    = params;
        this.operation = operation;
        this.funcName  = name;
        this.type      = (byte) type;
    }

    /**
     * Constructs ...
     *
     *
     * @param operation
     * @param type
     * @param name
     * @param npc
     * @param params
     */
    public RuntimeInstruction(ScriptInstructionContext operation, int type, String name, boolean npc, String... params) {
        if (operation == null) {
            throw new RuntimeException("Error method does not exist: " + name);
        }

        this.params    = params;
        this.operation = operation;
        this.funcName  = name;
        this.type      = (byte) type;
    }

    /**
     * Constructs ...
     *
     *
     * @param pos
     * @param operation
     * @param type
     * @param name
     * @param params
     */
    public RuntimeInstruction(SourcePosition pos, ScriptInstructionContext operation, int type, String name,
                              Node... params) {
        this(operation, type, name, params);
        this.position = pos;
    }

    /**
     * Constructs ...
     *
     *
     * @param pos
     * @param operation
     * @param type
     * @param name
     * @param params
     */
    public RuntimeInstruction(SourcePosition pos, ScriptInstructionContext operation, int type, String name,
                              String... params) {
        this(operation, type, name, params);
        this.position = pos;
    }

    /**
     * Method getForEachLoop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ForEachLoop getForEachLoop() {
        return forEachLoop;
    }

    /**
     * Method setForEachLoop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param forEachLoop
     */
    public void setForEachLoop(ForEachLoop forEachLoop) {
        this.forEachLoop = forEachLoop;
    }

    /**
     * Method getConditionalBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public BlockNode getConditionalBlock() {
        return conditionalBlock;
    }

    /**
     * Method getOperation
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ScriptInstructionContext getOperation() {
        return this.operation;
    }

    /**
     * Method getParams
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Node[] getParams() {
        return formattedParams;
    }

    /**
     * Method getForLoop
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ForLoop getForLoop() {
        return forLoop;
    }

    /**
     * Method getPosition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SourcePosition getPosition() {
        return this.position;
    }

    /**
     * Method getSwitchBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SwitchBlock getSwitchBlock() {
        return switchBlock;
    }

    /**
     * Method setSwitchBlock
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param switchBlock
     */
    public void setSwitchBlock(SwitchBlock switchBlock) {
        this.switchBlock = switchBlock;
    }

    /**
     * Method insertHead
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param cond
     */
    public void insertHead(Condition cond) {
        conditions.add(cond);
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return funcName;
    }

    /**
     * Method getInstructionParameters
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param script
     *
     * @return
     */
    public ExpressionParser getInstructionParameters(Scope script) {
        return new ExpressionParser(this.formattedParams, script);
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param player
     *
     * @return
     */
    public ScriptStatus eval(Scope scope, Player player) {
        return operation.eval(this, 0, player, getInstructionParameters(scope), scope);
    }

    /**
     * Method evaluateCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     * @param script
     *
     * @return
     */
    public boolean evaluateCondition(Player pla, Scope script) {
        return condtionalCode.eval(script);
    }

    /**
     * Method getCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public InternalCondition getCondition() {
        return internalCondition;
    }

    /**
     * Method isCondition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isCondition() {
        return condtionalCode != null;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte getType() {
        return type;
    }
}
