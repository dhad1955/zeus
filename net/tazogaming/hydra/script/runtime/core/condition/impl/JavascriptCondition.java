package net.tazogaming.hydra.script.runtime.core.condition.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptRuntime;
import net.tazogaming.hydra.script.runtime.core.condition.*;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.js.JPlugin;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 14:41
 */
public class JavascriptCondition implements ConditionType {
    String name;

    /**
     * Constructs ...
     *
     *
     * @param name
     */
    public JavascriptCondition(String name) {
        this.name = name;
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param tokenizer
     *
     * @return
     */
    @Override
    public boolean eval(Scope scope, Player player, ExpressionParser tokenizer) {

        try {
            for (JPlugin p : scope.getPlugins()) {
                if (p.hasFunction(name)) {
                    return p.getBoolean(player, scope, name, ((tokenizer == null) ||!tokenizer.hasNext())
                            ? new Object[0]
                            : tokenizer.remainingObjects());
                }
            }

            if (ScriptRuntime.GLOBAL_SCOPE.hasFunction(name)) {
                return ScriptRuntime.GLOBAL_SCOPE.getBoolean(player, scope, name,
                        ((tokenizer == null) ||!tokenizer.hasNext())
                        ? new Object[0]
                        : tokenizer.remainingObjects());
            }
        } catch (Exception ee) {
            scope.debug("<col=" + Text.RED + ">JavaScriptInvokeError: " + ee.getMessage());
            ee.printStackTrace();

            throw new RuntimeException("JAVASCRIPT ERROR");
        }

        return false;
    }
}
