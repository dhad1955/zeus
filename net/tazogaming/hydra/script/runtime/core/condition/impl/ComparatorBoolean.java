package net.tazogaming.hydra.script.runtime.core.condition.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.expr.Node;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 15:15
 */
public class ComparatorBoolean implements BooleanExpression {
    private Node compare, compareWith;
    private Token      comparator;

    /**
     * Constructs ...
     *
     *
     * @param compare
     * @param operator
     * @param compareWith
     */
    public ComparatorBoolean(Node compare, Token operator, Node compareWith) {
        this.compare     = compare;
        this.compareWith = compareWith;
        this.comparator  = operator;
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     *
     * @return
     */
    public boolean eval(Scope scope) {
        Object eval     = compare.eval(scope, scope.getController());
        Object evalWith = compareWith.eval(scope, scope.getController());

        if ((evalWith instanceof String) && (eval instanceof String)) {
            if (comparator.getType() == Token.TYPE_EQUALS) {
                return eval.toString().equalsIgnoreCase(evalWith.toString());
            } else if (comparator.getType() == Token.NOT_EQUAL_TO) {
                return !eval.toString().equalsIgnoreCase(evalWith.toString());
            } else {
                throw new RuntimeException("Error invalid comparator for string " + comparator.getData());
            }
        }

        if (((evalWith instanceof Integer) && (eval instanceof String))
                || ((eval instanceof Integer) && (evalWith instanceof String))) {
            throw new RuntimeException("Error cannot compare an integer to string " + eval.toString() + " "
                                       + evalWith.toString());
        }

        int compare     = (Integer) eval;
        int compareWith = (Integer) evalWith;

        switch (comparator.getType()) {
        case Token.TYPE_MORE_THAN :
            return compare > compareWith;

        case Token.TYPE_MORE_THAN_OR_EQUAL_TO :
            return compare >= compareWith;

        case Token.TYPE_EQUALS :
            return compare == compareWith;

        case Token.NOT_EQUAL_TO :
            return compare != compareWith;

        case Token.TYPE_LESS_THAN :
            return compare < compareWith;

        case Token.TYPE_LESS_THAN_OR_EQUAL_TO :
            return compare <= compareWith;
        }

        throw new RuntimeException("Error evaluating boolean");
    }
}
