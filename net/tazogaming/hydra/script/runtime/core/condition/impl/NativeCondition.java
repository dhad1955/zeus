package net.tazogaming.hydra.script.runtime.core.condition.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;
import net.tazogaming.hydra.script.runtime.instr.ConditionMap;
import net.tazogaming.hydra.script.runtime.instr.InternalCondition;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.ScriptParseException;
import net.tazogaming.hydra.script.runtime.core.condition.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 14:41
 */
public class NativeCondition implements ConditionType {
    private InternalCondition condition;
    private String            conditionName;

    /**
     * Constructs ...
     *
     *
     * @param conditionName
     */
    public NativeCondition(String conditionName) {
        try {
            this.conditionName = conditionName;
            condition          = ConditionMap.get(conditionName);
            if(condition == null)
                throw new RuntimeException();

        } catch (Exception EE) {
            throw new ScriptParseException("Error condition not found: " + conditionName);
        }
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     * @param tokenizer
     *
     * @return
     */
    @Override
    public boolean eval(Scope scope, Player player, ExpressionParser tokenizer) {
        return condition.onCondition(player, tokenizer, scope);
    }
}
