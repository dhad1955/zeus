package net.tazogaming.hydra.script.runtime.core.condition.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.expr.Node;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 14:52
 */
public class BooleanCondition {
    boolean            not;
    boolean            js;
    private Node node;

    /**
     * Constructs ...
     *
     *
     * @param data
     * @param not
     * @param js
     */
    public BooleanCondition(Node data, boolean not, boolean js) {
        this.node = data;
        this.not        = not;
        this.js         = js;
    }

    /**
     * Method isNot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isNot() {
        return not;
    }

    /**
     * Method setNot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param not
     */
    public void setNot(boolean not) {
        this.not = not;
    }

    /**
     * Method isJs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isJs() {
        return js;
    }

    /**
     * Method setJs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param js
     */
    public void setJs(boolean js) {
        this.js = js;
    }

    /**
     * Method getExpression
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Node getNode() {
        return node;
    }

    /**
     * Method setExpression
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param node
     */
    public void setNode(Node node) {
        this.node = node;
    }

    /**
     * Method getExpr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Node getExpr() {
        return node;
    }
}
