package net.tazogaming.hydra.script.runtime.core.condition.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.build.lexer.Token;
import net.tazogaming.hydra.script.runtime.Scope;

//~--- JDK imports ------------------------------------------------------------


/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 14:55
 */
public class ConditionalEvaluator {
    private byte[]              booleanOperators = new byte[40];
    private BooleanExpression[] values           = new BooleanExpression[40];
    private int                 caret            = 0;

    /**
     * Method addExpression
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bool
     */
    public void addExpression(BooleanExpression bool) {
        this.values[caret++] = bool;
    }

    /**
     * Method addBooleanOperator
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param current
     */
    public void addBooleanOperator(Token current) {
        this.booleanOperators[caret++] = (byte) current.getType();
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     *
     * @return
     */
    public boolean eval(Scope scope) {
        boolean mainValue = false;

        for (int pointer = 0; pointer < caret; pointer++) {
            if (values[pointer] == null) {
                continue;
            }

            boolean eval = values[pointer].eval(scope);

            if (eval && (booleanOperators[pointer + 1] == Token.TYPE_OR)) {
                return true;
            }

            if (!eval && (booleanOperators[pointer + 1] == Token.TYPE_AND)) {
                return false;
            }

            if ((pointer > 0) &&!eval && (booleanOperators[pointer - 1] == Token.TYPE_AND)) {
                return false;
            }

            if ((pointer > 0) && (booleanOperators[pointer - 1] == Token.TYPE_AND)) {
                mainValue &= eval;
            } else {
                mainValue = eval;
            }
        }

        return mainValue;
    }
}
