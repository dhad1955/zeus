package net.tazogaming.hydra.script.runtime.core.condition.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.core.condition.*;
import net.tazogaming.hydra.script.runtime.expr.Node;
import net.tazogaming.hydra.script.runtime.expr.ExpressionParser;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 07/08/14
 * Time: 15:24
 */
public class NormalBooleanExpression implements BooleanExpression {
    private ConditionType type;
    private Node[]  params;
    private boolean       not;
    private long externalIdentifier = 0;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param params
     * @param not
     */
    public NormalBooleanExpression(ConditionType type, Node[] params, boolean not) {
        this.type   = type;
        this.params = params;
        this.not    = not;
    }

    public NormalBooleanExpression setExternalIdentifier(long l){
        this.externalIdentifier = l;
        return this;
    }

    private ExpressionParser buildExpressions(Scope scope) {
        if (params == null) {
            return null;
        }

        return new ExpressionParser(params, scope);
    }

    /**
     * Method eval
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param scope
     *
     * @return
     */
    @Override
    public boolean eval(Scope scope) {

        Player player = scope.getController();


        if(externalIdentifier > 0)
            player = (Player)scope.getVar(externalIdentifier).getData();

        if (not) {
            return !type.eval(scope, player, buildExpressions(scope));
        }

        return type.eval(scope, player, buildExpressions(scope));
    }
}
