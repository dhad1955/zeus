package net.tazogaming.hydra.script.runtime;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.script.runtime.runnable.Trigger;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/07/14
 * Time: 20:42
 */
public class ScriptEnum {
    private HashMap<Integer, ScriptEnumerator> types = new HashMap<Integer, ScriptEnumerator>();
    private List<Integer>              keys  = new ArrayList<Integer>();
    private String                     name;

    /**
     * Constructs ...
     *
     *
     * @param name
     */
    public ScriptEnum(String name) {
        this.name = name;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return this.name;
    }

    /**
     * Method addType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void addType(ScriptEnumerator type) {
        types.put(type.hashcode(), type);
        keys.add(type.firstKey());
    }

    /**
     * Method getKey
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     *
     * @return
     */
    public int getKey(int index) {
        return keys.get(index);
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return types.size();
    }

    /**
     * Method get
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param hashcodes
     *
     * @return
     */
    public ScriptEnumerator get(int... hashcodes) {
        if (types.containsKey(Trigger.hashcode(hashcodes))) {
            return types.get(Trigger.hashcode(hashcodes));
        }

        return null;
    }
}
