package net.tazogaming.hydra.script.sched;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Context;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.runnable.ScriptRunnable;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/08/14
 * Time: 15:23
 *
 * @param <T>
 */
public class ScheduledScriptTask<T> {

    /** MAX_EXCEPTIONS made: 14/08/20 **/
    private static final int MAX_EXCEPTIONS = 27;

    /** delaySuspended made: 14/08/19 */
    private boolean delaySuspended = false;

    /** exceptionCount made: 14/08/20 **/
    private byte exceptionCount = 0;

    /** name made: 14/08/19 */
    private String name;

    /** terminated made: 14/08/19 */
    private boolean terminated;

    /** type made: 14/08/19 */
    private TimingType type;

    /** removeSetting made: 14/08/19 */
    private RemoveSetting removeSetting;

    /** saveSettings made: 14/08/19 */
    private SaveSettings saveSettings;

    /** blockName made: 14/08/19 */
    private String blockName;

    /** removeTime made: 14/08/19 */
    private long removeTime;

    /** removeTimeOrig made: 14/08/19 */
    private long removeTimeOrig;

    /** blockToRun made: 14/08/19 */
    private ScriptRunnable blockToRun;

    /** scope made: 14/08/19 */
    private Scope scope;

    /** t made: 14/08/19 */
    private T t;

    /** isTickableScript made: 14/08/19 */
    private boolean isTickableScript;

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param time
     * @param timingType
     * @param runnable
     */
    public ScheduledScriptTask(String name, long time, TimingType timingType, ScriptRunnable runnable) {
        this.name          = name;
        this.saveSettings  = SaveSettings.REMOVE_ON_DC;
        this.removeSetting = RemoveSetting.RUN_FOREVER;
        this.type          = timingType;
        this.schedule(time);
        this.blockToRun = runnable;
        setTickableScript(true);
        this.removeTimeOrig = time;
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param blockName
     * @param time
     * @param type
     * @param setting
     * @param save
     * @param t
     * @param context
     */
    public ScheduledScriptTask(String name, String blockName, long time, TimingType type, RemoveSetting setting,
                               SaveSettings save, T t, Context context) {
        this.saveSettings   = save;
        this.type           = type;
        this.removeSetting  = setting;
        this.name           = name;
        this.removeTimeOrig = time;
        this.t              = t;

        if (context != null) {
            this.blockToRun = context.getBlockByName(blockName);
        }

        if ((blockToRun != null) && (save == SaveSettings.SAVE_TO_FILE)) {
            throw new RuntimeException("Error scheduling task: " + name + " cannot save to file if block is local");
        } else if (blockToRun == null) {
            this.blockToRun = World.getWorld().getScriptManager().get_block(blockName);
        }

        this.blockName = blockName;
        schedule(time);
    }


    public Player getPlayer() {
        if(t instanceof Player)
            return (Player)t;
        return null;
    }

    /**
     * Method isTickableScript
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTickableScript() {
        return isTickableScript;
    }

    /**
     * Method setTickableScript
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param isTickableScript
     */
    public void setTickableScript(boolean isTickableScript) {
        this.isTickableScript = isTickableScript;
    }

    /**
     * Method getBlockName
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getBlockName() {
        return blockName;
    }

    /**
     * Method getRemoveTimeOrig
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getRemoveTimeOrig() {
        return removeTimeOrig;
    }

    /**
     * Method setRemoveTimeOrig
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param removeTimeOrig
     */
    public void setRemoveTimeOrig(long removeTimeOrig) {
        this.removeTimeOrig = removeTimeOrig;
    }

    /**
     * Method isDelaySuspended
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isDelaySuspended() {
        return delaySuspended;
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void run() {
        if (this.blockToRun != null) {
            if (this.scope == null) {
                scope = new Scope();
                scope.setRunning(blockToRun);

                if (t instanceof Player) {
                    scope.setController((Player) t);
                } else {
                    scope.declare(Text.longForName("param"), t);
                    scope.declare(Text.longForName("task"), this);
                }
            }

            if (World.getWorld().getScriptManager().runScript(scope)) {
                if (scope.getLastException() != null) {
                    exceptionCount++;
                }

                if (exceptionCount == MAX_EXCEPTIONS) {
                    this.setTerminated(true);

                    try {
                        World.getWorld().getScriptManager().logErrorMessage(
                            "Error, max exceptions occured for script terminating from task scheduler",
                            "File: " + (scope.getRunning().getSourcePosition()),
                            "Total errors: " + exceptionCount + "",
                            "Task name: " + this.name + "" + "Block name: " + this.blockName,
                            "Exception cause: " + scope.getLastException().getClass().getSimpleName() + ": "
                            + scope.getLastException().getMessage());
                    } catch (Exception ee) {}
                }

                scope = null;
                unSuspendDelay();
            } else {
                suspendDelay();
            }
        } else {
            setTerminated(true);
        }
    }

    /**
     * Method error
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param ee
     */
    void error(Exception ee) {
        ee.printStackTrace();
    }

    /**
     * Method suspendDelay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void suspendDelay() {
        delaySuspended = true;
    }

    /**
     * Method unSuspendDelay
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void unSuspendDelay() {
        delaySuspended = false;
    }

    /**
     * Method rewind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void rewind() {
        this.schedule(this.removeTimeOrig);
    }

    /**
     * Method schedule
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param time
     */
    public void schedule(long time) {
        this.removeTime = (type == TimingType.TICK)
                          ? Core.currentTime + time
                          : System.currentTimeMillis() + time;
    }

    /**
     * Method getRemoveSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public RemoveSetting getRemoveSetting() {
        return removeSetting;
    }

    /**
     * Method setRemoveSetting
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param removeSetting
     */
    public void setRemoveSetting(RemoveSetting removeSetting) {
        this.removeSetting = removeSetting;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method setName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Method isTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isTerminated() {
        return terminated;
    }

    /**
     * Method setTerminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param terminated
     */
    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public TimingType getType() {
        return type;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(TimingType type) {
        this.type = type;
    }

    /**
     * Method getSaveSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public SaveSettings getSaveSettings() {
        return saveSettings;
    }

    /**
     * Method setSaveSettings
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param saveSettings
     */
    public void setSaveSettings(SaveSettings saveSettings) {
        this.saveSettings = saveSettings;
    }

    /**
     * Method getRemoveTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public long getRemoveTime() {
        return removeTime;
    }

    /**
     * Method setRemoveTime
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param removeTime
     */
    public void setRemoveTime(int removeTime) {
        this.removeTime = removeTime;
    }
}
