package net.tazogaming.hydra.script.sched;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/08/14
 * Time: 15:28
 */
public enum RemoveSetting { RUN_ONCE, RUN_FOREVER }
