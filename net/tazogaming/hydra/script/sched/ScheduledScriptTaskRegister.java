package net.tazogaming.hydra.script.sched;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Context;
import net.tazogaming.hydra.script.runtime.runnable.ScriptRunnable;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/08/14
 * Time: 15:34
 *
 * @param <T>
 */
public abstract class ScheduledScriptTaskRegister<T> {

    /** scheduledList made: 14/08/19 */
    private List<ScheduledScriptTask> scheduledList = new ArrayList<ScheduledScriptTask>();

    /** newTasks made: 14/08/19 */
    private List<ScheduledScriptTask> newTasks = new ArrayList<ScheduledScriptTask>();

    protected abstract void saveTasks(byte[] buffer, T t);

    /**
     * Method register
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param task
     */
    public void register(ScheduledScriptTask task) {
        if (getByName(task.getName()) != null) {
            throw new RuntimeException("Error task " + task.getName() + " is already registered!");
        }

        newTasks.add(task);
    }

    /**
     * Method register
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     * @param name
     * @param blockName
     * @param time
     * @param type
     * @param setting
     * @param settings
     * @param context
     */
    public void register(T t, String name, String blockName, long time, TimingType type, RemoveSetting setting,
                         SaveSettings settings, Context context) {
        ScheduledScriptTask<T> taskToRegister;

        taskToRegister = new ScheduledScriptTask<T>(name, blockName, time, type, setting, settings, t, context);

        if (getByName(name) != null) {
            throw new RuntimeException("Error duplicated task added: " + name);
        }

        newTasks.add(taskToRegister);
    }

    /**
     * Method register
     * Registers a normal script runnable task
     * that constantly repeats it self and doesnt end
     * saving is not supported, as this is managed by the script.
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     * @param runnable
     * @param time
     * @param type
     */
    public void register(String name, ScriptRunnable runnable, long time, TimingType type) {
        ScheduledScriptTask task = new ScheduledScriptTask(name, time, type, runnable);

        if (getByName(name) != null) {
            throw new RuntimeException("Error duplicated task added: " + name);
        }

        newTasks.add(task);
    }

    /**
     * Method removeAllTickableScripts
     * Created on 14/08/19
     * Removes all scripts in this register that were made using @schedule
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void removeAllTickableScripts() {
        for (ScheduledScriptTask task : scheduledList) {
            if (task.isTickableScript()) {
                task.setTerminated(true);
            }
        }
    }

    /**
     * Method notifyShutdown
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param t
     */
    public void notifyShutdown(T t) {
        if (t instanceof Player) {
            if (((Player) t).getIoSession().isConnected()) {
                return;
            }
        }

        Buffer buffer = new Buffer(new byte[10000]);

        buffer.setPtr(0);

        List<ScheduledScriptTask> tasksToSave = new ArrayList<ScheduledScriptTask>();

        for (ScheduledScriptTask task : scheduledList) {
            if (task.getSaveSettings() == SaveSettings.SAVE_TO_FILE) {
                tasksToSave.add(task);
            }
        }

        buffer.writeWord(tasksToSave.size());

        for (ScheduledScriptTask task : tasksToSave) {
            buffer.writeString(task.getName());
            buffer.writeString(task.getBlockName());
            buffer.writeByte(task.getSaveSettings().ordinal());
            buffer.writeByte(task.getRemoveSetting().ordinal());
            buffer.writeByte(task.getType().ordinal());

            long removeTime = 0;

            if (task.getType() == TimingType.TICK) {
                removeTime = (long) Core.timeUntil((int) task.getRemoveTime());
            } else {
                removeTime = task.getRemoveTime();
            }

            buffer.writeQWord(task.getRemoveTimeOrig());
            buffer.writeQWord(removeTime);
        }

        byte[] returnBuffer = new byte[buffer.getPtr()];

        System.arraycopy(buffer.getBuffer(), 0, returnBuffer, 0, buffer.getPtr());
        saveTasks(returnBuffer, t);
        scheduledList.clear();
    }

    /**
     * Method load
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     * @param loader
     */
    public void load(byte[] data, T loader) {
        Buffer buffer = new Buffer(data);

        buffer.setPtr(0);

        int siz = buffer.readShort();

        for (int i = 0; i < siz; i++) {
            String        name      = buffer.readString();
            String        blockname = buffer.readString();
            SaveSettings  s         = SaveSettings.values()[buffer.readByte()];
            RemoveSetting set       = RemoveSetting.values()[buffer.readByte()];
            TimingType    type      = TimingType.values()[buffer.readByte()];
            long          orig      = buffer.readQWord();
            long          remaining = buffer.readQWord();

            if (type == TimingType.TIMED) {
                remaining -= 0;
            } else {
                remaining += Core.currentTime;
            }


            ScheduledScriptTask task = new ScheduledScriptTask<T>(name, blockname, remaining, type, set, s, loader,
                                           null);

            task.setRemoveTimeOrig(orig);
            newTasks.add(task);
        }
    }

    /**
     * Method getByName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return null if a method cant be found
     */
    public ScheduledScriptTask getByName(String name) {
        for (ScheduledScriptTask task : scheduledList) {
            if (task.getName().equals(name) &&!task.isTerminated()) {
                return task;
            }
        }

        return null;
    }

    /**
     * Method getByClass
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param c
     *
     * @return
     */
    public ScheduledScriptTask getByClass(Class c) {
        for (ScheduledScriptTask task : scheduledList) {
            if (task.getClass() == c) {
                return task;
            }
        }

        return null;
    }

    /**
     * Method taskExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public boolean taskExists(String name) {
        return getByName(name) != null;
    }

    /**
     * Method removeIfExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public boolean removeIfExists(String name) {
        ScheduledScriptTask task = getByName(name);

        if (task != null) {
            task.setTerminated(true);

            return true;
        }

        return false;
    }

    /**
     * Method removeIfExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param name
     *
     * @return
     */
    public boolean removeIfExists(Class name) {
        ScheduledScriptTask task = getByClass(name);

        if (task != null) {
            task.setTerminated(true);

            return true;
        }

        return false;
    }

    /**
     * Method addIfAbsent
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param task
     */
    public void addIfAbsent(ScheduledScriptTask task) {
        if (getByClass(task.getClass()) != null) {
            newTasks.add(task);
        }
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        return this.scheduledList.size();
    }

    private static String timeRemaining(ScheduledScriptTask task) {
        switch (task.getType()) {
        case TIMED :
            long         milliseconds = task.getRemoveTime() - Core.currentTimeMillis();
            int          seconds      = (int) (milliseconds / 1000) % 60;
            int          minutes      = (int) ((milliseconds / (1000 * 60)) % 60);
            int          hours        = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
            StringBuffer buffer       = new StringBuffer();

            if (hours > 0) {
                buffer.append(hours + " hours ");
            }

            if (minutes > 0) {
                buffer.append(minutes + " mins ");
            }

            buffer.append(seconds + " sec");

            return buffer.toString();

        case TICK :
            int cycles = (int) task.getRemoveTime() - Core.currentTime;
            int secs   = Core.getSecondsForTicks(cycles);
            int min    = secs / 60;
            int hr     = min % 60;

            return min + ":" + secs + " or " + cycles + " cycles";
        }

        throw new RuntimeException("Error invalid timing type!");
    }

    private static String dumpTaskInfo(ScheduledScriptTask task) {
        return "name: " + task.getName() + "" + " | " + "type: " + ((task.getType() == TimingType.TICK)
                ? "tick"
                : "dated") + "Behavior: " + ((task.getRemoveSetting() == RemoveSetting.RUN_ONCE)
                ? "run once"
                : "Run forever") + " | " + "save to file: " + ((task.getSaveSettings() == SaveSettings.SAVE_TO_FILE)
                ? "Yes"
                : "No") + " | " + "Time remaining: " + timeRemaining(task) + " | class: "
                        + task.getClass().getSimpleName();
    }

    /**
     * Method taskDump
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public List<String> taskDump() {
        ArrayList<String> returnList = new ArrayList<String>(newTasks.size() + scheduledList.size() + 2);

        returnList.add("Task dump for " + getClass().getSimpleName() + " scheduled: " + scheduledList.size()
                       + " incoming: " + newTasks.size());
        returnList.add("---- Dumping new (tasks) awaiting entering into processing queue.");

        for (ScheduledScriptTask task : newTasks) {
            returnList.add(dumpTaskInfo(task));
        }

        returnList.add("---- Dumping (tasks) current.");

        for (ScheduledScriptTask task : scheduledList) {
            returnList.add(dumpTaskInfo(task));
        }

        return returnList;
    }

    /**
     * Method update
     * Created on 14/08/19
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void update() {
        ScheduledScriptTask task;
        boolean             taskFinished = false;

        if (newTasks.size() > 0) {
            scheduledList.addAll(newTasks);
            newTasks.clear();
        }

        for (Iterator<ScheduledScriptTask> tasksToProcess = scheduledList.iterator(); tasksToProcess.hasNext(); ) {
            task = tasksToProcess.next();

            if (task.isTerminated()) {
                tasksToProcess.remove();

                continue;
            }

            if (task.isDelaySuspended()) {
                try {
                    task.run();
                } catch (Exception ee) {
                    ee.printStackTrace();
                    task.error(ee);
                }

                continue;
            }

            if (task.getType() == TimingType.TIMED) {
                taskFinished = (task.getRemoveTime() - Core.currentTimeMillis() <= 0);
            } else {
                taskFinished = task.getRemoveTime() - Core.currentTime <= 0;
            }

            if (taskFinished) {
                if (task.getRemoveSetting() == RemoveSetting.RUN_ONCE) {
                    task.setTerminated(true);
                } else {
                    task.rewind();
                }

                try {
                    task.run();
                } catch (Exception error) {
                    task.error(error);
                }

                if (task.getRemoveSetting() == RemoveSetting.RUN_ONCE) {
                    task.setTerminated(true);
                } else {
                    task.rewind();
                }
            }
        }
    }
}
