package net.tazogaming.hydra.script.sched;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.runtime.Core;
import net.tazogaming.hydra.script.runtime.Scope;
import net.tazogaming.hydra.script.runtime.runnable.ScriptRunnable;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 13/08/14
 * Time: 11:26
 */
public class ScriptScheduledTask {
    private boolean autoRemove = false;
    private boolean terminated = false;
    private int     lastRun    = 0;
    private Scope scope;
    private int     delay;
    private String  name;

    public static final int MAX_EXCEPTIONS = 30;
    private int exceptionCount = 0;

    /**
     * Constructs ...
     *
     *
     * @param scope
     * @param delay
     */
    public ScriptScheduledTask(Scope scope, int delay) {
        this("unknown", scope, delay, false);
    }

    /**
     * Constructs ...
     *
     *
     * @param c
     * @param delay
     * @param remove
     */
    public ScriptScheduledTask(Scope c, int delay, boolean remove) {
        this("unknown", c, delay, remove);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param scope
     * @param delay
     * @param remove
     */
    public ScriptScheduledTask(String name, Scope scope, int delay, boolean remove) {
        this.scope      = scope;
        this.delay      = delay;
        this.autoRemove = remove;
        this.name       = name;
        lastRun         = Core.currentTime;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method terminate
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void terminate() {
        this.terminated = true;
    }

    /**
     * Method terminated
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean terminated() {
        return terminated;
    }

    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean run() {
        if (Core.currentTime - lastRun < delay) {
            return false;
        }

        if (terminated) {
            return true;
        }

        if (scope.canRun()) {
            boolean finished = World.getWorld().getScriptManager().runScript(scope);



            if (finished && autoRemove) {
                return true;
            }

            ScriptRunnable run = scope.getRunning();

            scope = new Scope();
            scope.setRunning(run);
        } else {
            lastRun = Core.currentTime;
        }

        return false;
    }
}
