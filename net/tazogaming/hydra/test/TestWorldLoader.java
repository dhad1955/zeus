
/*
* TestWorldLoader.java
*
* Created on 16-Dec-2007, 21:45:53
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
 */
package net.tazogaming.hydra.test;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.PlayerAppearance;
import net.tazogaming.hydra.io.*;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

//~--- JDK imports ------------------------------------------------------------

import java.util.Arrays;

/**
 *
 * @author alex
 */
public class TestWorldLoader implements WorldLoader {

    /**
     * Method loadWorld
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param w
     */
    public void loadWorld(World w) {}

    /**
     * Class description
     * Hydrascape 639 Game server
     * Copyright (C) Tazogaming 2014
     *
     *
     * @version        Enter version here..., 14/08/18
     * @author         Daniel Hadland
     */
    static class TestPlayerLoader implements PlayerLoader {

        /**
         * Method save
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @param p
         * @param con
         *
         * @throws UnsupportedOperationException
         */
        public void save(Player p, MysqlConnection con) throws UnsupportedOperationException {

            // don't really care much
        }

        /**
         * Method load
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @param p
         * @param con
         *
         * @return
         *
         * @throws UnsupportedOperationException
         */
        public byte load(Player p, MysqlConnection con, boolean lobby) throws UnsupportedOperationException {
            p.setAppearance(new PlayerAppearance(3, 19, 29, 35, 39, 44, 7, 8, 9, 5, 0));
            p.setLocation(Point.location(3254 + (int) (Math.random() * 50), 3420 + (int) (Math.random() * 50), 0),
                          true);

            int[] stats = new int[25];

            Arrays.fill(stats, 1);
            p.setCurStats(stats);
            Arrays.fill(stats, 1);    // exp
            p.setExps(stats);

            return (byte) 2;
        }
    }
}
