package net.tazogaming.hydra.test.performancetest;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.ChatMessage;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.entity3d.player.Account;
import net.tazogaming.hydra.entity3d.player.Equipment;
import net.tazogaming.hydra.entity3d.player.PlayerFollowing;
import net.tazogaming.hydra.entity3d.player.actions.ActionRequest;
import net.tazogaming.hydra.game.ui.special.SpecialAttacks;
import net.tazogaming.hydra.io.UserAccount;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.script.runtime.runnable.Trigger;
import net.tazogaming.hydra.game.gametick.PlayerTickEvent;
import net.tazogaming.hydra.game.ui.GameFrame;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.util.Random;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/08/14
 * Time: 16:50
 */
public class BotPlayer extends Player {
    public static final Point baseLocation = Point.location(3222, 3400, 0);
    public static int         botID        = 0;

    /**
     * Constructs ...
     *
     *
     * @param loc
     */
    public BotPlayer(Point loc) {
        super(new PlaySession(true));

        Player player = this;

        UserAccount.newPlayer(player, null, 0);
        setLocation(loc);
        player.setResponseCode((byte) 2);
        player.setGameFrame(new GameFrame(player));
        player.setCredentials("bot" + botID, "ladfl", false);
        player.setId(botID++);
        World.getWorld().addPlayerAwaitingLogin(player);

        for (int i = 0; i < 25; i++) {
            player.addXp(i, 1020000000);
        }

        player.getInventory().addItem(565, 1000);
        player.getInventory().addItem(555, 10000);
        player.getInventory().addItem(560, 10000);
        player.getInventory().addItem(554, 10000);
        player.getInventory().addItem(556, 100000);
        player.getEquipment().set(1048, 1, Equipment.HAT);

        switch (GameMath.rand3(3)) {
        case 0 :
            player.getEquipment().set(861, 1, 3);
            player.getEquipment().set(882, 10000, Equipment.ARROWS);

            break;

        case 1 :
            player.getEquipment().set(1231, 1, 3);

            break;

        case 2 :
            player.setRequestedSpell(80);
            player.setAutocast(80);

            break;
        }

        player.setRights(2);
        getTickEvents().add(new PlayerTickEvent(player, 2, false, true, 0) {
            @Override
            public void doTick(Player owner) {
                if (owner.getRoute().isIdle() &&!owner.isActionsDisabled() &&!owner.isFollowing()) {
               //     owner.requestWalk(owner.getX() + -10 + new Random().nextInt(15),
                 //                     owner.getY() + -10 + new Random().nextInt(15));
                }

                owner.getAccount().set(Account.AUTO_RETALIATE, 1);

                if (GameMath.rand3(1000) < 10) {
                    switch (new Random().nextInt(3)) {
                    case 0 :
                        owner.setLastChatMessage(new ChatMessage(owner, "Die a horrible death", 0));

                        break;

                    case 1 :
                        owner.setLastChatMessage(new ChatMessage(owner, "u are already dead lol", 0));

                        break;

                    case 2 :
                        owner.setLastChatMessage(new ChatMessage(owner, "lelelel", 0));

                        break;
                    }
                }

                if (!owner.isFollowing() && owner.isInWilderness() &&!owner.getCombatAdapter().isInCombat()) {
                    for (Player player : owner.getWatchedPlayers().getAllEntities()) {
                        if (player.canBeAttacked(owner)) {
                            if (owner.isFollowing()) {
                                owner.unFollow();
                            }

                            owner.getRoute().resetPath();
                            World.getWorld().getPathService().removePathIfExists(owner);
                            owner.getFocus().focus(player);
                            owner.getRequest().setRequest(ActionRequest.TYPE_PLAYER_ATTACK, -1, player);
                            owner.followPlayer(player);
                            owner.getFollowing().setMode(PlayerFollowing.COMBAT_BASED_FOLLOW);
                        }
                    }
                }

                if (owner.getCombatAdapter().isInCombat()) {
                    if (GameMath.getPercentFromTotal(owner.getCurrentHealth(), owner.getMaxHealth()) < 20) {
                        if (World.getWorld().getScriptManager().forceTrigger(owner, Trigger.ITEM_CLICK, null, 385)) {}
                    }

                    if (owner.getCombatAdapter().getFightingWith().getCurrentHealth() < 40) {
                        if ((owner.getCurrentSpecial() > 3) &&!isSpecialHighlighted()) {
                            SpecialAttacks.changeHighlight(owner);
                        }
                    }
                }
            }
            ;
        });
    }
}
