package net.tazogaming.hydra.test.performancetest;

import net.tazogaming.hydra.net.Packet;
import org.apache.mina.core.filterchain.IoFilterChain;
import org.apache.mina.core.future.CloseFuture;
import org.apache.mina.core.future.ReadFuture;
import org.apache.mina.core.future.WriteFuture;
import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.service.IoService;
import org.apache.mina.core.service.TransportMetadata;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;
import org.apache.mina.core.session.IoSessionConfig;
import org.apache.mina.core.write.WriteRequest;
import org.apache.mina.core.write.WriteRequestQueue;

import java.net.SocketAddress;
import java.util.Set;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 29/08/14
 * Time: 16:46
 */
public class FakeIOSession implements IoSession {
    @Override
    public long getId() {
        return 0;
    }

    @Override
    public IoService getService() {
        return null;
    }

    @Override
    public IoHandler getHandler() {
        return null;
    }

    @Override
    public IoSessionConfig getConfig() {
        return null;
    }

    @Override
    public IoFilterChain getFilterChain() {
        return null;
    }

    @Override
    public WriteRequestQueue getWriteRequestQueue() {
        return null;
    }

    @Override
    public TransportMetadata getTransportMetadata() {
        return null;
    }

    @Override
    public ReadFuture read() {
        return null;
    }

    @Override
    public WriteFuture write(Object o) {

        Packet p = (Packet)o;

        byte[] copy = new byte[p.getData().length];
        System.arraycopy(((Packet) o).getData(), 0, copy, 0, copy.length);
        return null;
    }

    @Override
    public WriteFuture write(Object o, SocketAddress socketAddress) {
        return null;
    }

    @Override
    public CloseFuture close(boolean b) {
        return null;
    }

    /**
     * @deprecated
     */
    @Override
    public CloseFuture close() {
        return null;
    }

    /**
     * @deprecated
     */
    @Override
    public Object getAttachment() {
        return null;
    }

    /**
     * @param o
     * @deprecated
     */
    @Override
    public Object setAttachment(Object o) {
        return null;
    }

    @Override
    public Object getAttribute(Object o) {
        return null;
    }

    @Override
    public Object getAttribute(Object o, Object o2) {
        return null;
    }

    @Override
    public Object setAttribute(Object o, Object o2) {
        return null;
    }

    @Override
    public Object setAttribute(Object o) {
        return null;
    }

    @Override
    public Object setAttributeIfAbsent(Object o, Object o2) {
        return null;
    }

    @Override
    public Object setAttributeIfAbsent(Object o) {
        return null;
    }

    @Override
    public Object removeAttribute(Object o) {
        return null;
    }

    @Override
    public boolean removeAttribute(Object o, Object o2) {
        return false;
    }

    @Override
    public boolean replaceAttribute(Object o, Object o2, Object o3) {
        return false;
    }

    @Override
    public boolean containsAttribute(Object o) {
        return false;
    }

    @Override
    public Set<Object> getAttributeKeys() {
        return null;
    }

    @Override
    public boolean isConnected() {
        return true;
    }

    @Override
    public boolean isClosing() {
        return false;
    }

    @Override
    public CloseFuture getCloseFuture() {
        return null;
    }

    @Override
    public SocketAddress getRemoteAddress() {
        return new SocketAddress() {
            /**
             * Returns a hash code value for the object. This method is
             * supported for the benefit of hash tables such as those provided by
             * {@link java.util.HashMap}.
             * <p/>
             * The general contract of {@code hashCode} is:
             * <ul>
             * <li>Whenever it is invoked on the same object more than once during
             * an execution of a Java application, the {@code hashCode} method
             * must consistently return the same integer, provided no information
             * used in {@code equals} comparisons on the object is modified.
             * This integer need not remain consistent from one execution of an
             * application to another execution of the same application.
             * <li>If two objects are equal according to the {@code equals(Object)}
             * method, then calling the {@code hashCode} method on each of
             * the two objects must produce the same integer result.
             * <li>It is <em>not</em> required that if two objects are unequal
             * according to the {@link Object#equals(Object)}
             * method, then calling the {@code hashCode} method on each of the
             * two objects must produce distinct integer results.  However, the
             * programmer should be aware that producing distinct integer results
             * for unequal objects may improve the performance of hash tables.
             * </ul>
             * <p/>
             * As much as is reasonably practical, the hashCode method defined by
             * class {@code Object} does return distinct integers for distinct
             * objects. (This is typically implemented by converting the internal
             * address of the object into an integer, but this implementation
             * technique is not required by the
             * Java<font size="-2"><sup>TM</sup></font> programming language.)
             *
             * @return a hash code value for this object.
             * @see Object#equals(Object)
             * @see System#identityHashCode
             */
            @Override
            public int hashCode() {
                return super.hashCode();
            }
        };
    }

    @Override
    public SocketAddress getLocalAddress() {
        return null;
    }

    @Override
    public SocketAddress getServiceAddress() {
        return null;
    }

    @Override
    public void setCurrentWriteRequest(WriteRequest writeRequest) {

    }

    @Override
    public void suspendRead() {

    }

    @Override
    public void suspendWrite() {

    }

    @Override
    public void resumeRead() {

    }

    @Override
    public void resumeWrite() {

    }

    @Override
    public boolean isReadSuspended() {
        return false;
    }

    @Override
    public boolean isWriteSuspended() {
        return false;
    }

    @Override
    public void updateThroughput(long l, boolean b) {

    }

    @Override
    public long getReadBytes() {
        return 0;
    }

    @Override
    public long getWrittenBytes() {
        return 0;
    }

    @Override
    public long getReadMessages() {
        return 0;
    }

    @Override
    public long getWrittenMessages() {
        return 0;
    }

    @Override
    public double getReadBytesThroughput() {
        return 0;
    }

    @Override
    public double getWrittenBytesThroughput() {
        return 0;
    }

    @Override
    public double getReadMessagesThroughput() {
        return 0;
    }

    @Override
    public double getWrittenMessagesThroughput() {
        return 0;
    }

    @Override
    public int getScheduledWriteMessages() {
        return 0;
    }

    @Override
    public long getScheduledWriteBytes() {
        return 0;
    }

    @Override
    public Object getCurrentWriteMessage() {
        return null;
    }

    @Override
    public WriteRequest getCurrentWriteRequest() {
        return null;
    }

    @Override
    public long getCreationTime() {
        return 0;
    }

    @Override
    public long getLastIoTime() {
        return 0;
    }

    @Override
    public long getLastReadTime() {
        return 0;
    }

    @Override
    public long getLastWriteTime() {
        return 0;
    }

    @Override
    public boolean isIdle(IdleStatus idleStatus) {
        return false;
    }

    @Override
    public boolean isReaderIdle() {
        return false;
    }

    @Override
    public boolean isWriterIdle() {
        return false;
    }

    @Override
    public boolean isBothIdle() {
        return false;
    }

    @Override
    public int getIdleCount(IdleStatus idleStatus) {
        return 0;
    }

    @Override
    public int getReaderIdleCount() {
        return 0;
    }

    @Override
    public int getWriterIdleCount() {
        return 0;
    }

    @Override
    public int getBothIdleCount() {
        return 0;
    }

    @Override
    public long getLastIdleTime(IdleStatus idleStatus) {
        return 0;
    }

    @Override
    public long getLastReaderIdleTime() {
        return 0;
    }

    @Override
    public long getLastWriterIdleTime() {
        return 0;
    }

    @Override
    public long getLastBothIdleTime() {
        return 0;
    }
}
