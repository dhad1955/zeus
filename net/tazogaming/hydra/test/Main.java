
/*
* Main.java
*
* Created on 23-Dec-2007, 11:38:13
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
 */
package net.tazogaming.hydra.test;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.MagicFormulaTest;
import net.tazogaming.hydra.boot.BootStrapList;
import net.tazogaming.hydra.boot.impl.*;
import net.tazogaming.hydra.boot.impl.ServiceLoader;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.io.TextFile;
import net.tazogaming.hydra.io.js5.format.CacheItemDefinition;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.security.AdminAuthenticator;
import net.tazogaming.hydra.util.*;
import net.tazogaming.hydra.util.math.GameMath;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

//~--- JDK imports ------------------------------------------------------------


/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class Main {
    public static boolean TEST_MODE = false;

    /**
     * Method getConfigPath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public static final String getConfigPath() {
        return "./config/";
    }

    private static void log(String log) {
        Logger.err("[Startup]: " + log);
    }

    /**
     * Method boot
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */


    /**
     * Method boot
     * Created on 14/09/06
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public static void boot() {



        BootStrapList bootQueue = new BootStrapList();

        bootQueue.add(CoreLoader.class);
        bootQueue.add(MapLoader.class);
        bootQueue.add(ItemLoader.class);
        bootQueue.add(NpcLoader.class);
        bootQueue.add(ScriptLoader.class);
        bootQueue.add(SkillLoader.class);
        bootQueue.add(ContentLoader.class);
        bootQueue.add(ServiceLoader.class);
        bootQueue.boot();

        AdminAuthenticator.load();

    }


    /**
     * Method main
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param args
     *
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        boot();
    }
}
