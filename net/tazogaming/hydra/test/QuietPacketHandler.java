
/*
* BlankPacketHandler.java
*
* Created on 23-Dec-2007, 15:32:19
*
* To change this template, choose Tools | Template Manager
* and open the template in the editor.
 */
package net.tazogaming.hydra.test;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.Packet;
import net.tazogaming.hydra.net.packethandler.DelayPolicy;
import net.tazogaming.hydra.net.packethandler.PacketHandler;

/**
 *
 * @author alex
 */
public class QuietPacketHandler implements PacketHandler {

    /**
     * Method handlePacket
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *@param p
     * @param player
     */
    public void handlePacket(Packet p, Player player) {

        // ignore
    }

    @Override
    public DelayPolicy getDelayPolicy() {
        return DelayPolicy.INSTANT;
    }
}
