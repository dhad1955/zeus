package net.tazogaming.hydra.io.js5.bzip;

//~--- JDK imports ------------------------------------------------------------

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class BZ2Decompressor {

    /**
     * Method decompress
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param slen
     * @param off
     * @param in
     * @param out
     *
     * @throws IOException
     */
    public static void decompress(int slen, int off, byte[] in, byte[] out) throws IOException {
        byte in2[] = new byte[slen];

        System.arraycopy(in, off, in2, 0, slen);

        DataInputStream ins = new DataInputStream(new CBZip2InputStream(new ByteArrayInputStream(in2), 0));

        try {
            ins.readFully(out);
        } finally {
            ins.close();
        }
    }
}
