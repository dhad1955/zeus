package net.tazogaming.hydra.io.js5.format;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.js5.CacheConstants;
import net.tazogaming.hydra.io.js5.CacheManager;

//~--- JDK imports ------------------------------------------------------------

import java.nio.ByteBuffer;

import java.util.HashMap;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 22/07/14
 * Time: 17:51
 */
public class CacheItemDefinition {
    private static HashMap<Integer, Object> clientScriptData;
    private int                             maleWornModelId1 = -1;
    private int                             maleWornModelId2 = -1;
    private int                             id;
    private boolean                         loaded;
    private int                             interfaceModelId;
    private String                          name;
    private int                             modelZoom;
    private int                             modelRotation1;
    private int                             modelRotation2;
    private int                             modelOffset1;
    private int                             modelOffset2;
    private int                             stackable;
    private int                             value;
    private boolean                         membersOnly;
    private int                             femaleWornModelId1;
    private int                             femaleWornModelId2;
    private String[]                        groundOptions;
    private String[]                        inventoryOptions;
    private int[]                           originalModelColors;
    private int[]                           modifiedModelColors;
    private int[]                           textureColour1;
    private int[]                           textureColour2;
    private byte[]                          unknownArray1;
    private int[]                           unknownArray2;
    private boolean                         unnoted;
    private int                             colourEquip1;
    private int                             colourEquip2;
    private int                             certId;
    private int                             certTemplateId;
    private int[]                           stackIds;
    private int[]                           stackAmounts;
    private int                             teamId;
    private int                             lendId;
    private int                             lendTemplateId;

    /**
     * Constructs ...
     *
     *
     * @param id
     */
    public CacheItemDefinition(int id) {
        this.id = id;
    }

    /**
     * Method getItemDefinition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param itemId
     *
     * @return
     */
    public static CacheItemDefinition getItemDefinition(int itemId) {
        CacheItemDefinition def = new CacheItemDefinition(itemId);

        def.loadItemDefinition();

        return def;
    }

    /**
     * Method isLoaded
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isLoaded() {
        return loaded;
    }

    /**
     * Method loadItemDefinition
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void loadItemDefinition() {
        setDefaultsVariableValules();
        setDefaultOptions();

        byte[] is = null;

        try {
            is = CacheManager.getData(CacheConstants.ITEMDEF_IDX_ID, id >>> 8, id & 0xFF);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Item " + id + " doesn't exist in the cache!");
        }

        if (is != null) {
            try {
                readOpcodeValues(ByteBuffer.wrap(is));
            } catch (Exception e) {
                System.out.println("Error while reading " + id);
            }
        }

        loaded = true;
    }

    /**
     * Method canEquip
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean canEquip() {
        return (maleWornModelId1 >= 0) || (maleWornModelId2 >= 0);
    }

    /**
     * Method hasSpecialBar
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean hasSpecialBar() {
        if (clientScriptData == null) {
            return false;
        }

        Object specialBar = clientScriptData.get(687);

        return (specialBar instanceof Integer) && (Integer) specialBar == 1;
    }

    /**
     * Method getGroupId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getGroupId() {
        if (clientScriptData == null) {
            return 0;
        }

        Object specialBar = clientScriptData.get(686);

        if (specialBar instanceof Integer) {
            return (Integer) specialBar;
        }

        return 0;
    }

    /**
     * Method getRenderAnimId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getRenderAnimId() {
        if (clientScriptData == null) {
            return 1426;
        }

        Object animId = clientScriptData.get(644);

        if (animId instanceof Integer) {
            return (Integer) animId;
        }

        return 1426;
    }

    /**
     * Method getQuestId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getQuestId() {
        if (clientScriptData == null) {
            return -1;
        }

        Object questId = clientScriptData.get(861);

        if ((questId != null) && (questId instanceof Integer)) {
            return (Integer) questId;
        }

        return -1;
    }

    /**
     * Method test
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void test() {
        if (clientScriptData == null) {
            return;
        }

        for (Object o : clientScriptData.entrySet()) {
            System.out.println(o);
        }
    }

    /**
     * Method getWearingSkillRequiriments
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public HashMap<Integer, Integer> getWearingSkillRequiriments() {
        if (clientScriptData == null) {
            return null;
        }

        HashMap<Integer, Integer> skills    = new HashMap<Integer, Integer>();
        int                       nextLevel = -1;
        int                       nextSkill = -1;

        for (int key : clientScriptData.keySet()) {
            Object value = clientScriptData.get(key);

            if (value instanceof String) {
                continue;
            }

            if ((key >= 749) && (key < 797)) {
                if (key % 2 == 0) {
                    nextLevel = (Integer) value;
                } else {
                    nextSkill = (Integer) value;
                }

                if ((nextLevel != -1) && (nextSkill != -1)) {
                    skills.put(nextSkill, nextLevel);
                    nextLevel = -1;
                    nextSkill = -1;
                }
            }
        }

        return skills;
    }

    private void setDefaultsVariableValules() {}

    /**
     * Method getMediumInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buffer
     *
     * @return
     */
    public static int getMediumInt(ByteBuffer buffer) {
        return ((buffer.get() & 0xFF) << 16) | ((buffer.get() & 0xFF) << 8) | (buffer.get() & 0xFF);
    }

    private void setDefaultOptions() {
        groundOptions    = new String[] { null, null, "take", null, null };
        inventoryOptions = new String[] { null, null, null, null, "drop" };
    }

    private void readValues(ByteBuffer buffer, int opcode) {
        switch (opcode) {
        case 4 :
            modelZoom = buffer.getShort() & 0xFFFF;

            break;

        case 5 :
            modelRotation1 = buffer.getShort() & 0xFFFF;

            break;

        case 6 :
            modelRotation2 = buffer.getShort() & 0xFFFF;

            break;

        case 11 :
            stackable = 1;

            break;

        case 12 :
            value = buffer.getInt();

            break;

        case 23 :
            maleWornModelId1 = buffer.getShort() & 0xFFFF;

            break;

        case 25 :
            maleWornModelId2 = buffer.getShort() & 0xFFFF;

            break;

        case 26 :
            femaleWornModelId2 = buffer.getShort() & 0xFFFF;

            break;

        case 35 :
        case 36 :
        case 37 :
        case 38 :
        case 39 :
            readRS2String(buffer);

            break;

        case 40 : {
            int length = buffer.get() & 0xFF;

            originalModelColors = new int[length];
            modifiedModelColors = new int[length];

            for (int index = 0; index < length; index++) {
                originalModelColors[index] = buffer.getShort() & 0xFFFF;
                modifiedModelColors[index] = buffer.getShort() & 0xFFFF;
            }

            break;
        }

        case 41 : {
            int length = buffer.get() & 0xFF;

            textureColour1 = new int[length];
            textureColour2 = new int[length];

            for (int index = 0; index < length; index++) {
                textureColour1[index] = buffer.getShort() & 0xFFFF;
                textureColour2[index] = buffer.getShort() & 0xFFFF;
            }

            break;
        }

        case 42 : {
            int length = buffer.get() & 0xFF;

            unknownArray1 = new byte[length];

            for (int index = 0; index < length; index++) {
                unknownArray1[index] = buffer.get();
            }

            break;
        }

        case 65 :
            unnoted = true;

            break;

        case 78 :
            colourEquip1 = buffer.getShort() & 0xFFFF;

            break;

        case 79 :
            colourEquip2 = buffer.getShort() & 0xFFFF;

            break;

        case 91 :
            buffer.getShort();

            break;

        case 98 :
            certTemplateId = buffer.getShort() & 0xFFFF;

            break;

        case 110 :
            buffer.getShort();

            break;

        case 111 :
            buffer.getShort();

            break;

        case 115 :
            teamId = buffer.get() & 0xFF;

            break;

        case 122 :
            lendTemplateId = buffer.getShort() & 0xFFFF;

            break;

        case 130 :
            buffer.get();
            buffer.getShort();

            break;

        case 139 :
            buffer.getShort();

            break;

        case 249 : {
            int length = buffer.get() & 0xFF;

            if (clientScriptData == null) {
                clientScriptData = new HashMap<Integer, Object>();
            }

            for (int index = 0; index < length; index++) {
                boolean stringInstance = buffer.get() == 1;
                int     key            = getMediumInt(buffer);
                Object  value          = stringInstance
                                         ? readRS2String(buffer)
                                         : buffer.getInt();

                clientScriptData.put(key, value);
            }

            break;
        }

        case 140 :
            buffer.getShort();

            break;

        case 134 :
            buffer.get();

            break;

        case 132 : {
            int length = buffer.get() & 0xFF;

            unknownArray2 = new int[length];

            for (int index = 0; index < length; index++) {
                unknownArray2[index] = buffer.getShort() & 0xFFFF;
            }

            break;
        }

        case 129 :
        case 128 :
        case 127 :
            buffer.get();
            buffer.getShort();

            break;

        case 126 :
        case 125 :
            buffer.get();
            buffer.get();
            buffer.get();

            break;

        case 121 :
            lendId = buffer.getShort() & 0xFFFF;

            break;

        case 114 :
            buffer.get();

            break;

        case 113 :
            buffer.get();

            break;

        case 112 :
            buffer.getShort();

            break;

        case 100 :
        case 101 :
        case 102 :
        case 103 :
        case 104 :
        case 105 :
        case 106 :
        case 107 :
        case 108 :
        case 109 :
            if (stackIds == null) {
                stackIds     = new int[10];
                stackAmounts = new int[10];
            }

            stackIds[opcode - 100]     = buffer.getShort() & 0xFFFF;
            stackAmounts[opcode - 100] = buffer.getShort() & 0xFFFF;

            break;

        case 96 :
            buffer.get();

            // certId = buffer.getShort();
            break;

        case 97 :
            certTemplateId = buffer.getShort();

            break;

        case 95 :
        case 93 :
        case 92 :
        case 90 :    // unknown
            buffer.getShort();

            break;

        case 30 :
        case 31 :
        case 32 :
        case 33 :
        case 34 :
            groundOptions[opcode - 30] = readRS2String(buffer);

            break;

        case 24 :
            femaleWornModelId1 = buffer.getShort();

            break;

        case 18 :
            buffer.getShort();

            break;

        case 16 :
            membersOnly = true;

            break;

        case 8 :
            modelOffset2 = buffer.getShort() & 0xFFFF;

            if (modelOffset2 > 32767) {
                modelOffset2 -= 65536;
            }

            modelOffset2 <<= 0;

            break;

        case 7 :
            modelOffset1 = buffer.getShort() & 0xFFFF;

            if (modelOffset1 > 32767) {
                modelOffset1 -= 65536;
            }

            modelOffset1 <<= 0;

            break;

        case 2 :
            name = readRS2String(buffer);

            break;

        case 1 :
            interfaceModelId = buffer.getShort() & 0xFFFF;

            break;
        }
    }

    /**
     * Method readRS2String
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buffer
     *
     * @return
     */
    public static String readRS2String(ByteBuffer buffer) {
        StringBuilder sb = new StringBuilder();
        byte          b;

        while ((buffer.remaining() > 0) && (b = buffer.get()) != 0) {
            sb.append((char) b);
        }

        return sb.toString();
    }

    private void readOpcodeValues(ByteBuffer buffer) {
        while (true) {
            int opcode = buffer.get() & 0xFF;

            if (opcode == 0) {
                break;
            }

            readValues(buffer, opcode);
        }
    }

    /**
     * Method getId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Method getInterfaceModelId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getInterfaceModelId() {
        return interfaceModelId;
    }

    /**
     * Method getName
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Method getModelZoom
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModelZoom() {
        return modelZoom;
    }

    /**
     * Method getModelRotation1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModelRotation1() {
        return modelRotation1;
    }

    /**
     * Method getModelRotation2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModelRotation2() {
        return modelRotation2;
    }

    /**
     * Method getModelOffset1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModelOffset1() {
        return modelOffset1;
    }

    /**
     * Method getModelOffset2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getModelOffset2() {
        return modelOffset2;
    }

    /**
     * Method getStackable
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getStackable() {
        return stackable;
    }

    /**
     * Method getValue
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getValue() {
        return value;
    }

    /**
     * Method isMembersOnly
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isMembersOnly() {
        return membersOnly;
    }

    /**
     * Method getMaleWornModelId1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaleWornModelId1() {
        return maleWornModelId1;
    }

    /**
     * Method getFemaleWornModelId1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFemaleWornModelId1() {
        return femaleWornModelId1;
    }

    /**
     * Method getMaleWornModelId2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getMaleWornModelId2() {
        return maleWornModelId2;
    }

    /**
     * Method getFemaleWornModelId2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getFemaleWornModelId2() {
        return femaleWornModelId2;
    }

    /**
     * Method getGroundOptions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getGroundOptions() {
        return groundOptions;
    }

    /**
     * Method getInventoryOptions
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String[] getInventoryOptions() {
        return inventoryOptions;
    }

    /**
     * Method getOriginalModelColors
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getOriginalModelColors() {
        return originalModelColors;
    }

    /**
     * Method getModifiedModelColors
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getModifiedModelColors() {
        return modifiedModelColors;
    }

    /**
     * Method getTextureColour1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getTextureColour1() {
        return textureColour1;
    }

    /**
     * Method getTextureColour2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getTextureColour2() {
        return textureColour2;
    }

    /**
     * Method getUnknownArray1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte[] getUnknownArray1() {
        return unknownArray1;
    }

    /**
     * Method getUnknownArray2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getUnknownArray2() {
        return unknownArray2;
    }

    /**
     * Method isUnnoted
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean isUnnoted() {
        return unnoted;
    }

    /**
     * Method getColourEquip1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getColourEquip1() {
        return colourEquip1;
    }

    /**
     * Method getColourEquip2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getColourEquip2() {
        return colourEquip2;
    }

    /**
     * Method getCertId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCertId() {
        return certId;
    }

    /**
     * Method getCertTemplateId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getCertTemplateId() {
        return certTemplateId;
    }

    /**
     * Method getStackIds
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getStackIds() {
        return stackIds;
    }

    /**
     * Method getStackAmounts
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int[] getStackAmounts() {
        return stackAmounts;
    }

    /**
     * Method getTeamId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getTeamId() {
        return teamId;
    }

    /**
     * Method getLendId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLendId() {
        return lendId;
    }

    /**
     * Method getLendTemplateId
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLendTemplateId() {
        return lendTemplateId;
    }
}
