package net.tazogaming.hydra.io.js5.stream;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class ByteInputStream {
    public byte[] buffer;
    public int    pos;

    /**
     * Constructs ...
     *
     *
     * @param data
     */
    public ByteInputStream(byte[] data) {
        buffer = data;
        pos    = 0;
    }

    /**
     * Method readSmart2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readSmart2() {
        int i     = 0;
        int i_33_ = readSmart();

        while (i_33_ == 32767) {
            i_33_ = readSmart();
            i     += 32767;
        }

        i += i_33_;

        return i;
    }

    /**
     * Method readSmart
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readSmart() {
        int i = buffer[pos] & 0xff;

        if (i >= 128) {
            return readUShort() - 32768;
        }

        return readUByte();
    }

    /**
     * Method readUShort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readUShort() {
        pos += 2;

        return ((buffer[pos - 2] & 0xff) << 8) | (buffer[pos - 1] & 0xff);
    }

    /**
     * Method readByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readByte() {
        return buffer[pos++];
    }

    /**
     * Method readUByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readUByte() {
        return buffer[pos++] & 0xff;
    }
}
