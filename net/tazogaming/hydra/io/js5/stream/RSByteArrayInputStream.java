package net.tazogaming.hydra.io.js5.stream;

//~--- JDK imports ------------------------------------------------------------

import java.io.ByteArrayInputStream;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class RSByteArrayInputStream extends ByteArrayInputStream {

    /**
     * Constructs ...
     *
     *
     * @param buf
     */
    public RSByteArrayInputStream(byte[] buf) {
        super(buf);
    }

    /**
     * Constructs ...
     *
     *
     * @param buf
     * @param offset
     * @param length
     */
    public RSByteArrayInputStream(byte[] buf, int offset, int length) {
        super(buf, offset, length);
    }

    /**
     * Method seek
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public void seek(int i) {
        pos = i;
    }

    /**
     * Method position
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int position() {
        return pos;
    }
}
