package net.tazogaming.hydra.io.js5.stream;

//~--- JDK imports ------------------------------------------------------------

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class RSInputStream extends DataInputStream {

    /**
     * Constructs ...
     *
     *
     * @param in
     */
    public RSInputStream(InputStream in) {
        super(in);
    }

    /**
     * Method readRS2String
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public String readRS2String() throws IOException {
        StringBuilder sb = new StringBuilder();
        byte          b;

        while ((b = readByte()) != 0) {
            sb.append((char) b);
        }

        return sb.toString();
    }

    /**
     * Method readShortA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int readShortA() throws IOException {
        return (((readByte() & 0xff) << 8) + (readByte() - 128 & 0xff));
    }

    /**
     * Method readShortLEA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int readShortLEA() throws IOException {
        byte bl = readByte();
        byte bh = readByte();

        return ((bl - 128 & 0xff)) + ((bh & 0xff) << 8);
    }

    /**
     * Method readSByteA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public byte readSByteA() throws IOException {
        return (byte) (read() - 128);
    }

    /**
     * Method readSByteC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public byte readSByteC() throws IOException {
        return (byte) (-read());
    }

    /**
     * Method readSByteS
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public byte readSByteS() throws IOException {
        return (byte) (128 - read());
    }

    /**
     * Method readByteA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int readByteA() throws IOException {
        return (readUnsignedByte() - 128 & 0xff);
    }

    /**
     * Method readByteC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int readByteC() throws IOException {
        return -(readUnsignedByte() & 0xff);
    }

    /**
     * Method readByteS
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int readByteS() throws IOException {
        return (128 - readUnsignedByte() & 0xff);
    }

    /**
     * Method read24BitInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int read24BitInt() throws IOException {
        return ((read() & 0xff) << 16) + ((read() & 0xff) << 8) + (read() & 0xff);
    }

    /**
     * Method readSShort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int readSShort() throws IOException {
        int i_54_ = readShort();

        if (i_54_ > 32767) {
            i_54_ -= 65536;
        }

        return i_54_;
    }

    /**
     * Method seek
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i_18_
     */
    public void seek(int i_18_) {
        ((RSByteArrayInputStream) in).seek(i_18_);
    }

    /**
     * Method readUnsignedSmart
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int readUnsignedSmart() throws IOException {
        int i = readUnsignedByte();

        seek(((RSByteArrayInputStream) in).position() - 1);

        if (i >= 128) {
            return -32768 + readUnsignedShort();
        }

        return readUnsignedByte();
    }
}
