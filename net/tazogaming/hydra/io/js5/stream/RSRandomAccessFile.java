package net.tazogaming.hydra.io.js5.stream;

//~--- JDK imports ------------------------------------------------------------

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class RSRandomAccessFile extends RandomAccessFile {

    /**
     * Constructs ...
     *
     *
     * @param file
     * @param mode
     *
     * @throws FileNotFoundException
     */
    public RSRandomAccessFile(File file, String mode) throws FileNotFoundException {
        super(file, mode);
    }

    /**
     * Constructs ...
     *
     *
     * @param name
     * @param mode
     *
     * @throws FileNotFoundException
     */
    public RSRandomAccessFile(String name, String mode) throws FileNotFoundException {
        super(name, mode);
    }

    /**
     * Method read24BitInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws IOException
     */
    public int read24BitInt() throws IOException {
        int i = readByte() << 16;

        i += readByte() << 8;

        return i + readByte();
    }

    /**
     * Method write24BitInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     *
     * @throws IOException
     */
    public void write24BitInt(int i) throws IOException {
        writeByte(i >> 16);
        writeByte(i >> 8);
        writeByte(i);
    }
}
