package net.tazogaming.hydra.io.js5;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.js5.bzip.BZ2Decompressor;
import net.tazogaming.hydra.io.js5.bzip.GZIPDecompressor;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class CacheContainer {
    private byte[] bytes;
    private int    length;
    private byte   compression;
    private int    decompressedLength;

    /**
     * Constructs ...
     *
     *
     * @param bytes
     *
     * @throws IOException
     */
    public CacheContainer(byte[] bytes) throws IOException {
        this.bytes  = bytes;
        compression = bytes[0];
        length      = readInt(1, bytes);

        if (compression != 0) {
            decompressedLength = readInt(5, bytes);
        } else {
            decompressedLength = length;
        }
    }

    /**
     * Method readInt
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param index
     * @param buffer
     *
     * @return
     */
    public static int readInt(int index, byte[] buffer) {
        return ((buffer[index++] & 0xff) << 24) | ((buffer[index++] & 0xff) << 16) | ((buffer[index++] & 0xff) << 8)
               | (buffer[index++] & 0xff);
    }

    /**
     * Method decompress
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     *
     * @throws Exception
     */
    public byte[] decompress() throws Exception {
        if (decompressedLength > 1000000) {
            return null;
        }

        byte[] data = new byte[decompressedLength];

        switch (compression) {
        case 0 :
            System.arraycopy(bytes, 5, data, 0, decompressedLength);

            break;

        case 1 :
            BZ2Decompressor.decompress(length, 9, bytes, data);

            break;

        default :
            GZIPDecompressor.decompress(length, 9, bytes, data);

            break;
        }

        return data;
    }
}
