package net.tazogaming.hydra.io.message;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.ChatMessage;
import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.clan.ClanChannel;
import net.tazogaming.hydra.util.Text;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 21/10/13
 * Time: 18:23
 */
public class PlayerMessage {
    public static final int
        TYPE_CLAN_MESSAGE    = 0,
        TYPE_YELL_MESSAGE    = 1,
        TYPE_PRIVATE_MESSAGE = 2;
    private long        target;
    private String      sender;
    private String      messageData_string;
    private int         playerRights;
    private byte[]      messageData;
    private int         type;
    private int iron_mode = 0;
    private ClanChannel channel;

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param rights
     * @param data
     * @param sender
     * @param target
     */
    public PlayerMessage(int type, int rights, byte[] data, String sender, long target) {
        this.sender = sender;

        if (((rights == Player.PATRON) || (rights == Player.OFFICIAL_HELPER)) && (type == TYPE_PRIVATE_MESSAGE)) {
            rights = 0;
        }

        this.playerRights = rights;
        this.messageData  = data;
        this.type         = type;
        this.target       = target;
    }
    public void setIronMode(int mode){
        this.iron_mode = mode;
    }

    public int getIronMode() {
        return this.iron_mode;
    }
    private String title = "";

    public void setTitle(String title){
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param rights
     * @param data
     * @param sender
     * @param target
     */
    public PlayerMessage(int type, int rights, byte[] data, String sender, String target) {
        this(type, rights, data, sender, ((target != null)
                                          ? Text.stringToLong(target)
                                          : 0));
    }

    /**
     * Constructs ...
     *
     *
     * @param type
     * @param message
     * @param sender
     * @param chan
     * @param rights
     */
    public PlayerMessage(int type, String message, String sender, ClanChannel chan, int rights) {
        messageData_string = message;
        this.playerRights  = rights;
        this.sender        = sender;
        this.type          = type;
        this.channel       = chan;
    }

    /**
     * Method getChannel
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public ClanChannel getChannel() {
        return channel;
    }

    /**
     * Method getMsg
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getMsg() {
        return ChatMessage.textUnpack(messageData, messageData.length);
    }

    /**
     * Method getTarget
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Long getTarget() {
        return target;
    }

    /**
     * Method getMessageData_string
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getMessageData_string() {
        return messageData_string;
    }

    /**
     * Method setMessageData_string
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param messageData_string
     *
     * @return
     */
    public PlayerMessage setMessageData_string(String messageData_string) {
        this.messageData_string = messageData_string;

        return this;
    }

    /**
     * Method getSender
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String getSender() {
        return sender;
    }

    /**
     * Method setSender
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sender
     */
    public void setSender(String sender) {
        this.sender = sender;
    }

    /**
     * Method getMessageData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte[] getMessageData() {
        return messageData;
    }

    /**
     * Method setMessageData
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param messageData
     */
    public void setMessageData(byte[] messageData) {
        this.messageData = messageData;
    }

    /**
     * Method getType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getType() {
        return type;
    }

    /**
     * Method setType
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param type
     */
    public void setType(int type) {
        this.type = type;
    }

    /**
     * Method getPlayerRights
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getPlayerRights() {
        return playerRights;
    }

    /**
     * Method setPlayerRights
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param playerRights
     */
    public void setPlayerRights(int playerRights) {
        this.playerRights = playerRights;
    }
}
