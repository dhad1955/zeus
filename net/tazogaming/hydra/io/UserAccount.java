package net.tazogaming.hydra.io;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.*;
import net.tazogaming.hydra.entity3d.player.PlayerAppearance;
import net.tazogaming.hydra.game.CompletionistCape;
import net.tazogaming.hydra.game.Referral;
import net.tazogaming.hydra.game.minigame.achievement.Achievement;
import net.tazogaming.hydra.io.tfs.Archive;
import net.tazogaming.hydra.io.tfs.SaveGameFactory;
import net.tazogaming.hydra.map.Point;
import net.tazogaming.hydra.net.PlaySession;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.net.packetbuilder.MessageBuilder;
import net.tazogaming.hydra.security.Authentication;
import net.tazogaming.hydra.security.Authenticator;
import net.tazogaming.hydra.security.Strike;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.test.performancetest.BotPlayer;
import net.tazogaming.hydra.util.SystemUpdate;
import net.tazogaming.hydra.util.WorldList;
import net.tazogaming.hydra.util.background.BackgroundServiceRequest;
import net.tazogaming.hydra.util.math.GameMath;

//~--- JDK imports ------------------------------------------------------------

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 23/09/13
 * Time: 18:22
 * To change this template use File | Settings | File Templates.
 */
public class UserAccount implements PlayerLoader {
    public static ArrayList<Strike> strikes           = new ArrayList<Strike>();
    public static int               NEW_ACCOUNTS      = 0;
    public static final int         DATA_FILE_VERSION = 1;
    public static final int
        OPCODE_PLAYER_POSITION                        = 0,
        OPCODE_INVENTORY                              = 1,
        OPCODE_EQUIPMENT                              = 2,
        OPCODE_SKILLS                                 = 3,
        OPCODE_SETTINGS                               = 4,
        OPCODE_TIMERS                                 = 5,
        OPCODE_EOF                                    = 12,
        OPCODE_HP                                     = 6,
        OPCODE_PRAYER                                 = 7,
        OPCODE_POISON                                 = 8,
        OPCODE_BANKDATA                               = 9,
        OPCODE_APPEARANCE                             = 10,
        OPCODE_PLAYERVARS                             = 11,
        OPCODE_PRIVACY_SETTINGS                       = 13,
        OPCODE_FRIENDS                                = 14,
        OPCODE_IGNORES                                = 15,
        OPCODE_FAMILIAR_DATA                          = 16,
        OPCODE_QUESTS                                 = 17,
        OPCODE_SLAYERINFO                             = 18,
        OPCODE_POINTS                                 = 19,
        OPCODE_PLAYTIME                               = 20,
        OPCODE_PK_KILLS                               = 21,
        OPCODE_DEGRADEABLES                           = 22,
        OPCODE_SKILLING_TASK                          = 23,
        OPCODE_RENTALS                                = 24,
        OPCODE_POH                                    = 25,
        OPCODE_COSTUMES                               = 26,
        OPCODE_DATED_TIMERS                           = 27,
        OPCODE_LOADOUTS                               = 28,
        OPCODE_ACHIEVEMENTS                           = 29,
        OPCODE_FRIENDS_IGNORES                        = 30;
    public static final int
        RESPONSE_SUCCESS                              = 2,
        RESPONSE_WAIT                                 = 1,
        RESPONSE_ALREADY_LOGGED                       = 5,
        RESPONSE_BANNED                               = 4,
        RESPONSE_INVALID_PASS                         = 3,
        RESPONSE_UPDATE                               = 6,
        RESPONSE_TOO_MANY                             = 9;
    public static final int
        GROUP_ADMIN                                   = 4,
        GROUP_DEV                                     = 9,
        GROUP_MANAGER                                 = 11,
        GROUP_BANNED                                  = 5,
        GROUP_DONOR                                   = 14,
        GROUP_PLAT                                    = 15,
        GROUP_PATRON                                  = 16,
        GROUP_QC                                      = 18,
        GROUP_SERVER_HELPER                           = 13,
        GROUP_MOD                                     = 7,
        GROUP_FORUM_MANAGER                           = 7,
        GROUP_HEAD_MOD                                = 12;
    public static final int
        CROWN_MOD                                     = 1,
        CROWN_ADMIN                                   = 2,
        CROWN_PATRON                                  = 4,
        CROWN_SUPPORT                                 = 5,
        CROWN_dONOR                                   = 6,
        CROWN_CMANAGER                                = 7,
        CROWN_DIRECTOR                                = 8,
        CROWN_PLATINUM                                = 9,
        CROWN_YOUTUBER                                = 10,
        CROWN_HEADMOD                                 = 7,
        CROWN_ULT_IRON_MAN = 13,
        CROWN_IRONMAN = 14;
    public static final int
        REFERAL_STATUS_NEW                            = 0,
        REFERRAL_STATUS_PENDING                       = 1,
        REFERRAL_STATUS_COMPLETE                      = 2,
        REFERRAL_STATUS_BANNED                        = 3;

    private static void checkReferrals(Player player, MysqlConnection databaseLink) {
        try {
            ResultSet res = databaseLink.query("SELECT uid, ip from logins WHERE ip='"
                                               + player.getIoSession().getUserIP() + "' or uid='" + player.getUID()
                                               + "'");
            if (res.next()) {
                res.close();
                return;
            }

            res = databaseLink.query("SELECT * FROM referrals WHERE uid='" + player.getUID() + "' OR ip='"
                                     + player.getIoSession().getUserIP() + "' order by state desc");

            if (res.next()) {
                if (res.getInt("state") == 0) {
                    databaseLink.query("UPDATE referrals SET uid='" + player.getUID() + "' state=1 where id="
                                       + res.getInt("id"));
                    player.getAccount().setReferral(new Referral(res.getInt("userid"), 0));
                    res.close();
                } else if (res.getInt("state") == 1) {
                    player.getAccount().setReferral(new Referral(res.getInt("userid"), 0));
                    res.close();
                } else {
                    res.close();

                    return;
                }
            } else {
                res.close();
            }
        } catch (Exception error) {
            error.printStackTrace();
        }
    }

    /**
     * Method getCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     *
     * @return
     */
    public static int getCount(long uid) {
        int c = 0;

        for (Strike strike : strikes) {
            if (strike.getUID() == uid) {
                return strike.getCount();
            }
        }

        return c;
    }

    /**
     * Method serverGroupForMemberGroup
     * Created on 14/08/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param groupID
     *
     * @return
     */
    public static final int serverGroupForMemberGroup(int groupID) {
        switch (groupID) {
        case GROUP_MOD :
        case GROUP_HEAD_MOD :
            return Player.MODERATOR;

        case GROUP_ADMIN :
        case GROUP_DEV :
        case GROUP_MANAGER :
            return Player.ADMINISTRATOR;

        case GROUP_QC :
        case GROUP_PATRON :
            return Player.PATRON;

        case GROUP_DONOR :
            return Player.DONATOR;

        case GROUP_PLAT :
            return Player.PLATINUM;

        case GROUP_SERVER_HELPER :
            return Player.OFFICIAL_HELPER;
        }

        return 0;
    }

    /**
     * Method saveLogs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param instant
     */
    public static void saveLogs(final Player player, boolean instant) {
        final String[] tmp = new String[player.getLogs().size()];

        player.getLogs().toArray(tmp);

        BackgroundServiceRequest on = new BackgroundServiceRequest() {
            @Override
            public boolean compute() {
                BufferedWriter writer = null;

                try {
                    writer = new BufferedWriter(new FileWriter("config/logs/" + player.getUsername().toLowerCase()
                            + ".txt", true));

                    for (String str : tmp) {
                        writer.write(str);
                        writer.newLine();
                    }
                } catch (Exception ee) {
                    ee.printStackTrace();
                } finally {
                    if (writer != null) {
                        try {
                            writer.close();
                        } catch (Exception ee) {}
                    }
                }

                return true;
            }
        };

        if (instant) {
            on.compute();
        } else {
            World.getWorld().getBackgroundService().submitRequest(on);
        }
    }

    /**
     * Method addStrike
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param uid
     */
    public void addStrike(long uid) {
        boolean added = false;

        for (Strike strike : strikes) {
            if (strike.getUID() == uid) {
                strike.updateCount();
                strike.updateTime();

                return;
            }
        }

        strikes.add(new Strike(uid, System.currentTimeMillis()));
    }

    /**
     * Method updateStrikes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void updateStrikes() {
        for (int i = 0; i < strikes.size(); i++) {
            if (System.currentTimeMillis() - strikes.get(i).getTime() < 300000) {
                strikes.remove(i);
            }
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param con
     *
     * @throws UnsupportedOperationException
     */
    @Override
    public void save(Player p, MysqlConnection con) throws UnsupportedOperationException {
        build_save_game(p);

        if (p instanceof BotPlayer) {
            return;
        }

        int streak = 0;
        int kills  = 0;
        int deaths = 0;
        int rank   = p.getRank();
        String email = "";

        if (p.getAccount().hasVar("kills")) {
            kills = p.getAccount().getInt32("kills");
        }
        if(p.getAccount().hasVar("email"))
            email = p.getAccount().getString("email");

        if (p.getAccount().hasVar("deaths")) {
            deaths = p.getAccount().getInt32("deaths");
        }

        if (p.getAccount().hasVar("pk_streak")) {
            streak = p.getAccount().getInt32("pk_streak");
        }

        int ironman = p.getAccount().getIronManMode();

        if (!p.getAccount().isPkMode() &&!SystemUpdate.isRunning()) {
            for (int i = 0; i < 25; i++) {
                con.query("UPDATE hs_skills SET level='" + p.getMaxStat(i) + "', experience='" + p.getExps()[i]
                          + "', prestige='0', ironman=" + ironman + " where skillid='" + i + "' and userid='"
                          + p.getId() + "'");
            }
        }

        try {
            int completionist_cape_completed = CompletionistCape.getTotalCompleted(p);
            int completionist_cape_percent   = CompletionistCape.getCompletionPercent(p);
            int achievements_completed       = p.getAchievementsCompletedTotal();
            int achievements_percent         = (int)GameMath.getPercentFromTotal(achievements_completed,
                                                   Achievement.ACHIEVEMENTS.length);
            PreparedStatement statement =
                con.prepareStatement(
                    "UPDATE `user` SET `kills` = ?, `deaths` = ?, streak = ?, comp_complete = ?, comp_percent = ?, achievement_complte = ?,  achievement_perc = ?, online=? WHERE id='"
                    + p.getId() + "'");

            statement.setInt(1, kills);
            statement.setInt(2, deaths);
            statement.setInt(3, streak);
            statement.setInt(4, completionist_cape_completed);
            statement.setInt(5, completionist_cape_percent);
            statement.setInt(6, achievements_completed);
            statement.setInt(7, achievements_percent);
            statement.setInt(8, 0);
            statement.executeUpdate();
            System.err.println("updated satement" );
        } catch (Exception ee) {
            ee.printStackTrace();
        }

        saveLogs(p, true);
    }

    /**
     * Method newPlayer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param con
     * @param id
     *
     * @return
     */
    public static byte newPlayer(Player p, MysqlConnection con, int id) {
        p.setAppearance(new PlayerAppearance(3, 19, 29, 35, 39, 44, 7, 8, 9, 5, 0, 0, 14));
        p.setLocation(Point.location(3211, 3424, 0), true);

        int[] stats = new int[25];
        int[] exps  = new int[25];

        Arrays.fill(stats, 1);
        p.setCurStats(stats);
        Arrays.fill(exps, 1);    // exp
        p.setExps(exps);
        stats[3] = 10;
        exps[3]  = GameMath.getXPForLevel(10);
        p.setMaxStat(3, 10);
        p.getAccount().setDefaults();
        p.setCurrentHealth(10);
        p.getPrayerBook().setPoints(1);

        return 2;
    }

    /**
     * Method setDefaults
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void setDefaults(Player p) {
        p.setAppearance(new PlayerAppearance(3, 19, 29, 35, 39, 44, 7, 8, 9, 5, 0, 0, 14));

//      p.setLocation(Point.location(3211, 3424, 0), true);

        int[] stats = new int[25];
        int[] exps  = new int[25];

        Arrays.fill(stats, 1);
        p.setCurStats(stats);
        Arrays.fill(exps, 1);    // exp
        p.setExps(exps);
        stats[3] = 10;
        exps[3]  = GameMath.getXPForLevel(10);
        p.getAccount().setDefaults();
        p.setCurrentHealth(10);
        p.getPrayerBook().setPoints(1);
    }

    /**
     * Method GET_CROWN
     * Created on 14/08/31
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param group
     *
     * @return
     */
    public static final int GET_CROWN(int group, int ironmode) {

        if(ironmode != 0 && group < Player.MODERATOR)
        {
            return ironmode == 1 ? 15 : 14;
        }
        switch (group) {
        case Player.ADMINISTRATOR :
        case Player.DEVELOPER :
            return CROWN_ADMIN;

        case Player.EXTREME_DONATOR :
            return 13;

        case Player.GOLDEN_YOUTUBER :
            return 11;

        case Player.HEAD_MODERATOR :
            return 12;

        case Player.COMMUNITY_MANAGER :
            return CROWN_HEADMOD;

        case Player.PROMOTER :
            return CROWN_YOUTUBER;

        case Player.ROOT_ADMIN :
            return CROWN_DIRECTOR;

        case Player.CONTENT_MANAGER :
            return CROWN_CMANAGER;

        case Player.OFFICIAL_HELPER :
            return CROWN_SUPPORT;

        case Player.MODERATOR :
            return CROWN_MOD;

        case Player.PATRON :
            return CROWN_PATRON;

        case Player.DONATOR :
            return CROWN_dONOR;

        case Player.PLATINUM :
            return CROWN_PLATINUM;
        }

        return -2;
    }

    private void sendLobbyResponse(Player player, int opcode) {
        PlaySession session = player.getIoSession();

        session.write(new MessageBuilder().setBare(true).addByte((byte) opcode).toPacket());

        if (opcode != LoadResult.OK) {
            return;
        }

        MessageBuilder bldr = new MessageBuilder();

        bldr.addByte((byte) 0);
        bldr.addByte((byte) 0);
        bldr.addByte((byte) 0);
        bldr.addByte((byte) 0);
        bldr.addByte((byte) 0);
        bldr.addShort(0);       // member days left
        bldr.addShort(0);       // recovery questions
        bldr.addShort(0);       // unread messages
        bldr.addShort(3229);    // 3229 - lastDays

        int ipHash = 444;

        bldr.addInt(ipHash);       // last ip
        bldr.addByte((byte) 0);    // email status (0 - no email, 1 - pending

        // parental confirmation, 2 - pending
        // confirmation, 3 - registered)
        bldr.addShort(0);
        bldr.addShort(0);
        bldr.addByte((byte) 0);
        bldr.putGJString("fagget");
        bldr.addByte((byte) 0);
        bldr.addInt(1);
        bldr.addShort(1);    // current world id
        bldr.putGJString("127.0.0.1");

        MessageBuilder lbyResponse = new MessageBuilder();

        lbyResponse.addByte(bldr.getLength());
        lbyResponse.addBytes(bldr.toPacket().getData());
        lbyResponse.setBare(true);
        bldr.setBare(true);
        session.write(lbyResponse.toPacket());

        // session.getChannel().getPipeline().remove("decoder");
        session.write(WorldList.getData(true, true));
    }

    /**
     * Method getLoadResult
     * Created on 14/08/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param player
     * @param isLobby
     * @param connection
     *
     * @return
     */
    public byte getLoadResult(Player player, boolean isLobby, MysqlConnection connection) {
        try {
            Player otherPlayer = World.getWorld().getPlayer(player.getUsernameHash());

            if ((otherPlayer != null) && (otherPlayer.getDisconTime() == 0)) {
                if (isLobby) {
                    if (otherPlayer == player) {
                        throw new RuntimeException("error!!!");
                    }

                    otherPlayer.packetTracked = true;
                    System.err.println("already online: " + player.getUsername() + " " + otherPlayer.getUsername()
                                       + " " + otherPlayer.getIoSession().isConnected() + " "
                                       + (System.currentTimeMillis() - otherPlayer.getLastPacket()) + " | "
                                       + otherPlayer.getIoSession().getChannel().isBound() + " "
                                       + otherPlayer.getIoSession().getChannel().isWritable() + " "
                                       + otherPlayer.getIoSession().getChannel().isReadable());
                    sendLobbyResponse(player, LoadResult.ACCOUNT_STILL_ONLINE);
                }

                System.err.println("already online: " + player.getUsername() + " " + otherPlayer.getUsername() + " "
                                   + otherPlayer.getIoSession().isConnected());

                return LoadResult.ACCOUNT_STILL_ONLINE;
            }

            if (Main.TEST_MODE) {
                newPlayer(player, null, 0);
                player.setId(0);
                player.setRights(11);

                return LoadResult.OK;
            }

            Authenticator<Player, MysqlConnection> mysqlAuth = new Authentication();
            LoadResult                             result    = mysqlAuth.getResult(player, connection);

            if (isLobby) {
                sendLobbyResponse(player, result.getCode());

                return (byte) result.getCode();
            }

            if (result.getCode() == LoadResult.OK) {
                try {
                    setDefaults(player);
                    player.packetTracked = false;

                    if (otherPlayer != null) {
                        player.setHashingFrom(otherPlayer);

                        return LoadResult.OK;
                    }

                    ResultSet resultSet = connection.query("SELECT uid from logins WHERE uid='" + player.getUID()
                                              + "' AND username='" + player.getUsername() + "'");

                    if (!resultSet.next()) {
                        connection.query("INSERT into logins(id, username, ip, uid) VALUES(null, '"
                                         + player.getUsername() + "', '" + player.getIoSession().getUserIP() + "', '"
                                         + player.getUID() + "');");
                    }

                    if (!load_save_game(player)) {
                        build_save_game(player);
                    }

                    resultSet.close();

                    return (byte) result.getCode();
                } catch (Exception ee) {
                    ee.printStackTrace();

                    return LoadResult.ERROR_LOADING_PROFILE;
                }
            }

            return (byte) result.getCode();
        } catch (Exception ee) {
            ee.printStackTrace();

            return LoadResult.ERROR_LOADING_PROFILE;
        }
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param connection
     * @param lobby
     *
     * @return
     *
     * @throws UnsupportedOperationException
     */
    @Override
    public byte load(Player p, MysqlConnection connection, boolean lobby) throws UnsupportedOperationException {
        return getLoadResult(p, lobby, connection);
    }

    /**
     * Method build_save_game
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pla
     *
     * @return
     */
    public boolean build_save_game(Player pla) {
        SaveGameFactory codec = new SaveGameFactory(pla);

        try {
            codec.save();
        } catch (Exception ee) {
            ee.printStackTrace();

            return false;
        }

        return true;
    }

    private boolean load_save_game(Player pla) {
        try {
            SaveGameFactory codec = new SaveGameFactory(pla);

            codec.load(new Archive(new File("characters/" + pla.getId() + ".char")));

            return true;
        } catch (Exception ee) {
            return false;
        }
    }
}
