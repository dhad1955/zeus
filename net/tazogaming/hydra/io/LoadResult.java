package net.tazogaming.hydra.io;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

enum ResultCode { BAD_USERNAME, INVALID_PASSWORD, FAILED_TO_CONNECT, IO_FAILURE, USER_VALID, USER_BANNED, SQL_ERROR, SUCCESS}

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/08/14
 * Time: 23:31
 */
public class LoadResult {

    public static final int
    ACCOUNT_STILL_ONLINE = 5,
    WORLD_FULL = 7,
    LOGIN_SERVER_OFFLINE = 8,
    MULTI_LOG_EXCEEDED = 9,
    BAD_SESSION_ID = 10,
    COULD_NOT_COMPLETE_LOGIN = 13,
    TOO_MANY_WRONG_LOGINS = 16,
    INVALID_LOGIN_SERVER = 20,
    MALFORMED_LOGIN_PACKET = 22,
    NO_REPLY_FROM_LOGIN_SERVER = 23,
    ERROR_LOADING_PROFILE = 25,
    IP_bANNED = 26,
    SERVICE_UNAVAILABLE = 27,
    AUTHENTICATION_SERVER_OFFLINE = 36,
    SYSTEMS_UNAVAILABLE = 44,
    SERVER_BEING_UPDATED = 14,
    RELOAD_THE_PAGE = 6,
    BANNED = 4,
    OK = 2,
    BAD_USERNAME = 44,
    INVALID_PASS = 3;


    private Map<String, Object> data = new HashMap<String, Object>();

    /** resultCode made: 14/08/19 **/
    private int resultCode;


    /**
     * Constructs ...
     *
     *
     * @param resultCode
     * @param data
     */
    public LoadResult(int resultCode, Map data) {
        this.resultCode = resultCode;
        this.data = data;
    }

    public Object getData(String key) {
        return data.get(key);
    }

    public int getCode() {
        return resultCode;
    }
}
