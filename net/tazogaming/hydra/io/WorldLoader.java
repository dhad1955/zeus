
/* WorldLoader.java
* Created on 16-Dec-2007, 12:07:32
* To change this template, choose Tools | Template Managerand open the template in the editor.
*/
package net.tazogaming.hydra.io;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.World;

public interface WorldLoader {

    /**
     * Method loadWorld
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param w
     */
    public void loadWorld(World w);
}
