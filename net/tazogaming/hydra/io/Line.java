package net.tazogaming.hydra.io;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/11/2014
 * Time: 02:07
 */
public class Line {

    public Line(String dat, int no) {
        this.data = dat;
        this.lineNumber = no;
    }
    private String data;

    public int getLineNumber() {
        return lineNumber;
    }


    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        return data;
    }



    private int lineNumber;

}
