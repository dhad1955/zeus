package net.tazogaming.hydra.io;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 02:32
 */
public class Buffer {
    private static final int FRAME_STACK_SIZE = 10;
    public static final int  MAX_BUFFER_SIZE  = 1024;
    private int              frameStackPtr    = -1;
    private byte             buffer[]         = null;
    private int              bufferPtr        = 0;
    private int              bitPosition      = 0;
    private int              frameStack[]     = new int[FRAME_STACK_SIZE];
    private String           path;

    /**
     * Constructs ...
     *
     *
     * @param payload
     */
    public Buffer(byte[] payload) {
        this.buffer    = payload;
        this.bufferPtr = payload.length;
    }

    /**
     * RS2Buffer
     * @author Jagex, Gander, Winterlove
     * @version 2.1
     * @since 1.0
     * @description
     * Creates a payload and returns it in a byte array format
     *
     *
     *
     * @param file
     *
     * @throws IOException
     */
    public Buffer(File file) throws IOException {
        int             i               = (int) file.length();
        byte            abyte0[]        = new byte[i];
        DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new FileInputStream(file)));

        datainputstream.readFully(abyte0, 0, i);
        datainputstream.close();
        buffer    = abyte0;
        this.path = file.getPath();
    }

    /**
     * Constructs ...
     *
     *
     * @param leng
     */
    public Buffer(int leng) {
        this.buffer = new byte[leng];
    }

    /**
     * Constructs ...
     *
     *
     * @param path
     *
     * @throws IOException
     */
    public Buffer(String path) throws IOException {
        this(new File(path));
    }

    /**
     * Constructs ...
     *
     *
     * @param payload
     * @param rewind
     */
    public Buffer(byte[] payload, boolean rewind) {
        this.buffer = payload;
    }

    /**
     * Method rewind
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void rewind() {
        this.bufferPtr = 0;
    }

    /**
     * Method setSavePath
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param str
     */
    public void setSavePath(String str) {
        this.path = str;
    }

    /**
     * Method mkdirs
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean mkdirs() {
        String[] split;

        if (File.separator.equals("\\")) {
            split = path.split("\\" + File.separator);
        } else {
            split = path.split(File.separator);
        }

        StringBuffer currentPath = new StringBuffer();

        if (split.length > 1) {
            for (int i = 0; i < split.length - 1; i++) {
                currentPath.append(split[i] + File.separator);

                File file = new File(currentPath.toString());

                if (!file.exists()) {
                    file.mkdir();
                }
            }
        } else {
            return false;
        }

        return true;
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     *
     * @throws IOException
     */
    public void save(String path) throws IOException {
        FileOutputStream fis       = new FileOutputStream(new File(path));
        byte[]           newBuffer = new byte[bufferPtr];

        System.arraycopy(buffer, 0, newBuffer, 0, bufferPtr);
        fis.write(newBuffer);
        fis.close();
    }

    /**
     * Method writeBoolean
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param m
     */
    public void writeBoolean(boolean m) {
        this.writeByte((m
                        ? 1
                        : 0));
    }

    /**
     * Method readByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readByte() {
        return readUnsignedByte();
    }

    /**
     * Method readMagicByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int readMagicByte() {
        int i = readUnsignedByte();

        return ((i == 255)
                ? -1
                : i);
    }

    /**
     * Method readBoolean
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean readBoolean() {
        return readUnsignedByte() == 1;
    }

    /**
     * Method copyToSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param size
     *
     * @return
     */
    public byte[] copyToSize(int size) {
        byte[] buffer = new byte[size];

        for (int i = 0; i < size; i++) {
            buffer[i] = this.buffer[i];
        }

        return buffer;
    }

    /**
     * Method addByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param byte0
     */
    public void addByte(byte byte0) {
        this.buffer[this.bufferPtr++] = byte0;
    }

    /**
     * Method addByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param int1
     */
    public void addByte(int int1) {
        this.addByte((byte) int1);
    }

    /**
     * Method insertBytes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param bf
     */
    public synchronized void insertBytes(byte[] bf) {
        for (Byte abyte : bf) {
            this.addByte(abyte);
        }
    }

    /**
     * Method setPtr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param off
     */
    public synchronized void setPtr(int off) {
        this.bufferPtr = off;
    }

    /**
     * Method indexOf
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param offset
     *
     * @return
     */
    public byte indexOf(int offset) {
        return this.buffer[offset];
    }

    /**
     * Method getBuffer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized byte[] getBuffer() {
        return this.buffer;
    }

    /**
     * Method getPtr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int getPtr() {
        return this.bufferPtr;
    }

    /**
     * Method writeByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeByte(int i) {
        this.addByte(i);
    }

    /**
     * Method writeWord
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeWord(int i) {
        this.addByte((i >> 8));
        this.addByte(i);
    }

    /**
     * Method readSignedWor1d
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readSignedWor1d() {
        bufferPtr += 2;

        int i = ((this.indexOf(bufferPtr - 2) & 0xff) << 8) + (this.indexOf(bufferPtr - 1) & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method writeWordBigEndian
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeWordBigEndian(int i) {
        this.addByte(i);
        this.addByte((i >> 8));
    }

    /**
     * Method write3Byte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void write3Byte(int i) {
        this.addByte((i >> 16));
        this.addByte((i >> 8));
        this.addByte(i);
    }

    /**
     * Method writeDWord
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeDWord(int i) {
        this.addByte((i >> 24));
        this.addByte((i >> 16));
        this.addByte((i >> 8));
        this.addByte(i);
    }

    /**
     * Method writeDWordBigEndian
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeDWordBigEndian(int i) {
        this.addByte(i);
        this.addByte((i >> 8));
        this.addByte((i >> 16));
        this.addByte((i >> 24));
    }

    /**
     * Method writeQWord
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param l
     */
    public synchronized void writeQWord(long l) {
        this.addByte((int) (l >> 56));
        this.addByte((int) (l >> 48));
        this.addByte((int) (l >> 40));
        this.addByte((int) (l >> 32));
        this.addByte((int) (l >> 24));
        this.addByte((int) (l >> 16));
        this.addByte((int) (l >> 8));
        this.addByte((int) l);
    }

    /**
     * Method writeWordBigEndianA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeWordBigEndianA(int i) {
        this.addByte((byte) (i + 128));
        this.addByte((byte) (i >> 8));
    }

    /**
     * Method writeWordA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeWordA(int i) {
        this.addByte((byte) (i >> 8));
        this.addByte((byte) (i + 128));
    }

    /**
     * Method writeWordBigEndian_dup
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeWordBigEndian_dup(int i) {
        this.addByte((byte) i);
        this.addByte((byte) (i >> 8));
    }

    /**
     * Method skipPtr
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param off
     */
    public synchronized void skipPtr(int off) {
        this.bufferPtr += off;
    }

    /**
     * Method writeString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     */
    @SuppressWarnings("deprecated")
    public synchronized void writeString(String s) {
        byte[] buff = s.getBytes();

        this.insertBytes(buff);
        this.addByte(10);
    }

    /**
     * Method writeByteA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeByteA(int i) {
        this.addByte((i + 128));
    }

    /**
     * Method writeByteS
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeByteS(int i) {
        this.addByte((128 - i));
    }

    /**
     * Method writeByteC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeByteC(int i) {
        this.addByte(-i);
    }

    /**
     * Method writeDWord_v1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeDWord_v1(int i) {
        this.addByte((i >> 8));
        this.addByte(i);
        this.addByte(i >> 24);
        this.addByte((i >> 16));
    }

    /**
     * Method writeDWord_v2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeDWord_v2(int i) {
        this.addByte(i >> 16);
        this.addByte(i >> 24);
        this.addByte(i);
        this.addByte(i >> 8);
    }

    /**
     * Method readSignedWordBigEndian
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readSignedWordBigEndian() {
        bufferPtr += 2;

        int i = ((buffer[bufferPtr - 1] & 0xff) << 8) + (buffer[bufferPtr - 2] & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readSignedWordA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readSignedWordA() {
        bufferPtr += 2;

        int i = ((buffer[bufferPtr - 2] & 0xff) << 8) + (buffer[bufferPtr - 1] - 128 & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readSignedWordBigEndianA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readSignedWordBigEndianA() {
        bufferPtr += 2;

        int i = ((buffer[bufferPtr - 1] & 0xff) << 8) + (buffer[bufferPtr - 2] - 128 & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readUnsignedWordBigEndian
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readUnsignedWordBigEndian() {
        bufferPtr += 2;

        return ((buffer[bufferPtr - 1] & 0xff) << 8) + (buffer[bufferPtr - 2] & 0xff);
    }

    /**
     * Method readUnsignedWordA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readUnsignedWordA() {
        bufferPtr += 2;

        return ((buffer[bufferPtr - 2] & 0xff) << 8) + (buffer[bufferPtr - 1] - 128 & 0xff);
    }

    /**
     * Method readUnsignedWordBigEndianA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readUnsignedWordBigEndianA() {
        bufferPtr += 2;

        return ((buffer[bufferPtr - 1] & 0xff) << 8) + (buffer[bufferPtr - 2] - 128 & 0xff);
    }

    /**
     * Method readDWord_v1
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readDWord_v1() {
        bufferPtr += 4;

        return ((buffer[bufferPtr - 2] & 0xff) << 24) + ((buffer[bufferPtr - 1] & 0xff) << 16)
               + ((buffer[bufferPtr - 4] & 0xff) << 8) + (buffer[bufferPtr - 3] & 0xff);
    }

    /**
     * Method readDWord_v2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readDWord_v2() {
        bufferPtr += 4;

        return ((buffer[bufferPtr - 3] & 0xff) << 24) + ((buffer[bufferPtr - 4] & 0xff) << 16)
               + ((buffer[bufferPtr - 1] & 0xff) << 8) + (buffer[bufferPtr - 2] & 0xff);
    }

    /**
     * Method readBytes_reverse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param abyte0
     * @param i
     * @param j
     */
    public synchronized void readBytes_reverse(byte abyte0[], int i, int j) {
        for (int k = (j + i) - 1; k >= j; k--) {
            abyte0[k] = buffer[bufferPtr++];
        }
    }

    /**
     * Method writeBytes_reverse
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param abyte0
     * @param i
     * @param j
     */
    public synchronized void writeBytes_reverse(byte abyte0[], int i, int j) {
        for (int k = (j + i) - 1; k >= j; k--) {
            this.addByte(abyte0[k]);
        }
    }

    /**
     * Method readBytes_reverseA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param abyte0
     * @param i
     * @param j
     */
    public synchronized void readBytes_reverseA(byte abyte0[], int i, int j) {
        for (int k = (j + i) - 1; k >= j; k--) {
            abyte0[k] = (byte) (buffer[bufferPtr++] - 128);
        }
    }

    /**
     * Method writeBytes_reverseA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param abyte0
     * @param i
     * @param j
     */
    public synchronized void writeBytes_reverseA(byte abyte0[], int i, int j) {
        for (int k = (j + i) - 1; k >= j; k--) {
            this.addByte((abyte0[k] + 128));
        }
    }

    /**
     * Method writeBytes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param abyte0
     * @param i
     * @param j
     */
    public synchronized void writeBytes(byte abyte0[], int i, int j) {
        for (int k = j; k < j + i; k++) {
            this.addByte(abyte0[k]);
        }
    }

    /**
     * Method writeFrameSize
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeFrameSize(int i) {
        buffer[bufferPtr - i - 1] = (byte) i;
    }

    /**
     * Method writeFrameSizeWord
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param i
     */
    public synchronized void writeFrameSizeWord(int i) {
        buffer[bufferPtr - i - 2] = (byte) (i >> 8);
        buffer[bufferPtr - i - 1] = (byte) i;
    }

    /**
     * Method readUnsignedByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readUnsignedByte() {
        return (this.indexOf(this.bufferPtr++) & 0xff);
    }

    /**
     * Method readSignedByte
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized byte readSignedByte() {
        return this.indexOf(this.bufferPtr++);
    }

    /**
     * Method readShort
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readShort() {
        this.bufferPtr += 2;

        return ((this.indexOf(bufferPtr - 2) & 0xff) << 8) + (this.indexOf(bufferPtr - 1) & 0xff);
    }

    /**
     * Method readSignedWord
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readSignedWord() {
        bufferPtr += 2;

        int i = ((this.indexOf(bufferPtr - 2) & 0xff) << 8) + (this.indexOf(bufferPtr - 1) & 0xff);

        if (i > 32767) {
            i -= 0x10000;
        }

        return i;
    }

    /**
     * Method readDWord
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readDWord() {
        bufferPtr += 4;

        return ((this.indexOf(bufferPtr - 4) & 0xff) << 24) + ((this.indexOf(bufferPtr - 3) & 0xff) << 16)
               + ((this.indexOf(bufferPtr - 2) & 0xff) << 8) + (this.indexOf(bufferPtr - 1) & 0xff);
    }

    /**
     * Method readQWord
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized long readQWord() {
        long l  = (long) readDWord() & 0xffffffffL;
        long l1 = (long) readDWord() & 0xffffffffL;

        return (l << 32) + l1;
    }

    /**
     * Method readString2
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized String readString2() {
        int i = bufferPtr;

        while (this.indexOf(bufferPtr++) != 0);

        return new String(buffer, i, bufferPtr - i - 1);
    }

    /**
     * Method readString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized String readString() {
        int i = bufferPtr;

        while (this.indexOf(bufferPtr++) != 10);

        return new String(buffer, i, bufferPtr - i - 1);
    }

    /**
     * Method readBytes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param abyte0
     * @param i
     * @param j
     */
    public synchronized void readBytes(byte abyte0[], int i, int j) {
        for (int k = j; k < j + i; k++) {
            abyte0[k] = this.indexOf(bufferPtr++);
        }
    }

    /**
     * Method readBytes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     * @param amount
     */
    public synchronized void readBytes(byte[] data, int amount) {
        for (int i = 0; i < amount; i++) {
            data[i] = this.indexOf(bufferPtr++);
        }
    }

    /**
     * Method writeBytes
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param data
     * @param leng
     */
    public void writeBytes(byte[] data, int leng) {
        System.arraycopy(data, 0, buffer, bufferPtr, leng);
        this.bufferPtr += leng;
    }

    /**
     * Method initBitAccess
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public synchronized void initBitAccess() {
        bitPosition = bufferPtr * 8;
    }

    /**
     * Method readSignedByteA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized byte readSignedByteA() {
        return (byte) (this.indexOf(bufferPtr++) - 128);
    }

    /**
     * Method readSignedByteC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized byte readSignedByteC() {
        return (byte) (-this.indexOf(bufferPtr++));
    }

    /**
     * Method readSignedByteS
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized byte readSignedByteS() {
        return (byte) (128 - this.indexOf(bufferPtr++));
    }

    /**
     * Method readUnsignedByteA
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readUnsignedByteA() {
        return this.indexOf(bufferPtr++) - 128 & 0xff;
    }

    /**
     * Method readUnsignedByteC
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readUnsignedByteC() {
        return -this.indexOf(bufferPtr++) & 0xff;
    }

    /**
     * Method readUnsignedByteS
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public synchronized int readUnsignedByteS() {
        return 128 - this.indexOf(bufferPtr++) & 0xff;
    }

    /**
     * Method finishBitAccess
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public synchronized void finishBitAccess() {
        bufferPtr = (bitPosition + 7) / 8;
    }
}
