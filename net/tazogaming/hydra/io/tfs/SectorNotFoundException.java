package net.tazogaming.hydra.io.tfs;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:59
 */
public class SectorNotFoundException extends RuntimeException {

    /**
     * Constructs ...
     *
     *
     * @param username
     * @param sector
     */
    public SectorNotFoundException(String username, int sector) {
        super("Error " + username + " sector not found: " + sector);
    }
}
