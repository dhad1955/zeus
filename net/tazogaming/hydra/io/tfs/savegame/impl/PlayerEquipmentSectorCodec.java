package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:40
 */
public class PlayerEquipmentSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void load(Buffer payload, Player player) {
        for (int i = 0; i < 14; i++) {
            int id = payload.readShort();

            if (id != 65535) {
                player.getEquipment().set(id, payload.readDWord(), i);
            }
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void save(Buffer payload, Player player) {
        for (int i = 0; i < 14; i++) {
            if (player.getEquipment().isEquipped(i)) {
                payload.writeWord(player.getEquipment().getId(i));
                payload.writeDWord(player.getEquipment().getAmount(i));
            } else {
                payload.writeWord(65535);
            }
        }
    }
}
