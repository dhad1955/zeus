package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.ui.PlayerGrandExchange;
import net.tazogaming.hydra.game.ui.ge.BuyOffer;
import net.tazogaming.hydra.game.ui.ge.Offer;
import net.tazogaming.hydra.game.ui.ge.OfferState;
import net.tazogaming.hydra.game.ui.ge.SellOffer;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 25/01/2015
 * Time: 17:46
 */
public class GrandExchangeSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method save
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void load(Buffer sector, Player player) {
        for (int i = 0; i < 6; i++) {
            int opcode = sector.readByte();

            if (opcode == 0) {
                continue;    // offer is null, doesnt exist mate.
            }

            boolean isBuy     = sector.readByte() == 1;
            int     itemId    = sector.readShort();
            int     remaining = sector.readDWord();
            int     amount    = sector.readDWord();
            int     pot       = sector.readDWord();
            int     ppi       = sector.readDWord();
            int     itemAmt   = sector.readDWord();
            int     cashAmt   = sector.readDWord();
            int     offerId   = -1;

            if (opcode == 1) {

                // offer is active
                offerId = sector.readDWord();
            }

            Offer offer;

            if (isBuy) {
                offer = new BuyOffer(offerId, player, itemId, amount, ppi);
            } else {
                offer = new SellOffer(player, offerId, itemId, amount, ppi);
            }

            offer.getCollectionBox().setCashAmt(cashAmt);
            offer.getCollectionBox().setItemAmt(itemAmt);
            offer.setCurrentPot(pot);
            offer.setPricePerItem(ppi);
            offer.setRemaining(remaining);
            offer.setAmount(amount);

            if (opcode == 1) {
                offer.setState(OfferState.ACTIVE);
            } else if (opcode == 2) {
                offer.setState(OfferState.ABORTED);
            } else if (opcode == 3) {
                offer.setState(OfferState.COMPLETED);
            }

            player.getAccount().getGrandExchange().setOffer(offer, i);
        }
    }

    /**
     * Method save
     * Created on 15/02/12
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void save(Buffer sector, Player player) {
        PlayerGrandExchange exchange = player.getAccount().getGrandExchange();

        for (Offer offer : exchange.getOffers()) {
            if (offer == null || offer.getState() == OfferState.FAILED) {
                sector.addByte(0);

                continue;
            }
            if (offer.getState() == OfferState.ACTIVE) {
                sector.addByte(1);
            }else if (offer.getState() == OfferState.ABORTED) {
                sector.addByte(2);
            } else if (offer.getState() == OfferState.COMPLETED) {
                sector.addByte(3);
            } else{
                sector.addByte(0);
                continue;
            }

            sector.addByte((offer instanceof BuyOffer)
                           ? 1
                           : 0);

            sector.writeWord(offer.getItem());
            sector.writeDWord(offer.getRemaining());
            sector.writeDWord(offer.getAmount());
            sector.writeDWord(offer.getCurrentPot());
            sector.writeDWord(offer.getPricePerItem());
            sector.writeDWord(offer.getCollectionBox().getItemAmt());
            sector.writeDWord(offer.getCollectionBox().getCashAmt());

            if (offer.getState() == OfferState.ACTIVE) {
                sector.writeDWord(offer.getId());
            }
        }
    }
}
