package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;
import net.tazogaming.hydra.map.Point;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:32
 */
public class PlayerPositionSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void load(Buffer payload, Player player) {
        int x = payload.readShort();
        int y = payload.readShort();
        int h = payload.readByte();

        player.setLocation(Point.location(x, y, h));
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void save(Buffer payload, Player player) {
    if(player.getLocation() == null){
        payload.writeWord(3222);
        payload.writeWord(3222);
        payload.writeByte(0);
    }                      else {
        payload.writeWord(player.getX());
        payload.writeWord(player.getY());
        payload.writeByte(player.getHeight());
    }
    }
}
