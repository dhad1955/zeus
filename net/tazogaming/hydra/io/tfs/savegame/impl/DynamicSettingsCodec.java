package net.tazogaming.hydra.io.tfs.savegame.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/11/2014
 * Time: 14:04
 */
public class DynamicSettingsCodec implements FileSectorCodec<Player> {

    public static final int VERSION = 2;
    /**
     * Method load
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void load(Buffer sector, Player player) {
        int version = sector.readByte();
        if(version > 0) {
            player.getAccount().setPkMode(sector.readByte() == 1);
        }
        if(version > 1) {
            player.getAccount().setSetting("spell_book", sector.readShort());
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void save(Buffer sector, Player player) {
            sector.writeByte(VERSION);
            sector.writeByte(player.getAccount().isPkMode() ? 1 : 0);
       if(player.getGameFrame() != null)
            sector.writeWord(player.getGameFrame().getSpellbook());
         else
           sector.writeWord(192);
    }

}
