package net.tazogaming.hydra.io.tfs.savegame.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/04/2015
 * Time: 22:13
 */
public class DboxTimeCodec implements FileSectorCodec<Player> {
    /**
     * Method load
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void load(Buffer sector, Player player) {
        player.getAccount().dboxTime = sector.readQWord();
    }

    /**
     * Method save
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void save(Buffer sector, Player player) {
        sector.writeQWord(player.getAccount().dboxTime);

    }
}
