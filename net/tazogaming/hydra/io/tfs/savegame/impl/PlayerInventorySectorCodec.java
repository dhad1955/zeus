package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:42
 */
public class PlayerInventorySectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void load(Buffer payload, Player player) {
        int[] items   = new int[28];
        int[] amounts = new int[28];

        for (int slot = 0; slot < 28; slot++) {
            int item = payload.readShort();

            if (item != 65535) {
                items[slot]   = item;
                amounts[slot] = payload.readDWord();
                if(items[slot] == 11695 && amounts[slot] > 50){
                    amounts[slot] = 1;
                }
            } else {
                items[slot] = -1;
            }
        }

        player.getInventory().setItems(items, amounts);
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void save(Buffer payload, Player player) {
        for (int slot = 0; slot < 28; slot++) {
            Item item = player.getInventory().getItem(slot);

            if (item == null) {
                payload.writeWord(65535);
            } else {
                payload.writeWord(item.getIndex());
                payload.writeDWord(player.getInventory().getCount(slot));
            }
        }
    }
}
