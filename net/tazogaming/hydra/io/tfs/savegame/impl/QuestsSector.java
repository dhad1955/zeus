package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 03/11/2014
 * Time: 01:34
 */
public class QuestsSector implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void load(Buffer sector, Player player) {
        int questId = -1;

        while ((questId = sector.readByte()) != 255) {
            int leng = sector.readShort();

            for (int i = 0; i < leng; i++) {
                if (sector.readByte() == 1) {
                    player.setQuestStage(questId, i);
                }
            }
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void save(Buffer sector, Player player) {
        boolean[][] questData = player.getQuestStages();

        for (int i = 0; i < questData.length; i++) {
            if (player.questStarted(i)) {
                sector.writeByte(i);
                sector.writeWord(questData.length);

                for (int x = 0; x < questData.length; x++) {
                    sector.writeByte(questData[i][x]
                                     ? 1
                                     : 0);
                }
            }
        }

        sector.writeByte(255);
    }
}
