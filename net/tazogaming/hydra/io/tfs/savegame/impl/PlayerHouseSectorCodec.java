package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;
import net.tazogaming.hydra.game.skill.construction.RoomConfig;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 11:24
 */
public class PlayerHouseSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chunk
     * @param player
     */
    @Override
    public void load(Buffer chunk, Player player) {
        int              data         = chunk.readByte();
        RoomConfig[][][] house_layout = player.getAccount().getHouseLayout();

        if (data == 0) {
            return;
        }

        for (int h = 0; h < 4; h++) {
            for (int x = 0; x < 13; x++) {
                for (int y = 0; y < 13; y++) {
                    int room_id = chunk.readByte();

                    if (room_id == 255) {
                        house_layout[h][x][y] = null;

                        continue;
                    }

                    int        rotation = chunk.readByte();
                    RoomConfig config   = house_layout[h][x][y] = new RoomConfig(room_id, rotation);

                    config.setRugID(chunk.readMagicByte());

                    switch (room_id) {
                    case 22 :
                    case 7 :
                    case 21 :
                    case 20 :
                    case 17 :
                        int ind = chunk.readShort();

                        if (ind == 65535) {
                            ind = -1;
                        }

                        house_layout[h][x][y].setStairs(ind);

                        break;

                    case 6 :
                        config.setRingType(chunk.readMagicByte());

                        break;

                    case 13 :
                        config.setFence(chunk.readMagicByte());
                        config.setHedge(chunk.readMagicByte());

                        break;

                    case 15 :
                        config.changeJail(chunk.readMagicByte());
                        config.changeFloorType(chunk.readMagicByte());

                        break;
                    }

                    while (true) {
                        int index = chunk.readMagicByte();

                        if (index == -1) {
                            break;
                        }

                        config.set_node(index, chunk.readShort());
                    }
                }
            }
        }

        player.getAccount().loadCostumes(chunk);
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param chunk
     * @param player
     */
    @Override
    public void save(Buffer chunk, Player player) {
        if (!player.getAccount().hasVar("house_theme")) {
            chunk.addByte(0);

            return;
        }

        chunk.addByte(1);

        RoomConfig[][][] house_layout = player.getAccount().getHouseLayout();
        RoomConfig       config;

        for (int i = 0; i < 4; i++) {
            for (int x = 0; x < 13; x++) {
                for (int y = 0; y < 13; y++) {
                    config = house_layout[i][x][y];

                    if (house_layout[i][x][y] == null) {
                        chunk.addByte(255);
                    } else {
                        chunk.addByte(house_layout[i][x][y].getRoom());
                        chunk.addByte(config.getRotation());
                        chunk.addByte((config.getRugID() == -1)
                                      ? 255
                                      : config.getRugID());

                        switch (house_layout[i][x][y].getRoom()) {
                        case 22 :
                        case 7 :
                        case 21 :
                        case 20 :
                        case 17 :
                            chunk.writeWord(config.getStairsType());

                            break;

                        case 6 :
                            chunk.addByte((config.getRingType() == -1)
                                          ? 255
                                          : config.getRingType());

                            break;

                        case 13 :
                            chunk.addByte((config.getFence() == -1)
                                          ? 255
                                          : config.getFence());
                            chunk.addByte((config.getHedge() == -1)
                                          ? 255
                                          : config.getHedge());

                            break;

                        case 15 :
                            chunk.addByte((config.getPrisonType() == -1)
                                          ? 255
                                          : config.getPrisonType());
                            chunk.addByte((config.getFloorType() == -1)
                                          ? 255
                                          : config.getFloorType());

                            break;
                        }

                        for (int ix = 0; ix < 80; ix++) {
                            if (config.getBuilt(ix) > 0) {
                                chunk.addByte(ix);
                                chunk.writeWord(config.getBuilt(ix));
                            }
                        }

                        chunk.addByte(255);
                    }
                }
            }
        }

        player.getAccount().saveCostumeRooms(chunk);
    }
}
