package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:45
 */
public class PlayerAppearanceSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void load(Buffer payload, Player player) {
        player.getAppearance().setGender(payload.readByte());

        for (int lookType = 0; lookType < 7; lookType++) {
            player.getAppearance().setLook(lookType, payload.readShort());
        }

        for (int colorType = 0; colorType < 5; colorType++) {
            player.getAppearance().setColor(colorType, payload.readShort());
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void save(Buffer payload, Player player) {
        payload.addByte(player.getAppearance().getGender());

        for (int lookType = 0; lookType < 7; lookType++) {
            payload.writeWord(player.getAppearance().getLook()[lookType]);
        }

        for (int colorType = 0; colorType < 5; colorType++) {
            payload.writeWord(player.getAppearance().getColour()[colorType]);
        }
    }
}
