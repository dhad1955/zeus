package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.player.PlayerVariable;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;
import net.tazogaming.hydra.script.runtime.data.StructInstance;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 11:03
 */
public class PlayerVarSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    private static final int
        TYPE_DWORD  = 0,
        TYPE_STRING = 1,
        TYPE_LIST   = 2;

    /**
     * Method load
     * Created on 14/10/28
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void load(Buffer sector, Player player) {
        int total_vars = sector.readShort();

        for (int i = 0; i < total_vars; i++) {
            String         name = sector.readString();
            int            type = sector.readByte();
            PlayerVariable var  = null;

            switch (type) {
            case TYPE_DWORD :
                var = new PlayerVariable(name, sector.readDWord(), true);

                break;

            case TYPE_STRING :
                var = new PlayerVariable(name, sector.readString(), true);

                break;

            case TYPE_LIST :

                // list
                int  listSize = sector.readShort();
                List list     = new ArrayList(listSize);

                for (int listIndex = 0; listIndex < listSize; listIndex++) {
                    int listObjType = sector.readByte();

                    if (listObjType == 0) {
                        list.add(sector.readDWord());
                    } else if (type == 1) {
                        list.add(sector.readString());
                    }
                }

                var = new PlayerVariable(name, list, true);

                break;
            }

            player.getAccount().putVariable(name, var);
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void save(Buffer sector, Player player) {
        List<PlayerVariable> vars     = player.getAccount().getSaveVars();
        List<PlayerVariable> saveable = new ArrayList<PlayerVariable>();

        for (PlayerVariable var : vars) {
            if (var.getFullData() instanceof List | var.getFullData() instanceof Integer
                    | var.getFullData() instanceof String) {
                saveable.add(var);
            }
        }

        sector.writeWord(saveable.size());

        for (PlayerVariable variable : saveable) {
            sector.writeString(variable.getName());

            if (variable.getFullData() instanceof List) {
                sector.addByte(2);

                List l = (List) variable.getFullData();

                // ensure that the list doesnt contain any null types
                boolean fail = false;

                for (Object o : l) {
                    if (!(o instanceof Integer | o instanceof String)) {
                        fail = true;

                        break;
                    }
                }

                if (fail) {
                    sector.writeWord(0);
                } else {
                    sector.writeWord(l.size());

                    for (Object o : l) {
                        if (o instanceof Integer) {
                            sector.writeByte(0);
                            sector.writeDWord((Integer) o);
                        } else if (o instanceof StructInstance) {
                            sector.writeByte(3);

                            StructInstance str = (StructInstance) o;
                        } else {
                            sector.writeByte(1);
                            sector.writeString((String) o);
                        }
                    }
                }
            } else if (variable.getFullData() instanceof Integer) {
                sector.addByte(0);
                sector.writeDWord((Integer) variable.getFullData());
            } else if (variable.getFullData() instanceof String) {
                sector.addByte(1);
                sector.writeString((String) variable.getFullData());
            }
        }
    }
}
