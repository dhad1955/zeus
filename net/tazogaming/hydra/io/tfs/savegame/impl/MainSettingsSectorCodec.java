package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 10:56
 */
public class MainSettingsSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void load(Buffer sector, Player player) {
        int setting = -1;

        while ((setting = sector.readShort()) != 65535) {
            player.getAccount().set(setting, sector.readDWord());
        }

        while ((setting = sector.readShort()) != 65535) {
            player.getAccount().setB(setting, sector.readDWord(), false);
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void save(Buffer sector, Player player) {
        int[] mainSettings   = player.getAccount().getMainSettings();
        int[] buttonSettings = player.getAccount().getButtonSettings();

        for (int i = 0; i < mainSettings.length; i++) {
            if (mainSettings[i] != -1) {
                sector.writeWord(i);
                sector.writeDWord(mainSettings[i]);
            }
        }

        sector.writeWord(65535);

        for (int i = 0; i < buttonSettings.length; i++) {
            if (buttonSettings[i] > -1) {
                sector.writeWord(i);
                sector.writeDWord(buttonSettings[i]);
            }
        }

        sector.writeWord(65535);
    }
}
