package net.tazogaming.hydra.io.tfs.savegame.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 19/11/2014
 * Time: 07:34
 */
public class KillCountsCodec implements FileSectorCodec<Player> {
    /**
     * Method load
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void load(Buffer sector, Player player) {
        int siz = sector.readShort();
        for(int i = 0; i < siz; i++) {
            player.getKillCounts().put(sector.readShort(), sector.readShort());
        }

    }

    /**
     * Method save
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void save(Buffer sector, Player player) {
        int size = player.getKillCounts().size();
        sector.writeWord(size);
        for(Integer i : player.getKillCounts().keySet())
        {
            sector.writeWord(i);
            sector.writeWord(player.getKillCounts().get(i));
        }

    }
}
