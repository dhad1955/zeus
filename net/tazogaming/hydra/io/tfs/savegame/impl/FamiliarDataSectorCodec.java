package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;
import net.tazogaming.hydra.util.TimingUtility;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 11:33
 */
public class FamiliarDataSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void load(Buffer sector, Player player) {
        player.setSumSpecPts(sector.readByte());

        if (sector.readBoolean()) {
            int familiarId = sector.readShort();
            int bob_count  = sector.readByte();
            int time       = sector.readShort();

            for (int bob_slot = 0; bob_slot < bob_count; bob_slot++) {
                int item = sector.readShort();

                if (item != 65535) {
                    player.getBobItems()[bob_slot]  = item;
                    player.getBobItemsC()[bob_slot] = sector.readDWord();
                }
            }

            player.getTimers().setTime(TimingUtility.SUMMON_TIMER, time);
            player.getAccount().setSetting("familiar_request", familiarId);
            player.getAccount().setSetting("bob_max", bob_count);
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void save(Buffer sector, Player player) {
        sector.writeByte(player.getSummonSpecPoints());
        sector.writeBoolean(player.getCurrentFamiliar() != null);

        if (player.getCurrentFamiliar() != null) {
            sector.writeWord(player.getCurrentFamiliar().getId());

            int bob_count = player.getAccount().getInt32("bob_max");

            if(bob_count < -1 || bob_count > 30)
                bob_count = 30;

            sector.writeByte(bob_count);
            sector.writeWord(player.getTimers().getAbsTime(TimingUtility.SUMMON_TIMER));

            for (int i = 0; i < bob_count; i++) {
                int item   = player.getBobItems()[i];
                int amount = player.getBobItemsC()[i];

                if ((item == -1) || (item == 0)) {
                    sector.writeWord(65535);
                } else {
                    sector.writeWord(player.getBobItems()[i]);
                    sector.writeDWord(player.getBobItemsC()[i]);
                }
            }
        }
    }
}
