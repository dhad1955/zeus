package net.tazogaming.hydra.io.tfs.savegame.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.item.Item;
import net.tazogaming.hydra.game.ui.shops.Shop;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/11/2014
 * Time: 01:31
 */
public class BuybackCodecSector implements FileSectorCodec<Player> {
    /**
     * Method load
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void load(Buffer sector, Player player) {

            Shop shop = player.getBuyBackShop();
           int siz = sector.readByte();
           for(int i = 0; i < siz; i++) {
               int itemId = sector.readShort();
               int amt = sector.readByte();
               int cost = sector.readDWord();
               if(itemId != 65535) {
                   shop.addNewItem(Item.forId(itemId), amt, cost);
               }
           }
    }

    /**
     * Method save
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void save(Buffer sector, Player player) {
        Shop buyBack = player.getBuyBackShop();
        sector.writeByte(buyBack.length());
        for(int i = 0; i < buyBack.length(); i++ ){
           sector.writeWord(buyBack.getItem(i) == null ? 65535 : buyBack.getItem(i).getIndex());
           sector.writeByte(buyBack.getItem(i) == null ? 0 : buyBack.getItemsCount()[i]);
           sector.writeDWord(buyBack.getCost(i));
        }

    }
}
