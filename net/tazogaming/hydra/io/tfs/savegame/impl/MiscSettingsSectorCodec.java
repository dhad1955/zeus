package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.game.skill.slayer.SlayerTask;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 11:10
 */
public class MiscSettingsSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void load(Buffer sector, Player player) {
        player.setCurrentSpecial((int) sector.readByte());
        player.setCurrentEnergy((int) sector.readByte());
        player.getAccount().setPlayTime(sector.readDWord());
        player.getTimers().load(sector);

        for (int i = 0; i < 20; i++) {
            player.setPoints(i, sector.readDWord());
        }

        int slay = sector.readShort();

        if (slay == 65535) {
            return;
        }

        SlayerTask task = new SlayerTask(player, slay, sector.readShort());
        task.setTotalKills(sector.readShort());
        player.setSlayerTask(task);
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void save(Buffer sector, Player player) {
        sector.writeByte((int) player.getCurrentSpecial());
        sector.writeByte((int) player.getCurrentEnergy());
        sector.writeDWord(player.getAccount().getPlayTime());
        player.getTimers().save(sector);

        for (int i = 0; i < 20; i++) {
            sector.writeDWord(player.getPoints(i));
        }


        if(player.hasSlayerTask() && !player.getSlayerTask().isDuoTask()) {
                sector.writeWord(player.getSlayerTask().getTaskId());
                sector.writeWord(player.getSlayerTask().getKillsToComplete());
                sector.writeWord(player.getSlayerTask().getTotalKills());

        }else{
            sector.writeWord(65535);
        }

    }
}
