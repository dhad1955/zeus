package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;
import net.tazogaming.hydra.game.skill.farming2.PlayerGrew;

//~--- JDK imports ------------------------------------------------------------

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 11:43
 */
public class FarmingDataSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void load(Buffer sector, Player player) {
        int                      total_patches = sector.readByte();
        Map<Integer, PlayerGrew> map           = new HashMap<Integer, PlayerGrew>();

        for (int i = 0; i < total_patches; i++) {
            int        patchID     = sector.readShort();
            int        grew        = sector.readShort();
            int        harvest     = sector.readByte();
            int        logs        = sector.readByte();
            PlayerGrew grewProduct = new PlayerGrew(patchID, grew, harvest, logs);

            map.put(patchID, grewProduct);
        }

        player.getAccount().setFarmPatches(map);
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     * @param player
     */
    @Override
    public void save(Buffer sector, Player player) {
        Map<Integer, PlayerGrew> farmPatches = player.getAccount().getFarmPatches();

        sector.writeByte(farmPatches.size());

        for (Integer i : farmPatches.keySet()) {
            sector.writeWord(i);
            PlayerGrew grew = farmPatches.get(i);

            sector.writeWord(grew.getGrewIndex());
            sector.writeByte(grew.getHarvestAmount());
            sector.writeByte(grew.logsRemaining());
        }
    }
}
