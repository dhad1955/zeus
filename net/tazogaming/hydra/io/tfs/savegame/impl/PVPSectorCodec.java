package net.tazogaming.hydra.io.tfs.savegame.impl;

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 20/10/14
 * Time: 13:33
 */
public class PVPSectorCodec implements FileSectorCodec<Player> {
    /**
     * Method load
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */


    @Override
    public void load(Buffer sector, Player player) {
        player.getAccount().setKillStreak((byte)sector.readByte());
        player.getAccount().setMaxRecordedHit((byte)sector.readByte());
        player.getAccount().setPvpPlayTime(sector.readDWord());
        player.getAccount().setFightTime(sector.readDWord());
        player.getAccount().setPvpDamageDealt(sector.readQWord());
        player.getAccount().setPvpDamageReceived(sector.readQWord());

        player.getEquipment().loadLoadout(sector);

    }

    /**
     * Method save
     * Created on 14/08/18
     *
     * @param sector
     * @param player
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void save(Buffer sector, Player player) {
        sector.addByte(player.getAccount().getKillStreak());
        sector.addByte(player.getAccount().getMaxRecordedHit());
        sector.writeDWord(player.getAccount().getPvpPlayTime());
        sector.writeDWord(player.getAccount().getPvpFightTime());
        sector.writeQWord(player.getAccount().getPvpDamageDealt());
        sector.writeQWord(player.getAccount().getPvpDamageReceived());
        player.getEquipment().saveLoadout(sector);
    }
}
