package net.tazogaming.hydra.io.tfs.savegame.impl;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.FileSectorCodec;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:38
 */
public class PlayerSkillsSectorCodec implements FileSectorCodec<Player> {

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void load(Buffer payload, Player player) {
        int[] curStats = new int[25];
        int[] exps     = new int[25];

        for (int i = 0; i < 25; i++) {
            curStats[i] = payload.readByte();
            exps[i]     = payload.readDWord();
        }

        player.setCurStats(curStats);
        player.setExps(exps);

        int prayer    = payload.readByte();
        int health    = payload.readByte();
        double actualHealth = health;

        if(health == 255)
            actualHealth = (double)(payload.readShort() / 10);


        int summoning = payload.readByte();

        player.getPrayerBook().setPoints((int) prayer);
        player.setCurrentHealth(actualHealth);
        player.setCurrSummonPts(summoning);
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param payload
     * @param player
     */
    @Override
    public void save(Buffer payload, Player player) {
        for (int i = 0; i < 25; i++) {
            payload.addByte(player.getCurStat(i));
            payload.writeDWord(player.getExps()[i]);
        }

        payload.addByte(player.getPrayerBook().getPoints());
        payload.addByte(255);
        payload.writeWord((int)(player.getCurrentHealth() * 10));
        payload.addByte(player.getCurrSummonPts());
    }
}
