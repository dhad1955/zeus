package net.tazogaming.hydra.io.tfs;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 08:59
 */
public class FileChunk {
    private Buffer buffer = null;
    private int    opcode;
    private int    length;
    private int    version;

    /**
     * Constructs ...
     *
     *
     * @param buffer
     * @param opcode
     * @param version
     * @param length
     */
    public FileChunk(Buffer buffer, int opcode, int version, int length) {
        this.buffer  = buffer;
        this.opcode  = opcode;
        this.length  = length;
        this.version = version;
    }

    /**
     * Method getOpcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getOpcode() {
        return opcode;
    }

    /**
     * Method setOpcode
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param opcode
     */
    public void setOpcode(int opcode) {
        this.opcode = opcode;
    }

    /**
     * Method getBuffer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Buffer getBuffer() {
        return buffer;
    }

    /**
     * Method setBuffer
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param buffer
     */
    public void setBuffer(Buffer buffer) {
        this.buffer = buffer;
    }

    /**
     * Method getVersion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getVersion() {
        return version;
    }

    /**
     * Method setVersion
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param version
     */
    public void setVersion(byte version) {
        this.version = version;
    }

    /**
     * Method getLength
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int getLength() {
        return length;
    }

    /**
     * Method setLength
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param length
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * Method build
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte[] build() {
        Buffer returnBuffer = new Buffer(buffer.getPtr() + 7);

        returnBuffer.writeWord(opcode);
        buffer.writeByte(version);
        buffer.writeDWord(buffer.getPtr());

        return buffer.getBuffer();
    }
}
