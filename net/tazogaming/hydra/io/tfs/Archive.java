package net.tazogaming.hydra.io.tfs;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.io.Buffer;

//~--- JDK imports ------------------------------------------------------------

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:07
 */
public class Archive {
    private Map<Integer, FileChunk> sectors = new HashMap<Integer, FileChunk>();
    private Buffer                  buffer;

    /**
     * Constructs ...
     *
     */
    public Archive() {}

    /**
     * Constructs ...
     *
     *
     * @param f
     *
     * @throws IOException
     */
    public Archive(File f) throws IOException {
        this.buffer = new Buffer(f);
        loadSectors();
    }

    /**
     * Method putSector
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param id
     * @param chunk
     */
    public void putSector(int id, FileChunk chunk) {
        this.sectors.put(id, chunk);
    }

    /**
     * Method size
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int size() {
        int siz = 0;

        for (FileChunk chunkToCheck : sectors.values()) {
            siz += chunkToCheck.getLength();
        }

        return siz;
    }

    /**
     * Method build
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public byte[] build() {
        Buffer buffer = new Buffer(size() + (sectors.size() * 7));

        for (FileChunk chunk : sectors.values()) {
            buffer.writeWord(chunk.getOpcode());
            buffer.writeByte(chunk.getVersion());
            buffer.writeDWord(chunk.getLength());
            buffer.writeBytes(chunk.getBuffer().getBuffer(), chunk.getBuffer().getPtr());
        }

        return buffer.getBuffer();
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param location
     *
     * @throws IOException
     */
    public void save(String location) throws IOException {
        DataOutputStream out = new DataOutputStream(new FileOutputStream(location));

        out.write(build());
        out.flush();
        out.close();
    }

    /**
     * Method sectorExists
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     *
     * @return
     */
    public boolean sectorExists(int sector) {
        return sectors.containsKey(sector);
    }

    /**
     * Method getSector
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param sector
     *
     * @return
     */
    public FileChunk getSector(int sector) {
        return sectors.get(sector);
    }

    private void loadSectors() {
        while (buffer.getPtr() < buffer.getBuffer().length) {
            int    sectorID   = buffer.readShort();
            int    version    = buffer.readByte();
            int    length     = buffer.readDWord();
            byte[] sectorData = new byte[length];

            buffer.readBytes(sectorData, length);
            sectors.put(sectorID, new FileChunk(new Buffer(sectorData, true), sectorID, version, length));
        }
    }

    /**
     * Method iterator
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public Iterator<FileChunk> iterator() {
        return sectors.values().iterator();
    }
}
