package net.tazogaming.hydra.io.tfs;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.io.Buffer;
import net.tazogaming.hydra.io.tfs.savegame.impl.*;
import net.tazogaming.hydra.util.Logger;

//~--- JDK imports ------------------------------------------------------------

import java.io.IOException;

import java.util.Iterator;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 09/08/14
 * Time: 09:26
 */
public class SaveGameFactory implements FileFactory<Player> {

    /** sectorDecoders made: 14/08/22 */
    private final static FileSectorCodec[] sectorDecoders = new FileSectorCodec[100];

    /** player made: 14/08/22 */
    private Player player;

    /**
     * Constructs ...
     *
     *
     * @param player
     */
    public SaveGameFactory(Player player) {
        this.player = player;
    }

    public enum Sectors {
        NOTHING(null),
        LOCATION(PlayerPositionSectorCodec.class),
        INVENTORY(PlayerInventorySectorCodec.class),
        EQUIPMENT(PlayerEquipmentSectorCodec.class),
        SKILLS(PlayerSkillsSectorCodec.class),
        PRIV_SETTINGS(PrivacySettingsSectorCodec.class),
        APPEARANCE(PlayerAppearanceSectorCodec.class),
        PLAYERVARS(PlayerVarSectorCodec.class),
        POH(PlayerHouseSectorCodec.class),
        MISC_SETTINGS(MiscSettingsSectorCodec.class),
        FRIENDS_IGNORE(FriendsIgnoresSectorCodec.class),
        FAMILIAR(FamiliarDataSectorCodec.class),
        FARMING(FarmingDataSectorCodec.class),
        MAIN_SETTINGS(MainSettingsSectorCodec.class),
        SCHEDULED_TASKS(ScriptedEventsSectorCodec.class),
        BANK(BankingSectorCodec.class),
        DUNGBIND(BindedDungSectorCodec.class),
        PUNISHMENTS(PlayerPunishmentSectorCodec.class),
        PRAYERINFO(PrayerInformation.class), PVPINFO(PVPSectorCodec.class),
        QUESTS(QuestsSector.class),
        DYNAMICSETTINGS(DynamicSettingsCodec.class),
        ACHIEVEMENTS(AchievementsCodecSector.class),
        SHOP_BUYBACK(BuybackCodecSector.class),
        KILLCOUNTS(KillCountsCodec.class),
        CAPES(CapeColoursCodec.class),
        GRANDEXCHANGE(GrandExchangeSectorCodec.class),
        DBOXTIME(DboxTimeCodec.class),
        RESTRAINTS(RestraintsSectorCodec.class);
        /** sector made: 14/08/22 */
        private FileSectorCodec sector;

        /**
         * Constructs ...
         *
         *
         * @param sec
         */
        private Sectors(Class sec) {
            try {
                if (sec != null) {
                    this.sector = (FileSectorCodec) sec.newInstance();
                }
            } catch (Exception ee) {
                ee.printStackTrace();
                System.exit(-1);
            }
        }

        /**
         * Method getSector
         * Created on 14/08/22
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public FileSectorCodec<Player> getSector() {
            return this.sector;
        }
    }

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param loader
     */
    @Override
    public void load(Archive loader) {
        synchronized (sectorDecoders) {
            Iterator<FileChunk> sectors = loader.iterator();

            while (sectors.hasNext()) {
                FileChunk chunk = sectors.next();

                try {
                    FileSectorCodec<Player> sector = Sectors.values()[chunk.getOpcode()].getSector();

                    if (sector == null) {
                        throw new SectorNotFoundException(player.getUsername(), chunk.getOpcode());
                    }

                    sector.load(chunk.getBuffer(), player);
                } catch (SectorNotFoundException er) {
                    Logger.debug(er.getMessage());
                } catch (Exception ee) {
                    Logger.debug("Warning, player: " + player.getId() + " " + player.getUsername()
                                 + " failed to load sector: " + chunk.getOpcode() + " " + chunk.getLength());
                    ee.printStackTrace();
                }
            }
        }
    }

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    @Override
    public void save() {
        try {
            Archive archive = new Archive();

            for (int i = 1; i < Sectors.values().length; i++) {
                Buffer buffer = new Buffer(100000);

                Sectors.values()[i].getSector().save(buffer, player);
                archive.putSector(i, new FileChunk(buffer, i, 1, buffer.getPtr()));
            }

            archive.save("characters/" + player.getId() + ".char");
        } catch (IOException ee) {
            ee.printStackTrace();
        }
    }
}
