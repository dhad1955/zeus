package net.tazogaming.hydra.io;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.net.Server;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.test.Main;
import net.tazogaming.hydra.util.Config;

//~--- JDK imports ------------------------------------------------------------

import java.util.Properties;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Enchanta Runescape 2 Emulator
 * Enchanta is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 15/10/13
 * Time: 00:57
 */
public class PlayerLoadPool {
    private PlayerLoadQueue loader;
    private PlayerLoadQueue saver;

    /**
     * Constructs ...
     *
     */
    public PlayerLoadPool() {
        Properties      dbProperties = Config.getDatabaseInfo();
        MysqlConnection con2         = new MysqlConnection(dbProperties.getProperty("DATABASE_HOST"),
                                           dbProperties.getProperty("DATABASE_USER"),
                                           dbProperties.getProperty("DATABASE_PASSWORD"),
                                           dbProperties.getProperty("DATABASE_NAME"));
        MysqlConnection con1 = new MysqlConnection(dbProperties.getProperty("DATABASE_HOST"),
                                   dbProperties.getProperty("DATABASE_USER"),
                                   dbProperties.getProperty("DATABASE_PASSWORD"),
                                   dbProperties.getProperty("DATABASE_NAME"));

        loader = new PlayerLoadQueue();
        loader.setLoader(new UserAccount());
        saver = new PlayerLoadQueue();
        saver.setLoader(new UserAccount());
        saver.saver_test = true;

       if(!Main.TEST_MODE) {

           con2.connect();
           con1.connect();
       }
        loader.setConnection(con1);
        saver.setConnection(con2);
        Server.newThread(loader, "load_queue", Thread.NORM_PRIORITY);
        Server.newThread(saver, "save_queue", Thread.MIN_PRIORITY);
    }

    /**
     * Method getLoader
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PlayerLoadQueue getLoader() {
        return loader;
    }

    /**
     * Method getSaver
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public PlayerLoadQueue getSaver() {
        return saver;
    }
}
