package net.tazogaming.hydra.io;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

import java.util.LinkedList;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 18/11/2014
 * Time: 02:07
 */
public class TextFile extends LinkedList<Line> {

    int lineCaret = 0;
    /**
     * Method fromPath
     * Created on 14/11/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path
     *
     * @return
     */
    public static final TextFile fromPath(String path) {
        TextFile text = new TextFile();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
            int            lineNo = 0;
            String         line   = "";

            while ((line = reader.readLine()) != null) {
                ++lineNo;
                if (line.startsWith("//")) {
                    continue;
                }

                text.add(new Line(line, lineNo));
            }

            reader.close();
        } catch (FileNotFoundException ignored) {
            System.err.println("Error loading file: "+path);
        }
        catch (IOException ignored) {}

        return text;
    }

    public void push(String str) {
            super.add(new Line(str, ++lineCaret));
    }

    public void save(String path) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(path));
        for(Line line : this){
            writer.write(line.getData());
            writer.newLine();
        }
        writer.flush();
        writer.close();

    }

    /**
     * Method exists
     * Created on 14/11/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public boolean exists() {
        return size() > 0;
    }
}
