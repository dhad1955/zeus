package net.tazogaming.hydra.io;

//~--- JDK imports ------------------------------------------------------------

import java.io.*;

/**
 * Created with IntelliJ IDEA.
 * User: RuneWar
 * Date: 25/09/13
 * Time: 19:59
 * To change this template use File | Settings | File Templates.
 */
public class IoUtils {

    /**
     * Method ReadFile
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param s
     *
     * @return
     */
    public static final byte[] ReadFile(String s) {
        try {
            File            file            = new File(s);
            int             i               = (int) file.length();
            byte            abyte0[]        = new byte[i];
            DataInputStream datainputstream = new DataInputStream(new BufferedInputStream(new FileInputStream(s)));

            datainputstream.readFully(abyte0, 0, i);
            datainputstream.close();

            return abyte0;
        } catch (Exception exception) {}

        return null;
    }

    /**
     * Method dumpFile
     * Dumps a byte array to f a file.
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param path the path to save the data to
     * @param data the buffer containing the data
     *
     * @throws IOException
     */
    public static void dumpFile(String path, byte[] data) throws IOException {
        DataOutputStream dos = new DataOutputStream(new FileOutputStream(path));

        dos.write(data);
        dos.flush();
        dos.close();
    }
}
