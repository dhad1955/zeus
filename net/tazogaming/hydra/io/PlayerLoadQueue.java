package net.tazogaming.hydra.io;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.entity3d.World;
import net.tazogaming.hydra.net.mysql.MysqlConnection;
import net.tazogaming.hydra.util.Logger;
import net.tazogaming.hydra.util.SystemUpdate;

//~--- JDK imports ------------------------------------------------------------

import java.util.ArrayList;
import java.util.List;

/**
 * Class description
 * Hydrascape 639 Game server
 * Copyright (C) Tazogaming 2014
 *
 *
 * @version        Enter version here..., 14/08/18
 * @author         Daniel Hadland
 */
public class PlayerLoadQueue implements Runnable {

    /** eventList made: 14/08/20 **/
    private ArrayList<PlayerEvent> eventList = new ArrayList<PlayerEvent>();

    /** processing made: 14/08/20 **/
    private ArrayList<PlayerEvent> processing = new ArrayList<PlayerEvent>();

    /** toBeProcessed made: 14/08/20 **/
    private int toBeProcessed = -1;

    /** loader made: 14/08/20 **/
    private PlayerLoader loader;

    /** myConnection made: 14/08/20 **/
    private MysqlConnection myConnection;

    private enum EventType { Load, Save, LobbyLoad }

    ;

    /**
     * Method setConnection
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param con
     */
    public void setConnection(MysqlConnection con) {
        myConnection = con;
    }

    /**
     * Method setLoader
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param pel
     */
    public void setLoader(PlayerLoader pel) {
        loader = pel;
    }

    private void checkConnection() {

        // if(myConnection.isClosed()){
        // }
        if (myConnection == null) {
            return;
        }

        int attempts = 1;

        while (myConnection.isClosed()) {
            Logger.err("Database link is down, attempting reconnect attempt: " + attempts);

            try {
                myConnection.connect();
            } catch (Exception EE) {
                EE.printStackTrace();
                Logger.log("Could not connect, will try again in 10 seconds");

                try {
                    Thread.sleep(10000);
                } catch (Exception ee) {}
            }
        }

        /* Keep the connection alive */
       if(System.currentTimeMillis() % 5000 == 0)
            myConnection.query("DESCRIBE hs_skills");
    }

    /**
     * Method toString
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public String toString() {
        return eventList.size() + " " + toBeProcessed + " " + processing.size();
    }

    public boolean saver_test = false;
    /**
     * Method run
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     */
    public void run() {
        do {
            checkConnection();

            while (eventList.isEmpty()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ie) {}
            }

            List<PlayerEvent> tmp = null;

            synchronized (eventList) {
                tmp           = (List<PlayerEvent>) eventList.clone();
                toBeProcessed = eventList.size();
                processing.addAll(eventList);
                eventList.clear();
            }

            for (PlayerEvent pa : tmp) {
                try {
                    if (pa.getType() == EventType.LobbyLoad) {

                        loader.load(pa.getPlayer(), this.myConnection, true);
                    } else if (pa.getType() == EventType.Load) {
                        Player p = pa.getPlayer();

                        p.setResponseCode(loader.load(p, this.myConnection, false));
                        World.getWorld().addPlayerAwaitingLogin(p);
                    } else if (pa.getType() == EventType.Save) {
                        loader.save(pa.getPlayer(), this.myConnection);
                    }
                } catch (UnsupportedOperationException uoe) {    // ignore it
                } catch (Exception e) {
                    e.printStackTrace();
                    Logger.err(e);
                }

                toBeProcessed--;
            }

            processing.clear();
            toBeProcessed = -1;
        } while (true);
    }

    /**
     * Method eventCount
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @return
     */
    public int eventCount() {
        return (toBeProcessed == -1)
               ? eventList.size()
               : toBeProcessed;
    }

    /**
     * Method pending
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param username
     *
     * @return
     */
    public boolean pending(String username) {
        synchronized (eventList) {
            for (PlayerEvent c : eventList) {
                if (c.getPlayer().getUsername().equalsIgnoreCase(username)) {
                    return true;
                }
            }
        }

        synchronized (processing) {
            for (PlayerEvent c : processing) {
                if (c.getPlayer().getUsername().equalsIgnoreCase(username)) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Method addLobbyLoad
     * Created on 14/08/20
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void addLobbyLoad(Player p) {
        if (pending(p.getUsername())) {
            return;
        }

        this.add(new PlayerEvent(p, EventType.LobbyLoad));
    }

    /**
     * Method addLoad
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void addLoad(Player p) {
        if (SystemUpdate.shutdown) {
            return;
        }

        if (pending(p.getUsername()) || World.getWorld().getIOPool().getSaver().pending(p.getUsername())) {
            p.setResponseCode((byte) 23);    // instructs to wait
            World.getWorld().addPlayerAwaitingLogin(p);

            return;
        }

        add(new PlayerEvent(p, EventType.Load));
    }

    /**
     * Method addSave
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     */
    public void addSave(Player p) {
        add(new PlayerEvent(p, EventType.Save));
    }

    private void add(PlayerEvent pe) {
        synchronized (eventList) {
            eventList.add(pe);
        }
    }

    /**
     * Class description
     * Hydrascape 639 Game server
     * Copyright (C) Tazogaming 2014
     *
     *
     * @version        Enter version here..., 14/08/18
     * @author         Daniel Hadland
     */
    public class PlayerEvent {

        /** p made: 14/08/20 **/
        private Player p;

        /** type made: 14/08/20 **/
        private EventType type;

        /** attachments made: 14/08/20 **/
        private Object[] attachments;

        /**
         * Constructs ...
         *
         *
         * @param p
         * @param pt
         * @param attachments
         */
        public PlayerEvent(Player p, EventType pt, Object... attachments) {
            this.p           = p;
            this.type        = pt;
            this.attachments = attachments;
        }

        /**
         * Method getAttachment
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @param i
         *
         * @return
         */
        public Object getAttachment(int i) {
            return attachments[i];
        }

        /**
         * Method getController
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public Player getPlayer() {
            return p;
        }

        /**
         * Method getType
         * Created on 14/08/18
         * @author Daniel Hadland
         * Hydrascape 639 Game server
         * For use on Tazogaming products only!
         * Contact: dan@tazogaming.net
         * http://www.tazogaming.com
         *
         * @return
         */
        public EventType getType() {
            return type;
        }
    }
}
