package net.tazogaming.hydra.io;

//~--- non-JDK imports --------------------------------------------------------

import net.tazogaming.hydra.entity3d.Player;
import net.tazogaming.hydra.net.mysql.MysqlConnection;

public interface PlayerLoader {

    /**
     * Method save
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param connection
     *
     * @throws UnsupportedOperationException
     */
    public void save(Player p, MysqlConnection connection) throws UnsupportedOperationException;

    /**
     * Method load
     * Created on 14/08/18
     * @author Daniel Hadland
     * Hydrascape 639 Game server
     * For use on Tazogaming products only!
     * Contact: dan@tazogaming.net
     * http://www.tazogaming.com
     *
     * @param p
     * @param connection
     *
     * @return
     *
     * @throws UnsupportedOperationException
     */
    public byte load(Player p, MysqlConnection connection, boolean lobby) throws UnsupportedOperationException;
}
