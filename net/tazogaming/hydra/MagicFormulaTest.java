package net.tazogaming.hydra;

import net.tazogaming.hydra.util.math.GameMath;

/**
 * Copyright (C) Tazogaming ltd
 * Official website: http://www.tazogaming.net
 * Zeus Runescape 2 Emulator
 * Zeus is a Runescape 2 Server emulator which has been designed
 * for educational purposes only
 * Created by Daniel Hadland
 * Date: 23/04/2015
 * Time: 09:12
 */
public class MagicFormulaTest {

    // current setup: hybrid
    //
    private static int mageDefenceBonus = 158;
    private static int mageLevel = 99;
    private static int mageAttackBonus = 113;
    private static int defenceLevel = 99;


    public static double getChance() {
            final double A = 0.705;
            double atkBonus = mageAttackBonus;
            double defBonus = mageDefenceBonus;
            if (atkBonus < 1) {
                atkBonus = 0;
            }
            if (defBonus < 1) {
                defBonus = 0;
            }

            double atk = (atkBonus * mageLevel);
            double def = (defBonus * defenceLevel);

            double OUTCOME = A * (atk / def);



            return OUTCOME;
        }

        //    return  accuracy;


    }




